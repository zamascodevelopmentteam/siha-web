export const c = {
  ALERT_TYPE: {
    SUCCESS: "success",
    DANGER: "danger",
    WARNING: "warning"
  },
  BREADCRUMBS: {
    DASHBOARD: "Dashboard",
    ARV_KHUSUS: "Jumlah Penggunaan ARV Khusus",
    HIV_IMS: "HIV-IMS",
    KONDISI_ODHA: "Kondisi ODHA Setelah ART",
    NOTIFIKASI_PASANGAN: "Notifikasi Pasangan",
    PASIEN_MASUK: "Pasien Masuk",
    PPK: "Pengobatan Pencegahan Kotrimoksazol",
    TB_HIV: "Koinfeksi TB-HIV",
    TPT: "Terapi Pencegahan TBC",
    VIRAL_LOAD: "Viral Load"
  },
  PATIENT_TYPE: {
    HIV: "HIV",
    ART: "ART"
  },
  PATIENT_TYPE_LABEL: {
    HIV: "Dalam Perawatan HIV",
    ART: "Masuk dengan ART"
  },
  STORES: {
    MSIHA: "msihaStore",
    COMMON: "commonStore",
    AUTH: "authStore",
    USER: "userStore"
  },
  VARIABLE_TYPE: {
    newOdhaHIV:
      "1.1 Jumlah ODHA baru yang masuk perawatan HIV selama bulan ini",
    odhaReferIn:
      "1.2 Jumlah ODHA yang dirujuk masuk selama perawatan HIV bulan ini",
    odhaReferOut:
      "1.3 Jumlah ODHA yang dirujuk keluar selama perawatan HIV bulan ini",
    cumulativeOdhaHIV:
      "1.4 Jumlah kumulatif ODHA pernah masuk perawatan HIV s/d akhir bulan ini",
    odhaNonArtNotVisit:
      "1.5 Jumlah ODHA yang belum mulai ART dan tidak hadir pada bulan ini",
    cumulativeOdhaNonArtDeath:
      "1.6 Jumlah kumulatif ODHA yang belum mulai ART meninggal sampai dengan bulan ini",
    totalVisitHiv:
      "1.7 Jumlah ODHA berkunjung ke perawatan HIV selama bulan ini",
    newOdhaArt: "2.1 Jumlah ODHA baru yang memulai ART selama bulan ini",
    odhaReferInArt:
      "2.2 Jumlah ODHA yang dirujuk masuk dengan ART selama bulan ini",
    odhaReferOutArt:
      "2.3 Jumlah ODHA yang dirujuk keluar dengan ART selama bulan ini",
    cumulativeOdhaArt:
      "2.4 Jumlah kumulatif ODHA pernah memulai ART s/d akhir bulan ini",

    odhaDeath: "3.1 Jumlah ODHA yang dilaporkan meninggal dunia s/d akhir bulan ini",
    odhaStopArt:  "3.2 Jumlah ODHA yang pasti menghentikan ART s/d  akhir bulan ini",
    odhaNotVisit: "3.3 Jumlah ODHA yang tidak hadir s/d akhir bulan ini ",
    failFollowUp3Month: "3.4 Jumlah ODHA yang gagal follow-up > 3 bulan s/d akhir bulan ini",
    cumulativeOdhaArt_3:
      "3.5 Jumlah kumulatif ODHA dengan ART s/d akhir bulan ini",
    odhaRejimen1Original:
      "3.5.1 Jumlah yang masih dengan rejimen lini ke-1 orisinal",
    odhaRejimen1: "3.5.2 Jumlah yang substitusi dalam rejimen lini ke-1",
    odhaRejimen2: "3.5.3 Jumlah switch ke rejimen lini ke-2",

    patients_tb: "4.1 Jumlah ODHA yang diskrining status TBC selama Bulan Ini.",
    patients_t_hiv:
      "4.2 Jumlah kasus baru orang dengan koinfeksi TB-HIV selama bulan ini",
    patients_tb_hiv_arv:
      "4.3 Jumlah kasus baru orang dengan koinfeksi TB-HIV dan mendapatkan OAT dan ARV selama bulan ini",

    new_patients_PPK:
      "5.1 Jumlah orang baru yang mendapat PPK selama bulan ini",
    patients_PPK: "5.2 Jumlah orang yang sedang mendapat PPK selama bulan ini",

    new_patients_PP_INH:
      "6.1 Jumlah ODHA baru masuk perawatan dan memulai PP INH selama bulan ini",
    old_patients_PP_INH:
      "6.2 Jumlah ODHA lama masuk perawatan (>30 hari) dan memulai PP INH selama bulan ini",
    patients_rifampentin:
      "6.3 Jumlah ODHA memulai Rifampentin selama bulan ini",

    patients_VL: "7.1 Jumlah ODHA yang dites VL selama bulan ini",
    patients_over_VL:
      "7.2 Jumlah ODHA yang jumlah virusnya tersupresi (≤1.000 kopi/ml) selama bulan ini",

    patients_ARV:
      "8.1 Jumlah ODHA yang mengambil ARV >1 bulan selama bulan ini",
    patients_profilaksis:
      "8.2 Jumlah orang yang mendapat profilaksis (pasca pajanan dan bayi baru lahir) selama bulan ini.",

    patients_offer_couple_notif:
      "9.1 Jumlah ODHA yang ditawarkan notifikasi pasangan selama bulan ini",
    patients_accept_offer_couple_notif:
      "9.2 Jumlah ODHA yang menerima tawaran notifikasi pasangan selama bulan ini",
    couple:
      "9.3 Jumlah pasangan/anak ODHA yang didata oleh ODHA selama bulan ini",

    odhaIms: "10.1 Jumlah ODHA yang Dites IMS selama bulan ini",
    odhaImsPositive: "10.2 Jumlah ODHA dengan IMS selama bulan ini",
    odhaSifilis: "10.3 Jumlah ODHA yang Dites Sifilis selama bulan ini",
    odhaSifilisPositive: "10.4 Jumlah ODHA dengan Sifilis Positif selama bulan ini",
    
    code: "name"
  }
};

export const ROLE = {
  LAB_STAFF: "LAB_STAFF",
  PHARMA_STAFF: "PHARMA_STAFF",
  RR_STAFF: "RR_STAFF",
  SUDIN_STAFF: "SUDIN_STAFF",
  PROVINCE_STAFF: "PROVINCE_STAFF",
  MINISTRY_STAFF: "MINISTRY_STAFF",
  DOCTOR: "DOCTOR"
};

export const ROLEURL = {
  LAB_STAFF: "lab",
  PHARMA_STAFF: "pharma",
  RR_STAFF: "rr"
};

export const colors = {
  black: "#333333",
  blackFull: "#000000",
  primary: "#1cc1c9",
  gray: "#e5e5e5",
  secondary: "#999999",
  red: "#ff4b4b",
  textActive: "#149147",
  white: "white",
  whiteTwo: "#ffffff",
  green: "#31b057",
  RR_STAFF: "#2a47f9",
  DOCTOR: "#2a47f9",
  LAB_STAFF: "#1cc1c9",
  PHARMA_STAFF: "#7c34d7",
  MINISTRY_STAFF: "#e9843b"
};

export const ENUM = {
  GENDER: {
    LAKI_LAKI: "LAKI-LAKI",
    PEREMPUAN: "PEREMPUAN"
  },
  STATUS_PATIENT: {
    BLM_TAHU: "Bukan ODHA",
    HIV_NEGATIF: "HIV NEGATIF",
    HIV_POSITIF: "HIV POSITIF", // ODHA
    ODHA: "ODHA", // ODHA Dalam Pengobatan
    ODHA_LAMA: "ODHA LAMA" // pengen jadi ODHA
  },
  LABEL_STATUS_PATIENT: {
    "BELUM TAHU": "Belum Diketahui",
    "HIV NEGATIF": "HIV NEGATIF",
    "HIV POSITIF": "ODHA",
    ODHA: "ODHA Dalam Pengobatan",
    "ODHA LAMA": "ODHA Masuk Perawatan"
  },
  VISIT_TYPE: {
    TEST: "TEST",
    TREATMENT: "TREATMENT"
  },
  VISIT_ACT_TYPE: {
    HIV: "HIV",
    IMS: "IMS",
    VL_CD4: "VL_CD4",
    TREATMENT: "TREATMENT"
  },
  TEST_TYPE: {
    HIV: "HIV",
    IMS: "IMS",
    VL_CD4: "VL_CD4"
  },
  KEL_POPULASI: {
    WPS: "WPS",
    WARIA: "Waria",
    PENASUN: "Penasun",
    LSL: "LSL",
    BUMIL: "Bumil",
    PASIEN_TB: "Pasien TB",
    PASIEN_IMS: "Pasien IMS",
    PASIEN_HEPATITIS: "Pasien Hepatitis",
    PASANGAN_ODHA: "Pasangan ODHA",
    ANAK_ODHA: "Anak ODHA",
    WBP: "WBP",
    POPULASI_UMUM: "Populasi Umum",
    PASANGAN_IMS: "pasangan IMS",
    PRIA_PENJAJA_SEKS: "Pria Penjaja Seks",
  },
  HIV_TEST_TYPE: {
    RDT: "RDT",
    PCR_DNA: "PCR-DNA (EID)",
    PCR_RNA: "PCR-RNA",
    // NAT: "NAT"
    WB: "WESTERN BLOT",
    ELISA: "ELISA"
  },
  RUJUKAN_LAB_LANJUT: {
    WB: "WESTERN BLOT",
    ELISA: "ELISA"
  },
  STATUS_TB: {
    NEGATIF_TB: "NEGATIF TB",
    POSITIF_TB: "POSITIF TB",
    TIDAK_TAU_STATUS: "TIDAK TAHU STATUS"
  },
  PERIKSA_TB: {
    NEGATIF_TB: "NEGATIF TB",
    POSITIF_TB: "POSITIF TB",
    TIDAK_DIPERIKSA: "TIDAK DIPERKSA",
    DALAM_PEMERIKSAAN: "DALAM PEMERIKSAAN"
  },
  STATUS_FUNGSIONAL: {
    KERJA: "KERJA",
    AMBULATORI: "AMBULATORI",
    BARING: "BARING"
  },
  STADIUM_KLINIS: {
    STADIUM_1: "STADIUM 1",
    STADIUM_2: "STADIUM 2",
    STADIUM_3: "STADIUM 3",
    STADIUM_4: "STADIUM 4"
  },
  STATUS_TB_TPT: {
    TDK_DIBERIKAN: "TIDAK DIBERIKAN",
    INH: "INH",
    "3HP": "3HP"
  },
  RUJUK_LAB_LANJUT: {
    WB: "WESTERN BLOT",
    ELISA: "ELISA"
  },
  STATUS_PADUAN: {
    ORISINAL: "ORISINAL",
    SUBSTITUSI: "SUBSTITUSI",
    SWITCH: "SWITCH"
  },
  TES_LAB: {
    VL: "VL (KOPI/ml)",
    CD4: "CD4 (SEL/m3)"
  },
  VL_TEST_TYPE: {
    VL: "VL",
    CD4: "CD4"
  },
  HASIL_LAB_SATUAN: {
    KOPI: "KOPI/ml",
    SEL: "SEL/m3"
  },
  LSM_RUJUKAN: {
    SPIRITIA: "SPIRITIA",
    IAC: "IAC",
    UNFPA: "UNFPA",
    IPPI: "IPPI",
    YPI: "YPI",
    PKVHI: "PKVHI",
    PKBI: "PKBI",
    KADER_KESEHATAN: "KADER KESEHATAN",
    //DOTS: "DOTS",
    LAPAS_RUTAN: "LAPAS RUTAN",
    HEPATITIS: "HEPATITIS",
    DOTSTB: "DOTS/TB",
    //TB: "TB",
    KIA: "KIA",
    POLI_LAINNYA: "POLI_LAINNYA",
    DATANG_SENDIRI: "DATANG_SENDIRI",
  },

  LSM_PENDAMPING: {
    SPIRITIA: "SPIRITIA",
    IAC: "IAC",
    UNFPA: "UNFPA",
    IPPI: "IPPI",
    YPI: "YPI",
    PKVHI: "PKVHI",
    PKBI: "PKBI",
    KADER_KESEHATAN: "KADER KESEHATAN"
  },

  ALASAN_TEST: {
    BY_DOCTOR: "Rekomendasi Dokter",
    SECOND_OPINION: "Second Opinion",
    SUKARELA: "Sukarela",
    NOTIF_COUPLE: "Notifikasi Pasangan",
    SKRINING: "SKRINING"
  },
  ALASAN_TEST_IMS: {
    SKRINING: "SKRINING",
    SAKIT: "SAKIT",
    FOLLOWUP: "FOLLOWUP",
  },
  KELUHAN_IMS: {
    DUH_TUBUH: "DUH_TUBUH",
    GATAL: "GATAL",
    KENCING_SAKIT: "KENCING_SAKIT",
    NYERI_PERUT: "NYERI_PERUT",
    LECET: "LECET",
    BINTIL_SAKIT: "BINTIL_SAKIT",
    LUKA_ULKUS: "LUKA_ULKUS",
    JENGGER: "JENGGER",
    BENJOLAN: "BENJOLAN",
    TIDAK_ADA: "TIDAK_ADA"
  },
  PREGNANCY_STEP: {
    TRIMESTER_1: 'TRIMESTER_1',
    TRIMESTER_2: 'TRIMESTER_2',
    TRIMESTER_3: 'TRIMESTER_3'
  },
  TANDA_KLINIS_IMS_LAKI: {
    nyeri_perut: "Nyeri Perut",
    lecet: "Lecet",
    bintil_sakit: "Bintil Sakit",
    luka_ulkus: "Luka Ulkus",
    // jengger: "Jengger",
    vegetasi: "Vegetasi",
    bubo: "Bubo",
    dtu: "DTU",
    scortum_bengkak: "Scortum Bengkak",
    dta: "DTA",
    dtm: "DTM",
    tidak_ada: "Tidak Ada",
    lainnya: "Lainnya",
  },
  TANDA_KLINIS_IMS_PEREMPUAN: {
    dtv: "DTV",
    dts: "DTS",
    nyeri_perut: "Nyeri Perut",
    lecet: "Lecet",
    bintil_sakit: "Bintil Sakit",
    luka_ulkus: "Luka/Ulkus",
    // jengger: "Jengger",
    vegetasi: "Vegetasi",
    bubo: "Bubo",
    nyeri_goyang_serviks: "Nyeri Goyang Serviks",
    dta: "DTA",
    dtm: "DTM",
    menstruasi: 'Menstruasi',
    tidak_ada: "Tidak Ada",
    lainnya: "Lainnya",
  },
  DIAGNOSIS_SYNDROME_LAKI: {
    "dtu": "Duh Tubuh Uretra",
    "dta": "Duh Tubuh Anus",
    "dtm": "Duh Tubuh Mata",
    "prp": "Penyakit Radang Panggul",
    "ug": "Ulkus genital",
    "pembengkakanSkrotum": "Pembengkakan Skortum",
    "bi": "Bubo inguinal"
  },
  DIAGNOSIS_SYNDROME_PEREMPUAN: {
    "dtv": "Duh Tubuh Vagina",
    "dtu": "Duh Tubuh Uretra",
    "dta": "Duh Tubuh Anus",
    "dtm": "Duh Tubuh Mata",
    "prp": "Penyakit Radang Panggul",
    "ug": "Ulkus genital",
    "bi": "Bubo inguinal"
  },
  DIAGNOSIS_KLINIS_LAKI: {
    "herpes": "Herpes genitalis",
    "sifilis": "Sifilis primer",
    "sifilisSekunder": "Sifilis sekunder",
    "chancroid": "Chancroid",
    "kutilAnogenital": "Kutil anogenital",
    "uretritis": "Uretritis gonore",
    "moluskum": "Moluskum",
    "kontagiosum": "Kontagiosum"
  },
  DIAGNOSIS_KLINIS_PEREMPUAN: {
    "herpes": "Herpes genitalis",
    "sifilis": "Sifilis primer",
    "sifilisSekunder": "Sifilis sekunder",
    "chancroid": "Chancroid",
    "kutilAnogenital": "Kutil anogenital",
    "uretritis": "Uretritis gonore",
    "moluskum": "Moluskum",
    "kontagiosum": "Kontagiosum"
  },
  DIAGNOSIS_LAB_LAKI: {
    "uritritisGonore": "Uritritis gonore",
    "uritritisNonGonore": "Uritritis non gonore",
    "proktitisGonore": "Proktitis gonore",
    "proktitisNonGonore": "Proktitis non gonore",
    "konjungtivitisGonore": "Konjungtivitis gonore",
    "konjungtivitisNonGonore": "Konjungtivitis non gonore",
    "sifilisDini": "Sifilis dini",
    "sifilisLaten ": "Sifilis laten ",
    "sifilisKongenital": "Sifilis kongenital",
    "trikomoniasis": "Trikomoniasis",
    "vaginosisBakterial": "Vaginosis bakterial",
    "kandidiasis": "Kandidiasis",
    "limfogranulomaVenereum": "Limfogranuloma venereum",
  },
  DIAGNOSIS_LAB_PEREMPUAN: {
    "servisitisGonore": "Servisitis gonore",
    "ServisitisNonGonore": "Servisitis non gonore",
    "VaginitisGonore": "Vaginitis gonore",
    "VaginitisNonGonore": "Vaginitis non gonore",
    // -----------
    "uritritisGonore": "Uritritis gonore",
    "uritritisNonGonore": "Uritritis non gonore",
    "proktitisGonore": "Proktitis gonore",
    "proktitisNonGonore": "Proktitis non gonore",
    "konjungtivitisGonore": "Konjungtivitis gonore",
    "konjungtivitisNonGonore": "Konjungtivitis non gonore",
    "sifilisDini": "Sifilis dini",
    "sifilisLaten ": "Sifilis laten",
    "sifilisKongenital": "Sifilis kongenital",
    "trikomoniasis": "Trikomoniasis",
    "vaginosisBakterial": "Vaginosis bakterial",
    "kandidiasis": "Kandidiasis",
    "limfogranulomaVenereum": "Limfogranuloma venereum",
  },
  NOTIF_COUPLE: {
    MENOLAK: "menolak",
    MENERIMA: "menerima",
    TDK_MEMENUHI: "tidak memenuhi syarat"
  },
  REAGEN_RESULT: {
    REAKTIF: "REAKTIF",
    NON_REAKTIF: "NON_REAKTIF",
    INVALID: "INVALID"
  },
  TEST_RESULT: {
    REAKTIF: "REAKTIF",
    NON_REAKTIF: "NON_REAKTIF",
    INKONKLUSIF: "INKONKLUSIF"
  },
  TEST_RESULT_RNA: {
    INVALID: "INVALID",
    UNDETECTED: "UNDETECTED",
    DETECTED: "DETECTED"
  },
  TEST_RESULT_DNA: {
    INVALID: "INVALID",
    NOT_DETECTED: "NOT DETECTED",
    DETECTED: "DETECTED",
    ERROR: "ERROR",
    NO_RESULT: "NO RESULT"
  },
  REAGEN_TYPE_ALL: {
    VIRAL_LOAD: "Viral Load",
    CD4: "CD4",
    EID: "EID"
  },
  REAGEN_TYPE: {
    VIRAL_LOAD: "Viral Load",
    CD4: "CD4"
  },
  REAGEN_TYPE_DBS: {
    VIRAL_LOAD: "Viral Load",
    EID: "EID"
  },
  LAST_ORDER_STATUS: {
    DRAFT: "DRAFT",
    EVALUATION: "EVALUATION",
    PROCESSED: "PROCESSED",
    EXECUTED: "EXECUTED",
    REJECTED: "REJECTED"
  },
  LAST_APPROVAL_STATUS: {
    DRAFT: "DRAFT",
    IN_EVALUATION: "IN_EVALUATION",
    APPROVED_INTERNAL: "APPROVED_INTERNAL",
    APPROVED_FULL: "APPROVED_FULL",
    REJECTED_INTERNAL: "REJECTED_INTERNAL",
    REJECTED_UPPER: "REJECTED_UPPER",
    REJECTED: "REJECTED"
  },
  PLAN_STATUS: {
    DRAFT: "DRAFT",
    EVALUATION: "EVALUATION",
    PROCESSED: "PROCESSED",
    ACCEPTED_FULLY: "ACCEPTED_FULLY",
    ACCEPTED_DIFFERENT: "ACCEPTED_DIFFERENT",
    LOST: "LOST",
    REJECTED: "REJECTED"
  },
  RECEIPT_STATUS: {
    ACCEPTED_FULLY: "ACCEPTED_FULLY",
    ACCEPTED_DIFFERENT: "ACCEPTED_DIFFERENT",
    LOST: "LOST"
  },
  RECEIPT_ITEM_STATUS: {
    NOT_CHANGED: "NOT_CHANGED",
    MORE_THAN_DEMAND: "MORE_THAN_DEMAND",
    LESS_THAN_DEMAND: "LESS_THAN_DEMAND"
  },
  ORDER_STATUS_STATE: {
    DRAFT: "DRAFT",
    IN_EVALUATION: "EVALUATION",
    APPROVED_INTERNAL: "EVALUATION",
    APPROVED_FULL: "PROCESSED",
    REJECTED_INTERNAL: "REJECTED",
    REJECTED_UPPER: "REJECTED"
  },
  AKHIR_FOLLOW_UP: {
    RUJUK_KELUAR: "RUJUK KELUAR",
    MENINGGAL: "MENINGGAL",
    BERHENTI_ARV: "BERHENTI ARV"
  },
  DIST_STATUS_STATE: {
    DRAFT: "DRAFT",
    IN_EVALUATION: "EVALUATION",
    APPROVED_FULL: "PROCESSED",
    REJECTED: "REJECTED"
  },
  FUND_SOURCE: {
    APBN: "APBN",
    NON_APBN: "NON_APBN"
  },
  LOGISTIC_ROLE: {
    LAB_ENTITY: "LAB_ENTITY",
    PHARMA_ENTITY: "PHARMA_ENTITY",
    UPK_ENTITY: "UPK_ENTITY",
    SUDIN_ENTITY: "SUDIN_ENTITY",
    PROVINCE_ENTITY: "PROVINCE_ENTITY",
    MINISTRY_ENTITY: "MINISTRY_ENTITY"
  },
  USER_ROLE: {
    ADMIN: "ADMIN",
    LAB_STAFF: "LAB_STAFF",
    PATIENT: "PATIENT",
    PHARMA_STAFF: "PHARMA_STAFF",
    RR_STAFF: "RR_STAFF",
    SUDIN_STAFF: "SUDIN_STAFF",
    PROVINCE_STAFF: "PROVINCE_STAFF",
    MINISTRY_STAFF: "MINISTRY_STAFF",
    DOCTOR: "DOCTOR"
  },
  PKG_UNIT_TYPE: {
    TABLET: "TABLET",
    TEST: "TEST",
    KAPSUL: "KAPSUL",
    PAKET: "PAKET",
    VIAL: "VIAL",
    PCS: "PCS",
    BUAH: "BUAH",
    KIT: "KIT",
    BOTOL: "BOTOL",
    BOX: "BOX",
    SACHET: "SACHET"
  },
  MEDICINE_TYPE: {
    ARV: "ARV",
    NON_ARV: "NON_ARV"
  },
  MEDICINE_SEDIAAN: {
    TUNGGAL: "TUNGGAL",
    BOOSTED: "BOOSTED",
    KDT_FDC: "KDT/FDC",
    FDC_PEDIATRIK: "FDC_PEDIATRIK"
  },
  ADJUSTMENT_TYPE: {
    ADDITION: "ADDITION",
    DEDUCTION: "DEDUCTION"
  },
  UNIT_TYPE: {
    SATUAN: "SATUAN",
    PAKET: "PAKET"
  },
  HASIL_TEST_VLCD4: {
    INVALID: "INVALID",
    NOT_DETECTED: "NOT DETECTED",
    ERROR: "ERROR",
    NO_RESULT: "NO RESULT",
    INPUT_ANGKA: "INPUT ANGKA"
  },
  ALASAN_TEST_VLCD4: {
    MONITORING_ROUTINE: "Monitoring Routine",
    GAGAL_TERAPI: "Gagal Terapi",
    PPIA: "PPIA"
  },
  HASIL_TEST_VLCD4_NEW: {
    20: "< 20",
    34: "< 34",
    40: "< 40",
    50: "< 50",
    NOT_DETECTED: "NOT DETECTED",
    INVALIDERROR: "INVALID / ERROR",
    INPUT_ANGKA: "INPUT ANGKA"
  },
  PARALEL_CHECK: {
    TIDAK_PARALEL: "Tidak Paralel",
    // PARALEL_2: "Paralel 2 Test",
    // PARALEL_3: "Paralel 3 Test"
    PARALEL: "Paralel Test"
  },
  RELATIONSHIP: {
    SUAMI_ISTRI: "Suami atau Istri",
    ANAK_KANDUNG: "Anak Kandung",
    ORANG_TUA: "Orang Tua Kandung",
    MITRA_SEKS: "Pasangan Seks",
    MITRA_JARUM: "Mitra Berbagi Jarum"
  },
  MEDICINE_CATEGORIES: {
    IMS: "IMS", 
    REAGEN_IMS: "Reagen IMS",
    HIV: "HIV",
    ALKES: "Alkes",
    REAGEN_HIV: "Reagen HIV",
    IO: "IO",
    IO_TB: "IO (TB)",
  },
  VLCD4CATEGORY: {
    "Abbott": "Abbott",
    "Gen Expert": "Gen Expert",
    "Roche": "Roche",
    "Pima CD4": "Pima CD4",
    "FacsCount CD4 Close System": "FacsCount CD4 Close System",
    "Trucount CD4 Open System": "Trucount CD4 Open System"
  },
  VLCD4CATEGORY_OPTIONS: {
    VL: {
      "Abbott": "Abbott",
      "Gen Expert": "Gen Expert",
      "Roche": "Roche"
    },
    CD4: {
      "Pima CD4": "Pima CD4",
      "FacsCount CD4 Close System": "FacsCount CD4 Close System",
      "Trucount CD4 Open System": "Trucount CD4 Open System"
    }
  },
};

export const LOGISTIK = {
  PENERIMAAN_TYPE: {
    IN: "in",
    OUT: "out"
  }
};

export const LIST_APPROVAL_STATUS = [
  "ALL",
  "DRAFT",
  "IN_EVALUATION",
  "APPROVED_INTERNAL",
  "REJECTED_INTERNAL",
  "APPROVED_FULL",
  "REJECTED_UPPER",
  "REJECTED"
];

export const LIST_PLAN_STATUS = [
  "ALL",
  "PROCESSED",
  "ACCEPTED_FULLY",
  "ACCEPTED_DIFFERENT"
  // "LOST"
];

export const LIST_JENIS_PENGIRIMAN = [
  "ALL",
  "DISTRIBUSI_KHUSUS",
  "PENGIRIMAN_REGULER",
  "PENGIRIMAN_KHUSUS"
];

export const LIST_RELOKASI = ["ALL", "YA", "TIDAK"];

export const REQUEST_TYPE = ["ALL", "PERMINTAAN_REGULER", "PERMINTAAN_KHUSUS"];

export const TAG_LABEL = {
  TAG_APPROVAL_STATUS: {
    ALL: "#262626",
    DRAFT: "#bfbfbf",
    IN_EVALUATION: "#d3adf7",
    APPROVED_INTERNAL: "#40a9ff",
    APPROVED_FULL: "#73d13d",
    REJECTED_INTERNAL: "#cf1322",
    REJECTED_UPPER: "#cf1322",
    REJECTED: "#cf1322",
    ACCEPTED_FULLY: "#73d13d",
    ACCEPTED_DIFFERENT: "#f759ab",
    LOST: "#612500"
  },
  TAG_APPROVAL_STATUS_IN: {
    ALL: "#262626",
    DRAFT: "#bfbfbf",
    IN_EVALUATION: "#d3adf7",
    APPROVED_INTERNAL: "#40a9ff",
    APPROVED_FULL: "#73d13d",
    REJECTED_INTERNAL: "#cf1322",
    REJECTED_UPPER: "#cf1322",
    REJECTED: "#cf1322",
    ACCEPTED_FULLY: "#73d13d",
    ACCEPTED_DIFFERENT: "#f759ab",
    LOST: "#612500"
  },
  TAG_PLAN_STATUS: {
    ALL: "#262626",
    DRAFT: "#bfbfbf",
    EVALUATION: "#d3adf7",
    PROCESSED: "#ffc069",
    ACCEPTED_FULLY: "#73d13d",
    ACCEPTED_DIFFERENT: "#f759ab",
    LOST: "#612500",
    EXECUTED: "#08979c",
    REJECTED: "#cf1322"
  },
  TAG_PLAN_STATUS_IN: {
    ALL: "#262626",
    DRAFT: "#bfbfbf",
    EVALUATION: "#bfbfbf",
    PROCESSED: "#ffc069",
    ACCEPTED_FULLY: "#73d13d",
    ACCEPTED_DIFFERENT: "#f759ab",
    LOST: "#612500",
    EXECUTED: "#08979c",
    REJECTED: "#cf1322"
  },
  LABEL_APPROVAL_STATUS: {
    ALL: "Semua status",
    DRAFT: "Draft",
    IN_EVALUATION: "Review Pengelola Program",
    APPROVED_INTERNAL: "Disetujui Pengelola Program",
    APPROVED_FULL: "Disetujui Pengirim",
    REJECTED_INTERNAL: "Tidak Disetujui Pengelola Program",
    REJECTED_UPPER: "Tidak Disetujui Pengirim",
    ACCEPTED_FULLY: "Diterima Penuh",
    ACCEPTED_DIFFERENT: "Diterima Berbeda",
    LOST: "Hilang",
    REJECTED: "Tidak Disetujui"
  },
  LABEL_APPROVAL_STATUS_IN: {
    ALL: "Semua status",
    DRAFT: "Draft",
    IN_EVALUATION: "Review",
    APPROVED_INTERNAL: "Review",
    APPROVED_FULL: "Disetujui",
    REJECTED_INTERNAL: "Tidak Disetujui Pengelola Program",
    REJECTED_UPPER: "Tidak Disetujui",
    ACCEPTED_FULLY: "Diterima Penuh",
    ACCEPTED_DIFFERENT: "Diterima Berbeda",
    LOST: "Hilang",
    REJECTED: "Tidak Disetujui"
  },
  LABEL_PLAN_STATUS: {
    ALL: "Semua status",
    DRAFT: "Draf",
    EVALUATION: "Dalam Evaluasi",
    PROCESSED: "Dalam Proses",
    ACCEPTED_FULLY: "Diterima Penuh",
    ACCEPTED_DIFFERENT: "Diterima Berbeda",
    LOST: "Hilang",
    EXECUTED: "Selesai",
    REJECTED: "Tidak Disetujui"
  },
  LABEL_PLAN_STATUS_IN: {
    ALL: "Semua status",
    DRAFT: "Draf",
    EVALUATION: "Draf",
    PROCESSED: "Dalam Proses Pemenuhan",
    ACCEPTED_FULLY: "Diterima Penuh",
    ACCEPTED_DIFFERENT: "Diterima Berbeda",
    LOST: "Hilang",
    EXECUTED: "Selesai",
    REJECTED: "Tidak Disetujui"
  },
  TAG_KETERSEDIAAN_STOK: {
    STOCKOUT: "red",
    SHORTAGE: "orange",
    NORMAL: "green",
    OVERSTOCK: "blue"
  },
  LABEL_KETERSEDIAAN_STOK: {
    STOCKOUT: "Stock Out",
    SHORTAGE: "Shortage",
    NORMAL: "Normal",
    OVERSTOCK: "Overstock"
  },
  DIST_TYPE: {
    // HIBAH: "HIBAH",
    // LUAR_NEGERI: "LUAR_NEGERI",
    // PEMBELIAN_APBN: "PEMBELIAN_APBN",
    // PEMBELIAN_APBD: "PEMBELIAN_APBD",
    // BINA_FARMA: "BINA_FARMA",
    // DAK: "Pembelian DAK",
    // BLUD: "Pembelian BLUD",
    HIBAH_BANTUAN: "HIBAH_BANTUAN",
    PEMBELIAN_APBN: "PEMBELIAN_APBN",
    PEMBELIAN_APBD: "PEMBELIAN_APBD",
    PEMBELIAN_MANDIRI: "PEMBELIAN_MANDIRI"
  },
  LABEL_DIST_TYPE: {
    HIBAH: "Hibah Dalam Negeri",
    // PEMBELIAN_APBN: "Pembelian APBN (Penyedia)",
    // PEMBELIAN_APBD: "Pembelian APBD",
    LUAR_NEGERI: "Hibah Luar Negeri",
    BINA_FARMA: "Pembelian APBN",
    DAK: "Pembelian DAK",
    BLUD: "Pembelian BLUD",
    // new --------------------
    HIBAH_BANTUAN: "Hibah / Bantuan",
    PEMBELIAN_APBN: "Pembelian APBN",
    PEMBELIAN_APBD: "Pembelian APBD",
    PEMBELIAN_MANDIRI: "Pembelian Mandiri"
  },
  TAG_REQUEST_TYPE: {
    ALL: "#262626",
    PERMINTAAN_REGULER: "#1890ff",
    PERMINTAAN_KHUSUS: "#fa541c"
  },
  VALUE_REQUEST_TYPE: {
    ALL: "ALL",
    PERMINTAAN_REGULER: true,
    PERMINTAAN_KHUSUS: false
  },
  LABEL_REQUEST_TYPE: {
    ALL: "Semua Permintaan",
    PERMINTAAN_REGULER: "Permintaan Reguler",
    PERMINTAAN_KHUSUS: "Permintaan Khusus"
  },
  TAG_JENIS_PENGIRIMAN: {
    ALL: "#262626",
    DISTRIBUSI_KHUSUS: "#7cb305",
    PENGIRIMAN_KHUSUS: "#fa541c",
    PENGIRIMAN_REGULER: "#1890ff"
  },
  LABEL_JENIS_PENGIRIMAN: {
    ALL: "Semua Pengiriman",
    DISTRIBUSI_KHUSUS: "Distribusi Khusus",
    PENGIRIMAN_KHUSUS: "Pengiriman Khusus",
    PENGIRIMAN_REGULER: "Pengiriman Reguler"
  },
  TAG_RELOKASI: {
    ALL: "#262626",
    YA: "#fa541c",
    TIDAK: "#1890ff"
  },
  LABEL_RELOKASI: {
    ALL: "Semua Data",
    YA: "Ya",
    TIDAK: "Tidak"
  }
};

export const SCORING = {
  ODHA_BARU: {
    label: "ODHA baru",
    answer: [
      {
        label: "Layanan mampu tes dan pengobatan",
        score: 0
      },
      {
        label: "Layanan hanya mampu tes",
        score: 1
      },
    ]
  },
  BARU_MULAI_PENGOBATAN: {
    label: "Baru mulai pengobatan",
    answer: [
      {
        label: "Tanpa keluhan",
        score: 0
      },
      {
        label: "Keluhan ringan",
        score: 1
      },
      {
        label: "Keluhan sedang",
        score: 2
      },
      {
        label: "Keluhan berat",
        score: 3
      },
    ]
  },
  MENGALAMI_IO: {
    label: "Mengalami IO",
    answer: [
      {
        label: "tanpa IO",
        score: 0
      },
      {
        label: "IO ringan",
        score: 1
      },
      {
        label: "IO sedang",
        score: 2
      },
      {
        label: "IO berat",
        score: 3
      },
    ]
  },
  PUTUS_ARV: {
    label: "Putus ARV",
    answer: [
      {
        label: "Tidak",
        score: 0
      },
      {
        label: "Ya",
        score: 1
      }
    ]
  },
  TIDAK_ADA_KELUARGA: {
    label: "Tidak ada keluarga",
    answer: [
      {
        label: "Ada",
        score: 0
      },
      {
        label: "Ya",
        score: 1
      }
    ]
  },
  DITINGGAL_KELUARGA: {
    label: "Ditinggal keluarga",
    answer: [
      {
        label: "Tidak",
        score: 0
      },
      {
        label: "Ya",
        score: 1
      }
    ]
  },
  ADA_MASALAH_KEJIWAAN: {
    label: "Ada masalah kejiwaan(putus asa, ??)",
    answer: [
      {
        label: "Tidak ada",
        score: 0
      },
      {
        label: "Ada",
        score: 1
      }
    ]
  },
  ADA_EFEK_SAMPING: {
    label: "Ada efek samping",
    answer: [
      {
        label: "Tidak",
        score: 0
      },
      {
        label: "Ringan (pusing)",
        score: 1
      },
      {
        label: "Sedang (mual, lemes)",
        score: 2
      },
      {
        label: "Berat (mimpi buruk, halusinasi)",
        score: 3
      },
    ]
  }
};

export const sumberDana = [
  'APBN 2018',
  'APBN 2019',
  'APBN 2020',
  'APBN 2021',
  'APBN 2022',
  'APBN 2023',
  'APBN 2024',
  'APBN 2025',
  'APBN 2026',
  'APBN 2027',
  'APBN 2028',
  'APBN 2029',
  'GF 2018',
  'GF 2019',
  'GF 2020',
  'GF 2021',
  'GF 2022',
  'GF 2023',
  'GF 2024',
  'GF 2025',
  'GF 2026',
  'GF 2027',
  'GF 2028',
  'GF 2029',
  'APBD 2018',
  'APBD 2019',
  'APBD 2020',
  'APBD 2021',
  'APBD 2022',
  'APBD 2023',
  'APBD 2024',
  'APBD 2025',
  'APBD 2026',
  'APBD 2027',
  'APBD 2028',
  'APBD 2029',
  'DAK 2018',
  'DAK 2019',
  'DAK 2020',
  'DAK 2021',
  'DAK 2022',
  'DAK 2023',
  'DAK 2024',
  'DAK 2025',
  'DAK 2026',
  'DAK 2027',
  'DAK 2028',
  'DAK 2029',
  'BLUD 2018',
  'BLUD 2019',
  'BLUD 2020',
  'BLUD 2021',
  'BLUD 2022',
  'BLUD 2023',
  'BLUD 2024',
  'BLUD 2025',
  'BLUD 2026',
  'BLUD 2027',
  'BLUD 2028',
  'BLUD 2029',
  'Pembelian Mandiri 2018',
  'Pembelian Mandiri 2019',
  'Pembelian Mandiri 2020',
  'Pembelian Mandiri 2021',
  'Pembelian Mandiri 2022',
  'Pembelian Mandiri 2023',
  'Pembelian Mandiri 2024',
  'Pembelian Mandiri 2025',
  'Pembelian Mandiri 2026',
  'Pembelian Mandiri 2027',
  'Pembelian Mandiri 2028',
  'Pembelian Mandiri 2029',
  'Sumber Lain'
];
