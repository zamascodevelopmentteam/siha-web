import { action, observable } from "mobx";
import API from "utils/API";

export interface IMsihaStore {
  inProgress: boolean;
  errors: any;
  listEntriArt: any;
  listEntriHiv: any;
  listHivIms: any;
  listCoenfectionTb: any;
  listTreatmentPpk: any;
  listTherapyTpt: any;
  listViralLoad: any;
  listNotifCouple: any;
  listArvSpecial: any;
  listAfterArt: any;
  generateFakeData(): any;
  getEntriArt(): Promise<any>;
  getEntriHiv(): Promise<any>;
  getHivIms(): Promise<any>;
  getCoenfectionTb(): Promise<any>;
  getTreatmentPpk(): Promise<any>;
  getTherapyTpt(): Promise<any>;
  getViralLoad(): Promise<any>;
  getNotifCouple(): Promise<any>;
  getArvSpecial(): Promise<any>;
  getAfterArt(): Promise<any>;
  setDataPastienMasuk(listDataPastien: any): void;
}

export class MsihaStore {
  @observable
  inProgress = false;

  @observable
  errors = undefined;

  @observable
  listEntriArt = [];

  @observable
  listEntriHiv = [];

  @observable
  listHivIms = [];

  @observable
  listCoenfectionTb = [];

  @observable
  listTreatmentPpk = [];

  @observable
  listTherapyTpt = [];

  @observable
  listViralLoad = [];

  @observable
  listNotifCouple = [];

  @observable
  listArvSpecial = [];

  @observable
  listAfterArt = [];

  generateFakeData = () => {
    let fake: Array<any> = [];
    for (let y = 0; y < 12; y++) {
      // fake.push(Math.round(Math.random() * 10));
      fake.push("-");
    }
    return fake;
  };

  @action
  getEntriHiv() {
    this.inProgress = true;
    this.listEntriHiv = [];
    return API.Msiha.enterHiv()
      .then(rsp => {
        this.listEntriHiv = rsp.data;
      })
      .catch(({ data }) => {
        this.errors = data.message;
        throw data;
      })
      .finally(() => {
        this.inProgress = false;
      });
  }

  @action
  getEntriArt() {
    this.inProgress = true;
    this.listEntriArt = [];
    return API.Msiha.enterArt()
      .then(rsp => {
        this.listEntriArt = rsp.data;
      })
      .catch(({ data }) => {
        this.errors = data.message;
        throw data;
      })
      .finally(() => {
        this.inProgress = false;
      });
  }

  @action
  getHivIms() {
    this.inProgress = true;
    this.listHivIms = [];
    return API.Msiha.hivIms()
      .then(rsp => {
        this.listHivIms = rsp.data;
      })
      .catch(({ data }) => {
        this.errors = data.message;
        throw data;
      })
      .finally(() => {
        this.inProgress = false;
      });
  }

  @action
  getCoenfectionTb() {
    this.inProgress = true;
    this.listCoenfectionTb = [];
    return API.Msiha.coenfectionTb()
      .then(rsp => {
        this.listCoenfectionTb = rsp.data;
      })
      .catch(({ data }) => {
        this.errors = data.message;
        throw data;
      })
      .finally(() => {
        this.inProgress = false;
      });
  }

  @action
  getTreatmentPpk() {
    this.inProgress = true;
    this.listTreatmentPpk = [];
    return API.Msiha.treatmentPpk()
      .then(rsp => {
        this.listTreatmentPpk = rsp.data;
      })
      .catch(({ data }) => {
        this.errors = data.message;
        throw data;
      })
      .finally(() => {
        this.inProgress = false;
      });
  }

  @action
  getTherapyTpt() {
    this.inProgress = true;
    this.listTherapyTpt = [];
    return API.Msiha.therapyTpt()
      .then(rsp => {
        this.listTherapyTpt = rsp.data;
      })
      .catch(({ data }) => {
        this.errors = data.message;
        throw data;
      })
      .finally(() => {
        this.inProgress = false;
      });
  }

  @action
  getViralLoad() {
    this.inProgress = true;
    this.listViralLoad = [];
    return API.Msiha.viralLoad()
      .then(rsp => {
        this.listViralLoad = rsp.data;
      })
      .catch(({ data }) => {
        this.errors = data.message;
        throw data;
      })
      .finally(() => {
        this.inProgress = false;
      });
  }

  @action
  getNotifCouple() {
    this.inProgress = true;
    this.listNotifCouple = [];
    return API.Msiha.notifCouple()
      .then(rsp => {
        this.listNotifCouple = rsp.data;
      })
      .catch(({ data }) => {
        this.errors = data.message;
        throw data;
      })
      .finally(() => {
        this.inProgress = false;
      });
  }

  @action
  getArvSpecial() {
    this.inProgress = true;
    this.listArvSpecial = [];
    return API.Msiha.arvSpecial()
      .then(rsp => {
        this.listArvSpecial = rsp.data;
      })
      .catch(({ data }) => {
        this.errors = data.message;
        throw data;
      })
      .finally(() => {
        this.inProgress = false;
      });
  }

  @action
  getAfterArt() {
    this.inProgress = true;
    this.listAfterArt = [];
    return API.Msiha.afterArt()
      .then(rsp => {
        this.listAfterArt = rsp.data;
      })
      .catch(({ data }) => {
        this.errors = data.message;
        throw data;
      })
      .finally(() => {
        this.inProgress = false;
      });
  }

  // @action
  // setDataPastienMasuk(listDataPastien: any) {
  //   this.listDataPastien = listDataPastien;
  // }
}

export default new MsihaStore();
