import { action, observable, reaction } from "mobx";
import API from "utils/API";

export interface IUserStore {
  inProgress: boolean;
  errors: any;
  user: any;
  setUser(): void;
  setError(): void;
  getUser(): void;
}

class userStore {
  @observable
  inProgress = false;

  @observable
  isError = false;

  @observable
  errors = undefined;

  @observable
  errorMessage = "";

  @observable
  user = JSON.parse(window.localStorage.getItem("user") || "{}");

  constructor() {
    reaction(
      () => this.user,
      user => {
        if (user) {
          window.localStorage.setItem("user", JSON.stringify(user));
        } else {
          window.localStorage.removeItem("user");
        }
      }
    );
  }

  @action
  setUser(user) {
    this.user = user;
  }

  @action
  setError(isError, message = "") {
    this.isError = isError;
    this.errorMessage = message;
  }

  @action
  getUser() {
    this.inProgress = true;
    return API.User.get().then(rsp => {
      this.user = rsp.data;
    });
  }
}

export default new userStore();
