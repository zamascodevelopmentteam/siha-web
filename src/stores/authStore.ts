import { action, observable } from "mobx";
import API from "utils/API";
import commonStore from "./commonStore";
import userStore from "./userStore";
import { ROLE } from "constant";

export interface IAuthStore {
  inProgress: boolean;
  errors: any;
  loginData: {
    nik: string;
    password: string;
  };
  updateLoginData(key: string, value: any): void;
  resetLoginData(): void;
  login(): Promise<any>;
  logout(): void;
}

export class AuthStore implements IAuthStore {
  @observable
  inProgress = false;

  @observable
  errors = undefined;

  @observable
  loginData = {
    nik: "",
    password: ""
  };

  @action
  resetLoginData() {
    this.loginData = {
      nik: "",
      password: ""
    };
    this.errors = undefined;
    this.inProgress = false;
  }

  @action
  updateLoginData = (key: string, value: any) => {
    this.loginData[key] = value;
  };

  @action
  login() {
    this.inProgress = true;
    return API.Auth.login(this.loginData)
      .then(rsp => {
        if (Object.values(ROLE).includes(rsp.data.user.role)) {
          commonStore.setToken(rsp.data.token);
          userStore.setUser(rsp.data.user);
          return true;
        }
        return false;
      })
      .catch(({ data }) => {
        this.errors = data.message;
        throw data;
      })
      .finally(() => {
        this.inProgress = false;
      });
  }

  @action
  logout() {
    this.resetLoginData();
    userStore.setUser({});
    commonStore.setToken(undefined);
    return;
  }
}

export default new AuthStore();
