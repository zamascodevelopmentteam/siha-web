import { action, toJS, observable, reaction } from "mobx";
import API from "utils/API";

export interface ICommonStore {
  inProgress: boolean;
  isDrawerOpen: boolean;
  errors: any;
  token: any;
  visits: any;
  visitCount: any;
  breadcrumbs: string;
  toggleDrawer(condition: boolean): void;
  setToken(token: any): void;
  setError(): void;
  getToken(): void;
  getAllVisit(): Promise<any>;
  getCountVisit(): Promise<any>;
  setBreadcrumbs(breadcrumbs: string): void;
  isLogin(): boolean;
}

class commonStore {
  @observable
  inProgress = false;

  @observable
  isError = false;

  @observable
  errors = undefined;

  @observable
  errorMessage = "";

  @observable
  breadcrumbs = "";

  @observable
  isDrawerOpen = false;

  @observable
  visitCount = 0;

  @observable
  token: any = window.localStorage.getItem("token");

  @observable
  visits = [];

  constructor() {
    reaction(
      () => this.token,
      token => {
        if (token) {
          window.localStorage.setItem("token", token);
        } else {
          window.localStorage.removeItem("token");
        }
      }
    );
  }

  @action
  toggleDrawer = (condition: boolean) => {
    this.isDrawerOpen = condition;
  };

  consoleLog(value, name = "Log mobx data:") {
    console.log(name, toJS(value));
  }

  @action
  setToken = (token: any) => {
    this.token = token;
  };

  @action
  getAllVisit() {
    this.inProgress = true;
    return API.Visit.getAll()
      .then(rsp => {
        this.visits = rsp.data;
        return false;
      })
      .catch(({ data }) => {
        this.errors = data.message;
        throw data;
      })
      .finally(() => {
        this.inProgress = false;
      });
  }

  @action
  getCountVisit() {
    this.inProgress = true;
    return API.Visit.getCount()
      .then(rsp => {
        this.visitCount = rsp.data;
        return false;
      })
      .catch(({ data }) => {
        this.errors = typeof data.message === "undefined" ? data : data.message;
        throw data;
      })
      .finally(() => {
        this.inProgress = false;
      });
  }

  @action
  setBreadcrumbs(breadcrumbs: string) {
    this.breadcrumbs = breadcrumbs;
  }

  @action
  setError(isError, message = "") {
    this.isError = isError;
    this.errorMessage = message;
  }

  getToken() {
    return this.token;
  }

  isLogin = () => {
    if (typeof this.token === "undefined") {
      return false;
    }
    if (this.token) {
      return true;
    } else {
      return false;
    }
  };
}

export default new commonStore();
