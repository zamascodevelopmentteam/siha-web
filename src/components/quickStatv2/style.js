import styled, { css } from "styled-components";

export const WelcomeWrapper = styled.div`
  border-radius: 12px;
  background-image: linear-gradient(to right, #005ecd, #1cc1c9),
    linear-gradient(to right, #1cc1c9, #1cc1c9);
`;

export const IconWrapper = styled.div`
  width: 36px;
  height: 36px;
  border-radius: 8px;
  font-size: 16px;
  display: flex;
  justify-content: center;
  align-items: center;
  color: white;
  background-color: #1cc1c9;
`;

export const StatWrapper = styled.div`
  ${props =>
    props.inverted &&
    css`
      border-radius: 12px;
      box-shadow: 0 2px 6px 0 rgba(0, 0, 0, 0.1);
      background-color: #06d2ae;
      padding: 10px;
      height: 100%;
    `}
`;
