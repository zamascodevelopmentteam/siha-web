import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUser } from "@fortawesome/free-solid-svg-icons";
import { Typography } from "components";
import { IconWrapper, StatWrapper } from "./style";

const QuickStatV2 = ({ count, title, description, inverted }) => {
  const fontColor = inverted ? "white" : "black";
  return (
    <StatWrapper inverted={inverted}>
      <div className="d-flex mb-3">
        <IconWrapper className="mr-2">
          <FontAwesomeIcon icon={faUser} />
        </IconWrapper>
        <Typography fontSize="24px" fontWeight="bold" color={fontColor}>
          {count}
        </Typography>
      </div>
      <Typography
        fontSize="14px"
        fontWeight="bold"
        color={fontColor}
        className="d-block mb-3"
      >
        {title}
      </Typography>
      <Typography
        fontSize="12px"
        color={inverted ? "white" : "secondary"}
        className="d-block"
      >
        {description}
      </Typography>
    </StatWrapper>
  );
};

QuickStatV2.defaultProps = {
  inverted: false
};

export default QuickStatV2;
