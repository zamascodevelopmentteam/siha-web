import React from "react";
import { AvatarWrapper } from "./style";

const Avatar = ({ src, size, ...props }) => {
  return <AvatarWrapper src={src} size={size} {...props} />;
};

Avatar.defaultProps = {
  size: "50px"
};

export default Avatar;
