import styled from "styled-components";

export const AvatarWrapper = styled.img`
  object-fit: cover;
  width: ${props => props.size || "50px"};
  height: ${props => props.size || "50px"};
  border-radius: 50%;
`;
