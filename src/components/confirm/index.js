import { Modal } from "antd";

const { confirm } = Modal;

export const deleteConfirm = ({
  type,
  name,
  onDelete,
  message,
  typeConfirm = "danger"
}) => {
  const title = message || `Anda yakin akan menghapus ${type} ${name || ""} ?`;
  confirm({
    title,
    okText: "Ya",
    okType: typeConfirm,
    cancelText: "Batal",
    onOk() {
      onDelete();
    }
  });
};
