import React, { useState, useEffect } from "react";
import API from "utils/API";
import axios from "axios";
import { Select, Spin } from "antd";
// import { formatNumber } from "utils";

const { Option } = Select;

const ChooseStock = ({
  destType,
  destID,
  destLogisticRole,
  showStock,
  ...props
}) => {
  let source = axios.CancelToken.source();
  const [items, setItems] = useState([]);
  const [isLoading, setLoading] = useState(false);

  const _loadData = () => {
    // if (destLogisticRole) {
    setLoading(true);
    API.Logistic.listStock(
      source.token,
      destType || "",
      destID || "",
      destLogisticRole || ""
    )
      .then(rsp => {
        setItems(rsp.data || []);
      })
      .catch(e => {
        console.error("e: ", e);
      })
      .finally(() => {
        setLoading(false);
      });
    // }
  };

  useEffect(() => {
    _loadData();

    return () => {
      source.cancel("request canceled");
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [destType, destID]);

  return (
    <Select
      showSearch
      optionFilterProp="children"
      notFoundContent={isLoading ? <Spin size="small" /> : null}
      {...props}
    >
      <Option disabled value={null}>
        Pilih Barang
      </Option>
      {items.map(item => {
        if (props.type === 'ARV') {
          if (item.medicine_type === "ARV") {
            return (
              <Option key={item.id} value={item.id}>
                {item.codename}
              </Option>
            )
          }
        } else if (props.type === 'NON_ARV') {
          if (item.medicine_type === "NON_ARV") {
            return (
              <Option key={item.id} value={item.id}>
                {item.codename}
              </Option>
            )
          }
        } else {
          return (
            <Option key={item.id} value={item.id}>
              {item.codename}
            </Option>
          )
        }
      })}
    </Select>
  );
};

export default ChooseStock;
