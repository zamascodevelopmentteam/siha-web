import styled from "styled-components";

export const Label = styled.label`
  display: block;
  font-size: 12px;
  font-weight: ${props => (props.isInfo ? "normal" : "600")};
`;

export const LabelInline = styled.label`
  font-size: 12px;
  font-weight: ${props => (props.isInfo ? "normal" : "600")};
`;
