import React, { useState, useEffect } from "react";
import API from "utils/API";
import axios from "axios";
import { Select, Spin } from "antd";
// import { formatNumber } from "utils";

const { Option } = Select;

const ChooseStock = ({ showStock, onChange, ...props }) => {
  let source = axios.CancelToken.source();
  const [items, setItems] = useState([]);
  const [isLoading, setLoading] = useState(false);
  const [mapData, setMapData] = useState([]);
  const [keyword, setKeyword] = useState("");

  const _quickSearch = value => {
    const keyword = value.trim();
    setKeyword(keyword);
  };

  const _loadData = () => {
    setLoading(true);
    API.Logistic.listAllStock(source.token, 0, 100, keyword)
      .then(rsp => {
        const constructed = _constructData(rsp.data);
        setMapData(constructed.mapData);
        setItems(constructed.data || []);
      })
      .catch(e => {
        console.error("e: ", e);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const _handleChange = value => {
    onChange(mapData[value]);
  };

  useEffect(() => {
    _loadData();

    return () => {
      source.cancel("request canceled");
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [keyword]);

  return (
    <Select
      showSearch
      optionFilterProp="children"
      notFoundContent={isLoading ? <Spin size="small" /> : null}
      onChange={_handleChange}
      onSearch={_quickSearch}
      {...props}
    >
      <Option disabled value={null}>
        Pilih Barang
      </Option>
      {
        items.map(item => {
          if (props.pharma) {
            if (["IO", "IMS", "IO (TB)", "HIV", "Alkes"].includes(item.category)) {
              return (
                <Option key={item.id} value={item.id}>
                  {item.name}
                </Option>
              )
            }
          } else if (props.lab) {
            if (["Reagen IMS", "Reagen HIV"].includes(item.category)) {
              return (
                <Option key={item.id} value={item.id}>
                  {item.name}
                </Option>
              )
            }
          } else {
            return (
              <Option key={item.id} value={item.id}>
                {item.name}
              </Option>
            )
          }
        })
      }
    </Select>
  );
};

export default ChooseStock;

const _constructData = data => {
  let newData = [];
  let mapData = {};
  for (let x = 0; x < data.length; x++) {
    let id, packageQuantity, packageUnitType, category;
    id = data[x].id;
    packageQuantity = data[x].packageQuantity;
    packageUnitType = data[x].packageUnitType;
    category = data[x].brand.medicine.category;
    newData.push({
      id,
      packageQuantity,
      packageUnitType,
      name: `${data[x].batchCode} - ${data[x].brand.medicine.codeName}`,
      category: category
    });

    mapData[id] = {
      id,
      packageQuantity,
      packageUnitType,
      name: `${data[x].batchCode} - ${data[x].brand.medicine.codeName}`
    };
  }
  return { data: newData, mapData };
};
