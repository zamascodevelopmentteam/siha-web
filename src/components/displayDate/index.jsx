import React from "react";
import moment from "moment";
import "moment/locale/id";

moment.locale("id");

const DisplayDate = ({ date, format }) => {
  return <React.Fragment>{moment(date).format(format)}</React.Fragment>;
};

DisplayDate.defaultProps = {
  format: "D MMM YYYY"
};
export default DisplayDate;
