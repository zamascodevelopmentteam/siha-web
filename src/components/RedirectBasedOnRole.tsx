import * as React from "react";
import { IUserStore } from "stores/userStore";
import { inject, observer } from "mobx-react";
import { c, ROLE } from "constant";
import { Redirect } from "react-router-dom";

interface ComponentProps {
  userStore: IUserStore;
}

const RedirectBasedOnRole = ({ userStore }: ComponentProps) => {
  const { user } = userStore;
  switch (user.role) {
    case ROLE.LAB_STAFF:
    case ROLE.RR_STAFF:
    case ROLE.DOCTOR:
    case ROLE.PHARMA_STAFF:
      return <Redirect to="/intra/dashboard" />;
    case ROLE.SUDIN_STAFF:
    case ROLE.PROVINCE_STAFF:
      return <Redirect to="/pharma/stock" />;
    case ROLE.MINISTRY_STAFF:
      return <Redirect to="/master/medicine" />;
    default:
      return <Redirect to="/dashboard" />;
  }
};

export default inject(c.STORES.USER)(observer(RedirectBasedOnRole));
