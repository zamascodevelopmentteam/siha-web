import * as React from "react";
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  Label,
  Button, Card, CardHeader, CardBody
} from "reactstrap";
import { c, ROLE } from "../constant";
import { observer, inject } from "mobx-react";
import { ICommonStore } from "../stores/commonStore";
import { IAuthStore } from "../stores/authStore";
import { IUserStore } from "../stores/userStore";
// import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
// import { faUserCircle } from "@fortawesome/free-solid-svg-icons";

import User from "../images/icons/user.png";

interface NavProps { }

interface InjectedProps extends NavProps {
  commonStore: ICommonStore;
  authStore: IAuthStore;
  userStore: IUserStore;
}

interface ITopBarState {
  isOpen: boolean;
  logoutForm: boolean;
}

@inject(c.STORES.AUTH)
@inject(c.STORES.COMMON)
@inject(c.STORES.USER)
@observer
class TopBar extends React.Component<NavProps, ITopBarState> {
  state = {
    isOpen: false,
    logoutForm: false
  };

  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.displayHandler = this.displayHandler.bind(this);
  }

  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  displayHandler() {
    this.setState({
      logoutForm: !this.state.logoutForm
    });
  }

  get injected() {
    return this.props as InjectedProps;
  }

  render() {
    const { breadcrumbs } = this.injected.commonStore;
    const { user } = this.injected.userStore;
    const isDesktopShow = "d-none d-sm-block";
    const isPhoneShow = "d-block d-sm-none";
    const { name, role, sudinKabKota, province } = user;

    const lg = this.state.logoutForm ? "block" : "none";

    let dispalyRole = [ROLE.LAB_STAFF, ROLE.PHARMA_STAFF, ROLE.RR_STAFF, ROLE.SUDIN_STAFF].includes(role) ?
      (sudinKabKota ? sudinKabKota.name : role) :
      (role === ROLE.PROVINCE_STAFF ?
        (province ? province.name : role) :
        role
      );

    return (
      <React.Fragment>
        <div>
          <Navbar light expand="md" className="navbar pl-4 pr-4">
            <NavbarBrand href="/" style={{ fontSize: "24px" }}>
              {breadcrumbs}
            </NavbarBrand>
            <NavbarToggler onClick={this.toggle} />
            <Collapse isOpen={this.state.isOpen} navbar>
              <Nav className="ml-auto" navbar>
                <NavItem
                  className={isDesktopShow + " p-0 pl-1 "}
                  style={{ minWidth: "auto" }}
                >
                  <NavLink
                    disabled
                    className={`text-primary text-truncate pl-2 p-0 text-${user.role ||
                      ""}`}
                    style={{ maxWidth: "100%", textAlign: "right" }}
                  >
                    {/* <div className="row pl-2 p-0">
                      <img
                        className="col-3 p-1 img-avatar"
                        src={avatar}
                        style={{ width: "36px" }}
                        alt="avatar"
                      /> */}
                    <div className="col-12 p-1">
                      <Label className="m-0">
                        {name}
                        <br />
                        <span className="m-0" style={{ fontSize: "10px" }}>
                          ({dispalyRole})
                        </span>
                      </Label>
                    </div>
                    {/* </div> */}
                  </NavLink>
                </NavItem>
                <div
                  className={isDesktopShow + " mt-2"}
                  style={{
                    border: "1px solid #bdbdbd",
                    height: "20px",
                    position: "relative",
                    top: "8px"
                  }}
                />
                <NavItem className={isDesktopShow + " pt-2"}>
                  {/* <FontAwesomeIcon icon={faUserCircle} onClick={this.displayHandler} style={{fontSize: '40px'}} className={`text-${user.role}`} /> */}
                  <img src={User} height={"40"} onClick={this.displayHandler} alt="user" />
                  {/* <Button
                    color="secondary"
                    onClick={() => this.injected.authStore.logout()}
                    className={`hs-btn-outline ${user.role}`}
                  >
                    <span className="d-md-inline-block">Keluar</span>
                  </Button> */}
                </NavItem>
                <div
                  className={isPhoneShow}
                  style={{
                    borderTop: "1px solid #bdbdbd"
                  }}
                />
                <NavItem className={`${isPhoneShow}`}>
                  <NavLink
                    onClick={() => this.injected.authStore.logout()}
                    className={`hs-btn-outline ${user.role}`}
                  >
                    Keluar
                  </NavLink>
                </NavItem>
              </Nav>
            </Collapse>
          </Navbar>
        </div>
        <Card style={{ zIndex: 1, position: "absolute", right: 40, top: 65, width: '300px', display: lg }} onMouseLeave={this.displayHandler}>
          <CardHeader style={{ height: '80px' }}>
            <div className={"d-flex align-items-center"}>
              <img src={User} height={"40"} className={'mr-2'} alt="user" />
              {/* <FontAwesomeIcon icon={faUserCircle} onClick={this.displayHandler} style={{fontSize: '40px'}} className={`mr-2 text-${user.role}`} /> */}
              <Label className={`text-${user.role} m-0`}>
                {name}
                <br />
                <span className="m-0" style={{ fontSize: "10px" }}>
                  ({dispalyRole})
                </span>
              </Label>
            </div>
          </CardHeader>
          <CardBody>
            <Button
              color="secondary"
              onClick={() => this.injected.authStore.logout()}
              className={`hs-btn-outline ${user.role}`}
              block
            >
              <span className="d-md-inline-block">Keluar</span>
            </Button>
          </CardBody>
        </Card>
      </React.Fragment>
    );
  }
}

export default TopBar;
