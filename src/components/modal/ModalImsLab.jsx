import React, { useState, useEffect } from "react";
import { ENUM, ROLE } from "constant";
import { Modal, Button, Select, Input, Col, Checkbox, Row, Radio, InputNumber } from "antd";
import { Label } from "./style";
import { ChooseReagen, Typography } from "components";

const { Option } = Select;
const {
  DIAGNOSIS_SYNDROME_LAKI,
  DIAGNOSIS_SYNDROME_PEREMPUAN,
  DIAGNOSIS_KLINIS_LAKI,
  DIAGNOSIS_KLINIS_PEREMPUAN,
  DIAGNOSIS_LAB_PEREMPUAN,
  DIAGNOSIS_LAB_LAKI,
} = ENUM;

const Handler = ({
  onOk,
  onCancel,
  isLoading,
  role,
  onChange,
  onChangeImsLabInnerChild,
  onChangeImsDiagnosis,
  data,
  dataDiagnosis,
  dataMedicine,
  reagent,
  gender,
  medicineList,
  onAddMedicines,
  onRemoveMedicines,
  onChangeMedicine,
  newData,
  newOnChange,
  resetPemeriksaan
}) => {
  const [diagnosisSyndrome, setDiagnosisSyndrome] = useState([])
  const [diagnosisKlinis, setDiagnosisKlinis] = useState([])
  const [diagnosisLab, setDiagnosisLab] = useState([])

  let typeDiagnosisSyndromeIms = gender === "PEREMPUAN" ? DIAGNOSIS_SYNDROME_PEREMPUAN : DIAGNOSIS_SYNDROME_LAKI
  let typeDiagnosisKlinisIms = gender === "PEREMPUAN" ? DIAGNOSIS_KLINIS_PEREMPUAN : DIAGNOSIS_KLINIS_LAKI
  let typeDiagnosisLabIms = gender === "PEREMPUAN" ? DIAGNOSIS_LAB_PEREMPUAN : DIAGNOSIS_LAB_LAKI
  let disabled = !data.is_tested || role === ROLE.DOCTOR

  // let ds = dataDiagnosis;
  useEffect(() => {
    if (dataDiagnosis) {
      let arrDiagnosisSyndrome = []
      let arrDiagnosisKlinis = []
      let arrDiagnosisLab = []

      Object.keys(dataDiagnosis.syndrome).forEach(v => {
        if (dataDiagnosis.syndrome[v]) {
          arrDiagnosisSyndrome.push(v)
        }
      })

      Object.keys(dataDiagnosis.klinis).forEach(v => {
        if (dataDiagnosis.klinis[v]) {
          arrDiagnosisKlinis.push(v)
        }
      })

      Object.keys(dataDiagnosis.lab).forEach(v => {
        if (dataDiagnosis.lab[v]) {
          arrDiagnosisLab.push(v)
        }
      })

      setDiagnosisSyndrome(arrDiagnosisSyndrome)
      setDiagnosisKlinis(arrDiagnosisKlinis)
      setDiagnosisLab(arrDiagnosisLab)
    }
  }, [])

  const _handleChange = (name, value) => {
    onChange(name, value);
  };

  const _handleChangeImsLabInnerChild = (parentName, name, value) => {
    onChangeImsLabInnerChild(parentName, name, value)
  }

  useEffect(() => {
    if (dataDiagnosis)
      onChangeImsDiagnosis('syndrome', diagnosisSyndrome)
  }, [diagnosisSyndrome])

  useEffect(() => {
    if (dataDiagnosis)
      onChangeImsDiagnosis('klinis', diagnosisKlinis)
  }, [diagnosisKlinis])

  useEffect(() => {
    if (dataDiagnosis)
      onChangeImsDiagnosis('lab', diagnosisLab)
  }, [diagnosisLab])

  const _handleChangeImsDiagnosis = (name, value) => {
    if (name === 'syndrome')
      setDiagnosisSyndrome(value)
    else if (name === 'klinis')
      setDiagnosisKlinis(value)
    else if (name === 'lab')
      setDiagnosisLab(value)
  }

  const _handleSubmit = e => {
    e.preventDefault();
    onOk()
  };

  return (
    <Modal
      title="Tes Lab IMS"
      visible
      style={{ top: 30 }}
      onOk={onOk}
      maskClosable={false}
      onCancel={onCancel}
      footer={null}
    >
      <form onSubmit={_handleSubmit}>
        <Typography fontSize="14px" fontWeight="bold" className="typography-display">
          Dilakukan Tes Lab
        </Typography>
        <Radio.Group
          className="group-margin"
          value={data.is_tested}
          onChange={e => _handleChange('is_tested', e.target.value)}
          disabled={role === ROLE.DOCTOR}>
          <Radio value={true}>Ya</Radio>
          <Radio value={false}>Tidak</Radio>
        </Radio.Group>
        {/* ---------------------- */}
        <Typography fontSize="14px" fontWeight="bold" className="typography-display">
          Jenis Kelamin
        </Typography>
        <Radio.Group
          className="group-margin"
          value={gender}
          // onChange={e => setGender(e.target.value)}
          disabled={role === ROLE.DOCTOR}>
          <Radio value={"LAKI-LAKI"}>Laki-laki</Radio>
          <Radio value={"PEREMPUAN"}>Perempuan</Radio>
        </Radio.Group>
        {/* ---------------------- */}
        <hr />
        {data.is_tested && (
          <>
            <Row>
              <Col span="12">
                <Typography fontSize="14px" fontWeight="bold" className="d-flex">
                  <h5><b>Pemeriksaan</b></h5>
                  <Button className="ml-3" type="danger" size="small" onClick={resetPemeriksaan} disabled={disabled}>Reset</Button>
                </Typography>
              </Col>
            </Row>
            <Row>
              <Col span="12">
                {gender === "PEREMPUAN" && (
                  <React.Fragment>
                    {/*  */}
                    <Typography fontSize="14px" fontWeight="bold" className="typography-display">
                      PMN Serviks
                    </Typography>
                    <Radio.Group
                      className="group-margin"
                      value={data.pmnUretraServiks}
                      onChange={e => _handleChange('pmnUretraServiks', e.target.value)}
                      disabled={disabled}>
                      <Radio value={true} >Positif</Radio>
                      <Radio value={false}>Negatif</Radio>
                    </Radio.Group>
                    {/* new */}
                    <Typography fontSize="14px" fontWeight="bold" className="typography-display">
                      PMN Vagina
                    </Typography>
                    <Radio.Group
                      className="group-margin"
                      value={data.pmnVagina}
                      onChange={e => _handleChange('pmnVagina', e.target.value)}
                      disabled={disabled}>
                      <Radio value={true} >Positif</Radio>
                      <Radio value={false}>Negatif</Radio>
                    </Radio.Group>
                  </React.Fragment>
                )}
                {/* new */}
                <Typography fontSize="14px" fontWeight="bold" className="typography-display">
                  PMN Uretra
                </Typography>
                <Radio.Group
                  className="group-margin"
                  value={data.pmnUretra}
                  onChange={e => _handleChange('pmnUretra', e.target.value)}
                  disabled={disabled}>
                  <Radio value={true} >Positif</Radio>
                  <Radio value={false}>Negatif</Radio>
                </Radio.Group>
                {/*  */}
                <Typography fontSize="14px" fontWeight="bold" className="typography-display">
                  PMN Anus
                </Typography>
                <Radio.Group
                  className="group-margin"
                  value={data.pmnAnus}
                  onChange={e => _handleChange('pmnAnus', e.target.value)}
                  disabled={disabled}>
                  <Radio value={true}>Positif</Radio>
                  <Radio value={false}>Negatif</Radio>
                </Radio.Group>
                {/* new */}
                <Typography fontSize="14px" fontWeight="bold" className="typography-display">
                  PMN Mata
                </Typography>
                <Radio.Group
                  className="group-margin"
                  value={data.pmnMata}
                  onChange={e => _handleChange('pmnMata', e.target.value)}
                  disabled={disabled}>
                  <Radio value={true}>Positif</Radio>
                  <Radio value={false}>Negatif</Radio>
                </Radio.Group>
                {gender === "PEREMPUAN" && (
                  <React.Fragment>
                    {/* new */}
                    <Typography fontSize="14px" fontWeight="bold" className="typography-display">
                      Trichomonas vag
                    </Typography>
                    <Radio.Group
                      className="group-margin"
                      value={data.trichomonasVag}
                      onChange={e => _handleChange('trichomonasVag', e.target.value)}
                      disabled={disabled}>
                      <Radio value={true}>Positif</Radio>
                      <Radio value={false}>Negatif</Radio>
                    </Radio.Group>
                  </React.Fragment>
                )}
                {/*  */}
                <Typography fontSize="14px" fontWeight="bold" className="typography-display">
                  Clue Cells
                </Typography>
                <Radio.Group
                  className="group-margin"
                  value={data.clueCells}
                  onChange={e => _handleChange('clueCells', e.target.value)}
                  disabled={disabled}>
                  <Radio value={true}>Positif</Radio>
                  <Radio value={false}>Negatif</Radio>
                </Radio.Group>
              </Col>
              <Col span="12">
                {gender === "PEREMPUAN" && (
                  <React.Fragment>
                    {/*  */}
                    <Typography fontSize="14px" fontWeight="bold" className="typography-display">
                      Diplokakus Intrasel Serviks
                    </Typography>
                    <Radio.Group
                      className="group-margin"
                      value={data.diplokokusIntraselUretraServiks}
                      onChange={e => _handleChange('diplokokusIntraselUretraServiks', e.target.value)}
                      disabled={disabled}>
                      <Radio value={true}>Positif</Radio>
                      <Radio value={false}>Negatif</Radio>
                    </Radio.Group>
                    {/* new */}
                    <Typography fontSize="14px" fontWeight="bold" className="typography-display">
                      Diplokokus intrasel Vagina
                    </Typography>
                    <Radio.Group
                      className="group-margin"
                      value={data.diplokokusIntraselVagina}
                      onChange={e => _handleChange('diplokokusIntraselVagina', e.target.value)}
                      disabled={disabled}>
                      <Radio value={true}>Positif</Radio>
                      <Radio value={false}>Negatif</Radio>
                    </Radio.Group>
                  </React.Fragment>
                )}
                {/* new */}
                <Typography fontSize="14px" fontWeight="bold" className="typography-display">
                  Diplokokus intrasel Uretra
                </Typography>
                <Radio.Group
                  className="group-margin"
                  value={data.diplokokusIntraselUretra}
                  onChange={e => _handleChange('diplokokusIntraselUretra', e.target.value)}
                  disabled={disabled}>
                  <Radio value={true}>Positif</Radio>
                  <Radio value={false}>Negatif</Radio>
                </Radio.Group>
                {/*  */}
                <Typography fontSize="14px" fontWeight="bold" className="typography-display">
                  Diplokokus Intrasel Anus
                </Typography>
                <Radio.Group
                  className="group-margin"
                  value={data.diplokokusIntraselAnus}
                  onChange={e => _handleChange('diplokokusIntraselAnus', e.target.value)}
                  disabled={disabled}>
                  <Radio value={true}>Positif</Radio>
                  <Radio value={false}>Negatif</Radio>
                </Radio.Group>
                {/* new */}
                <Typography fontSize="14px" fontWeight="bold" className="typography-display">
                  Diplokokus Intrasel Mata
                </Typography>
                <Radio.Group
                  className="group-margin"
                  value={data.diplokokusIntraselMata}
                  onChange={e => _handleChange('diplokokusIntraselMata', e.target.value)}
                  disabled={disabled}>
                  <Radio value={true}>Positif</Radio>
                  <Radio value={false}>Negatif</Radio>
                </Radio.Group>
                {/* new */}
                <Typography fontSize="14px" fontWeight="bold" className="typography-display">
                  Pseudohifa/blastospora
                </Typography>
                <Radio.Group
                  className="group-margin"
                  value={data.pseudohifaBlastospora}
                  onChange={e => _handleChange('pseudohifaBlastospora', e.target.value)}
                  disabled={disabled}>
                  <Radio value={true}>Positif</Radio>
                  <Radio value={false}>Negatif</Radio>
                </Radio.Group>
                {/* <Typography fontSize="14px" fontWeight="bold" className="typography-display">
                  T Vaginalis
                </Typography>
                <Radio.Group
                  className="group-margin"
                  value={data.tVaginalis}
                  onChange={e => _handleChange('tVaginalis', e.target.value)}
                  disabled={disabled}>
                  <Radio value={true}>Positif</Radio>
                  <Radio value={false}>Negatif</Radio>
                </Radio.Group>

                <Typography fontSize="14px" fontWeight="bold" className="typography-display">
                  Kandida
                </Typography>
                <Radio.Group
                  className="group-margin"
                  value={data.kandida}
                  onChange={e => _handleChange('kandida', e.target.value)}
                  disabled={disabled}>
                  <Radio value={true}>Positif</Radio>
                  <Radio value={false}>Negatif</Radio>
                </Radio.Group>

                <Typography fontSize="14px" fontWeight="bold" className="typography-display">
                  pH
                </Typography>
                <Radio.Group
                  className="group-margin"
                  value={data.ph}
                  onChange={e => _handleChange('ph', e.target.value)}
                  disabled={disabled}>
                  <Radio value={true}>Positif</Radio>
                  <Radio value={false}>Negatif</Radio>
                </Radio.Group>

                <Typography fontSize="14px" fontWeight="bold" className="typography-display">
                  Sniff Test
                </Typography>
                <Radio.Group
                  className="group-margin"
                  value={data.sniffTest}
                  onChange={e => _handleChange('sniffTest', e.target.value)}
                  disabled={disabled}>
                  <Radio value={true}>Positif</Radio>
                  <Radio value={false}>Negatif</Radio>
                </Radio.Group> */}
              </Col>
            </Row>

            <hr />

            <Typography fontSize="14px" fontWeight="bold">
              Pemeriksaan Sifilis
            </Typography>
            <Select
              placeholder="Pilih Pemeriksaan"
              style={{ width: "100%" }}
              className="mb-3"
              name="pemeriksaan"
              value={data.pemeriksaan ? data.pemeriksaan : undefined}
              disabled={disabled}
              onChange={v => _handleChange('pemeriksaan', v)} >
              <Option value={null} disabled>Pilih Pemeriksaam</Option>
              <Option value={"RAPID_SIFILIS"}>Rapid Sifilis</Option>
              <Option value={"TPHA"}>TPHA</Option>
            </Select>
            {data.pemeriksaan === "RAPID_SIFILIS" && (
              <React.Fragment>
                <Typography fontSize="16px" fontWeight="bold">
                  Tes Sifilis
                </Typography>

                <Label>Pilih Reagen</Label>
                {/* <Select
                  placeholder="Pilih Reagen"
                  style={{ width: "100%" }}
                  className="mb-3"
                  name="reagent"
                  value={data.rprVdrl.reagent ? data.rprVdrl.reagent : undefined}
                  onChange={v => _handleChangeImsLabInnerChild('rprVdrl', 'reagent', v)}
                  disabled={disabled}>
                  <Option value={null}>Pilih Reagen</Option>
                  {reagent.map(v => (
                    <Option key={v.id} value={v.id}>
                      {v.name}
                    </Option>
                  ))}
                </Select> */}
                <ChooseReagen
                  placeholder="Pilih Reagen"
                  style={{ width: "100%" }}
                  className="mb-3"
                  value={data.rprVdrl.reagent ? data.rprVdrl.reagent : undefined}
                  name="reagent"
                  onChange={v => _handleChangeImsLabInnerChild('rprVdrl', 'reagent', v)}
                  isNotNol
                  required
                  reagenIms
                />

                <Label>Jumlah Pengguna</Label>
                <Input
                  placeholder="Tulis Jumlah Penggunaan"
                  style={{ width: "100%" }}
                  className="mb-3"
                  value={data.rprVdrl.totalReagentUsed}
                  onChange={e => _handleChangeImsLabInnerChild("rprVdrl", 'totalReagentUsed', e.target.value)}
                  disabled={disabled} />

                <Radio.Group
                  className="group-margin"
                  value={data.rprVdrl.result}
                  onChange={e => _handleChangeImsLabInnerChild('rprVdrl', 'result', e.target.value)}
                  disabled={disabled}>
                  <Radio value="REAKTIF">Reaktif</Radio>
                  <Radio value="NON_REAKTIF">Non Reaktif</Radio>
                  <Radio value="INVALID">Invalid</Radio>
                </Radio.Group>

                <br />
                <br />

                <Typography fontSize="16px" fontWeight="bold" className="typography-display">
                  RPR/VDRL
                </Typography>
                <Radio.Group
                  className="group-margin"
                  value={data.realRprVdrl.status ? data.realRprVdrl.status : undefined}
                  onChange={e => _handleChangeImsLabInnerChild("realRprVdrl", 'status', e.target.value)}
                  disabled={disabled}
                >
                  <Radio value={true}>Ya</Radio>
                  <Radio value={false}>Tidak</Radio>
                </Radio.Group>

                {data.realRprVdrl.status && (
                  <React.Fragment>
                    <Label>Pilih Reagen</Label>
                    <Select
                      placeholder="Pilih Reagen"
                      style={{ width: "100%" }}
                      className="mb-3"
                      name="reagent"
                      value={data.realRprVdrl.reagent ? data.realRprVdrl.reagent : undefined}
                      onChange={v => _handleChangeImsLabInnerChild('realRprVdrl', 'reagent', v)}
                      disabled={disabled}>
                      <Option value={null}>Pilih Reagen</Option>
                      {reagent.map(v => (
                        <Option key={v.id} value={v.id}>
                          {v.name}
                        </Option>
                      ))}
                    </Select>

                    <Label>Jumlah Pengguna</Label>
                    <Input
                      placeholder="Tulis Jumlah Penggunaan"
                      style={{ width: "100%" }}
                      className="mb-3"
                      value={data.realRprVdrl.totalReagentUsed}
                      onChange={e => _handleChangeImsLabInnerChild("realRprVdrl", 'totalReagentUsed', e.target.value)}
                      disabled={disabled} />

                    <Typography fontSize="14px" fontWeight="bold">
                      RPR/VDRL TITER
                    </Typography>
                    <Radio.Group
                      className="group-margin"
                      value={data.rprVdrlTiter}
                      onChange={e => _handleChange('rprVdrlTiter', e.target.value)}
                      disabled={disabled}>
                      <table>
                        <tr>
                          <td><Radio value="1/1">1/1</Radio></td>
                          <td><Radio value="1/2">1/2</Radio></td>
                          <td><Radio value="1/4">1/4</Radio></td>
                          <td><Radio value="1/8">1/8</Radio></td>
                          <td><Radio value="1/16">1/16</Radio></td>
                        </tr>
                        <tr>
                          <td><Radio value="1/32">1/32</Radio></td>
                          <td><Radio value="1/64">1/64</Radio></td>
                          <td><Radio value="1/128">1/128</Radio></td>
                          <td><Radio value="1/256">1/256</Radio></td>
                          <td><Radio value="1/512">1/512</Radio></td>
                        </tr>
                      </table>
                    </Radio.Group>
                  </React.Fragment>
                )}
              </React.Fragment>
            )}

            {data.pemeriksaan === "TPHA" && (
              <React.Fragment>
                <Typography fontSize="14px" fontWeight="bold">
                  TPHA/TPPA (TP Rapid)
                </Typography>

                <br />

                <Radio.Group
                  className="group-margin"
                  value={data.tphaTppa.result}
                  onChange={e => _handleChangeImsLabInnerChild('tphaTppa', 'result', e.target.value)}
                  disabled={disabled}>
                  <Radio value="REAKTIF">Reaktif</Radio>
                  <Radio value="NON_REAKTIF">Non Reaktif</Radio>
                  <Radio value="INVALID">Invalid</Radio>
                </Radio.Group>

                {/* <Label>Pilih Reagen</Label>
                <Select
                  placeholder="Pilih Reagen"
                  style={{ width: "100%" }}
                  className="mb-3"
                  value={data.tphaTppa.reagent ? data.tphaTppa.reagent : undefined}
                  onChange={v => _handleChangeImsLabInnerChild('tphaTppa', 'reagent', v)}
                  disabled={disabled}>
                  <Option value={null}>Pilih Reagen</Option>
                  {reagent.map(v => (
                    <Option key={v.id} value={v.id}>
                      {v.medicine.name}
                    </Option>
                  ))}
                </Select> */}

                <Label className="mb-0">Jumlah Penggunaan</Label>
                <Radio.Group
                  className="group-margin"
                  value={data.tphaTppa.totalReagentUsed ? data.tphaTppa.totalReagentUsed.toString() : null}
                  onChange={e => _handleChangeImsLabInnerChild("tphaTppa", 'totalReagentUsed', e.target.value)}
                  disabled={disabled}>
                  <table>
                    <tr>
                      <td><Radio value="1">1/1</Radio></td>
                      <td><Radio value="2">1/2</Radio></td>
                      <td><Radio value="4">1/4</Radio></td>
                      <td><Radio value="8">1/8</Radio></td>
                      <td><Radio value="16">1/16</Radio></td>
                    </tr>
                    <tr>
                      <td><Radio value="32">1/32</Radio></td>
                      <td><Radio value="64">1/64</Radio></td>
                      <td><Radio value="128">1/128</Radio></td>
                      <td><Radio value="256">1/256</Radio></td>
                      <td><Radio value="512">1/512</Radio></td>
                    </tr>
                  </table>
                </Radio.Group>
                {/* <Input
                  addonBefore="1:"
                  placeholder="Tulis Jumlah Penggunaan"
                  style={{ width: "100%" }}
                  className="mb-3"
                  value={data.tphaTppa.totalReagentUsed}
                  onChange={e => _handleChangeImsLabInnerChild("tphaTppa", 'totalReagentUsed', e.target.value)}
                  disabled={disabled} /> */}
              </React.Fragment>
            )}

            <hr />

            <Typography fontSize="14px" fontWeight="bold">
              Hasil Pemeriksaan Lab Lainnya
            </Typography>
            <Input
              placeholder="Tulis Alasan Rujukan"
              style={{ width: "100%" }}
              className="mb-3"
              name="etc"
              value={data.etc}
              onChange={e => _handleChange("etc", e.target.value)}
              disabled={disabled} />
          </>
        )}

        {role === ROLE.DOCTOR && (data.is_tested === true || data.is_tested === false) &&
          <>
            {/* <br />
            <br /> */}
            <Typography fontSize="14px" fontWeight="bold">
              Diagnosis IMS
                </Typography>

            <Label>Syndrome</Label>
            <Checkbox.Group style={{ width: '100%' }}
              value={diagnosisSyndrome}
              onChange={value => _handleChangeImsDiagnosis('syndrome', value)}>
              <Row>
                {Object.keys(typeDiagnosisSyndromeIms).map(key => (
                  <Col key={typeDiagnosisSyndromeIms[key]} span={10}>
                    <Checkbox value={key}>
                      {typeDiagnosisSyndromeIms[key]}
                    </Checkbox>
                  </Col>
                ))}
              </Row>
            </Checkbox.Group>

            <Label>Klinis</Label>
            <Checkbox.Group style={{ width: '100%' }}
              value={diagnosisKlinis}
              onChange={value => _handleChangeImsDiagnosis('klinis', value)}>
              <Row>
                {Object.keys(typeDiagnosisKlinisIms).map(key => (
                  <Col key={typeDiagnosisKlinisIms[key]} span={10}>
                    <Checkbox value={key}>
                      {typeDiagnosisKlinisIms[key]}
                    </Checkbox>
                  </Col>
                ))}
              </Row>
            </Checkbox.Group>

            <Label>Laboratorium</Label>
            <Checkbox.Group style={{ width: '100%' }}
              value={diagnosisLab}
              onChange={value => _handleChangeImsDiagnosis('lab', value)}>
              <Row>
                {Object.keys(typeDiagnosisLabIms).map(key => (
                  <Col key={typeDiagnosisLabIms[key]} span={10}>
                    <Checkbox value={key}>
                      {typeDiagnosisLabIms[key]}
                    </Checkbox>
                  </Col>
                ))}
              </Row>
            </Checkbox.Group>

            <br />
            <br />
            <Typography fontSize="14px" fontWeight="bold">
              Obat dan Konseling
                </Typography>

            {dataMedicine.map((v, i) =>
              <Row>
                <Col span={22}>
                  <Row>
                    <Col span={24}>
                      <Label>Pilih Obat</Label>
                      <ChooseReagen
                        placeholder="Pilih Obat"
                        style={{ width: "100%" }}
                        className="mb-3"
                        value={v.medicineId ? v.medicineId : undefined}
                        onChange={v => onChangeMedicine('medicineId', v, i)}
                        name="medicineId"
                        ims
                        disabledStock
                        showDetail
                      />

                      {/* <Select
                            placeholder="Pilih Obat"
                            style={{ width: "100%" }}
                            className="mb-3"
                            value={v.medicineId ? v.medicineId : undefined}
                            onChange={v => onChangeMedicine('medicineId', v, i)}
                          >
                            <Option value={null}>Pilih Obat</Option>
                            {medicineList.map(v => {
                              console.log(v.category);
                              if (v.category === "IMS") {
                                return (
                                  <Option key={v.id} value={v.id}>
                                    {v.name}
                                  </Option>
                                )
                              }
                            })}
                          </Select> */}
                    </Col>

                    <Col span={8}>
                      <Label>Jumlah Obat</Label>
                      <InputNumber
                        placeholder="Masukan Jumlah Obat"
                        style={{ width: "93%" }}
                        className="mb-3"
                        name="totalQty"
                        value={v.totalQty ? v.totalQty : undefined}
                        onChange={v => onChangeMedicine('totalQty', v, i)}
                      // value={stockQty}
                      // onChange={isIms ?
                      //   value => onChange('totalQty', value, 'medicines', idx)
                      //   :
                      //   value => onChange(idx, "stockQty", value)}
                      />
                    </Col>
                    <Col span={8}>
                      <Label>Jumlah Hari</Label>
                      <InputNumber
                        placeholder="Masukan Jumlah Hari"
                        style={{ width: "93%" }}
                        className="mb-3"
                        name="totalDays"
                        value={v.totalDays ? v.totalDays : undefined}
                        onChange={v => onChangeMedicine('totalDays', v, i)}
                      // value={jumlahHari}
                      // onChange={isIms ?
                      //   value => onChange('totalDays', value, 'medicines', idx)
                      //   :
                      //   value => onChange(idx, "jumlahHari", value)}
                      />
                    </Col>
                    <Col span={8}>
                      <Label>Aturan Pemakaian</Label>
                      <Input
                        placeholder="Aturan Pemakaian"
                        style={{ width: "98%" }}
                        className="mb-3"
                        name="note"
                        value={v.note ? v.note : undefined}
                        onChange={v => {
                          onChangeMedicine('note', v.target.value, i)
                        }}
                      // name="notes"
                      // value={notes}
                      // onChange={isIms ?
                      //   e => onChange('note', e.target.value, 'medicines', idx)
                      //   :
                      //   _handleChange}
                      />
                    </Col>
                  </Row>
                </Col>
                <Col span={2} style={{ paddingLeft: '10px' }}>
                  {i === 0 &&
                    <Button
                      size="small"
                      className={`hs-btn PHARMA_STAFF`}
                      icon="plus"
                      onClick={onAddMedicines}
                    />
                  }

                  {i > 0 && <Button
                    style={{ marginTop: i === 0 ? '5px' : '30px' }}
                    size="small"
                    type="danger"
                    onClick={() => onRemoveMedicines(i)}
                    icon="delete"
                  />}
                </Col>
              </Row>
            )}


            <Typography fontSize="14px" fontWeight="bold">
              Konseling
                </Typography>
            <Input
              placeholder="Tulis Konseling"
              style={{ width: "100%" }}
              className="mb-3"
              name="counseling"
              value={newData.counseling ? newData.counseling : undefined}
              onChange={e => newOnChange('counseling', e.target.value)}
            />
          </>
        }

        <div className="d-flex justify-content-end mt-2">
          <Button key="back" type="danger" onClick={onCancel}>
            Batal
            </Button>

          <Button
            key="submit"
            type="primary"
            className={`hs-btn ${role} ml-2`}
            loading={isLoading}
            htmlType="submit"
          >
            Simpan
            </Button>
        </div>
      </form>
    </Modal>
  );
};

export default Handler;