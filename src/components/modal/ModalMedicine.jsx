import React from "react";
import { Button, Modal, InputNumber, Empty } from "antd";
import { Label } from "./style";
import { keys } from "lodash";

export const ModalMedicine = ({
  onOk,
  onCancel,
  onChange,
  isLoading,
  role,
  medicines,
  data,
  isFound
}) => {
  const formNames = keys(data);

  const _handleSubmit = e => {
    e.preventDefault();
    if (isLoading) {
      return;
    }
    onOk();
  };

  return (
    <Modal
      title="Ubah Sisa Obat"
      visible
      style={{ top: 30 }}
      onOk={onOk}
      maskClosable={false}
      onCancel={onCancel}
      footer={null}
    >
      {!isFound && !isLoading && (
        <Empty description="Data obat kunjungan sebelumnya tidak ditemukan" />
      )}
      <form onSubmit={_handleSubmit}>
        {Object.keys(medicines).map((key, idx) => (
          <React.Fragment key={key}>
            <Label>Sisa Obat {medicines[key].name}</Label>
            <InputNumber
              placeholder="Masukan Sisa Obat"
              style={{ width: "100%" }}
              className="mb-3"
              name={formNames[idx]}
              onChange={value => onChange(formNames[idx], value)}
              required
            />
          </React.Fragment>
        ))}
        <div className="d-flex justify-content-end mt-2">
          <Button key="back" type="danger" onClick={onCancel}>
            {isFound ? "Batal" : "Tutup"}
          </Button>

          {isFound && (
            <Button
              key="submit"
              type="primary"
              className={`hs-btn ${role}`}
              loading={isLoading}
              htmlType="submit"
            >
              Simpan
            </Button>
          )}
        </div>
      </form>
    </Modal>
  );
};
