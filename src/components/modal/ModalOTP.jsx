import React from "react";
import { Button, Input, Modal } from "antd";
import { Typography } from "components";

export const ModalOTP = ({
  onOk,
  onCancel,
  isLoading,
  role,
  onChange,
  email,
  requestOTP
}) => {
  const _handleChange = e => {
    onChange(e.target.value);
  };

  const _handleSubmit = e => {
    e.preventDefault();
    if (isLoading) {
      return;
    }
    onOk();
  };
  return (
    <Modal
      title="Kode OTP"
      style={{ top: 30 }}
      visible
      maskClosable={false}
      onOk={onOk}
      onCancel={onCancel}
      footer={null}
    >
      <form onSubmit={_handleSubmit}>
        <div className="d-flex flex-column align-items-center">
          <Typography
            fontSize="14px"
            fontWeight="bold"
            className="d-block mb-3"
          >
            Masukkan kode verifikasi yang sudah dikirimkan ke
          </Typography>

          <Typography
            fontSize="14px"
            color={role}
            fontWeight="bold"
            className="d-block mb-3"
          >
            {email.toLowerCase() || ""}
          </Typography>

          <Input
            placeholder="Masukan Kode OTP"
            style={{ width: "50%" }}
            size="large"
            className="mb-3 text-center"
            onChange={_handleChange}
            required
          />

          <Typography
            fontSize="14px"
            fontWeight="bold"
            className="d-block mb-3"
          >
            Tidak menerima kode OTP?
          </Typography>
          <Button type="link" onClick={requestOTP}>
            Kirim ulang
          </Button>
        </div>
        <div className="d-flex justify-content-end mt-2">
          <Button
            key="submit"
            type="primary"
            className={`hs-btn ${role}`}
            loading={isLoading}
            htmlType="submit"
          >
            Lanjutkan
          </Button>
        </div>
      </form>
    </Modal>
  );
};
