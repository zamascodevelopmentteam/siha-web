import React, { useState, useEffect } from "react";
import { Button, Input, Modal, InputNumber } from "antd";
import { Label } from "./style";
import { ChooseDestination, ChooseStockAll } from "components";
import { ENUM } from "constant";

export const ModalPengiriman = ({
  onOk,
  isEdit,
  onCancel,
  isLoading,
  role,
  data,
  onChange,
  items,
  addItem,
  removeItem,
  onChangeItem,
  onChangeDestination
}) => {
  const [isPharma, setIsPharma] = useState(false);
  const [isLab, setIsLab] = useState(false);
  const _handleChange = e => {
    onChange(e.target.name, e.target.value);
  };

  const _handleSubmit = e => {
    e.preventDefault();
    if (isLoading) {
      return;
    }
    onOk();
  };

  useEffect(() => {
    if (data.logisticRole === ENUM.LOGISTIC_ROLE.PHARMA_ENTITY) {
      setIsPharma(true);
      setIsLab(false);
    } else if (data.logisticRole === ENUM.LOGISTIC_ROLE.LAB_ENTITY) {
      setIsPharma(false);
      setIsLab(true);
    } else {
      setIsPharma(false);
      setIsLab(false);
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data.logisticRole]);
  
  

  return (
    <Modal
      title="Tambah Pengiriman Keluar"
      visible
      width="1000px"
      onOk={onOk}
      maskClosable={false}
      onCancel={onCancel}
      footer={null}
    >
      <form onSubmit={_handleSubmit}>
        {/* <Label>Nomor Surat Jalan</Label>
        <Input
          placeholder="Nomor Surat Jalan"
          style={{ width: "100%" }}
          className="mb-3"
          name="droppingNumber"
          value={data.droppingNumber}
          onChange={_handleChange}
        /> */}

        <Label>Ditujukan ke</Label>

        <ChooseDestination
          placeholder="Pilih Tujuan Permintaan"
          style={{ width: "100%" }}
          className="mb-3"
          onChange={onChangeDestination}
          isDown={true}
          required
        />

        <div className="row mb-2">
          <div className="col-1">
            <Label>No.</Label>
          </div>
          <div className="col-6">
            <Label>Nama Barang</Label>
          </div>
          <div className="col-2">
            <Label>Stok Tersedia</Label>
          </div>
          <div className="col-2">
            <Label>Stok Dikirim</Label>
          </div>
          <div className="col-1">
            <Button size="small" type="primary" icon="plus" onClick={addItem} />
          </div>
        </div>
        {items.map((value, index) => {
          return (
            <ItemRow
              key={value.id}
              idx={index}
              inventoryId={value.inventoryId}
              packageQuantity={value.packageQuantity}
              quantityAvailable={value.quantityAvailable}
              packageUnitType={value.packageUnitType}
              onChange={onChangeItem}
              onRemove={removeItem}
              isPharma={isPharma}
              isLab={isLab}
            />
          );
        })}

        <Label>Alasan Pengiriman</Label>
        <Input
          placeholder="Alasan Pengiriman"
          style={{ width: "100%" }}
          className="mb-3"
          name="notes"
          value={data.notes}
          onChange={_handleChange}
        />

        <div className="d-flex justify-content-end mt-2">
          <Button key="back" type="danger" onClick={onCancel} className="mr-2">
            Batal
          </Button>

          <Button
            key="submit"
            type="primary"
            className={`hs-btn ${role}`}
            loading={isLoading}
            htmlType="submit"
          >
            Simpan
          </Button>
        </div>
      </form>
    </Modal>
  );
};

const ItemRow = ({
  idx,
  inventoryId,
  packageQuantity,
  quantityAvailable,
  packageUnitType,
  onRemove,
  onChange,
  isPharma,
  isLab
}) => {
  return (
    <div className="row">
      <div className="col-1">{idx + 1}</div>
      <div className="col-6">
        <ChooseStockAll
          placeholder="Nama Jenis Barang"
          style={{ width: "100%" }}
          className="mb-3"
          name="inventoryId"
          showStock
          value={inventoryId}
          onChange={value => {
            onChange(idx, "inventoryId", value.id);
            onChange(idx, "quantityAvailable", value.packageQuantity);
            onChange(idx, "packageUnitType", value.packageUnitType);
          }}
          required
          pharma={isPharma}
          lab={isLab}
        />
      </div>
      <div className="col-2">
        <Input
          placeholder="Stok Tersedia"
          style={{ width: "100%" }}
          className="mb-3"
          name="quantityAvailable"
          value={
            quantityAvailable ? quantityAvailable + " " + packageUnitType : ""
          }
          disabled
        />
      </div>
      <div className="col-2">
        <InputNumber
          placeholder="Jumlah Paket"
          style={{ width: "100%" }}
          className="mb-3"
          name="packageQuantity"
          value={packageQuantity}
          min={1}
          max={quantityAvailable ? quantityAvailable : 0}
          onChange={value => onChange(idx, "packageQuantity", value)}
        />
      </div>

      <div className="col-1">
        <Button
          size="small"
          type="danger"
          onClick={() => onRemove(idx)}
          icon="delete"
        />
      </div>
    </div>
  );
};
