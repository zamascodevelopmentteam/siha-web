import React from "react";
import { Input, Modal } from "antd";
import { Alert, Button, Form, FormGroup } from "reactstrap";

export const ModalForgotPassword = props => {
    const handleChange = e => {
        props.changeHandler(e);
    };

    return (
        <Modal
            title="Lupa Password"
            style={{ top: 30 }}
            visible
            maskClosable={false}
            onOk={props.submitHanler}
            onCancel={props.setModal}
            footer={null}
        >
            <Form className="" onSubmit={props.submitHanler}>
                {/* <div className="login-alert-wrapper"> */}
                    <Alert
                        color={'danger'}
                        isOpen={props.alert.length > 0}
                        // toggle={props.onDismiss}
                        >
                        {props.alert}
                    </Alert>
                {/* </div> */}
                <FormGroup>
                    <Input
                        className="width-full"
                        size="large"
                        type="text"
                        name="nik"
                        required
                        onChange={handleChange}
                        placeholder="Kode Layanan"
                        value={props.data.nik}
                    />
                </FormGroup>
                <FormGroup>
                    <Input.Password
                        placeholder="Password Baru"
                        name="password"
                        size="large"
                        className="w-100"
                        required
                        onChange={handleChange}
                        value={props.data.password}
                    />
                </FormGroup>
                <FormGroup>
                    <Input.Password
                        placeholder="Konfirmasi Password"
                        name="confirmPassword"
                        size="large"
                        className="mb-4 w-100"
                        required
                        onChange={handleChange}
                        value={props.data.confirmPassword}
                    />
                </FormGroup>
                <div className="text-center">
                    <Button
                        color="primary"
                        className="width-full"
                        type="submit"
                        disabled={props.inProgress ? props.inProgress : (props.alert.length > 0)}
                    >
                        {props.inProgress ? (
                            <React.Fragment>
                                <div
                                    className="spinner-border spinner-border-sm text-light"
                                    role="status"
                                />
                                <span> Loading...</span>
                            </React.Fragment>
                        ) : (
                                "Submit"
                            )}
                    </Button>
                </div>
            </Form>
        </Modal>
    );
};
