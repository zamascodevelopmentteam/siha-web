import React, { useState, useEffect, useContext } from "react";

import { Button, DatePicker, InputNumber, Modal, Radio, Checkbox, Row, Col } from "antd";
import { ENUM, ROLE, SCORING } from "constant";
import { Input, Select, Tabs } from "antd";
import { Label } from "./style";
import { ChooseReagen, Typography, ChooseUpk } from "components";
import moment from "moment";
import { validateForm } from "utils/validator";
import { useParams } from "react-router";
import axios from "axios";
import API from "utils/API";
import GlobalContext from "containers/GlobalContext";

const { Option } = Select;
const { TabPane } = Tabs;

const {
  AKHIR_FOLLOW_UP,
  GENDER,
  KEL_POPULASI,
  NOTIF_COUPLE,
  STATUS_TB,
  STATUS_FUNGSIONAL,
  STADIUM_KLINIS,
  STATUS_TB_TPT,
  PERIKSA_TB,
  // LSM_RUJUKAN,
  LSM_PENDAMPING
} = ENUM;



export const ModalPDP = ({
  hideBtnSave,
  isAdd,
  onOk,
  onCancel,
  isLoading,
  role,
  data,
  onChange,
  onChangeNotif,
  addNotif,
  removeNotif,
  couple,
  dataMedicine,
  dataMedicineAction
}) => {
  // let source = axios.CancelToken.source()
  const { id } = useParams();
  const user = useContext(GlobalContext);

  const mAction = dataMedicineAction;

  let requiredField = {
    noRegNas: "Nomor Register Nasional",
    statusFungsional: "Status Fungsional",
    stadiumKlinis: "Stadium Klinis",
    weight: "Berat Badan",
    height: "Tinggi Badan",
    // kelompokPopulasi: "Kelompok Populasi",
    tglKonfirmasiHivPos: "Tanggal Konfirmasi Test HIV Positif",
    tglKunjungan: "Tanggal Kunjungan",
    // tglRujukMasuk: "Tanggal Rujuk Masuk",
    statusTb: "Status TB",
    // ppk: "PPK",
    notifikasiPasangan: "Notifikasi Pasangan"
  }
  const [tabIndex, setTabIndex] = useState("1");
  const [dataPatien, setDataPatien] = useState({});
  // const [medicineList, setMedicineList] = useState([]);

  const _handleChange = e => {
    onChange(e.target.name, e.target.value);
  };

  const _handleChangeSelect = (name, value, key = null) => {
    if (key == null) {
      onChange(name, value);
    } else {
      let newValue = { ...data.skoring };
      newValue[key] = value;
      onChange(name, newValue);
    }
  };

  const _changeTab = to => {
    let newIndex = parseInt(tabIndex) - 1;
    if (to === "next") {
      newIndex = parseInt(tabIndex) + 1;
    }
    setTabIndex(newIndex.toString());
  };

  const _filterKelomokPopulation = (gender, KEL_POPULASI) => {
    if (!gender) return KEL_POPULASI

    let obj = {}

    if (gender === "LAKI-LAKI") {
      for (let key in KEL_POPULASI) {
        if (key !== "WPS" && key !== "BUMIL") {
          obj[key] = KEL_POPULASI[key]
        }
      }
    } else {
      for (let key in KEL_POPULASI) {
        if (key !== "WARIA" && key !== "LSL") {
          obj[key] = KEL_POPULASI[key]
        }
      }
    }

    return obj
  }

  const _handleChangeKelompokPopulasi = checkedValues => {
    onChange("kelompokPopulasi", checkedValues)
  }

  const _handleChangeBulanPemeriksaan = checkedValues => {
    onChange("bulanPemeriksaan", checkedValues);
  }

  if (data.statusTb === STATUS_TB.POSITIF_TB || data.periksaTB === PERIKSA_TB.POSITIF_TB) {
    // data.statusTbTpt = null
    // data.tglPemberianObat = null
  }
  if (data.statusTb === STATUS_TB.NEGATIF_TB || data.periksaTB === PERIKSA_TB.NEGATIF_TB) {
    data.tglPengobatanTb = null
    data.isDiberiOAT = null
  }
  if (data.statusTb === STATUS_TB.NEGATIF_TB || data.statusTb === STATUS_TB.POSITIF_TB) {
    data.periksaTB = null
  }
  if (data.periksaTB === PERIKSA_TB.TIDAK_DIPERIKSA || data.periksaTB === PERIKSA_TB.DALAM_PEMERIKSAAN) {
    data.statusTbTpt = null
    data.tglPemberianObat = null
    data.tglPengobatanTb = null
    data.isDiberiOAT = null
  }
  if (data.statusTb === STATUS_TB.POSITIF_TB || data.periksaTB === PERIKSA_TB.POSITIF_TB) {
    requiredField = {
      ...requiredField,
      isDiberiOAT: "Diberi OAT"
    }
  }
  if (data.isDiberiOAT === true && (data.statusTb === STATUS_TB.POSITIF_TB || data.periksaTB === PERIKSA_TB.POSITIF_TB)) {
    requiredField = {
      ...requiredField,
      tglPengobatanTb: "Tanggal Mulai Pengobatan TB (OAT)"
    }
  }
  if (data.statusTb === STATUS_TB.NEGATIF_TB || data.periksaTB === PERIKSA_TB.NEGATIF_TB) {
    requiredField = {
      ...requiredField,
      statusTbTpt: "Status Terapi Pencegahan TB (TPT)"
    }
  }
  if (data.ppk) {
    requiredField = {
      ...requiredField,
      tglPemberianPpk: "Tanggal Mulai Pemberian PPK"
    }
  }
  if (data.statusTbTpt && data.statusTbTpt !== STATUS_TB_TPT.TDK_DIBERIKAN) {
    requiredField = {
      ...requiredField,
      tglPemberianObat: "Tanggal Mulai Terapi Pencegahan TB"
    }
  }
  if (data.akhirFollowUp === AKHIR_FOLLOW_UP.RUJUK_KELUAR) {
    requiredField = {
      ...requiredField,
      tglRujukKeluar: "Tanggal Rujuk Keluar"
    }
  }
  if (data.akhirFollowUp === AKHIR_FOLLOW_UP.MENINGGAL) {
    requiredField = {
      ...requiredField,
      tglMeninggal: "Tanggal Meninggal"
    }
  }
  if (data.akhirFollowUp === AKHIR_FOLLOW_UP.BERHENTI_ARV) {
    requiredField = {
      ...requiredField,
      tglBerhentiArv: "Tanggal Berhenti ARV"
    }
  }
  // new
  let bulan = 0;
  let bulanPemerisaan = [];
  if (data.statusTbTpt === STATUS_TB_TPT.INH) {
    bulan = 6;
  }
  if (data.statusTbTpt === STATUS_TB_TPT["3HP"]) {
    bulan = 3;
  }
  for (let i = 0; i < bulan; i++) {
    bulanPemerisaan.push(
      <Col key={i} span="4">
        <Checkbox value={'Bulan ' + (i + 1)}>
          Bulan {i + 1}
        </Checkbox>
      </Col>
    );
  }
  // end new

  const _handleSubmit = e => {
    e.preventDefault();
    const validated = validateForm(data, requiredField, false)
    if (!validated) {
      return
    }
    if (isLoading) {
      return;
    }
    onOk();
  };

  // const _loadData = () => {
  //   // setLoading(true);
  //   API.Patient.getByID(source.token, id)
  //     .then(rsp => {
  //       const contructed = _constructResponse(rsp.data);
  //       setDataPatien(contructed);
  //     })
  //     .catch(e => {
  //       console.error("e: ", e);
  //     })
  //     .finally(() => {
  //       // setLoading(false);
  //     });
  // };

  // const _fetchMedicine = () => {
  //   API.IMS.medicinesList(source.token)
  //     .then(rsp => {
  //       setMedicineList(rsp.data)
  //     })
  //     .catch(e => {
  //       console.error("e: ", e);
  //     })
  //     .finally(() => {
  //       // setLoading(false);
  //     });
  // }

  const newIdd = id;
  useEffect((newId = newIdd) => {
    const _loadData = () => {
      // setLoading(true);
      let source = axios.CancelToken.source();
      // const { id } = useParams();
      API.Patient.getByID(source.token, newId)
        .then(rsp => {
          const contructed = _constructResponse(rsp.data);
          setDataPatien(contructed);
        })
        .catch(e => {
          console.error("e: ", e);
        })
        .finally(() => {
          // setLoading(false);
        });
    };
    _loadData();
    // _fetchMedicine();
  }, [newIdd]);

  let skor = 0;
  data.skoring && Object.values(data.skoring).map(v => {
    if (v !== null) {
      skor += v;
    }
    return true;
  });

  return (
    <Modal
      title={isAdd ? "Tambah PDP" : "Ubah PDP"}
      style={{ top: 30 }}
      width="800px"
      visible
      maskClosable={false}
      onOk={onOk}
      onCancel={onCancel}
      footer={null}
    >
      <form onSubmit={_handleSubmit}>
        <Typography fontSize="14px" fontWeight="bold" className="d-block mb-3">
          Kondisi Fisik
        </Typography>
        <Label>
          Tanggal Masuk Perawatan{" "}
          <span className="text-danger">*Harus Diisi</span>
        </Label>
        <DatePicker
          placeholder="Pilih Tanggal Masuk Perawatan"
          style={{ width: "100%" }}
          allowClear={false}
          className="mb-3"
          name="treatmentStartDate"
          value={
            data.treatmentStartDate ? moment(data.treatmentStartDate) : null
          }
          onChange={date => onChange("treatmentStartDate", date.toISOString())}
          required
        />
        <Label>
          Tanggal Mulai ART{" "}
          <span className="text-danger">*Harus Diisi</span>
        </Label>
        <DatePicker
          placeholder="Pilih Tanggal Mulai ART"
          style={{ width: "100%" }}
          allowClear={false}
          className="mb-3"
          name="artStartDate"
          value={
            data.artStartDate ? moment(data.artStartDate) : null
          }
          onChange={date => onChange("artStartDate", date.toISOString())}
          required
        />
        <Label>
          Nomor Register Nasional{" "}
          <span className="text-danger">*Harus Diisi</span>
        </Label>
        <Input
          placeholder="Masukkan Nama Nomor Register Nasional"
          style={{ width: "100%" }}
          className="mb-3"
          name="noRegNas"
          value={data.noRegNas}
          onChange={_handleChange}
          required
        />

        <Label>
          Status Fungsional <span className="text-danger">*Harus Diisi</span>
        </Label>
        <Select
          placeholder="Pilih Status Fungsional"
          style={{ width: "100%" }}
          className="mb-3"
          name="statusFungsional"
          value={data.statusFungsional}
          allowClear
          onChange={value => onChange("statusFungsional", value)}
          required
        >
          <Option disabled value={null}>
            Pilih Status Fungsional
          </Option>
          {Object.keys(STATUS_FUNGSIONAL).map(key => (
            <Option key={STATUS_FUNGSIONAL[key]} value={STATUS_FUNGSIONAL[key]}>
              {STATUS_FUNGSIONAL[key]}
            </Option>
          ))}
        </Select>

        <Label>
          Stadium Klinis <span className="text-danger">*Harus Diisi</span>
        </Label>
        <Select
          allowClear
          placeholder="Pilih Stadium Klinis"
          style={{ width: "100%" }}
          className="mb-3"
          name="stadiumKlinis"
          value={data.stadiumKlinis}
          onChange={value => onChange("stadiumKlinis", value)}
          required
        >
          <Option disabled value={null}>
            Pilih Stadium Klinis
          </Option>
          {Object.keys(STADIUM_KLINIS).map(key => (
            <Option key={STADIUM_KLINIS[key]} value={STADIUM_KLINIS[key]}>
              {STADIUM_KLINIS[key]}
            </Option>
          ))}
        </Select>
        {/* start new */}
        {data.stadiumKlinis !== STADIUM_KLINIS.STADIUM_1 && (
          <React.Fragment>
            <Label>
              PPK <span className="text-danger">*Harus Diisi</span>
            </Label>
            <Radio.Group
              className="mb-3"
              value={data.ppk}
              name="ppk"
              onChange={_handleChange}
            >
              <Radio value={true}>Ya</Radio>
              <Radio value={false}>Tidak</Radio>
            </Radio.Group>

            {data.ppk && (
              <React.Fragment>
                <Label>
                  Tanggal Mulai Pemberian PPK{" "}
                  <span className="text-danger">*Harus Diisi</span>
                </Label>
                <DatePicker
                  placeholder="Pilih Tanggal Mulai Pemberian PPK"
                  style={{ width: "100%" }}
                  className="mb-3"
                  name="tglPemberianPpk"
                  // allowClear={false}
                  allowClear
                  value={
                    data.tglPemberianPpk ? moment(data.tglPemberianPpk) : null
                  }
                  onChange={date =>
                    onChange(
                      "tglPemberianPpk",
                      date ? date.toISOString() : null
                    )
                  }
                  required
                />
              </React.Fragment>
            )}
          </React.Fragment>
        )}
        {/* end new */}
        <Label>
          Berat Badan (kg) <span className="text-danger">*Harus Diisi</span>
        </Label>
        <InputNumber
          placeholder="Masukan Berat Badan Pasien (kg)"
          style={{ width: "100%" }}
          className="mb-3"
          name="weight"
          min={0}
          value={data.weight}
          onChange={value => onChange("weight", value)}
          required
        />

        <Label>
          Tinggi Badan (cm) <span className="text-danger">*Harus Diisi</span>
        </Label>
        <InputNumber
          placeholder="Masukan Tinggi Badan Pasien (cm)"
          style={{ width: "100%" }}
          className="mb-3"
          min={0}
          name="height"
          value={data.height}
          onChange={value => onChange("height", value)}
          required
        />
        <Tabs
          activeKey={tabIndex}
          onChange={value => {
            setTabIndex(value);
          }}
        >
          <TabPane tab="Detail Pasien" key="1">
            <Label>
              Kelompok Populasi{" "}
              {/* <span className="text-danger">*Harus Diisi</span> */}
            </Label>
            <Select
              placeholder="Masukan Kelompok Populasi"
              style={{ width: "100%" }}
              name="kelompokPopulasi"
              value={dataPatien.kelompokPopulasi ? dataPatien.kelompokPopulasi[0] : null}
              onChange={
                // value => _handleChangeSelect("lsmPenjangkau", value)
                value => _handleChangeKelompokPopulasi([value])
              }
              required
            >
              <Option disabled value={null}>Pilih Kelompok Populasi</Option>
              {Object.keys(_filterKelomokPopulation(data.gender, KEL_POPULASI)).map(key => (
                <Option key={KEL_POPULASI[key]} value={KEL_POPULASI[key]}>
                  {KEL_POPULASI[key]}
                </Option>
              ))}
            </Select>
            {/* <Checkbox.Group style={{ width: '100%' }} name="kelompokPopulasi" value={dataPatien.kelompokPopulasi} onChange={_handleChangeKelompokPopulasi} required>
              <Row>
                {Object.keys(_filterKelomokPopulation(dataPatien.gender, KEL_POPULASI)).map(key => (
                  <Col key={KEL_POPULASI[key]} span={8}>
                    <Checkbox value={KEL_POPULASI[key]} readOnly>
                      {KEL_POPULASI[key]}
                    </Checkbox>
                  </Col>
                ))}
              </Row>
            </Checkbox.Group> */}
            <br />
            <br />

            <Label>
              Tanggal Konfirmasi Test HIV Positif{" "}
              <span className="text-danger">*Harus Diisi</span>
            </Label>
            <DatePicker
              placeholder="Masukkan Tanggal Masuk Perawatan"
              style={{ width: "100%" }}
              allowClear={false}
              className="mb-3"
              name="tglKonfirmasiHivPos"
              value={
                data.tglKonfirmasiHivPos
                  ? moment(data.tglKonfirmasiHivPos)
                  : null
              }
              onChange={date =>
                onChange(
                  "tglKonfirmasiHivPos",
                  date ? date.toISOString() : null
                )
              }
              required
            />

            <Label>
              Tanggal Kunjungan{" "}
              <span className="text-danger">*Harus Diisi</span>
            </Label>
            <DatePicker
              placeholder="Masukkan Tanggal Kunjungan"
              style={{ width: "100%" }}
              allowClear={false}
              className="mb-3"
              name="tglKunjungan"
              value={data.tglKunjungan ? moment(data.tglKunjungan) : null}
              onChange={date =>
                onChange("tglKunjungan", date ? date.toISOString() : null)
              }
              required
            />

            <Label>
              Tanggal Rujuk Masuk{" "}
              {/* <span className="text-danger">*Harus Diisi</span> */}
            </Label>
            <DatePicker
              // disabled
              placeholder="Masukkan Tanggal Rujuk Masuk"
              style={{ width: "100%" }}
              allowClear={false}
              className="mb-3"
              name="tglRujukMasuk"
              value={data.tglRujukMasuk ? moment(data.tglRujukMasuk) : null}
              onChange={date =>
                onChange("tglRujukMasuk", date ? date.toISOString() : null)
              }
            // required
            />

            <Label>Nama Fasyankes Sebelumnya</Label>
            <Input
              // disabled
              style={{ width: "100%" }}
              className="mb-3"
              name="upkSebelumnyaData"
              value={data.upkSebelumnyaData}
              onChange={value => onChange("upkSebelumnyaData", value)}
            />
          </TabPane>
          <TabPane tab="Skoring Pendampingan" key="2">
            <table>
              <thead>
                <tr>
                  <th width="2%" className="text-center">No</th>
                  <th width="49%">Parameter</th>
                  <th width="49%">Jawaban</th>
                </tr>
              </thead>
              <tbody>
                {Object.keys(SCORING).map((k, i) => (
                  <tr>
                    <td className="text-center">{i + 1}</td>
                    <td>{SCORING[k].label}</td>
                    <td>
                      <Select
                        placeholder="Pilih jawaban"
                        style={{ width: "100%" }}
                        className="mb-3"
                        name="skoring"
                        value={data.skoring ? data.skoring[k] : undefined}
                        onChange={value => _handleChangeSelect("skoring", value, k)}
                      // required
                      >
                        <Option disabled value={null}>Pilih Jawaban</Option>
                        {SCORING[k].answer.map(r => (
                          <Option key={r.score} value={r.score}>
                            {r.label}
                          </Option>
                        ))}
                      </Select>
                    </td>
                  </tr>
                ))}
                <tr>
                  <td colSpan="2">Skor</td>
                  <td>{skor} ({skor >= 8 ? "Didampingi maksimal" : "Didampingi minimal"})</td>
                </tr>
              </tbody>
            </table>
            {/* {skor >= 8 && ( */}
            <React.Fragment>
              {/* <Label>
                  
                </Label> */}
              <Radio.Group
                className="mb-3"
                value={data.didampingi === "true" ? true : false}
                name="didampingi"
                onChange={e => {
                  e.target.value = e.target.value.toString();
                  _handleChange(e);
                }}
              >
                <Radio value={true}>Bersedia di dampingi</Radio>
                <Radio value={false}>Tidak bersedia di dampingi</Radio>
              </Radio.Group>
              {data.didampingi === "true" && (
                <React.Fragment>
                  <Label>
                    LSM Pendamping / Kader
                    </Label>
                  <Select
                    placeholder="Pilih Nama LSM Pendamping"
                    style={{ width: "100%" }}
                    className="mb-3"
                    name="lsmPenjangkau"
                    value={data.lsmPenjangkau && data.lsmPenjangkau.length ? data.lsmPenjangkau : undefined}
                    onChange={value => _handleChangeSelect("lsmPenjangkau", value)}
                  // required
                  >
                    <Option disabled value={null}>Pilih LSM Pendamping</Option>
                    {Object.keys(LSM_PENDAMPING).map(key => (
                      <Option key={LSM_PENDAMPING[key]} value={LSM_PENDAMPING[key]}>
                        {LSM_PENDAMPING[key]}
                      </Option>
                    ))}
                  </Select>
                </React.Fragment>
              )}
            </React.Fragment>
            {/* )} */}
          </TabPane>
          <TabPane tab="TBC" key="3">
            <Label>
              Status TB <span className="text-danger">*Harus Diisi</span>
            </Label>
            <Select
              allowClear
              placeholder="Pilih Status TB"
              style={{ width: "100%" }}
              className="mb-3"
              name="statusTb"
              value={data.statusTb}
              onChange={value => onChange("statusTb", value)}
              required
            >
              <Option disabled value={null}>
                Pilih Status TB
              </Option>
              {Object.keys(STATUS_TB).map(key => (
                <Option key={STATUS_TB[key]} value={STATUS_TB[key]}>
                  {STATUS_TB[key]}
                </Option>
              ))}
            </Select>

            {data.statusTb === STATUS_TB.TIDAK_TAU_STATUS && (
              <React.Fragment>
                <Label>
                  Periksa TB{" "}
                  <span className="text-danger">*Harus Diisi</span>
                </Label>
                <Select
                  placeholder="Pilih Terapi Pencegahan TB"
                  allowClear
                  style={{ width: "100%" }}
                  className="mb-3"
                  name="periksaTB"
                  value={data.periksaTB}
                  onChange={value => onChange("periksaTB", value)}
                  required
                >
                  <Option disabled value={null}>
                    Diperiksa TB
                  </Option>
                  {Object.keys(PERIKSA_TB).map(key => (
                    <Option key={PERIKSA_TB[key]} value={PERIKSA_TB[key]}>
                      {PERIKSA_TB[key]}
                    </Option>
                  ))}
                </Select>
              </React.Fragment>
            )}

            {/* ---------------------------------------------- */}

            {(data.statusTb === STATUS_TB.POSITIF_TB || data.periksaTB === PERIKSA_TB.POSITIF_TB) && (
              <React.Fragment>
                <Label>
                  Diberi OAT{" "}
                  <span className="text-danger">*Harus Diisi</span>
                </Label>
                <Select
                  placeholder="Diberi OAT"
                  allowClear
                  style={{ width: "100%" }}
                  className="mb-3"
                  name="isDiberiOAT"
                  value={data.isDiberiOAT}
                  onChange={value => onChange("isDiberiOAT", value)}
                  required
                >
                  <Option disabled value={null}>
                    Diberi OAT
                  </Option>
                  <Option key={"Ya"} value={true}>
                    {"Ya"}
                  </Option>
                  <Option key={"Tidak"} value={false}>
                    {"Tidak"}
                  </Option>
                </Select>
              </React.Fragment>
            )}

            {(data.statusTb === STATUS_TB.POSITIF_TB || data.periksaTB === PERIKSA_TB.POSITIF_TB) && data.isDiberiOAT === true && (
              <React.Fragment>
                <Label>
                  Tanggal Mulai Pengobatan TB (OAT){" "}
                  <span className="text-danger">*Harus Diisi</span>
                </Label>
                <DatePicker
                  placeholder="Pilih Tanggal Mulai Pengobatan TB"
                  style={{ width: "100%" }}
                  className="mb-3"
                  name="tglPengobatanTb"
                  // allowClear={false}
                  allowClear
                  value={
                    data.tglPengobatanTb ? moment(data.tglPengobatanTb) : null
                  }
                  onChange={date =>
                    onChange(
                      "tglPengobatanTb",
                      date ? date.toISOString() : null
                    )
                  }
                  required
                />

                <Label>
                  Tanggal Selesai Pengobatan TB (OAT){" "}
                  <span className="text-danger">*Harus Diisi</span>
                </Label>
                <DatePicker
                  placeholder="Pilih Tanggal Selesai Pengobatan TB"
                  style={{ width: "100%" }}
                  className="mb-3"
                  name="tglPengobatanTbSelesai"
                  // allowClear={false}
                  allowClear
                  value={
                    data.tglPengobatanTbSelesai ? moment(data.tglPengobatanTbSelesai) : null
                  }
                  onChange={date =>
                    onChange(
                      "tglPengobatanTbSelesai",
                      date ? date.toISOString() : null
                    )
                  }
                  required
                />
              </React.Fragment>
            )}

            {/* ---------------------------------------------- */}

            {
              (
                (
                  // data.statusTb === STATUS_TB.NEGATIF_TB ||
                  // data.periksaTB === PERIKSA_TB.NEGATIF_TB ||
                  data.statusTb === STATUS_TB.POSITIF_TB ||
                  data.periksaTB === PERIKSA_TB.POSITIF_TB
                ) &&
                data.isDiberiOAT &&
                data.tglPengobatanTbSelesai
              ) && (
                <React.Fragment>
                  <Label>
                    Status Terapi Pencegahan TB (TPT){" "}
                    <span className="text-danger">*Harus Diisi</span>
                  </Label>
                  <Select
                    placeholder="Pilih Terapi Pencegahan TB"
                    allowClear
                    style={{ width: "100%" }}
                    className="mb-3"
                    name="statusTbTpt"
                    value={data.statusTbTpt}
                    onChange={value => onChange("statusTbTpt", value)}
                    required
                  >
                    <Option disabled value={null}>
                      Pilih Terapi Pencegahan TB
                    </Option>
                    {Object.keys(STATUS_TB_TPT).map(key => (
                      <Option key={STATUS_TB_TPT[key]} value={STATUS_TB_TPT[key]}>
                        {STATUS_TB_TPT[key]}
                      </Option>
                    ))}
                  </Select>
                </React.Fragment>
              )
            }

            {
              (
                (
                  data.statusTb === STATUS_TB.NEGATIF_TB ||
                  data.periksaTB === PERIKSA_TB.NEGATIF_TB
                )
              ) && (
                <React.Fragment>
                  <Label>
                    Status Terapi Pencegahan TB (TPT){" "}
                    <span className="text-danger">*Harus Diisi</span>
                  </Label>
                  <Select
                    placeholder="Pilih Terapi Pencegahan TB"
                    allowClear
                    style={{ width: "100%" }}
                    className="mb-3"
                    name="statusTbTpt"
                    value={data.statusTbTpt}
                    onChange={value => onChange("statusTbTpt", value)}
                    required
                  >
                    <Option disabled value={null}>
                      Pilih Terapi Pencegahan TB
                    </Option>
                    {Object.keys(STATUS_TB_TPT).map(key => (
                      <Option key={STATUS_TB_TPT[key]} value={STATUS_TB_TPT[key]}>
                        {STATUS_TB_TPT[key]}
                      </Option>
                    ))}
                  </Select>
                </React.Fragment>
              )
            }

            {(data.statusTbTpt && data.statusTbTpt !== STATUS_TB_TPT.TDK_DIBERIKAN) && (
              <React.Fragment>
                <Label>
                  Tanggal Mulai Terapi Pencegahan TB{" "}
                  <span className="text-danger">*Harus Diisi</span>
                </Label>
                <DatePicker
                  placeholder="Pilih Tanggal Mulai Terapi Pencegahan TB"
                  allowClear
                  style={{ width: "100%" }}
                  className="mb-3"
                  name="tglPemberianObat"
                  // allowClear={false}
                  value={
                    data.tglPemberianObat ? moment(data.tglPemberianObat) : null
                  }
                  onChange={date =>
                    onChange(
                      "tglPemberianObat",
                      date ? date.toISOString() : null
                    )
                  }
                  required
                />
              </React.Fragment>
            )}

            {(data.statusTbTpt && data.statusTbTpt !== STATUS_TB_TPT.TDK_DIBERIKAN && data.tglPemberianObat) && (
              <React.Fragment>
                <Checkbox.Group style={{ width: '100%' }} name="bulanPengecekan" value={data.bulanPemeriksaan} onChange={_handleChangeBulanPemeriksaan}>
                  <Row>
                    {bulanPemerisaan}
                  </Row>
                </Checkbox.Group>
                <br />
                <br />
              </React.Fragment>
            )}

            {(data.statusTbTpt && data.statusTbTpt !== STATUS_TB_TPT.TDK_DIBERIKAN && data.tglPemberianObat && (data.bulanPemeriksaan && data.bulanPemeriksaan.includes("Bulan " + (bulan - 1)))) && (
              <React.Fragment>
                <Label>
                  Tanggal Selesai Terapi Pencegahan TB{" "}
                  <span className="text-danger">*Harus Diisi</span>
                </Label>
                <DatePicker
                  placeholder="Pilih Tanggal Selesai Terapi Pencegahan TB"
                  allowClear
                  style={{ width: "100%" }}
                  className="mb-3"
                  name="tglPemberianObatSelesai"
                  // allowClear={false}
                  value={
                    data.tglPemberianObatSelesai ? moment(data.tglPemberianObatSelesai) : null
                  }
                  onChange={date =>
                    onChange(
                      "tglPemberianObatSelesai",
                      date ? date.toISOString() : null
                    )
                  }
                  required
                />
              </React.Fragment>
            )}
          </TabPane>
          <TabPane tab="Resep Obat" key="4">
            {/* {isIms ? */}
            {false ? <React.Fragment></React.Fragment>
              // dataIms.medicines.map((v, i) =>
              //   <MedicineRow
              //     key={v.id}
              //     idx={i}
              //     notes={v.note}
              //     medicineId={v.tmpMedicineId !== 0 ? v.tmpMedicineId : undefined}
              //     stockQty={v.totalQty}
              //     jumlahHari={v.totalDays}
              //     onChange={onChangeMedicineIms}
              //     onRemove={removeMedicine}
              //     isIms={true}
              //   />
              // )
              :
              dataMedicine.map((value, index) => {
                return (
                  <MedicineRow
                    key={index}
                    idx={index}
                    notes={value.notes}
                    medicineId={value.medicineId}
                    stockQty={value.stockQty}
                    jumlahHari={value.jumlahHari}
                    onChange={mAction.inputMedicineChangeHandler}
                    onRemove={mAction.onRemoveMedicines}
                    onAdd={mAction.onAddMedicines}
                  />
                );
              })
            }
            <div className="row">
              <div className="col-12">
                <Label>Status Paduan</Label>
                <Select
                  onChange={value => onChange("statusPaduan", value)}
                  required
                  name="statusPaduan"
                  value={data.statusPaduan}
                  placeholder="Status Paduan"
                >
                  <Option disabled value={null} >Pilih Status Paduan</Option>
                  {Object.keys(ENUM.STATUS_PADUAN).map(key =>
                    <Option key={key} value={ENUM.STATUS_PADUAN[key]}>
                      {ENUM.STATUS_PADUAN[key]}
                    </Option>)}
                </Select>
              </div>
            </div>
          </TabPane>
          <TabPane tab="Pasangan" key="5">
            <Label>
              Notifikasi Pasangan{" "}
              <span className="text-danger">*Harus Diisi</span>
            </Label>
            <Select
              placeholder="Pilih Notifikasi Pasangan"
              style={{ width: "100%" }}
              className="mb-3"
              name="notifikasiPasangan"
              value={data.notifikasiPasangan}
              onChange={value => onChange("notifikasiPasangan", value)}
              required
            >
              <Option disabled value={null}>
                Pilih Notifikasi Pasangan
              </Option>
              {Object.keys(NOTIF_COUPLE).map(key => (
                <Option key={NOTIF_COUPLE[key]} value={NOTIF_COUPLE[key]}>
                  {NOTIF_COUPLE[key]}
                </Option>
              ))}
            </Select>

            {data.notifikasiPasangan === NOTIF_COUPLE.MENERIMA && (
              <React.Fragment>
                <div className="row mb-2">
                  <div className="col-3">
                    <Label>Nama Tes Index</Label>
                  </div>
                  <div className="col-2">
                    <Label>Umur</Label>
                  </div>
                  <div className="col-3">
                    <Label>Jenis Kelamin</Label>
                  </div>
                  <div className="col-3">
                    <Label>Hubungan</Label>
                  </div>
                  <div className="col-1">
                    <Button
                      size="small"
                      className={`hs-btn ${role}`}
                      icon="plus"
                      onClick={() => addNotif()}
                    />
                  </div>
                </div>

                {couple.map((notif, idx) => (
                  <NotificationRow
                    key={notif.id}
                    idx={idx}
                    data={notif}
                    onRemove={removeNotif}
                    onChange={onChangeNotif}
                  />
                ))}
              </React.Fragment>
            )}
          </TabPane>
          <TabPane tab="Akhir Follow Up" key="6">
            <Label>
              Jenis Follow-Up <span className="text-danger">*Harus Diisi</span>
            </Label>
            <Select
              placeholder="Masukkan Jenis Follow-Up"
              style={{ width: "100%" }}
              className="mb-3"
              name="akhirFollowUp"
              value={data.akhirFollowUp}
              onChange={value => onChange("akhirFollowUp", value)}
              required
            >
              <Option value={null}>MASIH DALAM PENGOBATAN</Option>
              {Object.keys(AKHIR_FOLLOW_UP).map(key => (
                <Option key={AKHIR_FOLLOW_UP[key]} value={AKHIR_FOLLOW_UP[key]}>
                  {AKHIR_FOLLOW_UP[key]}
                </Option>
              ))}
            </Select>

            {data.akhirFollowUp === AKHIR_FOLLOW_UP.RUJUK_KELUAR && (
              <React.Fragment>
                <Label>
                  Tanggal Rujuk Keluar{" "}
                  <span className="text-danger">*Harus Diisi</span>
                </Label>
                <DatePicker
                  placeholder="Pilih Tanggal"
                  style={{ width: "100%" }}
                  className="mb-3"
                  allowClear={false}
                  name="tglRujukKeluar"
                  value={
                    data.tglRujukKeluar ? moment(data.tglRujukKeluar) : null
                  }
                  onChange={date =>
                    onChange("tglRujukKeluar", date ? date.toISOString() : null)
                  }
                  required
                />
                <Label>
                  UPK Rujukan <span className="text-danger">*Harus Diisi</span>
                </Label>
                <ChooseUpk
                  onChange={value => onChange("upkId", value)}
                  placeholder="Pilih UPK"
                  // sudinKabID={user.sudinKabKotaId}
                  style={{ width: "100%" }}
                  allowClear
                  defaultValue={null}
                  value={data.upkId}
                  limit={999999}
                />
              </React.Fragment>
            )}
            {data.akhirFollowUp === AKHIR_FOLLOW_UP.MENINGGAL && (
              <React.Fragment>
                <Label>
                  Tanggal Meninggal{" "}
                  <span className="text-danger">*Harus Diisi</span>
                </Label>
                <DatePicker
                  placeholder="Pilih Tanggal"
                  style={{ width: "100%" }}
                  allowClear={false}
                  className="mb-3"
                  name="tglMeninggal"
                  value={data.tglMeninggal ? moment(data.tglMeninggal) : null}
                  onChange={date =>
                    onChange("tglMeninggal", date ? date.toISOString() : null)
                  }
                  required
                />
              </React.Fragment>
            )}
            {data.akhirFollowUp === AKHIR_FOLLOW_UP.BERHENTI_ARV && (
              <React.Fragment>
                <Label>
                  Tanggal Berhenti ARV{" "}
                  <span className="text-danger">*Harus Diisi</span>
                </Label>
                <DatePicker
                  placeholder="Pilih Tanggal"
                  style={{ width: "100%" }}
                  className="mb-3"
                  allowClear={false}
                  name="tglBerhentiArv"
                  value={
                    data.tglBerhentiArv ? moment(data.tglBerhentiArv) : null
                  }
                  onChange={date =>
                    onChange("tglBerhentiArv", date ? date.toISOString() : null)
                  }
                  required
                />
              </React.Fragment>
            )}
          </TabPane>
        </Tabs>
        <div className="d-flex justify-content-end mt-2">
          {tabIndex >= 6 && (
            <React.Fragment>
              <Button
                key="back"
                type="danger"
                onClick={onCancel}
                className="mr-2"
              >
                {role === ROLE.RR_STAFF ? "Batal" : "Tutup"}
              </Button>
              {role === ROLE.RR_STAFF && !hideBtnSave && (
                <Button
                  key="submit"
                  type="primary"
                  className={`hs-btn ${role}`}
                  loading={isLoading}
                  htmlType="submit"
                  onClick={() => validateForm(data, requiredField)}
                >
                  Simpan
                </Button>
              )}
            </React.Fragment>
          )}
          {tabIndex < 6 && tabIndex > 1 && (
            <Button
              key="back"
              type="danger"
              onClick={() => _changeTab("previous")}
              className="mr-2"
            >
              Sebelumnya
            </Button>
          )}
          {tabIndex < 6 && (
            <Button
              type="primary"
              className={`hs-btn ${role}`}
              loading={isLoading}
              onClick={() => _changeTab("next")}
            >
              Berikutnya
            </Button>
          )}
        </div>
      </form>
    </Modal>
  );
};

const NotificationRow = ({ idx, onRemove, onChange, data }) => {
  const _handleChange = e => {
    onChange(idx, e.target.name, e.target.value);
  };

  return (
    <div className="row">
      <div className="col-3">
        <Input
          placeholder="Nama"
          style={{ width: "100%" }}
          className="mb-3"
          name="name"
          value={data.name}
          onChange={_handleChange}
        />
      </div>
      <div className="col-2">
        <InputNumber
          style={{ width: "100%" }}
          className="mb-3"
          name="age"
          value={data.age}
          onChange={value => onChange(idx, "age", value)}
        />
      </div>
      <div className="col-3">
        <Select
          placeholder="Pilih Jenis Kelamin"
          style={{ width: "100%" }}
          className="mb-3"
          name="gender"
          value={data.gender}
          onChange={value => onChange(idx, "gender", value)}
        >
          <Option disabled value={""}>
            Pilih Jenis Kelamin
          </Option>
          {Object.keys(GENDER).map(key => (
            <Option key={GENDER[key]} value={GENDER[key]}>
              {GENDER[key]}
            </Option>
          ))}
        </Select>
      </div>
      <div className="col-3">
        <Select
          placeholder="Pilih Jenis Hubungan"
          style={{ width: "100%" }}
          className="mb-3"
          name="relationship"
          value={data.relationship}
          onChange={value => onChange(idx, "relationship", value)}
          required
        >
          <Option value={""} disabled>
            Pilih Jenis Hubungan
          </Option>
          {Object.keys(ENUM.RELATIONSHIP).map(key => (
            <Option key={ENUM.RELATIONSHIP[key]} value={ENUM.RELATIONSHIP[key]}>
              {ENUM.RELATIONSHIP[key]}
            </Option>
          ))}
        </Select>
      </div>
      <div className="col-1">
        <Button
          size="small"
          type="danger"
          onClick={() => onRemove(idx)}
          icon="delete"
        />
      </div>
    </div>
  );
};

const _constructResponse = res => {
  const {
    nik,
    name,
    addressKTP,
    addressDomicile,
    dateBirth,
    gender,
    phone,
    statusPatient,
    weight,
    height,
    namaPmo,
    hubunganPmo,
    noHpPmo,
    user,
    tglMeninggal,
    lsmPenjangkau,
    kelompokPopulasi
  } = res;

  return {
    nik,
    name,
    addressKTP,
    addressDomicile,
    dateBirth,
    gender,
    phone,
    statusPatient,
    weight,
    height,
    namaPmo,
    hubunganPmo,
    noHpPmo,
    tglMeninggal,
    email: user.email,
    lsmPenjangkau,
    kelompokPopulasi
  };
};

const MedicineRow = ({
  idx,
  medicineId,
  notes,
  stockQty,
  jumlahHari,
  onRemove,
  onChange,
  isIms = false,
  onAdd
}) => {
  // const _handleChange = e => {
  //   onChange(idx, e.target.name, e.target.value);
  // };

  return (
    <div className="row">
      <div className="col-1">{idx + 1}</div>
      <div className="col-10">
        <ChooseReagen
          placeholder="Pilih Obat"
          style={{ width: "100%" }}
          className="mb-3"
          name="medicineId"
          value={medicineId}
          showDetail
          medicineType="ARV"
          onChange={isIms ?
            value => onChange('tmpMedicineId', value, 'medicines', idx)
            :
            value => onChange('medicineId', idx, value)}
          required
          hiv
        />
      </div>
      <div className="col-1">
        {idx === 0 ? (
          <Button
            size="small"
            className={`hs-btn btn-primary`}
            icon="plus"
            onClick={onAdd}
          />
        ) : (
          <Button
            size="small"
            type="danger"
            onClick={() => onRemove(idx)}
            icon="delete"
          />
        )
        }
      </div>

      <div className="col-1"></div>
      <div className="col-3">
        <InputNumber
          placeholder="Masukan Jumlah Obat"
          style={{ width: "100%" }}
          className="mb-3"
          name="stockQty"
          value={stockQty}
          onChange={isIms ?
            value => onChange('totalQty', value, 'medicines', idx)
            :
            value => onChange("stockQty", idx, value)}
        />
      </div>
      <div className="col-3">
        <InputNumber
          placeholder="Masukan Jumlah Hari"
          style={{ width: "100%" }}
          className="mb-3"
          name="jumlahHari"
          value={jumlahHari}
          onChange={isIms ?
            value => onChange('totalDays', value, 'medicines', idx)
            :
            value => onChange("jumlahHari", idx, value)}
        />
      </div>
      <div className="col-4">
        <Input
          placeholder="Aturan Minum"
          style={{ width: "100%" }}
          className="mb-3"
          name="notes"
          value={notes}
          onChange={isIms ?
            e => onChange('note', e.target.value, 'medicines', idx)
            :
            e => onChange('notes', idx, e.target.value)}
        />
      </div>
    </div>
  );
};