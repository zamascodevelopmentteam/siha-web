import React from "react";
import { DatePicker, Button, Input, InputNumber, Modal } from "antd";
import { Label } from "./style";
import { ChooseReagen } from "components";
import moment from "moment";

export const ModalProfilaksis = ({
  isAdd,
  onOk,
  onCancel,
  isLoading,
  role,
  data,
  onChange
}) => {
  const _handleSubmit = e => {
    e.preventDefault();
    if (isLoading) {
      return;
    }
    onOk();
  };

  const _handleChangeDate = date => {
    onChange("tglProfilaksis", date.toISOString());
  };

  return (
    <Modal
      title={
        isAdd
          ? "Tambah Data Penggunaan Profilaksis"
          : "Ubah Data Penggunaan Profilaksis"
      }
      visible
      onOk={onOk}
      onCancel={onCancel}
      footer={null}
    >
      <form onSubmit={_handleSubmit}>
        <React.Fragment>
          <Label>
            Tanggal Profilaksis{" "}
            <span className="text-danger">*Harus Diisi</span>
          </Label>
          <DatePicker
            placeholder="Pilih tanggal Profilaksis"
            style={{ width: "100%" }}
            allowClear={false}
            className="mb-3"
            name="tglProfilaksis"
            value={data.tglProfilaksis ? moment(data.tglProfilaksis) : null}
            onChange={_handleChangeDate}
            required
          />

          <Label>
            Obat yang Diberikan{" "}
            <span className="text-danger">*Harus Diisi</span>
          </Label>
          <ChooseReagen
            placeholder="Pilih Obat"
            style={{ width: "100%" }}
            className="mb-3"
            value={data.obatDiberikan || null}
            name="obatDiberikan"
            // paramsFilter="is_profilaksis=true"
            medicineType="ARV"
            showDetail
            onChange={value => onChange("obatDiberikan", value)}
            required
          />

          <Label>
            Jumlah Obat yang Diberikan{" "}
            <span className="text-danger">*Harus Diisi</span>
          </Label>
          <InputNumber
            placeholder="Masukkan Jumlah Obat (Contoh: 2)"
            style={{ width: "100%" }}
            className="mb-3"
            name="qtyObat"
            value={data.qtyObat}
            onChange={value => onChange("qtyObat", value)}
            required
          />

          <Label>
            Status Profilaksis <span className="text-danger">*Harus Diisi</span>
          </Label>
          <Input
            placeholder="Masukkan Status Profilaksis"
            style={{ width: "100%" }}
            className="mb-3"
            value={data.statusProfilaksis}
            name="statusProfilaksis"
            onChange={e => onChange("statusProfilaksis", e.target.value)}
            required
          />
        </React.Fragment>

        <div className="d-flex justify-content-end mt-2">
          <Button key="back" type="danger" onClick={onCancel} className="mr-2">
            Batal
          </Button>
          <Button
            key="submit"
            type="primary"
            className={`hs-btn ${role}`}
            loading={isLoading}
            htmlType="submit"
          >
            Simpan
          </Button>
        </div>
      </form>
    </Modal>
  );
};
