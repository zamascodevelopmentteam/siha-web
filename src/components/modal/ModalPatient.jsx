import React from "react";
import { ENUM } from "constant";
import { DatePicker, Modal, Button, Select, Input, Checkbox, Row, Col } from "antd";
import { Label } from "./style";
import { Typography } from "components";
import moment from "moment";
import { validateForm } from "utils/validator";
import { openNotification } from "../../utils/Notification"

const { Option } = Select;
const { TextArea } = Input;
const { STATUS_PATIENT, GENDER, KEL_POPULASI, LSM_RUJUKAN } = ENUM;

export const ModalPatient = ({
  onOk,
  onCancel,
  isLoading,
  role,
  onChange,
  data,
  isView,
  isEdit
}) => {
  let requiredField = {
    nik: "NIK",
    name: "Nama",
    addressKTP: "Alamat (Sesuai KTP)",
    addressDomicile: "Alamat Domisili",
    dateBirth: "Tanggal Lahir",
    gender: "Jenis Kelamin",
    phone: "No. Telepon",
    // email: "Alamat Email",
    statusPatient: "Status ODHA",
    lsmPenjangkau: "Rujukan Dari",
    kelompokPopulasi: "Kelompok Populasi",
  }
  let title = "Tambah Pasien";
  if (isView) {
    title = "Lihat Pasien";
  }
  if (isEdit) {
    title = "Edit Pasien";
  }
  if (data.statusPatient === STATUS_PATIENT.ODHA || data.statusPatient === STATUS_PATIENT.ODHA_LAMA) {
    requiredField = {
      ...requiredField,
      namaPmo: "Nama Lengkap PMO",
      hubunganPmo: "Hubungan PMO dengan Pasien",
      noHpPmo: "Nomor Telepon PMO",
    }
  }
  if (data.dateBirth && moment().diff(moment(data.dateBirth), "years") <= 1) {
    requiredField = {
      ...requiredField,
      motherNik: "NIK Ibu",
    }
  }
  const _handleChange = e => {
    onChange(e.target.name, e.target.value);
  };

  const _handleChangeSelect = (name, value) => {
    onChange(name, value);
  };

  const _handleChangeDate = date => {
    onChange("dateBirth", date.toISOString());
  };

  const _handleChangeDateMeninggal = date => {
    onChange("tglMeninggal", date.toISOString());
  };

  const _handleChangeKelompokPopulasi = checkedValues => {
    onChange("kelompokPopulasi", checkedValues)
  }

  const _handleSubmit = e => {
    e.preventDefault();

    const validated = validateForm(data, requiredField, false)
    if (!validated) {
      return
    }

    if (data.kelompokPopulasi.length > 1) {
      openNotification("error", "Validation Error", 'Kelompok populasi tidak boleh lebih dari 1')
      return;
    }

    if (isLoading) {
      return;
    }
    onOk();
  };

  const _filterKelomokPopulation = (gender, KEL_POPULASI) => {
    if (!gender) return KEL_POPULASI

    let obj = {}

    if (gender === "LAKI-LAKI") {
      for (let key in KEL_POPULASI) {
        if (key !== "WPS" && key !== "BUMIL") {
          obj[key] = KEL_POPULASI[key]
        }
      }
    } else {
      for (let key in KEL_POPULASI) {
        if (key !== "WARIA" && key !== "LSL") {
          obj[key] = KEL_POPULASI[key]
        }
      }
    }

    return obj
  }

  return (
    <Modal
      title={title}
      visible
      style={{ top: 30 }}
      onOk={onOk}
      maskClosable={false}
      onCancel={onCancel}
      footer={null}
    >
      <form onSubmit={_handleSubmit}>
        <Typography fontSize="14px" fontWeight="bold" className="d-block mb-3">
          Data Pribadi
        </Typography>

        {isView && <ViewMode data={data} />}

        {!isView && (
          <React.Fragment>
            <Label>
              NIK <span className="text-danger">*Harus Diisi</span>
            </Label>
            <Input
              placeholder="Masukan NIK Pasien"
              style={{ width: "100%" }}
              className="mb-3"
              name="nik"
              minLength={16}
              maxLength={16}
              value={data.nik}
              onChange={_handleChange}
            />

            <Label>
              Nama <span className="text-danger">*Harus Diisi</span>
            </Label>
            <Input
              placeholder="Masukan Nama Pasien"
              style={{ width: "100%" }}
              className="mb-3"
              name="name"
              value={data.name}
              onChange={_handleChange}
              required
            />

            <Label>
              Alamat (Sesuai KTP){" "}
              <span className="text-danger">*Harus Diisi</span>
            </Label>
            <TextArea
              placeholder="Masukkan Alamat Lengkap Sesuai KTP"
              style={{ width: "100%" }}
              className="mb-3"
              name="addressKTP"
              value={data.addressKTP}
              onChange={_handleChange}
              required
            />

            <Label>
              Alamat Domisili <span className="text-danger">*Harus Diisi</span>
            </Label>
            <TextArea
              placeholder="Masukkan Alamat Lengkap Domisili"
              style={{ width: "100%" }}
              className="mb-3"
              name="addressDomicile"
              value={data.addressDomicile}
              onChange={_handleChange}
              required
            />

            <Label>
              Tanggal Lahir <span className="text-danger">*Harus Diisi</span>
            </Label>
            <DatePicker
              placeholder="Pilih tanggal lahir"
              style={{ width: "100%" }}
              allowClear={false}
              className="mb-3"
              name="dateBirth"
              value={data.dateBirth ? moment(data.dateBirth) : null}
              onChange={_handleChangeDate}
              required
            />
            {data.dateBirth && (
              <Label isInfo className="mb-3">
                Usia: {moment().diff(moment(data.dateBirth), "years")} Tahun
              </Label>
            )}

            {data.dateBirth && moment().diff(moment(data.dateBirth), "years") <= 1 && (
              <React.Fragment>
                <Label>
                  NIK Ibu <span className="text-danger">*Harus Diisi</span>
                </Label>
                <Input
                  placeholder="Masukan NIK Ibu"
                  style={{ width: "100%" }}
                  className="mb-3"
                  name="motherNik"
                  value={data.motherNik}
                  onChange={_handleChange}
                />
              </React.Fragment>
            )}

            <Label>
              Jenis Kelamin <span className="text-danger">*Harus Diisi</span>
            </Label>
            <Select
              placeholder="Pilih Jenis Kelamin"
              style={{ width: "100%" }}
              className="mb-3"
              name="gender"
              value={data.gender}
              onChange={value => _handleChangeSelect("gender", value)}
              required
            >
              <Option disabled value={null}>
                Pilih Jenis Kelamin
              </Option>
              {Object.keys(GENDER).map(key => (
                <Option key={GENDER[key]} value={GENDER[key]}>
                  {GENDER[key]}
                </Option>
              ))}
            </Select>
            <Label>
              No. Telepon <span className="text-danger">*Harus Diisi</span>
            </Label>
            <Input
              placeholder="Masukkan Nomor Telepon Pasien"
              style={{ width: "100%" }}
              className="mb-3"
              name="phone"
              value={data.phone}
              onChange={_handleChange}
              required
            />
            <Label>
              Alamat Email
              {/* <span className="text-danger">*Harus Diisi</span> */}
            </Label>
            <Input
              placeholder="Masukkan Alamat Email Pasien"
              style={{ width: "100%" }}
              className="mb-3"
              name="email"
              value={data.email}
              type="email"
              onChange={_handleChange}
            />
            {!isEdit && (
              <React.Fragment>
                <Label>
                  Status ODHA <span className="text-danger">*Harus Diisi</span>
                </Label>
                <Select
                  placeholder="Pilih status ODHA"
                  style={{ width: "100%" }}
                  className="mb-3"
                  name="statusPatient"
                  value={data.statusPatient}
                  onChange={value =>
                    _handleChangeSelect("statusPatient", value)
                  }
                  required
                >
                  <Option disabled value={null}>
                    Pilih Status ODHA
                  </Option>
                  <Option value={STATUS_PATIENT.BLM_TAHU}>
                    {STATUS_PATIENT.BLM_TAHU}
                  </Option>
                  <Option value={STATUS_PATIENT.ODHA_LAMA}>
                    {STATUS_PATIENT.ODHA_LAMA}
                  </Option>
                </Select>
              </React.Fragment>
            )}

            <Label>
              Rujukan Dari{" "}
              <span className="text-danger">*Harus Diisi</span>
            </Label>
            <Select
              placeholder="Masukan LSM Rujukan"
              style={{ width: "100%" }}
              className="mb-3"
              name="lsmPenjangkau"
              value={data.lsmPenjangkau && data.lsmPenjangkau.length ? data.lsmPenjangkau : undefined}
              onChange={value => _handleChangeSelect("lsmPenjangkau", value)}
              required
            >
              <Option disabled value={null}>Pilih LSM Rujukan</Option>
              {Object.keys(LSM_RUJUKAN).map(key => (
                <Option key={LSM_RUJUKAN[key]} value={LSM_RUJUKAN[key]}>
                  {LSM_RUJUKAN[key]}
                </Option>
              ))}
            </Select>

            <Label>
              Kelompok Populasi{" "}
              <span className="text-danger">*Harus Diisi</span>
            </Label>
            <Select
              placeholder="Masukan Kelompok Populasi"
              style={{ width: "100%" }}
              className="mb-3"
              name="kelompokPopulasi"
              value={data.kelompokPopulasi ? data.kelompokPopulasi[0] : null}
              onChange={
                // value => _handleChangeSelect("lsmPenjangkau", value)
                value => _handleChangeKelompokPopulasi([value])
              }
              required
            >
              <Option disabled value={null}>Pilih Kelompok Populasi</Option>
              {Object.keys(_filterKelomokPopulation(data.gender, KEL_POPULASI)).map(key => (
                <Option key={KEL_POPULASI[key]} value={KEL_POPULASI[key]}>
                  {KEL_POPULASI[key]}
                </Option>
              ))}
            </Select>
            {/* <Checkbox.Group style={{ width: '100%' }} name="kelompokPopulasi" value={data.kelompokPopulasi} onChange={_handleChangeKelompokPopulasi} required>
              <Row>
                {Object.keys(_filterKelomokPopulation(data.gender, KEL_POPULASI)).map(key => (
                  <Col key={KEL_POPULASI[key]} span={8}>
                    <Checkbox value={KEL_POPULASI[key]}>
                      {KEL_POPULASI[key]}
                    </Checkbox>
                  </Col>
                ))}
              </Row>
            </Checkbox.Group> */}
            <br />
            <br />

            {(data.statusPatient === STATUS_PATIENT.ODHA || data.statusPatient === STATUS_PATIENT.ODHA_LAMA) && (
              <React.Fragment>
                <Typography
                  fontSize="14px"
                  fontWeight="bold"
                  className="d-block mb-3"
                >
                  Data Pengawas Minum Obat (PMO)
                </Typography>
                <Label>
                  Nama Lengkap <span className="text-danger">*Harus Diisi</span>
                </Label>
                <Input
                  placeholder="Masukkan Nama Lengkap PMO"
                  style={{ width: "100%" }}
                  className="mb-3"
                  name="namaPmo"
                  value={data.namaPmo}
                  onChange={_handleChange}
                  required
                />
                <Label>
                  Status Hubungan{" "}
                  <span className="text-danger">*Harus Diisi</span>
                </Label>
                <Input
                  placeholder="Masukkan Hubungan PMO dengan Pasien"
                  style={{ width: "100%" }}
                  className="mb-3"
                  name="hubunganPmo"
                  value={data.hubunganPmo}
                  onChange={_handleChange}
                  required
                />
                <Label>
                  No. Telepon <span className="text-danger">*Harus Diisi</span>
                </Label>
                <Input
                  placeholder="Masukkan Nomor Telepon PMO"
                  style={{ width: "100%" }}
                  className="mb-3"
                  name="noHpPmo"
                  value={data.noHpPmo}
                  onChange={_handleChange}
                  required
                />
                {isEdit && (
                  <React.Fragment>
                    <Label>Tanggal Meninggal</Label>
                    <DatePicker
                      placeholder="Pilih tanggal Meninggal"
                      style={{ width: "100%" }}
                      allowClear={false}
                      className="mb-3"
                      name="tglMeninggal"
                      value={
                        data.tglMeninggal ? moment(data.tglMeninggal) : null
                      }
                      onChange={_handleChangeDateMeninggal}
                    />
                  </React.Fragment>
                )}
              </React.Fragment>
            )}
          </React.Fragment>
        )}
        {!isView && (
          <div className="d-flex justify-content-end mt-2">
            <Button key="back" type="danger" onClick={onCancel}>
              Batal
            </Button>

            <Button
              key="submit"
              type="primary"
              className={`hs-btn ${role} ml-2`}
              loading={isLoading}
              htmlType="submit"
              onClick={() => validateForm(data, requiredField)}
            >
              Simpan
            </Button>
          </div>
        )}
      </form>
    </Modal>
  );
};

const ViewMode = ({ data }) => (
  <React.Fragment>
    <Label>NIK</Label>
    <Label isInfo className="mb-3">
      {data.nik}
    </Label>

    <Label>Nama</Label>
    <Label isInfo className="mb-3">
      {data.name}
    </Label>

    <Label>Alamat (Sesuai KTP)</Label>
    <Label isInfo className="mb-3">
      {data.addressKTP}
    </Label>

    <Label>Alamat Domisili</Label>
    <Label isInfo className="mb-3">
      {data.addressDomicile}
    </Label>

    <Label>Tanggal Lahir</Label>
    <Label isInfo className="mb-3">
      {data.dateBirth}
    </Label>

    <Label>Jenis Kelamin</Label>
    <Label isInfo className="mb-3">
      {data.gender}
    </Label>

    <Label>No. Telepon</Label>
    <Label isInfo className="mb-3">
      {data.phone}
    </Label>

    <Label>Alamat Email</Label>
    <Label isInfo className="mb-3">
      {data.email}
    </Label>

    <Label>Status ODHA</Label>
    <Label isInfo className="mb-3">
      {data.statusPatient}
    </Label>

    {(data.statusPatient === STATUS_PATIENT.ODHA || data.statusPatient === STATUS_PATIENT.ODHA_LAMA) && (
      <React.Fragment>
        <Typography fontSize="14px" fontWeight="bold" className="d-block mb-3">
          Data Pengawas Minum Obat (PMO)
        </Typography>

        <Label>Nama Lengkap</Label>
        <Label isInfo className="mb-3">
          {data.namaPmo}
        </Label>

        <Label>Status Hubungan</Label>
        <Label isInfo className="mb-3">
          {data.hubunganPmo}
        </Label>

        <Label>No. Telepon</Label>
        <Label isInfo className="mb-3">
          {data.noHpPmo}
        </Label>
      </React.Fragment>
    )}
  </React.Fragment>
);
