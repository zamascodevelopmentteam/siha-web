import React from "react";
// import moment from "moment";
import { Button, Modal, InputNumber } from "antd";
import { Label, LabelInline } from "./style";
import { Typography, DisplayDate } from "components";

export const ModalRencanaDistribusi = ({
  onOk,
  onCancel,
  isLoading,
  role,
  data,
  onChange
}) => {
  // const _handleChange = e => {
  //   onChange(e.target.name, e.target.value);
  // };
  let isEmptyStock = true;
  let total = [];
  const _handleSubmit = e => {
    e.preventDefault();
    if (isLoading) {
      return;
    }
    onOk();
  };

  let lastR = "";
  let isValid = true;

  return (
    <Modal
      title="Pemenuhan Permintaan"
      visible
      width="1000px"
      onOk={onOk}
      maskClosable={false}
      onCancel={onCancel}
      footer={null}
    >
      <form onSubmit={_handleSubmit}>
        {data.map((dist, idxDist) => {
          if (!dist.isValid) {
            isValid = false;
          }

          const same = lastR == dist.type;
          lastR = dist.type
          return (
            <React.Fragment key={dist.id}>
              {(isEmptyStock = dist.pickings.length > 0 ? false : isEmptyStock)}
              <div className="row mb-2">
                {!same && idxDist != 0 && (
                  <div className="col-12">
                    <hr />
                  </div>
                )}
                {!same && dist.type && (
                  <div className="col-12 text-center my-3">
                    <h4><b>Jumlah {dist.type ? dist.type.replace('r', 'Rapid Diagnostic Test ') : ''} : {dist.rAmount} {dist.packageUnitType}</b></h4>
                  </div>
                )}
                <div className="col-12 text-center mb-3">
                  <Typography fontSize="14px" fontWeight="bold">
                    Data Barang yang Diminta: {dist.type ? dist.type.toUpperCase() + ' ' : ''}{dist.medicine.name} {!dist.type  ? `(${dist.packageQuantity} ${dist.packageUnitType})` : ``}
                  </Typography>
                </div>
                <div className="col-3">
                  <LabelInline>Nama Brand</LabelInline>
                </div>
                <div className="col-2">
                  <Label>Batch Code</Label>
                </div>
                <div className="col-2">
                  <Label>Expired Date</Label>
                </div>
                <div className="col-2">
                  <Label>Jumlah Stock</Label>
                </div>
                <div className="col-3">
                  <Label>Stock Dikirim</Label>
                </div>
              </div>
              {dist.pickings.length === 0 ? (
                <div className="row">
                  <div className="col-12 text-center bg-light p-2">
                    <Typography fontSize="14px" fontWeight="bold">
                      Data picking list kosong
                    </Typography>
                  </div>
                </div>
              ) : (
                dist.pickings.map((item, idxPicking) => {
                  var date1 = new Date();
                  var date2 = new Date(item.expiredDate);
                  var difference_In_Time = date2.getTime() - date1.getTime();
                  var difference_In_Days = difference_In_Time / (1000 * 3600 * 24);
                  let textClass = "";
                  if (difference_In_Days <= 90) {
                    textClass = "text-warning";
                  }
                  if (difference_In_Days <= 30) {
                    textClass = "text-danger";
                  }
                  if (total[idxDist] == undefined) {
                    total[idxDist] = item.packageQuantity;
                  } else {
                    total[idxDist] += item.packageQuantity;
                  }
                  return (
                    <ItemRow
                      key={item.inventoryId}
                      idx={idxPicking}
                      idxDist={idxDist}
                      brandName={item.brandName}
                      batchCode={item.batchCode}
                      expiredDate={item.expiredDate}
                      packageQuantity={item.packageQuantity}
                      stockQty={item.stockQty}
                      packageUnitType={item.packageUnitType}
                      onChange={onChange}
                      // max={dist.type ? dist.rAmount : item.max}
                      max={dist.type ? dist.rAmount : item.stockQty}
                      textClass={textClass}
                      isValid={dist.isValid}
                    />
                  );
                })
              )}
              {(dist.pickings.length !== 0 && (
                <ItemRow
                  key={9999999}
                  idx={null}
                  idxDist={null}
                  brandName={null}
                  batchCode={null}
                  expiredDate={null}
                  packageQuantity={total[idxDist]}
                  stockQty={null}
                  packageUnitType={null}
                  onChange={null}
                  max={total[idxDist]}
                  textClass={""}
                  isValid={dist.isValid}
                />
              ))}
            </React.Fragment>
          )
        })}

        <div className="d-flex justify-content-end mt-2">
          <Button key="back" type="danger" onClick={onCancel} className="mr-2">
            Batal
          </Button>
          {!isEmptyStock && (
            <Button
              key="submit"
              type="primary"
              className={`hs-btn ${role}`}
              loading={isLoading}
              htmlType="submit"
              disabled={!isValid}
            >
              Simpan
            </Button>
          )}
        </div>
      </form>
    </Modal>
  );
};

const ItemRow = ({
  idx,
  idxDist,
  brandName,
  batchCode,
  expiredDate,
  packageQuantity,
  packageUnitType,
  stockQty,
  onChange,
  max,
  textClass,
  isValid
}) => {
  return (
    <div className={"row " + textClass}>
      <div className="col-3">{brandName}</div>
      <div className="col-2">{batchCode}</div>
      <div className="col-2">
        {(
          expiredDate !== null && (
            <DisplayDate date={expiredDate} />
          )
        )}
      </div>
      <div className="col-2">
        {(stockQty !== null ? stockQty + ' ' + packageUnitType : <b>Total</b>)}
      </div>
      <div className="col-3">
        <InputNumber
          readOnly={onChange === null}
          placeholder="Contoh: 100"
          style={{ width: "70%" }}
          className={"mb-3 mr-2 " + (!isValid ? "border-danger text-danger" : "")}
          name="packageQuantity"
          max={max}
          min={0}
          value={packageQuantity}
          onChange={value => onChange(idxDist, idx, value)}
        />
        {packageUnitType}
      </div>
    </div>
  );
};
