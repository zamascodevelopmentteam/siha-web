import React from "react";
import moment from "moment";
import { Button, DatePicker, Input, Modal, InputNumber, Select } from "antd";
import { Label } from "./style";
import { ChooseBrand, ChooseMedicine } from "components";
import { ROLE, TAG_LABEL, sumberDana } from "constant";

const { Option } = Select;
let medicineTypeParam = "";

export const ModalPenerimaan = ({
  medicineType,
  onOk,
  isEdit,
  onCancel,
  isLoading,
  role,
  data,
  onChange,
  items,
  addItem,
  removeItem,
  selectedDest,
  onChangeItem,
  onChangeDestination,
  producers
}) => {
  medicineTypeParam = medicineType;
  const _handleChange = e => {
    onChange(e.target.name, e.target.value);
  };

  const _handleSubmit = e => {
    e.preventDefault();
    if (isLoading) {
      return;
    }
    onOk();
  };

  return (
    <Modal
      title="Detail Pembelian"
      visible
      width="60%"
      onOk={onOk}
      maskClosable={false}
      onCancel={onCancel}
      footer={null}
    >
      <form onSubmit={_handleSubmit}>
        <Label>Jenis Pembelian</Label>
        <Select
          placeholder="Pilih Jenis Pembelian"
          style={{ width: "100%" }}
          className="mb-3"
          name="distType"
          value={data.distType}
          onChange={value => onChange("distType", value)}
        >
          <Option disabled value={null}>
            Pilih Jenis Pembelian
          </Option>
          {Object.keys(TAG_LABEL.DIST_TYPE).map(
            key => (
              <Option key={key} value={key}>
                {TAG_LABEL.LABEL_DIST_TYPE[key]}
              </Option>
            )
            // ((role === ROLE.MINISTRY_STAFF &&
            //   key !== TAG_LABEL.DIST_TYPE.PEMBELIAN_APBD) ||
            //   (role !== ROLE.MINISTRY_STAFF &&
            //     key !== TAG_LABEL.DIST_TYPE.PEMBELIAN_APBN)) && (
            //   <Option key={key} value={key}>
            //     {TAG_LABEL.LABEL_DIST_TYPE[key]}
            //   </Option>
            // )
          )}
        </Select>

        <Label>Nomor Surat Jalan</Label>
        <Input
          placeholder="Nomor Surat Jalan"
          style={{ width: "100%" }}
          className="mb-3"
          name="droppingNumber"
          value={data.droppingNumber}
          onChange={_handleChange}
        />

        <Label>Pengirim</Label>
        <Input
          placeholder="Pengirim"
          style={{ width: "100%" }}
          className="mb-3"
          name="entityName"
          value={data.entityName}
          onChange={_handleChange}
          required
        />

        <Label>
          Data Barang
          <Button
            size="small"
            className={`hs-btn ${ROLE.PHARMA_STAFF} ml-3`}
            icon="plus"
            onClick={addItem}
          />
        </Label>
        <div className="row mb-2">
          {
            items.map((value, index) => {
              let produsen = [];
              producers.map(r => {
                if (r.medicineType == value.medicineType) {
                  produsen.push(r);
                }
              });
              return (
                <ItemRow
                  medicineType={medicineType}
                  key={value.id}
                  idx={index}
                  medicineId={value.medicineId}
                  brandId={value.brandId}
                  packageQuantity={value.packageQuantity}
                  expiredDate={value.expiredDate}
                  fundSource={value.fundSource}
                  packagePrice={value.packagePrice}
                  manufacture={value.manufacture}
                  batchCode={value.batchCode}
                  onChange={onChangeItem}
                  onRemove={removeItem}
                  unit={value.unit}
                  produsen={produsen}
                  distType={data.distType}
                />
              )
            })
          }
        </div>
        <div className="row mb-2">

        </div>
        <div className="d-flex justify-content-end mt-2">
          <Button key="back" type="danger" onClick={onCancel} className="mr-2">
            {role === ROLE.PHARMA_STAFF ? "Batal" : "Tutup"}
          </Button>
          {/* {role === ROLE.PHARMA_STAFF && ( */}
          <Button
            key="submit"
            type="primary"
            className={`hs-btn ${role}`}
            loading={isLoading}
            htmlType="submit"
          >
            Simpan
          </Button>
          {/* )} */}
        </div>
      </form>
    </Modal>
  );
};

const _disabledDate = current => {
  return (
    current &&
    current <
    moment()
      .subtract(1, "days")
      .endOf("day")
  );
};

const ItemRow = ({
  idx,
  medicineId,
  brandId,
  packageQuantity,
  fundSource,
  packagePrice,
  expiredDate,
  batchCode,
  manufacture,
  onRemove,
  onChange,
  unit,
  produsen,
  distType
}) => {
  return (
    <React.Fragment>
      <div className="col-md-12">
        <fieldset style={{ border: "1px solid #dee2e6" }}>
          <legend style={{ width: "25%", marginLeft: ".5rem" }} className={'d-flex align-items-center'}>
            <span>No. {idx + 1}</span>
            <Button
              className={'ml-3'}
              size="small"
              type="danger"
              onClick={() => onRemove(idx)}
              icon="delete"
            />
          </legend>
          <div className="row pt-0 pr-3 pl-3 pb-3">
            <div className="col-md-12 pb-1">
              <Label>Nama Barang</Label>
              <ChooseMedicine
                placeholder="Pilih Obat"
                style={{ width: "100%" }}
                name="medicineId"
                value={medicineId}
                medicineType={medicineTypeParam}
                onChange={value => {
                  onChange(idx, "medicineId", value);
                }}
                required
              />
            </div>
            <div className="col-md-6 pb-1">
              <Label>Nama Merek</Label>
              <ChooseBrand
                placeholder="Pilih Merek"
                style={{ width: "100%" }}
                name="brandId"
                value={brandId}
                medicineID={medicineId}
                onChange={value => onChange(idx, "brandId", value)}
                required
              />
            </div>
            <div className="col-md-6 pb-1">
              <Label>No. Batch</Label>
              <Input
                placeholder="No. Batch: 2019-2-213-2"
                style={{ width: "100%" }}
                name="batchCode"
                value={batchCode}
                onChange={({ target }) => onChange(idx, "batchCode", target.value)}
                required
              />
            </div>
            <div className="col-md-6 pb-1">
              <Label>Tanggal Kadaluarsa</Label>
              <DatePicker
                placeholder="Tanggal Kadaluarsa"
                style={{ width: "100%" }}
                name="expiredDate"
                allowClear={false}
                disabledDate={_disabledDate}
                value={expiredDate ? moment(expiredDate) : null}
                onChange={date => onChange(idx, "expiredDate", date.toISOString())}
                required
              />
            </div>
            <div className="col-md-6 pb-1">
              <Label>Jumlah</Label>
              <InputNumber
                placeholder="Contoh: 100"
                style={{ width: "100%" }}
                name="packageQuantity"
                value={packageQuantity}
                min={1}
                onChange={value => onChange(idx, "packageQuantity", value)}
                required
              />
            </div>
            <div className="col-md-6 pb-1">
              <Label>Satuan</Label>
              <Input
                placeholder="Satuan"
                style={{ width: "100%" }}
                name="satuan"
                value={unit}
                required
              />

            </div>
            <div className="col-md-6 pb-1">
              <Label>Sumber Dana</Label>
              <Select
                showSearch
                optionFilterProp="children"
                // notFoundContent={isLoading ? <Spin size="small" /> : null}
                placeholder="APBN-2019"
                style={{ width: "100%" }}
                name="fundSource"
                value={fundSource}
                onChange={value => onChange(idx, "fundSource", value)}
                required
              >
                <Option disabled value={null}>
                  Pilih Sumber Dana
                </Option>
                {sumberDana.map(item => {
                  if (distType === TAG_LABEL.DIST_TYPE.PEMBELIAN_APBD && (item.includes('APBD') || item.includes('BLUD'))) {
                    return (
                      <Option key={item} value={item} title={item}>
                        {item}
                      </Option>
                    )
                  } else if (distType === TAG_LABEL.DIST_TYPE.PEMBELIAN_APBN && (item.includes('APBN') || item.includes('DAK'))) {
                    return (
                      <Option key={item} value={item} title={item}>
                        {item}
                      </Option>
                    )
                  } else if (distType === TAG_LABEL.DIST_TYPE.HIBAH_BANTUAN && (item.includes('GF'))) {
                    return (
                      <Option key={item} value={item} title={item}>
                        {item}
                      </Option>
                    )
                  } else if (distType === TAG_LABEL.DIST_TYPE.PEMBELIAN_MANDIRI && (item.includes('Pembelian Mandiri'))) {
                    return (
                      <Option key={item} value={item} title={item}>
                        {item}
                      </Option>
                    )
                  }
                })}
              </Select>
              {/* <Input
                placeholder="APBN-2019"
                style={{ width: "100%" }}
                name="fundSource"
                value={fundSource}
                onChange={({ target }) => onChange(idx, "fundSource", target.value)}
                required
              /> */}
            </div>
            <div className="col-md-6 pb-1">
              <Label>Produsen</Label>
              <Select
                showSearch
                optionFilterProp="children"
                // notFoundContent={isLoading ? <Spin size="small" /> : null}
                placeholder="Produsen"
                style={{ width: "100%" }}
                name="manufacture"
                value={manufacture}
                onChange={value =>
                  onChange(idx, "manufacture", value)
                }
                required
              >
                <Option disabled value={null}>
                  Pilih Produsen
                </Option>
                {produsen.map(item => (
                  <Option key={item.name} value={item.name} title={item.name}>
                    {item.name}
                  </Option>
                ))}
              </Select>
              {/* <Input
                placeholder="Produsen"
                style={{ width: "100%" }}
                name="manufacture"
                value={manufacture}
                onChange={({ target }) =>
                  onChange(idx, "manufacture", target.value)
                }
                required
              /> */}
            </div>
            <div className="col-md-6 pb-1">
              <Label>Harga per Satuan</Label>
              <InputNumber
                placeholder="Harga per Satuan"
                style={{ width: "100%" }}
                name="packagePrice"
                value={packagePrice}
                min={1}
                onChange={value => onChange(idx, "packagePrice", value)}
                required
              />
            </div>
          </div>
        </fieldset>
      </div>
    </React.Fragment>
  );
};

// const produsen = [
//   'Abbott',
//   'Aurobindo',
//   'BD',
//   'Bharat Serums and Vaccines Limited',
//   'Cipla',
//   'Combiphar',
//   'Edurant',
//   'Indec Diagnostic',
//   'Kimia Farma',
//   'Labmate',
//   'LFA',
//   'Strides Pharma Science',
//   'Mylan',
//   'Micro',
//   'Strides Pharma Science',
//   'PT Sampharindo',
//   'Pharos',
//   'Mersi Farma',
//   'Thai Nippon Rubber Industry Co.,I.td',
//   'Terumo (Philippines)',
//   'Standard Diagnostcs',
//   'Shanghai Kehua Bio Engineering',
//   'Zhejiang Orient Gene Biotech ',
//   'Medquest',
// ];