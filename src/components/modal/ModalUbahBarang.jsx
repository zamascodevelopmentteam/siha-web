import React from "react";
// import moment from "moment";
import { Button, Modal, InputNumber } from "antd";
import { Label, LabelInline } from "./style";
import {
  // ChooseDestination,
  // ChooseStock,
  // ChooseBrand,
  // ChooseMedicine,
  DisplayDate
} from "components";
// import { ROLE, ENUM } from "constant";

// const { Option } = Select;
// const { FUND_SOURCE } = ENUM;

export const ModalUbahBarang = ({
  onOk,
  onCancel,
  isLoading,
  role,
  data,
  onChange
}) => {
  // const _handleChange = e => {
  //   onChange(e.target.name, e.target.value);
  // };

  const _handleSubmit = e => {
    e.preventDefault();
    if (isLoading) {
      return;
    }
    onOk();
  };

  return (
    <Modal
      title="Ubah Barang"
      visible
      width="1000px"
      onOk={onOk}
      maskClosable={false}
      onCancel={onCancel}
      footer={null}
    >
      <form onSubmit={_handleSubmit}>
        {data.map((dist, idxDist) => (
          <React.Fragment>
            <div className="row mb-2">
              <div className="col-12 text-center mb-3">
                Data Barang {dist.medicine.name}
              </div>
              <div className="col-2">
                <LabelInline>Nama Brand</LabelInline>
              </div>
              <div className="col-3">
                <Label>Batch Code</Label>
              </div>
              <div className="col-2">
                <Label>Expired Date</Label>
              </div>
              <div className="col-2">
                <Label>Jumlah Stock</Label>
              </div>
              <div className="col-3">
                <Label>Quantity</Label>
              </div>
            </div>
            {dist.pickings.map((item, idxPicking) => {
              return (
                <ItemRow
                  key={item.id}
                  idx={idxPicking}
                  idxDist={idxDist}
                  brandName={item.brand.brandName}
                  batchCode={item.batchCode}
                  expiredDate={item.expiredDate}
                  packageQuantity={item.packageQuantity}
                  packageUnitType={item.packageUnitType}
                  onChange={onChange}
                />
              );
            })}
          </React.Fragment>
        ))}

        <div className="d-flex justify-content-end mt-2">
          <Button key="back" type="danger" onClick={onCancel} className="mr-2">
            Batal
          </Button>
          <Button
            key="submit"
            type="primary"
            className={`hs-btn ${role}`}
            loading={isLoading}
            htmlType="submit"
          >
            Simpan
          </Button>
        </div>
      </form>
    </Modal>
  );
};

const ItemRow = ({
  idx,
  idxDist,
  brandName,
  batchCode,
  expiredDate,
  packageQuantity,
  packageUnitType,
  onChange
}) => {
  return (
    <div className="row">
      <div className="col-2">{brandName}</div>
      <div className="col-3">{batchCode}</div>
      <div className="col-2">
        <DisplayDate date={expiredDate} />
      </div>
      <div className="col-2">
        {packageQuantity} {packageUnitType}
      </div>
      <div className="col-3">
        <InputNumber
          placeholder="Contoh: 100"
          style={{ width: "70%" }}
          className="mb-3"
          name="packageQuantity"
          value={packageQuantity}
          onChange={value => onChange(idxDist, idx, value)}
          required
        />
        {packageUnitType}
      </div>
    </div>
  );
};
