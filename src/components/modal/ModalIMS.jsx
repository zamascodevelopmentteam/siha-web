import React from "react";
import { Button, Modal, Radio, InputNumber } from "antd";
import { Label } from "./style";
// import { ROLE } from "constant";
import { ChooseReagen } from "components";

export const ModalIMS = ({
  isAdd,
  onOk,
  onCancel,
  isLoading,
  role,
  data,
  onChange
}) => {
  const _handleChange = e => {
    onChange(e.target.name, e.target.value);
  };

  const _handleSelectChange = value => {
    onChange("namaReagen", value);
  };

  const _handleSubmit = e => {
    e.preventDefault();
    if (isLoading) {
      return;
    }
    onOk();
  };
  return (
    <Modal
      title={
        isAdd ? "Tambah Data Pemeriksaan IMS" : "Ubah Data Pemeriksaan IMS"
      }
      visible
      // maskClosable={role === ROLE.RR_STAFF}
      onOk={onOk}
      onCancel={onCancel}
      footer={null}
    >
      <form onSubmit={_handleSubmit}>
        <React.Fragment>
          <Label>Di-Test IMS</Label>
          <Radio.Group
            className="mb-3"
            value={data.ditestIms}
            name="ditestIms"
            onChange={_handleChange}
            required
          >
            <Radio value={true}>Ya</Radio>
            <Radio value={false}>Tidak</Radio>
          </Radio.Group>

          {data.ditestIms && (
            <React.Fragment>
              <Label>Hasil Test IMS</Label>
              <Radio.Group
                className="mb-3"
                value={data.hasilTestIms}
                name="hasilTestIms"
                onChange={_handleChange}
                required
              >
                <Radio value={true}>Positif</Radio>
                <Radio value={false}>Negatif</Radio>
              </Radio.Group>

              <Label>Di-Test Sifilis</Label>
              <Radio.Group
                className="mb-3"
                name="ditestSifilis"
                onChange={_handleChange}
                value={data.ditestSifilis}
                required
              >
                <Radio value={true}>Ya</Radio>
                <Radio value={false}>Tidak</Radio>
              </Radio.Group>

              {data.ditestSifilis && (
                <React.Fragment>
                  <Label>Reagen Test</Label>
                  <ChooseReagen
                    placeholder="Pilih Reagen"
                    style={{ width: "100%" }}
                    className="mb-3"
                    value={data.namaReagen}
                    name="namaReagen"
                    onChange={_handleSelectChange}
                    showDetail
                    isNotNol
                    required
                    reagenIms
                  />
                  <Label>Jumlah Reagen yg digunakan</Label>
                  <InputNumber
                    placeholder="Masukan Jumlah Reagen yg digunakan"
                    style={{ width: "100%" }}
                    className="mb-3"
                    name={"qtyReagen"}
                    value={data.qtyReagen}
                    onChange={value => onChange("qtyReagen", value)}
                    required
                  />

                  <Label>Hasil Test</Label>
                  <Radio.Group
                    className="mb-3"
                    defaultValue={true}
                    name="hasilTestSifilis"
                    onChange={_handleChange}
                    value={data.hasilTestSifilis}
                    required
                  >
                    <Radio value={true}>Reaktif</Radio>
                    <Radio value={false}>Non-Reaktif</Radio>
                  </Radio.Group>
                </React.Fragment>
              )}
            </React.Fragment>
          )}
        </React.Fragment>
        <div className="d-flex justify-content-end mt-2">
          <Button key="back" type="danger" onClick={onCancel} className="mr-2">
            Batal
          </Button>
          <Button
            key="submit"
            type="primary"
            className={`hs-btn ${role}`}
            loading={isLoading}
            htmlType="submit"
          >
            Simpan
          </Button>
        </div>
      </form>
    </Modal>
  );
};
