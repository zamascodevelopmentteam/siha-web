import React, { useRef, useState } from "react";
import { Select, Input } from "antd";
import { Button, DatePicker, InputNumber, Modal, Radio } from "antd";
import { Label } from "./style";
import { ENUM, ROLE } from "constant";
import { ChooseReagen, Typography, PrintResultTest } from "components";
import moment from "moment";
import ReactToPrint from "react-to-print";
import { validateForm } from "utils/validator";

const { Option } = Select;
const {
  ALASAN_TEST,
  KEL_POPULASI,
  HIV_TEST_TYPE,
  TEST_RESULT,
  REAGEN_RESULT,
  TEST_RESULT_DNA,
  TEST_RESULT_RNA,
  RUJUKAN_LAB_LANJUT
} = ENUM;


export const ModalHIV = ({
  isAdd,
  onOk,
  onCancel,
  isLoading,
  role,
  data,
  onChange,
  result,
  disableReagen,
  dataPasien,
  gender,
  addTest,
  deleteTest,
  changeTestValue
}) => {
  const [paralelCheck, setParalelCheck] = useState(null);
  const printComponent = useRef();
  // const fields = getRequiredFields(data, role, paralelCheck)

  const _handleChange = e => {
    onChange(e.target.name, e.target.value);
  };

  const _handleChangeResult = e => {
    const { name, value } = e.target;
    onChange(name, value);
  };

  const _handleSubmit = e => {
    e.preventDefault();

    // return console.log(data);
    // let fields;
    // if (data.jenisTest === "RDT" || paralelCheck === "PARALEL") {
    //   
    // }
    // if (data.hasilTestR1 === "REAKTIF" || paralelCheck === "PARALEL") {
    //   fields = {
    //     ...fields,
    //     namaReagenR2: "Reagen R2",
    //     hasilTestR2: "Hasil Tes Reagen R2"
    //   };
    // }
    // if (data.hasilTestR2 === "REAKTIF" || paralelCheck === "PARALEL") {
    //   fields = {
    //     ...fields,
    //     namaReagenR3: "Reagen R3",
    //     hasilTestR3: "Hasil Tes Reagen R3"
    //   };
    // }
    // if (data.kesimpulanHiv) {
    //   fields = {
    //     ...fields,
    //     jenisTest: "Jenis Test"
    //   };
    // }

    // if (fields) {
    //   const validated = validateForm(data, fields);
    //   if (!validated) {
    //     return
    //   }
    // }

    for (let i = 0; i < data.test.length; i++) {
      const r = data.test[i];
      let fields = {
        type: "Jenis Test",
        name: "Reagen",
        qty: "Jumlah Reagen yg digunakan",
        result: "Hasil Test"
      };

      const validated = validateForm(r, fields);
      if (!validated) {
        return
      }
    }

    if (isLoading) {
      return;
    }
    onOk();
  };

  // const _filterKelomokPopulation = (gender, KEL_POPULASI) => {
  //   if (!gender) return KEL_POPULASI

  //   let obj = {}

  //   if (gender === "LAKI-LAKI") {
  //     for (let key in KEL_POPULASI) {
  //       if (key !== "WPS" && key !== "BUMIL") {
  //         obj[key] = KEL_POPULASI[key]
  //       }
  //     }
  //   } else {
  //     for (let key in KEL_POPULASI) {
  //       if (key !== "WARIA" && key !== "LSL") {
  //         obj[key] = KEL_POPULASI[key]
  //       }
  //     }
  //   }

  //   return obj
  // }

  const _filterTestWBElisa = (TEST_RESULT) => {
    let obj = {}

    for (let key in TEST_RESULT) {
      if (key !== "INKONKLUSIF") {
        obj[key] = TEST_RESULT[key]
      }
    }

    return obj
  }

  return (
    <Modal
      title={
        isAdd ? "Tambah Data Pemeriksaan HIV" : "Ubah Data Pemeriksaan HIV"
      }
      style={{ top: 30 }}
      visible
      // maskClosable={role === ROLE.RR_STAFF}
      onOk={onOk}
      onCancel={onCancel}
      footer={null}
    >
      <form onSubmit={_handleSubmit}>
        <div hidden>
          <PrintResultTest
            title={"Hasil Lab"}
            dataHiv={data}
            printComponent={printComponent}
            dataPasien={dataPasien}
          ></PrintResultTest>
        </div>
        {role !== ROLE.LAB_STAFF && (
          <React.Fragment>
            {/* <Label>
              Kelompok Populasi{" "}
              <span className="text-danger">*Harus Diisi</span>
            </Label>
            <Select
              placeholder="Pilih Kelompok Populasi"
              style={{ width: "100%" }}
              className="mb-3"
              name="kelompokPopulasi"
              required
              value={data.kelompokPopulasi || null}
              onChange={value => onChange("kelompokPopulasi", value)}
            >
              <Option disabled value={null}>
                Pilih Kelompok Populasi
              </Option>
              {Object.keys(_filterKelomokPopulation(gender, KEL_POPULASI)).map(key => (
                <Option key={KEL_POPULASI[key]} value={KEL_POPULASI[key]}>
                  {KEL_POPULASI[key]}
                </Option>
              ))}
            </Select> */}
            {[
              KEL_POPULASI.WPS,
              KEL_POPULASI.WARIA,
              KEL_POPULASI.PENASUN,
              KEL_POPULASI.LSL
            ].includes(data.kelompokPopulasi) && (
                <React.Fragment>
                  <Label>
                    LSM Penjangkau{" "}
                    {/* <span className="text-danger">*Harus Diisi</span> */}
                  </Label>
                  <Input
                    placeholder="Masukan LSM Penjangkau"
                    style={{ width: "100%" }}
                    className="mb-3"
                    name="lsmPenjangkau"
                    value={data.lsmPenjangkau}
                    onChange={_handleChange}
                  />
                </React.Fragment>
              )}

            <Label>
              Alasan Test HIV <span className="text-danger">*Harus Diisi</span>
            </Label>
            <Select
              placeholder="Pilih Alasan"
              style={{ width: "100%" }}
              className="mb-3"
              name="alasanTest"
              value={data.alasanTest || null}
              onChange={value => onChange("alasanTest", value)}
              required
            >
              <Option disabled value={null}>
                Pilih Alasan
              </Option>
              {Object.keys(ALASAN_TEST).map(key => (
                <Option key={ALASAN_TEST[key]} value={ALASAN_TEST[key]}>
                  {ALASAN_TEST[key]}
                </Option>
              ))}
            </Select>
          </React.Fragment>
        )}

        {role !== ROLE.RR_STAFF && (
          <React.Fragment>
            <Label>
              Tanggal Test <span className="text-danger">*Harus Diisi</span>
            </Label>
            <DatePicker
              placeholder="Pilih tanggal"
              style={{ width: "100%" }}
              allowClear={false}
              className="mb-3"
              name="tanggalTest"
              value={data.tanggalTest ? moment(data.tanggalTest) : null}
              onChange={date => onChange("tanggalTest", date.toISOString())}
              required
            />

            <Typography
              fontSize="14px"
              fontWeight="bold"
              className="d-block mb-3"
            >
              Test HIV
            </Typography>

            <Label>
              Jenis Test <span className="text-danger">*Harus Diisi</span>
            </Label>
            <Select
              placeholder="Pilih Test"
              style={{ width: "100%" }}
              className="mb-3"
              name="jenisTest"
              value={data.jenisTest || null}
              onChange={value => onChange("jenisTest", value)}
              required
              disabled={disableReagen.disablejenisTest}
            >
              <Option disabled value={null}>
                Pilih Jenis Test
              </Option>
              {Object.keys(HIV_TEST_TYPE).map(key => (
                <Option key={HIV_TEST_TYPE[key]} value={HIV_TEST_TYPE[key]}>
                  {HIV_TEST_TYPE[key]}
                </Option>
              ))}
            </Select>

            {data.jenisTest === HIV_TEST_TYPE.RDT && (
              <React.Fragment>
                {/* <Label>
                  Pengecekan Paralel Test{" "}
                  <span className="text-danger">*Harus Diisi</span>
                </Label>
                <Select
                  placeholder="Pilih Paralel Test"
                  style={{ width: "100%" }}
                  className="mb-3"
                  name="paralelCheck"
                  value={paralelCheck}
                  onChange={value => setParalelCheck(value)}
                  disabled={
                    disableReagen.disablenamaReagenR1 ||
                    disableReagen.disablenamaReagenR2 ||
                    disableReagen.disablenamaReagenR3
                  }
                  required
                >
                  <Option disabled value={null}>
                    Pilih Pengecekan Paralel
                  </Option>
                  {Object.keys(ENUM.PARALEL_CHECK).map(
                    key =>
                      // !(role === ROLE.LAB_STAFF && key === "PARALEL_3") && (
                        <Option key={ENUM.PARALEL_CHECK[key]} value={key}>
                          {ENUM.PARALEL_CHECK[key]}
                        </Option>
                      // )
                  )}
                </Select> */}

                <Button className="mb-3" onClick={addTest} size="small" type="primary">Tambah Test</Button>
                {/* <Button
              key="submit"
              type="primary"
              className={`hs-btn ${role}`}
              loading={isLoading}
              htmlType="submit"
            // onClick={() => validateForm(data, fields)}
            >
              Simpan
            </Button> */}

                {data.test && data.test.map((r, i) => {
                  return (
                    <React.Fragment>
                      <Label className="pt-2  border-top">
                        Jenis Test <span className="text-danger">*Harus Diisi</span>
                        {i != 0 && <Button className="ml-3" onClick={() => deleteTest(i)} size="small" type="danger">Hapus Test</Button>}
                      </Label>
                      <Select
                        placeholder="Pilih Jenis Test"
                        style={{ width: "100%" }}
                        className="mb-3"
                        name="type"
                        value={r.type}
                        onChange={value => changeTestValue(i, 'type', value)}
                        required
                      >
                        <Option disabled value={null}>Pilih Jenis Test</Option>
                        {[0, 2].includes(i) && <Option value={'R1'}>R1</Option>}
                        {[1, 3].includes(i) && <Option value={'R2'}>R2</Option>}
                        {i >= 2 && <Option value={'R3'}>R3</Option>}
                      </Select>
                      <Label>
                        Reagen <span className="text-danger">*Harus Diisi</span>
                      </Label>
                      <ChooseReagen
                        placeholder="Pilih Reagen"
                        style={{ width: "100%" }}
                        className="mb-3"
                        value={r.name}
                        name="namaReagenR1"
                        onChange={value => changeTestValue(i, 'name', value)}
                        showDetail
                        isNotNol
                        required
                        // disabled={disableReagen.disablenamaReagenR1}
                        reagenHiv
                      />
                      <Label>
                        Jumlah Reagen yg digunakan{" "}
                        <span className="text-danger">*Harus Diisi</span>
                      </Label>
                      <InputNumber
                        placeholder="Masukan Jumlah Reagen yg digunakan"
                        style={{ width: "100%" }}
                        className="mb-3"
                        name="qtyReagenR1"
                        max="9"
                        min="0"
                        value={r.qty}
                        onChange={value => changeTestValue(i, 'qty', value)}
                        required
                      // disabled={disableReagen.disableqtyReagenR1}
                      />

                      <Label>
                        Hasil Test{" "}
                        <span className="text-danger">*Harus Diisi</span>
                      </Label>
                      <Radio.Group
                        className="mb-3"
                        name="hasilTestR1"
                        value={r.result}
                        onChange={e => changeTestValue(i, 'result', e.target.value)}
                        required
                      >
                        {Object.keys(REAGEN_RESULT).map(key => (
                          <Radio key={REAGEN_RESULT[key]} value={REAGEN_RESULT[key]}>
                            {REAGEN_RESULT[key]}
                          </Radio>
                        ))}
                      </Radio.Group>
                    </React.Fragment>
                  )
                })}
              </React.Fragment>
            )}

            {data.jenisTest === HIV_TEST_TYPE.PCR_RNA && (
              <React.Fragment>
                <Label className="pt-2  border-top">
                  Reagen RNA <span className="text-danger">*Harus Diisi</span>
                </Label>
                <ChooseReagen
                  placeholder="Pilih Reagen"
                  style={{ width: "100%" }}
                  className="mb-3"
                  value={data.namaReagenRna}
                  name="namaReagenRna"
                  onChange={value => onChange("namaReagenRna", value)}
                  required
                  disabled={disableReagen.disablenamaReagenRna}
                  jenisTest={data.jenisTest}
                  isNotNol
                  reagenHiv
                />

                <Label>
                  Jumlah Reagen RNA yg digunakan{" "}
                  <span className="text-danger">*Harus Diisi</span>
                </Label>
                <InputNumber
                  placeholder="Masukan Jumlah Reagen RNA yg digunakan"
                  style={{ width: "100%" }}
                  className="mb-3"
                  max="9"
                  min="0"
                  name={"qtyReagenRna"}
                  value={data.qtyReagenRna}
                  onChange={value => onChange("qtyReagenRna", value)}
                  required
                  disabled={disableReagen.disableqtyReagenRna}
                />

                <Label>
                  Hasil Test RNA{" "}
                  <span className="text-danger">*Harus Diisi</span>
                </Label>
                <Radio.Group
                  className="mb-3"
                  name="hasilTestRna"
                  value={data.hasilTestRna}
                  onChange={_handleChangeResult}
                  required
                >
                  {Object.keys(TEST_RESULT_RNA).map(key => (
                    <Radio
                      key={TEST_RESULT_RNA[key]}
                      value={TEST_RESULT_RNA[key]}
                    >
                      {TEST_RESULT_RNA[key]}
                    </Radio>
                  ))}
                </Radio.Group>
              </React.Fragment>
            )}

            {data.jenisTest === HIV_TEST_TYPE.PCR_DNA && (
              <React.Fragment>
                <Label className="pt-2  border-top">
                  Reagen DNA <span className="text-danger">*Harus Diisi</span>
                </Label>
                <ChooseReagen
                  placeholder="Pilih Reagen"
                  style={{ width: "100%" }}
                  className="mb-3"
                  value={data.namaReagenDna}
                  name="namaReagenDna"
                  onChange={value => onChange("namaReagenDna", value)}
                  required
                  disabled={disableReagen.disablenamaReagenDna}
                  jenisTest={data.jenisTest}
                  isNotNol
                  reagenHiv
                />

                <Label>
                  Jumlah Reagen DNA yg digunakan{" "}
                  <span className="text-danger">*Harus Diisi</span>
                </Label>
                <InputNumber
                  placeholder="Masukan Jumlah Reagen DNA yg digunakan"
                  style={{ width: "100%" }}
                  className="mb-3"
                  max="9"
                  min="0"
                  value={data.qtyReagenDna}
                  name={"qtyReagenDna"}
                  onChange={value => onChange("qtyReagenDna", value)}
                  required
                  disabled={disableReagen.disableqtyReagenDna}
                />

                <Label>
                  Hasil Test DNA{" "}
                  <span className="text-danger">*Harus Diisi</span>
                </Label>
                <Radio.Group
                  className="mb-3"
                  name="hasilTestDna"
                  value={data.hasilTestDna}
                  onChange={_handleChangeResult}
                  required
                >
                  {Object.keys(TEST_RESULT_DNA).map(key => (
                    <Radio
                      key={TEST_RESULT_DNA[key]}
                      value={TEST_RESULT_DNA[key]}
                    >
                      {TEST_RESULT_DNA[key]}
                    </Radio>
                  ))}
                </Radio.Group>
              </React.Fragment>
            )}

            {/* {data.jenisTest === HIV_TEST_TYPE.NAT && ( */}
            {/* <React.Fragment>
            <Label className="pt-2  border-top">
              Reagen NAT <span className="text-danger">*Harus Diisi</span>
            </Label>
            <ChooseReagen
              placeholder="Pilih Reagen"
              style={{ width: "100%" }}
              className="mb-3"
              value={data.namaReagenNat}
              name="namaReagenNat"
              onChange={value => onChange("namaReagenNat", value)}
              required
            />

            <Label>
              Jumlah Reagen NAT yg digunakan{" "}
              <span className="text-danger">*Harus Diisi</span>
            </Label>
            <InputNumber
              placeholder="Masukan Jumlah Reagen NTA yg digunakan"
              style={{ width: "100%" }}
              className="mb-3"
              max="9"
              min="0"
              value={data.qtyReagenNta}
              name={"qtyReagenNta"}
              onChange={value => onChange("qtyReagenNta", value)}
              required
            />

            <Label>
              Hasil Test NAT <span className="text-danger">*Harus Diisi</span>
            </Label>
            <Radio.Group
              className="mb-3"
              name="hasilTestNat"
              value={data.hasilTestNat}
              onChange={_handleChangeResult}
              required
            >
              {Object.keys(TEST_RESULT).map(key => (
                <Radio key={TEST_RESULT[key]} value={TEST_RESULT[key]}>
                  {TEST_RESULT[key]}
                </Radio>
              ))}
            </Radio.Group>
          </React.Fragment> */}
            {/* )} */}

            {/* <Typography
              fontSize="14px"
              fontWeight="bold"
              className="d-block mb-3"
            >
              Pemeriksaan Lab Lanjutan
            </Typography>

            <Label>Rujukan untuk Pemeriksaan Lab Lanjutan</Label>
            <Select
              placeholder="Pilih Rujukan Lab Lanjutan"
              style={{ width: "100%" }}
              className="mb-3"
              name="rujukanLabLanjut"
              value={data.rujukanLabLanjut}
              onChange={value => onChange("rujukanLabLanjut", value)}
              allowClear
              disabled={disableReagen.disablerujukanLabLanjut}
            >
              <Option disabled value={null}>
                Pilih Rujukan Lab Lanjutan
              </Option>
              {Object.keys(RUJUKAN_LAB_LANJUT).map(key => (
                <Option
                  key={RUJUKAN_LAB_LANJUT[key]}
                  value={RUJUKAN_LAB_LANJUT[key]}
                >
                  {RUJUKAN_LAB_LANJUT[key]}
                </Option>
              ))}
            </Select> */}

            {data.jenisTest === HIV_TEST_TYPE.WB && (
              <React.Fragment>
                <Label>Nama Reagen WB</Label>
                <ChooseReagen
                  placeholder="Pilih Reagen"
                  style={{ width: "100%" }}
                  className="mb-3"
                  value={data.namaReagenWb}
                  name="namaReagenWb"
                  onChange={value => onChange("namaReagenWb", value)}
                  disabled={disableReagen.disablenamaReagenWb}
                />

                <Label>
                  Jumlah Reagen WB yg digunakan{" "}
                  <span className="text-danger">*Harus Diisi</span>
                </Label>
                <InputNumber
                  placeholder="Masukan Jumlah Reagen WB yg digunakan"
                  style={{ width: "100%" }}
                  className="mb-3"
                  max="9"
                  min="0"
                  name={"qtyReagenWb"}
                  value={data.qtyReagenWb}
                  onChange={value => onChange("qtyReagenWb", value)}
                  required
                  disabled={disableReagen.disableqtyReagenWb}
                />

                <Label>Hasil Test WB</Label>
                <Radio.Group
                  className="mb-3"
                  name="hasilTestWb"
                  value={data.hasilTestWb}
                  onChange={_handleChangeResult}
                >
                  {Object.keys(_filterTestWBElisa(TEST_RESULT)).map(key => (
                    <Radio key={TEST_RESULT[key]} value={TEST_RESULT[key]}>
                      {TEST_RESULT[key]}
                    </Radio>
                  ))}
                </Radio.Group>
              </React.Fragment>
            )}
            {data.jenisTest === HIV_TEST_TYPE.ELISA && (
              <React.Fragment>
                <Label>Nama Reagen ELISA</Label>
                <ChooseReagen
                  placeholder="Pilih Reagen"
                  style={{ width: "100%" }}
                  className="mb-3"
                  value={data.namaReagenElisa}
                  name="namaReagenElisa"
                  onChange={value => onChange("namaReagenElisa", value)}
                  disabled={disableReagen.disablenamaReagenElisa}
                />

                <Label>
                  Jumlah Reagen ELISA yg digunakan{" "}
                  <span className="text-danger">*Harus Diisi</span>
                </Label>
                <InputNumber
                  placeholder="Masukan Jumlah Reagen ELISA yg digunakan"
                  style={{ width: "100%" }}
                  className="mb-3"
                  max="9"
                  min="0"
                  name={"qtyReagenElisa"}
                  value={data.qtyReagenElisa}
                  onChange={value => onChange("qtyReagenElisa", value)}
                  required
                  disabled={disableReagen.disableqtyReagenElisa}
                />

                <Label>Hasil Test ELISA</Label>
                <Radio.Group
                  className="mb-3"
                  name="hasilTestElisa"
                  value={data.hasilTestElisa}
                  onChange={_handleChangeResult}
                >
                  {Object.keys(_filterTestWBElisa(TEST_RESULT)).map(key => (
                    <Radio key={TEST_RESULT[key]} value={TEST_RESULT[key]}>
                      {TEST_RESULT[key]}
                    </Radio>
                  ))}
                </Radio.Group>
              </React.Fragment>
            )}
            <Typography
              fontSize="14px"
              fontWeight="bold"
              className="d-block mb-3"
            >
              Kesimpulan Hasil Test HIV
            </Typography>
            <Label>Kesimpulan Hasil Test HIV</Label>
            <Radio.Group
              className="mb-3"
              name="kesimpulanHiv"
              value={data.kesimpulanHiv}
              onChange={_handleChangeResult}
            >
              <Radio key="-" value={null}>
                -
              </Radio>
              {Object.keys(TEST_RESULT).map(key => (
                <Radio key={TEST_RESULT[key]} value={TEST_RESULT[key]}>
                  {TEST_RESULT[key]}
                </Radio>
              ))}
            </Radio.Group>
          </React.Fragment>
        )}

        <div className="row col-12 p-0 mt-4">
          <div className="col-6 d-flex justify-content-left">
            {/* {!isAdd && (
              <ReactToPrint
                className="mr-2 text-left"
                trigger={() => (
                  <Button type="primary" icon="printer">
                    Print Hasil Lab
                  </Button>
                )}
                content={() => printComponent.current}
                bodyClass="p-5"
              />
            )} */}
          </div>
          <div className="col-6 p-0 d-flex justify-content-end">
            <Button
              key="back"
              type="danger"
              onClick={onCancel}
              className="ml-2 mr-2"
            >
              Batal
            </Button>
            <Button
              key="submit"
              type="primary"
              className={`hs-btn ${role}`}
              loading={isLoading}
              htmlType="submit"
            // onClick={() => validateForm(data, fields)}
            >
              Simpan
            </Button>
          </div>
        </div>
      </form>
    </Modal>
  );
};


// function getRequiredFields(data, role, paralelCheck) {
//   let reqFields = {}
//   if (role !== ROLE.LAB_STAFF) {
//     reqFields = {
//       kelompokPopulasi: "Kelompok Populasi",
//       alasanTest: "Alasan Test HIV",
//     }
//   }
//   if (role !== ROLE.RR_STAFF) {
//     reqFields = {
//       tanggalTest: "Tanggal Test",
//       jenisTest: "Jenis Test",
//       kesimpulanHiv: "Kesimpulan Hasil Tes",
//     }
//     if (data.jenisTest === HIV_TEST_TYPE.RDT) {
//       reqFields = {
//         ...reqFields,
//         namaReagenR1: "Pilih Reagen R1",
//         qtyReagenR1: "Jumlah Reagen R1",
//         hasilTestR1: "Hasil Test R1",
//       }
//     }
//     if (data.jenisTest === HIV_TEST_TYPE.PCR_RNA) {
//       reqFields = {
//         ...reqFields,
//         namaReagenRna: "Pilih Reagen RNA",
//         hasilTestRna: "Jumlah Reagen RNA",
//         qtyReagenRna: "Hasil Test RNA",
//       }
//     }
//     if (data.jenisTest === HIV_TEST_TYPE.PCR_DNA) {
//       reqFields = {
//         ...reqFields,
//         namaReagenDna: "Pilih Reagen DNA",
//         qtyReagenDna: "Jumlah Reagen DNA",
//         hasilTestDna: "Hasil Test DNA",
//       }
//     }
//     if (data.hasilTestR1 === REAGEN_RESULT.REAKTIF || paralelCheck === "PARALEL_2" || paralelCheck === "PARALEL_3") {
//       reqFields = {
//         ...reqFields,
//         namaReagenR2: "Pilih Reagen R2",
//         qtyReagenR2: "Jumlah Reagen R2",
//         hasilTestR2: "Hasil Test R2",
//       }
//     }
//     if (data.hasilTestR2 === REAGEN_RESULT.REAKTIF || paralelCheck === "PARALEL_3") {
//       reqFields = {
//         ...reqFields,
//         namaReagenR3: "Pilih Reagen R3",
//         qtyReagenR3: "Jumlah Reagen R3",
//         hasilTestR3: "Hasil Test R3",
//       }
//     }
//     if (data.rujukanLabLanjut === RUJUKAN_LAB_LANJUT.WB) {
//       reqFields = {
//         ...reqFields,
//         // namaReagenElisa: "Nama Reagen WB",
//         qtyReagenWb: "Jumlah Reagen WB",
//         // hasilTestWb: "Hasil Test WB",
//       }
//     }
//     if (data.rujukanLabLanjut === RUJUKAN_LAB_LANJUT.ELISA) {
//       reqFields = {
//         ...reqFields,
//         // namaReagenElisa: "Nama Reagen ELISA",
//         qtyReagenElisa: "Jumlah Reagen ELISA",
//         // hasilTestElisa: "Hasil Test ELISA",
//       }
//     }
//   }
//   return reqFields
// }