import React from "react";
// import moment from "moment";
import { Button, Modal, InputNumber } from "antd";
import { Label, LabelInline } from "./style";
import { Typography, DisplayDate } from "components";
// import { ROLE, ENUM } from "constant";

export const ModalKonfirmasiPenerimaanBarang = ({
  onOk,
  onCancel,
  isLoading,
  role,
  data,
  onChange
}) => {
  const _handleSubmit = e => {
    e.preventDefault();
    if (isLoading) {
      return;
    }
    onOk();
  };

  return (
    <Modal
      title="Konfirmasi Penerimaan Barang"
      visible
      width="1000px"
      onOk={onOk}
      maskClosable={false}
      onCancel={onCancel}
      footer={null}
    >
      <form onSubmit={_handleSubmit}>
        {data.distribution_plans_items.map((dist, idxDist) => (
          <React.Fragment key={dist.distPlanItemId}>
            <div className="row mb-2">
              <div className="col-12 text-center mb-3">
                <Typography fontSize="14px" fontWeight="bold">
                  Data Barang {dist.inventory.medicine.name}
                </Typography>
              </div>
              <div className="col-3">
                <LabelInline>Nama Brand</LabelInline>
              </div>
              <div className="col-2">
                <Label>Batch Code</Label>
              </div>
              <div className="col-2">
                <Label>Expired Date</Label>
              </div>
              <div className="col-2">
                <Label>Stock Dikirim</Label>
              </div>
              <div className="col-3">
                <Label>Stock Diterima</Label>
              </div>
            </div>

            <ItemRow
              idx={idxDist}
              brandName={dist.inventory.brand.name}
              batchCode={dist.inventory.batchCode}
              expiredDate={dist.expiredDate}
              packageQuantity={dist.packageQuantity}
              packageUnitType={dist.packageUnitType}
              onChange={onChange}
            />
          </React.Fragment>
        ))}

        <div className="d-flex justify-content-end mt-2">
          <Button key="back" type="danger" onClick={onCancel} className="mr-2">
            Batal
          </Button>
          <Button
            key="submit"
            type="primary"
            className={`hs-btn ${role}`}
            loading={isLoading}
            htmlType="submit"
          >
            Simpan
          </Button>
        </div>
      </form>
    </Modal>
  );
};

const ItemRow = ({
  idx,
  brandName,
  batchCode,
  expiredDate,
  packageQuantity,
  packageUnitType,
  onChange
}) => {
  return (
    <div className="row">
      <div className="col-3">{brandName}</div>
      <div className="col-2">{batchCode}</div>
      <div className="col-2">
        <DisplayDate date={expiredDate} />
      </div>
      <div className="col-2">
        {packageQuantity} {packageUnitType}
      </div>
      <div className="col-3">
        <InputNumber
          placeholder="Contoh: 100"
          style={{ width: "70%" }}
          className="mb-3 mr-2"
          name="packageQuantity"
          onChange={value => onChange(idx, value)}
          required
        />
        {packageUnitType}
      </div>
    </div>
  );
};
