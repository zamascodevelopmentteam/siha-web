import React, { useContext } from "react";
import { Select } from "antd";
import { Button, InputNumber, Modal, DatePicker } from "antd";
import { Label } from "./style";
import { ENUM } from "constant";
import { ChooseReagen } from "components";
import { validateForm } from "utils/validator";
import GlobalContext from "containers/GlobalContext";
import moment from "moment";

const { Option } = Select;
const { VL_TEST_TYPE } = ENUM;

export const ModalVLCD4 = ({
  isAdd,
  onOk,
  onCancel,
  isLoading,
  role,
  data,
  onChange
}) => {
  const user = useContext(GlobalContext);
  // const isEmpty = data.id === "";
  let reqFields = {}
  if (user.role === ENUM.USER_ROLE.RR_STAFF) {
    reqFields = {
      testVlCd4Type: "Pilih Test",
      reason: "Alasan Pemeriksaan",
      hasilTestVlCd4: "Hasil Pemeriksaan"
    }
  } else {
    reqFields = {
      testVlCd4Type: "Pilih Test",
      date: "Tanggal",
      namaReagen: "Reagen",
      // qtyReagen: "Jumlah Reagen",
    }
  }

  const _handleSubmit = e => {
    e.preventDefault();
    if (isLoading) {
      return;
    }
    // console.log(data);
    if (!validateForm(data, reqFields)) {
      return
    }
    if (data.hasilTestVlCd4 === ENUM.HASIL_TEST_VLCD4_NEW.INPUT_ANGKA) {
      data.hasilTestVlCd4 = data.hasilTestInNumber
    }
    onOk();
  };
  return (
    <Modal
      title={
        isAdd
          ? "Tambah Data Pemeriksaan VL/CD4"
          : "Ubah Data Pemeriksaan VL/CD4"
      }
      visible
      onOk={onOk}
      // maskClosable={role !== ROLE.LAB_STAFF}
      onCancel={onCancel}
      footer={null}
    >
      <form onSubmit={_handleSubmit}>
        <Label>
          Test <span className="text-danger">*Harus Diisi</span>
        </Label>
        <Select
          placeholder="Pilih Test"
          style={{ width: "100%" }}
          className="mb-3"
          value={data.testVlCd4Type || null}
          name="testVlCd4Type"
          onChange={value => onChange("testVlCd4Type", value)}
          required
        >
          <Option disabled value={null}>
            Pilih Test
            </Option>
          {Object.keys(VL_TEST_TYPE).map(key => (
            <Option key={VL_TEST_TYPE[key]} value={VL_TEST_TYPE[key]}>
              {VL_TEST_TYPE[key]}
            </Option>
          ))}
        </Select>

        {user.role === ENUM.USER_ROLE.RR_STAFF && (
          <React.Fragment>
            <Label>
              Alasan Pemeriksaan <span className="text-danger">*Harus Diisi</span>
            </Label>
            <Select
              style={{ width: "100%" }}
              className="mb-3"
              name="reason"
              value={data.reason || null}
              onChange={value => onChange("reason", value)}
            >
              <Option disabled value={null}>
                Pilih Alasan Pemeriksaan
              </Option>
              {Object.keys(ENUM.ALASAN_TEST_VLCD4).map(key => (
                <Option key={ENUM.ALASAN_TEST_VLCD4[key]} value={ENUM.ALASAN_TEST_VLCD4[key]}>
                  {ENUM.ALASAN_TEST_VLCD4[key]}
                </Option>
              ))}
            </Select>
          </React.Fragment>
        )}

        {user.role === ENUM.USER_ROLE.LAB_STAFF && (
          <React.Fragment>
            <Label>
              Tanggal Pemeriksaan <span className="text-danger">*Harus Diisi</span>
            </Label>
            <DatePicker
              placeholder="Masukkan Tanggal Pemeriksaan"
              style={{ width: "100%" }}
              allowClear={false}
              className="mb-3"
              name="date"
              value={data.date ? moment(data.date) : null}
              onChange={date =>
                onChange("date", date ? date.toISOString() : null)
              }
              required
            />

            <Label>
              Mesin <span className="text-danger">*Harus Diisi</span>
            </Label>
            <Select
              placeholder="Pilih Mesin"
              style={{ width: "100%" }}
              className="mb-3"
              name="reagentType"
              value={data.reagentType}
              onChange={value => onChange("reagentType", value)}
            >
              <Option value={null} disabled>
                Pilih Mesin
              </Option>
              {
                Object.keys(ENUM.VLCD4CATEGORY_OPTIONS).map(key => {
                  if (key === data.testVlCd4Type) {
                    return (
                      // <OptGroup label={key}>
                      // {
                      Object.keys(ENUM.VLCD4CATEGORY_OPTIONS[key]).map(kk => (
                        <Option key={ENUM.VLCD4CATEGORY[kk]} value={ENUM.VLCD4CATEGORY[kk]}>
                          {ENUM.VLCD4CATEGORY[kk]}
                        </Option>
                      ))
                      // }
                      // </OptGroup>
                    )
                  }
                })
              }
            </Select>

            <Label>
              Reagen <span className="text-danger">*Harus Diisi</span>
            </Label>
            <ChooseReagen
              placeholder="Pilih Reagen"
              style={{ width: "100%" }}
              className="mb-3"
              value={data.namaReagen || null}
              name="namaReagen"
              onChange={value => onChange("namaReagen", value)}
              required
              isNotNol={true}
              vlcd4={true}
              vlcd4Type={data.testVlCd4Type}
              vlcd4ReagentType={data.reagentType}
            />

            {/* <Label>
              Jumlah Reagen yang Digunakan <span className="text-danger">*Harus Diisi</span>
            </Label>
            <InputNumber
              placeholder="Masukkan Jumlah Reagen (Contoh: 2)"
              style={{ width: "100%" }}
              className="mb-3"
              value={data.qtyReagen}
              name="qtyReagen"
              onChange={value => onChange("qtyReagen", value)}
              required
            /> */}
          </React.Fragment>
        )}

        <Label>
          Hasil Pemeriksaan Lab (
            {data.testVlCd4Type === VL_TEST_TYPE.VL
            ? "dalam kopi/ml"
            : "dalam sel/mm"}
            )
          </Label>
        <Select
          style={{ width: "100%" }}
          className="mb-3"
          name="hasilTestVlCd4"
          value={data.hasilTestVlCd4 || null}
          onChange={value => onChange("hasilTestVlCd4", value)}
        >
          <Option disabled value={null}>
            Pilih Hasil Pemeriksaan Lab
            </Option>
          {Object.keys(ENUM.HASIL_TEST_VLCD4_NEW).map(key => {
            return (
              <Option key={ENUM.HASIL_TEST_VLCD4_NEW[key]} value={ENUM.HASIL_TEST_VLCD4_NEW[key]}>
                {ENUM.HASIL_TEST_VLCD4_NEW[key]}
              </Option>
            );
          })}
        </Select>
        {data.hasilTestVlCd4 === ENUM.HASIL_TEST_VLCD4_NEW.INPUT_ANGKA && (
          <React.Fragment>
            <Label>
              Hasil (
                {data.testVlCd4Type === VL_TEST_TYPE.VL
                ? "dalam kopi/ml"
                : "dalam sel/mm"}
                )
              </Label>
            <InputNumber
              placeholder="Masukkan Hasil Pemeriksaan (Contoh: 1000)"
              style={{ width: "100%" }}
              className="mb-3"
              value={data.hasilTestInNumber}
              name="hasilTestInNumber"
              onChange={value => onChange("hasilTestInNumber", value)}
              required
            />
          </React.Fragment>
        )}

        <div className="d-flex justify-content-end mt-2">
          <Button key="back" type="danger" onClick={onCancel} className="mr-2">
            Batal
          </Button>
          <Button
            key="submit"
            type="primary"
            className={`hs-btn ${role}`}
            loading={isLoading}
            htmlType="submit"
            onClick={() => validateForm(data, reqFields)}
          >
            Simpan
          </Button>
        </div>
      </form>
    </Modal>
  );
};
