import React from "react";
import { Select } from "antd";
import { Button, DatePicker, InputNumber, Modal } from "antd";
import { Label } from "./style";
import { ENUM } from "constant";
import { ChooseReagen } from "components";
import moment from "moment";
import { validateForm } from "utils/validator";

const { Option } = Select;

export const ModalKertasDBS = ({
  isAdd,
  onOk,
  onCancel,
  isLoading,
  role,
  data,
  onChange
}) => {
  const fields = {
    testType: "Tujuan Penggunaan",
    dateUsage: "Tanggal Penggunaan",
    toolId: "Pilih Jenis Alat",
    usageStock: "Jumlah yang Digunakan",
  }
  
  const _handleSubmit = e => {
    e.preventDefault();
    if (!validateForm(data, fields, false)) {
      return;
    }
    if (isLoading) {
      return;
    }
    onOk();
  };
  
  return (
    <Modal
      title={
        isAdd ? "Tambah Penggunaan Kertas DBS" : "Ubah Penggunaan Kertas DBS"
      }
      visible
      onOk={onOk}
      // maskClosable={role !== ROLE.LAB_STAFF}
      onCancel={onCancel}
      footer={null}
    >
      <form onSubmit={_handleSubmit}>
        <React.Fragment>
          <Label>
            Tujuan Penggunaan <span className="text-danger">*Harus Diisi</span>
          </Label>
          <Select
            placeholder="Pilih Test"
            style={{ width: "100%" }}
            className="mb-3"
            value={data.testType || null}
            name="testType"
            onChange={value => onChange("testType", value)}
            required
          >
            <Option disabled value={null}>
              Pilih Penggunaan
            </Option>
            {Object.keys(ENUM.REAGEN_TYPE_DBS).map(key => (
              <Option key={ENUM.REAGEN_TYPE_DBS[key]} value={key}>
                {ENUM.REAGEN_TYPE_DBS[key]}
              </Option>
            ))}
          </Select>

          <Label>
            Tanggal Penggunaan <span className="text-danger">*Harus Diisi</span>
          </Label>
          <DatePicker
            placeholder="Pilih tanggal"
            style={{ width: "100%" }}
            allowClear={false}
            className="mb-3"
            name="dateUsage"
            value={data.dateUsage ? moment(data.dateUsage) : null}
            onChange={date => onChange("dateUsage", date.toISOString())}
            required
          />

          <Label>
            Pilih Jenis Alat <span className="text-danger">*Harus Diisi</span>
          </Label>
          <ChooseReagen
            placeholder="Pilih Reagen"
            style={{ width: "100%" }}
            className="mb-3"
            value={data.toolId || null}
            name="toolId"
            filterByMedicineId={[24]}
            onChange={value => onChange("toolId", value)}
            required
          />

          <Label>
            Jumlah yang Digunakan{" "}
            <span className="text-danger">*Harus Diisi</span>
          </Label>
          <InputNumber
            placeholder="Masukkan Jumlah yang Digunakan"
            style={{ width: "100%" }}
            className="mb-3"
            value={data.usageStock}
            name="usageStock"
            onChange={value => onChange("usageStock", value)}
            required
          />
        </React.Fragment>

        <div className="d-flex justify-content-end mt-2">
          <Button key="back" type="danger" onClick={onCancel} className="mr-2">
            Batal
          </Button>
          <Button
            key="submit"
            type="primary"
            className={`hs-btn ${role}`}
            loading={isLoading}
            htmlType="submit"
            onClick={() => validateForm(data, fields)}
          >
            Simpan
          </Button>
        </div>
      </form>
    </Modal>
  );
};

