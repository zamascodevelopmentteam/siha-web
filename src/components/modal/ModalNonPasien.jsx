import React from "react";
import { Select } from "antd";
import { Button, DatePicker, InputNumber, Modal } from "antd";
import { Label } from "./style";
import { ENUM } from "constant";
import { ChooseReagen } from "components";
import moment from "moment";
import { validateForm } from "utils/validator";

const { Option } = Select;

export const ModalNonPasien = ({
  isAdd,
  onOk,
  onCancel,
  isLoading,
  role,
  data,
  onChange
}) => {
  const fields = {
    testType: "Tujuan Penggunaan",
    dateUsage: "Tanggal Penggunaan",
    toolId: "Pilih Jenis Alat",
    medicineId: "Reagen yang Digunakan",
    usageControl: "Jumlah untuk Kontrol",
    usageError: "Jumlah yang Digunakan Untuk Kalibrasi",
    usageOthers: "Jumlah Sebab Lainnya",
    usageWasted: "Jumlah Terbuang",
  }
  
  const _handleSubmit = e => {
    e.preventDefault();
    if (!validateForm(data, fields, false)) {
      return
    }
    data.usageStock = _totalUsage()
    if (isLoading) {
      return;
    }
    onOk();
  };

  const _totalUsage = () => {
    return data.usageControl + data.usageError + data.usageOthers + data.usageWasted;
  };

  return (
    <Modal
      title={
        isAdd ? "Tambah Penggunaan Non Pasien" : "Ubah Penggunaan Non Pasien"
      }
      visible
      onOk={onOk}
      // maskClosable={role !== ROLE.LAB_STAFF}
      onCancel={onCancel}
      footer={null}
    >
      <form onSubmit={_handleSubmit}>
        <React.Fragment>
          <Label>
            Tujuan Penggunaan <span className="text-danger">*Harus Diisi</span>
          </Label>
          <Select
            placeholder="Pilih Test"
            style={{ width: "100%" }}
            className="mb-3"
            value={data.testType || null}
            name="testType"
            onChange={value => onChange("testType", value)}
            required
          >
            <Option disabled value={null}>
              Pilih Penggunaan
            </Option>
            {Object.keys(ENUM.REAGEN_TYPE).map(key => (
              <Option key={ENUM.REAGEN_TYPE[key]} value={key}>
                {ENUM.REAGEN_TYPE[key]}
              </Option>
            ))}
          </Select>

          <Label>
            Tanggal Penggunaan <span className="text-danger">*Harus Diisi</span>
          </Label>
          <DatePicker
            placeholder="Pilih tanggal"
            style={{ width: "100%" }}
            allowClear={false}
            className="mb-3"
            name="dateUsage"
            value={data.dateUsage ? moment(data.dateUsage) : null}
            onChange={date => onChange("dateUsage", date.toISOString())}
            required
          />

          <Label>
            Pilih Jenis Alat <span className="text-danger">*Harus Diisi</span>
          </Label>
          <ChooseReagen
            placeholder="Pilih Reagen"
            style={{ width: "100%" }}
            className="mb-3"
            value={data.toolId || null}
            name="toolId"
            paramsFilter={
              data.reagenType === ENUM.REAGEN_TYPE.VIRAL_LOAD
                ? "is_machine=true&is_vl=true"
                : "is_machine=true&is_cd4=true"
            }
            onChange={value => onChange("toolId", value)}
            required
          />

          <Label>
            Reagen yang Digunakan{" "}
            <span className="text-danger">*Harus Diisi</span>
          </Label>
          <ChooseReagen
            placeholder="Pilih Reagen"
            style={{ width: "100%" }}
            className="mb-3"
            value={data.medicineId || null}
            name="medicineId"
            paramsFilter={
              data.reagenType === ENUM.REAGEN_TYPE.VIRAL_LOAD
                ? "is_machine=false&is_vl=true"
                : "is_machine=false&is_cd4=true"
            }
            onChange={value => onChange("medicineId", value)}
            required
          />

          <Label>
            Jumlah untuk Kontrol <span className="text-danger">*Harus Diisi</span>
          </Label>
          <InputNumber
            placeholder="Masukkan Jumlah untuk Kontrol"
            style={{ width: "100%" }}
            className="mb-3"
            value={data.usageControl}
            name="usageControl"
            onChange={value => onChange("usageControl", value)}
            required
          />

          {/* <Label>
            Jumlah yang Digunakan Untuk Kalibrasi <span className="text-danger">*Harus Diisi</span>
          </Label>
          <InputNumber
            placeholder="Masukkan Jumlah yang Digunakan Untuk Kalibrasi)"
            style={{ width: "100%" }}
            className="mb-3"
            value={data.usageError}
            name="usageError"
            onChange={value => onChange("usageError", value)}
            required
          /> */}

          <Label>
            Jumlah Gangguan Teknis <span className="text-danger">*Harus Diisi</span>
          </Label>
          <InputNumber
            placeholder="Masukkan Jumlah Gangguan Teknis"
            style={{ width: "100%" }}
            className="mb-3"
            value={data.usageError}
            name="usageError"
            onChange={value => onChange("usageError", value)}
            required
          />

          <Label>
            Jumlah Sebab Lainnya <span className="text-danger">*Harus Diisi</span>
          </Label>
          <InputNumber
            placeholder="Masukkan Jumlah Sebab Lainnya"
            style={{ width: "100%" }}
            className="mb-3"
            value={data.usageOthers}
            name="usageOthers"
            onChange={value => onChange("usageOthers", value)}
            required
          />

          <Label>
            Jumlah Terbuang <span className="text-danger">*Harus Diisi</span>
          </Label>
          <InputNumber
            placeholder="Masukkan Jumlah Terbuang"
            style={{ width: "100%" }}
            className="mb-3"
            value={data.usageWasted}
            name="usageWasted"
            onChange={value => onChange("usageWasted", value)}
            required
          />

          <Label>Total Penggunaan</Label>
          <InputNumber
            placeholder="Total Penggunaan"
            style={{ width: "100%" }}
            className="mb-3"
            value={_totalUsage()}
            onChange={value => onChange("usageStock", value)}
            name="usageStock"
            disabled
          />
        </React.Fragment>

        <div className="d-flex justify-content-end mt-2">
          <Button key="back" type="danger" onClick={onCancel} className="mr-2">
            Batal
          </Button>
          <Button
            key="submit"
            type="primary"
            className={`hs-btn ${role}`}
            loading={isLoading}
            htmlType="submit"
            onClick={() => validateForm(data, fields)}
          >
            Simpan
          </Button>
        </div>
      </form>
    </Modal>
  );
};
