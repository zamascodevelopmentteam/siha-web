import React from "react";
import { Modal, Button, Input } from "antd";
import { Link } from "react-router-dom";
import { Label } from "./style";
import { Typography } from "components";
import { ROLE } from "constant";
import { validateForm } from "utils/validator";

export const ModalKunjungan = ({
  nik,
  onOk,
  onSearch,
  onCancel,
  isLoading,
  role,
  onChange,
  data,
  isAdded
}) => {
  let title = role === ROLE.LAB_STAFF ? "Tambah Test Lab" : "Tambah Kunjungan";

  const _handleSubmit = e => {
    e.preventDefault();
    if (isLoading) {
      return;
    }
    onSearch();
  };

  return (
    <Modal
      title={title}
      visible
      style={{ top: 30 }}
      onOk={onOk}
      maskClosable={false}
      onCancel={onCancel}
      footer={null}
    >
      <form onSubmit={_handleSubmit}>
        <Typography fontSize="14px" fontWeight="bold" className="d-block mb-3">
          Cari Data Pasien
        </Typography>

        <Label>
          NIK <span className="text-danger">*Harus Diisi</span>
        </Label>
        <Input
          placeholder="Masukan NIK Pasien"
          style={{ width: "100%" }}
          className="mb-3"
          name="nik"
          minLength={16}
          maxLength={16}
          value={nik}
          required
          onChange={e => onChange(e.target.value)}
        />

        {data && (
          <div className="border p-3">
            <div className="text-center">
              <Typography fontWeight="bold" fontSize="14px">
                Detail Pasien
              </Typography>
            </div>

            <Label>NIK</Label>
            <Label isInfo className="mb-3">
              {data?.nik || "-"}
            </Label>

            <Label>Nama</Label>
            <Label isInfo className="mb-3">
              {data?.name || "-"}
            </Label>

            <Label>Alamat (Sesuai KTP)</Label>
            <Label isInfo className="mb-3">
              {data?.addressKTP || "-"}
            </Label>

            <Label>Alamat Domisili</Label>
            <Label isInfo className="mb-3">
              {data?.addressDomicile || "-"}
            </Label>

            <Label>Tanggal Lahir</Label>
            <Label isInfo className="mb-3">
              {data?.dateBirth || "-"}
            </Label>

            <Label>Jenis Kelamin</Label>
            <Label isInfo className="mb-3">
              {data?.gender}
            </Label>

            <Label>Status ODHA</Label>
            <Label isInfo className="mb-3">
              {data?.statusPatient}
            </Label>
            {!isAdded && (
              <div className="text-center">
                <Button
                  type="primary"
                  className={`hs-btn ${role} ml-2`}
                  loading={isLoading}
                  onClick={() => onOk()}
                >
                  {title}
                </Button>
              </div>
            )}

            {isAdded && (
              <div className="text-center">
                <Link
                  to={`/data-pasien/${role}/${data.id}`}
                  className={`ant-btn hs-btn ${role} ant-btn-primary`}
                >
                  Lihat Detail Kunjungan
                </Link>
              </div>
            )}
          </div>
        )}
        <div className="d-flex justify-content-end mt-2">
          <Button key="back" type="danger" onClick={onCancel}>
            Batal
          </Button>

          <Button
            key="submit"
            type="primary"
            className={`hs-btn ${role} ml-2`}
            loading={isLoading}
            htmlType="submit"
            onClick={() => validateForm({ nik }, { nik: "NIK" })}
          >
            Cari
          </Button>
        </div>
      </form>
    </Modal>
  );
};
