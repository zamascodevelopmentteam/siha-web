import React, { useEffect, useState } from "react";
import { ENUM, ROLE } from "constant";
import { Modal, Button, Select, Input, Col, Checkbox, Row } from "antd";
import { Label } from "./style";
import { Typography, ChooseUpk } from "components";
// import moment from "moment";
// import { validateForm } from "../../../../utils/validator";

const { Option } = Select;
const {
  LSM_RUJUKAN,
  ALASAN_TEST_IMS,
  KELUHAN_IMS,
  TANDA_KLINIS_IMS_LAKI,
  TANDA_KLINIS_IMS_PEREMPUAN,
  PREGNANCY_STEP
} = ENUM;
let requiredField = {
  tanggalKunjunga: "Tanggal Kunjungan",
  kunjunganKe: "Kunjungan Ke",
  kelompokPopulasi: "Kelompok Populasi",
  lsmRujukan: "LSM Rujukan",
  alasanKunjungan: "Alasan Kunjungan",
}

const RUJUKAN = { YA: true, TIDAK: false }
const TES_LAB = { YA: true, TIDAK: false }

export default ({
  onOk,
  onCancel,
  isLoading,
  role,
  onChange,
  onChangeExamination,
  onChangePhysicalExamination,
  data,
  dataExamination,
  isEdit,
  gender,
  province,
  faskes
}) => {
  const [physicalExam, setPhysicalExam] = useState([])

  let typeKlinikIms = gender === "PEREMPUAN" ? TANDA_KLINIS_IMS_PEREMPUAN : TANDA_KLINIS_IMS_LAKI

  useEffect(() => {
    let arrPhysical = []
    Object.keys(dataExamination.physicalExamination).forEach(v => {
      if (dataExamination.physicalExamination[v]) {
        arrPhysical.push(v)
      }
    })

    setPhysicalExam(arrPhysical)
  }, [])

  const _handleChangeSelect = (name, value) => {
    onChange(name, value);
  };

  const _handleChangeExamination = (name, value) => {
    onChangeExamination(name, value)
  }

  useEffect(() => {
    if (!dataExamination.isPregnant) {
      onChangeExamination('pregnancyStage', null)
    }
  }, [dataExamination.isPregnant])


  useEffect(() => {
    onChangePhysicalExamination(physicalExam)
  }, [physicalExam])

  const _handleChangePhysicalExamination = value => {
    setPhysicalExam(value)
  }

  const _handleSubmit = e => {
    e.preventDefault();
    onOk()
  };

  return (
    <Modal
      title="Pemeriksaan IMS"
      visible
      style={{ top: 30 }}
      onOk={onOk}
      maskClosable={false}
      onCancel={onCancel}
      footer={null}
    >
      <form onSubmit={_handleSubmit}>
        {/* <Label>LSM Rujukan</Label>
        <Select
          placeholder="Masukan LSM Rujukan"
          style={{ width: "100%" }}
          className="mb-3"
          name="lsmReferral"
          value={data.lsmReferral.length ? data.lsmReferral : undefined}
          disabled
        >
          <Option disabled value={null}>Pilih LSM Rujukan</Option>
          {Object.keys(LSM_RUJUKAN).map(key => (
            <Option key={LSM_RUJUKAN[key]} value={LSM_RUJUKAN[key]}>
              {LSM_RUJUKAN[key]}
            </Option>
          ))}
        </Select> */}

        <Label>
          Alasan Kunjungan
        </Label>
        <Select
          placeholder="Pilih Alasan Kunjungan"
          style={{ width: "100%" }}
          className="mb-3"
          name="reasonVisit"
          value={data.reasonVisit && data.reasonVisit.length ? data.reasonVisit : undefined}
          required
          disabled
        >
          <Option disabled value={null}>
            Pilih Alasan Kunjungan
              </Option>
          {Object.keys(ALASAN_TEST_IMS).map(key => (
            <Option key={ALASAN_TEST_IMS[key]} value={ALASAN_TEST_IMS[key]}>
              {ALASAN_TEST_IMS[key]}
            </Option>
          ))}
        </Select>

        <Label>
          Keluhan IMS <span className="label--required">* Di isi oleh Dokter</span>
        </Label>
        <Select
          placeholder="Pilih Keluhan IMS"
          style={{ width: "100%" }}
          className="mb-3"
          name="complaint"
          value={data.complaint && data.complaint.length ? data.complaint : undefined}
          onChange={value => _handleChangeSelect("complaint", value)}
        >
          <Option disabled value={null}>
            Pilih Keluhan IMS
              </Option>
          {Object.keys(KELUHAN_IMS).map(key => (
            <Option key={KELUHAN_IMS[key]} value={KELUHAN_IMS[key]}>
              {KELUHAN_IMS[key]}
            </Option>
          ))}
        </Select>

        {gender === "PEREMPUAN" &&
          <>
            <br />
            <br />
            <Typography fontSize="16px" fontWeight="bold">
              ANAMNESA
            </Typography>

            <Label>Status Kehamilan</Label>
            <Select
              placeholder="Status Kehamilan"
              style={{ width: "100%" }}
              className="mb-3"
              name="isPregnant"
              value={dataExamination.isPregnant !== null ? dataExamination.isPregnant : undefined}
              onChange={value => _handleChangeExamination("isPregnant", value)}
            >
              <Option disabled value={null}>Pilih Status Kehamilan</Option>
              <Option value={true}>Hamil</Option>
              <Option value={false}>Tidak Hamil</Option>
            </Select>

            <Label>Usia Kehamilan</Label>
            <Select
              placeholder="Pilih Usia Kehamilan"
              style={{ width: "100%" }}
              className="mb-3"
              name="pregnancyStage"
              value={dataExamination.pregnancyStage ? dataExamination.pregnancyStage : undefined}
              onChange={value => _handleChangeExamination("pregnancyStage", value)}
              disabled={!dataExamination.isPregnant}
            >
              <Option disabled value={null}>Pilih Usia Kehamilan</Option>
              {Object.keys(PREGNANCY_STEP).map(key => (
                <Option key={PREGNANCY_STEP[key]} value={PREGNANCY_STEP[key]}>
                  {PREGNANCY_STEP[key]}
                </Option>
              ))}
            </Select>
          </>
        }

        {gender === "PEREMPUAN" &&
          <>
            <br />
            <br />
            <Typography fontSize="16px" fontWeight="bold">
              IVA
            </Typography>

            <Label>Pemeriksaan IVA</Label>
            <Select
              placeholder="Pemeriksaan IVA"
              style={{ width: "100%" }}
              className="mb-3"
              name="isIvaTested"
              value={dataExamination.isIvaTested}
              onChange={value => _handleChangeExamination("isIvaTested", value)}
            >
              <Option disabled value={null}>Pilih Pemeriksaan IVA</Option>
              <Option value={true}>Iya</Option>
              <Option value={false}>Tidak</Option>
            </Select>

            <Label>Hasil Pemeriksaan IVA</Label>
            <Select
              placeholder="Hasil Pemeriksaan IVA"
              style={{ width: "100%" }}
              className="mb-3"
              name="ivaTest"
              value={dataExamination.ivaTest}
              onChange={value => _handleChangeExamination("ivaTest", value)}
              disabled={!dataExamination.isIvaTested}
            >
              <Option disabled value={null}>Pilih Hasil Pemeriksaan IVA</Option>
              <Option value={true}>Positif</Option>
              <Option value={false}>Negatif</Option>
            </Select>
          </>
        }

        <br />
        <br />
        <Typography fontSize="16px" fontWeight="bold">
          Pemeriksaan Fisik
            </Typography>

        <Label>
          Tanda Klinis IMS
              <span className="label--required">*Harus Diisi</span>
        </Label>
        <Checkbox.Group
          style={{ width: '100%' }}
          value={physicalExam}
          onChange={value => _handleChangePhysicalExamination(value)}
        >
          <Row>
            {Object.keys(typeKlinikIms).map(key => (
              <Col key={typeKlinikIms[key]} span={10}>
                <Checkbox value={key}>
                  {typeKlinikIms[key]}
                </Checkbox>
              </Col>
            ))}
          </Row>
        </Checkbox.Group>

        <br />
        <Label>Rujuk</Label>
        <Select
          placeholder="Rujuk"
          style={{ width: "100%" }}
          className="mb-3"
          name="rujukan"
          value={dataExamination.rujukan || dataExamination.upkId > 0}
          onChange={value => _handleChangeExamination("rujukan", value)}
        >
          <Option disabled value={null}>
            Pilih Rujuk
                </Option>
          {Object.keys(RUJUKAN).map(key => (
            <Option key={key} value={RUJUKAN[key]}>
              {key}
            </Option>
          ))}
        </Select>

        {(dataExamination.rujukan || dataExamination.upkId > 0) &&
          <>
            <Label>Kota/Kabupaten</Label>
            <Select
              optionFilterProp="children"
              placeholder="Pilih Kota/Kabupaten"
              style={{ width: "100%" }}
              className="ml-1 mb-3"
              name="province"
              value={dataExamination.provinceId ? dataExamination.provinceId : undefined}
              onChange={value => _handleChangeExamination("provinceId", value)}
            >
              <Option disabled value={null}>
                Pilih Kota/Kabupaten
                  </Option>
              {province.map(item => (
                <Option key={item.codeId} value={item.codeId}>
                  {item.name}
                </Option>
              ))}
            </Select>

            <Label>Faskes</Label>
            <ChooseUpk
              placeholder="Pilih Faskes"
              style={{ width: "100%" }}
              className="ml-1 mb-3"
              name="upkId"
              sudinKabID={dataExamination.provinceId}
              value={dataExamination.upkId ? dataExamination.upkId : undefined}
              onChange={value => _handleChangeExamination("upkId", value)}
            />

            <Label>Alasan Rujuk</Label>
            <Input
              placeholder="Tulis Alasan Rujukan"
              style={{ width: "100%" }}
              className="mb-3"
              name="reasonReferral"
              value={dataExamination.reasonReferral}
              onChange={e => _handleChangeExamination("reasonReferral", e.target.value)}
            />
          </>
        }

        <br />
        <Label>Tes Lab</Label>
        <Select
          placeholder="Tes Lab"
          style={{ width: "100%" }}
          className="mb-3"
          name="tesLab"
          value={dataExamination.tesLab}
          onChange={value => _handleChangeExamination("tesLab", value)}
        >
          <Option disabled value={null}>
            Pilih Rujuk
          </Option>
          {Object.keys(TES_LAB).map(key => (
            <Option key={key} value={RUJUKAN[key]}>
              {key}
            </Option>
          ))}
        </Select>

        <div className="d-flex justify-content-end mt-2">
          <Button key="back" type="danger" onClick={onCancel}>
            Batal
            </Button>

          <Button
            key="submit"
            type="primary"
            className={`hs-btn ${role} ml-2`}
            loading={isLoading}
            htmlType="submit"
          >
            Simpan
            </Button>
        </div>
      </form>
    </Modal>
  );
};