import React, { useContext } from "react";
import { Button, Modal, Empty, Tag } from "antd";
import { DataTable, DisplayDate } from "components";
import { ROLE, TAG_LABEL } from "constant";
import GlobalContext from "containers/GlobalContext";

export const ModalChangeLogs = ({ onCancel, isLoading, data }) => {
  const user = useContext(GlobalContext);

  const columnsList = [
    {
      title: "Tanggal Aktifitas",
      dataIndex: "activityDate",
      key: "activityDate",
      render: date => <DisplayDate date={date} />
    },
    {
      title: "Dilakukan Oleh",
      dataIndex: "actorData",
      key: "actorData",
      render: actorData =>
        actorData && actorData.actorName ? actorData.actorName : "-"
    },
    {
      title: "Status Permintaan",
      dataIndex: "orderStatus",
      key: "orderStatus",
      render: orderStatus => (
        <Tag color={TAG_LABEL.TAG_PLAN_STATUS[orderStatus]}>
          {TAG_LABEL.LABEL_PLAN_STATUS[orderStatus]}
        </Tag>
      )
    },
    {
      title: "Status Approval",
      dataIndex: "approvalStatus",
      key: "approvalStatus",
      render: approvalStatus => (
        <Tag color={TAG_LABEL.TAG_APPROVAL_STATUS[approvalStatus]}>
          {
            user.role !== ROLE.PHARMA_STAFF ?
              TAG_LABEL.LABEL_APPROVAL_STATUS[approvalStatus] : TAG_LABEL.LABEL_APPROVAL_STATUS[approvalStatus].replace('Pengelola Program', 'Kepala Faskes')
          }
        </Tag>
      )
    },
    {
      title: "Catatan",
      dataIndex: "notes",
      key: "notes",
      render: notes => (notes ? notes : "-")
    }
  ];

  return (
    <Modal
      title="Detail Riwayat Perubahan"
      visible
      style={{ top: 30 }}
      width="1000px"
      maskClosable={false}
      onCancel={onCancel}
      footer={null}
    >
      {!data && !isLoading && (
        <Empty description="Data riwayat perubahan tidak ditemukan" />
      )}

      <DataTable
        columns={columnsList}
        data={data}
        isScrollX
        rowKey="id"
        pagination={false}
        isLoading={isLoading}
      />
      <div className="d-flex justify-content-end mt-2">
        <Button key="back" type="danger" onClick={onCancel}>
          Tutup
        </Button>
      </div>
    </Modal>
  );
};
