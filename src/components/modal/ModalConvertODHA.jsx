import React from "react";
import { Button, Modal, Input } from "antd";
import { Label } from "./style";
import { Typography } from "components";

export const ModalConvertODHA = ({
  onOk,
  onCancel,
  isLoading,
  role,
  data,
  onChange
}) => {
  const _handleChange = e => {
    onChange(e.target.name, e.target.value);
  };

  const _handleSubmit = e => {
    e.preventDefault();
    if (isLoading) {
      return;
    }
    onOk();
  };

  return (
    <Modal
      title="Ubah menjadi ODHA"
      visible
      style={{ top: 30 }}
      onOk={onOk}
      maskClosable={false}
      onCancel={onCancel}
      footer={null}
    >
      <form onSubmit={_handleSubmit}>
        <Typography fontSize="14px" fontWeight="bold" className="d-block mb-3">
          Data Pribadi
        </Typography>

        <Label>No. Telepon</Label>
        <Input
          placeholder="Masukkan Nomor Telepon Pasien"
          style={{ width: "100%" }}
          className="mb-3"
          name="phone"
          type="phone"
          value={data.phone}
          onChange={_handleChange}
        />

        <Typography fontSize="14px" fontWeight="bold" className="d-block mb-3">
          Data Pengawas Minum Obat (PMO)
        </Typography>

        <Label>Nama Lengkap</Label>
        <Input
          placeholder="Masukkan Nama Lengkap PMO"
          style={{ width: "100%" }}
          className="mb-3"
          name="namaPmo"
          value={data.namaPmo}
          onChange={_handleChange}
        />

        <Label>Status Hubungan</Label>
        <Input
          placeholder="Masukkan Hubungan PMO dengan Pasien"
          style={{ width: "100%" }}
          className="mb-3"
          name="hubunganPmo"
          value={data.hubunganPmo}
          onChange={_handleChange}
        />

        <Label>No. Telepon</Label>
        <Input
          placeholder="Masukkan Nomor Telepon PMO"
          style={{ width: "100%" }}
          className="mb-3"
          name="noHpPmo"
          value={data.noHpPmo}
          onChange={_handleChange}
        />

        <div className="d-flex justify-content-end mt-2">
          <Button key="back" type="danger" onClick={onCancel}>
            Batal
          </Button>
          <Button
            key="submit"
            type="primary"
            className={`hs-btn ${role} ml-2`}
            loading={isLoading}
            htmlType="submit"
          >
            Simpan
          </Button>
        </div>
      </form>
    </Modal>
  );
};
