import React from "react";
import { Button, Modal, Input, InputNumber, Select } from "antd";
import { Label } from "./style";

const { TextArea } = Input;
const { Option } = Select;

export const ModalStockAdjustment = ({
  name,
  batchCode,
  onOk,
  onCancel,
  isLoading,
  role
}) => {
  return (
    <Modal
      title="Penyesuaian Stok Obat"
      visible
      style={{ top: 30 }}
      onOk={onOk}
      maskClosable={false}
      onCancel={onCancel}
      footer={[
        <Button key="back" type="danger" onClick={onCancel}>
          Batal
        </Button>,
        <Button
          key="submit"
          type="primary"
          className={`hs-btn ${role}`}
          loading={isLoading}
          onClick={onOk}
        >
          Simpan
        </Button>
      ]}
    >
      <Label>Nama Brand</Label>
      <Label>{name}</Label>

      <Label>Batch Code</Label>
      <Label>{batchCode}</Label>

      <Label>Jenis Penyesuaian</Label>
      <Select
        placeholder="Pilih Jenis Penyesuaian"
        style={{ width: "100%" }}
        className="mb-3"
      >
        <Option value="">Jenis 1</Option>
      </Select>

      <Label>Nilai Penyesuaian</Label>
      <InputNumber
        placeholder="Masukkan Nilai Penyesuaian"
        style={{ width: "100%" }}
        className="mb-3"
      />

      <Label>Alasan Penyesuaian</Label>
      <TextArea
        placeholder="Masukkan Alasan Penyesuaian"
        style={{ width: "100%" }}
        className="mb-3"
      />
    </Modal>
  );
};
