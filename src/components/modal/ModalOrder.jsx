import React from "react";
// import moment from "moment";
import { Button, Input, Modal, InputNumber } from "antd";
import { Label } from "./style";
import { ChooseDestination, ChooseStock } from "components";
import { ROLE } from "constant";

export const ModalOrder = ({
  onOk,
  isEdit,
  onCancel,
  isLoading,
  role,
  data,
  onChange,
  items,
  addItem,
  removeItem,
  selectedDest,
  onChangeItem,
  onChangeDestination,
  type,
  dataReguler,
  onChangeQty,
  medicineType
}) => {
  // const date = new Date()
  // const isRegularTime = date.getDate() <= 5 || date.getDate() >= 26
  const isRegularTime = true

  const _handleChange = e => {
    onChange(e.target.name, e.target.value);
  };

  const _handleSubmit = e => {
    e.preventDefault();
    if (isLoading) {
      return;
    }
    onOk();
  };

  let no = 0;
  let noArv = 1;
  let noNonArv = 1;

  return (
    <Modal
      title={type === 'reguler' ? 'Tambah Permintaan Reguler' : `${isEdit ? "Ubah" : "Tambah"} Permintaan Khusus`}
      visible
      width="90%"
      onOk={onOk}
      maskClosable={false}
      onCancel={onCancel}
      footer={null}
    >
      <form onSubmit={_handleSubmit}>
        <Label>Ditujukan ke</Label>
        {type === 'reguler' &&
          // <Input
          //   placeholder="Dituju ke"
          //   style={{ width: "100%" }}
          //   className="mb-3"
          //   value={data.to ? data.to.name : ""}
          // />
          <ChooseDestination
            // placeholder="Pilih Tujuan Permintaan"
            style={{ width: "100%" }}
            className="mb-3"
            onChange={onChangeDestination}
            required
            // isUp
            one
          />
        }
        {type !== 'reguler' && !isEdit && (
          <ChooseDestination
            // placeholder="Pilih Tujuan Permintaan"
            style={{ width: "100%" }}
            className="mb-3"
            onChange={onChangeDestination}
            required
          />
        )}

        {type !== 'reguler' && isEdit && data.senderName}

        {type === "reguler" &&
          <div className="row mb-2">
            <div className="col-1" style={{ maxWidth: "55px" }}>
              <Label>No.</Label>
            </div>
            <div className="col-4">
              <Label>Nama Jenis Barang</Label>
            </div>
            <div className="col">
              <Label>{medicineType === "ARV" ? 'Jumlah Pasien' : 'Rata-Rata 6 Bulan (Test)'}</Label>
            </div>
            <div className="col">
              <Label>Stok Yang Tersedia{medicineType !== "ARV" ? ' (KIT)' : ''}</Label>
            </div>
            {medicineType !== "ARV" && (
              <div className="col">
                <Label>Stok Yang Tersedia (Test)</Label>
              </div>
            )}
            {medicineType !== "ARV" && (
              <div className="col">
                <Label>Jumlah Logistik Diperlukan (Test)</Label>
              </div>
            )}
            <div className="col">
              <Label>Jumlah Logistik Diperlukan{medicineType !== "ARV" ? ' (KIT)' : ''}</Label>
            </div>
            <div className="col">
              <Label>Jumlah Logistik Diminta{medicineType !== "ARV" ? ' (KIT)' : ''}</Label>
            </div>
            <div className="col">
              <Label>Alasan</Label>
            </div>
          </div>
        }

        {/* {data.medicines && data.medicines.map((v, i) =>
          <ItemReguler
            key={v.medicineid}
            idx={i}
            medicineName={v.medicinename}
            medicineQty={v.qty}
            totalPatient={data.totalPatient ? data.totalPatient : 0}
          />
        )} */}

        {/* new */}
        {type === 'reguler' && dataReguler && dataReguler.map((v, i) => {
          // console.log(v);
          return (
            <ItemReguler
              type={v.type ? v.type : null}
              key={i}
              idx={no++}
              medicineName={v.medicineName}
              medicineQty={v.package_quantity}
              totalPatient={v.totalPatient}
              onHand={v.onHand}
              orderQty={v.orderQty}
              onChangeQty={onChangeQty}
              reason={v.reason}
              rataPemakaian={v.rataPemakaian}
              medicineType={medicineType}
              packageMultiplier={v.packageMultiplier}
              smallOrderQty={v.smallOrderQty}
            />
          )
        })}
        {/* end new */}

        {type !== 'reguler' && <span clasname="mb-3"><b>ARV</b></span>}

        {type !== "reguler" &&
          <div className="row mb-2">
            <div className="col-1" style={{ maxWidth: "55px" }}>
              <Label>No.</Label>
            </div>
            <div className="col-5">
              <Label>Nama Jenis Barang</Label>
            </div>
            <div className="col">
              <Label>Jumlah Barang</Label>
            </div>
            <div className="col">
              <Label>Satuan</Label>
            </div>
            <div className="col-1">
              <Button
                size="small"
                className={`hs-btn ${ROLE.PHARMA_STAFF}`}
                icon="plus"
                onClick={() => addItem('ARV')}
              />
            </div>
          </div>
        }

        {type !== 'reguler' && items.map((value, index) => {
          if (value.medicine ? value.medicine.medicineType === "ARV" : value.medicineType === "ARV") {
            return (
              <ItemRow
                no={noArv++}
                key={value.id}
                idx={index}
                destID={selectedDest.id}
                destType={selectedDest.type}
                destLogisticRole={selectedDest.logisticRole}
                medicineId={value.medicineId}
                packageQuantity={value.packageQuantity}
                expiredDate={value.expiredDate}
                onChange={onChangeItem}
                onRemove={removeItem}
                unit={value.unit || (value.medicine ? value.medicine.packageUnitType : null)}
                type={'ARV'}
              />
            )
          }
        })}

        {type !== 'reguler' && <span clasname="mb-3"><b>NON ARV</b></span>}

        {type !== "reguler" &&
          <div className="row mb-2">
            <div className="col-1" style={{ maxWidth: "55px" }}>
              <Label>No.</Label>
            </div>
            <div className="col-5">
              <Label>Nama Jenis Barang</Label>
            </div>
            <div className="col">
              <Label>Jumlah Barang</Label>
            </div>
            <div className="col">
              <Label>Satuan</Label>
            </div>
            <div className="col-1">
              <Button
                size="small"
                className={`hs-btn ${ROLE.PHARMA_STAFF}`}
                icon="plus"
                onClick={() => addItem('NON_ARV')}
              />
            </div>
          </div>
        }

        {type !== 'reguler' && items.map((value, index) => {
          if (value.medicine ? value.medicine.medicineType === "NON_ARV" : value.medicineType === "NON_ARV") {
            return (
              <ItemRow
                no={noNonArv++}
                key={value.id}
                idx={index}
                destID={selectedDest.id}
                destType={selectedDest.type}
                destLogisticRole={selectedDest.logisticRole}
                medicineId={value.medicineId}
                packageQuantity={value.packageQuantity}
                expiredDate={value.expiredDate}
                onChange={onChangeItem}
                onRemove={removeItem}
                unit={value.unit || (value.medicine ? value.medicine.packageUnitType : null)}
                type={'NON_ARV'}
              />
            )
          }
        })}

        {type !== "reguler" &&
          <>
            <Label>Alasan Permintaan</Label>
            <Input
              placeholder="Alasan Permintaan"
              style={{ width: "100%" }}
              className="mb-3"
              name="orderNotes"
              value={data.orderNotes}
              onChange={_handleChange}
            />
          </>
        }

        <div className="d-flex justify-content-end mt-2">
          <Button key="back" type="danger" onClick={onCancel} className="mr-2">
            {role === ROLE.PHARMA_STAFF ? "Batal" : "Tutup"}
          </Button>
          {role === ROLE.PHARMA_STAFF && type !== 'reguler' ?
            <Button
              key="submit"
              type="primary"
              className={`hs-btn ${role}`}
              loading={isLoading}
              htmlType="submit"
            >
              Simpan
            </Button>
            :
            dataReguler && isRegularTime && (
              <Button
                key="submit"
                type="primary"
                className={`hs-btn ${role}`}
                loading={isLoading}
                htmlType="submit"
              >
                Simpan
              </Button>
            )}
        </div>
      </form>
    </Modal>
  );
};

const ItemRow = ({
  no,
  idx,
  destID,
  destType,
  destLogisticRole,
  medicineId,
  packageQuantity,
  expiredDate,
  onRemove,
  onChange,
  unit,
  type
}) => {
  return (
    <div className="row">
      <div className="col-1" style={{ maxWidth: "55px" }}>{no}</div>
      <div className="col-5">
        <ChooseStock
          placeholder="Nama Jenis Barang"
          style={{ width: "100%" }}
          className="mb-3"
          name="medicineId"
          destType={destType}
          destID={destID}
          destLogisticRole={destLogisticRole}
          showStock
          value={medicineId}
          onChange={value => onChange(idx, "medicineId", value, type)}
          required
          type={type}
        />
      </div>
      <div className="col">
        <InputNumber
          placeholder="Jumlah Paket"
          style={{ width: "100%" }}
          className="mb-3"
          name="packageQuantity"
          value={packageQuantity}
          onChange={value => onChange(idx, "packageQuantity", value)}
        />
      </div>
      <div className="col">
        <Input
          placeholder="Satuan"
          style={{ width: "100%" }}
          className="mb-3"
          name="unit"
          value={unit}
          onChange={value => onChange(idx, "unit", value)}
          readOnly
        />
      </div>
      <div className="col-1">
        <Button
          size="small"
          type="danger"
          onClick={() => onRemove(idx)}
          icon="delete"
        />
      </div>
    </div>
  );
};

const ItemReguler = ({
  type,
  idx,
  medicineName,
  medicineQty,
  totalPatient,
  onHand,
  orderQty,
  onChangeQty,
  reason,
  rataPemakaian,
  medicineType,
  packageMultiplier,
  smallOrderQty,
}) => {
  return (
    <React.Fragment>
      <div className="row">
        <div className="col-1" style={{ maxWidth: "55px" }}>{idx + 1}</div>
        <div className="col-4">
          <Input
            placeholder="Nama Jenis Barang"
            style={{ width: "100%" }}
            className="mb-3"
            name="medicineId"
            value={medicineName}
          />
        </div>
        <div className="col">
          <Input
            placeholder={medicineType === "ARV" ? "Jumlah Pasien" : "Jumlah Rata - Rata"}
            style={{ width: "100%" }}
            className="mb-3"
            name="totalPatient"
            // value={medicineType === "ARV" && type ? totalPatient : rataPemakaian}
            value={medicineType === "ARV" ? totalPatient : rataPemakaian}
          />
        </div>
        <div className="col">
          <Input
            placeholder="On Hand"
            style={{ width: "100%" }}
            className="mb-3"
            name="onHand"
            value={onHand}
          />
        </div>
        {medicineType !== "ARV" && (
          <div className="col">
            <Input
              placeholder="On Hand"
              style={{ width: "100%" }}
              className="mb-3"
              name="onHand"
              value={onHand * packageMultiplier}
            />
          </div>
        )}
        {medicineType !== "ARV" && (
          <div className="col">
            <Input
              placeholder="Jumlah Paket"
              style={{ width: "100%" }}
              className="mb-3"
              name="smallOrderQty"
              value={smallOrderQty}
            />
          </div>
        )}
        <div className="col">
          <Input
            placeholder="Jumlah Paket"
            style={{ width: "100%" }}
            className="mb-3"
            name="medicineQty"
            value={medicineQty}
          />
        </div>
        <div className="col">
          <Input
            type={'number'}
            placeholder="Jumlah Paket"
            style={{ width: "100%" }}
            className="mb-3"
            name="orderQty"
            value={orderQty}
            onChange={value => onChangeQty('orderQty', value, idx)}
          />
        </div>
        <div className="col">
          <Input
            placeholder="Alasan"
            style={{ width: "100%" }}
            className="mb-3"
            name="reason"
            value={reason}
            onChange={value => onChangeQty('reason', value, idx)}
          />
        </div>
      </div>
    </React.Fragment>
  );
}
