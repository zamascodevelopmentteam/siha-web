import React, { useEffect } from "react";
import moment, { now } from "moment";
import { Button, DatePicker, Input, Modal, InputNumber, Select } from "antd";
import { Label } from "./style";
import { ChooseReagen, Typography } from "components";
import { ENUM } from "constant";
import styled from "styled-components";

const { Option } = Select

export const ModalRecipe = ({
  onOk,
  onCancel,
  isLoading,
  role,
  data,
  dataIms,
  onChange,
  medicines,
  addMedicine,
  removeMedicine,
  onChangeMedicine,
  onChangeMedicineIms,
  recipeCombination,
  patient,
  isIms
}) => {
  const _handleSubmit = e => {
    e.preventDefault();
    if (isLoading) {
      return;
    }
    onOk();
  };

  return (
    <Modal
      title="Ubah Resep Obat"
      visible
      width="80%"
      style={{ top: 30 }}
      onOk={onOk}
      maskClosable={false}
      onCancel={onCancel}
      footer={null}
    >
      <form onSubmit={_handleSubmit}>
        <Typography fontSize="14px" fontWeight="bold" className="d-block mb-3">
          Obat
        </Typography>

        <SummaryWrapper>
          <tbody>
            <tr>
              <td width="100px">NIK</td>
              <td>{patient.nik}</td>
            </tr>
            <tr>
              <td>Nama</td>
              <td>{patient.name}</td>
            </tr>
            <tr>
              <td>Jenis Kelamin</td>
              <td>{patient.gender}</td>
            </tr>
          </tbody>
        </SummaryWrapper>
        <br />

        <div className="row mb-2">
          <div className="col-0">
            <Label>No.</Label>
          </div>
          <div className="col-8">
            <Label>Nama Obat</Label>
          </div>
          <div className="col-1">
            <Label>Jumlah Obat</Label>
          </div>
          <div className="col-1">
            <Label>Jumlah Hari</Label>
          </div>
          <div className="col-1">
            <Label>Aturan Minum</Label>
          </div>
          {/* <div className="col-0">
            <Button
              size="small"
              className={`hs-btn ${role}`}
              icon="plus"
              onClick={addMedicine}
            />
          </div> */}
        </div>

        {isIms ?
          dataIms.medicines.map((v, i) =>
            <MedicineRow
              key={v.id}
              idx={i}
              notes={v.note}
              medicineId={v.medicineId ? v.medicineId : (v.tmpMedicineId !== 0 ? v.tmpMedicineId : undefined)}
              stockQty={v.totalQty}
              jumlahHari={v.totalDays}
              onChange={onChangeMedicineIms}
              onRemove={removeMedicine}
              isIms={true}
            />
          )
          :
          medicines.map((value, index) => {
            return (
              <MedicineRow
                key={value.id}
                idx={index}
                notes={value.notes}
                medicineId={value.medicineId}
                stockQty={value.stockQty}
                jumlahHari={value.jumlahHari}
                onChange={onChangeMedicine}
                onRemove={removeMedicine}
              />
            );
          })
        }

        <Label>Paduan Obat (Regiment)</Label>
        <Typography fontSize="12px" className="d-block mb-3">
          {data.paduanObatStr ? (
            data.paduanObatStr.search(' | ') > -1 ? data.paduanObatStr.split(' | ').map(r => [r, <br/>]) : data.paduanObatStr
          ) : (
            recipeCombination.map(
              (value, idx) => `${idx !== 0 ? " + " : ""}${value}`
            ) || "-"
          )}
        </Typography>
        <div className="row mb-2">
          <div className="col-4">
            <Label>Tanggal Pemberian Obat</Label>
            <DatePicker
              disabledDate={current =>
                current &&
                current.format("YYYY-MM-DD") < moment().add(-2, 'day').format("YYYY-MM-DD")
              }
              placeholder="Pilih Tanggal Pemberian Obat"
              style={{ width: "70%" }}
              allowClear={false}
              name="tglPemberianObat"
              value={isIms ? moment(dataIms.dateGivenMedicine) : data.tglPemberianObat ? moment(data.tglPemberianObat) : null}
              onChange={isIms ?
                date => onChangeMedicineIms("dateGivenMedicine", date.toISOString())
                :
                date => onChange("tglPemberianObat", date.toISOString())}
            />
          </div>
          {!isIms &&
            <div className="col-2">
              <Label>Status Paduan</Label>
              <Select
                onChange={value => onChange("statusPaduan", value)}
                required
                name="statusPaduan"
                value={data.statusPaduan}
                placeholder="Status Paduan"
                disabled
              >
                <Option disabled value={null} >Pilih Status Paduan</Option>
                {Object.keys(ENUM.STATUS_PADUAN).map(key =>
                  <Option key={key} value={ENUM.STATUS_PADUAN[key]}>
                    {ENUM.STATUS_PADUAN[key]}
                  </Option>)}
              </Select>
            </div>
          }
        </div>

        {isIms &&
          <div className="row mb-2">
            <div className="col-2">
              <Label>Kondom</Label>
              <InputNumber
                placeholder="Masukan Jumlah Kondom"
                style={{ width: "100%" }}
                className="mb-3"
                name="condom"
                value={dataIms.condom}
                onChange={value => onChangeMedicineIms("condom", value)}
              />
            </div>

            <div className="col-12" />

            <div className="col-2">
              <Label>Lubrikan</Label>
              <InputNumber
                placeholder="Masukan Jumlah Lubrikan"
                style={{ width: "100%" }}
                className="mb-3"
                name="lubricant"
                value={dataIms.lubricant}
                onChange={value => onChangeMedicineIms("lubricant", value)}
              />
            </div>

            <div className="col-12" />

            <div className="col-2">
              <Label>Tanggal Rencana Kunjungan</Label>
              <DatePicker
                placeholder="Pilih Tanggal Rencana Kunjungan"
                style={{ width: "70%" }}
                allowClear={false}
                name="rencanaKunjungan"
                value={dataIms.rencanaKunjungan ? moment(dataIms.rencanaKunjungan) : moment()}
                onChange={date => onChangeMedicineIms("rencanaKunjungan", date.toISOString())}
              />
            </div>
          </div>
        }

        <div className="d-flex justify-content-end mt-2">
          <Button key="back" type="danger" onClick={onCancel} className="mr-2">
            Batal
          </Button>

          <Button
            key="submit"
            type="primary"
            className={`hs-btn ${role}`}
            loading={isLoading}
            htmlType="submit"
          >
            Simpan
          </Button>
        </div>
      </form>
    </Modal>
  );
};

const MedicineRow = ({
  idx,
  medicineId,
  notes,
  stockQty,
  jumlahHari,
  onRemove,
  onChange,
  isIms = false
}) => {
  const _handleChange = e => {
    onChange(idx, e.target.name, e.target.value);
  };

  return (
    <div className="row">
      <div className="col-0">{idx + 1}</div>
      <div className="col-8">
        <ChooseReagen
          placeholder="Pilih Obat"
          style={{ width: "100%" }}
          className="mb-3"
          name="medicineId"
          value={medicineId}
          showDetail={!isIms}
          medicineType="ARV"
          onChange={isIms ?
            value => onChange('medicineId', value, 'medicines', idx)
            :
            value => onChange('medicineId', idx, value)}
          required
          ims={isIms}
          disabled

        // placeholder="Pilih Obat"
        // style={{ width: "100%" }}
        // className="mb-3"
        // name="medicineId"
        // value={medicineId}
        // showDetail={!isIms}
        // medicineType="ARV"
        // onChange={isIms ?
        //   value => onChange('medicineId', value, 'medicines', idx)
        //   :
        //   value => onChange(idx, "medicineId", value)}
        // required
        // isIms
        />
      </div>

      <div className="col-1">
        <InputNumber
          placeholder="Masukan Jumlah Obat"
          style={{ width: "100%" }}
          className="mb-3"
          name="stockQty"
          value={stockQty}
          onChange={isIms ?
            value => onChange('totalQty', value, 'medicines', idx)
            :
            value => onChange(idx, "stockQty", value)}
          disabled
        />
      </div>
      <div className="col-1">
        <InputNumber
          placeholder="Masukan Jumlah Hari"
          style={{ width: "100%" }}
          className="mb-3"
          name="jumlahHari"
          value={jumlahHari}
          onChange={isIms ?
            value => onChange('totalDays', value, 'medicines', idx)
            :
            value => onChange(idx, "jumlahHari", value)}
          disabled
        />
      </div>
      <div className="col-1">
        <Input
          placeholder="Aturan Minum"
          style={{ width: "100%" }}
          className="mb-3"
          name="notes"
          value={notes}
          onChange={isIms ?
            e => onChange('note', e.target.value, 'medicines', idx)
            :
            _handleChange}
          disabled
        />
      </div>
      {/* <div className="col-0">
        <Button
          size="small"
          type="danger"
          onClick={() => onRemove(idx)}
          icon="delete"
        />
      </div> */}
    </div>
  );
};

const SummaryWrapper = styled.table`
  font-size: 12px;
  tr > td {
    padding: 5px 0;
  }
`;