import React, { useContext } from "react";
import { Alert, Button, Input, Modal, Select, Tag } from "antd";
import { Label } from "./style";
import { ROLE, TAG_LABEL } from "constant";
import GlobalContext from "containers/GlobalContext";

const { Option } = Select;

const { TextArea } = Input;

export const ModalKonfirmasiPermintaan = ({
  onOk,
  onCancel,
  isLoading,
  role,
  error,
  onChange,
  options,
  title
}) => {
  const user = useContext(GlobalContext);

  const _handleChange = e => {
    onChange(e.target.name, e.target.value);
  };

  const _handleSubmit = e => {
    e.preventDefault();
    if (isLoading) {
      return;
    }
    onOk();
  };
  return (
    <Modal
      title={title || "Mengubah Status Permintaan"}
      visible
      maskClosable={role === ROLE.RR_STAFF}
      onOk={onOk}
      onCancel={onCancel}
      footer={null}
    >
      {error !== "" && <Alert className="mb-3" message={error} type="error" />}
      <form onSubmit={_handleSubmit}>
        <Label>Status Permintaan</Label>
        <Select
          placeholder="Pilih Status"
          style={{ width: "100%" }}
          className="mb-3"
          name="status"
          onChange={value => onChange("status", value)}
        >
          <Option disabled value={null}>
            Pilih Status Permintaan
          </Option>
          {options.map(option => (
            <Option key={option} value={option}>
              <Tag color={TAG_LABEL.TAG_APPROVAL_STATUS[option]}>
                {
                  user.role !== ROLE.PHARMA_STAFF ?
                    TAG_LABEL.LABEL_APPROVAL_STATUS[option] : TAG_LABEL.LABEL_APPROVAL_STATUS[option].replace('Pengelola Program', 'Kepala Faskes')
                }
              </Tag>
            </Option>
          ))}
        </Select>

        <Label>Catatan Approval (Opsional)</Label>
        <TextArea
          placeholder="Ketikan Catatan  terkait Permintaan"
          style={{ width: "100%" }}
          className="mb-3"
          name="notes"
          onChange={_handleChange}
        />
        <div className="d-flex justify-content-end mt-2">
          <Button key="back" type="danger" onClick={onCancel} className="mr-2">
            Batal
          </Button>
          <Button
            key="submit"
            type="primary"
            className={`hs-btn ${role}`}
            loading={isLoading}
            htmlType="submit"
          >
            Simpan
          </Button>
        </div>
      </form>
    </Modal>
  );
};
