import React, { useState, useEffect } from "react";
import API from "utils/API";
import axios from "axios";
import { Select, Spin } from "antd";
import { formatNumber } from "utils";

const { Option } = Select;

const ChooseReagen = ({
  paramsFilter,
  medicineType,
  showDetail,
  isNotNol,
  filterByMedicineId,
  jenisTest,
  ...props
}) => {
  let source = axios.CancelToken.source();
  const [listReagen, setListReagen] = useState([]);
  const [isLoading, setLoading] = useState(false);

  const _loadData = async () => {
    setLoading(true);
    if (!medicineType) {
      medicineType = "NON_ARV";
    }
    if (!props.isIms) {
      await API.Medicine.listStock(source.token, 'all', paramsFilter)
        .then(rsp => {
          let dataList = rsp.data;
          if (filterByMedicineId && filterByMedicineId.length > 0) {
            dataList = dataList.filter(data => {
              return filterByMedicineId.includes(data.id);
            });
          }
          if (String(jenisTest).startsWith("PCR")) {
            dataList = dataList.filter((item) => {
              return !String(item.name).startsWith("Rapid HIV")
            })
          }
          setListReagen(dataList || []);
        })
        .catch(e => {
          console.error("e: ", e);
        })
        .finally(() => {
          setLoading(false);
        });
    } else {
      const res = await API.IMS.medicinesList();
      setListReagen(res.data || []);
    }
  };

  useEffect(() => {
    _loadData();

    return () => {
      source.cancel("request canceled");
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  if (isNotNol) {
    return (
      <Select
        showSearch
        optionFilterProp="children"
        notFoundContent={isLoading ? <Spin size="small" /> : null}
        {...props}
      >
        <Option disabled value={null}>
          Pilih Reagen
        </Option>
        {/* reagenHiv */}
        {listReagen.map(
          reagen => {
            if (props.reagenHiv) {
              return reagen.category === "Reagen HIV" && (
                <Option key={reagen.id} value={reagen.id} disabled={reagen.stock_qty <= 0}>
                  {reagen.name}{" "}
                  {/* {showDetail && `${reagen.codename}  - ${formatNumber(reagen.stock_qty)} ${reagen.stock_unit_type}`} */}
                  {reagen.stock_qty <= 0 ? ' (Stok kosong)' : ''}
                </Option>
              )
            } else if (props.reagenIms) {
              return reagen.category === "Reagen IMS" && (
                <Option key={reagen.id} value={reagen.id} disabled={reagen.stock_qty <= 0}>
                  {reagen.name}{" "}
                  {/* {showDetail && `${reagen.codename}  - ${formatNumber(reagen.stock_qty)} ${reagen.stock_unit_type}`} */}
                  {reagen.stock_qty <= 0 ? ' (Stok kosong)' : ''}
                </Option>
              )
            } else if (props.vlcd4) {
              if (props.vlcd4Type == "CD4") {
                return (reagen.is_cd4 && reagen.vlcd4_category == props.vlcd4ReagentType) && (
                  <Option key={reagen.id} value={reagen.id} disabled={reagen.stock_qty <= 0}>
                    {reagen.name}{" "}
                    {/* {showDetail && `${reagen.codename}  - ${formatNumber(reagen.stock_qty)} ${reagen.stock_unit_type}`} */}
                    {reagen.stock_qty <= 0 ? ' (Stok kosong)' : ''}
                  </Option>
                )  
              } else if (props.vlcd4Type == "VL") {
                return (reagen.is_vl && reagen.vlcd4_category == props.vlcd4ReagentType) && (
                  <Option key={reagen.id} value={reagen.id} disabled={reagen.stock_qty <= 0}>
                    {reagen.name}{" "}
                    {/* {showDetail && `${reagen.codename}  - ${formatNumber(reagen.stock_qty)} ${reagen.stock_unit_type}`} */}
                    {reagen.stock_qty <= 0 ? ' (Stok kosong)' : ''}
                  </Option>
                )  
              }
            } else {
              return (
                <Option key={reagen.id} value={reagen.id} disabled={reagen.stock_qty <= 0}>
                  {reagen.name}{" "}
                  {/* {showDetail && `${reagen.codename}  - ${formatNumber(reagen.stock_qty)} ${reagen.stock_unit_type}`} */}
                  {reagen.stock_qty <= 0 ? ' (Stok kosong)' : ''}
                </Option>
              )
            }
          }
        )}
      </Select>
    );
  }

  return (
    <Select
      showSearch
      optionFilterProp="children"
      notFoundContent={isLoading ? <Spin size="small" /> : null}
      {...props}
    >
      <Option disabled value={null}>
        Pilih Reagen
      </Option>
      {listReagen.map(reagen => {
        if (props.hiv) {
          if (["HIV", "IO", "IO (TB)"].includes(reagen.category)) {
            return (
              <Option key={reagen.id} value={reagen.id}>
                {reagen.name}{" "}
                {showDetail && `${reagen.codename}  - ${formatNumber(reagen.stock_qty)} ${reagen.stock_unit_type} - ${reagen.category}`}
              </Option>
            )
          }
        } else if (props.ims) {
          if (reagen.category == "IMS") {
            return (
              <Option key={reagen.id} value={reagen.id} disabled={reagen.stock_qty <= 0 ? true : false}>
                {reagen.name}{" "}
                {showDetail && `${reagen.codename}  - ${formatNumber(reagen.stock_qty)} ${reagen.stock_unit_type} - ${reagen.category}`}
              </Option>
            )
          }
        } else {
          return (
            <Option key={reagen.id} value={reagen.id}>
              {reagen.name}{" "}
              {showDetail && `${reagen.codename}  - ${formatNumber(reagen.stock_qty)} ${reagen.stock_unit_type} - ${reagen.category}`}
            </Option>
          )
        }
      })}
    </Select>
  );
};

export default ChooseReagen;
