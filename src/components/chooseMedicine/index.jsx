import React, { useState, useEffect } from "react";
import API from "utils/API";
import axios from "axios";
import { Select, Spin } from "antd";

const { Option } = Select;

const ChooseMedicine = props => {
  let source = axios.CancelToken.source();
  const [items, setItems] = useState([]);
  const [isLoading, setLoading] = useState(false);
  const medicineType = props.medicineType;

  const _loadData = () => {
    setLoading(true);
    API.MasterData.listMedicine(source.token, medicineType)
      .then(rsp => {
        setItems(rsp.data || []);
      })
      .catch(e => {
        console.error("e: ", e);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  useEffect(() => {
    _loadData();

    return () => {
      source.cancel("request canceled");
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [medicineType]);

  return (
    <Select
      showSearch
      optionFilterProp="children"
      notFoundContent={isLoading ? <Spin size="small" /> : null}
      {...props}
    >
      <Option disabled value={null}>
        Pilih Obat
      </Option>
      {items.map(item => (
        <Option key={item.id} value={item.id} title={item.name}>
          {item.name}
        </Option>
      ))}
    </Select>
  );
};

export default ChooseMedicine;
