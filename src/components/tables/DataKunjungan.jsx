import React, { useState, useContext, useRef } from "react";

import { Button } from "antd";
import {
  DataTable,
  DisplayDate,
  Typography,
  deleteConfirm,
  PrintResultTest
} from "components";
import { ROLE, ENUM } from "constant";
import GlobalContext from "containers/GlobalContext";
import ReactToPrint from "react-to-print";

const { STATUS_PATIENT } = ENUM;

export const DataKunjungan = ({
  toggleModalHIV,
  toggleModalIMS,
  toggleModalIMSLab,
  toggleModalPDP,
  toggleModalMedicine,
  toggleModalRecipe,
  toggleModalRecipeIms,
  toggleModalVLCD,
  toggleModalProfilaksis,
  data,
  role,
  isLoading,
  addVisit,
  addVisitLab,
  endVisit,
  giveMedicine,
  dataPasien,
  statusPatient,
  deteleVisit
}) => {
  const user = useContext(GlobalContext);
  const [dataHivPrint, setDataHivPrint] = useState([]);
  const [dataPrescription, setDataPrescription] = useState([]);
  const printComponent = useRef();
  const printComponentHiv = useRef();
  const listPrintHiv = [];
  const isRR = role === ROLE.RR_STAFF;
  const isDoctor = role === ROLE.DOCTOR;
  const isPharma = role === ROLE.PHARMA_STAFF;
  const isLab = role === ROLE.LAB_STAFF;
  const isAdd = true;
  const [index, setIndex] = useState(0);
  const [isPrintIms, setIsPrintIms] = useState(false)
  const ButtonAddVisit = <Button onClick={addVisit}>Tambah Kunjungan</Button>;
  const ButtonAddVisitLab = (
    <Button onClick={addVisitLab}>Tambah Test Lab</Button>
  );
  const isDelete = item =>
    (
      (
        (
          item.upkId === user.upkId &&
          !item.testIms && (
            user.role === ROLE.RR_STAFF ||
            user.role === ROLE.DOCTOR
          )
        ) && (isRR || isLab || isDoctor)
      ) && (
        (
          item.upkId === user.upkId &&
          item.testHiv === null && (
            user.role === ROLE.RR_STAFF ||
            user.role === ROLE.DOCTOR
          )
        ) && (isRR || isLab || isDoctor)
      )
    ) || (
      (
        (
          item.upkId === user.upkId &&
          !item.testIms && (
            user.role === ROLE.RR_STAFF ||
            user.role === ROLE.DOCTOR
          )
        ) && (isRR || isLab || isDoctor)
      ) && (
        (
          item.upkId === user.upkId &&
          item.testHiv === null && (
            user.role === ROLE.RR_STAFF ||
            user.role === ROLE.DOCTOR
          )
        ) && (isRR || isLab || isDoctor)
      ) && (
        (
          item.upkId === user.upkId &&
          item.checkOutDate === null &&
          item.treatment === null &&
          (statusPatient === STATUS_PATIENT.ODHA || statusPatient === STATUS_PATIENT.ODHA_LAMA) &&
          item.visitType === ENUM.VISIT_TYPE.TREATMENT
        ) && isRR
      ) && (
        (
          item.upkId === user.upkId &&
          item.testVL === null &&
          user.role === ROLE.RR_STAFF &&
          (statusPatient === STATUS_PATIENT.ODHA || statusPatient === STATUS_PATIENT.ODHA_LAMA) &&
          item.visitType === ENUM.VISIT_TYPE.TREATMENT
        ) && (isRR || isLab || isDoctor)
      )
    );
  const columnOptions = {
    ordinal: {
      title: "Kunjungan ke",
      dataIndex: "ordinal",
      key: "ordinal"
    },
    visitDate: {
      title: "Tanggal",
      dataIndex: "visitDate",
      key: "visitDate",
      render: visitDate => <DisplayDate date={visitDate} />
    },
    status: {
      title: "Status",
      dataIndex: "checkOutDate",
      key: "checkOutDate",
      render: checkOutDate => {
        if (checkOutDate === null) {
          return (
            <Typography fontWeight="bold" fontSize="12px" color="red">
              Dalam Kunjungan
            </Typography>
          );
        }
        return (
          <Typography fontWeight="bold" fontSize="12px" color="green">
            Kunjungan Selesai
          </Typography>
        );
      }
    },
    statusTransit: {
      title: "Status Transit",
      key: "statusTransit",
      render: data => {
        if (data.upkId !== user.upkId && data.treatment && data.treatment.isOnTransit) {
          return (
            <Typography fontWeight="bold" fontSize="12px" color="red">
              Transit di ({data.upk.name || "-"})
            </Typography>
          );
        }
        if (data.upkId !== user.upkId) {
          return (
            <Typography fontWeight="bold" fontSize="12px" color="red">
              Kunjungan di ({data.upk.name || "-"})
            </Typography>
          );
        }
        if (data.treatment && data.treatment.isOnTransit) {
          return (
            <Typography fontWeight="bold" fontSize="12px" color="red">
              Transit dari ({data.treatment && data.treatment.upkSebelumnyaData ? data.treatment.upkSebelumnyaData.name : "-"})
            </Typography>
          );
        }
        return "-";
      }
    },
    pdp: {
      title: "Data Perawatan",
      key: "pdp",
      render: item => {
        if (
          item.upkId === user.upkId &&
          item.treatment !== null &&
          (statusPatient === STATUS_PATIENT.ODHA || statusPatient === STATUS_PATIENT.ODHA_LAMA) &&
          item.visitType === ENUM.VISIT_TYPE.TREATMENT
          ) {
            // const idx = data.indexOf(item);
            // if (idx != 0) {
            //   setPreviousDataPrescription(data[idx - 1]);
            // }
            return (
            <ButtonWrapper
              title="data PDP"
              isEdit={isRR}
              onClick={() => toggleModalPDP(item.id, false, item.visitDate)}
              role={role}
            />
          );
        } else if (
          item.upkId === user.upkId &&
          item.checkOutDate === null &&
          item.treatment === null &&
          (statusPatient === STATUS_PATIENT.ODHA || statusPatient === STATUS_PATIENT.ODHA_LAMA) &&
          item.visitType === ENUM.VISIT_TYPE.TREATMENT
        ) {
          return (
            <ButtonWrapper
              title="data PDP"
              isAdd={isRR}
              onClick={() => toggleModalPDP(item.id, isAdd, item.visitDate)}
              role={role}
            />
          );
        }
      }
    },
    hiv: {
      title: "Pemeriksaan Hasil Lab HIV",
      key: "hivExam",
      render: item => {
        if (
          item.upkId === user.upkId &&
          item.testHiv !== null &&
          (user.role === ROLE.LAB_STAFF || user.role === ROLE.RR_STAFF || user.role === ROLE.DOCTOR)
        ) {
          return (
            <ButtonWrapper
              title="data HIV"
              isEdit={isRR || isLab || isDoctor}
              onClick={() => toggleModalHIV(item.id)}
              role={role}
            />
          );
        } else if (item.upkId === user.upkId && item.testHiv === null && (user.role === ROLE.RR_STAFF || user.role === ROLE.DOCTOR)) {
          return (
            <ButtonWrapper
              title="data HIV"
              isAdd={isRR || isLab || isDoctor}
              onClick={() => toggleModalHIV(item.id, isAdd)}
              role={role}
            />
          );
        } else {
          return "-";
        }
      }
    },
    ims: {
      title: "Pemeriksaan IMS",
      key: "imsExam",
      render: item => {
        if (
          item.upkId === user.upkId &&
          item.testIms &&
          (user.role === ROLE.LAB_STAFF || user.role === ROLE.RR_STAFF || user.role === ROLE.DOCTOR)
        ) {
          return (
            <ButtonWrapper
              title="data IMS"
              isEdit={isRR || isLab || isDoctor}
              onClick={() => toggleModalIMS(item.id, false, item)}
              role={role}
            />
          );
        } else if (item.upkId === user.upkId && !item.testIms && (user.role === ROLE.RR_STAFF || user.role === ROLE.DOCTOR)) {
          return (
            <ButtonWrapper
              title="data IMS"
              isAdd={isRR || isLab || isDoctor}
              onClick={() => toggleModalIMS(item.id, isAdd)}
              role={role}
            />
          );
        } else {
          return "-";
        }
      }
    },
    imsDokter: {
      title: "Pemeriksaan IMS",
      key: "imsExam",
      render: item => {
        if (
          item.upkId === user.upkId &&
          item.testIms &&
          (item.testIms.complaint || Object.keys(item.testIms.physicalExamination).length) &&
          !item.visitImsLab
        ) {
          return (
            <ButtonWrapper
              title="data IMS"
              isEdit={true}
              onClick={() => toggleModalIMS(item.id, false, item)}
              role={role}
            />
          );
        } else if (
          item.upkId === user.upkId &&
          item.testIms &&
          !item.testIms.complaint &&
          !Object.keys(item.testIms.physicalExamination).length) {
          return (
            <ButtonWrapper
              title="data IMS"
              isAdd={true}
              onClick={() => toggleModalIMS(item.id, false, item)}
              role={role}
            />
          );
        } else {
          return "-";
        }
      }
    },
    imsLab: {
      title: "Tes Lab IMS",
      key: "imslab",
      render: item => {
        if (
          item.upkId === user.upkId &&
          item.testIms &&
          (item.testIms.complaint || Object.keys(item.testIms.physicalExamination).length) &&
          item.visitImsLab
        ) {
          return (
            <ButtonWrapper
              title="data IMS"
              isEdit={true}
              onClick={() => toggleModalIMS(item.id, false, item)}
              role={role}
            />
          );
        } else if (
          item.upkId === user.upkId &&
          item.testIms &&
          (item.testIms.complaint || Object.keys(item.testIms.physicalExamination).length) &&
          !item.visitImsLab) {
          return (
            <ButtonWrapper
              title="data IMS"
              isAdd={true}
              onClick={() => toggleModalIMS(item.id, false, item)}
              role={role}
            />
          );
        } else {
          return "-";
        }
      }
    },
    imsDokterLab: {
      title: "Pemeriksaan Hasil Lab IMS",
      key: "labImsExam",
      render: (item, itens, i) => {
        if (i !== data.length - 1) {
          i += 1;
        }
        
        if (
          item.upkId === user.upkId &&
          item.testIms &&
          (item.testIms.complaint || Object.keys(item.testIms.physicalExamination).length) &&
          item.visitImsLab &&
          (Object.keys(item.visitImsLab.diagnosisSyndrome).length || Object.keys(item.visitImsLab.diagnosisKlinis).length || Object.keys(item.visitImsLab.diagnosisLab).length)
        ) {
          return (
            <ButtonWrapper
              title="data IMS"
              isEdit={true}
              onClick={() => toggleModalIMSLab(item.id, false, item, data[i])}
              role={role}
            />
          );
        } else if (
          item.upkId === user.upkId &&
          item.testIms &&
          item.visitImsLab) {
          return (
            <ButtonWrapper
              title="data IMS"
              isAdd={true}
              onClick={() => toggleModalIMSLab(item.id, true, item, data[i])}
              role={role}
            />
          );
        } else {
          return "-";
        }
      }
    },
    vlcd: {
      title: "Test VL/CD4",
      key: "vlcd4Exam",
      render: item => {
        if (
          item.upkId === user.upkId &&
          item.testVL !== null &&
          ((user.role === ROLE.LAB_STAFF && statusPatient === STATUS_PATIENT.ODHA || statusPatient === STATUS_PATIENT.ODHA_LAMA) || (user.role === ROLE.RR_STAFF)) &&
          item.visitType === ENUM.VISIT_TYPE.TREATMENT
        ) {
          return (
            <ButtonWrapper
              isEdit={isRR || isLab || isDoctor}
              title="data VL/CD4"
              onClick={() => toggleModalVLCD(item.id)}
              role={role}
            />
          );
        } else if (
          item.upkId === user.upkId &&
          item.testVL === null &&
          ((user.role === ROLE.LAB_STAFF && statusPatient === STATUS_PATIENT.ODHA || statusPatient === STATUS_PATIENT.ODHA_LAMA) || (user.role === ROLE.RR_STAFF)) &&
          item.visitType === ENUM.VISIT_TYPE.TREATMENT
        ) {
          return (
            <ButtonWrapper
              isAdd={isRR || isLab || isDoctor}
              title="data VL/CD4"
              onClick={() => toggleModalVLCD(item.id, isAdd)}
              role={role}
            />
          );
        } else {
          return "-";
        }
      }
    },
    recipe: {
      title: "Resep Obat",
      key: "recipe",
      render: item => {
        if (item.upkId === user.upkId && item.treatment !== null) {
          return (
            <ButtonWrapper
              title="data Resep Obat"
              isEdit={isPharma || isRR}
              onClick={() => toggleModalRecipe(item.id)}
              role={role}
            />
          );
        }
      }
    },
    recipeIms: {
      title: "Resep Obat IMS",
      key: "recipe ims",
      render: item => {
        if (item.upkId === user.upkId &&
          item.visitImsLab && item.visitImsLab &&
          (Object.keys(item.visitImsLab.diagnosisSyndrome).length || Object.keys(item.visitImsLab.diagnosisKlinis).length || Object.keys(item.visitImsLab.diagnosisLab).length)) {
          return (
            <ButtonWrapper
              title="data Resep Obat IMS"
              isAdd={!item.visitPrescription ? true : false}
              isEdit={item.visitPrescription ? true : false}
              onClick={() => toggleModalRecipeIms(item.id, item)}
              role={role}
            />
          );
        }
      }
    },
    sisaObat: {
      title: "Sisa Obat",
      key: "sisaObat",
      render: item => {
        if (item.upkId === user.upkId && item.treatment !== null && (statusPatient === STATUS_PATIENT.ODHA || statusPatient === STATUS_PATIENT.ODHA_LAMA)) {
          return (
            <ButtonWrapper
              title="data Sisa Obat"
              isEdit={isPharma}
              onClick={() => toggleModalMedicine(item.id)}
              role={role}
            />
          );
        }
      }
    },
    profilaksis: {
      title: "Profilaksis",
      key: "profilaksis",
      render: item => {
        if (item.upkId === user.upkId && isPharma) {
          return (
            <ButtonWrapper
              title="data Profilaksis"
              isAdd={!item.profilaksis}
              isEdit={item.profilaksis}
              onClick={() => toggleModalProfilaksis(item.id)}
              role={role}
            />
          );
        }
      }
    },
    aksi: {
      title: "Aksi",
      key: "aksi",
      fixed: "right",
      render: (item, item2, idx) => {
        listPrintHiv[item.id] = printComponentHiv;
        if (item.upkId === user.upkId && item.treatment !== null && item.treatment.ordinal === 1) {
          setDataHivPrint(item.prevTestHiv);
          setDataPrescription(item.prescription);
        }
        return (
          <div className="d-flex flex-column">
            {item.upkId === user.upkId && item.checkOutDate === null &&
              item.treatment !== null &&
              (isPharma || isRR) && (
                <React.Fragment>
                  {/* <Button
                    size="small"
                    onClick={() => giveMedicine(item.id)}
                    className={"mb-1 hs-btn-outline " + role}
                  >
                    Beri Obat
                  </Button>
                  <br /> */}
                </React.Fragment>
              )}
            {item.upkId === user.upkId && item.checkOutDate === null && isRR && (
              <React.Fragment>
                {/* <Button
                  size="small"
                  onClick={() => endVisit(item.id)}
                  className={"mb-1 hs-btn-outline " + role}
                >
                  Kunjungan Selesai
                </Button>
                <br /> */}
              </React.Fragment>
            )}
            {/* {item.upkId === user.upkId && item.checkOutDate === null && isDoctor && (
              <React.Fragment>
                <Button
                  size="small"
                  onClick={() => toggleModalIMS(item.id, false, item)}
                  className={"mb-1 hs-btn " + role}
                >
                  Detail
                </Button>
                <br />
              </React.Fragment>
            )} */}
            {item.upkId === user.upkId && item.treatment !== null && item.treatment.ordinal === 1 && isRR && (
              <ReactToPrint
                className="mb-1 "
                onClick={() => console.log("print")}
                trigger={() => (
                  <Button
                    type="primary"
                    size="small"
                    icon="printer"
                    onClick={() => console.log("print")}
                  >
                    Print Ikhtisar
                  </Button>
                )}
                content={() => printComponent.current}
                bodyClass="p-5"
              />
            )}
            {/* setIsPrintIms */}
            {item.upkId === user.upkId && !!item.testHiv && !!item.testHiv.tanggalTest && isLab &&
              <ReactToPrint
                className="mb-1 "
                trigger={() => (
                  <Button type="primary" size="small" icon="printer" className="border border-white">
                    Print Hasil Lab (HIV)
                  </Button>
                )}
                onBeforeGetContent={() => {
                  return new Promise((resolve) => {
                    setIndex(idx)
                    setIsPrintIms(false);
                    setTimeout(() => resolve(null), 1000)
                  })
                }}
                content={() => listPrintHiv[item.id].current}
                bodyClass="p-5"
              />
            }
            {item.upkId === user.upkId && !!item.testIms && isLab &&
              <ReactToPrint
                className="mb-1 "
                trigger={() => (
                  <Button type="primary" size="small" icon="printer" className="border border-white">
                    Print Hasil Lab (IMS)
                  </Button>
                )}
                onBeforeGetContent={() => {
                  return new Promise((resolve) => {
                    setIndex(idx)
                    setIsPrintIms(true);
                    setTimeout(() => resolve(null), 1000)
                  })
                }}
                content={() => listPrintHiv[item.id].current}
                bodyClass="p-5"
              />
            }
            {
              isDelete(item) &&
              (
                <Button
                  className="hs-btn-outline"
                  type="danger"
                  size="small"
                  // icon="trash"
                  onClick={() => deteleVisit(item.id)}
                >
                  Hapus
                </Button>
              )
            }
          </div>
        );
      }
    }
  };

  const columns = {
    [ROLE.RR_STAFF]: [
      columnOptions.ordinal,
      columnOptions.visitDate,
      columnOptions.status,
      columnOptions.statusTransit,
      columnOptions.ims,
      columnOptions.hiv,
      columnOptions.pdp,
      columnOptions.vlcd,
      columnOptions.aksi
    ],
    [ROLE.DOCTOR]: [
      columnOptions.ordinal,
      columnOptions.visitDate,
      columnOptions.status,
      columnOptions.statusTransit,
      columnOptions.pdp,
      columnOptions.hiv,
      columnOptions.imsDokter,
      columnOptions.imsDokterLab,
      columnOptions.vlcd,
      columnOptions.aksi
    ],
    [ROLE.LAB_STAFF]: [
      columnOptions.ordinal,
      columnOptions.visitDate,
      columnOptions.status,
      columnOptions.hiv,
      columnOptions.imsLab,
      columnOptions.vlcd,
      columnOptions.aksi
    ],
    [ROLE.PHARMA_STAFF]: [
      columnOptions.ordinal,
      columnOptions.visitDate,
      columnOptions.status,
      columnOptions.recipe,
      columnOptions.recipeIms,
      columnOptions.profilaksis
    ]
  };

  return (
    <div className="bg-white rounded p-4 border">
      <Typography
        fontSize="14px"
        color={role}
        fontWeight="bold"
        className="d-block mb-2"
      >
        Data Kunjungan
      </Typography>
      <DataTable
        columns={columns[role]}
        data={data}
        isScrollX
        button={isRR ? ButtonAddVisit : isLab ? ButtonAddVisitLab : null}
        isLoading={isLoading}
      />
      <div hidden>
        <PrintResultTest
          title={"Hasil Pemeriksaan Laboratorium"}
          dataHiv={data[index] && data[index].testHiv}
          dataIms={data[index] && {...data[index].testIms, lab: data[index].visitImsLab}}
          isPrintIms={isPrintIms}
          printComponent={printComponentHiv}
          dataPasien={dataPasien}
        ></PrintResultTest>
      </div>
      <div hidden>
        <PrintResultTest
          title="Hasil Ikhtisar"
          dataHiv={dataHivPrint}
          printComponent={printComponent}
          dataPasien={dataPasien}
          dataPrescription={dataPrescription}
          isShowResep={true}
        ></PrintResultTest>
      </div>
    </div>
  );
};

const ButtonWrapper = ({
  isAdd = false,
  isEdit,
  role,
  onClick,
  title = ""
}) => {
  let className = `hs-btn${!isEdit ? "-outline" : ""} ${role}`;
  return (
    <Button
      size="small"
      onClick={() =>
        isAdd || isEdit
          ? deleteConfirm({
            onDelete: () => onClick(),
            message:
              `Anda yakin akan ` +
              (isAdd ? "menambah " : isEdit ? "mengubah " : "melihat ") +
              title +
              `?`,
            typeConfirm: "warning"
          })
          : onClick
      }
      className={className}
    >
      {isAdd ? "Tambah" : isEdit ? "Ubah" : "Lihat"}
    </Button>
  );
};
