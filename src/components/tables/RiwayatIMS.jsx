import React from "react";

import { Button } from "antd";
import { DataTable, DisplayDate, Typography } from "components";

export const RiwayatIMS = ({ data, isLoading, role }) => {
  return (
    <div className="bg-white rounded p-4 border">
      <Typography
        fontSize="14px"
        color={role}
        fontWeight="bold"
        className="d-block mb-2"
      >
        Riwayat Pemeriksaan IMS
      </Typography>
      <DataTable columns={columns} data={data} isLoading={isLoading} />
    </div>
  );
};

const columns = [
  {
    title: "Pemeriksaan ke",
    dataIndex: "testIms",
    key: "ordinal",
    render: testIms => testIms.ordinal
  },
  {
    title: "Kunjungan ke",
    dataIndex: "ordinal",
    key: "visitNumber"
  },
  {
    title: "Tanggal Pemeriksaan",
    dataIndex: "visitDate",
    key: "visitDate",
    render: visitDate => <DisplayDate date={visitDate} />
  },
  {
    title: "Aksi",
    key: "action",
    render: text => {
      return (
        <Button type="primary" size="small" className="hs-btn RR_STAFF">
          Detail
        </Button>
      );
    }
  }
];
