import React from "react";

import { Spin } from "antd";
import { Table } from "reactstrap";
import { Typography } from "components";
import { c } from "constant";
import './style.css'
const { VARIABLE_TYPE } = c;

const LBPHA1 = ({ data, isLoading, title, from }) => {
  let total = [];
  return (
    <React.Fragment>
      <Typography
        fontSize="15px"
        fontWeight="bold"
        className="d-block my-3 text-center"
      >
        {title}
      </Typography>
      {isLoading && (
        <div className="text-center">
          <Spin />
        </div>
      )}
      <Table bordered responsive striped style={{ fontSize: 12 }}>
        <thead className="text-center">
          <tr>
            <th rowSpan="2" className={'sticky-td'} style={{ paddingRight: "300px" }}>
              Variabel
            </th>
            <th colSpan="11">Laki-Laki</th>
            <th colSpan="11">Perempuan</th>
            <th rowspan="2">Total</th>
            <th colSpan="13">Kelompok Populasi Khusus</th>
          </tr>
          <tr>
            <th>&#60; 1</th>
            <th>1-14</th>
            <th>15-19</th>
            <th>20-24</th>
            <th>25-29</th>
            <th>30-34</th>
            <th>35-39</th>
            <th>40-44</th>
            <th>45-49</th>
            <th>{'>'} 50</th>
            <th>Jumlah</th>
            <th>&#60; 1</th>
            <th>1-14</th>
            <th>15-19</th>
            <th>20-24</th>
            <th>25-29</th>
            <th>30-34</th>
            <th>35-39</th>
            <th>40-44</th>
            <th>45-49</th>
            <th>{'>'} 50</th>
            <th>Jumlah</th>
            <th>WPS</th>
            <th>Waria</th>
            <th>Penasun</th>
            <th>LSL</th>
            <th>Bumil</th>
            <th>Pasien TB</th>
            <th>Pasien IMS</th>
            <th>Pasien Hepatitis</th>
            <th>Pasangan ODHA</th>
            <th>Anak ODHA</th>
            <th>WBP</th>
            <th>Populasi Umum</th>
            <th>Total</th>
          </tr>
        </thead>
        <tbody>
          {data.map((value, idx) => {
            const key = Object.keys(value)[0];
            // let title = null;
            // if (from && from === "DAMPAK ART" && idx === 4) {
            //   title = <tr><td className={'sticky-td'} width="400px">3.5 Jumlah kumulatif ODHA dengan ART s/d akhir bulan ini</td></tr>;
            // }
            return (
              <React.Fragment key={idx}>
                {/* {title} */}
                <tr>
                  <td className={'sticky-td'} width="400px">{VARIABLE_TYPE[key]}</td>
                  {Object.keys(value[key]).map((key2, idx2) => (
                    <React.Fragment key={idx2}>
                      {value[key][key2].map((value2, idx3) => {
                        if (total[idx] === undefined) {
                          total[idx] = 0;
                        }
                        total[idx] += value2.jumlah || 0;

                        const num = value2.count || value2.jumlah;
                        if ((value2.jumlah || value2.jumlah >= 0) && key2 == "PEREMPUAN") {
                          return (
                            <React.Fragment>
                              <td key={idx3}>{num || 0}</td>
                              <td key={idx3 + 999}>{total[idx]}</td>
                            </React.Fragment>
                          )  
                        }
                        return <td key={idx3}>{num || 0}</td>;
                      })}
                    </React.Fragment>
                  ))}
                </tr>
              </React.Fragment>
            );
          })}
        </tbody>
      </Table>
    </React.Fragment>
  );
};

export default LBPHA1;
