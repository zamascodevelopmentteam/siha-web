import * as React from "react";
import { observer, inject } from "mobx-react";
import { c } from "../constant";
import { ICommonStore } from "../stores/commonStore";

interface CardHeaderProps {
  isRealData: boolean;
  useGroupByPopulation?: boolean;
}

interface InjectedProps extends CardHeaderProps {
  commonStore: ICommonStore;
}

interface ICardHeaderState {}

@inject(c.STORES.COMMON)
@observer
class CardHeader extends React.Component<CardHeaderProps, ICardHeaderState> {
  // constructor(props) {
  //   super(props);
  // }

  get injected() {
    return this.props as InjectedProps;
  }

  render() {
    const { isRealData, useGroupByPopulation } = this.props;
    const classHeader = "bg-primary text-white text-center";
    const styleWidth_1 = { minWidth: "4rem", paddingBottom: "0.5rem" };
    const styleWidth_2 = { minWidth: "6rem", paddingBottom: "0.5rem" };
    return (
      <React.Fragment>
        <thead>
          <tr className={classHeader}>
            <th rowSpan={2} style={{ minWidth: "30rem", maxWidth: "30rem" }}>
              Variabel
            </th>
            {!isRealData ? (
              <React.Fragment>
                <th colSpan={11} className="pt-2">
                  Laki-Laki
                </th>
                <th colSpan={11} className="pt-2">
                  Perempuan
                </th>
                {useGroupByPopulation && (
                  <th colSpan={12} className="pt-2">
                    Kelompok Populasi Khusus
                  </th>
                )}
              </React.Fragment>
            ) : (
              <th rowSpan={2} style={styleWidth_2}>
                TOTAL
              </th>
            )}
          </tr>
          {!isRealData ? (
            <React.Fragment>
              <tr className={classHeader}>
                <th style={styleWidth_1}>{"<1"}</th>
                <th style={styleWidth_1}>1-14</th>
                <th style={styleWidth_1}>15-19</th>
                <th style={styleWidth_1}>20-24</th>
                <th style={styleWidth_1}>25-29</th>
                <th style={styleWidth_1}>30-34</th>
                <th style={styleWidth_1}>35-39</th>
                <th style={styleWidth_1}>40-44</th>
                <th style={styleWidth_1}>45-49</th>
                <th style={styleWidth_1}>
                  <u>{">"}</u>50
                </th>
                <th style={styleWidth_1}>Jumlah</th>

                <th style={styleWidth_1}>{"<1"}</th>
                <th style={styleWidth_1}>1-14</th>
                <th style={styleWidth_1}>15-19</th>
                <th style={styleWidth_1}>20-24</th>
                <th style={styleWidth_1}>25-29</th>
                <th style={styleWidth_1}>30-34</th>
                <th style={styleWidth_1}>35-39</th>
                <th style={styleWidth_1}>40-44</th>
                <th style={styleWidth_1}>45-49</th>
                <th style={styleWidth_1}>
                  <u>{">"}</u>50
                </th>
                <th style={styleWidth_1}>Jumlah</th>
                {useGroupByPopulation && (
                  <React.Fragment>
                    <th style={styleWidth_1}>WPS</th>
                    <th style={styleWidth_1}>Waria</th>
                    <th style={styleWidth_1}>Penasun</th>
                    <th style={styleWidth_1}>LSL</th>
                    <th style={styleWidth_1}>Bumil</th>
                    <th style={styleWidth_1}>Pasien TB</th>
                    <th style={styleWidth_1}>Pasien IMS</th>
                    <th style={styleWidth_1}>Pasien Hepatitis</th>
                    <th style={styleWidth_1}>Pasangan ODHA</th>
                    <th style={styleWidth_1}>Anak ODHA</th>
                    <th style={styleWidth_1}>WBP</th>
                    <th style={styleWidth_1}>Lainnya</th>
                  </React.Fragment>
                )}
              </tr>
            </React.Fragment>
          ) : (
            <tr></tr>
          )}
        </thead>
      </React.Fragment>
    );
  }
}

export default CardHeader;
