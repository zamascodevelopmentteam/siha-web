import * as React from "react";
import { observer, inject } from "mobx-react";
import { c } from "../constant";
import { ICommonStore } from "../stores/commonStore";

interface CardBodyProps {
  isRealData: boolean;
  data: any;
}

interface InjectedProps extends CardBodyProps {
  commonStore: ICommonStore;
}

interface ICardBodyState {}

@inject(c.STORES.COMMON)
@observer
class CardBody extends React.Component<CardBodyProps, ICardBodyState> {
  // constructor(props) {
  //   super(props);
  // }

  get injected() {
    return this.props as InjectedProps;
  }

  render() {
    const { isRealData, data } = this.props;
    const styleWidth_1 = { minWidth: "4rem", paddingBottom: "0.5rem" };
    const styleWidth_2 = { minWidth: "6rem", paddingBottom: "0.5rem" };
    const classBg_0 = "bg-gray-soft text-center p-2 pt-3";
    const classBg_1 = "bg-secondary-soft text-center p-2 pt-3";
    return (
      <React.Fragment>
        <tbody>
          {data.map((item, index) => {
            return (
              <tr key={index}>
                <td
                  className="bg-gray-soft text-left border-right-gray p-3"
                  style={{ minWidth: "30rem", maxWidth: "30rem" }}
                >
                  {isRealData ? c.VARIABLE_TYPE[index] : item.name}
                </td>
                {!isRealData && (
                  <React.Fragment>
                    <td className={classBg_0} style={styleWidth_1}>
                      {item.data_1}
                    </td>
                    <td className={classBg_1} style={styleWidth_1}>
                      {item.data_2}
                    </td>
                    <td className={classBg_0} style={styleWidth_1}>
                      {item.data_3}
                    </td>
                    <td className={classBg_1} style={styleWidth_1}>
                      {item.data_4}
                    </td>
                    <td className={classBg_0} style={styleWidth_1}>
                      {item.data_5}
                    </td>
                    <td className={classBg_1} style={styleWidth_1}>
                      {item.data_6}
                    </td>
                    <td className={classBg_0} style={styleWidth_1}>
                      {item.data_7}
                    </td>
                    <td className={classBg_1} style={styleWidth_1}>
                      {item.data_8}
                    </td>
                    <td className={classBg_0} style={styleWidth_1}>
                      {item.data_9}
                    </td>
                    <td className={classBg_1} style={styleWidth_1}>
                      {item.data_10}
                    </td>
                    <td className={classBg_0} style={styleWidth_1}>
                      {item.data_11}
                    </td>
                    <td className={classBg_1} style={styleWidth_2}>
                      {item.data_12}
                    </td>

                    <td className={classBg_0} style={styleWidth_1}>
                      {item.data_13}
                    </td>
                    <td className={classBg_1} style={styleWidth_1}>
                      {item.data_14}
                    </td>
                    <td className={classBg_0} style={styleWidth_1}>
                      {item.data_15}
                    </td>
                    <td className={classBg_1} style={styleWidth_1}>
                      {item.data_16}
                    </td>
                    <td className={classBg_0} style={styleWidth_1}>
                      {item.data_17}
                    </td>
                    <td className={classBg_1} style={styleWidth_1}>
                      {item.data_18}
                    </td>
                    <td className={classBg_0} style={styleWidth_1}>
                      {item.data_19}
                    </td>
                    <td className={classBg_1} style={styleWidth_1}>
                      {item.data_20}
                    </td>
                    <td className={classBg_0} style={styleWidth_1}>
                      {item.data_21}
                    </td>
                    <td className={classBg_1} style={styleWidth_1}>
                      {item.data_22}
                    </td>
                    <td className={classBg_0} style={styleWidth_1}>
                      {item.data_23}
                    </td>
                    <td className={classBg_1} style={styleWidth_2}>
                      {item.data_24}
                    </td>

                    <td className={classBg_0} style={styleWidth_2}>
                      {item.data_25}
                    </td>
                    <td className={classBg_1} style={styleWidth_2}>
                      {item.data_26}
                    </td>
                    <td className={classBg_0} style={styleWidth_2}>
                      {item.data_27}
                    </td>
                    <td className={classBg_1} style={styleWidth_2}>
                      {item.data_28}
                    </td>
                    <td className={classBg_0} style={styleWidth_2}>
                      {item.data_29}
                    </td>
                    <td className={classBg_1} style={styleWidth_2}>
                      {item.data_30}
                    </td>
                    <td className={classBg_0} style={styleWidth_2}>
                      {item.data_31}
                    </td>
                    <td className={classBg_1} style={styleWidth_2}>
                      {item.data_32}
                    </td>
                    <td className={classBg_0} style={styleWidth_2}>
                      {item.data_33}
                    </td>
                    <td className={classBg_1} style={styleWidth_2}>
                      {item.data_34}
                    </td>
                    <td className={classBg_0} style={styleWidth_2}>
                      {item.data_35}
                    </td>
                    <td className={classBg_1} style={styleWidth_2}>
                      {item.data_36}
                    </td>
                  </React.Fragment>
                )}
                <td className={classBg_0} style={styleWidth_2}>
                  {isRealData ? item : item.data_37}
                </td>
              </tr>
            );
          })}
        </tbody>
      </React.Fragment>
    );
  }
}

export default CardBody;
