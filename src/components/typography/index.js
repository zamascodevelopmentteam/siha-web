import React from "react";
import { TextWrapper } from "./style";

const Typography = ({ color, children, fontSize, fontWeight, ...props }) => {
  return (
    <TextWrapper
      color={color || "black"}
      fontSize={fontSize || "15px"}
      fontWeight={fontWeight || "normal"}
      {...props}
    >
      {children}
    </TextWrapper>
  );
};

Typography.defaultProps = {
  color: "black",
  fontSize: "15px",
  fontWeight: "normal"
};

export default Typography;
