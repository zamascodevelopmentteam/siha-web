import styled from "styled-components";
import { colors } from "constant";

export const TextWrapper = styled.span`
  font-size: ${props => props.fontSize || "15px"};
  font-weight: ${props => props.fontWeight || "normal"};
  color: ${props => colors[props.color] || colors.black};
  ${props => (props.underLine ? "text-decoration: underline;" : "")}
  ${props => (props.ellipsis ? "text-overflow: ellipsis;" : "")}
`;
