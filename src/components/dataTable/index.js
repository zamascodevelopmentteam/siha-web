import React from "react";
import { debounce } from "lodash";
import { Input, Table } from "antd";
import { Typography } from "components";
const { Search } = Input;

const DataTable = ({
  title,
  onSearch,
  searchPlaceholder,
  columns,
  data,
  button,
  pagination,
  onChange,
  isLoading,
  isScrollX,
  rowKey
}) => {
  const _onSearch = debounce(value => {
    onSearch(value);
  }, 1000);

  return (
    <div>
      <div
        className={`d-flex  align-items-center mb-3 ${
          title ? "justify-content-between" : "justify-content-end"
        }`}
      >
        {title && (
          <Typography fontSize="16px" fontWeight="bold">
            {title}
          </Typography>
        )}
        <div>
          {onSearch && (
            <Search
              placeholder={searchPlaceholder || "Cari di sini"}
              onChange={e => _onSearch(e.target.value)}
              onSearch={value => {
                onSearch(value);
                _onSearch.cancel();
              }}
              style={{ width: 240 }}
              className={`${button ? "mr-2" : ""}`}
            />
          )}
          {button && button}
        </div>
      </div>
      <Table
        columns={columns}
        dataSource={data}
        rowKey={rowKey || "id"}
        pagination={pagination}
        onChange={onChange}
        loading={isLoading}
        scroll={{ x: isScrollX }}
      />
    </div>
  );
};

export default DataTable;
