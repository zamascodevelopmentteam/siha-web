import * as React from "react";
import { Label } from "reactstrap";

const QuickStat = ({ title, subTitle, number, description, colorClass }) => {
  return (
    <div className={`text-white card-border-radius p-3 ${colorClass} h-100`}>
      <Label className="m-0 text-left" style={{ fontSize: "14px" }}>
        <span className="font-weight-bold" style={{ fontSize: "16px" }}>
          {title}
        </span>
        <br />
        {subTitle}
      </Label>
      <br />
      <div className="text-right">
        <h2 className="m-0 text-right">{number}</h2>
        <Label className="m-0 text-right" style={{ fontSize: "14px" }}>
          {description}
        </Label>
      </div>
    </div>
  );
};

export default QuickStat;
