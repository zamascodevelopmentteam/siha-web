import React, { useState, useEffect } from "react";
import API from "utils/API";
import axios from "axios";
import { Select, Spin } from "antd";

const { Option } = Select;

const Choose = ({ sudinKabID, ...props }) => {
  let source = axios.CancelToken.source();
  const [items, setItems] = useState([]);
  const [isLoading, setLoading] = useState(false);
  const [keyword, setKeyword] = useState("");

  const _quickSearch = value => {
    const keyword = value.trim();
    setKeyword(keyword);
  };

  const _loadData = () => {
    setLoading(true);
    API.MasterData.Upk.list(source.token, 0, props.limit ? props.limit : 100, keyword)
      .then(rsp => {
        setItems(rsp.data || []);
      })
      .catch(e => {
        console.error("e: ", e);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const _loadDataBySudinkabID = () => {
    setLoading(true);
    API.MasterData.Upk.listBySudinKabID(source.token, sudinKabID)
      .then(rsp => {
        setItems(rsp.data || []);
      })
      .catch(e => {
        console.error("e: ", e);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  useEffect(() => {
    if (sudinKabID) {
      _loadDataBySudinkabID();
    } else {
      _loadData();
    }

    return () => {
      source.cancel("request canceled");
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [sudinKabID, keyword]);

  return (
    <Select
      showSearch
      optionFilterProp="children"
      notFoundContent={isLoading ? <Spin size="small" /> : null}
      onSearch={_quickSearch}
      {...props}
    >
      <Option disabled value={null}>
        Pilih UPK
      </Option>
      {items.map(item => (
        <Option key={item.id} value={item.id}>
          {item.name}
        </Option>
      ))}
    </Select>
  );
};

export default Choose;
