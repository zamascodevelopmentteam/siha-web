import * as React from "react";

interface ISeparatorThousandsProps {
  numberData: any;
}

class SeparatorThousands extends React.Component<ISeparatorThousandsProps, {}> {
  numberWithThousands(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  }

  render() {
    const { numberData } = this.props;
    return <span>{this.numberWithThousands(numberData)}</span>;
  }
}

export default SeparatorThousands;
