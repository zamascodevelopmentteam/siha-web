import React, { useContext } from "react";
import moment from "moment";

import { DatePicker } from "antd";
import { Typography, ChooseProvince, ChooseSudin, ChooseUpk } from "components";
import GlobalContext from "containers/GlobalContext";
import { ENUM, ROLE } from "constant";
import { Button } from "reactstrap";

const { MonthPicker, RangePicker } = DatePicker;

const FILTER_TYPE = {
  MONTH: "MONTH",
  RANGE: "RANGE",
  PROVINCE: "PROVINCE",
  SUDINKAB: "SUDINKAB",
  UPK: "UPK"
};

const Filter = ({
  onFilter,
  currentFilter,
  hideLocation,
  hidePeriode,
  disablePrevDate,
  disablePrevMonth,
  disableNextMonth,
  disabledDate,
  excel = false,
  onDownloadExcel
}) => {
  const user = useContext(GlobalContext);
  const isRange = typeof currentFilter.RANGE !== "undefined";

  const disabledDateRule = current => {
    if (disablePrevDate) {
      return current && current < moment().endOf("day");
    }
    if (disablePrevMonth) {
      return current && current < moment().startOf("month");
    }
    if (disableNextMonth) {
      return current && current > moment().endOf("month");
    }
    return false;
  };

  return (
    <div className="row">
      {!hidePeriode && (
        <div className={`${isRange ? "col-4 pl-2" : "col-2 pl-2"}`}>
          <Typography
            fontSize="18px"
            fontWeight="bold"
            className="d-block mb-2"
          >
            Periode
          </Typography>
          {typeof currentFilter.MONTH !== "undefined" && (
            <MonthPicker
              disabledDate={disabledDate || disabledDateRule}
              onChange={(date, dtString) =>
                onFilter(FILTER_TYPE.MONTH, dtString)
              }
              placeholder="Pilih Bulan"
            />
          )}
          {typeof currentFilter.RANGE !== "undefined" && (
            <RangePicker
              onChange={(date, dtString) =>
                onFilter(FILTER_TYPE.RANGE, dtString)
              }
              placeholder="Pilih Periode"
            />
          )}
        </div>
      )}
      {!hideLocation && (
        <div className={`${isRange ? "col-8" : "col-10"}`}>
          <div className="row">
            <div className="col-12">
              <Typography
                fontSize="18px"
                fontWeight="bold"
                className="d-block mb-2"
              >
                Lokasi
              </Typography>
            </div>
            {typeof currentFilter.PROVINCE !== "undefined" && (
              <div className="col-3">
                <ChooseProvince
                  onChange={value => onFilter(FILTER_TYPE.PROVINCE, value)}
                  placeholder="Semua Provinsi"
                  style={{ width: "100%" }}
                  defaultValue={
                    [
                      ENUM.LOGISTIC_ROLE.PROVINCE_ENTITY,
                      ENUM.LOGISTIC_ROLE.SUDIN_ENTITY,
                      ENUM.LOGISTIC_ROLE.UPK_ENTITY,
                      ENUM.LOGISTIC_ROLE.LAB_ENTITY
                    ].includes(user.logisticrole)
                      ? user.provinceId
                      : null
                  }
                  disabled={
                    ![ENUM.LOGISTIC_ROLE.MINISTRY_ENTITY].includes(
                      user.logisticrole
                    )
                  }
                  allowClear
                />
              </div>
            )}
            {typeof currentFilter.SUDINKAB !== "undefined" && (
              <div className="col-3">
                <ChooseSudin
                  onChange={value => onFilter(FILTER_TYPE.SUDINKAB, value)}
                  placeholder="Semua Kab/Kota"
                  provinceID={currentFilter.PROVINCE}
                  style={{ width: "100%" }}
                  defaultValue={
                    [
                      ENUM.LOGISTIC_ROLE.SUDIN_ENTITY,
                      ENUM.LOGISTIC_ROLE.UPK_ENTITY,
                      ENUM.LOGISTIC_ROLE.LAB_ENTITY
                    ].includes(user.logisticrole)
                      ? user.sudinKabKotaId
                      : null
                  }
                  disabled={
                    ![
                      ENUM.LOGISTIC_ROLE.MINISTRY_ENTITY,
                      ENUM.LOGISTIC_ROLE.PROVINCE_ENTITY
                    ].includes(user.logisticrole)
                  }
                  allowClear
                />
              </div>
            )}
            {typeof currentFilter.UPK !== "undefined" && (
              <div className="col-4">
                <ChooseUpk
                  onChange={value => onFilter(FILTER_TYPE.UPK, value)}
                  placeholder="Semua UPK"
                  sudinKabID={currentFilter.SUDINKAB}
                  style={{ width: "100%" }}
                  allowClear
                  defaultValue={
                    [ENUM.LOGISTIC_ROLE.UPK_ENTITY, ENUM.LOGISTIC_ROLE.LAB_ENTITY, null].includes(user.logisticrole)
                      ? user.upkId
                      : null
                  }
                  disabled={user.logisticrole === ENUM.LOGISTIC_ROLE.UPK_ENTITY || user.logisticrole === ENUM.LOGISTIC_ROLE.PHARMA_ENTITY || user.logisticrole === ENUM.LOGISTIC_ROLE.LAB_ENTITY || user.role === ROLE.RR_STAFF || user.role === ROLE.UPK_ENTITY || user.logisticrole === null}
                />
              </div>
            )}
            {excel &&
              <div className={'col-2'}>
                <Button
                  onClick={onDownloadExcel}
                  className={`hs-btn-outline ${ROLE.RR_STAFF}`}
                  icon="download"
                  size={'sm'}>
                  Download Excel
              </Button>
              </div>
            }
          </div>
        </div>
      )}
    </div>
  );
};

export default Filter;
