import React, { useEffect, useState } from "react";
import { Label } from "./style";
import { ENUM } from "constant";
import { DataTable, DisplayDate, Typography } from "components";
import axios from "axios";
import API from "utils/API";

const { HIV_TEST_TYPE, RUJUKAN_LAB_LANJUT } = ENUM;

export const PrintResultTest = ({
  title,
  dataHiv,
  dataIms,
  isPrintIms,
  dataPrescription,
  dataPasien,
  printComponent,
  isShowResep = false
}) => {
  console.log(dataIms);
  const gender = dataPasien.gender;
  const [sifilisMed, setSifilisMed] = useState(null);
  const [rprMed, setRprMed] = useState(null);

  const _loadSifilisMed = async (id) => {
    let source = axios.CancelToken.source();

    await API.MasterData.Medicine.getByID(source.token, id)
      .then(r => {
        setSifilisMed(r.data.name);
      })
      .catch(e => {
        console.error("e: ", e);
      });
  };

  const _loadRprMed = async (id) => {
    let source = axios.CancelToken.source();

    await API.MasterData.Medicine.getByID(source.token, id)
      .then(r => {
        setRprMed(r.data.name);
      })
      .catch(e => {
        console.error("e: ", e);
      });
  };

  // useEffect(async () => {
  if (isPrintIms && dataIms && dataIms.lab) {
    if (dataIms.lab.rprVdrl && dataIms.lab.rprVdrl.reagent) {
      _loadSifilisMed(dataIms.lab.rprVdrl.reagent);
    }

    if (dataIms.lab.realRprVdrl && dataIms.lab.realRprVdrl.reagent) {
      _loadRprMed(dataIms.lab.realRprVdrl.reagent);
    }
  }
  // }, [])

  return (
    <React.Fragment>
      <div className="row mt-5 pt-5" ref={printComponent}>
        <div className="col-12 bg-white rounded p-4 border">
          <div className="row">
            <div className="col-12 pl-0 pb-4 text-center">
              <Typography fontSize="20px" fontWeight="bold" className="d-block">
                {title}
              </Typography>
            </div>
            <div className="col-10">
              <table>
                <tr>
                  <td>NIK</td>
                  <td>&nbsp;:&nbsp;</td>
                  <td>{(dataPasien && dataPasien.nik) || "-"}</td>
                </tr>
                <tr>
                  <td>Nama</td>
                  <td>&nbsp;:&nbsp;</td>
                  <td>{dataPasien && dataPasien.name}</td>
                </tr>
                <tr>
                  <td>Alamat Domisini</td>
                  <td>&nbsp;:&nbsp;</td>
                  <td>{(dataPasien && dataPasien.addressDomicile) || "-"}</td>
                </tr>
                <tr>
                  <td>Kelompok Populasi</td>
                  <td>&nbsp;:&nbsp;</td>
                  <td>{dataPasien && dataPasien.kelompokPopulasi}</td>
                </tr>
                <tr>
                  <td>LSM Penjangkau</td>
                  <td>&nbsp;:&nbsp;</td>
                  <td>{(dataPasien && dataPasien.lsmPenjangkau) || "-"}</td>
                </tr>
                <tr>
                  <td>Alasan Test HIV</td>
                  <td>&nbsp;:&nbsp;</td>
                  <td>{dataHiv && dataHiv.alasanTest}</td>
                </tr>
                {!isPrintIms && (
                  <tr>
                    <td>Tanggal Test</td>
                    <td>&nbsp;:&nbsp;</td>
                    <td>
                      {dataHiv && dataHiv.tanggalTest ? (
                        <DisplayDate date={dataHiv && dataHiv.tanggalTest} />
                      ) : (
                        ""
                      )}
                    </td>
                  </tr>
                )}
                <tr>
                  <td>Layanan</td>
                  <td>&nbsp;:&nbsp;</td>
                  <td>
                    {dataPasien && dataPasien.upk ? (
                      dataPasien.upk.name
                    ) : (
                      "-"
                    )}
                  </td>
                </tr>
                <tr>
                  <td>Alamat Layanan</td>
                  <td>&nbsp;:&nbsp;</td>
                  <td>
                    {dataPasien && dataPasien.upk ? (
                      dataPasien.upk.address
                    ) : (
                      "-"
                    )}
                  </td>
                </tr>
              </table>
              {!isPrintIms ? (
                <React.Fragment>
                  <Typography
                    fontSize="20px"
                    fontWeight="bold"
                    className="d-block mb-2 mt-4"
                  >
                    Test HIV
                  </Typography>
                  <Label className="row">
                    <span className="col-4">Jenis Test</span>
                    <span>: {(dataHiv && dataHiv.jenisTest) || "-"}</span>
                  </Label>

                  {dataHiv && dataHiv.jenisTest === HIV_TEST_TYPE.RDT && (
                    <React.Fragment>
                      <Label className="row">
                        <span className="col-4">Reagen R1 {dataHiv && dataHiv['namaReagenR12Data'] ? '(Test 1)' : ''}</span>
                        {/* <span>: {(dataHiv && dataHiv.namaReagenR1) || "-"}</span> */}
                      </Label>

                      <Label className="row">
                        <span className="col-4">Nama Reagen</span>
                        <span>
                          {dataHiv && dataHiv['namaReagenR1Data'] && dataHiv['namaReagenR1Data'].codeName ?
                            `: ${dataHiv['namaReagenR1Data'].codeName}`
                            : ": -"}
                        </span>
                      </Label>

                      <Label className="row">
                        <span className="col-4">Hasil Test R1</span>
                        <span>: {(dataHiv && dataHiv.hasilTestR1) || "-"}</span>
                      </Label>

                      <Label className="row">
                        <span className="col-4">Reagen R2 {dataHiv && dataHiv['namaReagenR22Data'] ? '(Test 1)' : ''}</span>
                        {/* <span>: {(dataHiv && dataHiv.namaReagenR2) || "-"}</span> */}
                      </Label>

                      <Label className="row">
                        <span className="col-4">Nama Reagen</span>
                        <span>
                          :{" "}
                          {dataHiv && dataHiv['namaReagenR2Data'] && dataHiv['namaReagenR2Data'].codeName ?
                            `${dataHiv['namaReagenR2Data'].codeName}`
                            : "-"}
                        </span>
                      </Label>

                      <Label className="row">
                        <span className="col-4">Hasil Test R2</span>
                        <span>: {(dataHiv && dataHiv.hasilTestR2) || "-"}</span>
                      </Label>

                      {dataHiv && dataHiv['namaReagenR12Data'] && (
                        <React.Fragment>
                          <Label className="row">
                            <span className="col-4">Reagen R1 {dataHiv && dataHiv['namaReagenR12Data'] ? '(Test 2)' : ''}</span>
                            {/* <span>: {(dataHiv && dataHiv.namaReagenR1) || "-"}</span> */}
                          </Label>

                          <Label className="row">
                            <span className="col-4">Nama Reagen</span>
                            <span>
                              {dataHiv && dataHiv['namaReagenR12Data'] && dataHiv['namaReagenR12Data'].codeName ?
                                `: ${dataHiv['namaReagenR12Data'].codeName}`
                                : ": -"}
                            </span>
                          </Label>

                          <Label className="row">
                            <span className="col-4">Hasil Test R1</span>
                            <span>: {(dataHiv && dataHiv.hasilTestR12) || "-"}</span>
                          </Label>
                        </React.Fragment>
                      )}

                      {dataHiv && dataHiv['namaReagenR22Data'] && (
                        <React.Fragment>
                          <Label className="row">
                            <span className="col-4">Reagen R2 {dataHiv && dataHiv['namaReagenR22Data'] ? '(Test 2)' : ''}</span>
                            {/* <span>: {(dataHiv && dataHiv.namaReagenR1) || "-"}</span> */}
                          </Label>

                          <Label className="row">
                            <span className="col-4">Nama Reagen</span>
                            <span>
                              {dataHiv && dataHiv['namaReagenR22Data'] && dataHiv['namaReagenR22Data'].codeName ?
                                `: ${dataHiv['namaReagenR22Data'].codeName}`
                                : ": -"}
                            </span>
                          </Label>

                          <Label className="row">
                            <span className="col-4">Hasil Test R2</span>
                            <span>: {(dataHiv && dataHiv.hasilTestR22) || "-"}</span>
                          </Label>
                        </React.Fragment>
                      )}

                      <Label className="row">
                        <span className="col-4">Reagen R3</span>
                        {/* <span>: {(dataHiv && dataHiv.namaReagenR3) || "-"}</span> */}
                      </Label>

                      <Label className="row">
                        <span className="col-4">Nama Reagen</span>
                        <span>
                          :{" "}
                          {dataHiv && dataHiv.namaReagenR3Data && dataHiv.namaReagenR3Data.codeName ?
                            `${dataHiv.namaReagenR3Data.codeName}`
                            : "-"}
                        </span>
                      </Label>

                      <Label className="row">
                        <span className="col-4">Hasil Test R3</span>
                        <span>: {(dataHiv && dataHiv.hasilTestR3) || "-"}</span>
                      </Label>
                    </React.Fragment>
                  )}
                  {dataHiv && dataHiv.jenisTest === HIV_TEST_TYPE.PCR_RNA && (
                    <React.Fragment>
                      <Label className="row">
                        <span className="col-4">Reagen RNA</span>
                        {/* <span>: {(dataHiv && dataHiv.namaReagenRna) || "-"}</span> */}
                      </Label>

                      <Label className="row">
                        <span className="col-4">Hasil Test RNA</span>
                        <span>: {(dataHiv && dataHiv.hasilTestRna) || "-"}</span>
                      </Label>
                    </React.Fragment>
                  )}
                  {dataHiv && dataHiv.jenisTest === HIV_TEST_TYPE.PCR_DNA && (
                    <React.Fragment>
                      <Label className="row">
                        <span className="col-4">Reagen DNA</span>
                        {/* <span>: {(dataHiv && dataHiv.namaReagenDna) || "-"}</span> */}
                      </Label>

                      <Label className="row">
                        <span className="col-4">Hasil Test DNA</span>
                        <span>: {(dataHiv && dataHiv.hasilTestDna) || "-"}</span>
                      </Label>
                    </React.Fragment>
                  )}

                  {dataHiv && dataHiv.jenisTest === HIV_TEST_TYPE.NAT && (
                    <React.Fragment>
                      <Label className="row">
                        <span className="col-4">Reagen NAT</span>
                        {/* <span>: {(dataHiv && dataHiv.namaReagenNat) || "-"}</span> */}
                      </Label>

                      <Label className="row">
                        <span className="col-4">Hasil Test NAT</span>
                        <span>: {(dataHiv && dataHiv.hasilTestNat) || "-"}</span>
                      </Label>
                    </React.Fragment>
                  )}
                  <Typography
                    fontSize="14px"
                    fontWeight="bold"
                    className="d-block mb-2 mt-4"
                  >
                    Pemeriksaan Lab Lanjutan
                  </Typography>

                  <Label className="row">
                    <span className="col-4">
                      Rujukan untuk Pemeriksaan Lab Lanjutan
                    </span>
                    <span>: {(dataHiv && dataHiv.rujukanLabLanjut) || "-"}</span>
                  </Label>

                  {dataHiv && dataHiv.rujukanLabLanjut === RUJUKAN_LAB_LANJUT.WB && (
                    <React.Fragment>
                      <Label className="row">
                        <span className="col-4">Nama Reagen WB</span>
                        {/* <span>: {(dataHiv && dataHiv.namaReagenWb) || "-"}</span> */}
                      </Label>

                      <Label className="row">
                        <span className="col-4">Hasil Test WB</span>
                        <span>: {(dataHiv && dataHiv.hasilTestWb) || "-"}</span>
                      </Label>
                    </React.Fragment>
                  )}
                  {dataHiv &&
                    dataHiv.rujukanLabLanjut === RUJUKAN_LAB_LANJUT.ELISA && (
                      <React.Fragment>
                        <Label className="row">
                          <span className="col-4">Nama Reagen ELISA</span>
                          <span>
                            {/* : {(dataHiv && dataHiv.namaReagenElisa) || "-"} */}
                          </span>
                        </Label>

                        <span className="row">
                          <span className="col-4">Hasil Test ELISA</span>
                          <span>
                            : {(dataHiv && dataHiv.hasilTestElisa) || "-"}
                          </span>
                        </span>
                      </React.Fragment>
                    )}
                  <Typography
                    fontSize="14px"
                    fontWeight="bold"
                    className="d-block mb-2 mt-4"
                  >
                    Kesimpulan Hasil Test HIV
                  </Typography>
                  <Label>Kesimpulan Hasil Test HIV</Label>
                  <Typography fontSize="12px" fontWeight="bold" color={dataHiv && dataHiv.kesimpulanHiv && dataHiv.kesimpulanHiv == 'REAKTIF' ? 'red' : null}>
                    {dataHiv && dataHiv.kesimpulanHiv}
                  </Typography>
                  {isShowResep && dataPrescription && (
                    <React.Fragment>
                      <Typography
                        fontSize="14px"
                        fontWeight="bold"
                        className="d-block mb-2 mt-4"
                      >
                        Resep Obat
                      </Typography>

                      <Label className="row">
                        <span className="col-4">Regiment</span>
                        <span>
                          :{" "}
                          {dataPrescription && dataPrescription.paduanObatStr
                            ? dataPrescription.paduanObatStr
                            : "-"}
                        </span>
                      </Label>

                      <DataTable
                        columns={columns}
                        data={
                          dataPrescription &&
                            dataPrescription.prescriptionMedicines
                            ? dataPrescription.prescriptionMedicines
                            : []
                        }
                        rowKey="prescriptionMedicines"
                        pagination={false}
                      />
                    </React.Fragment>
                  )}
                </React.Fragment>
              ) : dataIms && dataIms.lab && (
                <React.Fragment>
                  <Typography
                    fontSize="20px"
                    fontWeight="bold"
                    className="d-block mb-2 mt-4"
                  >
                    Test IMS
                  </Typography>
                  <h6 className="font-weight-bolder">Pemeriksaan</h6>
                  <table className="mb-2">
                    {gender == ENUM.GENDER.PEREMPUAN && (
                      <React.Fragment>
                        <tr>
                          <td>PMN Serviks</td>
                          <td>&nbsp;:&nbsp;</td>
                          <td>{dataIms.lab.pmnUretraServiks ? 'Positif' : (dataIms.lab.pmnUretraServiks === false ? 'Negatif' : '-')}</td>
                        </tr>
                        <tr>
                          <td>PMN Vagina</td>
                          <td>&nbsp;:&nbsp;</td>
                          <td>{dataIms.lab.pmnVagina ? 'Positif' : (dataIms.lab.pmnVagina === false ? 'Negatif' : '-')}</td>
                        </tr>
                      </React.Fragment>
                    )}
                    <tr>
                      <td>PMN Uretra</td>
                      <td>&nbsp;:&nbsp;</td>
                      <td>{dataIms.lab.pmnUretra ? 'Positif' : (dataIms.lab.pmnUretra === false ? 'Negatif' : '-')}</td>
                    </tr>
                    <tr>
                      <td>PMN Anus</td>
                      <td>&nbsp;:&nbsp;</td>
                      <td>{dataIms.lab.pmnAnus ? 'Positif' : (dataIms.lab.pmnAnus === false ? 'Negatif' : '-')}</td>
                    </tr>
                    <tr>
                      <td>PMN Mata</td>
                      <td>&nbsp;:&nbsp;</td>
                      <td>{dataIms.lab.pmnMata ? 'Positif' : (dataIms.lab.pmnMata === false ? 'Negatif' : '-')}</td>
                    </tr>
                    {gender == ENUM.GENDER.PEREMPUAN && (
                      <React.Fragment>
                        <tr>
                          <td>Trichomonas vag</td>
                          <td>&nbsp;:&nbsp;</td>
                          <td>{dataIms.lab.trichomonasVag ? 'Positif' : (dataIms.lab.trichomonasVag === false ? 'Negatif' : '-')}</td>
                        </tr>
                      </React.Fragment>
                    )}
                    <tr>
                      <td>Clue Cells</td>
                      <td>&nbsp;:&nbsp;</td>
                      <td>{dataIms.lab.clueCells ? 'Positif' : (dataIms.lab.clueCells === false ? 'Negatif' : '-')}</td>
                    </tr>
                    {gender == ENUM.GENDER.PEREMPUAN && (
                      <React.Fragment>
                        <tr>
                          <td>Diplokakus Intrasel Serviks</td>
                          <td>&nbsp;:&nbsp;</td>
                          <td>{dataIms.lab.diplokokusIntraselUretraServiks ? 'Positif' : (dataIms.lab.diplokokusIntraselUretraServiks === false ? 'Negatif' : '-')}</td>
                        </tr>
                        <tr>
                          <td>Diplokokus intrasel Vagina</td>
                          <td>&nbsp;:&nbsp;</td>
                          <td>{dataIms.lab.diplokokusIntraselVagina ? 'Positif' : (dataIms.lab.diplokokusIntraselVagina === false ? 'Negatif' : '-')}</td>
                        </tr>
                      </React.Fragment>
                    )}
                    <tr>
                      <td>Diplokokus intrasel Uretra</td>
                      <td>&nbsp;:&nbsp;</td>
                      <td>{dataIms.lab.diplokokusIntraselUretra ? 'Positif' : (dataIms.lab.diplokokusIntraselUretra === false ? 'Negatif' : '-')}</td>
                    </tr>
                    <tr>
                      <td>Diplokokus Intrasel Anus</td>
                      <td>&nbsp;:&nbsp;</td>
                      <td>{dataIms.lab.diplokokusIntraselAnus ? 'Positif' : (dataIms.lab.diplokokusIntraselAnus === false ? 'Negatif' : '-')}</td>
                    </tr>
                    <tr>
                      <td>Diplokokus Intrasel Mata</td>
                      <td>&nbsp;:&nbsp;</td>
                      <td>{dataIms.lab.diplokokusIntraselMata ? 'Positif' : (dataIms.lab.diplokokusIntraselMata === false ? 'Negatif' : '-')}</td>
                    </tr>
                    <tr>
                      <td>Pseudohifa/blastospora</td>
                      <td>&nbsp;:&nbsp;</td>
                      <td>{dataIms.lab.pseudohifaBlastospora ? 'Positif' : (dataIms.lab.pseudohifaBlastospora === false ? 'Negatif' : '-')}</td>
                    </tr>
                  </table>
                  {dataIms.lab.pemeriksaan == "RAPID_SIFILIS" && (
                    <React.Fragment>
                      <h6 className="font-weight-bolder">Tes Sifilis</h6>
                      <table className="mb-2">
                        <tr>
                          <td>Reagen</td>
                          <td>&nbsp;:&nbsp;</td>
                          <td>{sifilisMed}</td>
                        </tr>
                        <tr>
                          <td>Jumlah Pengguna</td>
                          <td>&nbsp;:&nbsp;</td>
                          <td>{dataIms.lab.rprVdrl.totalReagentUsed}</td>
                        </tr>
                        <tr>
                          <td>Hasil</td>
                          <td>&nbsp;:&nbsp;</td>
                          <td>{dataIms.lab.rprVdrl.result}</td>
                        </tr>
                      </table>
                      {dataIms.lab.realRprVdrl.status && (
                        <React.Fragment>
                          <h6 className="font-weight-bolder">RPR/VDRL</h6>
                          <table className="mb-2">
                            <tr>
                              <td>Reagen</td>
                              <td>&nbsp;:&nbsp;</td>
                              <td>{rprMed}</td>
                            </tr>
                            <tr>
                              <td>Jumlah Pengguna</td>
                              <td>&nbsp;:&nbsp;</td>
                              <td>{dataIms.lab.realRprVdrl.totalReagentUsed}</td>
                            </tr>\
                            <tr>
                              <td>Titer</td>
                              <td>&nbsp;:&nbsp;</td>
                              <td>{dataIms.lab.rprVdrlTiter}</td>
                            </tr>
                          </table>
                        </React.Fragment>
                      )}
                    </React.Fragment>
                  )}
                  {dataIms.lab.pemeriksaan == "TPHA" && (
                    <React.Fragment>
                      <h6 className="font-weight-bolder">TPHA/TPPA (TP Rapid)</h6>
                      <table className="mb-2">
                        <tr>
                          <td>Hasil</td>
                          <td>&nbsp;:&nbsp;</td>
                          <td>{dataIms.lab.tphaTppa.result}</td>
                        </tr>
                        <tr>
                          <td>Jumlah Pengguna</td>
                          <td>&nbsp;:&nbsp;</td>
                          <td>1/{dataIms.lab.tphaTppa.totalReagentUsed}</td>
                        </tr>
                      </table>
                    </React.Fragment>
                  )}
                </React.Fragment>
              )}
            </div>
          </div>
        </div>
        <div className="pt-5 mt-5 w-100">
          <table width="100%">
            <tr>
              <td className="text-center" width="50%">Pemeriksa</td>
              <td className="text-center" width="50%">Penanggung Jawab</td>
            </tr>
            <tr>
              <td className="text-center">
                <br />
                <br />
                <br />
                <br />
              </td>
              <td className="text-center">
                <br />
                <br />
                <br />
                <br />
              </td>
            </tr>
            <tr>
              <td className="text-center">....................</td>
              <td className="text-center">....................</td>
            </tr>
          </table>
        </div>
      </div>
    </React.Fragment>
  );
};

const columns = [
  {
    title: "Nama Obat",
    dataIndex: "medicine",
    key: "codeName",
    render: medicine => (medicine ? medicine.codeName : "-")
  },
  {
    title: "Jumlah Hari",
    dataIndex: "jumlahHari",
    key: "jumlahHari"
  },
  {
    title: "Jumlah Obat",
    dataIndex: "amount",
    key: "amount"
  }
];
