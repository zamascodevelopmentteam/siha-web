import React, { useState, useEffect } from "react";
import API from "utils/API";
import axios from "axios";
import { Select, Spin } from "antd";
// import { faLeaf } from "@fortawesome/free-solid-svg-icons";

const { Option } = Select;

const ChooseDestination = ({ isDown = false, onChange, isUp = false, one = false, ...props }) => {
  let source = axios.CancelToken.source();
  const [destinations, setDestinations] = useState([]);
  const [mapData, setMapData] = useState([]);
  const [isLoading, setLoading] = useState(false);

  const _loadData = () => {
    setLoading(true);
    if (isDown) {
      API.Logistic.listAvaliableDestinationDown(source.token)
        .then(rsp => {
          const constructed = _constructData(rsp.data);
          setMapData(constructed.mapData);
          setDestinations(constructed.data || []);
        })
        .catch(e => {
          console.error("e: ", e);
        })
        .finally(() => {
          setLoading(false);
        });
    } else if (isUp) {
      API.Logistic.listAvaliableDestinationUp(source.token)
        .then(rsp => {
          const constructed = _constructData(rsp.data);
          setMapData(constructed.mapData);
          setDestinations(constructed.data || []);
        })
        .catch(e => {
          console.error("e: ", e);
        })
        .finally(() => {
          setLoading(false);
        });
    } else {
      API.Logistic.listAvaliableDestination(source.token)
        .then(rsp => {
          const constructed = _constructData(rsp.data);
          setMapData(constructed.mapData);
          setDestinations(constructed.data || []);
        })
        .catch(e => {
          console.error("e: ", e);
        })
        .finally(() => {
          setLoading(false);
        });
    }
  };

  const _handleChange = value => {
    onChange(mapData[value]);
  };

  useEffect(() => {
    _loadData();

    return () => {
      source.cancel("request canceled");
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Select
      showSearch
      optionFilterProp="children"
      notFoundContent={isLoading ? <Spin size="small" /> : null}
      onChange={_handleChange}
      {...props}
      // value={destinations.length ? destinations[0].id : null}
    >
      <Option disabled value={null}>
        Pilih Tujuan
      </Option>
      {destinations.map((dest, i) => (
        <Option key={dest.id} value={dest.id} disabled={i !== 0 && one}>
          {dest.name}
        </Option>
      ))}
    </Select>
  );
};

export default ChooseDestination;

const _constructData = data => {
  let newData = [];
  let mapData = {};
  for (let x = 0; x < data.length; x++) {
    let id, type, itemId;
    for (const [key, value] of Object.entries(data[x])) {
      if (key === "name" || key === "logisticRole") {
        continue;
      }
      const logisticRole =
        typeof data[x].logisticRole !== "undefined" && data[x].logisticRole
          ? `_` + data[x].logisticRole
          : ``;
      if (value !== null) {
        id = `${value}_${key}${logisticRole}`;
        itemId = value;
        type = key;
      }
    }
    newData.push({
      id,
      itemId,
      type,
      logisticRole:
        typeof data[x].logisticRole !== "undefined" && data[x].logisticRole
          ? data[x].logisticRole
          : null,
      name: data[x].name
    });

    mapData[id] = {
      itemId,
      type,
      logisticRole:
        typeof data[x].logisticRole !== "undefined" && data[x].logisticRole
          ? data[x].logisticRole
          : null,
      name: data[x].name
    };
  }
  return { data: newData, mapData };
};
