import * as React from "react";
import { NavbarBrand, Nav, NavItem, NavLink } from "reactstrap";
import { c, ROLE, ENUM } from "../constant";
import { observer, inject } from "mobx-react";
import { IUserStore } from "../stores/userStore";
import { NavLink as RRNavLink } from "react-router-dom";
import { useLocation } from "react-router";

import Logo from "../images/icons/kemenkes_image.png";

// import { Menu } from "antd";
// const { SubMenu } = Menu;

interface Props { }

interface InjectedProps extends Props {
  userStore: IUserStore;
}

interface ISidebarState {
  isOpen: boolean;
  role: string;
}

@inject(c.STORES.AUTH)
@inject(c.STORES.COMMON)
@inject(c.STORES.USER)
@observer
class Sidebar extends React.Component<Props, ISidebarState> {
  constructor(props) {
    super(props);
    const { user } = this.injected.userStore;

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false,
      role: user.role || ""
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  get injected() {
    return this.props as InjectedProps;
  }

  loadMenu = (role, logisticrole) => {
    switch (role) {
      case ROLE.LAB_STAFF:
        return <LabMenu />;
      case ROLE.RR_STAFF:
        return <RrMenu />;
      case ROLE.DOCTOR:
        return <DokterMenu />;
      case ROLE.PHARMA_STAFF:
        return <PharmaMenu logisticrole={logisticrole} />;
      case ROLE.SUDIN_STAFF:
      case ROLE.PROVINCE_STAFF:
        return <LogisticMenu />;
      case ROLE.MINISTRY_STAFF:
        return <MinistryMenu />;
      default:
        return <AdminMenu />;
    }
  };

  render() {
    const { user } = this.injected.userStore;
    const role = user.role || "";
    const logisticrole = user.logisticrole || "";

    return (
      <React.Fragment>
        <NavbarBrand
          href="/"
          className={`text-primary font-weight-bolder p-3 pl-4 text-${user.role ||
            ""}`}
          style={{ fontSize: "24px" }}
        >
          <img src={Logo} className={"mr-1"} height={"40"} alt="logo" />
          SiHA 2.0
        </NavbarBrand>
        <Nav vertical>{this.loadMenu(role, logisticrole)}</Nav>
      </React.Fragment>
    );
  }
}

export default Sidebar;

const LogisticMenu = () => {
  const { pathname } = useLocation();

  return (
    <React.Fragment>
      <NavItem className="pt-1 pb-1 m-0">
        <NavLink
          className="nav-link-RR_STAFF"
          tag={RRNavLink}
          to="/pharma/stock"
          activeClassName="active"
        >
          Stok
        </NavLink>
      </NavItem>
      <React.Fragment>
        <NavItem className="pt-1 pb-1 m-0">
          <NavLink
            className="nav-link-RR_STAFF"
            tag={RRNavLink}
            to="/pharma/permintaan/in"
            activeClassName="active"
            isActive={(match, location) => {
              return location.pathname.startsWith("/pharma/permintaan");
            }}
          >
            Permintaan
            </NavLink>
        </NavItem>

        {pathname.startsWith("/pharma/permintaan") && (
          <React.Fragment>
            <NavItem className="pt-1 pb-1 m-0">
              <NavLink
                className="nav-link-RR_STAFF ml-3"
                tag={RRNavLink}
                to="/pharma/permintaan/in"
                activeClassName="active"
              >
                Permintaan Masuk
                </NavLink>
            </NavItem>
            <NavItem className="pt-1 pb-1 m-0">
              <NavLink
                className="nav-link-RR_STAFF ml-3"
                tag={RRNavLink}
                to="/pharma/permintaan/out"
                activeClassName="active"
              >
                Permintaan Keluar
                </NavLink>
            </NavItem>
            <NavItem className="pt-1 pb-1 m-0">
              <NavLink
                className="nav-link-RR_STAFF ml-4"
                tag={RRNavLink}
                to="/pharma/permintaan/out"
                activeClassName="active"
              >
                Permintaan Khusus
                </NavLink>
            </NavItem>
            <NavItem className="pt-1 pb-1 m-0">
              <NavLink
                className="nav-link-RR_STAFF ml-4"
                tag={RRNavLink}
                to="/pharma/permintaan/reguler"
                activeClassName="active"
              >
                Permintaan Reguler
                </NavLink>
            </NavItem>
          </React.Fragment>
        )}
        {/* <NavItem className="pt-1 pb-1 m-0">
          <NavLink
            className="nav-link-RR_STAFF"
            tag={RRNavLink}
            to="/pharma/pengiriman"
            activeClassName="active"
          >
            Pengiriman
            </NavLink>
        </NavItem> */}
      </React.Fragment>
      <NavItem className="pt-1 pb-1 m-0">
        <NavLink
          className="nav-link-RR_STAFF"
          tag={RRNavLink}
          to="/pharma/pengiriman"
          activeClassName="active"
        >
          Pengiriman
        </NavLink>
      </NavItem>
      <NavItem className="pt-1 pb-1 m-0">
        <NavLink
          className="nav-link-RR_STAFF"
          tag={RRNavLink}
          to="/pharma/penerimaan"
          activeClassName="active"
        >
          Penerimaan
        </NavLink>
      </NavItem>

      <NavItem className="pt-1 pb-1 m-0">
        <NavLink
          className="nav-link-RR_STAFF"
          tag={RRNavLink}
          to="/report/lbpha2"
          activeClassName="active"
          isActive={(match, location) => {
            return location.pathname.startsWith("/report");
          }}
        >
          Laporan
        </NavLink>
      </NavItem>

      {pathname.startsWith("/report") && <Report role="RR_STAFF" hideLbpha1 isPusat={false} />}
    </React.Fragment>
  );
};

const MinistryMenu = () => {
  const { pathname } = useLocation();

  return (
    <React.Fragment>
      <NavItem className="pt-1 pb-1 m-0">
        <NavLink
          className="nav-link-RR_STAFF"
          tag={RRNavLink}
          to="/pharma/stock"
          activeClassName="active"
        >
          Stok
        </NavLink>
      </NavItem>
      <NavItem className="pt-1 pb-1 m-0">
        <NavLink
          className="nav-link-RR_STAFF"
          tag={RRNavLink}
          to="/pharma/permintaan/in"
          activeClassName="active"
        >
          Permintaan Masuk
        </NavLink>
      </NavItem>
      <NavItem className="pt-1 pb-1 m-0">
        <NavLink
          className="nav-link-RR_STAFF"
          tag={RRNavLink}
          to="/pharma/pengiriman"
          activeClassName="active"
        >
          Pengiriman
        </NavLink>
      </NavItem>
      <NavItem className="pt-1 pb-1 m-0">
        <NavLink
          className="nav-link-RR_STAFF"
          tag={RRNavLink}
          to="/pharma/penerimaan"
          activeClassName="active"
        >
          Penerimaan
        </NavLink>
      </NavItem>

      <NavItem className="pt-1 pb-1 m-0">
        <NavLink
          className="nav-link-RR_STAFF"
          tag={RRNavLink}
          to="/master/medicine"
          activeClassName="active"
          isActive={(match, location) => {
            return location.pathname.startsWith("/master");
          }}
        >
          Master Data
        </NavLink>
      </NavItem>

      {pathname.startsWith("/master") && <MasterData />}

      <NavItem className="pt-1 pb-1 m-0">
        <NavLink
          className="nav-link-RR_STAFF"
          tag={RRNavLink}
          to="/report/lbpha1"
          activeClassName="active"
          isActive={(match, location) => {
            return location.pathname.startsWith("/report");
          }}
        >
          Laporan
        </NavLink>
      </NavItem>

      {pathname.startsWith("/report") && (
        <Report role="RR_STAFF" hideLbpha1={false} isPusat={true} />
      )}
    </React.Fragment>
  );
};
const Report = ({ role, hideLbpha1, isPusat }) => (
  <React.Fragment>
    {/* {!hideLbpha1 && ( */}
    <NavItem className="pt-1 pb-1 m-0">
      <NavLink
        className={`nav-link-${role} ml-3`}
        tag={RRNavLink}
        to="/report/lbpha1"
        activeClassName="active"
      >
        LBPHA 1
      </NavLink>
    </NavItem>
    {/* )} */}
    <NavItem className="pt-1 pb-1 m-0">
      <NavLink
        className={`nav-link-${role} ml-3`}
        tag={RRNavLink}
        to="/report/lbpha2"
        activeClassName="active"
      >
        LBPHA 2
      </NavLink>
    </NavItem>
    <NavItem className="pt-1 pb-1 m-0">
      <NavLink
        className={`nav-link-${role} ml-3`}
        tag={RRNavLink}
        to="/report/rejimen"
        activeClassName="active"
      >
        Laporan Rejimen
      </NavLink>
    </NavItem>
    <NavItem className="pt-1 pb-1 m-0">
      <NavLink
        className={`nav-link-${role} ml-3`}
        tag={RRNavLink}
        to="/report/register-lab"
        activeClassName="active"
      >
        LBADB
      </NavLink>
    </NavItem>
    <NavItem className="pt-1 pb-1 m-0">
      <NavLink
        className={`nav-link-${role} ml-3`}
        tag={RRNavLink}
        to="/report/lbadb-detail"
        activeClassName="active"
      >
        Register Lab
      </NavLink>
    </NavItem>
    <NavItem className="pt-1 pb-1 m-0">
      <NavLink
        className={`nav-link-${role} ml-3`}
        tag={RRNavLink}
        to="/report/layanan-aktif"
        activeClassName="active"
      >
        Layanan Aktif
      </NavLink>
    </NavItem>
    <NavItem className="pt-1 pb-1 m-0">
      <NavLink
        className={`nav-link-${role} ml-3`}
        tag={RRNavLink}
        to="/report/layanan-desentralisasi"
        activeClassName="active"
      >
        Layanan Desentralisasi
      </NavLink>
    </NavItem>
    <NavItem className="pt-1 pb-1 m-0">
      <NavLink
        className={`nav-link-${role} ml-3`}
        tag={RRNavLink}
        to="/report/ims-new"
        activeClassName="active"
      >
        Laporan IMS Baru
      </NavLink>
    </NavItem>
    <NavItem className="pt-1 pb-1 m-0">
      <NavLink
        className={`nav-link-${role} ml-3`}
        tag={RRNavLink}
        to="/report/ims-new2"
        activeClassName="active"
      >
        Laporan IMS Baru 2
      </NavLink>
    </NavItem>
    <NavItem className="pt-1 pb-1 m-0">
      <NavLink
        className={`nav-link-${role} ml-3`}
        tag={RRNavLink}
        to="/report/ims"
        activeClassName="active"
      >
        Laporan IMS
      </NavLink>
    </NavItem>
    {/* <NavItem className="pt-1 pb-1 m-0">
      <NavLink
        className={`nav-link-${role} ml-3`}
        tag={RRNavLink}
        to="/report/ims-old"
        activeClassName="active"
      >
        Laporan IMS (lama)
      </NavLink>
    </NavItem> */}
    <NavItem className="pt-1 pb-1 m-0">
      <NavLink
        className={`nav-link-${role} ml-3`}
        tag={RRNavLink}
        to="/report/ims2"
        activeClassName="active"
      >
        Laporan IMS 2
      </NavLink>
    </NavItem>
    {/* <NavItem className="pt-1 pb-1 m-0">
      <NavLink
        className={`nav-link-${role} ml-3`}
        tag={RRNavLink}
        to="/report/ims2-old"
        activeClassName="active"
      >
        Laporan IMS 2 (Lama)
      </NavLink>
    </NavItem> */}
    <NavItem className="pt-1 pb-1 m-0">
      <NavLink
        className={`nav-link-${role} ml-3`}
        tag={RRNavLink}
        to="/report/pasien-ims"
        activeClassName="active"
      >
        Jumlah Pasien IMS
      </NavLink>
    </NavItem>
    <NavItem className="pt-1 pb-1 m-0">
      <NavLink
        className={`nav-link-${role} ml-3`}
        tag={RRNavLink}
        to="/report/hiv"
        activeClassName="active"
      >
        Laporan HIV
      </NavLink>
    </NavItem>
    <NavItem className="pt-1 pb-1 m-0">
      <NavLink
        className={`nav-link-${role} ml-3`}
        tag={RRNavLink}
        to="/report/penggunaan-obat"
        activeClassName="active"
      >
        {'Pengunaan ARV & non-ARV'}
      </NavLink>
    </NavItem>
    <NavItem className="pt-1 pb-1 m-0">
      <NavLink
        className={`nav-link-${role} ml-3`}
        tag={RRNavLink}
        to="/report/pergerakan-stock"
        activeClassName="active"
      >
        Pergerakan Stock
      </NavLink>
    </NavItem>
    <NavItem className="pt-1 pb-1 m-0">
      <NavLink
        className={`nav-link-${role} ml-3`}
        tag={RRNavLink}
        to="/report/rekap-penerimaan-obat"
        activeClassName="active"
      >
        Rekap Penerimaan Obat
      </NavLink>
    </NavItem>
    <NavItem className="pt-1 pb-1 m-0">
      <NavLink
        className={`nav-link-${role} ml-3`}
        tag={RRNavLink}
        to="/report/relokasi"
        activeClassName="active"
      >
        Relokasi
      </NavLink>
    </NavItem>
    <NavItem className="pt-1 pb-1 m-0">
      <NavLink
        className={`nav-link-${role} ml-3`}
        tag={RRNavLink}
        to="/report/stock-adjustment"
        activeClassName="active"
      >
        Stock Adjustment
      </NavLink>
    </NavItem>
    <NavItem className="pt-1 pb-1 m-0">
      <NavLink
        className={`nav-link-${role} ml-3`}
        tag={RRNavLink}
        to="/report/ketersediaan-stok"
        activeClassName="active"
      >
        Ketersediaan Stok
      </NavLink>
    </NavItem>
    <NavItem className="pt-1 pb-1 m-0">
      <NavLink
        className={`nav-link-${role} ml-3`}
        tag={RRNavLink}
        to="/report/perkiraan-kebutuhan-tahunan"
        activeClassName="active"
      >
        Perkiraan Kebutuhan Tahunan
      </NavLink>
    </NavItem>
    {/* {isPusat && ( */}
      <React.Fragment>
        <NavItem className="pt-1 pb-1 m-0">
          <NavLink
            className={`nav-link-${role} ml-3`}
            tag={RRNavLink}
            to="/report/rekap-tes-hiv"
            activeClassName="active"
          >
            Laporan Rekap Tes HIV
          </NavLink>
        </NavItem>
        <NavItem className="pt-1 pb-1 m-0">
          <NavLink
            className={`nav-link-${role} ml-3`}
            tag={RRNavLink}
            to="/report/rekap-tes-hiv-individu"
            activeClassName="active"
          >
            Laporan Rekap Individu Tes HIV
          </NavLink>
        </NavItem>
        <NavItem className="pt-1 pb-1 m-0">
          <NavLink
            className={`nav-link-${role} ml-3`}
            tag={RRNavLink}
            to="/report/rekap-rencana-kunjungan-odha"
            activeClassName="active"
          >
            Laporan Rekap Rencana Kunjungan Odha
          </NavLink>
        </NavItem>
        {/* <NavItem className="pt-1 pb-1 m-0">
          <NavLink
            className={`nav-link-${role} ml-3`}
            tag={RRNavLink}
            to="/report/tes-vlcd4"
            activeClassName="active"
          >
            Laporan Penggunaan Tes VL dan CD4
          </NavLink>
        </NavItem> */}
      </React.Fragment>
    {/* )} */}
  </React.Fragment>
);

const MasterData = () => (
  <React.Fragment>
    <NavItem className="pt-1 pb-1 m-0">
      <NavLink
        className="nav-link-RR_STAFF ml-3"
        tag={RRNavLink}
        to="/master/medicine"
        activeClassName="active"
      >
        Master Obat
      </NavLink>
    </NavItem>
    <NavItem className="pt-1 pb-1 m-0">
      <NavLink
        className="nav-link-RR_STAFF ml-3"
        tag={RRNavLink}
        to="/master/brand"
        activeClassName="active"
      >
        Master Brand
      </NavLink>
    </NavItem>
    <NavItem className="pt-1 pb-1 m-0">
      <NavLink
        className="nav-link-RR_STAFF ml-3"
        tag={RRNavLink}
        to="/master/regiment"
        activeClassName="active"
      >
        Master Regiment
      </NavLink>
    </NavItem>
    <NavItem className="pt-1 pb-1 m-0">
      <NavLink
        className="nav-link-RR_STAFF ml-3"
        tag={RRNavLink}
        to="/master/province"
        activeClassName="active"
      >
        Master Province
      </NavLink>
    </NavItem>
    <NavItem className="pt-1 pb-1 m-0">
      <NavLink
        className="nav-link-RR_STAFF ml-3"
        tag={RRNavLink}
        to="/master/sudinkab"
        activeClassName="active"
      >
        Master Kab/Kota
      </NavLink>
    </NavItem>
    <NavItem className="pt-1 pb-1 m-0">
      <NavLink
        className="nav-link-RR_STAFF ml-3"
        tag={RRNavLink}
        to="/master/upk"
        activeClassName="active"
      >
        Master UPK
      </NavLink>
    </NavItem>
    <NavItem className="pt-1 pb-1 m-0">
      <NavLink
        className="nav-link-RR_STAFF ml-3"
        tag={RRNavLink}
        to={{ pathname: "/master/user", state: { roleName: "MINISTRY_STAFF" } }}
        activeClassName="active"
        isActive={(match, location) => {
          if (!location.state || location.state.roleName !== "MINISTRY_STAFF")
            return false

          return true
        }}
      >
        Master Ministry Staff
      </NavLink>
    </NavItem>
    <NavItem className="pt-1 pb-1 m-0">
      <NavLink
        className="nav-link-RR_STAFF ml-3"
        tag={RRNavLink}
        to={{ pathname: "/master/user", state: { roleName: "ADMIN" } }}
        activeClassName="active"
        isActive={(match, location) => {
          if (!location.state || location.state.roleName !== "ADMIN")
            return false

          return true
        }}
      >
        Master Admin
      </NavLink>
    </NavItem>
    <NavItem className="pt-1 pb-1 m-0">
      <NavLink
        className="nav-link-RR_STAFF ml-3"
        tag={RRNavLink}
        to={{ pathname: "/master/user", state: { roleName: "PATIENT" } }}
        activeClassName="active"
        isActive={(match, location) => {
          if (!location.state || location.state.roleName !== "PATIENT")
            return false

          return true
        }}
      >
        Master Patient
      </NavLink>
    </NavItem>
    <NavItem className="pt-1 pb-1 m-0">
      <NavLink
        className="nav-link-RR_STAFF ml-3"
        tag={RRNavLink}
        to={{ pathname: "/master/user", state: { roleName: "LAB_STAFF" } }}
        activeClassName="active"
        isActive={(match, location) => {
          if (!location.state || location.state.roleName !== "LAB_STAFF")
            return false

          return true
        }}
      >
        Master Lab Staff
      </NavLink>
    </NavItem>
    <NavItem className="pt-1 pb-1 m-0">
      <NavLink
        className="nav-link-RR_STAFF ml-3"
        tag={RRNavLink}
        to={{ pathname: "/master/user", state: { roleName: "PHARMA_STAFF" } }}
        activeClassName="active"
        isActive={(match, location) => {
          if (!location.state || location.state.roleName !== "PHARMA_STAFF")
            return false

          return true
        }}
      >
        Master Pharma Staff
      </NavLink>
    </NavItem>

    <NavItem className="pt-1 pb-1 m-0">
      <NavLink
        className="nav-link-RR_STAFF ml-3"
        tag={RRNavLink}
        to={{ pathname: "/master/user", state: { roleName: "DOCTOR" } }}
        activeClassName="active"
        isActive={(match, location) => {
          if (!location.state || location.state.roleName !== "DOCTOR")
            return false

          return true
        }}
      >
        Master Doctor
      </NavLink>
    </NavItem>

    <NavItem className="pt-1 pb-1 m-0">
      <NavLink
        className="nav-link-RR_STAFF ml-3"
        tag={RRNavLink}
        to={{ pathname: "/master/user", state: { roleName: "RR_STAFF" } }}
        activeClassName="active"
        isActive={(match, location) => {
          if (!location.state || location.state.roleName !== "RR_STAFF")
            return false

          return true
        }}
      >
        Master RR Staff
      </NavLink>
    </NavItem>
    <NavItem className="pt-1 pb-1 m-0">
      <NavLink
        className="nav-link-RR_STAFF ml-3"
        tag={RRNavLink}
        to={{ pathname: "/master/user", state: { roleName: "SUDIN_STAFF" } }}
        activeClassName="active"
        isActive={(match, location) => {
          if (!location.state || location.state.roleName !== "SUDIN_STAFF")
            return false

          return true
        }}
      >
        Master Sudin Staff
      </NavLink>
    </NavItem>
    <NavItem className="pt-1 pb-1 m-0">
      <NavLink
        className="nav-link-RR_STAFF ml-3"
        tag={RRNavLink}
        to={{ pathname: "/master/user", state: { roleName: "PROVINCE_STAFF" } }}
        activeClassName="active"
        isActive={(match, location) => {
          if (!location.state || location.state.roleName !== "PROVINCE_STAFF")
            return false

          return true
        }}
      >
        Master Province Staff
      </NavLink>
    </NavItem>
    <NavItem className="pt-1 pb-1 m-0">
      <NavLink
        className="nav-link-RR_STAFF ml-3"
        tag={RRNavLink}
        to="/master/produsen"
        activeClassName="active"
      >
        Master Produsen
      </NavLink>
    </NavItem>
  </React.Fragment>
);
const AdminMenu = () => (
  <React.Fragment>
    <NavItem className="pt-1 pb-1 m-0">
      <NavLink tag={RRNavLink} to="/dashboard" activeClassName="active">
        Dashboard
      </NavLink>
    </NavItem>
    <NavItem className="pt-1 pb-1 m-0">
      <NavLink tag={RRNavLink} to="/pasien-masuk" activeClassName="active">
        Pasien Masuk
      </NavLink>
    </NavItem>
    <NavItem className="pt-1 pb-1 m-0">
      <NavLink tag={RRNavLink} to="/kondisi-odha" activeClassName="active">
        Kondisi ODHA
      </NavLink>
    </NavItem>
    <NavItem className="pt-1 pb-1 m-0">
      <NavLink tag={RRNavLink} to="/tb-hiv" activeClassName="active">
        Koinfeksi TB-HIV
      </NavLink>
    </NavItem>
    <NavItem className="pt-1 pb-1 m-0">
      <NavLink tag={RRNavLink} to="/ppk" activeClassName="active">
        PPK
      </NavLink>
    </NavItem>
    <NavItem className="pt-1 pb-1 m-0">
      <NavLink tag={RRNavLink} to="/tpt" activeClassName="active">
        TPT
      </NavLink>
    </NavItem>
    <NavItem className="pt-1 pb-1 m-0">
      <NavLink tag={RRNavLink} to="/viral-load" activeClassName="active">
        Viral Load
      </NavLink>
    </NavItem>
    <NavItem className="pt-1 pb-1 m-0">
      <NavLink tag={RRNavLink} to="/arv-khusus" activeClassName="active">
        ARV Khusus
      </NavLink>
    </NavItem>
    <NavItem className="pt-1 pb-1 m-0">
      <NavLink
        tag={RRNavLink}
        to="/notifikasi-pasangan"
        activeClassName="active"
      >
        Notifikasi Pasangan
      </NavLink>
    </NavItem>
    <NavItem className="pt-1 pb-1 m-0">
      <NavLink tag={RRNavLink} to="/hiv-ims" activeClassName="active">
        HIV-IMS
      </NavLink>
    </NavItem>
  </React.Fragment>
);

const LabMenu = () => (
  <React.Fragment>
    <NavItem className="pt-1 pb-1 m-0">
      <NavLink
        tag={RRNavLink}
        to="/intra/dashboard"
        className="nav-link-LAB_STAFF"
        activeClassName="active"
      >
        Dashboard
      </NavLink>
    </NavItem>
    <NavItem className="pt-1 pb-1 m-0">
      <NavLink
        tag={RRNavLink}
        to={`/kunjungan/${ROLE.LAB_STAFF}`}
        className="nav-link-LAB_STAFF"
        activeClassName="active"
      >
        Kunjungan
      </NavLink>
    </NavItem>
    <NavItem className="pt-1 pb-1 m-0">
      <NavLink
        tag={RRNavLink}
        to={`/data-pasien/${ROLE.LAB_STAFF}`}
        className="nav-link-LAB_STAFF"
        activeClassName="active"
      >
        Data Pasien
      </NavLink>
    </NavItem>
    <NavItem className="pt-1 pb-1 m-0">
      <NavLink
        className="nav-link-LAB_STAFF"
        tag={RRNavLink}
        to="/pharma/stock"
        activeClassName="active"
      >
        Stok
      </NavLink>
    </NavItem>
    <NavItem className="pt-1 pb-1 m-0">
      <NavLink
        className="nav-link-LAB_STAFF"
        tag={RRNavLink}
        to="/pharma/penerimaan"
        activeClassName="active"
      >
        Penerimaan
      </NavLink>
    </NavItem>
    <NavItem className="pt-1 pb-1 m-0">
      <NavLink
        className="nav-link-LAB_STAFF"
        tag={RRNavLink}
        to="/report/register-lab"
        activeClassName="active"
      >
        LBADB
      </NavLink>
    </NavItem>
    <NavItem className="pt-1 pb-1 m-0">
      <NavLink
        className={`nav-link-LAB_STAFF`}
        tag={RRNavLink}
        to="/report/lbadb-detail"
        activeClassName="active"
      >
        Register Lab
      </NavLink>
    </NavItem>
    <NavItem className="pt-1 pb-1 m-0">
      <NavLink
        className={`nav-link-LAB_STAFF`}
        tag={RRNavLink}
        to="/report/stock-adjustment"
        activeClassName="active"
      >
        Stock Adjustment
      </NavLink>
    </NavItem>
    <NavItem className="pt-1 pb-1 m-0">
      <NavLink
        className={`nav-link-LAB_STAFF`}
        tag={RRNavLink}
        to="/lab/non-pasien"
        activeClassName="active"
      >
        Penggunaan Lab Nonpasien
      </NavLink>
    </NavItem>
    <NavItem className="pt-1 pb-1 m-0">
      <NavLink
        className={`nav-link-LAB_STAFF`}
        tag={RRNavLink}
        to="/lab/penggunaan-vlcd4"
        activeClassName="active"
      >
        Penggunaan Vl/CD4
      </NavLink>
    </NavItem>
  </React.Fragment>
);

const RrMenu = () => (
  <React.Fragment>
    <NavItem className="pt-1 pb-1 m-0">
      <NavLink
        className="nav-link-RR_STAFF"
        tag={RRNavLink}
        to="/intra/dashboard"
        activeClassName="active"
      >
        Dashboard
      </NavLink>
    </NavItem>
    <NavItem className="pt-1 pb-1 m-0">
      <NavLink
        className="nav-link-RR_STAFF"
        tag={RRNavLink}
        to="/rr/kunjungan"
        activeClassName="active"
      >
        Kunjungan
      </NavLink>
    </NavItem>
    <NavItem className="pt-1 pb-1 m-0">
      <NavLink
        className="nav-link-RR_STAFF"
        tag={RRNavLink}
        to={`/data-pasien/${ROLE.RR_STAFF}`}
        activeClassName="active"
      >
        Data Pasien
      </NavLink>
    </NavItem>
    <NavItem className="pt-1 pb-1 m-0">
      <NavLink
        className="nav-link-RR_STAFF"
        tag={RRNavLink}
        to="/report/lbpha1"
        activeClassName="active"
      >
        LBPHA 1
      </NavLink>
    </NavItem>
    <NavItem className="pt-1 pb-1 m-0">
      <NavLink
        className="nav-link-RR_STAFF"
        tag={RRNavLink}
        to="/report/ims-new"
        activeClassName="active"
      >
        Laporan IMS Baru
      </NavLink>
    </NavItem>
    <NavItem className="pt-1 pb-1 m-0">
      <NavLink
        className="nav-link-RR_STAFF"
        tag={RRNavLink}
        to="/report/ims-new2"
        activeClassName="active"
      >
        Laporan IMS Baru 2
      </NavLink>
    </NavItem>
    <NavItem className="pt-1 pb-1 m-0">
      <NavLink
        className="nav-link-RR_STAFF"
        tag={RRNavLink}
        to="/report/ims"
        activeClassName="active"
      >
        Laporan IMS 1
      </NavLink>
    </NavItem>
    {/* <NavItem className="pt-1 pb-1 m-0">
      <NavLink
        className={`nav-link-RR_STAFF ml-3`}
        tag={RRNavLink}
        to="/report/ims-old"
        activeClassName="active"
      >
        Laporan IMS (lama)
      </NavLink>
    </NavItem> */}
    <NavItem className="pt-1 pb-1 m-0">
      <NavLink
        className="nav-link-RR_STAFF"
        tag={RRNavLink}
        to="/report/ims2"
        activeClassName="active"
      >
        Laporan IMS 2
      </NavLink>
    </NavItem>
    {/* <NavItem className="pt-1 pb-1 m-0">
      <NavLink
        className="nav-link-RR_STAFF"
        tag={RRNavLink}
        to="/report/ims2-old"
        activeClassName="active"
      >
        Laporan IMS 2 (Lama)
      </NavLink>
    </NavItem> */}
    <NavItem className="pt-1 pb-1 m-0">
      <NavLink
        className="nav-link-RR_STAFF"
        tag={RRNavLink}
        to="/report/hiv"
        activeClassName="active"
      >
        Laporan HIV
      </NavLink>
    </NavItem>
  </React.Fragment>
);

const DokterMenu = () => (
  <React.Fragment>
    <NavItem className="pt-1 pb-1 m-0">
      <NavLink
        className="nav-link-RR_STAFF"
        tag={RRNavLink}
        to="/intra/dashboard"
        activeClassName="active"
      >
        Dashboard
      </NavLink>
    </NavItem>
    <NavItem className="pt-1 pb-1 m-0">
      <NavLink
        className="nav-link-RR_STAFF"
        tag={RRNavLink}
        to="/rr/kunjungan"
        activeClassName="active"
      >
        Kunjungan
      </NavLink>
    </NavItem>
    <NavItem className="pt-1 pb-1 m-0">
      <NavLink
        className="nav-link-RR_STAFF"
        tag={RRNavLink}
        to={`/data-pasien/${ROLE.DOCTOR}`}
        activeClassName="active"
      >
        Data Pasien
      </NavLink>
    </NavItem>
    <NavItem className="pt-1 pb-1 m-0">
      <NavLink
        className="nav-link-RR_STAFF"
        tag={RRNavLink}
        to="/report/lbpha1"
        activeClassName="active"
      >
        LBPHA 1
      </NavLink>
    </NavItem>
    <NavItem className="pt-1 pb-1 m-0">
      <NavLink
        className="nav-link-RR_STAFF"
        tag={RRNavLink}
        to="/report/ims-new"
        activeClassName="active"
      >
        Laporan IMS Baru
      </NavLink>
    </NavItem>
    <NavItem className="pt-1 pb-1 m-0">
      <NavLink
        className="nav-link-RR_STAFF"
        tag={RRNavLink}
        to="/report/ims-new2"
        activeClassName="active"
      >
        Laporan IMS Baru 2
      </NavLink>
    </NavItem>
    <NavItem className="pt-1 pb-1 m-0">
      <NavLink
        className="nav-link-RR_STAFF"
        tag={RRNavLink}
        to="/report/ims"
        activeClassName="active"
      >
        Laporan IMS
      </NavLink>
    </NavItem>
    {/* <NavItem className="pt-1 pb-1 m-0">
      <NavLink
        className={`nav-link-RR_STAFF ml-3`}
        tag={RRNavLink}
        to="/report/ims-old"
        activeClassName="active"
      >
        Laporan IMS (lama)
      </NavLink>
    </NavItem> */}
    <NavItem className="pt-1 pb-1 m-0">
      <NavLink
        className="nav-link-RR_STAFF"
        tag={RRNavLink}
        to="/report/ims2"
        activeClassName="active"
      >
        Laporan IMS 2
      </NavLink>
    </NavItem>
    {/* <NavItem className="pt-1 pb-1 m-0">
      <NavLink
        className="nav-link-RR_STAFF"
        tag={RRNavLink}
        to="/report/ims2-old"
        activeClassName="active"
      >
        Laporan IMS 2 (Lama)
      </NavLink>
    </NavItem> */}
    {/* <NavItem className="pt-1 pb-1 m-0">
      <NavLink
        className="nav-link-RR_STAFF"
        tag={RRNavLink}
        to="/report/hiv"
        activeClassName="active"
      >
        Laporan HIV
      </NavLink>
    </NavItem> */}
  </React.Fragment>
);

const PharmaMenu = ({ logisticrole }) => {
  const { pathname } = useLocation();

  return (
    <React.Fragment>
      <NavItem className="pt-1 pb-1 m-0">
        <NavLink
          className="nav-link-PHARMA_STAFF"
          tag={RRNavLink}
          to="/intra/dashboard"
          activeClassName="active"
        >
          Dashboard
        </NavLink>
      </NavItem>

      {logisticrole !== ENUM.LOGISTIC_ROLE.UPK_ENTITY && (
        <React.Fragment>
          <NavItem className="pt-1 pb-1 m-0">
            <NavLink
              className="nav-link-PHARMA_STAFF"
              tag={RRNavLink}
              to={`/kunjungan/${ROLE.PHARMA_STAFF}`}
              activeClassName="active"
            >
              Kunjungan
            </NavLink>
          </NavItem>
          <NavItem className="pt-1 pb-1 m-0">
            <NavLink
              className="nav-link-PHARMA_STAFF"
              tag={RRNavLink}
              to={`/data-pasien/${ROLE.PHARMA_STAFF}`}
              activeClassName="active"
            >
              Data Pasien
            </NavLink>
          </NavItem>
        </React.Fragment>
      )}

      <NavItem className="pt-1 pb-1 m-0">
        <NavLink
          className="nav-link-PHARMA_STAFF"
          tag={RRNavLink}
          to="/pharma/stock"
          activeClassName="active"
          isActive={(match, location) => {
            return location.pathname.startsWith("/pharma/stock");
          }}
        >
          Stok
        </NavLink>
      </NavItem>
      {logisticrole === ENUM.LOGISTIC_ROLE.UPK_ENTITY && pathname.startsWith("/pharma/stock") && (
        <React.Fragment>
          <NavItem className="pt-1 pb-1 m-0">
            <NavLink
              className="nav-link-PHARMA_STAFF ml-3"
              tag={RRNavLink}
              to="/pharma/stock"
              activeClassName="active"
            >
              Stok Gudang Layanan
          </NavLink>
          </NavItem>
          <NavItem className="pt-1 pb-1 m-0">
            <NavLink
              className="nav-link-PHARMA_STAFF ml-3"
              tag={RRNavLink}
              to="/pharma/stockpharma"
              activeClassName="active"
            >
              Stok Farmasi
          </NavLink>
          </NavItem>
          <NavItem className="pt-1 pb-1 m-0">
            <NavLink
              className="nav-link-PHARMA_STAFF ml-3"
              tag={RRNavLink}
              to="/pharma/stocklab"
              activeClassName="active"
            >
              Stok Lab
          </NavLink>
          </NavItem>
        </React.Fragment>
      )}
      {logisticrole === ENUM.LOGISTIC_ROLE.UPK_ENTITY && (
        <React.Fragment>
          <NavItem className="pt-1 pb-1 m-0">
            <NavLink
              className="nav-link-RR_STAFF"
              tag={RRNavLink}
              to="/pharma/permintaan/in"
              activeClassName="active"
              isActive={(match, location) => {
                return location.pathname.startsWith("/pharma/permintaan");
              }}
            >
              Permintaan
            </NavLink>
          </NavItem>

          {pathname.startsWith("/pharma/permintaan") && (
            <React.Fragment>
              <NavItem className="pt-1 pb-1 m-0">
                <NavLink
                  className="nav-link-RR_STAFF ml-3"
                  tag={RRNavLink}
                  to="/pharma/permintaan/in"
                  activeClassName="active"
                >
                  Permintaan Masuk
                </NavLink>
              </NavItem>
              <NavItem className="pt-1 pb-1 m-0">
                <NavLink
                  className="nav-link-RR_STAFF ml-3"
                  tag={RRNavLink}
                  to="/pharma/permintaan/out"
                  activeClassName="active"
                >
                  Permintaan Keluar
                </NavLink>
              </NavItem>
              <NavItem className="pt-1 pb-1 m-0">
                <NavLink
                  className="nav-link-RR_STAFF ml-5"
                  tag={RRNavLink}
                  to="/pharma/permintaan/out"
                  activeClassName="active"
                >
                  Permintaan Khusus
                </NavLink>
              </NavItem>
              <NavItem className="pt-1 pb-1 m-0">
                <NavLink
                  className="nav-link-RR_STAFF ml-5"
                  tag={RRNavLink}
                  to="/pharma/permintaan/reguler"
                  activeClassName="active"
                >
                  Permintaan Reguler
                </NavLink>
              </NavItem>
            </React.Fragment>
          )}
          <NavItem className="pt-1 pb-1 m-0">
            <NavLink
              className="nav-link-RR_STAFF"
              tag={RRNavLink}
              to="/pharma/pengiriman"
              activeClassName="active"
            >
              Pengiriman
            </NavLink>
          </NavItem>
        </React.Fragment>
      )}
      <NavItem className="pt-1 pb-1 m-0">
        <NavLink
          className="nav-link-PHARMA_STAFF"
          tag={RRNavLink}
          to="/pharma/penerimaan"
          activeClassName="active"
        >
          Penerimaan
        </NavLink>
      </NavItem>

      <NavItem className="pt-1 pb-1 m-0">
        <NavLink
          className="nav-link-PHARMA_STAFF"
          tag={RRNavLink}
          to="/report/lbpha1"
          activeClassName="active"
          isActive={(match, location) => {
            return location.pathname.startsWith("/report");
          }}
        >
          Laporan
        </NavLink>
      </NavItem>

      {pathname.startsWith("/report") && (
        <Report role="PHARMA_STAFF" hideLbpha1={false} isPusat={false} />
      )}
    </React.Fragment>
  );
};
