import React from "react";
import { WelcomeWrapper } from "./style";
import { Typography } from "components";

const WelcomeMessage = ({ welcomeMessage, name, description }) => {
  return (
    <WelcomeWrapper className="px-5 py-4 d-flex justify-content-between align-items-center">
      <div className="text-white">
        <Typography
          fontSize="24px"
          fontWeight="bold"
          color="white"
          className="d-block mb-4"
        >
          {welcomeMessage}, <br /> {name}!
        </Typography>
        <span>{description}</span>
      </div>
      {/* {avatar !== "" && <Avatar src={avatar} size="95px" />} */}
    </WelcomeWrapper>
  );
};

WelcomeMessage.defaultProps = {
  // avatar: "",
  welcomeMessage: "Selamat datang kembali",
  description: "Inilah beberapa informasi pada hari ini."
};

export default WelcomeMessage;
