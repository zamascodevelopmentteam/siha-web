import styled from "styled-components";

export const WelcomeWrapper = styled.div`
  border-radius: 12px;
  background-image: linear-gradient(to right, #005ecd, #1cc1c9),
    linear-gradient(to right, #1cc1c9, #1cc1c9);
`;
