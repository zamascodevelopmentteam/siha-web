import React from "react";
import {
  ArvKhusus,
  Dashboard,
  HivIms,
  KondisiOdha,
  NotifikasiPasangan,
  PasienMasuk,
  Ppk,
  TbHiv,
  Tpt,
  ViralLoad,
  LbphLembar1
} from "containers/admin";
import PasienGeneral from "containers/pasien";
import KunjunganGeneral from "containers/kunjungan";
import { LabDashboard, LabKunjungan, Pasien, NonPasien, PenggunaanVlcd4 } from "containers/lab";
import { PharmaDashboard, PharmaStock, PharmaStockPharma, PharmaStockLab } from "containers/pharma";
import { RrDashboard, RrKunjungan, RrPasien } from "containers/rr";
import { DokterPasien } from "containers/dokter";
import { ROLE } from "constant";
import { Pengirman, Permintaan, Penerimaan } from "containers/logistik";
import {
  HIV,
  TES_VLCD4,
  HIV_REKAP,
  HIV_REKAP_INDIVIDUAL,
  RencanaKunjungan,
  IMSNEW,
  IMSNEW2,
  IMS,
  IMSOLD,
  IMS2,
  IMS2OLD,
  LBPHA1,
  LBPHA2,
  Rejimen,
  RegisterLab,
  RegisterLabLbadbDetail,
  KetersediaanStok,
  LayananAktif,
  LayananDesentralisasi,
  PergerakanStock,
  PasienIMS,
  PenggunaanObat,
  PerkiraanKebutuhanTahunan,
  RekapPenerimaanObat,
  Relokasi,
  StockAdjustment,
} from "containers/report";

import DashboardIntra from "containers/dashboardIntra";

import {
  Brand,
  Medicine,
  Province,
  Regiment,
  SudinKab,
  Upk,
  User,
  Product
} from "containers/master";

const general = [
  {
    path: "/intra/dashboard",
    component: DashboardIntra,
    isPublic: false
  },
  {
    path: "/data-pasien/:role/:id?",
    component: PasienGeneral,
    isPublic: false
  },
  {
    path: "/kunjungan/:role/:id?",
    component: KunjunganGeneral,
    isPublic: false
  },
  {
    path: "/report/register-lab",
    component: RegisterLab,
    isPublic: false
  },
  {
    path: "/report/lbadb-detail",
    component: RegisterLabLbadbDetail,
    isPublic: false
  },
  {
    path: "/report/lbpha2",
    component: LBPHA2,
    isPublic: false
  },
  {
    path: "/report/rejimen",
    component: Rejimen,
    isPublic: false
  },
  {
    path: "/report/lbpha1",
    component: LBPHA1,
    isPublic: false
  },
  {
    path: "/report/ketersediaan-stok",
    component: KetersediaanStok,
    isPublic: false
  },
  {
    path: "/report/layanan-aktif",
    component: LayananAktif,
    isPublic: false
  },
  {
    path: "/report/layanan-desentralisasi",
    component: LayananDesentralisasi,
    isPublic: false
  },
  {
    path: "/report/pergerakan-stock",
    component: PergerakanStock,
    isPublic: false
  },
  {
    path: "/report/pasien-ims",
    component: PasienIMS,
    isPublic: false
  },
  {
    path: "/report/penggunaan-obat",
    component: PenggunaanObat,
    isPublic: false
  },
  {
    path: "/report/perkiraan-kebutuhan-tahunan",
    component: PerkiraanKebutuhanTahunan,
    isPublic: false
  },
  {
    path: "/report/rekap-penerimaan-obat",
    component: RekapPenerimaanObat,
    isPublic: false
  },
  {
    path: "/report/relokasi",
    component: Relokasi,
    isPublic: false
  },
  {
    path: "/report/stock-adjustment",
    component: StockAdjustment,
    isPublic: false
  },
  {
    path: "/report/ims-new",
    component: IMSNEW,
    isPublic: false
  },
  {
    path: "/report/ims-new2",
    component: IMSNEW2,
    isPublic: false
  },
  {
    path: "/report/ims",
    component: IMS,
    isPublic: false
  },
  {
    path: "/report/ims-old",
    component: IMSOLD,
    isPublic: false
  },
  {
    path: "/report/ims2",
    component: IMS2,
    isPublic: false
  },
  {
    path: "/report/ims2-old",
    component: IMS2OLD,
    isPublic: false
  },
  {
    path: "/report/tes-vlcd4",
    component: TES_VLCD4,
    isPublic: false
  },
  {
    path: "/report/hiv",
    component: HIV,
    isPublic: false
  },
  {
    path: "/report/rekap-tes-hiv",
    component: HIV_REKAP,
    isPublic: false
  },
  {
    path: "/report/rekap-tes-hiv-individu",
    component: HIV_REKAP_INDIVIDUAL,
    isPublic: false
  },
  {
    path: "/report/rekap-rencana-kunjungan-odha",
    component: RencanaKunjungan,
    isPublic: false
  },
  {
    path: "/stock/:id?/:subpage?",
    component: PharmaStock,
    isPublic: false
  },
  {
    path: "/permintaan/:type/:id?",
    component: Permintaan,
    isPublic: false
  },
  {
    path: "/pengiriman/:id?",
    component: Pengirman,
    isPublic: false
  },
  {
    path: "/penerimaan/:id?",
    component: Penerimaan,
    isPublic: false
  }
];

const master = [
  {
    path: "/master/brand",
    component: Brand,
    isPublic: false,
    role: ROLE.MINISTRY_STAFF
  },
  {
    path: "/master/medicine",
    component: Medicine,
    isPublic: false,
    role: ROLE.MINISTRY_STAFF
  },
  {
    path: "/master/province",
    component: Province,
    isPublic: false,
    role: ROLE.MINISTRY_STAFF
  },
  {
    path: "/master/sudinkab",
    component: SudinKab,
    isPublic: false,
    role: ROLE.MINISTRY_STAFF
  },
  {
    path: "/master/upk",
    component: Upk,
    isPublic: false,
    role: ROLE.MINISTRY_STAFF
  },
  {
    path: "/master/regiment",
    component: Regiment,
    isPublic: false,
    role: ROLE.MINISTRY_STAFF
  },
  {
    path: "/master/user",
    component: User,
    isPublic: false,
    role: ROLE.MINISTRY_STAFF
  },
  {
    path: "/master/produsen",
    component: Product,
    isPublic: false,
    role: ROLE.MINISTRY_STAFF
  },
];

const lab = [
  {
    path: "/lab/dashboard",
    component: LabDashboard,
    isPublic: false,
    role: ROLE.LAB_STAFF
  },
  {
    path: "/lab/kunjungan",
    component: LabKunjungan,
    isPublic: false,
    role: ROLE.LAB_STAFF
  },
  {
    path: "/lab/data-pasien/:id?",
    component: Pasien,
    isPublic: false,
    role: ROLE.LAB_STAFF
  },
  {
    path: "/lab/non-pasien",
    component: NonPasien,
    isPublic: false,
    role: ROLE.LAB_STAFF
  },
  {
    path: "/lab/penggunaan-vlcd4",
    component: PenggunaanVlcd4,
    isPublic: false,
    role: ROLE.LAB_STAFF
  }
];

const rr = [
  {
    path: "/rr/dashboard",
    component: RrDashboard,
    isPublic: false,
    role: ROLE.RR_STAFF
  },
  {
    path: "/rr/kunjungan",
    component: RrKunjungan,
    isPublic: false,
    role: ROLE.RR_STAFF
  },
  {
    path: "/rr/data-pasien/:id?",
    component: RrPasien,
    isPublic: false,
    role: ROLE.RR_STAFF
  }
];

const doctor = [
  {
    path: "/rr/dashboard",
    component: RrDashboard,
    isPublic: false,
    role: ROLE.RR_STAFF
  },
  {
    path: "/rr/kunjungan",
    component: RrKunjungan,
    isPublic: false,
    role: ROLE.RR_STAFF
  },
  {
    path: "/dokter/data-pasien/:id?",
    component: DokterPasien,
    isPublic: false,
    role: ROLE.DOCTOR
  }
];

const pharma = [
  {
    path: "/pharma/dashboard",
    component: PharmaDashboard,
    isPublic: false,
    role: ROLE.PHARMA_STAFF
  },
  {
    path: "/pharma/stock/:id?/:subpage?",
    component: PharmaStock,
    isPublic: false,
    role: ROLE.PHARMA_STAFF
  },
  // start new
  {
    path: "/pharma/stocklab/:id?/:subpage?",
    component: PharmaStockLab,
    isPublic: false,
    role: ROLE.PHARMA_STAFF
  },
  {
    path: "/pharma/stockpharma/:id?/:subpage?",
    component: PharmaStockPharma,
    isPublic: false,
    role: ROLE.PHARMA_STAFF
  },
  // end new
  {
    path: "/pharma/permintaan/:type/:id?",
    component: Permintaan,
    isPublic: false,
    role: ROLE.PHARMA_STAFF
  },
  {
    path: "/pharma/pengiriman/:id?",
    component: Pengirman,
    isPublic: false,
    role: ROLE.PHARMA_STAFF
  },
  {
    path: "/pharma/penerimaan/:id?",
    component: Penerimaan,
    isPublic: false,
    role: ROLE.PHARMA_STAFF
  },
  {
    path: "/pharma/data-pasien/:id?",
    component: () => <RrPasien role={ROLE.PHARMA_STAFF} />,
    isPublic: false,
    role: ROLE.PHARMA_STAFF
  },
  {
    path: "/pharma/lbph-1",
    component: LbphLembar1,
    isPublic: false,
    role: ROLE.PHARMA_STAFF
  }
];

const admin = [
  {
    path: "/dashboard",
    component: Dashboard,
    isPublic: false,
    role: ROLE.RR_STAFF
  },
  {
    path: "/pasien-masuk",
    component: PasienMasuk,
    isPublic: false,
    role: ROLE.RR_STAFF
  },
  {
    path: "/kondisi-odha",
    component: KondisiOdha,
    isPublic: false,
    role: ROLE.RR_STAFF
  },
  {
    path: "/tb-hiv",
    component: TbHiv,
    isPublic: false,
    role: ROLE.RR_STAFF
  },
  {
    path: "/ppk",
    component: Ppk,
    isPublic: false,
    role: ROLE.RR_STAFF
  },
  {
    path: "/tpt",
    component: Tpt,
    isPublic: false,
    role: ROLE.RR_STAFF
  },
  {
    path: "/viral-load",
    component: ViralLoad,
    isPublic: false,
    role: ROLE.RR_STAFF
  },
  {
    path: "/arv-khusus",
    component: ArvKhusus,
    isPublic: false,
    role: ROLE.RR_STAFF
  },
  {
    path: "/notifikasi-pasangan",
    component: NotifikasiPasangan,
    isPublic: false,
    role: ROLE.RR_STAFF
  },
  {
    path: "/hiv-ims",
    component: HivIms,
    isPublic: false,
    role: ROLE.RR_STAFF
  }
];

export default {
  admin,
  general,
  lab,
  master,
  pharma,
  rr,
  doctor
};
