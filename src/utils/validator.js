import { openNotification } from "./Notification"

export const validateForm = (data = {}, required = {key: "error message"}, showPopup = true) => {
  let validated = true
  let message = ''
  Object.keys(required).forEach(key => {
    if (data[key] === null || data[key] === undefined || data[key] === "" || data[key] === 0 || (Array.isArray(data[key]) ? data[key].length <= 0 : false)) {
      validated = false
      message = message ? `${message}, ${required[key]}` : required[key]
    }
  })
  if (!validated && showPopup) {
    openNotification("error", "Data harus diisi", message)
  }
  return validated
}