import request from "./request";

const useAuth = true;
const showHeader = true;

const Auth = {
  login: ({ nik, password }) =>
    request({
      url: "/public/auth/login",
      method: "POST",
      data: {
        nik,
        password
      }
    })
};

const ReportExcel = {
  downloadExcel: cancelToken =>
    request(
      {
        url: `/private/utils/download_xlsx_example`,
        method: "GET",
        // responseType: "blob",
        headers: {
          "Content-Type":
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
        },
        cancelToken
      },
      useAuth,
      showHeader
    )
};

const Logistic = {
  getOrderByID: (cancelToken, id) =>
    request(
      {
        url: `/private/orders/${id}`,
        method: "GET",
        cancelToken
      },
      useAuth
    ),
  getDistributionPlanByID: (cancelToken, id) =>
    request(
      {
        url: `/private/penerimaan/${id}`,
        method: "GET",
        cancelToken
      },
      useAuth
    ),
  listOrder: (cancelToken, type, status = "ALL", requestType = "ALL") =>
    request(
      {
        url: `/private/orders-logistic/${type}?status=${status}&isRegular=${requestType}`,
        method: "GET",
        cancelToken
      },
      useAuth
    ),
  listDistributionPlan: (
    cancelToken,
    status = "ALL",
    distributionType = "ALL",
    relokasi = "ALL"
  ) =>
    request(
      {
        url: `/private/pengiriman?status=${status}&distributionType=${distributionType}&relokasi=${relokasi}`,
        method: "GET",
        cancelToken
      },
      useAuth
    ),
  listPenerimaan: (cancelToken, status = "ALL") =>
    request(
      {
        url: `/private/penerimaan?status=${status}`,
        method: "GET",
        cancelToken
      },
      useAuth
    ),
  listAvaliableDestination: cancelToken =>
    request(
      {
        url: `/private/orders-logistic/availableFor`,
        method: "GET",
        cancelToken
      },
      useAuth
    ),
  listAvaliableDestinationDown: cancelToken =>
    request(
      {
        url: `/private/orders-logistic/availableForDown`,
        method: "GET",
        cancelToken
      },
      useAuth
    ),
  // new
  listAvaliableDestinationUp: cancelToken =>
    request(
      {
        url: `/private/orders-logistic/availableForUp`,
        method: "GET",
        cancelToken
      },
      useAuth
    ),
  // end new
  listStock: (cancelToken, type, destID, destLogisticRole) =>
    request(
      {
        url: `/private/medicines/stock/all?${type}=${destID}&logisticRole=${destLogisticRole}`,
        method: "GET",
        cancelToken
      },
      useAuth
    ),
  listAllStock: (cancelToken, page = 0, limit = 1000, keyword) =>
    request(
      {
        url: `/private/inventories/all?page${page}=&limit=${limit}&keyword=${keyword}`,
        method: "GET",
        cancelToken
      },
      useAuth
    ),
  requestOrderRequler: cancelToken =>
    request(
      {
        url: `/private/orders/create-reguler`,
        method: "POST",
        cancelToken
      },
      useAuth
    ),
  requestOrder: (cancelToken, data) =>
    request(
      {
        url: `/private/orders-logistic/create`,
        method: "POST",
        cancelToken,
        data
      },
      useAuth
    ),
  createPengiriman: (cancelToken, data) =>
    request(
      {
        url: `/private/pengiriman`,
        method: "POST",
        cancelToken,
        data
      },
      useAuth
    ),
  updateOrder: (cancelToken, id, data) =>
    request(
      {
        url: `/private/orders-logistic/${id}`,
        method: "PUT",
        cancelToken,
        data
      },
      useAuth
    ),
  createDistributionPlan: (cancelToken, data) =>
    request(
      {
        url: `/private/penerimaan?orders=FALSE`,
        method: "POST",
        cancelToken,
        data
      },
      useAuth
    ),
  createDistributionPlanWithOrder: (cancelToken, data) =>
    request(
      {
        url: `/private/penerimaan`,
        method: "POST",
        cancelToken,
        data
      },
      useAuth
    ),
  getPickingList: (cancelToken, medicineID, expiredDate) =>
    request(
      {
        url: `private/inventory/picking-list/${medicineID}/${expiredDate}`,
        method: "GET",
        cancelToken
      },
      useAuth
    ),
  confirmOrder: (cancelToken, orderID, status, data) =>
    request(
      {
        url: `/private/order-confirm/${orderID}?status=${status}`,
        method: "POST",
        cancelToken,
        data
      },
      useAuth
    ),
  confirmPenerimaan: (cancelToken, idDistributionPlan, status, data) =>
    request(
      {
        url: `/private/penerimaan/accept/${idDistributionPlan}?status=${status}`,
        method: "POST",
        cancelToken,
        data
      },
      useAuth
    ),
  confirmPengiriman: (cancelToken, idDistributionPlan, status, data) =>
    request(
      {
        url: `/private/penerimaan/${idDistributionPlan}?status=${status}`,
        method: "POST",
        cancelToken,
        data
      },
      useAuth
    ),
  adjustment: (cancelToken, data) =>
    request(
      {
        url: `/private/inventory/adjustment`,
        method: "POST",
        cancelToken,
        data
      },
      useAuth
    ),
  kalibration: (cancelToken, data) =>
    request(
      {
        url: `/private/inventory/kalibration`,
        method: "POST",
        cancelToken,
        data
      },
      useAuth
    ),
  listAdjustmentHistory: (cancelToken, medicineID, filter) => {
    let param = ``

    if (filter) {
      if (filter.UPK) {
        param = `?upk=${filter.UPK}`
      } else if (filter.SUDINKAB) {
        param = `?sudin=${filter.SUDINKAB}`
      } else if (filter.PROVINCE) {
        param = `?province=${filter.PROVINCE}`
      }

      if (filter.MONTH) {
        param += `${param.length ? '&' : '?'}month=${filter.MONTH}`
      }
    }

    return request(
      {
        url: `/private/inventory/adjustment/${medicineID}${param}`,
        method: "GET",
        cancelToken
      },
      useAuth
    )
  },
  getRegularDemand: () =>
    request(
      {
        url: `/private/orders/regular-demand`,
        method: "POST",
        data: {}
      },
      true
    ),
  postRegularDemand: (medichines, destination) =>
    request(
      {
        url: `/private/orders/regular-demand?create=t`,
        method: "POST",
        data: {
          medichines,
          destination
        }
      },
      true
    ),
};

const MasterData = {
  listBrandByMedicineID: (cancelToken, medicineID) =>
    request(
      {
        url: `/private/master-data/medicines/${medicineID}/brands?page=0&limit=1000`,
        method: "GET",
        cancelToken
      },
      useAuth
    ),
  listMedicine: (cancelToken, medicineType = "ALL") =>
    request(
      {
        url: `private/master-data/medicines?page=0&limit=1000&medicineType=` + medicineType,
        method: "GET",
        cancelToken
      },
      useAuth
    ),
  Medicine: {
    list: (cancelToken, page = 0, limit = 1000) =>
      request(
        {
          url: `/private/master-data/medicines?page${page}=&limit=${limit}`,
          method: "GET",
          cancelToken
        },
        useAuth
      ),
    getByID: (cancelToken, medicineID) =>
      request(
        {
          url: `/private/master-data/medicines/${medicineID}`,
          method: "GET",
          cancelToken
        },
        useAuth
      ),
    create: (cancelToken, data) =>
      request(
        {
          url: `/private/master-data/medicines`,
          method: "POST",
          cancelToken,
          data
        },
        useAuth
      ),
    update: (cancelToken, medicineID, data) =>
      request(
        {
          url: `/private/master-data/medicines/${medicineID}`,
          method: "PUT",
          cancelToken,
          data
        },
        useAuth
      ),
    delete: (cancelToken, medicineID) =>
      request(
        {
          url: `/private/master-data/medicines/${medicineID}`,
          method: "DELETE",
          cancelToken
        },
        useAuth
      )
  },
  Brand: {
    list: (cancelToken, page = 0, limit = 1000) =>
      request(
        {
          url: `/private/master-data/brands?page${page}=&limit=${limit}`,
          method: "GET",
          cancelToken
        },
        useAuth
      ),
    getByID: (cancelToken, ID) =>
      request(
        {
          url: `/private/master-data/brands/${ID}`,
          method: "GET",
          cancelToken
        },
        useAuth
      ),
    create: (cancelToken, data) =>
      request(
        {
          url: `/private/master-data/brands`,
          method: "POST",
          cancelToken,
          data
        },
        useAuth
      ),
    update: (cancelToken, ID, data) =>
      request(
        {
          url: `/private/master-data/brands/${ID}`,
          method: "PUT",
          cancelToken,
          data
        },
        useAuth
      ),
    delete: (cancelToken, ID) =>
      request(
        {
          url: `/private/master-data/brands/${ID}`,
          method: "DELETE",
          cancelToken
        },
        useAuth
      )
  },
  Regiment: {
    list: (cancelToken, page = 0, limit = 1000) =>
      request(
        {
          url: `/private/master-data/regiments?page${page}=&limit=${limit}`,
          method: "GET",
          cancelToken
        },
        useAuth
      ),
    getByID: (cancelToken, ID) =>
      request(
        {
          url: `/private/master-data/regiments/${ID}`,
          method: "GET",
          cancelToken
        },
        useAuth
      ),
    create: (cancelToken, data) =>
      request(
        {
          url: `/private/master-data/regiments`,
          method: "POST",
          cancelToken,
          data
        },
        useAuth
      ),
    update: (cancelToken, ID, data) =>
      request(
        {
          url: `/private/master-data/regiments/${ID}`,
          method: "PUT",
          cancelToken,
          data
        },
        useAuth
      ),
    delete: (cancelToken, ID) =>
      request(
        {
          url: `/private/master-data/regiments/${ID}`,
          method: "DELETE",
          cancelToken
        },
        useAuth
      )
  },
  Province: {
    list: (cancelToken, page = 0, limit = 1000) =>
      request(
        {
          url: `/private/master-data/provinces?page${page}=&limit=${limit}`,
          method: "GET",
          cancelToken
        },
        useAuth
      ),
    getByID: (cancelToken, ID) =>
      request(
        {
          url: `/private/master-data/provinces/${ID}`,
          method: "GET",
          cancelToken
        },
        useAuth
      ),
    create: (cancelToken, data) =>
      request(
        {
          url: `/private/master-data/provinces`,
          method: "POST",
          cancelToken,
          data
        },
        useAuth
      ),
    update: (cancelToken, ID, data) =>
      request(
        {
          url: `/private/master-data/provinces/${ID}`,
          method: "PUT",
          cancelToken,
          data
        },
        useAuth
      ),
    delete: (cancelToken, ID) =>
      request(
        {
          url: `/private/master-data/provinces/${ID}`,
          method: "DELETE",
          cancelToken
        },
        useAuth
      )
  },
  SudinKab: {
    list: (cancelToken, page = 0, limit = 1000, keyword = "") =>
      request(
        {
          url: `/private/master-data/sudin?page${page}=&limit=${limit}&keyword=${keyword}`,
          method: "GET",
          cancelToken
        },
        useAuth
      ),
    listByProvinceID: (
      cancelToken,
      provinceID,
      keyword = "",
      page = 0,
      limit = 1000
    ) =>
      request(
        {
          url: `/private/master-data/provinces/${provinceID}/sudin?page${page}=&limit=${limit}&keyword=${keyword}`,
          method: "GET",
          cancelToken
        },
        useAuth
      ),
    getByID: (cancelToken, ID) =>
      request(
        {
          url: `/private/master-data/sudin/${ID}`,
          method: "GET",
          cancelToken
        },
        useAuth
      ),
    create: (cancelToken, data) =>
      request(
        {
          url: `/private/master-data/sudin`,
          method: "POST",
          cancelToken,
          data
        },
        useAuth
      ),
    update: (cancelToken, ID, data) =>
      request(
        {
          url: `/private/master-data/sudin/${ID}`,
          method: "PUT",
          cancelToken,
          data
        },
        useAuth
      ),
    delete: (cancelToken, ID) =>
      request(
        {
          url: `/private/master-data/sudin/${ID}`,
          method: "DELETE",
          cancelToken
        },
        useAuth
      )
  },
  Upk: {
    list: (cancelToken, page = 0, limit = 1000, keyword = "") =>
      request(
        {
          url: `/private/master-data/upk?page${page}=&limit=${limit}&keyword=${keyword}`,
          method: "GET",
          cancelToken
        },
        useAuth
      ),
    listBySudinKabID: (
      cancelToken,
      sudinKabID,
      keyword = "",
      page = 0,
      limit = 1000
    ) =>
      request(
        {
          url: `/private/master-data/sudin/${sudinKabID}/upk?page${page}=&limit=${limit}&keyword=${keyword}`,
          method: "GET",
          cancelToken
        },
        useAuth
      ),
    getByID: (cancelToken, ID) =>
      request(
        {
          url: `/private/master-data/upk/${ID}`,
          method: "GET",
          cancelToken
        },
        useAuth
      ),
    create: (cancelToken, data) =>
      request(
        {
          url: `/private/master-data/upk`,
          method: "POST",
          cancelToken,
          data
        },
        useAuth
      ),
    update: (cancelToken, ID, data) =>
      request(
        {
          url: `/private/master-data/upk/${ID}`,
          method: "PUT",
          cancelToken,
          data
        },
        useAuth
      ),
    delete: (cancelToken, ID) =>
      request(
        {
          url: `/private/master-data/upk/${ID}`,
          method: "DELETE",
          cancelToken
        },
        useAuth
      )
  },
  User: {
    list: (cancelToken, keyword = "", page = 0, limit = 1000) =>
      request(
        {
          url: `/private/master-data/users?page${page}=&limit=${limit}&keyword=${keyword}`,
          method: "GET",
          cancelToken
        },
        useAuth
      ),
    listByRole: (cancelToken, role = "ADMIN", keyword = "", page = 0, limit = 1000) =>
      request(
        {
          url: `/private/master-data/users-by-role?page${page}=&limit=${limit}&role=${role}&keyword=${keyword}`,
          method: "GET",
          cancelToken
        },
        useAuth
      ),
    getByID: (cancelToken, ID) =>
      request(
        {
          url: `/private/master-data/users/${ID}`,
          method: "GET",
          cancelToken
        },
        useAuth
      ),
    create: (cancelToken, data) =>
      request(
        {
          url: `/private/master-data/users`,
          method: "POST",
          cancelToken,
          data
        },
        useAuth
      ),
    update: (cancelToken, ID, data) =>
      request(
        {
          url: `/private/master-data/users/${ID}`,
          method: "PUT",
          cancelToken,
          data
        },
        useAuth
      ),
    delete: (cancelToken, ID) =>
      request(
        {
          url: `/private/master-data/users/${ID}`,
          method: "DELETE",
          cancelToken
        },
        useAuth
      ),
    fotgotPassword: (data) =>
      request(
        {
          url: `/public/auth/forgot-password`,
          method: "POST",
          data
        },
        false
      )
  },
  Product: {
    list: (cancelToken, page = 0, limit = 1000) =>
      request(
        {
          url: `/private/master-data/producers?page${page}=&limit=${limit}`,
          method: "GET",
          cancelToken
        },
        useAuth
      ),
    getByID: (cancelToken, ID) =>
      request(
        {
          url: `/private/master-data/producers/${ID}`,
          method: "GET",
          cancelToken
        },
        useAuth
      ),
    create: (cancelToken, data) =>
      request(
        {
          url: `/private/master-data/producers`,
          method: "POST",
          cancelToken,
          data
        },
        useAuth
      ),
    update: (cancelToken, ID, data) =>
      request(
        {
          url: `/private/master-data/producers/${ID}`,
          method: "PUT",
          cancelToken,
          data
        },
        useAuth
      ),
    delete: (cancelToken, ID) =>
      request(
        {
          url: `/private/master-data/producers/${ID}`,
          method: "DELETE",
          cancelToken
        },
        useAuth
      )
  },
};

const Patient = {
  getByNik: (cancelToken, nik) =>
    request(
      {
        url: `/private/patient/${nik}/nik`,
        method: "GET",
        cancelToken
      },
      useAuth
    ),
  list: (cancelToken, keyword = "", page = 0, limit = 10, startDate, endDate) => {

    return request(
      {
        url: `/private/upk/patients?limit=${limit}&page=${page}&keyword=${keyword}${startDate ? `&startDate=${startDate}` : ''}${endDate ? `&endDate=${endDate}` : ''}`,
        method: "GET",
        cancelToken
      },
      useAuth
    )
  },
  getByID: (cancelToken, id) =>
    request(
      {
        url: `/private/patient/${id}`,
        method: "GET",
        cancelToken
      },
      useAuth
    ),
  create: (cancelToken, data) =>
    request(
      {
        url: `/private/patient/create`,
        method: "POST",
        cancelToken,
        data
      },
      useAuth
    ),
  update: (cancelToken, id, data) =>
    request(
      {
        url: `/private/patient/${id}`,
        method: "PUT",
        cancelToken,
        data
      },
      useAuth
    ),
  convertToODHA: (cancelToken, id, data) =>
    request(
      {
        url: `private/patient/${id}/odha`,
        method: "POST",
        cancelToken,
        data
      },
      useAuth
    ),
  requestOTP: (cancelToken, id) =>
    request(
      {
        url: `private/patient/${id}/otp`,
        method: "GET",
        cancelToken
      },
      useAuth
    ),
  confirmOTP: (cancelToken, id, data) =>
    request(
      {
        url: `private/patient/${id}/otp`,
        method: "POST",
        cancelToken,
        data
      },
      useAuth
    )
};

const Visit = {
  excelTest: (cancelToken) =>
    request(
      {
        url: `/private/excel-test`,
        method: "GET",
        cancelToken
      },
      useAuth
    ),
  list: (cancelToken, keyword = "", page = 0, limit = 10, where = "") =>
    request(
      {
        url: `/private/upk/visits?limit=${limit}&page=${page}&keyword=${keyword}&listType=ALL&whereParam=${where}&orderDate=DESC`,
        method: "GET",
        cancelToken
      },
      useAuth
    ),
  listToday: (cancelToken, keyword = "", page = 0, limit = 1000) =>
    request(
      {
        url: `/private/upk/visits?limit=${limit}&page=${page}&keyword=${keyword}&listType=TODAY`,
        method: "GET",
        cancelToken
      },
      useAuth
    ),
  createNewVisit: (cancelToken, patientID) =>
    request(
      {
        url: `/private/visits/${patientID}`,
        method: "POST",
        cancelToken
      },
      useAuth
    ),
  deleteVisit: (cancelToken, id) =>
    request(
      {
        url: `/private/visits/${id}`,
        method: "DELETE",
        cancelToken
      },
      useAuth
    ),
  listByPatientID: (cancelToken, id) =>
    request(
      {
        url: `private/patient/${id}/visits`,
        method: "GET",
        cancelToken
      },
      useAuth
    ),
  getAll: () =>
    request(
      {
        url: "/private/upk/visits?limit=20&page=0&listType=TODAY",
        method: "GET"
      },
      useAuth
    ),
  getCount: () =>
    request(
      {
        url: "/private/admin/visit/count",
        method: "GET"
      },
      useAuth
    ),
  createPDP: (cancelToken, visitID, data) =>
    request(
      {
        url: `/private/visit/${visitID}/treatment`,
        method: "POST",
        cancelToken,
        data
      },
      useAuth
    ),
  updatePDP: (cancelToken, visitID, data) =>
    request(
      {
        url: `/private/visit/${visitID}/treatment`,
        method: "PUT",
        cancelToken,
        data
      },
      useAuth
    ),
  getPDP: (cancelToken, visitID) =>
    request(
      {
        url: `/private/visit/${visitID}/treatment`,
        method: "GET",
        cancelToken
      },
      useAuth
    ),
  updateSisaObat: (cancelToken, visitID, data) =>
    request(
      {
        url: `/private/visit/${visitID}/sisa-obat`,
        method: "PUT",
        cancelToken,
        data
      },
      useAuth
    ),
  getPreviousTreatment: (cancelToken, visitID) =>
    request(
      {
        url: `/private/visit/${visitID}/previous-treatment`,
        method: "GET",
        cancelToken
      },
      useAuth
    )
};

const Exam = {
  listLabUsageNonpatient: (cancelToken, typeUsage = "ALL") =>
    request(
      {
        url: `/private/examination/lab-usage-nonpatient?typeUsage=${typeUsage}`,
        method: "GET",
        cancelToken
      },
      useAuth
    ),
  createLabUsageNonpatient: (cancelToken, data) =>
    request(
      {
        url: `/private/examination/lab-usage-nonpatient`,
        method: "POST",
        cancelToken,
        data
      },
      useAuth
    ),
  listIMSByPatientID: (cancelToken, id) =>
    request(
      {
        url: `/private/examination/ims/patient/${id}`,
        method: "GET",
        cancelToken
      },
      useAuth
    ),
  getIMSByVisitID: (cancelToken, id) =>
    request(
      {
        url: `/private/examination/ims/${id}`,
        method: "GET",
        cancelToken
      },
      useAuth
    ),

  listHIVByPatientID: (cancelToken, id) =>
    request(
      {
        url: `/private/examination/hiv/patient/${id}`,
        method: "GET",
        cancelToken
      },
      useAuth
    ),
  getHIVByVisitID: (cancelToken, id) =>
    request(
      {
        url: `/private/examination/hiv/${id}`,
        method: "GET",
        cancelToken
      },
      useAuth
    ),
  getVLCDByVisitID: (cancelToken, id) =>
    request(
      {
        url: `/private/examination/vl/${id}`,
        method: "GET",
        cancelToken
      },
      useAuth
    ),
  getProfilaksisByVisitID: (cancelToken, id) =>
    request(
      {
        url: `/private/examination/profilaksis/${id}`,
        method: "GET",
        cancelToken
      },
      useAuth
    ),
  createOrUpdateIMS: (cancelToken, visitID, data) =>
    request(
      {
        url: `/private/examination/ims/${visitID}`,
        method: "POST",
        cancelToken,
        data
      },
      useAuth
    ),
  createOrUpdateHIV: (cancelToken, visitID, data) =>
    request(
      {
        url: `/private/examination/hiv/${visitID}`,
        method: "POST",
        cancelToken,
        data
      },
      useAuth
    ),
  createOrUpdateVLCD: (cancelToken, visitID, data) =>
    request(
      {
        url: `/private/examination/vl/${visitID}`,
        method: "POST",
        cancelToken,
        data
      },
      useAuth
    ),
  createOrUpdateProfilaksis: (cancelToken, visitID, data) =>
    request(
      {
        url: `/private/examination/profilaksis/${visitID}`,
        method: "POST",
        cancelToken,
        data
      },
      useAuth
    ),
  endVisit: (cancelToken, visitID) =>
    request(
      {
        url: `/private/visit/${visitID}/checkout`,
        method: "POST",
        cancelToken
      },
      useAuth
    ),
  endVisitIms: (cancelToken, visitID) =>
    request(
      {
        url: `/private/visit/${visitID}/checkout-ims`,
        method: "POST",
        cancelToken
      },
      useAuth
    )
};

const Medicine = {
  listStock: (cancelToken, type, paramsFilter = "") =>
    request(
      {
        url: `/private/medicines/stock/${type}?${paramsFilter}`,
        method: "GET",
        cancelToken
      },
      useAuth
    ),
  stockDetail: (cancelToken, medicineID) =>
    request(
      {
        url: `/private/medicines/stock-detail/${medicineID}`,
        method: "GET",
        cancelToken
      },
      useAuth
    ),
  getLastPrescription: (cancelToken, patientID) =>
    request(
      {
        url: `/private/patient/${patientID}/last-prescription`,
        method: "GET",
        cancelToken
      },
      useAuth
    ),
  getPrescriptionByVisitId: (cancelToken, visitID) =>
    request(
      {
        url: `/private/visit/${visitID}/prescription-by-visit`,
        method: "GET",
        cancelToken
      },
      useAuth
    ),
  createPrescription: (cancelToken, visitID, data) =>
    request(
      {
        url: `/private/visit/${visitID}/prescription`,
        method: "POST",
        cancelToken,
        data
      },
      useAuth
    ),
  giveMedicine: (cancelToken, visitID) =>
    request(
      {
        url: `/private/visit/${visitID}/give-prescription`,
        method: "POST",
        cancelToken
      },
      useAuth
    ),
  updatePrescription: (cancelToken, visitID, data) =>
    request(
      {
        url: `/private/visit/${visitID}/prescription`,
        method: "PUT",
        cancelToken,
        data
      },
      useAuth
    )
};

const Inventory = {
  listByMedicineID: (cancelToken, id, page = 0, limit = 10) =>
    request(
      {
        url: `/private/inventories/${id}?limit=${limit}&page=${page}`,
        method: "GET",
        cancelToken
      },
      useAuth
    )
};

const Msiha = {
  getMeta: (
    cancelToken,
    provinceID = "",
    sudinKabKotaID = "",
    upkID = "",
    month = ""
  ) => request(
    {
      url: `/private/report/lbpha1/meta?month=${month}&provinceId=${provinceID}&sudinKabKotaId=${sudinKabKotaID}&upkId=${upkID}`,
      method: "GET",
      cancelToken
    },
    useAuth
  ),
  enterHiv: (
    cancelToken,
    provinceID = "",
    sudinKabKotaID = "",
    upkID = "",
    month = ""
  ) =>
    request(
      {
        url: `/private/report/lbpha1/masuk-perawatan-hiv?month=${month}&provinceId=${provinceID}&sudinKabKotaId=${sudinKabKotaID}&upkId=${upkID}`,
        method: "GET",
        cancelToken
      },
      useAuth
    ),
  enterArt: (
    cancelToken,
    provinceID = "",
    sudinKabKotaID = "",
    upkID = "",
    month = ""
  ) =>
    request(
      {
        url: `/private/report/lbpha1/masuk-dengan-art?month=${month}&provinceId=${provinceID}&sudinKabKotaId=${sudinKabKotaID}&upkId=${upkID}`,
        method: "GET",
        cancelToken
      },
      useAuth
    ),
  hivIms: (
    cancelToken,
    provinceID = "",
    sudinKabKotaID = "",
    upkID = "",
    month = ""
  ) =>
    request(
      {
        url: `/private/report/lbpha1/hiv-ims?month=${month}&provinceId=${provinceID}&sudinKabKotaId=${sudinKabKotaID}&upkId=${upkID}`,
        method: "GET",
        cancelToken
      },
      useAuth
    ),
  afterArt: (
    cancelToken,
    provinceID = "",
    sudinKabKotaID = "",
    upkID = "",
    month = ""
  ) =>
    request(
      {
        url: `/private/report/lbpha1/dampak-art?month=${month}&provinceId=${provinceID}&sudinKabKotaId=${sudinKabKotaID}&upkId=${upkID}`,
        method: "GET",
        cancelToken
      },
      useAuth
    ),
  coenfectionTb: (
    cancelToken,
    provinceID = "",
    sudinKabKotaID = "",
    upkID = "",
    month = ""
  ) =>
    request(
      {
        url: `/private/report/lbpha1/koinfeksi-tb-hiv?month=${month}&provinceId=${provinceID}&sudinKabKotaId=${sudinKabKotaID}&upkId=${upkID}`,
        method: "GET",
        cancelToken
      },
      useAuth
    ),
  treatmentPpk: (
    cancelToken,
    provinceID = "",
    sudinKabKotaID = "",
    upkID = "",
    month = ""
  ) =>
    request(
      {
        url: `/private/report/lbpha1/pengobatan-ppk?month=${month}&provinceId=${provinceID}&sudinKabKotaId=${sudinKabKotaID}&upkId=${upkID}`,
        method: "GET",
        cancelToken
      },
      useAuth
    ),
  therapyTpt: (
    cancelToken,
    provinceID = "",
    sudinKabKotaID = "",
    upkID = "",
    month = ""
  ) =>
    request(
      {
        url: `/private/report/lbpha1/tpt?month=${month}&provinceId=${provinceID}&sudinKabKotaId=${sudinKabKotaID}&upkId=${upkID}`,
        method: "GET",
        cancelToken
      },
      useAuth
    ),
  viralLoad: (
    cancelToken,
    provinceID = "",
    sudinKabKotaID = "",
    upkID = "",
    month = ""
  ) =>
    request(
      {
        url: `/private/report/lbpha1/viral-load?month=${month}&provinceId=${provinceID}&sudinKabKotaId=${sudinKabKotaID}&upkId=${upkID}`,
        method: "GET",
        cancelToken
      },
      useAuth
    ),
  notifCouple: (
    cancelToken,
    provinceID = "",
    sudinKabKotaID = "",
    upkID = "",
    month = ""
  ) =>
    request(
      {
        url: `/private/report/lbpha1/notifikasi-pasangan?month=${month}&provinceId=${provinceID}&sudinKabKotaId=${sudinKabKotaID}&upkId=${upkID}`,
        method: "GET",
        cancelToken
      },
      useAuth
    ),
  arvSpecial: (
    cancelToken,
    provinceID = "",
    sudinKabKotaID = "",
    upkID = "",
    month = ""
  ) =>
    request(
      {
        url: `/private/report/lbpha1/penggunaan-arv-khusus?month=${month}&provinceId=${provinceID}&sudinKabKotaId=${sudinKabKotaID}&upkId=${upkID}`,
        method: "GET",
        cancelToken
      },
      useAuth
    ),
  Lbpha1Excel: (
    cancelToken,
    provinceID = "",
    sudinKabKotaID = "",
    upkID = "",
    month = ""
  ) =>
    request(
      {
        url: `/private/report/lbpha1/excel?month=${month}&provinceId=${provinceID}&sudinKabKotaId=${sudinKabKotaID}&upkId=${upkID}`,
        method: "GET",
        cancelToken
      },
      useAuth
    ),
};

const Report = {
  lbpha2: (
    cancelToken,
    provinceID = "",
    sudinKabKotaID = "",
    upkID = "",
    month = "",
    withNonArv = false,
    lbpha = false
  ) => {
    let url = `/private/report/lbpha2?month=${month}&provinceId=${provinceID}&sudinKabKotaId=${sudinKabKotaID}&upkId=${upkID}`;
    if (withNonArv) {
      url += `&withNonArv=t`
    }
    if (lbpha) {
      url += `&lbpha=t`
    }
    return request(
      {
        url: url,
        method: "GET",
        cancelToken
      },
      useAuth
    )
  },
  lbpha2Excel: (
    cancelToken,
    provinceID = "",
    sudinKabKotaID = "",
    upkID = "",
    month = "",
    meta = "",
    withNonArv = false,
    lbpha = false
  ) => {
    let url = `/private/report/lbpha2Excel?month=${month}&provinceId=${provinceID}&sudinKabKotaId=${sudinKabKotaID}&upkId=${upkID}&meta=${JSON.stringify(meta)}`
    if (withNonArv) {
      url += `&withNonArv=t`
    }
    if (lbpha) {
      url += `&lbpha=t`
    }
    return request(
      {
        url: url,
        method: "GET",
        cancelToken
      },
      useAuth
    )
  },
  ArvNonArvExcel: (
    cancelToken,
    provinceID = "",
    sudinKabKotaID = "",
    upkID = "",
    month = "",
    meta = "",
    type = null
  ) =>
    request(
      {
        url: `/private/report/ArvNonArvExcel?month=${month}&provinceId=${provinceID}&sudinKabKotaId=${sudinKabKotaID}&upkId=${upkID}&type=${type}&withNonArv=t&meta=${meta ? JSON.stringify(meta) : ''}`,
        method: "GET",
        cancelToken
      },
      useAuth
    ),
  RegimenExcel: (
    cancelToken,
    provinceID = "",
    sudinKabKotaID = "",
    upkID = "",
    month = ""
  ) =>
    request(
      {
        url: `/private/report/regimenExcel?month=${month}&provinceId=${provinceID}&sudinKabKotaId=${sudinKabKotaID}&upkId=${upkID}`,
        method: "GET",
        cancelToken
      },
      useAuth
    ),
  registerLab: (
    cancelToken,
    provinceID = "",
    sudinKabKotaID = "",
    upkID = "",
    month = ""
  ) =>
    request(
      {
        url: `/private/report/register-lab?month=${month}&provinceId=${provinceID}&sudinKabKotaId=${sudinKabKotaID}&upkId=${upkID}`,
        method: "GET",
        cancelToken
      },
      useAuth
    ),
  lbadbDetail: (
    cancelToken,
    provinceID = "",
    sudinKabKotaID = "",
    upkID = "",
    month = "",
    isSixMonth
  ) =>
    request(
      {
        url: `/private/report/lbadb-detail?month=${month}&provinceId=${provinceID}&sudinKabKotaId=${sudinKabKotaID}&upkId=${upkID}${isSixMonth ? `&isSixMonth=true` : ``}`,
        method: "GET",
        cancelToken
      },
      useAuth
    ),
  ims: cancelToken =>
    request(
      {
        url: "/private/report/test-and-treat/ims",
        method: "GET",
        cancelToken
      },
      useAuth
    ),
  hiv: (
    cancelToken,
    provinceID = "",
    sudinKabKotaID = "",
    upkID = "",
    month = ""
  ) =>
    request(
      {
        url: `/private/report/test-and-treat/hiv?month=${month}&provinceId=${provinceID}&sudinKabKotaId=${sudinKabKotaID}&upkId=${upkID}`,
        method: "GET",
        cancelToken
      },
      useAuth
    ),
  hivRecap: (
    cancelToken,
    provinceID = "",
    sudinKabKotaID = "",
    upkID = "",
    month = "",
    excel = false
  ) =>
    request(
      {
        url: `/private/report/test-report/hiv?month=${month}&provinceId=${provinceID}&sudinKabKotaId=${sudinKabKotaID}&upkId=${upkID}&excel=${excel}`,
        method: "GET",
        cancelToken
      },
      useAuth
    ),
  hivRecapIndividual: (
    cancelToken,
    provinceID = "",
    sudinKabKotaID = "",
    upkID = "",
    month = "",
    excel = false
  ) =>
    request(
      {
        url: `/private/report/test-report/hiv-individual?month=${month}&provinceId=${provinceID}&sudinKabKotaId=${sudinKabKotaID}&upkId=${upkID}&excel=${excel}`,
        method: "GET",
        cancelToken
      },
      useAuth
    ),
  recapKunjunganOdha: (
    cancelToken,
    provinceID = "",
    sudinKabKotaID = "",
    upkID = "",
    month = "",
    excel = false
  ) =>
    request(
      {
        url: `/private/report/rekap-rencana-kunjungan-odha?month=${month}&provinceId=${provinceID}&sudinKabKotaId=${sudinKabKotaID}&upkId=${upkID}&excel=${excel}`,
        method: "GET",
        cancelToken
      },
      useAuth
    ),
  recapTestVl: (
    cancelToken,
    provinceID = "",
    sudinKabKotaID = "",
    upkID = "",
    month = "",
    excel = false
  ) =>
    request(
      {
        url: `/private/report/rekap-tes-vl?month=${month}&provinceId=${provinceID}&sudinKabKotaId=${sudinKabKotaID}&upkId=${upkID}&excel=${excel}`,
        method: "GET",
        cancelToken
      },
      useAuth
    ),
  recapTestVlCd4: (
    cancelToken,
    provinceID = "",
    sudinKabKotaID = "",
    upkID = "",
    month = ""
  ) =>
    request(
      {
        url: `/private/report/rekap-tes-vl-cd4?month=${month}&provinceId=${provinceID}&sudinKabKotaId=${sudinKabKotaID}&upkId=${upkID}`,
        method: "GET",
        cancelToken
      },
      useAuth
    ),
  hivById: (
    cancelToken,
    provinceID = "",
    sudinKabKotaID = "",
    upkID = "",
    month = ""
  ) =>
    request(
      {
        url: `/private/report/test-and-treat/hivById?month=${month}&provinceId=${provinceID}&sudinKabKotaId=${sudinKabKotaID}&upkId=${upkID}`,
        method: "GET",
        cancelToken
      },
      useAuth
    ),
  hivExcel: (
    cancelToken,
    provinceID = "",
    sudinKabKotaID = "",
    upkID = "",
    month = "",
    meta
  ) =>
    request(
      {
        url: `/private/report/test-and-treat/hivExcel?month=${month}&provinceId=${provinceID}&sudinKabKotaId=${sudinKabKotaID}&upkId=${upkID}&meta=${JSON.stringify(meta)}`,
        method: "GET",
        cancelToken
      },
      useAuth
    ),
  logistic: {
    avaliableStock: (
      cancelToken,
      provinceID = "",
      sudinKabKotaID = "",
      upkID = "",
      page = 0,
      limit = 1000,
      month = ""
    ) =>
      request(
        {
          url: `/private/report/ketersediaan-obat?provinceId=${provinceID}&sudinKabKotaId=${sudinKabKotaID}&upkId=${upkID}&page=${page}&limit=${limit}&month=${month}`,
          method: "GET",
          cancelToken
        },
        useAuth
      ),
    layananAktif: (cancelToken, month = "", page = 0, limit = 1000) =>
      request(
        {
          url: `/private/report/layanan-aktif?month='${month}'&page=${page}&limit=${limit}`,
          method: "GET",
          cancelToken
        },
        useAuth
      ),
    layananDesentralisasi: (cancelToken, page = 0, limit = 1000) =>
      request(
        {
          url: `/private/report/layanan-desentralisasi?page=${page}&limit=${limit}`,
          method: "GET",
          cancelToken
        },
        useAuth
      ),
    pergerakanStok: (
      cancelToken,
      monthStart = "",
      monthEnd = "",
      page = 0,
      limit = 1000
    ) =>
      request(
        {
          url: `/private/report/pergerakan-stok?monthStart=${monthStart}&monthEnd=${monthEnd}&page=${page}&limit=${limit}`,
          method: "GET",
          cancelToken
        },
        useAuth
      ),
    jumlahPasienIMS: (
      cancelToken,
      provinceID = "",
      sudinKabKotaID = "",
      month = "",
      page = 0,
      limit = 1000
    ) =>
      request(
        {
          url: `/private/report/jumlah-ims?month='${month}'&sudinKabKotaId=${sudinKabKotaID}&provinceId=${provinceID}&page=${page}&limit=${limit}`,
          method: "GET",
          cancelToken
        },
        useAuth
      ),
    relokasi: (
      cancelToken,
      sudinKabKotaID = "",
      month = "",
      page = 0,
      limit = 1000
    ) =>
      request(
        {
          url: `/private/report/relokasi?month='${month}'&sudinKabKotaId=${sudinKabKotaID}&page=${page}&limit=${limit}`,
          method: "GET",
          cancelToken
        },
        useAuth
      ),
    adjustment: (
      cancelToken,
      provinceID = "",
      sudinKabKotaID = "",
      month = "",
      page = 0,
      limit = 1000
    ) =>
      request(
        {
          url: `/private/report/adjustment?month='${month}&provinceId=${provinceID}&sudinKabKotaId=${sudinKabKotaID}&page=${page}&limit=${limit}`,
          method: "GET",
          cancelToken
        },
        useAuth
      ),
    penerimaanObat: (
      cancelToken,
      provinceID = "",
      sudinKabKotaID = "",
      upkID = "",
      month = "",
      page = 0,
      limit = 1000
    ) =>
      request(
        {
          url: `/private/report/penerimaan-obat?month=${month}&provinceId=${provinceID}&sudinKabKotaId=${sudinKabKotaID}&upkId=${upkID}&page=${page}&limit=${limit}`,
          method: "GET",
          cancelToken
        },
        useAuth
      ),
    pengunaanObat: (
      cancelToken,
      provinceID = "",
      sudinKabKotaID = "",
      upkID = "",
      month = "",
      page = 0,
      limit = 1000
    ) =>
      request(
        {
          url: `/private/report/pengunaan-obat?month=${month}&provinceId=${provinceID}&sudinKabKotaId=${sudinKabKotaID}&upkId=${upkID}&page=${page}&limit=${limit}`,
          method: "GET",
          cancelToken
        },
        useAuth
      ),
    relokasi: (cancelToken, filter) => request(
      {
        url: `/private/orders-logistic/report-relokasi?month=${filter.MONTH}`,
        method: "GET",
        cancelToken
      },
      useAuth
    )
  }
};

const Dashboard = {
  intra: cancelToken =>
    request(
      {
        url: `/private/hospitals/1/statistic`,
        method: "GET",
        cancelToken
      },
      useAuth
    )
};

const IMS = {
  tambahKunjungan: (id, data) =>
    request(
      {
        url: `/private/visit/ims/${id}`,
        method: "POST",
        data
      },
      true
    ),
  ubahKunjungan: (id, data) =>
    request(
      {
        url: `/private/visit/ims/${id}`,
        method: "PUT",
        data
      },
      true
    ),
  tambahUbahPemeriksaan: (id, data) =>
    request(
      {
        url: `/private/visit/ims/examination/${id}`,
        method: "POST",
        data
      },
      true
    ),
  tambahUbahPemeriksaanLab: (id, data) =>
    request(
      {
        url: `/private/visit/ims/lab/${id}`,
        method: "POST",
        data
      },
      true
    ),
  tambahUbahPemeriksaanDiagnosis: (id, data) =>
    request(
      {
        url: `/private/visit/ims/diagnosis/${id}`,
        method: "POST",
        data
      },
      true
    ),
  getReagen: () =>
    request(
      {
        url: `/private/visit/ims-reagen`,
        method: "GET",
      },
      true
    ),
  getReagenReal: () =>
    request(
      {
        url: `/private/visit/ims-reagen-real`,
        method: "GET",
      },
      true
    ),
  tambahUbahPemeriksaanFarma: (id, data) =>
    request(
      {
        url: `/private/visit/ims/prescription/${id}`,
        method: "POST",
        data
      },
      true
    ),
  laporan1: (upk = null, sudin = null, province = null, tgl, excel) => {

    // let param = `?tgl=${'2019-08-01'}`
    let param = ``

    if (upk) {
      param = `?upk=${upk}`
    } else if (sudin) {
      param = `?sudin=${sudin}`
    } else if (province) {
      param = `?province=${province}`
    }

    if (tgl) {
      param += `${param.length ? '&' : '?'}tgl=${tgl}`
    }

    if (excel) {
      param += `${param.length ? '&' : '?'}format=excel`
    }

    let options = {
      url: `/private/visit/ims-laporan-by-age${param}`,
      method: "GET",
    };

    // if (excel) {
    //   options = {
    //     ...options,
    //     responseType: 'arraybuffer'
    //   };
    // }

    return request(
      options,
      true
    )
  },
  laporan1All: (upk = null, sudin = null, province = null, tgl, excel) => {
    // let param = `?tgl=${'2019-08-01'}`
    let param = ``

    if (upk) {
      param = `?upk=${upk}`
    } else if (sudin) {
      param = `?sudin=${sudin}`
    } else if (province) {
      param = `?province=${province}`
    }

    if (tgl) {
      param += `${param.length ? '&' : '?'}tgl=${tgl}`
    }

    if (excel) {
      param += `${param.length ? '&' : '?'}format=excel`
    }

    return request(
      {
        url: `/private/visit/ims-laporan-by-all${param}`,
        method: "GET",
      },
      true
    )
  },
  laporan1All2: (upk = null, sudin = null, province = null, tgl, excel) => {
    // let param = `?tgl=${'2019-08-01'}`
    let param = ``

    if (upk) {
      param += `${param.length ? '&' : '?'}upk=${upk}`
    }

    if (sudin) {
      param += `${param.length ? '&' : '?'}sudin=${sudin}`
    }

    if (province) {
      param += `${param.length ? '&' : '?'}province=${province}`
    }

    if (tgl) {
      param += `${param.length ? '&' : '?'}tgl=${tgl}`
    }

    if (excel) {
      param += `${param.length ? '&' : '?'}format=excel`
    }

    return request(
      {
        url: `/private/visit/ims-laporan-by-all-new${param}`,
        method: "GET",
      },
      true
    )
  },
  laporan1old: (upk = null, sudin = null, province = null, tgl, excel) => {
    // let param = `?tgl=${'2019-08-01'}`
    let param = ``

    if (upk) {
      param = `?upk=${upk}`
    } else if (sudin) {
      param = `?sudin=${sudin}`
    } else if (province) {
      param = `?province=${province}`
    }

    if (tgl) {
      param += `${param.length ? '&' : '?'}tgl=${tgl}`
    }

    if (excel) {
      param += `${param.length ? '&' : '?'}format=excel`
    }

    return request(
      {
        url: `/private/visit/ims-laporan-by-age-old${param}`,
        method: "GET",
      },
      true
    )
  },
  laporan2: (upk = null, sudin = null, province = null, tgl, excel) => {
    // let param = `?tgl=${tgl}`
    let param = ''

    if (upk) {
      param = `?upk=${upk}`
    } else if (sudin) {
      param = `?sudin=${sudin}`
    } else if (province) {
      param = `?province=${province}`
    }

    if (tgl) {
      param += `${param.length ? '&' : '?'}tgl=${tgl}`
    }

    if (excel) {
      param += `${param.length ? '&' : '?'}format=excel`
    }

    return request(
      {
        url: `/private/visit/ims-laporan-by-populate${param}`,
        method: "GET",
      },
      true
    )
  },
  laporan2old: (upk = null, sudin = null, province = null, tgl, excel) => {
    // let param = `?tgl=${tgl}`
    let param = ''

    if (upk) {
      param = `?upk=${upk}`
    } else if (sudin) {
      param = `?sudin=${sudin}`
    } else if (province) {
      param = `?province=${province}`
    }

    if (tgl) {
      param += `${param.length ? '&' : '?'}tgl=${tgl}`
    }

    if (excel) {
      param += `${param.length ? '&' : '?'}format=excel`
    }

    return request(
      {
        url: `/private/visit/ims-laporan-by-populate-old${param}`,
        method: "GET",
      },
      true
    )
  },
  medicinesList: () =>
    request(
      {
        url: `/private/visit/ims-medicines`,
        method: "GET"
      },
      true
    ),
  downloadExcel: (path) =>
    request(
      {
        url: `${path}`,
        method: "GET"
      },
      true
    ),
}

const Lab = {
  getUsageVlCd4Data: (cancelToken, id = null) =>
    request(
      {
        url: `/private/lab/penggunaan-vlcd4?${id ? `id=${id}` : ``}`,
        method: "GET",
        cancelToken
      },
      useAuth
    ),
  saveUsageVlCd4Data: (cancelToken, data) =>
    request(
      {
        url: `/private/lab/penggunaan-vlcd4`,
        method: "POST",
        cancelToken,
        data
      },
      useAuth
    ),
  updateUsageVlCd4Data: (cancelToken, data, id) =>
    request(
      {
        url: `/private/lab/penggunaan-vlcd4?${id ? `id=${id}` : ``}`,
        method: "POST",
        cancelToken,
        data
      },
      useAuth
    ),
  deleteUsageVlCd4Data: (cancelToken, id) =>
    request(
      {
        url: `/private/lab/penggunaan-vlcd4?${id ? `id=${id}` : ``}`,
        method: "DELETE",
        cancelToken
      },
      useAuth
    ),
}

export default {
  Auth,
  Dashboard,
  Exam,
  MasterData,
  Inventory,
  Logistic,
  Medicine,
  Report,
  ReportExcel,
  Msiha,
  Patient,
  Visit,
  IMS,
  Lab
};
