export const filterExamForm = data => {
  for (let key in data) {
    if (data[key] === "" || data[key] === 0) {
      delete data[key];
    }
  }
  return data;
};
