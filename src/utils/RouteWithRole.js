import React, { Component } from "react";
import { Route, Redirect } from "react-router-dom";
import { inject, observer } from "mobx-react";
import { c } from "constant";

class RouteWithRole extends Component {
  render() {
    const { commonStore, userStore, role, ...props } = this.props;
    if (commonStore.token) return <Route {...props} />;
    return <Redirect to="/login" />;
  }
}

export default inject(c.STORES.COMMON, c.STORES.USER)(observer(RouteWithRole));
