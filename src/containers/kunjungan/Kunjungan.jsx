import React, { useState } from "react";
import { Button } from "antd";

import { DataTable, Typography } from "components";
import { Link } from "react-router-dom";
import { useParams } from "react-router";
import { Summary } from "./Summary";
import { ENUM } from "constant";
import FormKunjungan from "containers/kunjungan/formKunjungan";

export const Kunjungan = ({
  reload,
  visits,
  pagination,
  onChangeTable,
  onSearch,
  isLoading
}) => {
  const _onFilter = value => {
    console.log("value: ", value);
  };
  const [isVisitFormOpen, setVisitFormOpen] = useState(false);

  const _toggleModalAddVisit = () => {
    setVisitFormOpen(!isVisitFormOpen);
  };

  const { role } = useParams();

  // TODO: Filter perbulan & total visit total odha

  const columns = [
    {
      title: "Nama",
      dataIndex: "patient",
      key: "name",
      render: patient => patient.name
    },
    {
      title: "Status HIV Pasien",
      dataIndex: "patient",
      key: "statusPatient",
      render: patient => ENUM.LABEL_STATUS_PATIENT[patient.statusPatient]
    },
    {
      title: "Status Kunjungan",
      dataIndex: "checkOutDate",
      key: "checkOutDate",
      render: checkOutDate => {
        if (checkOutDate === null) {
          return (
            <Typography fontWeight="bold" fontSize="12px" color="red">
              Dalam Kunjungan
            </Typography>
          );
        }
        return (
          <Typography fontWeight="bold" fontSize="12px" color="green">
            Kunjungan Selesai
          </Typography>
        );
      }
    },
    {
      title: "Aksi",
      key: "action",
      render: item => {
        return (
          <Link
            to={`/data-pasien/${role}/${item.patient.id}`}
            className={`ant-btn hs-btn ${role} ant-btn-primary ant-btn-sm`}
          >
            Detail
          </Link>
        );
      }
    }
  ];

  return (
    <React.Fragment>
      <div className="container h-100 pt-4">
        <div className="row mb-3">
          <Summary onChangeFilter={_onFilter} totalVisit={100} totalOdha={34} />
        </div>
        <div className="row">
          <div className="col-12 bg-white rounded p-4 border">
            <DataTable
              title="Kunjungan"
              columns={columns}
              onSearch={onSearch}
              data={visits}
              pagination={pagination}
              isLoading={isLoading}
              onChange={onChangeTable}
              button={
                <Button
                  onClick={_toggleModalAddVisit}
                  className={`hs-btn-outline ${role}`}
                  icon="plus"
                >
                  Tambah Tes Lab
                </Button>
              }
            />
          </div>
        </div>
      </div>
      {isVisitFormOpen && (
        <FormKunjungan
          reload={reload}
          toggleModal={_toggleModalAddVisit}
        ></FormKunjungan>
      )}
    </React.Fragment>
  );
};
