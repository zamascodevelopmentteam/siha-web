import React from "react";
import { Typography } from "components";
import { useParams } from "react-router";

export const Summary = () => {
  const { role } = useParams();
  return (
    <React.Fragment>
      <div className="col-4">
        <Typography
          fontSize="18px"
          fontWeight="bold"
          color={role}
          className="d-block mb-3"
        >
          Data Kunjungan
        </Typography>
      </div>
    </React.Fragment>
  );
};
