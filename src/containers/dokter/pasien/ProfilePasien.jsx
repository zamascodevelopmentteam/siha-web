import React, { useState, useEffect } from "react";

import { Button } from "antd";
import axios from "axios";
import { Summary } from "../../pasien/profilePasien/Summary";
import { useHistory, useParams } from "react-router";

import DataKunjungan from "../../pasien/profilePasien/DataKunjungan";
import FormDetail from "containers/pasien/form.handler";
import { ENUM, ROLE } from "constant";
import API from '../../../utils/API'
import { openNotification } from '../../../utils/Notification'

import ModalIMSDokter from '../../../components/modal/ModalIMSDokter'
import ModalImsLab from '../../../components/modal/ModalImsLab'

const INITIAL_IMS_STATE = {
  lsmReferral: '',
  reasonVisit: '',
  complaint: '',
}

const INITIAL_IMS_EXAMINATION = {
  upkId: 0,
  provinceId: 0,
  isPregnant: false,
  pregnancyStage: null,
  physicalExamination: {
    dtv: false,
    dts: false,
    nyeri_perut: false,
    lecet: false,
    bintil_sakit: false,
    luka_ulkus: false,
    jengger: false,
    bubo: false,
    nyeri_goyang_serviks: false,
    dtu: false,
    scortum_bengkak: false,
    dta: false,
    dtm: false,
    menstruasi: false,
    tidak_ada: false,
    rujukan: false,
  },
  reasonReferral: '',
  isIvaTested: false,
  ivaTest: false,
  tesLab: true
}

const INITIAL_IMS_LAB_STATE = {
  is_tested: null,
  pmnUretraServiks: null,
  diplokokusIntraselUretraServiks: null,
  pmnAnus: null,
  diplokokusIntraselAnus: null,
  tVaginalis: null,
  kandida: null,
  ph: null,
  sniffTest: null,
  clueCells: null,
  rprVdrl: {
    reagent: "",
    totalReagentUsed: 0,
    result: ""
  },
  tphaTppa: {
    reagent: "",
    totalReagentUsed: 0,
    result: ""
  },
  rprVdrlTiter: "EMPTY",
  etc: ""
}

const INITIAL_IMS_DIAGNOSIS_STATE = {
  "syndrome": {
    "dtv": false,
    "dtu": false,
    "dta": false,
    "prp": false,
    "ulkus": false,
    "bubo": false,
    "pembengkakanSkrotum": false
  },
  "klinis": {
    "herpes": false,
    "sifilis": false,
    "chancroid": false,
    "kondilomaAkuminata": false,
    "konginetalGonorea": false,
    "konginetalSifilis": false
  },
  "lab": {
    "servisitisGonore": false,
    "servisitisNonGonore": false,
    "kandidiasis": false,
    "trikomoniasis": false,
    "proktitisGonore": false,
    "uretritisGonore": false,
    "vaginosisBakteri": false,
    "proktitisNonGonore": false,
    "uretritisNonGonore": false,
    "sifilis": false
  }
}

const INITIAL_MEDICINE = [
  { medicineId: 0, totalQty: 0, totalDays: 0, note: '' }
]

const {
  DIAGNOSIS_SYNDROME_PEREMPUAN,
  DIAGNOSIS_KLINIS_PEREMPUAN,
  DIAGNOSIS_LAB_PEREMPUAN
} = ENUM;

export default ({ patient, reload }) => {
  const [isDetailOpen, setDetailOpen] = useState(false);
  const [isIMSOpen, setIMSOpen] = useState(false);
  const [selectedVisitID, setSelectedVisitID] = useState(0);
  const [isReloadKunjungan, setReloadKunjungan] = useState(false);
  const [latestTestHiv, setLatestTestHiv] = useState()
  const [province, setProvince] = useState([])
  // const [faskes, setFaskes] = useState([])
  const [faskes] = useState([])
  const [ims, setIms] = useState(INITIAL_IMS_STATE)
  const [imsExamination, setImsExamination] = useState(INITIAL_IMS_EXAMINATION)
  const [isIMSLabOpen, setIMSLabOpen] = useState(false)
  const [imsLab, setImsLab] = useState(INITIAL_IMS_LAB_STATE)
  const [imsReagen, setImsReagen] = useState([])
  const [imsDiagnosis, setImsDiagnosis] = useState(INITIAL_IMS_DIAGNOSIS_STATE)
  const [medicineList, setMedicineList] = useState([])
  const [medicineData, setMediciniData] = useState(INITIAL_MEDICINE)
  const [newData, setNewdata] = useState({})

  const { id, role } = useParams();
  const { goBack } = useHistory();

  const _goBack = () => {
    goBack();
  };

  const _toggleModal = {
    IMS: (id = 0, isAdd = false, item = null) => {
      setSelectedVisitID(id);

      if (!isIMSOpen) {
        setIms({
          lsmReferral: item.testIms.lsmReferral,
          reasonVisit: item.testIms.reasonVisit,
          complaint: item.testIms.complaint
        })

        if (Object.keys(item.testIms.physicalExamination).length) {
          setImsExamination({
            ...imsExamination,
            pregnancyStage: item.testIms.pregnancyStage,
            upkId: item.testIms.upkId ? item.testIms.upkId : 0,
            provinceId: item.testIms.upk ? String(item.testIms.upk.sudinKabKotaId) : 0,
            isIvaTested: item.testIms.isIvaTested,
            ivaTest: item.testIms.ivaTest,
            physicalExamination: { ...imsExamination.physicalExamination, ...item.testIms.physicalExamination },
          })
        }
      }

      if (!isIMSOpen && !item) {
        setIms(INITIAL_IMS_STATE)
      }

      setIMSOpen(!isIMSOpen);
    },
    IMSLAB: (id = 0, isAdd = false, item = null, prevItem = null) => {
      setSelectedVisitID(id);

      if (item) {
        if (item.visitPrescription) {
          setMediciniData(item.visitPrescription.medicines);
        }
        // ------------------------
        // setNewdata({
        //   counseling: item
        // });
        // ------------------------
        setImsLab({
          is_tested: item.visitImsLab.isTested,
          pmnUretraServiks: item.visitImsLab.pmnUretraServiks,
          diplokokusIntraselUretraServiks: item.visitImsLab.diplokokusIntraselUretraServiks,
          pmnAnus: item.visitImsLab.pmnAnus,
          diplokokusIntraselAnus: item.visitImsLab.diplokokusIntraselAnus,
          tVaginalis: item.visitImsLab.tVaginalis,
          kandida: item.visitImsLab.kandida,
          ph: item.visitImsLab.ph,
          sniffTest: item.visitImsLab.sniffTest,
          clueCells: item.visitImsLab.clueCells,
          rprVdrl: {
            reagent: item.visitImsLab.rprVdrl.reagent,
            totalReagentUsed: item.visitImsLab.rprVdrl.totalReagentUsed,
            result: item.visitImsLab.rprVdrl.result
          },
          tphaTppa: {
            reagent: item.visitImsLab.tphaTppa.reagent,
            totalReagentUsed: item.visitImsLab.tphaTppa.totalReagentUsed,
            result: item.visitImsLab.tphaTppa.result
          },
          rprVdrlTiter: item.visitImsLab.rprVdrlTiter,
          etc: item.visitImsLab.etc,
          // ------------------------------------------
          pmnVagina: item.visitImsLab.pmnVagina,
          pmnUretra: item.visitImsLab.pmnUretra,
          pmnMata: item.visitImsLab.pmnMata,
          trichomonasVag: item.visitImsLab.trichomonasVag,
          diplokokusIntraselVagina: item.visitImsLab.diplokokusIntraselVagina,
          diplokokusIntraselUretra: item.visitImsLab.diplokokusIntraselUretra,
          diplokokusIntraselMata: item.visitImsLab.diplokokusIntraselMata,
          pseudohifaBlastospora: item.visitImsLab.pseudohifaBlastospora,
          pemeriksaan: item.visitImsLab.pemeriksaan,
          realRprVdrl: {
            // status: null,
            // reagent: "",
            // totalReagentUsed: 0,

            status: item.visitImsLab.realRprVdrl.status,
            reagent: item.visitImsLab.realRprVdrl.reagent,
            totalReagentUsed: item.visitImsLab.realRprVdrl.totalReagentUsed
          },
        })

        let diagnosisSyndrome = item.visitImsLab.diagnosisSyndrome
        let diagnosisKlinis = item.visitImsLab.diagnosisKlinis
        let diagnosisLab = item.visitImsLab.diagnosisLab
        // let prevDiagnosisSyndrome = prevItem.visitImsLab.diagnosisSyndrome
        // let prevDiagnosisKlinis = prevItem.visitImsLab.diagnosisKlinis
        // let prevDiagnosisLab = prevItem.visitImsLab.diagnosisLab
        // console.log(prevItem.visitImsLab);
        let prevDiagnosisSyndrome = prevItem.visitImsLab ? prevItem.visitImsLab.diagnosisSyndrome : []
        let prevDiagnosisKlinis = prevItem.visitImsLab ? prevItem.visitImsLab.diagnosisKlinis : []
        let prevDiagnosisLab = prevItem.visitImsLab ? prevItem.visitImsLab.diagnosisLab : []
        
        let syndrome = {};
        let klinis = {};
        let lab = {};
        let prevSyndrome = {};
        let prevKlinis = {};
        let prevLab = {};

        Object.keys(DIAGNOSIS_SYNDROME_PEREMPUAN).map(k => {
          syndrome[k] = diagnosisSyndrome[k] ? diagnosisSyndrome[k] : false;
          prevSyndrome[k] = prevDiagnosisSyndrome[k] ? prevDiagnosisSyndrome[k] : false;
        })
        syndrome.pembengkakanSkrotum = diagnosisSyndrome.pembengkakanSkrotum ? diagnosisSyndrome.pembengkakanSkrotum : false;
        prevSyndrome.pembengkakanSkrotum = prevDiagnosisSyndrome.pembengkakanSkrotum ? prevDiagnosisSyndrome.pembengkakanSkrotum : false;
        Object.keys(DIAGNOSIS_KLINIS_PEREMPUAN).map(k => {
          klinis[k] = diagnosisKlinis[k] ? diagnosisKlinis[k] : false;
          prevKlinis[k] = prevDiagnosisKlinis[k] ? prevDiagnosisKlinis[k] : false;
        });
        Object.keys(DIAGNOSIS_LAB_PEREMPUAN).map(k => {
          lab[k] = diagnosisLab[k] ? diagnosisLab[k] : false;
          prevLab[k] = prevDiagnosisLab[k] ? prevDiagnosisLab[k] : false;
        });

        setImsDiagnosis({
          syndrome: isAdd ? prevSyndrome : syndrome,
          klinis: isAdd ? prevKlinis : klinis,
          lab: isAdd ? prevLab : lab
        })
      }

      if (!isIMSLabOpen && !item) {
        setIms(INITIAL_IMS_LAB_STATE)
      }

      setIMSLabOpen(!isIMSLabOpen);
    },
    Detail: () => {
      setDetailOpen(!isDetailOpen);
    }
  };

  const _onChange = (name, value) => {
    setIms({ ...ims, [name]: value })
  }

  const _onChangeExamination = (name, value) => {
    setImsExamination({ ...imsExamination, [name]: value })
  }

  const _onChangePhysicalExamination = (value) => {
    let obj = {}

    value.forEach(v => obj[v] = true)

    setImsExamination({
      ...imsExamination,
      physicalExamination: { ...INITIAL_IMS_EXAMINATION['physicalExamination'], ...obj }
    })
  }

  const _onChangeImsDiagnosis = (name, value) => {
    let obj = {}

    value.forEach(v => obj[v] = true)

    setImsDiagnosis({
      ...imsDiagnosis,
      [name]: { ...INITIAL_IMS_DIAGNOSIS_STATE[name], ...obj }
    })
  }

  function _handleAddMedicine() {
    const medicines = medicineData.slice()
    medicines.push({ medicineId: 0, totalQty: 0, totalDays: 0, note: '' })
    setMediciniData(medicines)
  }

  function _handleRemoveMedicine(index) {
    const medicines = [...medicineData.slice(0, index), ...medicineData.slice(index + 1)]
    setMediciniData(medicines)
  }

  async function _handleChangeMedicine(name, value, idx) {
    const newMedicine = [...medicineData];
    newMedicine[idx][name] = value;
    await setMediciniData(newMedicine)
  }

  const _handleSaveIms = async () => {
    try {
      _toggleModal.IMS()

      await API.IMS.ubahKunjungan(selectedVisitID, ims)
      await API.IMS.tambahUbahPemeriksaan(selectedVisitID, imsExamination)

      setReloadKunjungan(true)
      openNotification(
        "success",
        "Berhasil",
        `Pemeriksaan IMS berhasil simpan`
      );
    } catch (ex) {
      openNotification(
        "error",
        "Gagal",
        `Terjadi kesalahan ketika menyimpan pemeriksaan IMS`
      );
    }
  }

  const _handleSaveImsLab = async () => {
    try {
      await API.IMS.tambahUbahPemeriksaanDiagnosis(selectedVisitID, imsDiagnosis)
      const dataTosend = {
        counseling: newData.counseling,
        dateGivenMedicine: new Date(),
        condom: 0,
        lubricant: 0,
        isDraft: true
      }
      await API.IMS.tambahUbahPemeriksaanFarma(selectedVisitID, {
        ...dataTosend,
        medicines: medicineData
      })

      setReloadKunjungan(true)
      _toggleModal.IMSLAB()

      openNotification(
        "success",
        "Berhasil",
        `Diagnosa IMS berhasil simpan`
      );
    } catch (ex) {
      console.log(ex);
      let message = `Terjadi kesalahan ketika menyimpan diagnosa IMS`;
      if (ex.message) {
        message = ex.message;
      }
      openNotification(
        "error",
        "Gagal",
        message
      );
    }
  }

  const _newOnChange = (name, value) =>{
    const newNewData = { ...newData };
    newNewData[name] = value;
    setNewdata(newNewData);
  }

  const _fetchMedicine = async () => {
    try {
      const res = await API.IMS.medicinesList()
      setMedicineList(res.data)
    } catch (ex) {
      openNotification(
        "error",
        "Gagal",
        `Gagal mendapatkan data Obat`
      );
    }
  }

  const _fetchProvince = async () => {
    try {
      let source = axios.CancelToken.source();
      const res = await API.MasterData.Province.list(source.token)
      setProvince(res.data || []);
    } catch (ex) {
      openNotification(
        "error",
        "Gagal",
        `Gagal mendapatkan data provinsi`
      );
    }
  };

  const _fetchReagen = async () => {
    try {
      const res = await API.IMS.getReagenReal()
      setImsReagen(res.data || []);
    } catch (ex) {
      openNotification(
        "error",
        "Gagal",
        `Gagal mendapatkan data reagent`
      );
    }
  };

  useEffect(() => {
    _fetchMedicine();
    _fetchProvince();
    _fetchReagen();
  }, [])

  return (
    <React.Fragment>
      <div className="container h-100 pt-4">
        <div className="row mb-3">
          <div className="col-12">
            <Button
              className={`hs-btn-outline ${role} `}
              icon="left"
              onClick={_goBack}>
              Kembali
            </Button>
          </div>
        </div>
        <div className="row mb-3">
          <div className="col-12 ">
            <Summary
              nik={patient.nik}
              name={patient.name}
              status={patient.statusPatient}
              odhaLama={patient.odhaLama}
              gender={patient.gender}
              tglMeninggal={patient.tglMeninggal}
              toggleModalDetail={_toggleModal.Detail}
              isActive={patient.user.isActive} />
          </div>
        </div>

        <div className="row mb-4">
          <div className="col-12">
            <DataKunjungan
              toggleModalIMS={_toggleModal.IMS}
              toggleModalIMSLab={_toggleModal.IMSLAB}
              dataPasien={patient}
              reloadKunjungan={isReloadKunjungan}
              changeReload={setReloadKunjungan}
              setLatestTestHiv={setLatestTestHiv}
              latestTestHiv={latestTestHiv}
            />
          </div>
        </div>
      </div>

      {isDetailOpen && <FormDetail reload={reload} toggleModal={_toggleModal.Detail} id={id} />}

      {isIMSOpen &&
        <ModalIMSDokter
          onCancel={_toggleModal.IMS}
          onOk={_handleSaveIms}
          isLoading={false}
          role={role || ROLE.RR_STAFF || ROLE.DOCTOR}
          gender={patient.gender}
          onChange={_onChange}
          onChangeExamination={_onChangeExamination}
          onChangePhysicalExamination={_onChangePhysicalExamination}
          data={ims}
          dataExamination={imsExamination}
          province={province}
          faskes={faskes}
        />
      }
      {isIMSLabOpen &&
        <ModalImsLab
          onCancel={_toggleModal.IMSLAB}
          onOk={_handleSaveImsLab}
          isLoading={false}
          role={role || ROLE.RR_STAFF || ROLE.DOCTOR}
          gender={patient.gender}
          onChangeImsDiagnosis={_onChangeImsDiagnosis}
          data={imsLab}
          dataDiagnosis={imsDiagnosis}
          dataMedicine={medicineData}
          reagent={imsReagen}
          medicineList={medicineList}
          onAddMedicines={_handleAddMedicine}
          onRemoveMedicines={_handleRemoveMedicine}
          onChangeMedicine={_handleChangeMedicine}
          newOnChange={_newOnChange}
          newData={newData}
        />
      }
    </React.Fragment>
  );
};
