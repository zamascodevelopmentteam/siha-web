import React, { useState } from "react";
import FormPasien from "./form.handler";

import { DatePicker, Select, Button } from "antd";
import { DataTable, DisplayDate, Typography } from "components";
import { Link } from "react-router-dom";
import { ROLE, ENUM } from "constant";
import { useParams } from "react-router";

const { RangePicker } = DatePicker;

const FILTER_TYPE = {
  visitDate: "visitDate",
  ischeckOut: "ischeckOut",
  isOnTransit: "isOnTransit",
  isComplete: "isComplete"
};

export const DataPasien = ({
  patients,
  pagination,
  onChangeTable,
  onSearch,
  isLoading,
  reload,
  onFilter
}) => {
  const [isAddPatientOpen, setAddPatientOpen] = useState(false);
  const { role } = useParams();

  const _toggleModalAddPatient = () => {
    setAddPatientOpen(!isAddPatientOpen);
  };

  const columns = [
    // {
    //   title: "No",
    //   dataIndex: "number",
    //   key: "number"
    // },
    {
      title: "Nama",
      dataIndex: "name",
      width: "40%"
    },
    {
      title: "Status HIV Pasien",
      // dataIndex: "statusPatient",
      render: i => i.odhaLama ? ENUM.LABEL_STATUS_PATIENT["ODHA LAMA"] : ENUM.LABEL_STATUS_PATIENT[i.statusPatient],
      width: "20%"
    },
    {
      title: "Aksi",
      key: "action",
      render: item => {
        return (
          <Link
            to={`/data-pasien/${role}/${item.id}`}
            className={`ant-btn hs-btn ${role} ant-btn-primary ant-btn-sm`}
          >
            Detail
          </Link>
        );
      },
      width: "20%"
    }
  ];

  return (
    <React.Fragment>
      {isAddPatientOpen && (
        <FormPasien toggleModal={_toggleModalAddPatient} reload={reload} />
      )}
      <div className="container h-100 pt-4">
        <div className="row mb-3 pl-0">
          <div className="col-4 pl-0">
            <Typography
              fontSize="16px"
              fontWeight="bold"
              className="d-block mb-3"
            >
              Periode
            </Typography>
            <RangePicker
              onChange={(date, dtString) =>
                onFilter(FILTER_TYPE.visitDate, dtString)
              }
              placeholder="Pilih Periode"
            />
          </div>
        </div>
        <div className="row">
          <div className="col-12 bg-white rounded p-4 border">
            <DataTable
              title="Pasien UPK"
              onSearch={onSearch}
              columns={columns}
              data={patients}
              pagination={pagination}
              isLoading={isLoading}
              onChange={onChangeTable}
              button={
                role === ROLE.RR_STAFF && (
                  <Button
                    onClick={_toggleModalAddPatient}
                    className={`hs-btn-outline ${ROLE.RR_STAFF}`}
                    icon="plus"
                  >
                    Tambah Pasien
                  </Button>
                )
              }
            />
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};
