import React, { useState, useEffect, useContext } from "react";
import API from "utils/API";
import axios from "axios";
import GlobalContext from "containers/GlobalContext";
import { openNotification } from "utils/Notification";
import { useParams } from "react-router";

import { ModalPatient } from "components";
import { ENUM } from "constant";

const patientEmptyForm = {
  nik: "",
  name: "",
  addressKTP: "",
  addressDomicile: "",
  dateBirth: "",
  gender: "",
  statusPatient: "",
  phone: "",
  namaPmo: "",
  hubunganPmo: "",
  noHpPmo: "",
  email: "",
  tglMeninggal: "",
  lsmPenjangkau: "",
  kelompokPopulasi: [],
  motherNik: "" 
};

const Handler = ({ toggleModal, reload }) => {
  let source = axios.CancelToken.source();
  const user = useContext(GlobalContext);
  const [formData, setFormData] = useState(patientEmptyForm);
  const [isLoading, setLoading] = useState(false);

  const { id } = useParams();

  const isView = user.role !== ENUM.USER_ROLE.RR_STAFF;
  const isEdit =
    user.role === ENUM.USER_ROLE.RR_STAFF && typeof id !== "undefined";

  const _handleChange = (name, value) => {
    setFormData({
      ...formData,
      [name]: value
    });
  };

  const _onSubmit = () => {
    const payload = _constructPayload(formData);
    if (isEdit) {
      _update(payload);
      return;
    }

    _create(payload);
  };

  const _create = payload => {
    setLoading(true);
    API.Patient.create(source.token, payload)
      .then(() => {
        setLoading(false);
        openNotification("success", "Berhasil", "Pasien berhasil ditambahkan!");
        reload(true);
        toggleModal();
      })
      .catch(e => {
        openNotification("error", "Gagal", e.message || String(e.data ? e.data.message : "").replace(/_/g, " "));
        console.error("e: ", e);
        setLoading(false);
      });
  };

  const _update = payload => {
    setLoading(true);
    var date = new Date(payload.dateBirth);
    date.setDate(date.getDate() + 1);

    payload.dateBirth = date;

    API.Patient.update(source.token, id, payload)
      .then(() => {
        setLoading(false);
        openNotification(
          "success",
          "Berhasil",
          "Pasien berhasil diperbaharui!"
        );
        reload(true);
        toggleModal();
      })
      .catch(e => {
        openNotification("error", "Gagal", e.message || String(e.data ? e.data.message : "").replace(/_/g, " "));
        console.error("e: ", e);
        setLoading(false);
      });
  };

  const _loadData = () => {
    setLoading(true);
    API.Patient.getByID(source.token, id)
      .then(rsp => {
        const contructed = _constructResponse(rsp.data || patientEmptyForm);
        setFormData(contructed);
      })
      .catch(e => {
        console.error("e: ", e);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  useEffect(() => {
    if (isEdit || isView) {
      _loadData();
    }

    return () => {
      source.cancel("request canceled");
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <ModalPatient
      onCancel={toggleModal}
      onOk={_onSubmit}
      isLoading={isLoading}
      onChange={_handleChange}
      data={formData}
      isView={isView}
      isEdit={isEdit}
    />
  );
};

export default Handler;

const _constructPayload = data => {
  if (data.statusPatient !== ENUM.STATUS_PATIENT.ODHA && data.statusPatient !== ENUM.STATUS_PATIENT.ODHA_LAMA) {
    data.namaPmo = null;
    data.hubunganPmo = null;
    data.noHpPmo = null;
  }

  return data;
};

const _constructResponse = res => {
  const {
    nik,
    name,
    addressKTP,
    addressDomicile,
    dateBirth,
    gender,
    phone,
    statusPatient,
    weight,
    height,
    namaPmo,
    hubunganPmo,
    noHpPmo,
    user,
    tglMeninggal,
    lsmPenjangkau,
    kelompokPopulasi,
    motherNik
  } = res;

  return {
    nik,
    name,
    addressKTP,
    addressDomicile,
    dateBirth,
    gender,
    phone,
    statusPatient,
    weight,
    height,
    namaPmo,
    hubunganPmo,
    noHpPmo,
    tglMeninggal,
    email: user.email,
    lsmPenjangkau,
    kelompokPopulasi,
    motherNik
  };
};
