import React from "react";

import DataPasien from "./handler";
import ProfilePasien from "./profilePasien";
import { useParams } from "react-router";

const Pasien = ({ role }) => {
  const { id } = useParams();

  return (
    <React.Fragment>
      {!id && <DataPasien role={role} />}
      {id && <ProfilePasien role={role} />}
    </React.Fragment>
  );
};

export default Pasien;
