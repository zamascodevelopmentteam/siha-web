import React, { useEffect, useState } from "react";
import API from "utils/API";
import axios from "axios";
import { useParams } from "react-router";
import { openNotification } from "utils/Notification";

import { ModalIMS } from "components";

const imsEmptyState = {
  id: "",
  ditestIms: false,
  namaReagen: null,
  qtyReagen: 1,
  hasilTestIms: null,
  ditestSifilis: false,
  hasilTestSifilis: false,
  namaReagenData: {
    name: "",
    codeName: ""
  }
};

const Handler = ({ toggleModal, visitID, isAdd = false, reload }) => {
  let source = axios.CancelToken.source();
  const { role } = useParams();

  const [data, setData] = useState(imsEmptyState);
  const [isLoading, setLoading] = useState(false);

  const _loadData = () => {
    setLoading(true);
    API.Exam.getIMSByVisitID(source.token, visitID)
      .then(rsp => {
        if (rsp.data.qtyReagen === 0) {
          rsp.data.qtyReagen = 1
        }

        setData(rsp.data || imsEmptyState);
      })
      .catch(e => {
        console.error("e: ", e);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const _handleChange = (name, value) => {
    setData({
      ...data,
      [name]: value
    });
  };

  const _onSubmit = () => {
    setLoading(true);
    const payload = _constructPayload(data);
    API.Exam.createOrUpdateIMS(source.token, visitID, payload)
      .then(() => {
        setLoading(false);
        openNotification("success", "Berhasil", "Exam IMS berhasil diinput");
        toggleModal();
        reload(true);
      })
      .catch(e => {
        openNotification("error", "Gagal", e.message || String(e.data ? e.data.message : "").replace(/_/g, " "));
        console.error("e: ", e);
        setLoading(false);
      });
  };

  useEffect(() => {
    if (!isAdd) {
      _loadData();
    }

    return () => {
      source.cancel("request canceled");
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isAdd]);

  return (
    <ModalIMS
      isAdd={isAdd}
      onCancel={toggleModal}
      onOk={_onSubmit}
      isLoading={isLoading}
      onChange={_handleChange}
      role={role}
      data={data}
    />
  );
};

export default Handler;

const _constructPayload = data => {
  const {
    ditestIms,
    namaReagen,
    qtyReagen,
    hasilTestIms,
    ditestSifilis,
    hasilTestSifilis
  } = data;

  return {
    ditestIms,
    namaReagen,
    qtyReagen,
    hasilTestIms,
    ditestSifilis,
    hasilTestSifilis
  };
};
