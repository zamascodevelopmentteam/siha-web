import React, { useState, useEffect } from "react";

import { Button } from "antd";
import { Summary } from "./Summary";
import { useHistory, useParams } from "react-router";

import DataKunjungan from "./DataKunjungan";
import RiwayatHIV from "./RiwayatHIV";
import RiwayatIMS from "./RiwayatIMS";
import ConvertODHA from "./ConvertODHA";
import ConfirmOTP from "./ConfirmOTP";
// import FormIMS from "./FormIMS";
import FormHIV from "./FormHIV";
import FormPDP from "./FormPDP";
import FormRecipe from "./FormRecipe";
import FormSisaObat from "./FormSisaObat";
import FormVLCD from "./FormVLCD";
import FormProfilaksis from "./FormProfilaksis";
import FormDetail from "containers/pasien/form.handler";
import { ROLE } from "constant";
import moment from "moment";
import API from '../../../utils/API'
import { openNotification } from '../../../utils/Notification'

import ModalAddIMS from '../../rr/pasien/profilePasien/modal'
import ModalImsLab from "components/modal/ModalImsLab";

const INITIAL_IMS_STATE = {
  lsmReferral: '',
  reasonVisit: '',
  complaint: ''
}

const INITIAL_IMS_LAB_STATE = {
  is_tested: null,
  // pmnUretraServiks: null,
  // diplokokusIntraselUretraServiks: null,
  // pmnAnus: null,
  // diplokokusIntraselAnus: null,
  tVaginalis: null,
  kandida: null,
  ph: null,
  sniffTest: null,
  // clueCells: null,
  rprVdrl: {
    reagent: "",
    totalReagentUsed: 0,
    result: ""
  },
  tphaTppa: {
    reagent: "",
    totalReagentUsed: 0,
    result: ""
  },
  rprVdrlTiter: "EMPTY",
  etc: "",
  realRprVdrl: {
    status: "",
    reagent: "",
    totalReagentUsed: 0,
  },
}

export const ProfilePasien = ({ patient, reload }) => {
  const [isHIVOpen, setHIVOpen] = useState(false);
  const [isDetailOpen, setDetailOpen] = useState(false);
  const [isODHAOpen, setODHAOpen] = useState(false);
  const [isOTPOpen, setOTPOpen] = useState(false);
  const [isIMSOpen, setIMSOpen] = useState(false);
  const [isMedicineOpen, setMedicineOpen] = useState(false);
  const [isPDPOpen, setPDPOpen] = useState(false);
  const [isRecipeOpen, setRecipeOpen] = useState(false);
  const [isRecipeImsOpen, setRecipeImsOpen] = useState(false);
  const [isVLCDOpen, setVLCCDOpen] = useState(false);
  const [isProfilaksisOpen, setProfilaksisOpen] = useState(false);
  const [selectedVisitID, setSelectedVisitID] = useState(0);
  const [isAddForm, setIsAddForm] = useState(false);
  const [isReloadKunjungan, setReloadKunjungan] = useState(false);
  const [selectedVisitDate, setVisitDate] = useState(moment().format());
  const [latestTestHiv, setLatestTestHiv] = useState()
  //new state form of modal ims
  const [ims, setIms] = useState(INITIAL_IMS_STATE)
  const [imsLab, setImsLab] = useState(INITIAL_IMS_LAB_STATE)
  const [imsReagen, setImsReagen] = useState([])
  const [itemPharmaIms, setItemPharmaIms] = useState(null)

  const { id, role } = useParams();
  const { goBack } = useHistory();

  const _goBack = () => {
    goBack();
  };

  const newRole = role;
  useEffect((role = newRole) => {
    if (role === ROLE.LAB_STAFF) {
      _fetchReagen()
    }
  }, [])

  const _fetchReagen = async () => {
    try {
      // const res = await API.IMS.getReagen()
      // setImsReagen(res.data || []);
      const res = await API.IMS.getReagenReal()
      setImsReagen(res.data || []);
    } catch (ex) {
      openNotification(
        "error",
        "Gagal",
        `Gagal mendapatkan data reagent`
      );
    }
  };

  const _toggleModal = {
    HIV: (id = 0, isAdd = false) => {
      setIsAddForm(isAdd);
      setSelectedVisitID(id);
      setHIVOpen(!isHIVOpen);
    },
    IMS: (id = 0, isAdd = false, item = null) => {
      setIsAddForm(isAdd);
      setSelectedVisitID(id);
      if (item) {
        setIms({ lsmReferral: item.testIms.lsmReferral, reasonVisit: item.testIms.reasonVisit, complaint: item.testIms.complaint })
      }

      if (!isIMSOpen && !item) {
        setIms(INITIAL_IMS_STATE)
      }

      setIMSOpen(!isIMSOpen);
    },
    IMSLAB: (id = 0, isAdd = false, item = null) => {
      setIsAddForm(isAdd);
      setSelectedVisitID(id);

      if (item && item.visitImsLab) {
        setImsLab({
          is_tested: item.visitImsLab.isTested,
          pmnUretraServiks: item.visitImsLab.pmnUretraServiks,
          diplokokusIntraselUretraServiks: item.visitImsLab.diplokokusIntraselUretraServiks,
          pmnAnus: item.visitImsLab.pmnAnus,
          diplokokusIntraselAnus: item.visitImsLab.diplokokusIntraselAnus,
          tVaginalis: item.visitImsLab.tVaginalis,
          kandida: item.visitImsLab.kandida,
          ph: item.visitImsLab.ph,
          sniffTest: item.visitImsLab.sniffTest,
          clueCells: item.visitImsLab.clueCells,
          rprVdrl: {
            reagent: item.visitImsLab.rprVdrl.reagent,
            totalReagentUsed: item.visitImsLab.rprVdrl.totalReagentUsed,
            result: item.visitImsLab.rprVdrl.result
          },
          tphaTppa: {
            reagent: item.visitImsLab.tphaTppa.reagent,
            totalReagentUsed: item.visitImsLab.tphaTppa.totalReagentUsed,
            result: item.visitImsLab.tphaTppa.result
          },
          rprVdrlTiter: item.visitImsLab.rprVdrlTiter,
          etc: item.visitImsLab.etc,
          // ------------------------------------------
          pmnVagina: item.visitImsLab.pmnVagina,
          pmnUretra: item.visitImsLab.pmnUretra,
          pmnMata: item.visitImsLab.pmnMata,
          trichomonasVag: item.visitImsLab.trichomonasVag,
          diplokokusIntraselVagina: item.visitImsLab.diplokokusIntraselVagina,
          diplokokusIntraselUretra: item.visitImsLab.diplokokusIntraselUretra,
          diplokokusIntraselMata: item.visitImsLab.diplokokusIntraselMata,
          pseudohifaBlastospora: item.visitImsLab.pseudohifaBlastospora,
          pemeriksaan: item.visitImsLab.pemeriksaan,
          realRprVdrl: {
            // status: null,
            // reagent: "",
            // totalReagentUsed: 0,

            status: item.visitImsLab.realRprVdrl ? item.visitImsLab.realRprVdrl.status : "",
            reagent: item.visitImsLab.realRprVdrl ? item.visitImsLab.realRprVdrl.reagent : "",
            totalReagentUsed: item.visitImsLab.realRprVdrl ? item.visitImsLab.realRprVdrl.totalReagentUsed : 0
          },
        })
      }

      if (!isIMSOpen && !item) {
        setIms(INITIAL_IMS_LAB_STATE)
      }

      setIMSOpen(!isIMSOpen);
    },
    VLCD: (id = 0, isAdd = false) => {
      setIsAddForm(isAdd);
      setSelectedVisitID(id);
      setVLCCDOpen(!isVLCDOpen);
    },
    Profilaksis: (id = 0, isAdd = false) => {
      setIsAddForm(isAdd);
      setSelectedVisitID(id);
      setProfilaksisOpen(!isProfilaksisOpen);
    },
    ODHA: (isReload = false) => {
      if (isReload) {
        reload();
      }
      setODHAOpen(!isODHAOpen);
    },
    OTP: () => setOTPOpen(!isOTPOpen),
    PDP: (id = 0, isAdd = false, date) => {
      setIsAddForm(isAdd);
      setSelectedVisitID(id);
      setPDPOpen(!isPDPOpen);
      setVisitDate(date)
    },
    Medicine: (id = 0) => {
      setSelectedVisitID(id);
      setMedicineOpen(!isMedicineOpen);
    },
    Recipe: (id = 0) => {
      setSelectedVisitID(id);
      setRecipeOpen(!isRecipeOpen);
    },
    RecipeIms: (id = 0, item = null) => {
      setSelectedVisitID(id);

      if (!isRecipeImsOpen) {
        setItemPharmaIms(item)
      }

      setRecipeImsOpen(!isRecipeImsOpen);
    },
    Detail: () => {
      setDetailOpen(!isDetailOpen);
    }
  };

  const _onChange = (name, value) => {
    setIms({ ...ims, [name]: value })
  }

  const _onChangeImsLab = (name, value) => {
    if (name === 'is_tested' && !value) {
      setImsLab({ ...INITIAL_IMS_LAB_STATE, [name]: value })
    } else {
      setImsLab({ ...imsLab, [name]: value })
    }
  }

  const _onChangeImsLabInnerChild = (parentName, name, value) => {
    setImsLab({
      ...imsLab,
      [parentName]: { ...imsLab[parentName], [name]: value }
    })
  }

  const _handleSaveIms = async () => {
    try {
      _toggleModal.IMS()

      if (isAddForm && !role.DOCTOR) {
        await API.IMS.tambahKunjungan(selectedVisitID, ims)
      } else {
        await API.IMS.ubahKunjungan(selectedVisitID, ims)
      }

      setReloadKunjungan(true)
      openNotification(
        "success",
        "Berhasil",
        `Pemeriksaan IMS berhasil ${isAddForm ? 'ditambahkan' : 'diubah'}`
      );
    } catch (ex) {
      openNotification(
        "error",
        "Gagal",
        `Terjadi kesalahan ketika ${isAddForm ? 'menambahkan' : 'mengubah'} pemeriksaan IMS`
      );
    }
  }

  const _handleSaveImsLab = async () => {
    try {
      let req = {}
      if (imsLab.is_tested) {
        console.log(imsLab);
        Object.keys(imsLab).forEach(v => {
          // if (imsLab[v] === null) {
          //   req[v] = false
          // } else {
            req[v] = imsLab[v]
          // }
        })

        Object.keys(imsLab.rprVdrl).forEach(v => {
          req['rprVdrl'][v] = imsLab.rprVdrl[v]

          if (v === 'totalReagentUsed' && typeof (imsLab['rprVdrl'][v]) === 'string') {
            if (/^\d+\b/g.test(imsLab['rprVdrl'][v])) {
              req['rprVdrl'][v] = parseInt(imsLab['rprVdrl'][v])
            } else {
              req['rprVdrl'][v] = 0
            }
          }
        })

        Object.keys(imsLab.tphaTppa).forEach(v => {
          req['tphaTppa'][v] = imsLab.tphaTppa[v]

          if (v === 'totalReagentUsed' && typeof (imsLab['tphaTppa'][v]) === 'string') {
            if (/^\d+\b/g.test(imsLab['tphaTppa'][v])) {
              req['tphaTppa'][v] = parseInt(imsLab['tphaTppa'][v])
            } else {
              req['tphaTppa'][v] = 0
            }
          }
        })
      } else {
        req = { ...INITIAL_IMS_LAB_STATE }

        Object.keys(INITIAL_IMS_LAB_STATE).forEach(v => {
          if (INITIAL_IMS_LAB_STATE[v] === null) {
            req[v] = false
          }
        })
      }

      await API.IMS.tambahUbahPemeriksaanLab(selectedVisitID, req)
      setReloadKunjungan(true)

      _toggleModal.IMS()

      openNotification(
        "success",
        "Berhasil",
        `Pemeriksaan Lab IMS berhasil ${isAddForm ? 'ditambahkan' : 'diubah'}`
      );
    } catch (ex) {
      console.log(ex)
      openNotification(
        "error",
        "Gagal",
        `Terjadi kesalahan ketika ${isAddForm ? 'menambahkan' : 'mengubah'} pemeriksaan Lab IMS`
      );
    }
  }

  const resetPemeriksaanHandler = () => {
    setImsLab({
      ...imsLab,
      pmnUretraServiks: null,
      pmnVagina: null,
      pmnUretra: null,
      pmnAnus: null,
      pmnMata: null,
      trichomonasVag: null,
      clueCells: null,
      diplokokusIntraselUretraServiks: null,
      diplokokusIntraselVagina: null,
      diplokokusIntraselUretra: null,
      diplokokusIntraselAnus: null,
      diplokokusIntraselMata: null,
      pseudohifaBlastospora: null
    })
  }

  // console.log(patient);

  return (
    <React.Fragment >
      <div className="container h-100 pt-4">
        <div className="row mb-3">
          <div className="col-12">
            <Button
              className={`hs-btn-outline ${role} `}
              icon="left"
              onClick={_goBack}
            >
              Kembali
            </Button>
          </div>
        </div>
        <div className="row mb-3">
          <div className="col-12 ">
            <Summary
              nik={patient.nik}
              name={patient.name}
              status={patient.statusPatient}
              odhaLama={patient.odhaLama}
              gender={patient.gender}
              tglMeninggal={patient.tglMeninggal}
              toggleModalDetail={_toggleModal.Detail}
              toggleModalODHA={_toggleModal.ODHA}
              toggleModalOTP={_toggleModal.OTP}
              isActive={patient.user.isActive}
              latestTestHiv={latestTestHiv}
            />
          </div>
        </div>
        {role === ROLE.RR_STAFF && role === ROLE.LAB_STAFF && (
          <div className="row mb-3">
            <div className="col-6">
              <RiwayatHIV />
            </div>
            <div className="col-6">
              <RiwayatIMS />
            </div>
          </div>
        )}
        <div className="row mb-4">
          <div className="col-12">
            <DataKunjungan
              toggleModalVLCD={_toggleModal.VLCD}
              toggleModalHIV={_toggleModal.HIV}
              toggleModalIMS={role !== ROLE.LAB_STAFF ? _toggleModal.IMS : _toggleModal.IMSLAB}
              toggleModalPDP={_toggleModal.PDP}
              toggleModalMedicine={_toggleModal.Medicine}
              toggleModalProfilaksis={_toggleModal.Profilaksis}
              toggleModalRecipe={_toggleModal.Recipe}
              toggleModalRecipeIms={_toggleModal.RecipeIms}
              statusPatient={patient.statusPatient}
              dataPasien={patient}
              reloadKunjungan={isReloadKunjungan}
              changeReload={setReloadKunjungan}
              setLatestTestHiv={setLatestTestHiv}
            />
          </div>
        </div>
      </div>

      {isODHAOpen && <ConvertODHA toggleModal={_toggleModal.ODHA} />}

      {
        isDetailOpen && (
          <FormDetail reload={reload} toggleModal={_toggleModal.Detail} id={id} />
        )
      }

      {isOTPOpen && <ConfirmOTP toggleModal={_toggleModal.OTP} />}

      {
        isIMSOpen && (
          role === ROLE.RR_STAFF || role === ROLE.DOCTOR ? (
            <ModalAddIMS
              onCancel={_toggleModal.IMS}
              onOk={_handleSaveIms}
              isLoading={false}
              role={role || ROLE.RR_STAFF || ROLE.DOCTOR}
              gender={patient.gender}
              onChange={_onChange}
              data={ims}
            />
          ) : (
            <ModalImsLab
              onCancel={_toggleModal.IMS}
              onOk={_handleSaveImsLab}
              isLoading={false}
              role={role || ROLE.RR_STAFF || ROLE.DOCTOR}
              gender={patient.gender}
              onChange={_onChangeImsLab}
              onChangeImsLabInnerChild={_onChangeImsLabInnerChild}
              data={imsLab}
              reagent={imsReagen}
              resetPemeriksaan={resetPemeriksaanHandler}
            />
            // <FormIMS
            //   toggleModal={_toggleModal.IMS}
            //   visitID={selectedVisitID}
            //   isAdd={isAddForm}
            //   reload={setReloadKunjungan}
            // />
          )
        )
      }

      {
        isHIVOpen && (
          <FormHIV
            toggleModal={_toggleModal.HIV}
            visitID={selectedVisitID}
            isAdd={isAddForm}
            dataPasien={patient}
            reload={setReloadKunjungan}
            gender={patient.gender}
          />
        )
      }

      {
        isVLCDOpen && (
          <FormVLCD
            toggleModal={_toggleModal.VLCD}
            visitID={selectedVisitID}
            isAdd={isAddForm}
            reload={setReloadKunjungan}
          />
        )
      }

      {
        isProfilaksisOpen && (
          <FormProfilaksis
            toggleModal={_toggleModal.Profilaksis}
            visitID={selectedVisitID}
            isAdd={isAddForm}
            reload={setReloadKunjungan}
          />
        )
      }

      {
        isPDPOpen && (
          <FormPDP
            toggleModal={_toggleModal.PDP}
            visitID={selectedVisitID}
            isAdd={isAddForm}
            reload={setReloadKunjungan}
            selectedVisitDate={selectedVisitDate}
          />
        )
      }

      {
        (isRecipeOpen || isRecipeImsOpen) && (
          <FormRecipe
            toggleModal={!isRecipeImsOpen ? _toggleModal.Recipe : _toggleModal.RecipeIms}
            visitID={selectedVisitID}
            reload={setReloadKunjungan}
            patient={patient}
            isIms={isRecipeImsOpen}
            item={itemPharmaIms}
          />
        )
      }

      {
        isMedicineOpen && (
          <FormSisaObat
            toggleModal={_toggleModal.Medicine}
            visitID={selectedVisitID}
          />
        )
      }
    </React.Fragment >
  );
};
