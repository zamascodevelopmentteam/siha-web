import React, { useEffect, useState } from "react";
import API from "utils/API";
import axios from "axios";
import { useParams } from "react-router";
import { openNotification } from "utils/Notification";
// import { filterExamForm } from "utils/filter";

import { ModalHIV } from "components";
import { ENUM } from "constant";

const hivEmptyState = {
  id: "",
  lsmPenjangkau: "",
  kelompokPopulasi: "",
  alasanTest: "",
  tanggalTest: "",
  jenisTest: "",
  namaReagenR1: "",
  hasilTestR1: "",
  qtyReagenR1: 1,
  namaReagenR2: "",
  hasilTestR2: "",
  qtyReagenR2: 1,
  namaReagenR3: "",
  hasilTestR3: "",
  qtyReagenR3: 1,
  namaReagenNat: "",
  hasilTestNat: "",
  namaReagenDna: "",
  hasilTestDna: "",
  qtyReagenDna: 1,
  namaReagenRna: "",
  hasilTestRna: "",
  qtyReagenRna: 1,
  namaReagenElisa: "",
  hasilTestElisa: "",
  qtyReagenElisa: 1,
  rujukanLabLanjut: "",
  namaReagenWb: "",
  qtyReagenWb: 1,
  hasilTestWb: "",
  kesimpulanHiv: null,
  namaReagenR12: "",
  hasilTestR12: "",
  qtyReagenR12: 1,
  namaReagenR22: "",
  hasilTestR22: "",
  qtyReagenR22: 1,
  test: []
};

const TEST_RESULT_ORDER = {
  NON_REAKTIF: 1,
  INKONKLUSIF: 2,
  REAKTIF: 3
};

const TEST_RESULT = {
  1: "NON_REAKTIF",
  2: "INKONKLUSIF",
  3: "REAKTIF"
};

const Handler = ({
  toggleModal,
  visitID,
  isAdd = false,
  dataPasien,
  reload,
  gender
}) => {
  let source = axios.CancelToken.source();
  const { role } = useParams();

  const [data, setData] = useState(hivEmptyState);
  const [isLoading, setLoading] = useState(false);
  const [disableReagen, setDisableReagen] = useState(false);
  const [result, setResult] = useState(ENUM.TEST_RESULT.NON_REAKTIF);

  const _loadData = () => {
    setLoading(true);
    API.Exam.getHIVByVisitID(source.token, visitID)
      .then(rsp => {
        const constructPayload = _constructPayload(rsp.data);
        setData(constructPayload.data || hivEmptyState);
        setDisableReagen(constructPayload.disableData);
      })
      .catch(e => {
        console.error("e: ", e);
      })
      .finally(() => {
        setLoading(false);
        // _generateResult();
      });
  };

  const _handleChange = (name, value) => {
    setData({
      ...data,
      [name]: value
    });
  };

  const _generateResult = () => {
    const {
      hasilTestR1,
      hasilTestR2,
      hasilTestR3,
      hasilTestNat,
      hasilTestDna,
      hasilTestRna,
      hasilTestElisa,
      hasilTestWb
    } = data;
    const collectedResult = {
      hasilTestR1,
      hasilTestR2,
      hasilTestR3,
      hasilTestNat,
      hasilTestDna,
      hasilTestRna,
      hasilTestElisa,
      hasilTestWb
    };
    let numbers = [1];

    for (let key in collectedResult) {
      if (collectedResult[key] === "" || collectedResult[key] === null) {
        continue;
      }
      numbers.push(TEST_RESULT_ORDER[collectedResult[key]]);
    }

    numbers.sort((a, b) => b - a);

    setResult(TEST_RESULT[numbers[0]]);
    _handleChange("kesimpulanHiv", TEST_RESULT[numbers[0]]);
  };

  const _onSubmit = () => {
    setLoading(true);
    const payload = data;
    API.Exam.createOrUpdateHIV(source.token, visitID, payload)
      .then(() => {
        setLoading(false);
        openNotification("success", "Berhasil", "Exam HIV berhasil diinput");
        toggleModal();
        reload(true);
      })
      .catch(e => {
        openNotification("error", "Gagal", e.message || String(e.data ? e.data.message : "").replace(/_/g, " "));
        console.error("e: ", e);
        setLoading(false);
      });
  };

  const _addTest = () => {
    let newData = { ...data };

    if (newData.test.length < 5) { 
      newData.test.push({
        type: null,
        name: null,
        qty: 1,
        result: null,
      });
    }
      
    setData(newData);
  }
  
  const _deleteTest = (idx) => {
    let newData = { ...data };
    newData.test.splice(idx, 1);
    newData.test = newData.test.map((r, i) => {
      if (i !== 0) {
        r.type = null;
      }
      return r;
    })
      
    setData(newData);
  }

  const _changeTestValue = (idx, name, value) => {
    let newData = { ...data };
    newData.test[idx][name] = value;

    if (idx < 2 || newData.test[idx].type === "R3") {
      newData['namaReagen' + newData.test[idx].type] = newData.test[idx].name;
      newData['hasilTest' + newData.test[idx].type] = newData.test[idx].result;
      newData['qtyReagen' + newData.test[idx].type] = newData.test[idx].qty;
    } else {
      newData['namaReagen' + newData.test[idx].type + '2'] = newData.test[idx].name;
      newData['hasilTest' + newData.test[idx].type + '2'] = newData.test[idx].result;
      newData['qtyReagen' + newData.test[idx].type + '2'] = newData.test[idx].qty;
      newData['typeReagen' + newData.test[idx].type + '2'] = newData.test[idx].type;
    }
      
    setData(newData);
  }

  useEffect(() => {
    if (!isAdd) {
      _loadData();
    }

    return () => {
      source.cancel("request canceled");
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isAdd]);

  useEffect(() => {
    // _generateResult();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [
    data.hasilTestR1,
    data.hasilTestR2,
    data.hasilTestR3,
    data.hasilTestNat,
    data.hasilTestDna,
    data.hasilTestRna,
    data.hasilTestElisa,
    data.hasilTestWb
  ]);

  return (
    <ModalHIV
      isAdd={isAdd}
      onCancel={toggleModal}
      onOk={_onSubmit}
      isLoading={isLoading}
      onChange={_handleChange}
      role={role}
      data={data}
      result={result}
      generateResult={_generateResult}
      dataPasien={dataPasien}
      disableReagen={disableReagen}
      gender={gender}
      addTest={_addTest}
      deleteTest={_deleteTest}
      changeTestValue={_changeTestValue}
    />
  );
};

export default Handler;

const _constructPayload = rsv => {
  const {
    lsmPenjangkau,
    kelompokPopulasi,
    alasanTest,
    tanggalTest,
    jenisTest,
    namaReagenR1,
    hasilTestR1,
    qtyReagenR1,
    namaReagenR2,
    hasilTestR2,
    qtyReagenR2,
    namaReagenR3,
    hasilTestR3,
    qtyReagenR3,
    namaReagenNat,
    hasilTestNat,
    namaReagenDna,
    hasilTestDna,
    qtyReagenDna,
    namaReagenRna,
    hasilTestRna,
    qtyReagenRna,
    namaReagenElisa,
    hasilTestElisa,
    qtyReagenElisa,
    rujukanLabLanjut,
    namaReagenWb,
    qtyReagenWb,
    hasilTestWb,
    kesimpulanHiv,
    namaReagenR12,
    hasilTestR12,
    qtyReagenR12,
    namaReagenR22,
    hasilTestR22,
    qtyReagenR22,
  } = rsv;

  let test = [];
  if (namaReagenR1) {
    test.push({
      type: 'R1',
      name: namaReagenR1,
      qty: qtyReagenR1,
      result: hasilTestR1,
    });
  } else {
    test.push({
      type: 'R1',
      name: null,
      qty: 1,
      result: null,
    });
  }

  if (namaReagenR2) {
    test.push({
      type: 'R2',
      name: namaReagenR2,
      qty: qtyReagenR2,
      result: hasilTestR2,
    });
  }

  if (namaReagenR12) {
    test.push({
      type: 'R1',
      name: namaReagenR12,
      qty: qtyReagenR12,
      result: hasilTestR12,
    });
  }

  if (namaReagenR22) {
    test.push({
      type: 'R2',
      name: namaReagenR22,
      qty: qtyReagenR22,
      result: hasilTestR22,
    });
  }

  if (namaReagenR3) {
    test.push({
      type: 'R3',
      name: namaReagenR3,
      qty: qtyReagenR3,
      result: hasilTestR3,
    });
  }

  const data = {
    lsmPenjangkau,
    kelompokPopulasi,
    alasanTest,
    tanggalTest,
    jenisTest,
    namaReagenR1,
    hasilTestR1,
    qtyReagenR1: qtyReagenR1 > 0 ? qtyReagenR1 : 1,
    namaReagenR2,
    hasilTestR2,
    qtyReagenR2: qtyReagenR2 > 0 ? qtyReagenR2 : 1,
    namaReagenR3,
    hasilTestR3,
    qtyReagenR3: qtyReagenR3 > 0 ? qtyReagenR3 : 1,
    namaReagenNat,
    hasilTestNat,
    namaReagenDna,
    hasilTestDna,
    qtyReagenDna: qtyReagenDna > 0 ? qtyReagenDna : 1,
    namaReagenRna,
    hasilTestRna,
    qtyReagenRna: qtyReagenRna > 0 ? qtyReagenRna : 1,
    namaReagenElisa,
    hasilTestElisa,
    qtyReagenElisa: qtyReagenElisa > 0 ? qtyReagenElisa : 1,
    rujukanLabLanjut,
    namaReagenWb,
    qtyReagenWb: qtyReagenWb > 0 ? qtyReagenWb : 1,
    hasilTestWb,
    kesimpulanHiv,
    namaReagenR12,
    hasilTestR12,
    qtyReagenR12: qtyReagenR12 > 0 ? qtyReagenR12 : 1,
    namaReagenR22,
    hasilTestR22,
    qtyReagenR22: qtyReagenR22 > 0 ? qtyReagenR22 : 1,
    test
  };
  const disableData = {
    disablenamaReagenR1: namaReagenR1 ? true : false,
    disablehasilTestR1: hasilTestR1 ? true : false,
    disableqtyReagenR1: qtyReagenR1 ? true : false,
    disablenamaReagenR2: namaReagenR2 ? true : false,
    disablehasilTestR2: hasilTestR2 ? true : false,
    disableqtyReagenR2: qtyReagenR2 ? true : false,
    disablenamaReagenR3: namaReagenR3 ? true : false,
    disablehasilTestR3: hasilTestR3 ? true : false,
    disableqtyReagenR3: qtyReagenR3 ? true : false,
    disablenamaReagenNat: namaReagenNat ? true : false,
    disablehasilTestNat: hasilTestNat ? true : false,
    disablenamaReagenDna: namaReagenDna ? true : false,
    disablehasilTestDna: hasilTestDna ? true : false,
    disableqtyReagenDna: qtyReagenDna ? true : false,
    disablenamaReagenRna: namaReagenRna ? true : false,
    disablehasilTestRna: hasilTestRna ? true : false,
    disableqtyReagenRna: qtyReagenRna ? true : false,
    disablerujukanLabLanjut: rujukanLabLanjut ? true : false,
    disablenamaReagenElisa: namaReagenElisa ? true : false,
    disablehasilTestElisa: hasilTestElisa ? true : false,
    disableqtyReagenElisa: qtyReagenElisa ? true : false,
    disablenamaReagenWb: namaReagenWb ? true : false,
    disableqtyReagenWb: qtyReagenWb ? true : false,
    disablehasilTestWb: hasilTestWb ? true : false
  };

  return {
    data,
    disableData
  };
};
