import React, { useEffect, useState } from "react";
import API from "utils/API";
import axios from "axios";

import { useParams } from "react-router";
import { DataKunjungan, deleteConfirm } from "components";
import { openNotification } from "utils/Notification";
// import { ROLE } from "constant";

const Handler = ({
  reloadKunjungan,
  changeReload,
  toggleModalHIV,
  toggleModalIMS,
  toggleModalIMSLab,
  toggleModalPDP,
  toggleModalMedicine,
  toggleModalRecipe,
  toggleModalRecipeIms,
  toggleModalVLCD,
  toggleModalProfilaksis,
  statusPatient,
  setLatestTestHiv,
  dataPasien
}) => {
  let source = axios.CancelToken.source();
  const { id, role } = useParams();
  const [visits, setVisits] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [isReload, setIsReload] = useState(true);
  // const isFarma = role === ROLE.PHARMA_STAFF
  // const isRR = role === ROLE.RR_STAFF
  // const isLab = role === ROLE.LAB_STAFF

  const _reload = () => {
    _loadData();
  };

  const _loadData = () => {
    setIsLoading(true);
    API.Visit.listByPatientID(source.token, id)
      .then(data => {
        const latestTest = data.data.reduce((carry, visit) => {
          if (carry === null && !!visit.testHiv && !!visit.testHiv.tanggalTest) {
            return { ...visit.testHiv, ordinal: visit.ordinal }
          }
          return carry
        }, null)
        setLatestTestHiv(latestTest)
        setVisits(data.data || []);
      })
      .catch(e => {
        console.error("e: ", e);
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  const _handleAdd = e => {
    deleteConfirm({
      onDelete: () => _createVisit(),
      message: `Anda yakin akan menambah kunjungan?`,
      typeConfirm: "warning"
    });
  };

  const _handleAddLab = e => {
    deleteConfirm({
      onDelete: () => _createVisit(),
      message: `Anda yakin akan menambah Test Lab?`,
      typeConfirm: "warning"
    });
  };

  const _endVisit = visitId => {
    setIsLoading(true);
    API.Exam.endVisit(source.token, visitId)
      .then(data => {
        openNotification(
          "success",
          "Berhasil",
          "Pasien berhasil mengakhiri kunjungan"
        );
        setIsReload(true);
      })
      .catch(e => {
        openNotification("error", "Gagal", e.message || String(e.data ? e.data.message : "").replace(/_/g, " "));
        console.error("e: ", e);
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  const _giveMedicine = visitId => {
    setIsLoading(true);
    API.Medicine.giveMedicine(source.token, visitId)
      .then(data => {
        setIsReload(true);
      })
      .catch(e => {
        console.error("e: ", e);
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  const _createVisit = () => {
    setIsLoading(true);
    API.Visit.createNewVisit(source.token, id)
      .then(() => {
        openNotification(
          "success",
          "Berhasil",
          "Pasien berhasil menambah kunjungan"
        );
        _loadData();
      })
      .catch(e => {
        openNotification("error", "Gagal", e.message || String(e.data ? e.data.message : "").replace(/_/g, " "));
        console.error("e: ", e);
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  const _deleteVisit = (id) => {
    deleteConfirm({
      onDelete: () => {
        setIsLoading(true);
        API.Visit.deleteVisit(source.token, id)
          .then(() => {
            openNotification(
              "success",
              "Berhasil",
              "Pasien berhasil menghapus kunjungan"
            );
            _loadData();
          })
          .catch(e => {
            openNotification("error", "Gagal", e.message || String(e.data ? e.data.message : "").replace(/_/g, " "));
            console.error("e: ", e);
          })
          .finally(() => {
            setIsLoading(false);
          });
      },
      message: `Anda yakin akan menghapus kunjungan?`,
      typeConfirm: "warning"
    });
  };

  useEffect(() => {
    _loadData();

    return () => {
      source.cancel("request canceled");
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (reloadKunjungan || isReload) {
      _loadData();
      changeReload(false);
      setIsReload(false);
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [reloadKunjungan, isReload]);
  return (
    <DataKunjungan
      data={visits}
      isLoading={isLoading}
      reload={_reload}
      role={role}
      toggleModalVLCD={toggleModalVLCD}
      toggleModalHIV={toggleModalHIV}
      toggleModalIMS={toggleModalIMS}
      toggleModalIMSLab={toggleModalIMSLab}
      toggleModalPDP={toggleModalPDP}
      toggleModalMedicine={toggleModalMedicine}
      toggleModalRecipe={toggleModalRecipe}
      toggleModalRecipeIms={toggleModalRecipeIms}
      toggleModalProfilaksis={toggleModalProfilaksis}
      statusPatient={statusPatient}
      addVisitLab={_handleAddLab}
      addVisit={_handleAdd}
      endVisit={_endVisit}
      giveMedicine={_giveMedicine}
      dataPasien={dataPasien}
      deteleVisit={_deleteVisit}
    />
  );
};

export default Handler;
