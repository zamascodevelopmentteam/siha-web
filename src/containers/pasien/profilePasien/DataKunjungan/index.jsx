import React from "react";

import DataKunjungan from "./handler";

const index = ({
  toggleModalHIV,
  toggleModalIMS,
  toggleModalIMSLab,
  toggleModalPDP,
  toggleModalMedicine,
  toggleModalRecipe,
  toggleModalRecipeIms,
  toggleModalVLCD,
  toggleModalProfilaksis,
  statusPatient,
  dataPasien,
  reloadKunjungan,
  setLatestTestHiv,
  changeReload
}) => (
    <DataKunjungan
      toggleModalVLCD={toggleModalVLCD}
      toggleModalHIV={toggleModalHIV}
      toggleModalIMSLab={toggleModalIMSLab}
      toggleModalIMS={toggleModalIMS}
      toggleModalPDP={toggleModalPDP}
      toggleModalMedicine={toggleModalMedicine}
      toggleModalProfilaksis={toggleModalProfilaksis}
      toggleModalRecipe={toggleModalRecipe}
      toggleModalRecipeIms={toggleModalRecipeIms}
      statusPatient={statusPatient}
      dataPasien={dataPasien}
      reloadKunjungan={reloadKunjungan}
      changeReload={changeReload}
      setLatestTestHiv={setLatestTestHiv}
    />
  );

export default index;
