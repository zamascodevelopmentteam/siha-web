import React, { useState, useEffect } from "react";
import API from "utils/API";
import axios from "axios";
import { openNotification } from "utils/Notification";
import { useParams } from "react-router";

import { ModalConvertODHA } from "components";

const odhaEmptyForm = {
  namaPmo: "",
  noHpPmo: "",
  hubunganPmo: "",
  phone: ""
};

const RELOAD = true;

const Handler = ({ toggleModal }) => {
  let source = axios.CancelToken.source();
  const [formData, setFormData] = useState(odhaEmptyForm);
  const [isLoading, setLoading] = useState(false);

  const { id } = useParams();

  const _handleChange = (name, value) => {
    setFormData({
      ...formData,
      [name]: value
    });
  };

  const _loadData = () => {
    setLoading(true);
    API.Patient.getByID(source.token, id)
      .then(rsp => {
        const data = _constructResponse(rsp.data);
        setFormData(data || odhaEmptyForm);
      })
      .catch(e => {
        console.error("e: ", e);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const _onSubmit = () => {
    setLoading(true);

    API.Patient.convertToODHA(source.token, id, formData)
      .then(() => {
        setLoading(false);
        openNotification(
          "success",
          "Berhasil",
          "Pasien berhasil diubah menjadi ODHA!"
        );
        toggleModal(RELOAD);
      })
      .catch(e => {
        console.error("e: ", e);
        setLoading(false);
      });
  };

  useEffect(() => {
    _loadData();

    return () => {
      source.cancel("request canceled");
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <ModalConvertODHA
      onCancel={toggleModal}
      onOk={_onSubmit}
      isLoading={isLoading}
      onChange={_handleChange}
      data={formData}
      role="RR_STAFF"
    />
  );
};

export default Handler;

const _constructResponse = res => {
  const { namaPmo, noHpPmo, hubunganPmo, phone } = res;

  return {
    namaPmo,
    noHpPmo,
    hubunganPmo,
    phone
  };
};
