import React, { useEffect, useState } from "react";
import API from "utils/API";
import axios from "axios";
import { useParams } from "react-router";
import { openNotification } from "utils/Notification";

import { ModalProfilaksis } from "components";

const profilaksisEmptyState = {
  tglProfilaksis: "",
  statusProfilaksis: "",
  obatDiberikan: "",
  qtyObat: ""
};

const Handler = ({ toggleModal, visitID, isAdd = false, reload }) => {
  let source = axios.CancelToken.source();
  const { role } = useParams();

  const [data, setData] = useState(profilaksisEmptyState);
  const [isLoading, setLoading] = useState(false);

  const _loadData = () => {
    setLoading(true);
    API.Exam.getProfilaksisByVisitID(source.token, visitID)
      .then(rsp => {
        setData(rsp.data || profilaksisEmptyState);
      })
      .catch(e => {
        console.error("e: ", e);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const _handleChange = (name, value) => {
    setData({
      ...data,
      [name]: value
    });
  };

  const _onSubmit = () => {
    setLoading(true);
    const payload = _constructPayload(data);
    API.Exam.createOrUpdateProfilaksis(source.token, visitID, payload)
      .then(() => {
        setLoading(false);
        openNotification(
          "success",
          "Berhasil",
          "Profilaksis berhasil disimpan"
        );
        toggleModal();
        reload(true);
      })
      .catch(e => {
        openNotification("error", "Gagal", e.message || String(e.data ? e.data.message : "").replace(/_/g, " "));
        console.error("e: ", e);
        setLoading(false);
      });
  };

  useEffect(() => {
    if (!isAdd) {
      _loadData();
    }

    return () => {
      source.cancel("request canceled");
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isAdd]);

  return (
    <ModalProfilaksis
      isAdd={isAdd}
      onCancel={toggleModal}
      onOk={_onSubmit}
      isLoading={isLoading}
      onChange={_handleChange}
      role={role}
      data={data}
    />
  );
};

export default Handler;

const _constructPayload = data => {
  const { tglProfilaksis, statusProfilaksis, obatDiberikan, qtyObat } = data;
  return {
    tglProfilaksis,
    statusProfilaksis,
    obatDiberikan,
    qtyObat
  };
};
