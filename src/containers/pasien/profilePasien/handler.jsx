import React, { useEffect, useState } from "react";
import API from "utils/API";
import axios from "axios";
import { useParams } from "react-router";
import { ROLE } from '../../../constant'
import { ProfilePasien } from "./ProfilePasien";
import ProfilePasienDokter from '../../dokter/pasien/ProfilePasien'

const patientInitialState = {
  nik: "",
  name: "",
  addressKTP: "",
  addressDomicile: "",
  dateBirth: "",
  gender: "",
  phone: "",
  statusPatient: "",
  weight: "",
  height: "",
  namaPmo: "",
  hubunganPmo: "",
  noHpPmo: "",
  user: {
    isActive: false
  }
};

const Handler = () => {
  let source = axios.CancelToken.source();
  const { id, role } = useParams();
  const [patient, setPatient] = useState(patientInitialState);
  const [isLoading, setIsLoading] = useState(false);

  const _loadData = () => {
    setIsLoading(true);
    API.Patient.getByID(source.token, id)
      .then(data => {
        setPatient(data.data || patientInitialState);
      })
      .catch(e => {
        console.error("e: ", e);
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  useEffect(() => {
    _loadData();

    return () => {
      source.cancel("request canceled");
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      {role === ROLE.DOCTOR ?
        <ProfilePasienDokter
          patient={patient}
          isLoading={isLoading}
          reload={_loadData}
          role={role} />
        :
        <ProfilePasien
          patient={patient}
          isLoading={isLoading}
          reload={_loadData}
          role={role} />
      }
    </>
  );
};

export default Handler;
