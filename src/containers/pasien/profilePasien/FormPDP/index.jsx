import React, { useEffect, useState } from "react";
import API from "utils/API";
import axios from "axios";
import shortid from "shortid";
import { useParams } from "react-router";
import { openNotification } from "utils/Notification";
// import { filterExamForm } from "utils/filter";
import { cloneDeep } from "lodash";
import { ENUM, SCORING } from "constant";

import { ModalPDP } from "components";

const pdpEmptyState = {
  id: "",
  treatmentStartDate: "",
  noRegNas: "",
  tglKonfirmasiHivPos: "",
  tglKunjungan: "",
  tglRujukMasuk: "",
  kelompokPopulasi: "",
  statusTb: "",
  tglPengobatanTb: "",
  statusFungsional: "",
  stadiumKlinis: "",
  ppk: "",
  tglPemberianPpk: "",
  statusTbTpt: "",
  tglPemberianObat: "",
  notifikasiPasangan: false,
  couple: [],
  akhirFollowUp: "",
  tglRujukKeluar: "",
  tglMeninggal: "",
  tglBerhentiArv: "",
  weight: "",
  height: "",
  statusPaduan: ENUM.STATUS_PADUAN.ORISINAL,
  uokId: ""
};

const emptyNotification = {
  name: "",
  age: 0,
  gender: "",
  relationship: ""
};

const Handler = ({ toggleModal, visitID, isAdd = false, reload, selectedVisitDate }) => {
  let source = axios.CancelToken.source();
  const { role } = useParams();

  const [data, setData] = useState({ ...pdpEmptyState, treatmentStartDate: selectedVisitDate });
  const [couple, setCouple] = useState([
    {
      id: shortid.generate(),
      ...emptyNotification
    }
  ]);
  const [dataMedicine, setDataMedicine] = useState([
    {
      medicineId: 12,
      amount: null,
      stockQty: null,
      notes: null
    }
  ]);
  const [isLoading, setLoading] = useState(false);
  const [isEdit, setIsEdit] = useState(false);

  // new
  const onAddMedicines = () => {
    setDataMedicine([
      ...dataMedicine,
      {
        medicineId: undefined,
        amount: null,
        stockQty: null,
        notes: null
      }
    ])
  }

  const onRemoveMedicines = (idx) => {
    let newDataMedicines = [...dataMedicine];
    newDataMedicines.splice(idx, 1);
    setDataMedicine(newDataMedicines);
  }

  const inputMedicineChangeHandler = (name, idx, value) => {
    let newDataMedicines = [...dataMedicine];
    newDataMedicines[idx][name] = value;
    setDataMedicine(newDataMedicines);
  }
  // endnew 

  const _handleChange = (name, value) => {
    if (name === "statusTb" && value === ENUM.STATUS_TB.POSITIF_TB) {
      setData({
        ...data,
        [name]: value,
        stadiumKlinis: ENUM.STADIUM_KLINIS.STADIUM_3
      });
    } else if (name === "notifikasiPasangan" && value === ENUM.NOTIF_COUPLE.MENERIMA) {
      if (couple.length === 0) {
        setCouple([
          {
            id: shortid.generate(),
            ...emptyNotification
          }
        ]);
      }
      setData({
        ...data,
        [name]: value
      });
    } else if (name === "statusTbTpt") {
      setData({
        ...data,
        [name]: value,
      });
      if ([ENUM.STATUS_TB_TPT.INH, ENUM.STATUS_TB_TPT["3HP"]].includes(value)) {
        const count = dataMedicine.filter(r => r.medicineId === 28).length;
        if (count <= 0) {
          setDataMedicine([
            {
              medicineId: 28,
              amount: null,
              stockQty: null,
              notes: null
            },
            ...dataMedicine
          ])
        }
      } else {
        const idx = dataMedicine.findIndex(r => r.medicineId === 28);
        if (idx >= 0) {
          onRemoveMedicines(idx);
        }
      }
    } else if (name === "isDiberiOAT") {
      setData({
        ...data,
        [name]: value,
      });
      if (value) {
        const count = dataMedicine.filter(r => r.medicineId === 38).length;
        if (count <= 0) {
          setDataMedicine([
            ...dataMedicine,
            {
              medicineId: 38,
              amount: null,
              stockQty: null,
              notes: null
            },
          ])
        }
      } else {
        const idx = dataMedicine.findIndex(r => r.medicineId === 38);
        if (idx >= 0) {
          onRemoveMedicines(idx);
        }
      }
    } else {
      setData({
        ...data,
        [name]: value
      });
    }
  };

  const _addNotif = () => {
    const newNotif = {
      id: shortid.generate(),
      ...emptyNotification
    };
    const newCouple = cloneDeep(couple);
    newCouple.push(newNotif);

    setCouple(newCouple);
  };

  const _removeNotif = idx => {
    const newCouple = cloneDeep(couple);
    newCouple.splice(idx, 1);
    setCouple(newCouple);
  };

  const _handleChangeNotif = (idx, name, value) => {
    const newCouple = cloneDeep(couple);
    newCouple[idx][name] = value;
    setCouple(newCouple);
  };

  const _loadData = () => {
    setLoading(true);
    API.Visit.getPDP(source.token, visitID)
      .then(rsp => {
        if (!rsp.data.treatmentStartDate) {
          rsp.data.treatmentStartDate = selectedVisitDate
        }
        // rsp.data.tglPengobatanTbSelesai = null;
        if (!rsp.data.bulanPemeriksaan) {
          rsp.data.bulanPemeriksaan = ['Bulan 1'];
        } else {
          rsp.data.bulanPemeriksaan = JSON.parse(rsp.data.bulanPemeriksaan);
        }

        if (!rsp.data.skoring) {
          rsp.data.skoring = {};
          Object.keys(SCORING).map(k => rsp.data.skoring[k] = null)
        } else {
          rsp.data.skoring = JSON.parse(rsp.data.skoring);
        }

        if (!rsp.data.didampingi) {
          rsp.data.didampingi = null;
        }

        // console.log(rsp.data, 'aaaaaaaaaaaaaaaaa');
        setData(rsp.data);
        setCouple(rsp.data.couples);
      })
      .catch(e => {
        console.error("e: ", e);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const _onSubmit = () => {
    const payload = constructPayload(data, couple);
    // payload.kelompokPopulasi = JSON.stringify(payload.kelompokPopulasi);
    payload.bulanPemeriksaan = JSON.stringify(payload.bulanPemeriksaan);
    payload.skoring = JSON.stringify(payload.skoring);
    setLoading(true);
    if (isAdd) {
      API.Visit.createPDP(source.token, visitID, [])
        .then(() => {
          reload(true);
          setLoading(false);
          openNotification("success", "Berhasil", "PDP berhasil diinput");
          toggleModal();
        })
        .catch(e => {
          openNotification("error", "Gagal", e.message || String(e.data ? e.data.message : "").replace(/_/g, " "));
          console.error("e: ", e);
          setLoading(false);
        });
    } else {
      API.Visit.updatePDP(source.token, visitID, payload)
        .then(() => {
          reload(true);
          setLoading(false);
          openNotification("success", "Berhasil", "PDP berhasil diinput");
          toggleModal();
          _onSubmitMedicice();
        })
        .catch(e => {
          openNotification("error", "Gagal", e.message || String(e.data ? e.data.message : "").replace(/_/g, " "));
          console.error("e: ", e);
          setLoading(false);
        });
    }
  };
  // console.log(visitID, 'zzzzzzzzz');
  const _createPrescription = payload => {
    // setLoading(true);
    API.Medicine.createPrescription(source.token, visitID, payload)
      .then(() => {
        // setLoading(false);
        openNotification("success", "Berhasil", "Resep berhasil diinput");
        // reload(true);
      })
      .catch(e => {
        openNotification("error", "Gagal", e.message || String(e.data ? e.data.message : "").replace(/_/g, " "));
        console.error("e: ", e);
        // setLoading(false);
      });
  };

  const _updatePrescription = payload => {
    // setLoading(true);
    API.Medicine.updatePrescription(source.token, visitID, payload)
      .then(() => {
        // setLoading(false);
        openNotification("success", "Berhasil", "Resep berhasil diinput");
        // reload(true);
      })
      .catch(e => {
        openNotification("error", "Gagal", e.message || String(e.data ? e.data.message : "").replace(/_/g, " "));
        console.error("e: ", e);
        // setLoading(false);
      });
  };

  const _onSubmitMedicice = () => {
    const payload = _constructPayloadMedicine(data, dataMedicine);
    if (isEdit) {
      return _updatePrescription(payload);
    }

    return _createPrescription(payload);
  };

  const getMedicideData = () => {
    API.Medicine.getPrescriptionByVisitId(source.token, visitID)
      .then(rsp => {
        if (rsp.data) {
          setIsEdit(true);
          const constructedRSP = _constructResponse(rsp.data);
          // setData(constructedRSP.data);
          setDataMedicine(constructedRSP.medicines);
        } else {
          setIsEdit(false);
          setDataMedicine([
            {
              medicineId: 12,
              amount: null,
              stockQty: null,
              notes: null
            }
          ]);
        }
      })
      .catch(e => {
        console.error("e: ", e);
      })
      .finally(() => {
        setLoading(false);
      });
  }
  // endnew

  useEffect(() => {
    if (!isAdd) {
      _loadData();
      getMedicideData();
    } else {
      _onSubmit();
    }

    return () => {
      source.cancel("request canceled");
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isAdd]);

  if (isAdd) {
    return null;
  }

  return (
    <ModalPDP
      isAdd={isAdd}
      onCancel={toggleModal}
      onOk={_onSubmit}
      isLoading={isLoading}
      onChange={_handleChange}
      onChangeNotif={_handleChangeNotif}
      role={role}
      data={data}
      addNotif={_addNotif}
      removeNotif={_removeNotif}
      couple={couple}
      dataMedicine={dataMedicine}
      dataMedicineAction={{
        onAddMedicines: onAddMedicines,
        onRemoveMedicines: onRemoveMedicines,
        inputMedicineChangeHandler: inputMedicineChangeHandler
      }}
    />
  );
};

export default Handler;

const constructPayload = (payload, couple) => {
  const {
    treatmentStartDate,
    noRegNas,
    tglKonfirmasiHivPos,
    tglKunjungan,
    tglRujukMasuk,
    // kelompokPopulasi,
    statusTb,
    tglPengobatanTb,
    statusFungsional,
    stadiumKlinis,
    ppk,
    tglPemberianPpk,
    statusTbTpt,
    tglPemberianObat,
    notifikasiPasangan,
    akhirFollowUp,
    tglRujukKeluar,
    tglMeninggal,
    tglBerhentiArv,
    weight,
    lsmPenjangkau,
    periksaTB,
    isDiberiOAT,
    height,
    bulanPemeriksaan,
    tglPemberianObatSelesai,
    artStartDate,
    skoring,
    didampingi,
    upkId
  } = payload;

  return {
    treatmentStartDate,
    noRegNas,
    tglKonfirmasiHivPos,
    tglKunjungan,
    tglRujukMasuk,
    // kelompokPopulasi,
    statusTb,
    tglPengobatanTb,
    statusFungsional,
    stadiumKlinis,
    ppk,
    tglPemberianPpk,
    statusTbTpt,
    tglPemberianObat,
    notifikasiPasangan,
    akhirFollowUp,
    tglRujukKeluar,
    tglMeninggal,
    tglBerhentiArv,
    weight,
    height,
    lsmPenjangkau,
    periksaTB,
    isDiberiOAT,
    couple,
    bulanPemeriksaan,
    tglPemberianObatSelesai,
    artStartDate,
    skoring,
    didampingi,
    upkId
  };
};

// new
const _constructPayloadMedicine = (data, medicines) => {
  const newMedicines = [...medicines];
  const { tglPemberianObat, notes, statusPaduan } = data;
  // const newData = { ...data };
  for (let x = 0; x < newMedicines.length; x++) {
    delete newMedicines[x].id;
  }

  // delete newData.id;

  return {
    tglPemberianObat,
    notes,
    statusPaduan,
    medicines: newMedicines
  };
};

const _constructResponse = data => {
  const { prescriptionMedicines, ...restData } = data;

  const medicines = [];
  if (prescriptionMedicines.length) {
    for (let x = 0; x < prescriptionMedicines.length; x++) {
      const newMedicine = {
        id: shortid.generate(),
        medicineId: prescriptionMedicines[x].medicineId,
        stockQty: prescriptionMedicines[x].amount,
        notes: prescriptionMedicines[x].notes,
        jumlahHari: prescriptionMedicines[x].jumlahHari
      };
      medicines.push(newMedicine);
    }
  } else {
    const newMedicine = {
      id: shortid.generate(),
      medicineId: 12,
      amount: null,
      stockQty: null,
      notes: null
    };
    medicines.push(newMedicine);
  }
  const treatments = restData.treatments || []
  return {
    data: {
      ...restData,
      statusPaduan: treatments[0] ? treatments[0].statusPaduan : "",
      tglPemberianObat: treatments[0] ? treatments[0].tglPemberianObat : ""
    },
    medicines
  };
};
// end new
