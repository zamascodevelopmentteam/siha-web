import React, { useEffect, useState } from "react";
import API from "utils/API";
import axios from "axios";
import { useParams } from "react-router";
import { pick, keys } from "lodash";
import { openNotification } from "utils/Notification";

import { ModalMedicine } from "components";

const emptyState = {
  sisaObatArv1: null,
  sisaObatArv2: null,
  sisaObatArv3: null
};

const Handler = ({ toggleModal, visitID }) => {
  let source = axios.CancelToken.source();
  const { role } = useParams();

  const [data, setData] = useState(emptyState);
  const [medicines, setMedicines] = useState({});
  const [isFound, setFound] = useState(false);
  const [isLoading, setLoading] = useState(false);

  const _loadData = () => {
    setLoading(true);
    API.Visit.getPreviousTreatment(source.token, visitID)
      .then(rsp => {
        if (rsp.data.treatment === null) {
          setFound(false);
          return;
        }
        const { isFound, medicines, formData } = _constructResponse(rsp.data);
        setData(formData);
        setMedicines(medicines);
        setFound(isFound);
      })
      .catch(e => {
        console.error("e: ", e);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const _handleChange = (name, value) => {
    setData({
      ...data,
      [name]: value
    });
  };

  const _onSubmit = () => {
    setLoading(true);
    API.Visit.updateSisaObat(source.token, visitID, data)
      .then(() => {
        setLoading(false);
        openNotification("success", "Berhasil", "Sisa Obat berhasil diinput");
        toggleModal();
      })
      .catch(e => {
        openNotification("error", "Gagal", e.message || String(e.data ? e.data.message : "").replace(/_/g, " "));
        console.error("e: ", e);
        setLoading(false);
      });
  };

  useEffect(() => {
    _loadData();

    return () => {
      source.cancel("request canceled");
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <ModalMedicine
      onCancel={toggleModal}
      onOk={_onSubmit}
      isLoading={isLoading}
      onChange={_handleChange}
      role={role}
      data={data}
      medicines={medicines}
      isFound={isFound}
    />
  );
};

export default Handler;

const _constructResponse = rsp => {
  let isFound = false;
  const formData = {};
  const required = {
    obatArv1Data: null,
    obatArv2Data: null,
    obatArv3Data: null
  };

  const medicines = pick(rsp.treatment, keys(required));
  for (let key in medicines) {
    if (medicines[key] === null) {
      delete medicines[key];
      continue;
    }
    isFound = true;
    switch (key) {
      case "obatArv1Data":
        formData.sisaObatArv1 = null;
        break;
      case "obatArv2Data":
        formData.sisaObatArv2 = null;
        break;
      case "obatArv3Data":
        formData.sisaObatArv3 = null;
        break;
      default:
        break;
    }
  }

  return {
    isFound,
    medicines,
    formData
  };
};
