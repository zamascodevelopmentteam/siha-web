import React from "react";

import { Alert, Button } from "antd";
import { SummaryWrapper } from "./style";
import { Typography, DisplayDate } from "components";
import { ENUM, ROLE } from "constant";
import { useParams } from "react-router";

const { HIV_TEST_TYPE } = ENUM

export const Summary = ({
  nik,
  name,
  status,
  gender,
  tglMeninggal,
  toggleModalODHA,
  toggleModalOTP,
  toggleModalDetail,
  latestTestHiv,
  isActive,
  odhaLama
}) => {
  const { role } = useParams();

  const isBanyak = latestTestHiv && latestTestHiv.namaReagenR12Data;

  return (
    <div className="bg-white rounded border p-4 d-flex justify-content-between align-items-center">
      <div>
        <Typography
          fontSize="14px"
          color={role}
          fontWeight="bold"
          className="d-block mb-2"
        >
          Profil Pasien
        </Typography>
        <SummaryWrapper>
          <tbody>
            <tr>
              <td width="200px">NIK</td>
              <td>{nik}</td>
            </tr>
            <tr>
              <td>Nama</td>
              <td>{name}</td>
            </tr>
            <tr>
              <td>Jenis Kelamin</td>
              <td>{gender}</td>
            </tr>
            <tr>
              <td>Status Pasien</td>
              <td>{odhaLama ? ENUM.LABEL_STATUS_PATIENT['ODHA LAMA'] : ENUM.LABEL_STATUS_PATIENT[status]}</td>
            </tr>
            {tglMeninggal && (
              <tr className="text-danger">
                <td>Tanggal Meninggal</td>
                <td>
                  <DisplayDate date={tglMeninggal} />
                </td>
              </tr>
            )}
          </tbody>
        </SummaryWrapper>
        {role === ROLE.RR_STAFF && latestTestHiv &&
          <React.Fragment>
            <Typography
              fontSize="14px"
              color={role}
              fontWeight="bold"
              className="d-block mb-2"
            >
              Hasil Test Terakhir
            </Typography>
            <SummaryWrapper>
              <tbody>
                <tr>
                  <td width="200px">Tanggal Test</td>
                  <td>
                    <DisplayDate date={latestTestHiv.tanggalTest} />
                    {" "} (Kunjungan Ke-{latestTestHiv.ordinal})
                  </td>
                </tr>
                <tr>
                  <td width="200px">Jenis Test</td>
                  <td>{latestTestHiv.jenisTest}</td>
                </tr>

                {latestTestHiv.jenisTest === HIV_TEST_TYPE.RDT &&
                  <React.Fragment>
                    <tr>
                      <td>Nama Reagen R1 {isBanyak ? '(Test 1)' : ''}</td>
                      <td>
                        {latestTestHiv && latestTestHiv.namaReagenR1Data && latestTestHiv.namaReagenR1Data.codeName ?
                          // `${latestTestHiv.namaReagenR1Data.codeName} (Qty: ${latestTestHiv.qtyReagenR1})`
                          `${latestTestHiv.namaReagenR1Data.codeName}`
                          : "-"}
                      </td>
                    </tr>
                    <tr>
                      <td>Hasil Test R1 {isBanyak ? '(Test 1)' : ''}</td>
                      <td>{(latestTestHiv && latestTestHiv.hasilTestR1) || "-"}</td>
                    </tr>
                    <tr>
                      <td>Nama Reagen R2 {isBanyak ? '(Test 1)' : ''}</td>
                      <td>
                        {latestTestHiv && latestTestHiv.namaReagenR2Data && latestTestHiv.namaReagenR2Data.codeName ?
                          `${latestTestHiv.namaReagenR2Data.codeName}`
                          : "-"}
                      </td>
                    </tr>
                    <tr>
                      <td>Hasil Test R2 {isBanyak ? '(Test 1)' : ''}</td>
                      <td>{(latestTestHiv && latestTestHiv.hasilTestR2) || "-"}</td>
                    </tr>
                    {latestTestHiv && latestTestHiv.namaReagenR12Data && (
                      <React.Fragment>
                        <tr>
                          <td>Nama Reagen R1 {isBanyak ? '(Test 2)' : ''}</td>
                          <td>
                            {latestTestHiv && latestTestHiv.namaReagenR12Data && latestTestHiv.namaReagenR12Data.codeName ?
                              `${latestTestHiv.namaReagenR12Data.codeName}`
                              : "-"}
                          </td>
                        </tr>
                        <tr>
                          <td>Hasil Test R1 {isBanyak ? '(Test 2)' : ''}</td>
                          <td>{(latestTestHiv && latestTestHiv.hasilTestR12) || "-"}</td>
                        </tr>
                      </React.Fragment>
                    )}
                    {latestTestHiv && latestTestHiv.namaReagenR22Data && (
                      <React.Fragment>
                        <tr>
                          <td>Nama Reagen R2 {isBanyak ? '(Test 2)' : ''}</td>
                          <td>
                            {latestTestHiv && latestTestHiv.namaReagenR22Data && latestTestHiv.namaReagenR22Data.codeName ?
                              `${latestTestHiv.namaReagenR22Data.codeName}`
                              : "-"}
                          </td>
                        </tr>
                        <tr>
                          <td>Hasil Test R2 {isBanyak ? '(Test 2)' : ''}</td>
                          <td>{(latestTestHiv && latestTestHiv.hasilTestR22) || "-"}</td>
                        </tr>
                      </React.Fragment>
                    )}
                    <tr>
                      <td>Nama Reagen R3</td>
                      <td>
                        {latestTestHiv && latestTestHiv.namaReagenR3Data && latestTestHiv.namaReagenR3Data.codeName ?
                          `${latestTestHiv.namaReagenR3Data.codeName}`
                          : "-"}
                      </td>
                    </tr>
                    <tr>
                      <td>Hasil Test R3</td>
                      <td>{(latestTestHiv && latestTestHiv.hasilTestR3) || "-"}</td>
                    </tr>
                  </React.Fragment>
                }

                {latestTestHiv.jenisTest === HIV_TEST_TYPE.PCR_DNA &&
                  <tr>
                    <td>Hasil Test DNA</td>
                    <td>{(latestTestHiv && latestTestHiv.hasilTestDna) || "-"}</td>
                  </tr>
                }

                {latestTestHiv.jenisTest === HIV_TEST_TYPE.PCR_RNA &&
                  <tr>
                    <td>Hasil Test RNA</td>
                    <td>{(latestTestHiv && latestTestHiv.hasilTestRna) || "-"}</td>
                  </tr>
                }

                <tr>
                  <td>Kesimpulan</td>
                  <td>{latestTestHiv.kesimpulanHiv}</td>
                </tr>
              </tbody>
            </SummaryWrapper>
          </React.Fragment>
        }
      </div>
      <div className="d-flex flex-column">
        {role !== ROLE.PHARMA_STAFF && (
          <Button
            onClick={toggleModalDetail}
            size="large"
            className={`hs-btn-outline ${role} mb-2`}
          >
            Detail Pasien
          </Button>
        )}
        {role === ROLE.RR_STAFF && (
          <React.Fragment>
            {status === ENUM.STATUS_PATIENT.HIV_POSITIF && (
              <Button
                size="large"
                className={`hs-btn ${role} mb-2`}
                onClick={toggleModalODHA}
              >
                ODHA Dalam Pengobatan
              </Button>
            )}

            {(status === ENUM.STATUS_PATIENT.ODHA || status === ENUM.STATUS_PATIENT.ODHA_LAMA) && !isActive && (
              <Button
                size="large"
                className={`hs-btn ${role}`}
                onClick={toggleModalOTP}
              >
                Verifikasi Akun
              </Button>
            )}
            {(status === ENUM.STATUS_PATIENT.ODHA || status === ENUM.STATUS_PATIENT.ODHA_LAMA) && isActive && (
              <Alert message="Akun Aktif" type="info" />
            )}
          </React.Fragment>
        )}
      </div>
    </div>
  );
};
