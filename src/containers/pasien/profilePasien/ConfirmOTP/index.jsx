import React, { useState, useEffect } from "react";
import API from "utils/API";
import axios from "axios";
import { openNotification } from "utils/Notification";
import { useParams } from "react-router";

import { ModalOTP } from "components";

const odhaEmptyForm = {
  namaPmo: "",
  noHpPmo: "",
  hubunganPmo: "",
  email: ""
};

const RELOAD = true;

const Handler = ({ toggleModal }) => {
  let source = axios.CancelToken.source();
  const [otpCode, setOtpCode] = useState(odhaEmptyForm);
  const [email, setEmail] = useState("");
  const [isLoading, setLoading] = useState(false);

  const { id, role } = useParams();

  const _handleChange = value => {
    setOtpCode(value);
  };

  const _requestOTP = () => {
    setLoading(true);
    API.Patient.requestOTP(source.token, id)
      .then(({ data }) => {
        setLoading(false);
        setEmail(data.phone);
        openNotification("success", "Berhasil", "Kode OTP Berhasil dikirim");
      })
      .catch(e => {
        console.error("e: ", e);
        setLoading(false);
      });
  };

  const _onSubmit = () => {
    setLoading(true);

    API.Patient.confirmOTP(source.token, id, { otpCode })
      .then(() => {
        setLoading(false);
        openNotification("success", "Berhasil", "Pasien berhasil diverifikasi");
        toggleModal(RELOAD);
      })
      .catch(e => {
        openNotification("error", "Gagal", e.message || String(e.data ? e.data.message : "").replace(/_/g, " "));
        console.error("e: ", e);
        setLoading(false);
      });
  };

  useEffect(() => {
    _requestOTP();
    return () => {
      source.cancel("request canceled");
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <ModalOTP
      onCancel={toggleModal}
      onOk={_onSubmit}
      isLoading={isLoading}
      onChange={_handleChange}
      email={email}
      requestOTP={_requestOTP}
      role={role}
    />
  );
};

export default Handler;
