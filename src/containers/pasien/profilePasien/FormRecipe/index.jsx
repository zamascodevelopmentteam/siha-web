import React, { useEffect, useState, useContext } from "react";
import API from "utils/API";
import axios from "axios";
import shortid from "shortid";
import { useParams } from "react-router";
import { openNotification } from "utils/Notification";

import { ModalRecipe } from "components";
// import commonStore from "stores/commonStore";
import GlobalContext from "containers/GlobalContext";
import { ROLE, ENUM } from "constant";
import moment from "moment";

const medicineEmptyState = {
  medicineId: "",
  stockQty: 0,
  notes: "",
  jumlahHari: 0
};

const recipeEmptyState = {
  id: "",
  tglPemberianObat: moment().local().format(),
  notes: "",
  statusPaduan: ENUM.STATUS_PADUAN.ORISINAL
};

const INITIAL_MEDICINE_IMS = {
  patientId: 0,
  note: "",
  dateGivenMedicine: new Date(),
  condom: 0,
  lubricant: 0,
  medicines: [
    {
      medicineId: 0,
      tmpMedicineId: 0,
      medicineName: "",
      totalQty: 0,
      totalDays: 0,
      note: "",
    }
  ]
}

const Handler = ({ toggleModal, visitID, reload, patient, isIms, item }) => {
  let source = axios.CancelToken.source();
  const { role } = useParams();
  const user = useContext(GlobalContext);

  const [data, setData] = useState(recipeEmptyState);
  const [recipeCombination, setRecipeCombination] = useState([]);
  const [codeNames, setCodeNames] = useState({});
  const [isLoading, setLoading] = useState(false);
  const [isEdit, setIsEdit] = useState(false);
  const [medicines, setMedicines] = useState([
    { id: shortid.generate(), ...medicineEmptyState }
  ]);
  const [medicinesIms, setMedicinesIms] = useState({ ...INITIAL_MEDICINE_IMS, medicines: [{ ...INITIAL_MEDICINE_IMS.medicines[0] }] })

  let newIsIms = isIms;
  let newItem = item;
  useEffect((isIms = newIsIms, item = newItem) => {
    if (isIms) {
      if (Object.keys(codeNames).length && item && item.visitPrescription) {
        let medList = []
        item.visitPrescription.medicines.forEach(v => {
          let med = Object.keys(codeNames).find(x => codeNames[x] === v.medicineName)
          medList.push({
            ...v,
            tmpMedicineId: med ? parseInt(med) : 0
          })
        })

        setMedicinesIms({
          ...item.visitPrescription,
          // medicines: medList
        })
      }
    }
  }, [codeNames, newIsIms])

  const _addMedicine = () => {
    if (isIms) {
      let med = { ...INITIAL_MEDICINE_IMS.medicines[0] }
      let medIms = [...medicinesIms.medicines]
      medIms.push(med)

      setMedicinesIms({ ...medicinesIms, medicines: medIms })
    } else {
      const newMedicine = {
        id: shortid.generate(),
        ...medicineEmptyState
      };
      setMedicines([...medicines, newMedicine]);
    }
  };

  const _removeMedicine = idx => {
    if (isIms) {
      let med = [...medicinesIms.medicines]
      med.splice(idx, 1)
      setMedicinesIms({ ...medicinesIms, medicines: med })
    } else {
      medicines.splice(idx, 1);
      setMedicines([...medicines]);
    }
  };

  const _handleChangeMedicine = (idx, name, value) => {
    let newMedicines = [...medicines];
    newMedicines[idx][name] = value;

    setMedicines(newMedicines);
  };

  const _loadData = () => {
    setLoading(true);
    API.Medicine.getPrescriptionByVisitId(source.token, visitID)
      .then(rsp => {
        if (rsp.data) {
          setIsEdit(true);
          const constructedRSP = _constructResponse(rsp.data);
          setData(constructedRSP.data);
          setMedicines(constructedRSP.medicines);
        } else {
          setIsEdit(false);
          setData(recipeEmptyState);
        }
      })
      .catch(e => {
        console.error("e: ", e);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const _createPrescription = payload => {
    setLoading(true);
    API.Medicine.createPrescription(source.token, visitID, payload)
      .then(() => {
        if (user.role === ROLE.PHARMA_STAFF) {
          _giveMedicine();
        } else {
          setLoading(false);
          openNotification("success", "Berhasil", "Resep berhasil diinput");
          toggleModal();
          reload(true);
        }
      })
      .catch(e => {
        openNotification("error", "Gagal", e.message || String(e.data ? e.data.message : "").replace(/_/g, " "));
        console.error("e: ", e);
        setLoading(false);
      });
  };

  const _updatePrescription = payload => {
    setLoading(true);
    API.Medicine.updatePrescription(source.token, visitID, payload)
      .then(() => {
        if (user.role === ROLE.PHARMA_STAFF) {
          _giveMedicine();
        } else {
          setLoading(false);
          openNotification("success", "Berhasil", "Resep berhasil diinput");
          toggleModal();
          reload(true);
        }
      })
      .catch(e => {
        openNotification("error", "Gagal", e.message || String(e.data ? e.data.message : "").replace(/_/g, " "));
        console.error("e: ", e);
        setLoading(false);
      });
  };

  const _giveMedicine = () => {
    API.Medicine.giveMedicine(source.token, visitID)
      .then(() => {
        setLoading(false);
        openNotification("success", "Berhasil", "Resep berhasil diinput");
        toggleModal();
        reload(true);
      })
      .catch(e => {
        console.error("e: ", e);
        setLoading(false);
      });
  };

  const _loadMedicine = () => {
    API.Medicine.listStock(source.token, "all")
      .then(rsp => {
        setCodeNames(_constructCodeList(rsp.data || {}));
      })
      .catch(e => {
        console.error("e: ", e);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const _handleChange = (name, value) => {
    setData({
      ...data,
      [name]: value
    });
  };

  const _handleChangeMedicineIms = (name, value, parentName = "", index) => {
    if (parentName.length) {
      let arr = [...medicinesIms[parentName]]
      let newData = arr[index]
      newData[name] = value

      if (name === 'tmpMedicineId') {
        newData.medicineName = codeNames[value]
      }

      setMedicinesIms({
        ...medicinesIms,
        [parentName]: arr
      })
    } else {
      setMedicinesIms({
        ...medicinesIms,
        [name]: value
      })
    }
  }

  const _onSubmit = () => {
    const payload = _constructPayload(data, medicines);

    if (isEdit) {
      return _updatePrescription(payload);
    }

    return _createPrescription(payload);
  };

  const _onSubmitIms = async () => {
    try {
      let dataToSend = { ...medicinesIms, patientId: patient.id };
      dataToSend.isDraft = false;
      await API.IMS.tambahUbahPemeriksaanFarma(visitID, dataToSend)
      await API.Exam.endVisitIms(source.token, visitID);
      // console.log(medicinesIms)
      openNotification(
        "success",
        "Berhasil",
        `Berhasil menyimpan data pemberian obat IMS`
      );
      toggleModal();
      reload(true);
    } catch (ex) {
      console.error(ex)
      openNotification(
        "error",
        "Gagal",
        `Gagal menyimpan data pemberian obat IMS` + (ex.data.message ? ` : ` + ex.data.message : ``)
      );
    }
  }

  const _generateCombination = () => {
    let medList = medicines

    if (isIms) {
      medList = medicinesIms.medicines
    }

    let combination = [];
    for (let x = 0; x < medList.length; x++) {
      const { medicineId } = medList[x];
      if (medicineId) {
        combination.push(codeNames[medicineId]);
      }
    }
    setRecipeCombination(combination);
  };

  useEffect(() => {
    _loadData()
    _loadMedicine();

    return () => {
      source.cancel("request canceled");
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    _generateCombination();

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [medicines, codeNames, medicinesIms]);

  return (
    <ModalRecipe
      onCancel={toggleModal}
      onOk={isIms ? _onSubmitIms : _onSubmit}
      isLoading={isLoading}
      onChange={_handleChange}
      role={role}
      data={data}
      dataIms={medicinesIms}
      medicines={[...medicines]}
      addMedicine={_addMedicine}
      removeMedicine={_removeMedicine}
      onChangeMedicine={_handleChangeMedicine}
      onChangeMedicineIms={_handleChangeMedicineIms}
      recipeCombination={recipeCombination}
      patient={patient}
      isIms={isIms}
    />
  );
};

export default Handler;

const _constructCodeList = rsp => {
  let list = {};
  for (let x = 0; x < rsp.length; x++) {
    list[rsp[x].id] = rsp[x].codename;
  }
  return list;
};

const _constructPayload = (data, medicines) => {
  const newMedicines = [...medicines];
  const { tglPemberianObat, notes, statusPaduan } = data;
  // const newData = { ...data };
  for (let x = 0; x < newMedicines.length; x++) {
    delete newMedicines[x].id;
  }

  // delete newData.id;

  return {
    tglPemberianObat,
    notes,
    statusPaduan,
    medicines: newMedicines
  };
};

const _constructResponse = data => {
  const { prescriptionMedicines, ...restData } = data;

  const medicines = [];
  for (let x = 0; x < prescriptionMedicines.length; x++) {
    const newMedicine = {
      id: shortid.generate(),
      medicineId: prescriptionMedicines[x].medicineId,
      stockQty: prescriptionMedicines[x].amount,
      notes: prescriptionMedicines[x].notes,
      jumlahHari: prescriptionMedicines[x].jumlahHari
    };
    medicines.push(newMedicine);
  }
  const treatments = restData.treatments || []
  return {
    data: {
      ...restData,
      statusPaduan: treatments[0] ? treatments[0].statusPaduan : "",
      tglPemberianObat: treatments[0] ? treatments[0].tglPemberianObat : ""
    },
    medicines
  };
};
