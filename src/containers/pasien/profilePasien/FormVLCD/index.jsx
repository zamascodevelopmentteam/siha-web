import React, { useEffect, useState } from "react";
import API from "utils/API";
import axios from "axios";
import { useParams } from "react-router";
import { openNotification } from "utils/Notification";

import { ModalVLCD4 } from "components";

const vlcdEmptyState = {
  namaReagen: null,
  testVlCd4Type: null,
  qtyReagen: 0,
  hasilTestVlCd4: null,
  reason: null,
  date: null,
  reagentType: null
};

const Handler = ({ toggleModal, visitID, isAdd = false, reload }) => {
  let source = axios.CancelToken.source();
  const { role } = useParams();

  const [data, setData] = useState(vlcdEmptyState);
  const [isLoading, setLoading] = useState(false);

  const _loadData = () => {
    setLoading(true);
    API.Exam.getVLCDByVisitID(source.token, visitID)
      .then(rsp => {
        setData(rsp.data || vlcdEmptyState);
      })
      .catch(e => {
        console.error("e: ", e);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const _handleChange = (name, value) => {
    let newData = {
      ...data,
      [name]: value
    };

    if (name === "testVlCd4Type") {
      newData.namaReagen = null;
      newData.reagentType = null;
    }

    setData(newData);
  };

  const _onSubmit = () => {
    setLoading(true);
    const payload = _constructPayload(data);
    API.Exam.createOrUpdateVLCD(source.token, visitID, payload)
      .then(() => {
        setLoading(false);
        openNotification("success", "Berhasil", "Test VL/CD4 berhasil diinput");
        toggleModal();
        reload(true);
      })
      .catch(e => {
        openNotification("error", "Gagal", e.message || String(e.data ? e.data.message : "").replace(/_/g, " "));
        console.error("e: ", e);
        setLoading(false);
      });
  };

  useEffect(() => {
    if (!isAdd) {
      _loadData();
    }

    return () => {
      source.cancel("request canceled");
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isAdd]);

  return (
    <ModalVLCD4
      isAdd={isAdd}
      onCancel={toggleModal}
      onOk={_onSubmit}
      isLoading={isLoading}
      onChange={_handleChange}
      role={role}
      data={data}
    />
  );
};

export default Handler;

const _constructPayload = data => {
  const { namaReagen, testVlCd4Type, qtyReagen, hasilTestVlCd4, hasilTestInNumber, reason, date, reagentType } = data;
  return {
    namaReagen,
    testVlCd4Type,
    qtyReagen,
    hasilTestVlCd4,
    hasilTestInNumber,
    reason,
    date,
    reagentType
  };
};
