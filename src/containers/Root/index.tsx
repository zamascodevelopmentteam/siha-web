import * as React from "react";
import { c, ROLE } from "constant";
import { inject, observer } from "mobx-react";
import { Route, Switch } from "react-router-dom";
import { ICommonStore } from "stores/commonStore";
import { IUserStore } from "stores/userStore";
import RouteWithRole from "utils/RouteWithRole";
import routes from "config/routes";
import {
  ContentWrapper,
  MainWrapper,
  RootWrapper,
  SidebarWrapper
} from "./style";

import GlobalContext from "containers/GlobalContext";

import { Sidebar, RedirectBasedOnRole, TopBar } from "components";
import { toJS } from "mobx";

interface AppProps { }

interface InjectedProps extends AppProps {
  commonStore: ICommonStore;
  userStore: IUserStore;
}

@inject(c.STORES.COMMON)
@inject(c.STORES.USER)
@observer
class Root extends React.Component<AppProps, {}> {
  get injected() {
    return this.props as InjectedProps;
  }

  render() {
    const { user } = this.injected.userStore;
    return (
      <RootWrapper>
        <GlobalContext.Provider value={toJS(user)}>
          <SidebarWrapper className="border-right overflow-auto">
            <Sidebar />
          </SidebarWrapper>
          <MainWrapper>
            <TopBar />
            <ContentWrapper className="h-100">
              <Switch>
                {user.role === ROLE.RR_STAFF &&
                  routes.rr.map(route => (
                    <RouteWithRole
                      key={route.path}
                      path={route.path}
                      role={route.role}
                      component={route.component}
                    />
                  ))}

                {user.role === ROLE.DOCTOR &&
                  routes.doctor.map(route => (
                    <RouteWithRole
                      key={route.path}
                      path={route.path}
                      role={route.role}
                      component={route.component}
                    />
                  ))}

                {user.role === ROLE.LAB_STAFF &&
                  routes.lab.map(route => (
                    <RouteWithRole
                      key={route.path}
                      path={route.path}
                      role={route.role}
                      component={route.component}
                    />
                  ))}

                {user.role === ROLE.MINISTRY_STAFF &&
                  routes.master.map(route => (
                    <RouteWithRole
                      key={route.path}
                      path={route.path}
                      role={route.role}
                      component={route.component}
                    />
                  ))}

                {/* {user.role === ROLE.PHARMA_STAFF  && */}
                {routes.pharma.map(route => (
                  <RouteWithRole
                    key={route.path}
                    path={route.path}
                    role={route.role}
                    component={route.component}
                  />
                ))}
                {/* ))} */}

                {routes.general.map(route => (
                  <Route
                    key={route.path}
                    path={route.path}
                    component={route.component}
                  />
                ))}

                <Route path="/" component={RedirectBasedOnRole} />
              </Switch>
            </ContentWrapper>
          </MainWrapper>
        </GlobalContext.Provider>
      </RootWrapper>
    );
  }
}

export default Root;
