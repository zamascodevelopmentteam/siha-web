import styled from "styled-components";

export const RootWrapper = styled.div`
  display: flex;
  width: 100vw;
  height: 100vh;
  background-color: #f5f6fa;
`;

export const SidebarWrapper = styled.div`
  width: 240px;
  height: 100%;
  background-color: white;
`;

export const MainWrapper = styled.div`
  width: calc(100% - 220px);
  height: calc(100% - 72px);
`;

export const ContentWrapper = styled.div`
  overflow: auto;
`;
