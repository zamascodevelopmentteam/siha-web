import React, { useState } from "react";
import { DataTable, Filter, DisplayDate } from "components";
import { formatNumber } from "utils";
import { sumberDana } from "constant";
import { Select } from "antd";
import { Label } from "reactstrap";
const { Option } = Select;

export const Report = ({ data, isLoading, onFilter, currentFilter }) => {
  const [type, setType] = useState("");
  const [typeDana, setTypeData] = useState("");
  const [filteredData, setFilteredData] = useState([]);

  const _onChangeType = (value, type) => {
    if (type === "type") {
      setType(value);
      setTypeData("");
    } else {
      setType("");
      setTypeData(value);
    }

    let filteredData2;

    if (value.length) {
      
      filteredData2 = data.filter(item => {
        // let startsWithCondition =
        //   item.name.toLowerCase().startsWith(value.toLowerCase()) ||
        //   item.codename.toLowerCase().startsWith(value.toLowerCase())
        // let includesCondition =
        //   item.name.toLowerCase().includes(value.toLowerCase()) ||
        //   item.codename.toLowerCase().includes(value.toLowerCase())

        // if (startsWithCondition) {
        //   return startsWithCondition
        // } else if (!startsWithCondition && includesCondition) {
        //   return includesCondition
        // } else return null

        let startsWithCondition =
          item.brand.medicine.medicineType.toLowerCase() === value.toLowerCase() ||
          item.inventories[0].fundSource.toLowerCase() === value.toLowerCase()

        if (startsWithCondition) {
          return startsWithCondition
        } else return null
      })
      setFilteredData(filteredData2);
    }
  }

  const columns = [
    {
      title: "No.",
      render: (text, record) => type.length ? (filteredData.indexOf(record) + 1) : (data.indexOf(record) + 1)
    },
    {
      title: "Nama Barang",
      dataIndex: "brand",
      render: brand => brand.medicine ? brand.medicine.medicineName : ""
    },
    {
      title: "Nama Merek",
      dataIndex: "brand",
      render: brand => brand.brandName
    },
    {
      title: "Satuan",
      render: r => r.receivedPkgUnitType
    },
    {
      title: "Nomor Batch",
      render: r => r.batchCode
    },
    {
      title: "Tanggal Kedaluarsa",
      render: r => <DisplayDate date={r.receivedExpiredDate} />
    },
    {
      title: "Sumber Dana",
      render: r => r.inventories[0] ? r.inventories[0].fundSource : '-'
    },
    {
      title: "Jumlah Paket",
      render: item => `${item.receivedPkgQuantity} ${item.receivedPkgUnitType}`
    },
    {
      title: "Harga Satuan",
      render: item => item.inventories[0] ? formatNumber(parseInt(item.inventories[0].packagePrice)) : '-'
    },
    {
      title: "Jumlah Harga",
      render: item => item.inventories[0] ? formatNumber(parseInt(item.inventories[0].packagePrice) * item.receivedPkgQuantity) : '-'
    },
    {
      title: "No. Invoice",
      dataIndex: "receipt",
      render: receipt => receipt.distributionPlan.droppingNumber
    },
  ];

  return (
    <React.Fragment>
      <div className="container h-100 pt-4">
        <div className="mb-2">
          <Filter onFilter={onFilter} currentFilter={currentFilter} />
          <div className="row">
            <div className="col-2 pl-2 mt-2">
              <Label><b>Tipe Obat</b></Label>
              <Select
                style={{ width: "100%" }}
                onChange={(v) => _onChangeType(v, 'type')}
                value={type}
              >
                <Option value="">All</Option>
                <Option value="ARV">ARV</Option>
                <Option value="NON_ARV">Non-ARV</Option>
              </Select>
            </div>
            <div className="col-3 mt-2">
            <Label><b>Sumber Dana</b></Label>
              <Select
                style={{ width: "100%" }}
                onChange={_onChangeType}
                value={typeDana}
              >
                <Option value="">All</Option>
                {sumberDana.map(item => (
                  <Option key={item} value={item} title={item}>
                    {item}
                  </Option>
                ))}
              </Select>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-12 bg-white rounded p-4 border">
            <DataTable
              title="Laporan Rekap Penerimaan Obat ARV & non-ARV"
              columns={columns}
              data={type.length || typeDana.length ? filteredData : data}
              isScrollX
              isLoading={isLoading}
            />
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};