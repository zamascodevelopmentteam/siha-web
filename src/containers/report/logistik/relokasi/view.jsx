import React from "react";
import { DataTable, Filter } from "components";
import { DisplayDate } from "components";

export const Report = ({ data, isLoading, onFilter, currentFilter }) => {
  let key = 0;
  const columns = [
    {
      title: "No. Invoice",
      key: key++,
      render: (r) => r.order.orderCode
    },
    {
      title: "Tanggal",
      key: key++,
      render: (r) => <DisplayDate date={r.order.createdAt} />
    },
    {
      title: "Penerima",
      key: key++,
      render: (r) =>
        (r.order.upkForData && r.order.upkForData.name) ||
        (r.order.sudinKabKotaForData && r.order.sudinKabKotaForData.name) ||
        (r.order.provinceForData && r.order.provinceForData.name)
    },
    {
      title: "Pengirim",
      key: key++,
      render: (r) =>
        (r.order.upkOwnerData && r.order.upkOwnerData.name) ||
        (r.order.sudinKabKotaOwnerData && r.order.sudinKabKotaOwnerData.name) ||
        (r.order.provinceOwnerData && r.order.provinceOwnerData.name)
    },
    {
      title: "Nama Barang",
      key: key++,
      render: (r) => r.medicine.name
    },
    {
      title: "Jumlah",
      key: key++,
      render: (r) => r.packageQuantity + " " + r.packageUnitType
    }
  ];

  return (
    <React.Fragment>
      <div className="container h-100 pt-4">
        <div className="mb-2">
          <Filter onFilter={onFilter} currentFilter={currentFilter} />
        </div>
        <div className="row">
          <div className="col-12 bg-white rounded p-4 border">
            <DataTable
              title="Laporan Relokasi"
              columns={columns}
              data={data}
              isScrollX
              isLoading={isLoading}
            />
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};
