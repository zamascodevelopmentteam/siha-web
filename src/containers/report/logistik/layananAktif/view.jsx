import React from "react";

import { DataTable, Filter } from "components";

export const Report = ({ data, isLoading, onFilter, currentFilter }) => {
  const columns = [
    {
      title: "Fasyankes",
      dataIndex: "name",
      key: "name"
    },
    {
      title: "Kota",
      dataIndex: "sudinKabKota",
      key: "kota",
      render: sudinKabKota => sudinKabKota.name
    },
    {
      title: "Provinsi",
      dataIndex: "sudinKabKota",
      key: "Provinsi",
      render: sudinKabKota => sudinKabKota.province.name
    }
  ];

  return (
    <React.Fragment>
      <div className="container h-100 pt-4">
        <div className="mb-2">
          <Filter
            onFilter={onFilter}
            currentFilter={currentFilter}
            hideLocation
          />
        </div>
        <div className="row">
          <div className="col-12 bg-white rounded p-4 border">
            <DataTable
              title="Layanan Aktif"
              columns={columns}
              data={data}
              isLoading={isLoading}
            />
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};
