import React, { useEffect, useState, useContext } from "react";
import API from "utils/API";
import axios from "axios";

import { Report } from "./view";
import GlobalContext from "containers/GlobalContext";
import { ENUM } from "constant";
import moment from "moment";

const Handler = () => {
  let source = axios.CancelToken.source();

  const user = useContext(GlobalContext);

  const defaultFilter = {
    PROVINCE: [
      ENUM.LOGISTIC_ROLE.PROVINCE_ENTITY,
      ENUM.LOGISTIC_ROLE.SUDIN_ENTITY,
      ENUM.LOGISTIC_ROLE.UPK_ENTITY
    ].includes(user.logisticrole)
      ? user.provinceId
      : "",
    SUDINKAB: [
      ENUM.LOGISTIC_ROLE.SUDIN_ENTITY,
      ENUM.LOGISTIC_ROLE.UPK_ENTITY
    ].includes(user.logisticrole)
      ? user.sudinKabKotaId
      : "",
    UPK: user.logisticrole === ENUM.LOGISTIC_ROLE.UPK_ENTITY ? user.upkId : "",
    MONTH: moment().format('YYYY-MM')
  };

  const [data, setData] = useState([]);
  const [upk, setUpk] = useState({});
  const [filter, setFilter] = useState(defaultFilter);
  const [isLoading, setLoading] = useState(false);

  const _handleFilter = (type, value) => {
    if (typeof value === "undefined") {
      value = "";
    }
    setFilter({
      ...filter,
      [type]: value
    });
  };

  const _loadData = () => {
    setLoading(true);
    API.Report.logistic
      .avaliableStock(
        source.token,
        filter.PROVINCE,
        filter.SUDINKAB,
        filter.UPK,
        null,
        null,
        filter.MONTH
      )
      .then(rsp => {
        setData(rsp.data || []);
        if (rsp.upk) {
          setUpk(rsp.upk);
        }
      })
      .catch(e => {
        console.error("e: ", e);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  useEffect(() => {
    _loadData();

    return () => {
      source.cancel("request canceled");
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [filter]);

  return (
    <Report
      data={data}
      isLoading={isLoading}
      onFilter={_handleFilter}
      currentFilter={filter}
      upk={upk}
    />
  );
};

export default Handler;

// const dummy = [
//   {
//     id: "15003",
//     stockQtyChanges: -60,
//     prevStockQty: 3000,
//     pkgQtyChanges: -2,
//     prevPkgQuantity: 100,
//     prevPkgUnitType: "BOTOL",
//     prevStockUnit: "TABLET",
//     prevExpiredDate: "2019-11-19T00:00:00.000Z",
//     brand: {
//       brandName: "Efavirenz (600 mg)",
//       medicine: {
//         medicineName: "Efavirenz (600 mg)",
//         codeName: "EFV(600mg)",
//         medicineType: "ARV"
//       }
//     },
//     stockQty: 2940,
//     batchCode: "SXWUPLNVMZ",
//     logisticRole: "UPK_ENTITY",
//     packageQty: 98,
//     entitasName: "RS BOGOR MEDICAL CENTER"
//   },
//   {
//     id: "15000",
//     stockQtyChanges: -1,
//     prevStockQty: 2500,
//     pkgQtyChanges: -1,
//     prevPkgQuantity: 100,
//     prevPkgUnitType: "KIT",
//     prevStockUnit: "TEST",
//     prevExpiredDate: "2019-11-26T00:00:00.000Z",
//     brand: {
//       brandName: "Rapid tes HIV 3 Vikia",
//       medicine: {
//         medicineName: "Rapid tes HIV 3 Vikia",
//         codeName: "Rapid tes HIV 3 Vikia",
//         medicineType: "NON_ARV"
//       }
//     },
//     stockQty: 2499,
//     batchCode: "QCQTTXAXYN",
//     logisticRole: "UPK_ENTITY",
//     packageQty: 99,
//     entitasName: "RS BOGOR MEDICAL CENTER"
//   },
//   {
//     id: "15001",
//     stockQtyChanges: -60,
//     prevStockQty: 6000,
//     pkgQtyChanges: -1,
//     prevPkgQuantity: 100,
//     prevPkgUnitType: "BOTOL",
//     prevStockUnit: "TABLET",
//     prevExpiredDate: "2019-11-23T00:00:00.000Z",
//     brand: {
//       brandName: "Neviral",
//       medicine: {
//         medicineName: "Nevirapine",
//         codeName: "NVP(200mg)",
//         medicineType: "ARV"
//       }
//     },
//     stockQty: 5940,
//     batchCode: "DJVSXSMPNG",
//     logisticRole: "UPK_ENTITY",
//     packageQty: 99,
//     entitasName: "RS BOGOR MEDICAL CENTER"
//   },
//   {
//     id: "15002",
//     stockQtyChanges: -60,
//     prevStockQty: 12000,
//     pkgQtyChanges: -1,
//     prevPkgQuantity: 100,
//     prevPkgUnitType: "BOTOL",
//     prevStockUnit: "TABLET",
//     prevExpiredDate: "2019-11-30T00:00:00.000Z",
//     brand: {
//       brandName: "Lopinavir/Ritonavir (100 mg/50 mg)",
//       medicine: {
//         medicineName: "Lopinavir/Ritonavir (100 mg/50 mg)",
//         codeName: "LPV/r(100/50mg)",
//         medicineType: "ARV"
//       }
//     },
//     stockQty: 11940,
//     batchCode: "AEQXYKUNUT",
//     logisticRole: "UPK_ENTITY",
//     packageQty: 99,
//     entitasName: "RS BOGOR MEDICAL CENTER"
//   }
// ];
