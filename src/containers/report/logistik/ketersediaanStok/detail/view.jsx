import React from "react";
import { Modal } from "antd";
import { DataTable, DisplayDate } from "components";

const Form = ({ onOk, onCancel, isLoading, data }) => {
  const columns = [
    {
      title: "Fasyankes",
      dataIndex: "brand",
      key: "1",
      render: brand => brand.medicine.medicineName
    },
    {
      title: "On Hand",
      key: "2",
      render: item => `${item.stockQty} ${item.prevStockUnit}`
    },
    {
      title: "In-Transit",
      key: "3",
      render: item => `${item.prevStockQty} ${item.prevStockUnit}`
    },
    {
      title: "Bulan",
      dataIndex: "prevExpiredDate",
      key: "4",
      render: date => <DisplayDate format="MMMM" date={date} />
    },
    {
      title: "Status",
      dataIndex: "entitasName",
      key: "5"
    }
  ];
  return (
    <Modal
      visible
      maskClosable
      onOk={onOk}
      onCancel={onCancel}
      footer={null}
      width="900px"
    >
      <DataTable
        title="Laporan Ketersediaan Stok ARV"
        columns={columns}
        data={data}
        isLoading={isLoading}
      />
    </Modal>
  );
};

export default Form;
