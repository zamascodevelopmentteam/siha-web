import React, { useState, useContext } from "react";
import { Tag } from "antd";
import { DataTable, Filter } from "components";
import Detail from "./detail";
import { formatNumber } from "utils";
import { TAG_LABEL, ROLE } from "constant";
import GlobalContext from "containers/GlobalContext";

export const Report = ({ data, isLoading, onFilter, currentFilter, upk }) => {
  const user = useContext(GlobalContext);

  const [isDetailOpen, setDetailOpen] = useState(false);
  const [selectedID, selectID] = useState(null);
  const _toggleModal = (id = 0) => {
    if (typeof id === "number") {
      selectID(id);
    }
    setDetailOpen(!isDetailOpen);
  };

  let key = 0;
  const columns = [
    {
      title: "Nama Produk",
      dataIndex: "name",
      key: key++
    },
    [ROLE.MINISTRY_STAFF].includes(user.role) ? {
      title: "Stok Terakhir Provinsi",
      key: key++,
      render: r => formatNumber(r.stok_provinsi)
    } : {},
    [ROLE.PROVINCE_STAFF, ROLE.MINISTRY_STAFF].includes(user.role) ? {
      title: "Stok Terakhir Kabkot",
      key: key++,
      render: r => formatNumber(r.stok_kabkota)
    } : {},
    [ROLE.SUDIN_STAFF, ROLE.PROVINCE_STAFF, ROLE.MINISTRY_STAFF].includes(user.role) ? {
      title: "Stok Terakhir Layanan",
      key: key++,
      render: r => formatNumber(r.stok_layanan)
    } : {},
    {
      title: "Jumlah Pasien",
      key: key++,
      render: r => r.prescriptionMedicines.length
    },
    {
      title: "Total Stok (Botol / Box)",
      key: key++,
      render: r => formatNumber(r.stok_layanan + r.stok_kabkota + r.stok_provinsi + r.stok_pusat)
    },
    {
      title: "Ketersediaan Stok (Bulan)",
      key: key++,
      render: r => {
        let hasil = ((r.stok_layanan + r.stok_kabkota + r.stok_provinsi + r.stok_pusat) / (r.prescriptionMedicines.length > 1 ? r.prescriptionMedicines.length : 1));
        let n = hasil;
        if (Number(n) === n && n % 1 !== 0) {
          hasil = hasil.toFixed(2);
        }

        let classes = "";
        if ([ROLE.PHARMA_STAFF].includes(user.role)) { // Di Layanan : < 1 bulan = stok kurang (kuning) 1– 3 bulan = stok cukup (hijau) >3 bulan = stok lebih (biru)
          if (hasil < 1) {
            classes = "text-warning";
          } else if (hasil >= 1 && hasil < 3) {
            classes = "text-success";
          } else if (hasil >= 3) {
            classes = "text-primary";
          }
        } else if ([ROLE.SUDIN_STAFF].includes(user.role)) { // Di kabkota : < 3 bulan = stok kurang (kuning) 3 – 6 bulan = stok cukup (hijau) >6 bulan = stok lebih (biru)
          if (hasil < 3) {
            classes = "text-warning";
          } else if (hasil >= 3 && hasil < 6) {
            classes = "text-success";
          } else if (hasil >= 6) {
            classes = "text-primary";
          }
        } else if ([ROLE.PROVINCE_STAFF].includes(user.role)) { // Di provinsi : < 6 bulan = stok kurang (kuning) 6– 9 bulan = stok normal (hijau) >9 bulan = stok lebih (biru)
          if (hasil < 6) {
            classes = "text-warning";
          } else if (hasil >= 6 && hasil < 9) {
            classes = "text-success";
          } else if (hasil >= 9) {
            classes = "text-primary";
          }
        }
        
        return (
          <span className={classes}>{hasil}</span>
        )
      }
    },
    {
      title: "Status Stok",
      key: key++,
      render: r => {
        const hasil = (r.stok_layanan + r.stok_kabkota + r.stok_provinsi + r.stok_pusat) / (r.prescriptionMedicines.length > 1 ? r.prescriptionMedicines.length : 1);

        let color = "";
        let label = "";
        if ([ROLE.PHARMA_STAFF].includes(user.role)) { // Di Layanan : < 1 bulan = stok kurang (kuning) 1– 3 bulan = stok cukup (hijau) >3 bulan = stok lebih (biru)
          if (hasil < 1) {
            color = "orange";
            label = "Stok Kurang";
          } else if (hasil >= 1 && hasil < 3) {
            color = "green";
            label = "Stok Cukup";
          } else if (hasil >= 3) {
            color = "blue";
            label = "Stok Lebih";
          }
        } else if ([ROLE.SUDIN_STAFF].includes(user.role)) { // Di kabkota : < 3 bulan = stok kurang (kuning) 3 – 6 bulan = stok cukup (hijau) >6 bulan = stok lebih (biru)
          if (hasil < 3) {
            color = "orange";
            label = "Stok Kurang";
          } else if (hasil >= 3 && hasil < 6) {
            color = "green";
            label = "Stok Cukup";
          } else if (hasil >= 6) {
            color = "blue";
            label = "Stok Lebih";
          }
        } else if ([ROLE.PROVINCE_STAFF].includes(user.role)) { // Di provinsi : < 6 bulan = stok kurang (kuning) 6– 9 bulan = stok normal (hijau) >9 bulan = stok lebih (biru)
          if (hasil < 6) {
            color = "orange";
            label = "Stok Kurang";
          } else if (hasil >= 6 && hasil < 9) {
            color = "green";
            label = "Stok Cukup";
          } else if (hasil >= 9) {
            color = "blue";
            label = "Stok Lebih";
          }
        }

        return (
          <Tag color={color}>
            {label}
          </Tag>
        )
      }
    }
    // {
    //   title: "Aksi",
    //   key: key++,
    //   render: item => (
    //     <Button
    //       type="primary"
    //       size="small"
    //       onClick={() => _toggleModal(item.id)}
    //     >
    //       Detail
    //     </Button>
    //   )
    // }
  ];

  return (
    <React.Fragment>
      {isDetailOpen && <Detail toggleModal={_toggleModal} id={selectedID} />}
      <div className="container h-100 pt-4">
        <div className="mb-2">
          <Filter
            // hidePeriode
            onFilter={onFilter}
            currentFilter={currentFilter}
          />
        </div>
        <div className="row">
          <div className="col-12 bg-white rounded p-4 border">
            <DataTable
              title="Laporan Ketersediaan Stok ARV"
              columns={columns}
              data={data}
              isScrollX
              isLoading={isLoading}
            />
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};
