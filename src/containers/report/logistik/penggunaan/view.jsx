import React, { useState, useContext } from "react";

import { DataTable, Filter, DisplayDate } from "components";
// import moment from "moment";
// import { ENUM } from "constant";
import { Select } from "antd";
import { Label } from "reactstrap";
import { sumberDana } from "constant";

// new
import GlobalContext from "containers/GlobalContext";
import { ROLE } from "constant";
// end new

import { formatNumber } from "utils";

const { Option } = Select;

export const Report = ({ data, isLoading, onFilter, currentFilter, onDownloadExcel }) => {
  const user = useContext(GlobalContext);
  const [type, setType] = useState("");
  const [typeDana, setTypeData] = useState("");
  const [filteredData, setFilteredData] = useState([]);

  const _onChangeType = (value, t) => {
    // let ttype = type;
    // let ttypeDana = typeDana;
    if (t === "type") {
      // ttype = value;
      setType(value);
      setTypeData("");
    } else {
      // ttypeDana = value;
      setType("");
      setTypeData(value);
    }

    let filteredData2;

    if (value.length) {
      filteredData2 = data.filter(item => {
        let startsWithCondition;
        // if (ttype.length && ttypeDana.length) {
        //   startsWithCondition =
        //     item.inventory.receiptItem.brand.medicine.medicineType.toLowerCase() === ttype.toLowerCase() &&
        //     item.inventory.fundSource.toLowerCase() === ttypeDana.toLowerCase();
        // } else if (ttype.length) {
        //   startsWithCondition = item.inventory.receiptItem.brand.medicine.medicineType.toLowerCase() === ttype.toLowerCase();
        // } else if (ttypeDana.length) {
        //   startsWithCondition = item.inventory.fundSource.toLowerCase() === ttypeDana.toLowerCase();
        // } else {
          startsWithCondition =
            item.inventory.receiptItem.brand.medicine.medicineType.toLowerCase() === value.toLowerCase() ||
            item.inventory.fundSource.toLowerCase() === value.toLowerCase();
        // }
        // let includesCondition =
        //     item.medicine.medicineType.toLowerCase() === value.toLowerCase()
        if (startsWithCondition) {
          return startsWithCondition
          // } else if (!startsWithCondition && includesCondition) {
          //     return includesCondition
        } else return null
      })
      setFilteredData(filteredData2);
    }
  }

  const columns = [
    {
      title: "No.",
      render: r => type.length ? (filteredData.indexOf(r) + 1) : (data.indexOf(r) + 1)
    },
    {
      title: "Nama Barang",
      dataIndex: "brand",
      render: brand => brand.medicine.name
    },
    {
      title: "Satuan",
      dataIndex: "brand",
      render: brand => brand.medicine.stockUnitType
    },
    {
      title: "Sumber Dana",
      dataIndex: "inventory",
      render: inventory => inventory.fundSource
    },
    {
      title: "No. Batch",
      dataIndex: "inventory",
      render: inventory => inventory.receiptItem.batchCode
    },
    {
      title: "Expired Date",
      dataIndex: "inventory",
      render: r => <DisplayDate date={r.receiptItem.receivedExpiredDate} />
    },
    {
      title: "Stok Awal",
      render: r => formatNumber(r.stockBefore)
    },
    {
      title: "Stok Diterima",
      dataIndex: "inventory",
      render: r => formatNumber(r.receiptItem.receivedPkgQuantity * r.receiptItem.brand.medicine.packageMultiplier)
    },
    {
      title: "Stok Digunakan",
      dataIndex: "usedStockUnit",
      render: r => formatNumber(r)
    },
    {
      title: "Stok Rusak dan Kedaluwarsa",
      render: r => 0
    },
    {
      title: "Stok Selisih",
      render: r => 0
    },
    {
      title: "Stok Akhir",
      render: r => formatNumber(r.stockAfter)
    }
  ];

  let title = "Laporan Penggunaan ";
  if (type) {
    title += type;
  } else {
    title += "ARV & NON_ARV";
  }

  return (
    <React.Fragment>
      <div className="container h-100 pt-4">
        <div className="row mb-2">
          <div className="col-12 bg-white rounded p-4 border">
            <div className="mb-2">
              <Filter
                onFilter={onFilter}
                // disabledDate={current =>
                //   current &&
                //   current.format("YYYY-MM") !== moment().format("YYYY-MM")
                // }
                currentFilter={currentFilter}
              // excel
              // onDownloadExcel={() => onDownloadExcel(type)}
              />
              <div className="row">
                <div className="col-2 pl-2 mt-2">
                  <Label><b>Tipe Obat</b></Label>
                  <Select
                    style={{ width: "100%" }}
                    onChange={(v) => _onChangeType(v, 'type')}
                    value={type}
                  >
                    <Option value="">All</Option>
                    <Option value="ARV">ARV</Option>
                    <Option value="NON_ARV">Non-ARV</Option>
                  </Select>
                </div>
                <div className="col-3 mt-2">
                  <Label><b>Sumber Dana</b></Label>
                  <Select
                    style={{ width: "100%" }}
                    onChange={_onChangeType}
                    value={typeDana}
                  >
                    <Option value="">All</Option>
                    {sumberDana.map(item => (
                      <Option key={item} value={item} title={item}>
                        {item}
                      </Option>
                    ))}
                  </Select>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-12 bg-white rounded p-4 border">
            <DataTable
              title={title}
              columns={columns}
              data={type.length || typeDana.length ? filteredData : data}
              isLoading={isLoading}
            />
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};