import React, { useEffect, useState, useContext } from "react";
import GlobalContext from "containers/GlobalContext";
import API from "utils/API";
import axios from "axios";
import { ENUM } from "constant";

import { useParams } from "react-router";
import { Report } from "./view";
import { BASE_URL } from "utils/request";

const Handler = () => {
  let source = axios.CancelToken.source();
  const user = useContext(GlobalContext);

  const defaultFilter = {
    PROVINCE: [
      ENUM.LOGISTIC_ROLE.PROVINCE_ENTITY,
      ENUM.LOGISTIC_ROLE.SUDIN_ENTITY,
      ENUM.LOGISTIC_ROLE.UPK_ENTITY
    ].includes(user.logisticrole)
      ? user.provinceId
      : "",
    SUDINKAB: [
      ENUM.LOGISTIC_ROLE.SUDIN_ENTITY,
      ENUM.LOGISTIC_ROLE.UPK_ENTITY
    ].includes(user.logisticrole)
      ? user.sudinKabKotaId
      : "",
    UPK: user.logisticrole === ENUM.LOGISTIC_ROLE.UPK_ENTITY ? user.upkId : "",
    MONTH: ""
  };

  const [data, setData] = useState([]);
  const [isLoading, setLoading] = useState(false);
  const { type } = useParams();
  const [filter, setFilter] = useState(defaultFilter);

  const _handleFilter = (type, value) => {
    if (typeof value === "undefined") {
      value = "";
    }
    setFilter({
      ...filter,
      [type]: value
    });
  };

  const _loadData = () => {
    setLoading(true);
    API.Report.logistic
      .pengunaanObat(
        source.token,
        filter.PROVINCE,
        filter.SUDINKAB,
        filter.UPK,
        filter.MONTH
      )
      .then(rsp => {
        setData(rsp.data);
        setLoading(false);
      })
      .catch(e => {
        setLoading(false);
        console.error("e: ", e);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const _handleDownloadExcel = async (type) => {
    try {
      await API.Report.ArvNonArvExcel(
        source.token,
        filter.PROVINCE,
        filter.SUDINKAB,
        filter.UPK,
        filter.MONTH,
        null,
        type
      )
        .then(res => {
          window.open(`${BASE_URL}/${res.data.split('/').slice(2).join('/')}`);
        })
        .catch(e => {
          console.error("e: ", e);
        });
    } catch (ex) {
      console.log(ex)
    }
  }

  useEffect(() => {
    _loadData();

    return () => {
      source.cancel("request canceled");
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [type, filter]);

  return (
    <Report
      data={data}
      isLoading={isLoading}
      onFilter={_handleFilter}
      currentFilter={filter}
      onDownloadExcel={_handleDownloadExcel}
    />
  );
};

export default Handler;
