import React from "react";

import { DataTable } from "components";

export const Report = ({ data, isLoading }) => {
  const columns = [
    {
      title: "Nama Barang",
      dataIndex: "activityDate",
      key: "1"
    },
    {
      title: "Tipe",
      dataIndex: "codeName",
      key: "2"
    },
    {
      title: "Penggunaan Stok",
      dataIndex: "codeName",
      key: "2"
    },
    {
      title: "Prediksi Kebutuhan 1 Tahun Ke Depan",
      dataIndex: "codeName",
      key: "2"
    }
  ];

  return (
    <React.Fragment>
      <div className="container h-100 pt-4">
        <div className="row">
          <div className="col-12 bg-white rounded p-4 border">
            <DataTable
              title="Laporan Perkiraan Kebutuhan Tahunan ARV & non-ARV"
              columns={columns}
              data={data}
              isLoading={isLoading}
            />
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};
