import React, { useEffect, useState } from "react";
// import API from "utils/API";
import axios from "axios";

import { useParams } from "react-router";
import { Report } from "./view";

const Handler = () => {
  let source = axios.CancelToken.source();

  // const [data, setData] = useState([]);
  const [data] = useState([]);
  // const [isLoading, setLoading] = useState(false);
  const [isLoading] = useState(false);
  const { type } = useParams();

  const _loadData = () => {
    // setLoading(true);
    // API.Report.logistic
    //   .pergerakanStok(source.token)
    //   .then(rsp => {
    //     setData(rsp.data || []);
    //   })
    //   .catch(e => {
    //     console.error("e: ", e);
    //   })
    //   .finally(() => {
    //     setLoading(false);
    //   });
  };

  useEffect(() => {
    _loadData();

    return () => {
      source.cancel("request canceled");
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [type]);

  return <Report data={data} isLoading={isLoading} />;
};

export default Handler;
