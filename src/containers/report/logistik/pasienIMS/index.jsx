import React, { useEffect, useState } from "react";
import API from "utils/API";
import axios from "axios";

import { Report } from "./view";

const defaultFilter = {
  PROVINCE: "",
  SUDINKAB: "",
  MONTH: ""
};

const Handler = () => {
  let source = axios.CancelToken.source();

  const [data, setData] = useState([]);
  const [filter, setFilter] = useState(defaultFilter);
  const [isLoading, setLoading] = useState(false);

  const _handleFilter = (type, value) => {
    if (typeof value === "undefined") {
      value = "";
    }
    setFilter({
      ...filter,
      [type]: value
    });
  };

  const _loadData = () => {
    setLoading(true);
    API.Report.logistic
      .jumlahPasienIMS(
        source.token,
        filter.PROVINCE,
        filter.SUDINKAB,
        filter.MONTH
      )
      .then(rsp => {
        setData(rsp.data || []);
      })
      .catch(e => {
        console.error("e: ", e);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  useEffect(() => {
    _loadData();

    return () => {
      source.cancel("request canceled");
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [filter]);

  return (
    <Report
      data={data}
      isLoading={isLoading}
      onFilter={_handleFilter}
      currentFilter={filter}
    />
  );
};

export default Handler;
