import React from "react";

import { DataTable, Filter } from "components";

export const Report = ({ data, isLoading, onFilter, currentFilter }) => {
  const columns = [
    {
      title: "Nama Entitas",
      dataIndex: "upkName",
      key: "1"
    },
    {
      title: "Jumlah Pasien IMS",
      dataIndex: "total",
      key: "2"
    }
  ];

  return (
    <React.Fragment>
      <div className="container h-100 pt-4">
        <div className="mb-2">
          <Filter onFilter={onFilter} currentFilter={currentFilter} />
        </div>
        <div className="row">
          <div className="col-12 bg-white rounded p-4 border">
            <DataTable
              title="Laporan Jumlah Pasien IMS"
              columns={columns}
              data={data}
              rowKey="upkId"
              isLoading={isLoading}
            />
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};
