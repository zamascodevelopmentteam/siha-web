import React, {useState} from "react";
import { formatNumber } from "utils";
import { DataTable, DisplayDate, Filter } from "components";
import { Select } from "antd";
import { Label } from "reactstrap";
import { sumberDana } from "constant";
const { Option } = Select;

export const Report = ({ data, isLoading, onFilter, currentFilter }) => {
  const [type, setType] = useState("");
  const [typeDana, setTypeData] = useState("");
  const [filteredData, setFilteredData] = useState([]);

  const _onChangeType = (value, type) => {
    if (type === "type") {
      setType(value);
      setTypeData("");
    } else {
      setType("");
      setTypeData(value);
    }

    let filteredData2;

    if (value.length) {
      
      filteredData2 = data.filter(item => {
        // console.log(item.inventory);

        let startsWithCondition =
          item.brand.medicine.medicineType.toLowerCase().includes(value.toLowerCase()) ||
          item.inventory.fundSource.toLowerCase().includes(value.toLowerCase())

        if (startsWithCondition) {
          return startsWithCondition
        } else return null
      })
      setFilteredData(filteredData2);
    }
  }

  const columns = [
    {
      title: "No.",
      render: (text, record) => data.indexOf(record) + 1
    },
    {
      title: "Berita Acara",
      dataIndex: "reportCode",
      render: v => v
    },
    {
      title: "Nama Barang",
      dataIndex: "inventory",
      render: inventory => inventory.brand.medicine.codeName
    },
    {
      title: "Nama Merek",
      dataIndex: "inventory",
      render: inventory => inventory.brand.brandName
    },
    {
      title: "Satuan",
      dataIndex: "inventory",
      render: inventory => inventory.brand.medicine.stockUnitType
    },
    {
      title: "No. Batch",
      dataIndex: "inventory",
      render: inventory => inventory.batchCode
    },
    {
      title: "Tgl. Kadaluarsa",
      dataIndex: "inventory",
      render: inventory => <DisplayDate date={inventory.expiredDate} />
    },
    {
      title: "Sumber Dana",
      dataIndex: "inventory",
      render: inventory => inventory.fundSource
    },
    {
      title: "Harga Satuan",
      dataIndex: "inventory",
      render: inventory => formatNumber(parseInt(inventory.packagePrice))
    },
    {
      title: "Nilai Sebelum Penyesuaian",
      dataIndex: "stockQty",
      render: inventory => 0
    },
    {
      title: "Nilai Penyesuaian",
      dataIndex: "stockQty",
    },
    {
      title: "Nilai Sesudah Penyesuaian",
      dataIndex: "stockQty",
      render: inventory => 0
    },
    // {
    //   title: "Jenis Penyesuaian",
    //   dataIndex: "adjustmentType",
    // },
    {
      title: "Alasan Penyesuaian",
      dataIndex: "notes",
    },
    {
      title: "Tgl. Penyesuaian",
      dataIndex: "createdAt",
      render: date => <DisplayDate date={date} />
    },
    {
      title: "Dibuat Oleh",
      dataIndex: "createdByData",
      render: item => item.createdName
    }
  ];

  return (
    <React.Fragment>
      <div className="container h-100 pt-4">
        <div className="mb-2">
          <Filter onFilter={onFilter} currentFilter={currentFilter} />
          <div className="row">
            <div className="col-2 pl-2 mt-2">
              <Label><b>Tipe Obat</b></Label>
              <Select
                style={{ width: "100%" }}
                onChange={(v) => _onChangeType(v, 'type')}
                value={type}
              >
                <Option value="">All</Option>
                <Option value="ARV">ARV</Option>
                <Option value="NON_ARV">Non-ARV</Option>
              </Select>
            </div>
            <div className="col-3 mt-2">
            <Label><b>Sumber Dana</b></Label>
              <Select
                style={{ width: "100%" }}
                onChange={_onChangeType}
                value={typeDana}
              >
                <Option value="">All</Option>
                {sumberDana.map(item => (
                  <Option key={item} value={item} title={item}>
                    {item}
                  </Option>
                ))}
              </Select>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-12 bg-white rounded p-4 border">
            <DataTable
              title="Laporan Stock Adjusment ARV & NONARV"
              columns={columns}
              data={type.length || typeDana.length ? filteredData : data}
              isScrollX
              isLoading={isLoading}
            />
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};
