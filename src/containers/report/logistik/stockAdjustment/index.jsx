import React, { useEffect, useState, useContext } from "react";
import API from "utils/API";
import axios from "axios";
import GlobalContext from "../../../GlobalContext";
import { ENUM } from "constant";
import moment from "moment";

import { Report } from "./view";

// const defaultFilter = {
//   PROVINCE: "",
//   SUDINKAB: "",
//   MONTH: ""
// };

const Handler = () => {
  let source = axios.CancelToken.source();
  const user = useContext(GlobalContext);
  
  const defaultFilter = {
    PROVINCE: [
      ENUM.LOGISTIC_ROLE.PROVINCE_ENTITY,
      ENUM.LOGISTIC_ROLE.SUDIN_ENTITY,
      ENUM.LOGISTIC_ROLE.UPK_ENTITY
    ].includes(user.logisticrole)
      ? user.provinceId
      : "",
    SUDINKAB: [
      ENUM.LOGISTIC_ROLE.SUDIN_ENTITY,
      ENUM.LOGISTIC_ROLE.UPK_ENTITY
    ].includes(user.logisticrole)
      ? user.sudinKabKotaId
      : "",
    UPK: undefined,
    MONTH: moment().format('YYYY-MM')
  };

  const [data, setData] = useState([]);
  const [filter, setFilter] = useState(defaultFilter);
  const [isLoading, setLoading] = useState(false);

  const _handleFilter = (type, value) => {
    if (typeof value === "undefined") {
      value = "";
    }
    setFilter({
      ...filter,
      [type]: value
    });
  };

  const _loadData = () => {
    setLoading(true);
    API.Logistic.listAdjustmentHistory(source.token, 0, filter)
    // API.Report.logistic
    //   .adjustment(source.token, filter.PROVINCE, filter.SUDINKAB, filter.MONTH)
      .then(rsp => {
        setData(rsp.data || []);
      })
      .catch(e => {
        console.error("e: ", e);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  useEffect(() => {
    _loadData();

    return () => {
      source.cancel("request canceled");
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [filter]);

  return (
    <Report
      data={data}
      isLoading={isLoading}
      onFilter={_handleFilter}
      currentFilter={filter}
    />
  );
};

export default Handler;
