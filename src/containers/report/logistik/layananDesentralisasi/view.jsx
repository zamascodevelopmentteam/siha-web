import React from "react";

import { DataTable } from "components";

export const Report = ({ data, isLoading }) => {
  const columns = [
    {
      title: "Layanan Desentralisasi",
      dataIndex: "upkName",
      key: "upkName"
    },
    {
      title: "Tujuan Desentralisasi",
      dataIndex: "sudinKabKota",
      key: "kota",
      render: sudinKabKota => sudinKabKota.province.provinceName
    }
  ];

  return (
    <React.Fragment>
      <div className="container h-100 pt-4">
        <div className="row">
          <div className="col-12 bg-white rounded p-4 border">
            <DataTable
              title="Layanan Desantralisasi"
              columns={columns}
              data={data}
              isLoading={isLoading}
            />
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};
