import React from "react";

import { DataTable, DisplayDate, Filter } from "components";
import { formatNumber } from "utils";

export const Report = ({
  data,
  isLoading,
  onFilter,
  currentFilter,
  chartData
}) => {
  const columns = [
    {
      title: "Bulan",
      dataIndex: "activityDate",
      key: "activityDate",
      render: date => <DisplayDate format="MMMM YYYY" date={date} />
    },
    {
      title: "Nama Barang",
      dataIndex: "codeName",
      key: "codeName"
    },
    {
      title: "Stok Akhir",
      key: "stockTotal",
      render: item => `${formatNumber(item.stockTotal)} ${item.prevStockUnit}`
    }
  ];

  return (
    <React.Fragment>
      <div className="container h-100 pt-4">
        <div className="mb-2">
          <Filter
            onFilter={onFilter}
            currentFilter={currentFilter}
            hideLocation
          />
        </div>
        <div className="row">
          <div className="col-12 bg-white rounded p-4 border">
            <DataTable
              title="Laporan Pergerakan Stok"
              columns={columns}
              data={data}
              rowKey="medicineId"
              isLoading={isLoading}
            />
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};
