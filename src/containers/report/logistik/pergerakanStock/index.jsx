import React, { useEffect, useState } from "react";
import API from "utils/API";
import axios from "axios";

import { Report } from "./view";

const defaultFilter = {
  RANGE: ["", ""]
};

const Handler = () => {
  let source = axios.CancelToken.source();

  const [data, setData] = useState([]);
  const [chartData, setChartData] = useState([]);
  const [filter, setFilter] = useState(defaultFilter);
  const [isLoading, setLoading] = useState(false);

  const _handleFilter = (type, value) => {
    if (typeof value === "undefined") {
      value = "";
    }
    setFilter({
      ...filter,
      [type]: value
    });
  };

  const _loadData = () => {
    setLoading(true);
    API.Report.logistic
      .pergerakanStok(source.token, filter.RANGE[0], filter.RANGE[1])
      .then(rsp => {
        setData(rsp.data || []);
        setChartData(_generateChartData(rsp.data || []));
      })
      .catch(e => {
        console.error("e: ", e);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  useEffect(() => {
    _loadData();

    return () => {
      source.cancel("request canceled");
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [filter]);

  return (
    <Report
      data={data}
      chartData={chartData}
      isLoading={isLoading}
      onFilter={_handleFilter}
      currentFilter={filter}
    />
  );
};

export default Handler;

const _generateChartData = data => {
  let chartData = [];
  for (let x = 0; x < data.length; x++) {
    const { codeName, stockTotal } = data[x];
    chartData.push({
      x: codeName,
      y: stockTotal
    });
  }

  return chartData;
};
