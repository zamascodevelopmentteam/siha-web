import React from "react";
import { Spin } from "antd";
import { Table } from "reactstrap";
import { Typography, Filter } from "components";
import './style.css';

const lakiDefault = [];
const perempuanDefault = [];
for (let i = 1; i <= 13; i++) {
  lakiDefault.push(i);
}
for (let i = 14; i <= 26; i++) {
  perempuanDefault.push(i);
}

const View = ({ data, isLoading, onFilter, currentFilter, onDownloadExcel }) => {
  return (
    <React.Fragment>
      <div className="container h-100 pt-4">
        <div className="row mb-2">
          <div className="col-12 bg-white rounded p-4 border">
            <div className="mb-2">
              <Filter
                onFilter={onFilter}
                disableNextMonth={true}
                currentFilter={currentFilter}
                // excel
                // onDownloadExcel={onDownloadExcel}
              />
            </div>
          </div>
        </div>

        <div className="row">
          <div className="col-12 bg-white rounded p-4 border mb-5">
            {isLoading && (
              <div className="text-center">
                <Spin />
              </div>
            )}
            <Typography
              fontSize="15px"
              fontWeight="bold"
              className="d-block mb-3 text-center"
            >
              LAPORAN BULANAN
              <br />
              INFEKSI MENULAR SEKSUAL
            </Typography>
            <Table bordered responsive style={{ fontSize: 12 }}>
              <thead>
                <tr>
                  <th className={'bg-light text-center align-middle sticky-td'} rowSpan="2">
                    <span className="px-5 mx-5">Indikator</span>
                  </th>
                  <th className={'bg-light text-center align-middle'} colSpan="13">Laki-laki</th>
                  <th className={'bg-light text-center align-middle'} colSpan="13">Perempuan</th>
                  <th className={'bg-light text-center align-middle'} rowSpan="2">Total</th>
                  <th className={'bg-light text-center align-middle'} colSpan="13">Kelompok Populasi</th>
                </tr>
                <tr>
                  <th className={'bg-light text-center align-middle'}>{'< 1'}</th>
                  <th className={'bg-light text-center align-middle'}>1-4</th>
                  <th className={'bg-light text-center align-middle'}>5-14</th>
                  <th className={'bg-light text-center align-middle'}>15-19</th>
                  <th className={'bg-light text-center align-middle'}>20-24</th>
                  <th className={'bg-light text-center align-middle'}>25-29</th>
                  <th className={'bg-light text-center align-middle'}>30-34</th>
                  <th className={'bg-light text-center align-middle'}>35-39</th>
                  <th className={'bg-light text-center align-middle'}>40-44</th>
                  <th className={'bg-light text-center align-middle'}>45-49</th>
                  <th className={'bg-light text-center align-middle'}>50-59</th>
                  <th className={'bg-light text-center align-middle'}>{">="} 60</th>
                  <th className={'bg-light text-center align-middle'}>Jumlah</th>

                  <th className={'bg-light text-center align-middle'}>{'< 1'}</th>
                  <th className={'bg-light text-center align-middle'}>1-4</th>
                  <th className={'bg-light text-center align-middle'}>5-14</th>
                  <th className={'bg-light text-center align-middle'}>15-19</th>
                  <th className={'bg-light text-center align-middle'}>20-24</th>
                  <th className={'bg-light text-center align-middle'}>25-29</th>
                  <th className={'bg-light text-center align-middle'}>30-34</th>
                  <th className={'bg-light text-center align-middle'}>35-39</th>
                  <th className={'bg-light text-center align-middle'}>40-44</th>
                  <th className={'bg-light text-center align-middle'}>45-49</th>
                  <th className={'bg-light text-center align-middle'}>50-59</th>
                  <th className={'bg-light text-center align-middle'}>{">="} 60</th>
                  <th className={'bg-light text-center align-middle'}>Jumlah</th>

                  <th className={'bg-light text-center align-middle'}>WPS</th>
                  <th className={'bg-light text-center align-middle'}>LSL</th>
                  <th className={'bg-light text-center align-middle'}>Waria</th>
                  <th className={'bg-light text-center align-middle'}>Penasun</th>
                  <th className={'bg-light text-center align-middle'}>Ibu Hamil</th>
                  <th className={'bg-light text-center align-middle'}>ODHA</th>
                  <th className={'bg-light text-center align-middle'}>Pasien Hepatitis</th>
                  <th className={'bg-light text-center align-middle'}>Pasangan ODHA</th>
                  <th className={'bg-light text-center align-middle'}>Anak ODHA</th>
                  <th className={'bg-light text-center align-middle'}>WBP</th>
                  <th className={'bg-light text-center align-middle'}>Pupulasi Umum</th>
                </tr>
              </thead>
              <tbody>
                {data.map((v, i) => {
                  let hide = [];
                  return (
                    <tr key={i}>
                      {
                        v.length ?
                          v.map((k, ii) => {
                            if (["Duh Tubuh Vagina", "Servisitis gonore", "Servisitis non gonore", "Vaginitis gonore", "Vaginitis non gonore", "Penyakit Radang Panggul", "Vaginosis bakterial", "Kandidiasis", "Jumlah pasien Sifilis yang diperiksa IVA Test"].includes(k)) {
                              hide = [...lakiDefault, ...hide];
                            }
                            if (["Pembengkakan Skortum"].includes(k)) {
                              hide = [...perempuanDefault, ...hide];
                            }
                            if (["Duh Tubuh Vagina", "Penyakit Radang Panggul", "Servisitis gonore", "Vaginitis gonore", "Servisitis non gonore", "Vaginitis non gonore", "Trikomoniasis", "Vaginosis bakterial", "Kandidiasis", "Jumlah pasien Sifilis yang diperiksa IVA Test"].includes(k)) {
                              hide = [29, 30, ...hide];
                            }
                            if (["Duh Tubuh Uretra", "Duh Tubuh Anus", "Uritritis gonore", "Proktitis gonore"].includes(k)) {
                              hide = [28, 32, ...hide];
                            }
                            if (["Pembengkakan Skortum", "Uritritis non gonore", "Proktitis non gonore"].includes(k)) {
                              hide = [28, ...hide];
                            }
                            if (["Servisitis non gonore", "Vaginitis non gonore"].includes(k)) {
                              hide = [32, ...hide];
                            }

                            return (
                              <React.Fragment>
                                <td
                                  className={
                                    (
                                      ii === 0 ?
                                        ' sticky-td bg-light ' + (
                                          v.length === 1 ?
                                            ' text-primary font-weight-bold h6 ' :
                                            ''
                                        ) :
                                        ''
                                    ) + (
                                      hide.includes(ii) ? ' bg-light ' : ''
                                    )
                                  } key={ii}
                                >
                                  {hide.includes(ii) ? '' : (ii == 13 || ii == 26 || ii == 27 ? <b>{k}</b> : k)}
                                </td>
                                {v.length === 1 && (
                                  <td colSpan={38}></td>
                                )}
                              </React.Fragment>
                            )
                          })
                          :
                          null
                      }
                    </tr>
                  )
                })}
              </tbody>
            </Table>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default View;
