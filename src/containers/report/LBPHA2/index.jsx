import React, { useEffect, useState, useContext } from "react";
import GlobalContext from "containers/GlobalContext";
import API from "utils/API";
import axios from "axios";
import { ENUM } from "constant";
import { openNotification } from "utils/Notification";

import { LBPHA2 } from "./view";
import { BASE_URL } from "utils/request";

const emptyFCD = {
  content: [],
  footer: {
    jumlahTabDiperlukan: {
      pasienReguler: {
        anak: 0,
        jumlahTab: 0
      },
      pasienTransit: {
        anak: 0,
        jumlahTab: 0
      },
      pasienMultiMonth: {
        anak: 0,
        jumlahTab: 0
      }
    },
    jumlahTabDiperlukanBuffer: {
      pasienReguler: {
        anak: 0,
        jumlahTab: 0
      },
      pasienTransit: {
        anak: 0,
        jumlahTab: 0
      },
      pasienMultiMonth: {
        anak: 0,
        jumlahTab: 0
      }
    }
  }
}

const emptyState = {
  rejimenStandarTable: {
    content: {},
    footer: {
      totalPasienRejimenStandar: {
        pasienReguler: {
          dewasa: 0,
          anak: 0
        },
        pasienTransit: {
          dewasa: 0,
          anak: 0
        },
        pasienMultiMonth: {
          dewasa: 0,
          anak: 0
        }
      },
      totalPasien: {
        pasienReguler: {
          dewasa: 0,
          anak: 0
        },
        pasienTransit: {
          dewasa: 0,
          anak: 0
        },
        pasienMultiMonth: {
          dewasa: 0,
          anak: 0
        }
      }
    }
  },
  rejimenLainTable: {
    content: {
      title: "Rejimen Lain",
      regimentList: []
    },
    footer: {
      totalPasienRejimenLain: {
        pasienReguler: {
          dewasa: 0,
          anak: 0
        },
        pasienTransit: {
          dewasa: 0,
          anak: 0
        },
        pasienMultiMonth: {
          dewasa: 0,
          anak: 0
        }
      }
    }
  },
  FDCTable: emptyFCD,
  FDCABCTable: emptyFCD,
  FDCLPVTable: emptyFCD,
  inventoryTable: {
    content: [],
    footer: {}
  }
};

const Handler = () => {
  let source = axios.CancelToken.source();
  const user = useContext(GlobalContext);

  const defaultFilter = {
    PROVINCE: [
      ENUM.LOGISTIC_ROLE.PROVINCE_ENTITY,
      ENUM.LOGISTIC_ROLE.SUDIN_ENTITY,
      ENUM.LOGISTIC_ROLE.UPK_ENTITY
    ].includes(user.logisticrole)
      ? user.provinceId
      : "",
    SUDINKAB: [
      ENUM.LOGISTIC_ROLE.SUDIN_ENTITY,
      ENUM.LOGISTIC_ROLE.UPK_ENTITY
    ].includes(user.logisticrole)
      ? user.sudinKabKotaId
      : "",
    UPK: user.logisticrole === ENUM.LOGISTIC_ROLE.UPK_ENTITY ? user.upkId : "",
    MONTH: ""
  };

  const [data, setData] = useState(emptyState);
  const [isLoading, setLoading] = useState(false);

  const [filter, setFilter] = useState(defaultFilter);

  const _handleFilter = (type, value) => {
    if (typeof value === "undefined") {
      value = "";
    }
    setFilter({
      ...filter,
      [type]: value
    });
  };

  const _loadData = () => {
    setLoading(true);
    API.Report.lbpha2(
      source.token,
      filter.PROVINCE,
      filter.SUDINKAB,
      filter.UPK,
      filter.MONTH,
      true,
      true
    )
      .then(rsp => {
        setData(rsp || emptyState);
        setLoading(false);
      })
      .catch(e => {
        setLoading(false);
        console.error("e: ", e);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const _createRequestReguler = () => {
    setLoading(true);
    API.Logistic.requestOrderRequler(source.token)
      .then(rsp => {
        setLoading(false);
        openNotification("success", "Berhasil", rsp.message);
      })
      .catch(e => {
        openNotification("error", "Gagal", e.message || String(e.data ? e.data.message : "").replace(/_/g, " "));
        console.error("e: ", e);
        setLoading(false);
      });
  };

  const _handleDownloadExcel = async () => {
    setLoading(true);
    const meta = data.meta;
    await API.Report.lbpha2Excel(
      source.token,
      filter.PROVINCE,
      filter.SUDINKAB,
      filter.UPK,
      filter.MONTH,
      meta,
      true,
      true
    )
      .then(res => {
        window.open(`${BASE_URL}/${res.data.split('/').slice(2).join('/')}`);
        setLoading(false);
      })
      .catch(e => {
        setLoading(false);
        console.error("e: ", e);
      })
      .finally(() => {
        setLoading(false);
      });
  }

  useEffect(() => {
    _loadData();

    return () => {
      source.cancel("request canceled");
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [filter]);

  return (
    <LBPHA2
      data={data}
      isLoading={isLoading}
      onFilter={_handleFilter}
      currentFilter={filter}
      handleCreateReguler={_createRequestReguler}
      // rejimenStandar={data.rejimenStandarTable}
      // rejimenLain={data.rejimenLainTable}
      onDownloadExcel={_handleDownloadExcel}
    />
  );
};

export default Handler;