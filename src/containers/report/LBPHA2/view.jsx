import React from "react";
// import moment from "moment";

import { Spin } from "antd";
import { Table } from "reactstrap";
import { Typography, Filter } from "components";
import { formatNumber } from "utils";

export const LBPHA2 = ({
  data,
  isLoading,
  onFilter,
  currentFilter,
  handleCreateReguler,
  onDownloadExcel
}) => {
  const
    meta = data.meta,
    rejimenStandar = data.rejimenStandarTable,
    rejimenLain = data.rejimenLainTable,
    FDC = data.FDCTable,
    FDCABC = data.FDCABCTable,
    FDCLPV = data.FDCLPVTable,
    inventory = data.inventoryTable;

  const monthNames = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Augustus", "September", "Oktober", "November", "Desember"];
  let dateObj = new Date();
  let month = monthNames[dateObj.getMonth()];
  let day = String(dateObj.getDate()).padStart(2, '0');
  let year = dateObj.getFullYear();
  let output = day + ' ' + month + ' ' + year;

  let month2 = null, year2 = null;
  if (currentFilter && currentFilter.MONTH !== "") {
    month2 = parseInt(currentFilter.MONTH.split('-')[1]) - 1;
    year2 = currentFilter.MONTH.split('-')[0];
  }

  let no = 1;

  return (
    <React.Fragment>
      <div className="container h-100 pt-4">
        <div className="row mb-2">
          <div className="col-12 bg-white rounded p-4 border">
            <div className="col-12 p-0">
              <Filter
                onFilter={onFilter}
                // disabledDate={current =>
                //   current &&
                //   current.format("YYYY-MM") !== moment().format("YYYY-MM")
                // }
                currentFilter={currentFilter}
                excel
                onDownloadExcel={onDownloadExcel}
              />
            </div>
            {/* <div className="col-12 pt-3 p-0 m-0 text-center">
              <Button
                className="hs-btn-outline PHARMA_STAFF"
                onClick={() => handleCreateReguler()}
              >
                Tambah Permintaan Reguler
              </Button>
            </div> */}
          </div>
        </div>
        <div
          className="row bg-white rounded p-1 border mb-1"
          style={{ fontSize: 12 }}
        >
          <div className="col-12">
            {isLoading && (
              <div className="text-center">
                <Spin />
              </div>
            )}
          </div>
          <div className="col-6 ">
            <dl className="dl-horizontal">
              <dt>Nama UPK</dt>
              <dd>{meta && meta.upk.name ? meta.upk.name : "-"}</dd>
              <dt>Nama Kab/Kota</dt>
              <dd>
                {meta && meta.sudinKabKota.name ? meta.sudinKabKota.name : "-"}
              </dd>
              <dt>Nama Provinsi</dt>
              <dd>{meta && meta.province.name ? meta.province.name : "-"}</dd>
            </dl>
          </div>
          <div className="col-6">
            <dl className="dl-horizontal">
              <dt>Bulan</dt>
              <dd>{month2 !== null ? monthNames[month2] : (meta && meta.bulan ? meta.bulan : "-")}</dd>
              <dt>Tahun</dt>
              <dd>{year2 !== null ? year2 : (meta && meta.tahun ? meta.tahun : "-")}</dd>
              <dt>Tanggal Akses</dt>
              {/* <dd>{meta && meta.tanggal ? meta.tanggal : "-"}</dd> */}
              <dd>{output}</dd>
            </dl>
          </div>
        </div>
        <div className="row">
          <div className="col-12 bg-white rounded p-4 border mb-5">
            <Typography
              fontSize="15px"
              fontWeight="bold"
              className="d-block mb-3 text-center"
            >
              Rejimen Standar
            </Typography>
            <Table bordered responsive striped style={{ fontSize: 12 }}>
              <thead>
                <tr>
                  <th rowSpan="2">No</th>
                  <th rowSpan="2">Rejimen Standar</th>
                  <th colSpan="2">Pasien Reguler</th>
                  <th colSpan="2">Pasien Transit</th>
                  <th colSpan="2">
                    Pasien Reguler yang Mengambil Obat {'>'} 1 bulan
                  </th>
                </tr>
                <tr>
                  <th>Dewasa</th>
                  <th>Anak</th>
                  <th>Dewasa</th>
                  <th>Anak</th>
                  <th>Dewasa</th>
                  <th>Anak</th>
                </tr>
              </thead>
              <tbody>
                {!isLoading &&
                  Object.keys(rejimenStandar.content).map(keyContent => (
                    <React.Fragment key={keyContent}>
                      <tr>
                        <th colSpan="8">
                          {rejimenStandar.content[keyContent].title}
                        </th>
                      </tr>
                      {rejimenStandar.content[keyContent].regimentList.map(
                        (value, index) => (
                          <tr key={index}>
                            <td>{index + 1}</td>
                            <td>{value.regiment.combinationStrInfo}</td>
                            {Object.keys(value.data).map(key => (
                              <React.Fragment key={key}>
                                {Object.keys(value.data[key]).map(keyData => (
                                  <td key={"c" + index + keyData}>{value.data[key][keyData]}</td>
                                ))}
                              </React.Fragment>
                            ))}
                          </tr>
                        )
                      )}
                      {rejimenStandar.content[keyContent].regimentList
                        .length === 0 && (
                          <tr>
                            <th colSpan="8" className="text-center">
                              Data Kosong
                          </th>
                          </tr>
                        )}
                    </React.Fragment>
                  ))}
                {isLoading && (
                  <tr>
                    <td colSpan="8" className="text-center">
                      Loading...
                    </td>
                  </tr>
                )}
                <tr>
                  <th colSpan="2">Total Pasien Rejimen Standar</th>
                  {Object.keys(
                    rejimenStandar.footer.totalPasienRejimenStandar
                  ).map(key => (
                    <React.Fragment key={key}>
                      {Object.keys(
                        rejimenStandar.footer.totalPasienRejimenStandar[key]
                      ).map(keyPasien => (
                        <td key={keyPasien}>
                          {
                            rejimenStandar.footer.totalPasienRejimenStandar[key][keyPasien]
                          }
                        </td>
                      ))}
                    </React.Fragment>
                  ))}
                </tr>
                <tr>
                  <th colSpan="2">Total Pasien</th>
                  {Object.keys(rejimenStandar.footer.totalPasien).map(key => (
                    <React.Fragment key={key}>
                      {Object.keys(rejimenStandar.footer.totalPasien[key]).map(
                        keyPasien => (
                          <td key={keyPasien}>
                            {rejimenStandar.footer.totalPasien[key][keyPasien]}
                          </td>
                        )
                      )}
                    </React.Fragment>
                  ))}
                </tr>
              </tbody>
            </Table>
            <Typography
              fontSize="15px"
              fontWeight="bold"
              className="d-block my-3 text-center"
            >
              Rejimen Non Standar
            </Typography>
            <Table bordered responsive striped style={{ fontSize: 12 }}>
              <thead>
                <tr>
                  <th rowSpan="2">No</th>
                  <th rowSpan="2">Rejimen Non Standar</th>
                  <th colSpan="2">Pasien Reguler</th>
                  <th colSpan="2">Pasien Transit</th>
                  <th colSpan="2">
                    Pasien Reguler yang Mengambil Obat {'>'} 1 bulan
                  </th>
                </tr>
                <tr>
                  <th>Dewasa</th>
                  <th>Anak</th>
                  <th>Dewasa</th>
                  <th>Anak</th>
                  <th>Dewasa</th>
                  <th>Anak</th>
                </tr>
              </thead>
              <tbody>
                {!isLoading &&
                  rejimenLain.content.regimentList.map((value, index) => (
                    <tr key={index}>
                      <td>{index + 1}</td>
                      <td>{value.regiment.combinationStrInfo}</td>
                      {Object.keys(value.data).map(key => (
                        <React.Fragment key={key}>
                          {Object.keys(value.data[key]).map(keyData => (
                            <td key={"c" + index + keyData}>{value.data[key][keyData]}</td>
                          ))}
                        </React.Fragment>
                      ))}
                    </tr>
                  ))}
                {!isLoading && rejimenLain.content.regimentList.length === 0 && (
                  <tr>
                    <td colSpan="8" className="text-center">
                      Data Kosong
                    </td>
                  </tr>
                )}
                {isLoading && (
                  <tr>
                    <td colSpan="8" className="text-center">
                      Loading...
                    </td>
                  </tr>
                )}

                <tr>
                  <th colSpan="2">Total Pasien Rejimen Non Standar</th>
                  {Object.keys(rejimenLain.footer.totalPasienRejimenLain).map(
                    key => (
                      <React.Fragment key={key}>
                        {Object.keys(
                          rejimenLain.footer.totalPasienRejimenLain[key]
                        ).map(keyPasien => (
                          <td key={keyPasien}>
                            {
                              rejimenLain.footer.totalPasienRejimenLain[key][keyPasien]
                            }
                          </td>
                        ))}
                      </React.Fragment>
                    )
                  )}
                </tr>
              </tbody>
            </Table>

            <Typography
              fontSize="15px"
              fontWeight="bold"
              className="d-block my-3 text-center"
            >
              FDC
            </Typography>
            <Table bordered responsive striped style={{ fontSize: 12 }}>
              <thead>
                <tr>
                  <th>Ketarangan Penggunaan FDC Junior</th>
                  <th colSpan="10">ZDV/3TC/NVP Triple FDC Junior (60/30/50)</th>
                </tr>
                <tr>
                  <th rowSpan="2">Berat Badan (kg)</th>
                  <th colSpan="3">Pasien Reguler</th>
                  <th colSpan="3">Pasien Transit</th>
                  <th colSpan="3">Pasien yang Mengambil Obat {'>'}1 bulan</th>
                </tr>
                <tr>
                  <th>Jumah pasien Anak</th>
                  <th>Tablet yang diperlukan/hari</th>
                  <th>Tablet yang diperlukan/bln</th>
                  <th>Jumah pasien Anak</th>
                  <th>Tablet yang diperlukan/hari</th>
                  <th>Tablet yang diperlukan/bln</th>
                  <th>Jumah pasien Anak</th>
                  <th>Tablet yang diperlukan/hari</th>
                  <th>Tablet yang diperlukan/bln</th>
                </tr>
              </thead>
              <tbody>
                {!isLoading &&
                  FDC.content.map((content, idx) => (
                    <tr key={idx}>
                      <td>{content.title}</td>
                      {Object.keys(content.data).map(keyData =>
                        Object.keys(content.data[keyData]).map(keyDetail => (
                          <React.Fragment>
                            <td key={keyData + keyDetail}>
                              {content.data[keyData][keyDetail]}
                            </td>
                            {keyDetail === "tablet_per_hari" && (
                              <td key={keyData + keyDetail + '_'}>
                                {content.data[keyData]['anak'] * content.data[keyData]['tablet_per_hari'] * 30}
                              </td>
                            )}
                          </React.Fragment>
                        ))
                      )}
                    </tr>
                  ))}
                {!isLoading && FDC.content.length === 0 && (
                  <tr>
                    <td colSpan="7" className="text-center">
                      Data Kosong
                    </td>
                  </tr>
                )}
                {isLoading && (
                  <tr>
                    <td colSpan="7" className="text-center">
                      Loading...
                    </td>
                  </tr>
                )}
                <tr>
                  <td>Jumlah tab yang diperlukan</td>
                  {Object.keys(FDC.footer.jumlahTabDiperlukan).map(key =>
                    Object.keys(
                      FDC.footer.jumlahTabDiperlukan[key]
                    ).map(keyDetail => (
                      <React.Fragment>
                        <td key={key + keyDetail}>
                          {/* {FDC.footer.jumlahTabDiperlukan[key][keyDetail]} */}
                        </td>
                        {keyDetail === "jumlahTab" && (
                          <td key={key + keyDetail}>
                            {FDC.footer.jumlahTabDiperlukan[key][keyDetail] * 30}
                          </td>
                        )}
                      </React.Fragment>
                    ))
                  )}
                </tr>
                <tr>
                  <td>Jumlah tab yang diperlukan + buffer 2 bulan</td>
                  {Object.keys(FDC.footer.jumlahTabDiperlukanBuffer).map(key =>
                    Object.keys(
                      FDC.footer.jumlahTabDiperlukanBuffer[key]
                    ).map(keyDetail => (
                      <React.Fragment>
                        <td key={key + keyDetail}>
                          {/* {FDC.footer.jumlahTabDiperlukanBuffer[key][keyDetail]} */}
                        </td>
                        {keyDetail === "jumlahTab" && (
                          <td key={key + keyDetail}>
                            {FDC.footer.jumlahTabDiperlukan[key][keyDetail] * 30 * 3}
                          </td>
                        )}
                      </React.Fragment>

                    ))
                  )}
                </tr>
              </tbody>
            </Table>

            <Table bordered responsive striped style={{ fontSize: 12 }}>
              <thead>
                <tr>
                  <th>Ketarangan Penggunaan FDC Junior</th>
                  <th colSpan="10">ABC(120)/3TC(60)</th>
                </tr>
                <tr>
                  <th rowSpan="2">Berat Badan (kg)</th>
                  <th colSpan="3">Pasien Reguler</th>
                  <th colSpan="3">Pasien Transit</th>
                  <th colSpan="3">Pasien yang Mengambil Obat {'>'}1 bulan</th>
                </tr>
                <tr>
                  <th>Jumah pasien Anak</th>
                  <th>Tablet yang diperlukan/hari</th>
                  <th>Tablet yang diperlukan/bln</th>
                  <th>Jumah pasien Anak</th>
                  <th>Tablet yang diperlukan/hari</th>
                  <th>Tablet yang diperlukan/bln</th>
                  <th>Jumah pasien Anak</th>
                  <th>Tablet yang diperlukan/hari</th>
                  <th>Tablet yang diperlukan/bln</th>
                </tr>
              </thead>
              <tbody>
                {!isLoading &&
                  FDCABC.content.map((content, idx) => (
                    <tr key={idx}>
                      <td>{content.title}</td>
                      {Object.keys(content.data).map(keyData =>
                        Object.keys(content.data[keyData]).map(keyDetail => (
                          <React.Fragment>
                            <td key={keyData + keyDetail}>
                              {content.data[keyData][keyDetail]}
                            </td>
                            {keyDetail === "tablet_per_hari" && (
                              <td key={keyData + keyDetail + '_'}>
                                {content.data[keyData]['anak'] * content.data[keyData]['tablet_per_hari'] * 30}
                              </td>
                            )}
                          </React.Fragment>
                        ))
                      )}
                    </tr>
                  ))}
                {!isLoading && FDCABC.content.length === 0 && (
                  <tr>
                    <td colSpan="7" className="text-center">
                      Data Kosong
                    </td>
                  </tr>
                )}
                {isLoading && (
                  <tr>
                    <td colSpan="7" className="text-center">
                      Loading...
                    </td>
                  </tr>
                )}
                <tr>
                  <td>Jumlah tab yang diperlukan</td>
                  {Object.keys(FDCABC.footer.jumlahTabDiperlukan).map(key =>
                    Object.keys(
                      FDCABC.footer.jumlahTabDiperlukan[key]
                    ).map(keyDetail => (
                      <React.Fragment>
                        <td key={key + keyDetail}>
                          {/* {FDCABC.footer.jumlahTabDiperlukan[key][keyDetail]} */}
                        </td>
                        {keyDetail === "jumlahTab" && (
                          <td key={key + keyDetail}>
                            {FDCABC.footer.jumlahTabDiperlukan[key][keyDetail] * 30}
                          </td>
                        )}
                      </React.Fragment>
                    ))
                  )}
                </tr>
                <tr>
                  <td>Jumlah tab yang diperlukan + buffer 2 bulan</td>
                  {Object.keys(FDCABC.footer.jumlahTabDiperlukanBuffer).map(key =>
                    Object.keys(
                      FDCABC.footer.jumlahTabDiperlukanBuffer[key]
                    ).map(keyDetail => (
                      <React.Fragment>
                        <td key={key + keyDetail}>
                          {/* {FDCABC.footer.jumlahTabDiperlukanBuffer[key][keyDetail]} */}
                        </td>
                        {keyDetail === "jumlahTab" && (
                          <td key={key + keyDetail}>
                            {FDCABC.footer.jumlahTabDiperlukan[key][keyDetail] * 30 * 3}
                          </td>
                        )}
                      </React.Fragment>

                    ))
                  )}
                </tr>
              </tbody>
            </Table>

            <Table bordered responsive striped style={{ fontSize: 12 }}>
              <thead>
                <tr>
                  <th>Ketarangan Penggunaan FDC Junior</th>
                  <th colSpan="10">LPV(40)/r(10)</th>
                </tr>
                <tr>
                  <th rowSpan="2">Berat Badan (kg)</th>
                  <th colSpan="3">Pasien Reguler</th>
                  <th colSpan="3">Pasien Transit</th>
                  <th colSpan="3">Pasien yang Mengambil Obat {'>'}1 bulan</th>
                </tr>
                <tr>
                  <th>Jumah pasien Anak</th>
                  <th>Sachet yang diperlukan/hari</th>
                  <th>Tablet yang diperlukan/bln</th>
                  <th>Jumah pasien Anak</th>
                  <th>Sachet yang diperlukan/hari</th>
                  <th>Tablet yang diperlukan/bln</th>
                  <th>Jumah pasien Anak</th>
                  <th>Sachet yang diperlukan/hari</th>
                  <th>Tablet yang diperlukan/bln</th>
                </tr>
              </thead>
              <tbody>
                {!isLoading &&
                  FDCLPV.content.map((content, idx) => (
                    <tr key={idx}>
                      <td>{content.title}</td>
                      {Object.keys(content.data).map(keyData =>
                        Object.keys(content.data[keyData]).map(keyDetail => (
                          <React.Fragment>
                            <td key={keyData + keyDetail}>
                              {content.data[keyData][keyDetail]}
                            </td>
                            {keyDetail === "tablet_per_hari" && (
                              <td key={keyData + keyDetail + '_'}>
                                {content.data[keyData]['anak'] * content.data[keyData]['tablet_per_hari'] * 30}
                              </td>
                            )}
                          </React.Fragment>
                        ))
                      )}
                    </tr>
                  ))}
                {!isLoading && FDCLPV.content.length === 0 && (
                  <tr>
                    <td colSpan="7" className="text-center">
                      Data Kosong
                    </td>
                  </tr>
                )}
                {isLoading && (
                  <tr>
                    <td colSpan="7" className="text-center">
                      Loading...
                    </td>
                  </tr>
                )}
                <tr>
                  <td>Jumlah tab yang diperlukan</td>
                  {Object.keys(FDCLPV.footer.jumlahTabDiperlukan).map(key =>
                    Object.keys(
                      FDCLPV.footer.jumlahTabDiperlukan[key]
                    ).map(keyDetail => (
                      <React.Fragment>
                        <td key={key + keyDetail}>
                          {/* {FDCLPV.footer.jumlahTabDiperlukan[key][keyDetail]} */}
                        </td>
                        {keyDetail === "jumlahTab" && (
                          <td key={key + keyDetail}>
                            {FDCLPV.footer.jumlahTabDiperlukan[key][keyDetail] * 30}
                          </td>
                        )}
                      </React.Fragment>
                    ))
                  )}
                </tr>
                <tr>
                  <td>Jumlah tab yang diperlukan + buffer 2 bulan</td>
                  {Object.keys(FDCLPV.footer.jumlahTabDiperlukanBuffer).map(key =>
                    Object.keys(
                      FDCLPV.footer.jumlahTabDiperlukanBuffer[key]
                    ).map(keyDetail => (
                      <React.Fragment>
                        <td key={key + keyDetail}>
                          {/* {FDCLPV.footer.jumlahTabDiperlukanBuffer[key][keyDetail]} */}
                        </td>
                        {keyDetail === "jumlahTab" && (
                          <td key={key + keyDetail}>
                            {FDCLPV.footer.jumlahTabDiperlukan[key][keyDetail] * 30 * 3}
                          </td>
                        )}
                      </React.Fragment>

                    ))
                  )}
                </tr>
              </tbody>
            </Table>

            <Typography
              fontSize="15px"
              fontWeight="bold"
              className="d-block my-3 text-center"
            >
              Stock Obat
            </Typography>
            <div class="table-responsive" style={{height: '1500px'}}>
              <Table bordered striped style={{ fontSize: 12 }}>
                {/* 
                  position: sticky;
                  left: 0;
                  background-color: white;
                  -webkit-box-shadow: 0px 0px 0px 1px #dee2e6;
                  -moz-box-shadow: 0px 0px 0px 1px #dee2e6;
                  box-shadow: 0px 0px 0px 1px #dee2e6;
                */}
                <thead>
                  <tr>
                    <th style={{ position: 'sticky', top: 0, backgroundColor: 'white', boxShadow: '0px 0px 0px 1px #dee2e6' }} rowSpan="2">No</th>
                    <th style={{ position: 'sticky', top: 0, backgroundColor: 'white', boxShadow: '0px 0px 0px 1px #dee2e6' }} rowSpan="2">Nama Obat</th>
                    <th style={{ position: 'sticky', top: 0, backgroundColor: 'white', boxShadow: '0px 0px 0px 1px #dee2e6' }}>Stok obat awal bulan (tablet)</th>
                    <th style={{ position: 'sticky', top: 0, backgroundColor: 'white', boxShadow: '0px 0px 0px 1px #dee2e6' }}>Stok obat yang diterima bulan ini (tablet)</th>
                    <th style={{ position: 'sticky', top: 0, backgroundColor: 'white', boxShadow: '0px 0px 0px 1px #dee2e6' }}>Stok obat yang dikeluarkan bulan ini (tablet)</th>
                    <th style={{ position: 'sticky', top: 0, backgroundColor: 'white', boxShadow: '0px 0px 0px 1px #dee2e6' }}>Stok obat yang kadaluarsa bulan ini (tablet)</th>
                    <th style={{ position: 'sticky', top: 0, backgroundColor: 'white', boxShadow: '0px 0px 0px 1px #dee2e6' }}>
                      Selisih fisik stok obat dengan pencatatan bulan ini (tablet)
                  </th>
                    <th style={{ position: 'sticky', top: 0, backgroundColor: 'white', boxShadow: '0px 0px 0px 1px #dee2e6' }}>Stok obat pada akhir bulan ini (tablet)</th>
                    <th style={{ position: 'sticky', top: 0, backgroundColor: 'white', boxShadow: '0px 0px 0px 1px #dee2e6' }}>Stok obat pada akhir bulan ini (botol)</th>
                    <th style={{ position: 'sticky', top: 0, backgroundColor: 'white', boxShadow: '0px 0px 0px 1px #dee2e6' }}>Tanggal kadaluarsa (botol)</th>
                    <th style={{ position: 'sticky', top: 0, backgroundColor: 'white', boxShadow: '0px 0px 0px 1px #dee2e6' }}>Perkiraan Jumlah obat yang diperlukan (botol)</th>
                    <th style={{ position: 'sticky', top: 0, backgroundColor: 'white', boxShadow: '0px 0px 0px 1px #dee2e6' }}>Jumlah Pasien reguler bulan ini</th>
                  </tr>
                  <tr>
                    <td style={{ position: 'sticky', top: 152, backgroundColor: 'white', boxShadow: '0px 0px 0px 1px #dee2e6' }}>(A)</td>
                    <td style={{ position: 'sticky', top: 152, backgroundColor: 'white', boxShadow: '0px 0px 0px 1px #dee2e6' }}>(B)</td>
                    <td style={{ position: 'sticky', top: 152, backgroundColor: 'white', boxShadow: '0px 0px 0px 1px #dee2e6' }}>(C)</td>
                    <td style={{ position: 'sticky', top: 152, backgroundColor: 'white', boxShadow: '0px 0px 0px 1px #dee2e6' }}>(D)</td>
                    <td style={{ position: 'sticky', top: 152, backgroundColor: 'white', boxShadow: '0px 0px 0px 1px #dee2e6' }}>(E)</td>
                    <td style={{ position: 'sticky', top: 152, backgroundColor: 'white', boxShadow: '0px 0px 0px 1px #dee2e6' }}>(F) = (A+b)-(C+D)+E</td>
                    <td style={{ position: 'sticky', top: 152, backgroundColor: 'white', boxShadow: '0px 0px 0px 1px #dee2e6' }}>(G)</td>
                    <td style={{ position: 'sticky', top: 152, backgroundColor: 'white', boxShadow: '0px 0px 0px 1px #dee2e6' }}>(H)</td>
                    <td style={{ position: 'sticky', top: 152, backgroundColor: 'white', boxShadow: '0px 0px 0px 1px #dee2e6' }}>(I)</td>
                    <td style={{ position: 'sticky', top: 152, backgroundColor: 'white', boxShadow: '0px 0px 0px 1px #dee2e6' }}>(-)</td>
                  </tr>
                </thead>
                <tbody>
                  {!isLoading &&
                    inventory.content.map((value, idx) => {
                      if (value.medicine && value.medicine.medicineType === "ARV") {
                        if (!value.title) {
                          return (
                            <tr key={idx}>
                              <td>{no++}</td>
                              <td>{value.medicine.name}</td>
                              {
                                Object.keys(value.data).map((key, index) => (
                                  <td key={key}>
                                    {typeof value.data[key] === "number" &&
                                      formatNumber(value.data[key])}
                                    {typeof value.data[key] !== "number" &&
                                      value.data[key]}
                                  </td>
                                ))
                              }
                            </tr>
                          )
                        } else {
                          return (
                            <tr key={idx}>
                              <td colSpan={12}><b>{value.title}</b></td>
                            </tr>
                          )
                        }
                      }
                    })}
                  {isLoading && (
                    <tr>
                      <td colSpan="12" className="text-center">
                        Loading...
                    </td>
                    </tr>
                  )}
                </tbody>
              </Table>
            </div>
            <div class="table-responsive mt-5" style={{height: '1500px'}}>
              <Table bordered striped style={{ fontSize: 12 }}>
                {/* 
                  position: sticky;
                  left: 0;
                  background-color: white;
                  -webkit-box-shadow: 0px 0px 0px 1px #dee2e6;
                  -moz-box-shadow: 0px 0px 0px 1px #dee2e6;
                  box-shadow: 0px 0px 0px 1px #dee2e6;
                */}
                <thead>
                  <tr>
                    <th style={{ position: 'sticky', top: 0, backgroundColor: 'white', boxShadow: '0px 0px 0px 1px #dee2e6' }} rowSpan="2">No</th>
                    <th style={{ position: 'sticky', top: 0, backgroundColor: 'white', boxShadow: '0px 0px 0px 1px #dee2e6' }} rowSpan="2">Nama Obat</th>
                    <th style={{ position: 'sticky', top: 0, backgroundColor: 'white', boxShadow: '0px 0px 0px 1px #dee2e6' }}>Stok obat awal bulan (tablet)</th>
                    <th style={{ position: 'sticky', top: 0, backgroundColor: 'white', boxShadow: '0px 0px 0px 1px #dee2e6' }}>Stok obat yang diterima bulan ini (tablet)</th>
                    <th style={{ position: 'sticky', top: 0, backgroundColor: 'white', boxShadow: '0px 0px 0px 1px #dee2e6' }}>Stok obat yang dikeluarkan bulan ini (tablet)</th>
                    <th style={{ position: 'sticky', top: 0, backgroundColor: 'white', boxShadow: '0px 0px 0px 1px #dee2e6' }}>Stok obat yang kadaluarsa bulan ini (tablet)</th>
                    <th style={{ position: 'sticky', top: 0, backgroundColor: 'white', boxShadow: '0px 0px 0px 1px #dee2e6' }}>
                      Selisih fisik stok obat dengan pencatatan bulan ini (tablet)
                  </th>
                    <th style={{ position: 'sticky', top: 0, backgroundColor: 'white', boxShadow: '0px 0px 0px 1px #dee2e6' }}>Stok obat pada akhir bulan ini (tablet)</th>
                    <th style={{ position: 'sticky', top: 0, backgroundColor: 'white', boxShadow: '0px 0px 0px 1px #dee2e6' }}>Stok obat pada akhir bulan ini (botol)</th>
                    <th style={{ position: 'sticky', top: 0, backgroundColor: 'white', boxShadow: '0px 0px 0px 1px #dee2e6' }}>Tanggal kadaluarsa (botol)</th>
                    <th style={{ position: 'sticky', top: 0, backgroundColor: 'white', boxShadow: '0px 0px 0px 1px #dee2e6' }}>Perkiraan Jumlah obat yang diperlukan (botol)</th>
                    <th style={{ position: 'sticky', top: 0, backgroundColor: 'white', boxShadow: '0px 0px 0px 1px #dee2e6' }}>Rata - rata penggunaan 6 bulan</th>
                  </tr>
                  <tr>
                    <td style={{ position: 'sticky', top: 152, backgroundColor: 'white', boxShadow: '0px 0px 0px 1px #dee2e6' }}>(A)</td>
                    <td style={{ position: 'sticky', top: 152, backgroundColor: 'white', boxShadow: '0px 0px 0px 1px #dee2e6' }}>(B)</td>
                    <td style={{ position: 'sticky', top: 152, backgroundColor: 'white', boxShadow: '0px 0px 0px 1px #dee2e6' }}>(C)</td>
                    <td style={{ position: 'sticky', top: 152, backgroundColor: 'white', boxShadow: '0px 0px 0px 1px #dee2e6' }}>(D)</td>
                    <td style={{ position: 'sticky', top: 152, backgroundColor: 'white', boxShadow: '0px 0px 0px 1px #dee2e6' }}>(E)</td>
                    <td style={{ position: 'sticky', top: 152, backgroundColor: 'white', boxShadow: '0px 0px 0px 1px #dee2e6' }}>(F) = (A+b)-(C+D)+E</td>
                    <td style={{ position: 'sticky', top: 152, backgroundColor: 'white', boxShadow: '0px 0px 0px 1px #dee2e6' }}>(G)</td>
                    <td style={{ position: 'sticky', top: 152, backgroundColor: 'white', boxShadow: '0px 0px 0px 1px #dee2e6' }}>(H)</td>
                    <td style={{ position: 'sticky', top: 152, backgroundColor: 'white', boxShadow: '0px 0px 0px 1px #dee2e6' }}>(I)</td>
                    <td style={{ position: 'sticky', top: 152, backgroundColor: 'white', boxShadow: '0px 0px 0px 1px #dee2e6' }}>(-)</td>
                  </tr>
                </thead>
                <tbody>
                  {!isLoading &&
                    inventory.content.map((value, idx) => {
                      if (value.medicine && value.medicine.medicineType === "NON_ARV") {
                        if (!value.title) {
                          return (
                            <tr key={idx}>
                              <td>{no++}</td>
                              <td>{value.medicine.name}</td>
                              {
                                Object.keys(value.data).map((key, index) => (
                                  <td key={key}>
                                    {typeof value.data[key] === "number" &&
                                      formatNumber(value.data[key])}
                                    {typeof value.data[key] !== "number" &&
                                      value.data[key]}
                                  </td>
                                ))
                              }
                            </tr>
                          )
                        } else {
                          return (
                            <tr key={idx}>
                              <td colSpan={12}><b>{value.title}</b></td>
                            </tr>
                          )
                        }
                      }
                    })}
                  {isLoading && (
                    <tr>
                      <td colSpan="12" className="text-center">
                        Loading...
                    </td>
                    </tr>
                  )}
                </tbody>
              </Table>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};
