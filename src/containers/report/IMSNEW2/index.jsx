import React, { useEffect, useState, useContext } from "react";
import API from "utils/API";
import { BASE_URL } from "utils/request";
import GlobalContext from "../../GlobalContext";
import { ENUM, ROLE } from "constant";
import moment from "moment";
import View from "./view";

const Handler = () => {
  const user = useContext(GlobalContext);

  const defaultFilter = {
    PROVINCE: [
      ENUM.LOGISTIC_ROLE.PROVINCE_ENTITY,
      ENUM.LOGISTIC_ROLE.SUDIN_ENTITY,
      ENUM.LOGISTIC_ROLE.UPK_ENTITY
    ].includes(user.logisticrole)
      ? user.provinceId
      : "",
    SUDINKAB: [
      ENUM.LOGISTIC_ROLE.SUDIN_ENTITY,
      ENUM.LOGISTIC_ROLE.UPK_ENTITY
    ].includes(user.logisticrole)
      ? user.sudinKabKotaId
      : "",
    UPK: user.logisticrole === ENUM.LOGISTIC_ROLE.UPK_ENTITY || user.logisticrole === null ? user.upkId : "",
    MONTH: moment().format('YYYY-MM')
  };

  const [data, setData] = useState([]);
  const [oldData, setOldData] = useState([]);
  // const [isLoading, setLoading] = useState(false);
  const [isLoading] = useState(false);
  const [filter, setFilter] = useState(defaultFilter);

  const _handleFilter = (type, value) => {
    if (typeof value === "undefined") {
      value = "";
    }

    setFilter({
      ...filter,
      [type]: value
    });
  };

  async function _handleDownloadExcel() {
    try {
      let role = user.role

      let res = await API.IMS.laporan1All2(
        filter.UPK ? filter.UPK : (role === ROLE.RR_STAFF && user.upkId),
        filter.SUDINKAB ? filter.SUDINKAB : (role === ROLE.SUDIN_STAFF && user.sudinKabKotaId),
        filter.PROVINCE ? filter.PROVINCE : (role === ROLE.PROVINCE_STAFF && user.provinceId),
        !filter.MONTH.length ? moment().format('YYYY-MM') + "-01" : `${filter.MONTH}-01`,
        true
      );
      window.open(`${BASE_URL}/${res.data.split('/').slice(2).join('/')}`);
    } catch (ex) {
      console.log(ex)
    }
  }

  async function _fetchData() {
    try {
      let role = user.role

      let res = await API.IMS.laporan1All2(
        filter.UPK ? filter.UPK : (role === ROLE.RR_STAFF && user.upkId),
        filter.SUDINKAB ? filter.SUDINKAB : (role === ROLE.SUDIN_STAFF && user.sudinKabKotaId),
        filter.PROVINCE ? filter.PROVINCE : (role === ROLE.PROVINCE_STAFF && user.provinceId),
        !filter.MONTH.length ? moment().format('YYYY-MM') + "-01" : `${filter.MONTH}-01`
      );
      setData(res.data.data)
      setOldData(res.lastReport)
    } catch (ex) {
      console.log(ex)
    }
  }

  useEffect(() => {
    _fetchData();
  }, [filter]);

  return (
    <View
      data={data}
      oldData={oldData}
      isLoading={isLoading}
      onFilter={_handleFilter}
      currentFilter={filter}
      onDownloadExcel={_handleDownloadExcel}
    />
  );
};

export default Handler;
