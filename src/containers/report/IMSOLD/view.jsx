import React from "react";
// import moment from "moment";

import { Spin } from "antd";
import { Table } from "reactstrap";
import { Typography, Filter } from "components";
// import { formatNumber } from "utils";
// import { ROLE } from '../../../constant'

import './style.css';

const View = ({ data, isLoading, onFilter, currentFilter, button, onDownloadExcel }) => {
  return (
    <React.Fragment>
      <div className="container h-100 pt-4">
        <div className="row mb-2">
          <div className="col-12 bg-white rounded p-4 border">
            <div className="mb-2">
              <Filter
                onFilter={onFilter}
                disableNextMonth={true}
                currentFilter={currentFilter}
                excel
                onDownloadExcel={onDownloadExcel}
              />
            </div>
          </div>
        </div>

        <div className="row">
          <div className="col-12 bg-white rounded p-4 border mb-5">
            {isLoading && (
              <div className="text-center">
                <Spin />
              </div>
            )}
            <Typography
              fontSize="15px"
              fontWeight="bold"
              className="d-block mb-3 text-center"
            >
              Laporan IMS
            </Typography>
            <Table bordered responsive striped style={{ fontSize: 12 }}>
              <thead>
                <tr>
                  <th className={'sticky-td'} rowSpan="2" style={{ paddingRight: "250px" }}>
                    Indikator
                  </th>
                  <th colSpan="13">Perempuan</th>
                  {/* <th rowSpan="2">Total</th> */}
                  <th colSpan="13">Laki-laki</th>
                  {/* <th rowSpan="2">Total</th> */}
                  {/* <th colSpan="12">Kelompok Populasi Khusus</th> */}
                  {/* <th rowSpan="2">Total</th> */}
                </tr>
                <tr>
                  <th>{'< 1'}</th>
                  <th>1-4</th>
                  <th>5-14</th>
                  <th>15-19</th>
                  <th>20-24</th>
                  <th>25-29</th>
                  <th>30-34</th>
                  <th>35-39</th>
                  <th>40-44</th>
                  <th>45-49</th>
                  <th>50-59</th>
                  <th>{">="} 60</th>
                  {/* <th>>= 60</th> */}
                  <th>Jumlah</th>

                  <th>{'< 1'}</th>
                  <th>1-4</th>
                  <th>5-14</th>
                  <th>15-19</th>
                  <th>20-24</th>
                  <th>25-29</th>
                  <th>30-34</th>
                  <th>35-39</th>
                  <th>40-44</th>
                  <th>45-49</th>
                  <th>50-59</th>
                  <th>{">="} 60</th>
                  {/* <th>>= 60</th> */}
                  <th>Jumlah</th>
                  {/* <th>WPS</th>
                  <th>Waria</th>
                  <th>Penasun</th>
                  <th>LSL</th>
                  <th>Bumil</th>
                  <th>Pasien TB</th>
                  <th>Pasien IMS</th>
                  <th>Pasien Hepatitis</th>
                  <th>Pasangan ODHA</th>
                  <th>Anak ODHA</th>
                  <th>WBP</th>
                  <th>Lainnya</th> */}
                </tr>
              </thead>
              <tbody>
                {data.map((v, i) => (
                    <tr key={i}>
                      {v.length ?
                        v.map((k, ii) => {
                          return (
                            <td className={(ii === 0 ? 'sticky-td ' + (v.length === 1 ? 'text-primary font-weight-bold h6' : '') : '')} key={ii}>{k ? k : '0'}</td>
                          )
                        }) :
                        Array(22).fill('-').map(
                          (k, i) => <td key={i}>{k ? k : '0'}</td>
                        )
                      }
                    </tr>
                ))}
                {/* {Object.keys(data.content).map((key, idx) => (
                  <React.Fragment key={idx}>
                    <tr>
                      <td colSpan="24">{data.content[key].title}</td>
                    </tr>
                    {data.content[key].data.map((value, idxData) => {
                      let sumMale = 0;
                      let sumFemale = 0;

                      return (
                        <tr key={idxData}>
                          <td>{value.label}</td>
                          {value.data
                            .slice(0, 10)
                            .map((valueCount, idxCount) => {
                              sumMale = sumMale + valueCount;
                              return (
                                <td key={idxCount}>
                                  {formatNumber(valueCount)}
                                </td>
                              );
                            })}
                          <td>{formatNumber(sumMale)}</td>
                          {value.data
                            .slice(10, 20)
                            .map((valueCount, idxCount) => {
                              sumFemale = sumFemale + valueCount;
                              return (
                                <td key={idxCount}>
                                  {formatNumber(valueCount)}
                                </td>
                              );
                            })}
                          <td>{formatNumber(sumFemale)}</td>
                          <td>{formatNumber(sumMale + sumFemale)}</td>
                        </tr>
                      ); */}
                {/* }
                    )}
                  </React.Fragment> */}
                {/* ))} */}
              </tbody>
            </Table>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default View;
