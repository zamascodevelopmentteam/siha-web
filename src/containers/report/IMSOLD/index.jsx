import React, { useEffect, useState, useContext } from "react";
import API from "utils/API";
import { BASE_URL } from "utils/request";
// import axios from "axios";
import GlobalContext from "../../GlobalContext";

import View from "./view";
import { ENUM, ROLE } from "constant";
import moment from "moment";

// const emptyState = {
//   content: {
//     diTestImsList: {
//       title: "Jumlah orang yang dites IMS",
//       data: []
//     },
//     testImsPositifList: {
//       title: "Jumlah orang yang Positif IMS",
//       data: []
//     },
//     diTestSifilisList: {
//       title: "Jumlah orang yang dites Sifilis",
//       data: []
//     },
//     testSifilisPositifList: {
//       title: "Jumlah orang yang Positif Sifilis",
//       data: []
//     }
//   }
// };

const Handler = () => {
  const user = useContext(GlobalContext);

  const defaultFilter = {
    PROVINCE: [
      ENUM.LOGISTIC_ROLE.PROVINCE_ENTITY,
      ENUM.LOGISTIC_ROLE.SUDIN_ENTITY,
      ENUM.LOGISTIC_ROLE.UPK_ENTITY
    ].includes(user.logisticrole)
      ? user.provinceId
      : "",
    SUDINKAB: [
      ENUM.LOGISTIC_ROLE.SUDIN_ENTITY,
      ENUM.LOGISTIC_ROLE.UPK_ENTITY
    ].includes(user.logisticrole)
      ? user.sudinKabKotaId
      : "",
    UPK: undefined,
    MONTH: ""
  };


  const [data, setData] = useState([]);
  // const [isLoading, setLoading] = useState(false);
  const [isLoading] = useState(false);
  const [filter, setFilter] = useState(defaultFilter);

  const _handleFilter = (type, value) => {
    if (typeof value === "undefined") {
      value = "";
    }

    setFilter({
      ...filter,
      [type]: value
    });
  };

  async function _handleDownloadExcel() {
    try {
      let role = user.role

      let res = await API.IMS.laporan1old(
        filter.UPK ? filter.UPK : (role === ROLE.RR_STAFF && user.upkId),
        filter.SUDINKAB ? filter.SUDINKAB : (role === ROLE.SUDIN_STAFF && user.sudinKabKotaId),
        filter.PROVINCE ? filter.PROVINCE : (role === ROLE.PROVINCE_STAFF && user.provinceId),
        !filter.MONTH.length ? moment().format('YYYY-MM') + "-01" : `${filter.MONTH}-01`,
        true
      );
      window.open(`${BASE_URL}/${res.data.split('/').slice(2).join('/')}`);
    } catch (ex) {
      console.log(ex)
    }
  }

  async function _fetchData() {
    try {
      let role = user.role

      let res = await API.IMS.laporan1old(
        filter.UPK ? filter.UPK : (role === ROLE.RR_STAFF && user.upkId),
        filter.SUDINKAB ? filter.SUDINKAB : (role === ROLE.SUDIN_STAFF && user.sudinKabKotaId),
        filter.PROVINCE ? filter.PROVINCE : (role === ROLE.PROVINCE_STAFF && user.provinceId),
        !filter.MONTH.length ? moment().format('YYYY-MM') + "-01" : `${filter.MONTH}-01`
      );
      setData(res.data.data)
    } catch (ex) {
      console.log(ex)
    }
  }

  useEffect(() => {
    _fetchData();
  }, [filter]);

  return (
    <View
      data={data}
      isLoading={isLoading}
      onFilter={_handleFilter}
      currentFilter={filter}
      onDownloadExcel={_handleDownloadExcel}
    />
  );
};

export default Handler;
