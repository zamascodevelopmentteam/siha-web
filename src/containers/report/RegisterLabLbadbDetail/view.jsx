import React from "react";
import moment from "moment";

import { Spin } from "antd";
import { Table } from "reactstrap";
import { Typography, Filter } from "components";
// import { formatNumber } from "utils";

export const RegisterLabLbadbDetail = ({
  data,
  isLoading,
  onFilter,
  currentFilter
}) => {
  const 
    meta = data.meta,
    lbadbDetail = data.lbadbDetail,
    hasilTesLab = data.hasilTesLab;

  return (
    <React.Fragment>
      <div className="container h-100 pt-4">
        <div className="row mb-2">
          <div className="col-12 bg-white rounded p-4 border">
            <div className="mb-2">
              <Filter
                onFilter={onFilter}
                // disabledDate={current =>
                //   current &&
                //   current.format("YYYY-MM") !== moment().format("YYYY-MM")
                // }
                currentFilter={currentFilter}
              />
            </div>
          </div>
        </div>
        <div
          className="row bg-white rounded p-1 border mb-1"
          style={{ fontSize: 12 }}
        >
          <div className="col-12">
            {isLoading && (
              <div className="text-center">
                <Spin />
              </div>
            )}
          </div>
          <div className="col-6 ">
            <dl className="dl-horizontal">
              <dt>Nama UPK</dt>
              <dd>{meta && meta.upk.name ? meta.upk.name : "-"}</dd>
              <dt>Nama Kab/Kota</dt>
              <dd>
                {meta && meta.sudinKabKota.name ? meta.sudinKabKota.name : "-"}
              </dd>
              <dt>Nama Provinsi</dt>
              <dd>{meta && meta.province.name ? meta.province.name : "-"}</dd>
            </dl>
          </div>
          <div className="col-6">
            <dl className="dl-horizontal">
              <dt>Bulan</dt>
              <dd>{meta && meta.bulan ? meta.bulan : "-"}</dd>
              <dt>Tahun</dt>
              <dd>{meta && meta.tahun ? meta.tahun : "-"}</dd>
              <dt>Tanggal Akses</dt>
              <dd>{meta && meta.tanggal ? meta.tanggal : "-"}</dd>
            </dl>
          </div>
        </div>
        <div className="row">
          <div className="col-12 bg-white rounded p-4 border mb-5">
            <Typography
              fontSize="15px"
              fontWeight="bold"
              className="d-block mb-3 text-center"
            >
              Ringkasan Hasil Pemeriksaan Laboratorium HIV AIDS & PIMS
            </Typography>
            {!isLoading &&
              hasilTesLab &&
              hasilTesLab.content.map((content, index) => (
                <Table bordered responsive striped style={{ fontSize: 12 }}>
                  <thead>
                    <tr>
                      <th width="50px">No</th>
                      <th width="250px">Jenis Pemeriksaan</th>
                      {content &&
                        Object.keys(content.data).map(
                          (keyContent, indexContent) => (
                            <React.Fragment key={keyContent}>
                              <th width={indexContent === 0 ? "200px" : "auto"}>
                                {keyContent}
                              </th>
                            </React.Fragment>
                          )
                        )}
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td width="50px">{index + 1}</td>
                      <td width="250px">{content.title}</td>
                      {content &&
                        Object.values(content.data).map(
                          (keyContent, indexContent) => (
                            <React.Fragment key={keyContent}>
                              <td width={indexContent === 0 ? "200px" : "auto"}>
                                {keyContent}
                              </td>
                            </React.Fragment>
                          )
                        )}
                    </tr>
                  </tbody>
                </Table>
              ))}
            {isLoading && (
              <div className="text-center">
                <Spin />
              </div>
            )}

            <Typography
              fontSize="15px"
              fontWeight="bold"
              className="d-block mb-3 text-center"
            >
              Rangkuman Penggunaan Komoditas Laboratorium Program HIV AIDS {'&'} PIMS
            </Typography>
            {isLoading && (
              <div className="text-center">
                <Spin />
              </div>
            )}
            <Table bordered responsive striped style={{ fontSize: 12 }}>
              <thead>
                <tr>
                  <th width="100px" rowSpan="2">
                    Jenis Pemeriksaan
                  </th>
                  <th style={{ paddingRight: "200px" }} rowSpan="2">
                    Merk
                  </th>
                  <th colSpan="4">Reagen 1</th>
                  <th colSpan="4">Reagen 2</th>
                  <th colSpan="4">Reagen 3</th>
                </tr>
                <tr>
                  <th>Jumlah stok yang dipakai</th>
                  <th>Jumlah Reaktif</th>
                  <th>Jumlah Non Reaktif</th>
                  <th>Jumlah Invalid</th>
                  <th>Jumlah stok yang dipakai</th>
                  <th>Jumlah Reaktif</th>
                  <th>Jumlah Non Reaktif</th>
                  <th>Jumlah Invalid</th>
                  <th>Jumlah stok yang dipakai</th>
                  <th>Jumlah Reaktif</th>
                  <th>Jumlah Non Reaktif</th>
                  <th>Jumlah Invalid</th>
                </tr>
              </thead>
              <tbody>
                {lbadbDetail &&
                  lbadbDetail.RDT &&
                  Object.values(lbadbDetail.RDT).map(
                    (keyContent, indexContent) => (
                      <React.Fragment key={keyContent}>
                        <tr>
                          <td>HIV - RDT</td>
                          <td>{keyContent.brand.name}</td>
                          <td>{keyContent.data.r1.stok_dipakai}</td>
                          <td>{keyContent.data.r1.REAKTIF}</td>
                          <td>{keyContent.data.r1.NON_REAKTIF}</td>
                          <td>{keyContent.data.r1.INVALID}</td>
                          <td>{keyContent.data.r2.stok_dipakai}</td>
                          <td>{keyContent.data.r2.REAKTIF}</td>
                          <td>{keyContent.data.r2.NON_REAKTIF}</td>
                          <td>{keyContent.data.r2.INVALID}</td>
                          <td>{keyContent.data.r3.stok_dipakai}</td>
                          <td>{keyContent.data.r3.REAKTIF}</td>
                          <td>{keyContent.data.r3.NON_REAKTIF}</td>
                          <td>{keyContent.data.r3.INVALID}</td>
                        </tr>
                      </React.Fragment>
                    )
                  )}
              </tbody>
            </Table>

            <Table bordered responsive striped style={{ fontSize: 12 }}>
              <thead>
                <tr>
                  <th width="100px">Jenis Pemeriksaan</th>
                  <th style={{ paddingRight: "200px" }}>Merk</th>
                  <th>Jumlah stok yang dipakai</th>
                  <th>Jumlah Detected</th>
                  <th>Jumlah Not Detected</th>
                  <th>Jumlah Invalid</th>
                  <th>Jumlah Error</th>
                  <th>Jumlah No Result</th>
                  <th>Jumlah Kalibrasi</th>
                  <th>Jumlah Terbuang</th>
                  <th>Jumlah Gangguan Teknis</th>
                  <th>Jumlah Sebab Lainnya</th>
                  <th>Jumlah Kontrol</th>
                </tr>
              </thead>
              <tbody>
                {lbadbDetail &&
                  lbadbDetail.EID &&
                  Object.values(lbadbDetail.EID).map(
                    (keyContent, indexContent) => (
                      <React.Fragment key={keyContent}>
                        <tr>
                          <td>EID</td>
                          <td>{keyContent.brand.name}</td>
                          <td>{keyContent.data.stok_dipakai}</td>
                          <td>{keyContent.data.DETECTED}</td>
                          <td>{keyContent.data["NOT DETECTED"]}</td>
                          <td>{keyContent.data["INVALID"]}</td>
                          <td>{keyContent.data["ERROR"]}</td>
                          <td>{keyContent.data["NO RESULT"]}</td>
                          <td>{keyContent.data["KALIBRASI"]}</td>
                          <td>{keyContent.data["TERBUANG"]}</td>
                          <td>{keyContent.data["GANGGUAN TEKNIS"]}</td>
                          <td>{keyContent.data["SEBAB LAINNYA"]}</td>
                          <td>{keyContent.data["KONTROL"]}</td>
                        </tr>
                      </React.Fragment>
                    )
                  )}
              </tbody>
            </Table>

            {/* <Table bordered responsive striped style={{ fontSize: 12 }}>
              <thead>
                <tr>
                  <th width="100px">Jenis Pemeriksaan</th>
                  <th style={{ paddingRight: "200px" }}>Merk</th>
                  <th>Jumlah stok yang dipakai</th>
                  <th>Jumlah Reaktif</th>
                  <th>Jumlah Non Reaktif</th>
                  <th>Jumlah Invalid</th>
                </tr>
              </thead>
              <tbody>
                {lbadbDetail &&
                  lbadbDetail.IMS &&
                  Object.values(lbadbDetail.IMS).map(
                    (keyContent, indexContent) => (
                      <React.Fragment key={keyContent}>
                        <tr>
                          <td>IMS</td>
                          <td>{keyContent.brand.name}</td>
                          <td>{keyContent.data.stok_dipakai}</td>
                          <td>{keyContent.data.REAKTIF}</td>
                          <td>{keyContent.data.NON_REAKTIF}</td>
                          <td>{keyContent.data.INVALID}</td>
                        </tr>
                      </React.Fragment>
                    )
                  )}
              </tbody>
            </Table> */}

            <Table bordered responsive striped style={{ fontSize: 12 }}>
              <thead>
                <tr>
                  <th>Jenis Pemeriksaan</th>
                  <th>Mesin</th>
                  <th>Jumlah Pasien</th>
                  <th>{'< 20'}</th>
                  <th>{'< 34'}</th>
                  <th>{'< 40'}</th>
                  <th>{'< 50'}</th>
                  <th>Not Detected</th>
                  <th>Invalid/Error</th>
                  <th>Input Angka</th>
                </tr>
              </thead>
              <tbody>
                {lbadbDetail &&
                  lbadbDetail.VL &&
                  Object.values(lbadbDetail.VL).map(
                    (keyContent, indexContent) => (
                      <React.Fragment key={keyContent}>
                        <tr>
                          <td>{keyContent.brand.name}</td>
                          <td>{keyContent.mesin}</td>
                          <td>{keyContent.data.DETECTED}</td>
                          <td>{keyContent.data['< 20']}</td>
                          <td>{keyContent.data['< 34']}</td>
                          <td>{keyContent.data['< 40']}</td>
                          <td>{keyContent.data['< 50']}</td>
                          <td>{keyContent.data['NOT DETECTED']}</td>
                          <td>{keyContent.data['INVALID']}</td>
                          <td>{keyContent.data['INPUT ANGKA']}</td>
                        </tr>
                      </React.Fragment>
                    )
                  )}
              </tbody>
            </Table>

            <Table bordered responsive striped style={{ fontSize: 12 }}>
              <thead>
                <tr>
                  <th>Jenis Pemeriksaan</th>
                  <th>Mesin</th>
                  <th>Jumlah Pasien</th>
                  <th>{'< 20'}</th>
                  <th>{'< 34'}</th>
                  <th>{'< 40'}</th>
                  <th>{'< 50'}</th>
                  <th>Not Detected</th>
                  <th>Invalid/Error</th>
                  <th>Input Angka</th>
                </tr>
              </thead>
              <tbody>
                {lbadbDetail &&
                  lbadbDetail.CD4 &&
                  Object.values(lbadbDetail.CD4).map(
                    (keyContent, indexContent) => (
                      <React.Fragment key={keyContent}>
                        <tr>
                          <td>{keyContent.brand.name}</td>
                          <td>{keyContent.mesin}</td>
                          <td>{keyContent.data.DETECTED}</td>
                          <td>{keyContent.data['< 20']}</td>
                          <td>{keyContent.data['< 34']}</td>
                          <td>{keyContent.data['< 40']}</td>
                          <td>{keyContent.data['< 50']}</td>
                          <td>{keyContent.data['NOT DETECTED']}</td>
                          <td>{keyContent.data['INVALID']}</td>
                          <td>{keyContent.data['INPUT ANGKA']}</td>
                        </tr>
                      </React.Fragment>
                    )
                  )}
              </tbody>
            </Table>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};
