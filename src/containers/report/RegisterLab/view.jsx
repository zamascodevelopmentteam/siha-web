import React, { useContext, useState } from "react";
// import moment from "moment";

import { Spin } from "antd";
import { Select } from "antd";
import { Table, Label } from "reactstrap";
import { Typography, Filter } from "components";
import { formatNumber } from "utils";

import GlobalContext from "containers/GlobalContext";
import { ROLE, sumberDana } from "constant";

const { Option } = Select;

export const RegisterLab = ({ data, isLoading, onFilter, currentFilter }) => {
  const user = useContext(GlobalContext);

  const [typeDana, setTypeDana] = useState("");

  const
    meta = data.meta,
    komoditasLab = data.komoditasLab,
    inventoryTable = data.inventoryTable;
  let komoditasLabFinalData = komoditasLab;
  let filteredKomoditasLab = [];

  if (typeDana.length > 0 && komoditasLab) {
    filteredKomoditasLab = {
      content: komoditasLab.content.filter(content => {
        if (content.sumber_dana) {
          if (content.sumber_dana.toLowerCase().includes(typeDana.toLowerCase())) {
            return true;
          } else {
            return false
          }
        } else {
          return false;
        }
      })
    }

    komoditasLabFinalData = filteredKomoditasLab;
  }

  let lastTitle = "";

  return (
    <React.Fragment>
      <div className="container h-100 pt-4">
        <div className="row mb-2">
          <div className="col-12 bg-white rounded p-4 border">
            <div className="mb-2">
              <Filter
                onFilter={onFilter}
                // disabledDate={current =>
                //   current &&
                //   current.format("YYYY-MM") !== moment().format("YYYY-MM")
                // }
                currentFilter={currentFilter}
              />
              <div className="row">
                <div className="col-4 mt-2">
                  <Label><b>Sumber Dana</b></Label>
                  <Select
                    style={{ width: "100%" }}
                    onChange={setTypeDana}
                    value={typeDana}
                  >
                    <Option value="">All</Option>
                    {sumberDana.map(item => (
                      <Option key={item} value={item} title={item}>
                        {item}
                      </Option>
                    ))}
                  </Select>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div
          className="row bg-white rounded p-1 border mb-1"
          style={{ fontSize: 12 }}
        >
          <div className="col-12">
            {isLoading && (
              <div className="text-center">
                <Spin />
              </div>
            )}
          </div>
          <div className="col-6 ">
            <dl className="dl-horizontal">
              <dt>Nama UPK</dt>
              <dd>{meta && meta.upk.name ? meta.upk.name : "-"}</dd>
              <dt>Nama Kab/Kota</dt>
              <dd>
                {meta && meta.sudinKabKota.name ? meta.sudinKabKota.name : "-"}
              </dd>
              <dt>Nama Provinsi</dt>
              <dd>{meta && meta.province.name ? meta.province.name : "-"}</dd>
            </dl>
          </div>
          <div className="col-6">
            <dl className="dl-horizontal">
              <dt>Bulan</dt>
              <dd>{meta && meta.bulan ? meta.bulan : "-"}</dd>
              <dt>Tahun</dt>
              <dd>{meta && meta.tahun ? meta.tahun : "-"}</dd>
              <dt>Tanggal Akses</dt>
              <dd>{meta && meta.tanggal ? meta.tanggal : "-"}</dd>
            </dl>
          </div>
        </div>
        <div className="row">
          <div className="col-12 bg-white rounded p-4 border mb-5">
            <Typography
              fontSize="15px"
              fontWeight="bold"
              className="d-block mb-3 text-center"
            >
              Ringkasan Penggunaan Komoditas Laboratorium HIV AIDS & PIMS
            </Typography>
            <Table bordered responsive striped style={{ fontSize: 12 }}>
              <thead>
                <tr>
                  <th>No</th>
                  <th>Jenis Pemeriksaan</th>
                  <th>Komoditas</th>
                  <th>Sumber Dana</th>
                  <th>Satuan</th>
                  <th>
                    Jumlah stok akhir
                    {user.role == ROLE.MINISTRY_STAFF && ' pusat'}
                    {user.role == ROLE.PROVINCE_STAFF && ' provinsi'}
                    {user.role == ROLE.SUDIN_STAFF && ' kabkota'}
                    {user.role == ROLE.PHARMA_STAFF && ' layanan'}
                  </th>
                  {[ROLE.MINISTRY_STAFF].includes(user.role) && (
                    <th>Jumlah stok akhir seluruh provinsi</th>
                  )}
                  {[ROLE.PROVINCE_STAFF, ROLE.MINISTRY_STAFF].includes(user.role) && (
                    <th>Jumlah stok akhir seluruh kabkota</th>
                  )}
                  {[ROLE.SUDIN_STAFF, ROLE.PROVINCE_STAFF, ROLE.MINISTRY_STAFF].includes(user.role) && (
                    <th>Jumlah stok akhir seluruh layanan</th>
                  )}
                  <th>Jumlah stok digunakan {user.role != ROLE.PHARMA_STAFF && ' seluruh layanan'}</th>
                  {user.role == ROLE.LAB_STAFF && (
                    <React.Fragment>
                      {/* <th>Jumlah stok yang digunakan untuk kalibrasi/kontrol</th> */}
                      <th>Jumlah stok yang terbuang, akibat gangguan teknis dan sebab lainnya</th>
                    </React.Fragment>
                  )}
                </tr>
              </thead>
              <tbody>
                {komoditasLabFinalData &&
                  komoditasLabFinalData.content.map((content, index) => {
                    let title = "";

                    if (lastTitle !== content.title) {
                      title = content.title;
                      lastTitle = content.title;
                    }

                    let view = (
                      <React.Fragment key={content}>
                        <tr>
                          <td>{index + 1}</td>
                          <td>{title}</td>
                          <td>{content.komoditas}</td>
                          <td>{content.sumber_dana}</td>
                          <td>{content.satuan}</td>
                          <td>{content.stok_akhir}</td>
                          {[ROLE.MINISTRY_STAFF].includes(user.role) && (
                            <td>{content.stok_akhir_provinsi}</td>
                          )}
                          {[ROLE.PROVINCE_STAFF, ROLE.MINISTRY_STAFF].includes(user.role) && (
                            <td>{content.stok_akhir_kabkota}</td>
                          )}
                          {[ROLE.SUDIN_STAFF, ROLE.PROVINCE_STAFF, ROLE.MINISTRY_STAFF].includes(user.role) && (
                            <td>{content.stok_akhir_layanan}</td>
                          )}
                          {/* <td>{content.jumlah_stok_digunakan + (user.role == ROLE.LAB_STAFF ? 0 : (content.kalibrasi + content.jumlah_stok_terbuang))}</td> */}
                          <td>{content.jumlah_stok_digunakan + (user.role == ROLE.LAB_STAFF ? 0 : content.jumlah_stok_terbuang)}</td>
                          {user.role == ROLE.LAB_STAFF && (
                            <React.Fragment>
                              {/* <td>{content.kalibrasi}</td> */}
                              <td>{content.jumlah_stok_terbuang}</td>
                            </React.Fragment>
                          )}
                        </tr>
                        {komoditasLab.content.length === 0 && (
                          <tr>
                            <td colSpan="9" className="text-center">
                              Data Kosong
                            </td>
                          </tr>
                        )}
                      </React.Fragment>
                    )

                    return view;
                  })
                }
                {isLoading && (
                  <tr>
                    <td colSpan="6" className="text-center">
                      Loading...
                    </td>
                  </tr>
                )}
              </tbody>
            </Table>

            <Typography
              fontSize="15px"
              fontWeight="bold"
              className="d-block my-3 text-center"
            >
              Laporan stok logistik NON-ARV
            </Typography>
            <Table bordered responsive striped style={{ fontSize: 12 }}>
              <thead>
                <tr>
                  <th rowSpan="2">Jenis Komoditas</th>
                  <th>Nama Merk/Dagang</th>
                  <th>Satuan</th>
                  <th>Tgl kadaluarsa</th>
                  <th>Stok awal bulan ini</th>
                  <th>Jumlah yang diterima bulan ini</th>
                  <th>Jumlah yang dipakai bulan ini</th>
                  <th>Jumlah yang rusak/kadaluarsa</th>
                  <th>Selisih stok fisik</th>
                  <th>Jumlah akhir bulan</th>
                  <th>Jumlah kebutuhan bulan berikutnya</th>
                  <th>Sumber dana</th>
                </tr>
                <tr>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td>(A)</td>
                  <td>(B)</td>
                  <td>(C)</td>
                  <td>(E)</td>
                  <td>(F)</td>
                  <td>(G) = (A+b)-(C+E)+F</td>
                  <td>(H) = (Multiplier x C) - G</td>
                  <td></td>
                </tr>
              </thead>
              <tbody>
                {!isLoading &&
                  inventoryTable &&
                  inventoryTable.content.map((value, idx) => (
                    <tr key={idx}>
                      <td>{value.data.jenis_komoditas}</td>
                      <td>{value.data.merk_dagang}</td>
                      <td>{value.data.satuan}</td>
                      <td>{value.data.tgl_kadaluarsa}</td>
                      <td>{formatNumber(value.data.column_A)}</td>
                      <td>{formatNumber(value.data.column_B)}</td>
                      <td>{formatNumber(value.data.column_C)}</td>
                      <td>{formatNumber(value.data.column_E)}</td>
                      <td>{formatNumber(value.data.column_F)}</td>
                      <td>{formatNumber(value.data.column_G)}</td>
                      <td>{formatNumber(value.data.column_H)}</td>
                      <td>{value.data.sumber_dana}</td>
                    </tr>
                  ))}
                {isLoading && (
                  <tr>
                    <td colSpan="15" className="text-center">
                      Loading...
                    </td>
                  </tr>
                )}
              </tbody>
            </Table>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};
