import React, { useEffect, useState, useContext } from "react";
import GlobalContext from "containers/GlobalContext";
import API from "utils/API";
import axios from "axios";
import { ENUM } from "constant";
import moment from "moment";

import { RegisterLab } from "./view";

const emptyState = {};

const Handler = () => {
  let source = axios.CancelToken.source();
  const user = useContext(GlobalContext);

  const defaultFilter = {
    PROVINCE: [
      ENUM.LOGISTIC_ROLE.PROVINCE_ENTITY,
      ENUM.LOGISTIC_ROLE.SUDIN_ENTITY,
      ENUM.LOGISTIC_ROLE.UPK_ENTITY,
      ENUM.LOGISTIC_ROLE.LAB_ENTITY
    ].includes(user.logisticrole)
      ? user.provinceId
      : "",
    SUDINKAB: [
      ENUM.LOGISTIC_ROLE.SUDIN_ENTITY,
      ENUM.LOGISTIC_ROLE.UPK_ENTITY,
      ENUM.LOGISTIC_ROLE.LAB_ENTITY
    ].includes(user.logisticrole)
      ? user.sudinKabKotaId
      : "",
    UPK: [
      ENUM.LOGISTIC_ROLE.UPK_ENTITY,
      ENUM.LOGISTIC_ROLE.LAB_ENTITY
    ].includes(user.logisticrole) ? user.upkId : "",
    MONTH: moment().format('YYYY-MM')
  };

  const [data, setData] = useState(emptyState);
  const [isLoading, setLoading] = useState(false);

  const [filter, setFilter] = useState(defaultFilter);

  const _handleFilter = (type, value) => {
    if (typeof value === "undefined") {
      value = "";
    }
    setFilter({
      ...filter,
      [type]: value
    });
  };

  const _loadData = () => {
    setLoading(true);
    API.Report.registerLab(
      source.token,
      filter.PROVINCE,
      filter.SUDINKAB,
      filter.UPK,
      filter.MONTH
    )
      .then(rsp => {
        setData(rsp || emptyState);
        setLoading(false);
      })
      .catch(e => {
        setLoading(false);
        console.error("e: ", e);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  useEffect(() => {
    _loadData();

    return () => {
      source.cancel("request canceled");
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [filter]);

  return (
    <RegisterLab
      data={data}
      isLoading={isLoading}
      onFilter={_handleFilter}
      currentFilter={filter}
    />
  );
};

export default Handler;
