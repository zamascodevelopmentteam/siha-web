import React, { useEffect, useState, useContext } from "react";
import GlobalContext from "containers/GlobalContext";
import API from "utils/API";
import axios from "axios";
import { ENUM, ROLE } from "constant";

import View from "./view";
import { BASE_URL } from "utils/request";
import moment from "moment";

const emptyState = {
  content: {}
};

const Handler = () => {
  let source = axios.CancelToken.source();
  const user = useContext(GlobalContext);

  const defaultFilter = {
    PROVINCE: [
      ENUM.LOGISTIC_ROLE.PROVINCE_ENTITY,
      ENUM.LOGISTIC_ROLE.SUDIN_ENTITY,
      ENUM.LOGISTIC_ROLE.UPK_ENTITY
    ].includes(user.logisticrole)
      ? user.provinceId
      : "",
    SUDINKAB: [
      ENUM.LOGISTIC_ROLE.SUDIN_ENTITY,
      ENUM.LOGISTIC_ROLE.UPK_ENTITY
    ].includes(user.logisticrole)
      ? user.sudinKabKotaId
      : "",
    UPK: user.logisticrole === ENUM.LOGISTIC_ROLE.UPK_ENTITY ? user.upkId : "",
    MONTH: ""
  };

  const [data, setData] = useState(emptyState);
  const [isLoading, setLoading] = useState(false);
  const [filter, setFilter] = useState(defaultFilter);

  const _handleFilter = (type, value) => {
    if (typeof value === "undefined") {
      value = "";
    }
    setFilter({
      ...filter,
      [type]: value
    });
  };

  const _loadData = async () => {
    setLoading(true);
    let role = user.role
    console.log(role);
    await API.Report.hivRecap(
      source.token,
      filter.PROVINCE ? filter.PROVINCE : (role === ROLE.PROVINCE_STAFF ? user.provinceId : ''),
      filter.SUDINKAB ? filter.SUDINKAB : (role === ROLE.SUDIN_STAFF ? user.sudinKabKotaId : ''),
      filter.UPK ? filter.UPK : (role === ROLE.RR_STAFF ? user.upkId : ''),
      !filter.MONTH.length ? moment().format('YYYY-MM') : `${filter.MONTH}`
    )
      .then(rsp => {
        setData(rsp || emptyState);
        setLoading(false);
      })
      .catch(e => {
        setLoading(false);
        console.error("e: ", e);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const _handleDownloadExcel = async () => {
    try {
      setLoading(true);
      let role = user.role
      await API.Report.hivRecap(
        source.token,
        filter.PROVINCE ? filter.PROVINCE : (role === ROLE.PROVINCE_STAFF ? user.provinceId : ''),
        filter.SUDINKAB ? filter.SUDINKAB : (role === ROLE.SUDIN_STAFF ? user.sudinKabKotaId : ''),
        filter.UPK ? filter.UPK : (role === ROLE.RR_STAFF ? user.upkId : ''),
        !filter.MONTH.length ? moment().format('YYYY-MM') : `${filter.MONTH}`,
        true
      )
        .then(res => {
          window.open(`${BASE_URL}/${res.data.split('/').slice(2).join('/')}`);
          setLoading(false);
        })
        .catch(e => {
          setLoading(false);
          console.error("e: ", e);
        });
    } catch (ex) {
      console.log(ex)
    }
  }

  useEffect(() => {
    _loadData();

    return () => {
      source.cancel("request canceled");
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [filter]);

  return (
    <View
      data={data}
      isLoading={isLoading}
      onFilter={_handleFilter}
      currentFilter={filter}
      onDownloadExcel={_handleDownloadExcel}
    />
  );
};

export default Handler;
