import React from "react";
// import moment from "moment";

import { Spin } from "antd";
import { Table } from "reactstrap";
import { Typography, Filter } from "components";
import { formatNumber } from "utils";
import './style.css';

const View = ({ data, isLoading, onFilter, currentFilter, dataById, onDownloadExcel }) => {
  const meta = data.meta;
  let total = [];

  const monthNames = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Augustus", "September", "Oktober", "November", "Desember"];
  let dateObj = new Date();
  let month = monthNames[dateObj.getMonth()];
  let day = String(dateObj.getDate()).padStart(2, '0');
  let year = dateObj.getFullYear();
  let output = day + ' ' + month + ' ' + year;

  let month2 = null, year2 = null;
  if (currentFilter && currentFilter.MONTH !== "") {
    month2 = parseInt(currentFilter.MONTH.split('-')[1]) - 1;
    year2 = currentFilter.MONTH.split('-')[0];
  } else {
    month2 = dateObj.getMonth();
    year2 = year;
  }

  return (
    <React.Fragment>
      <div className="container h-100 pt-4">
        <div className="row mb-2">
          <div className="col-12 bg-white rounded p-4 border">
            <div className="mb-2">
              <Filter
                onFilter={onFilter}
                // disabledDate={current =>
                //   current &&
                //   current.format("YYYY-MM") !== moment().format("YYYY-MM")
                // }
                currentFilter={currentFilter}
                excel
                onDownloadExcel={onDownloadExcel}
              />
            </div>
          </div>
        </div>
        <div
          className="row bg-white rounded p-1 border mb-1"
          style={{ fontSize: 12 }}
        >
          <div className="col-12">
            {isLoading && (
              <div className="text-center">
                <Spin />
              </div>
            )}
          </div>
          <div className="col-6">
            <dl className="dl-horizontal">
              <dt>Nama UPK</dt>
              <dd>{meta && meta.upk && meta.upk.name ? meta.upk.name : "-"}</dd>
              <dt>Nama Kab/Kota</dt>
              <dd>
                {meta && meta.sudinKabKota && meta.sudinKabKota.name ? meta.sudinKabKota.name : "-"}
              </dd>
              <dt>Nama Provinsi</dt>
              <dd>{meta && meta.province && meta.province.name ? meta.province.name : "-"}</dd>
            </dl>
          </div>
          <div className="col-6">
            <dl className="dl-horizontal">
              <dt>Bulan</dt>
              <dd>{month2 !== null ? monthNames[month2] : (meta && meta.bulan ? meta.bulan : "-")}</dd>
              <dt>Tahun</dt>
              <dd>{year2 !== null ? year2 : (meta && meta.tahun ? meta.tahun : "-")}</dd>
              <dt>Tanggal Akses</dt>
              {/* <dd>{meta && meta.tanggal ? meta.tanggal : "-"}</dd> */}
              <dd>{output}</dd>
            </dl>
          </div>
        </div>
        <div className="row">
          <div className="col-12 bg-white rounded p-4 border mb-5">
            <Typography
              fontSize="15px"
              fontWeight="bold"
              className="d-block mb-3 text-center"
            >
              Laporan HIV
            </Typography>
            <Table bordered responsive style={{ fontSize: 12 }}>
              <thead>
                <tr>
                  <th className={'sticky-td bg-light'} rowSpan="2" style={{ paddingRight: "250px" }}>
                    Indikator
                  </th>
                  <th className={'bg-light'} colSpan="11">Laki-laki</th>
                  <th className={'bg-light'} colSpan="11">Perempuan</th>
                  <th className={'bg-light'} rowSpan="2">Total</th>
                </tr>
                <tr>
                  <th className={'bg-light'}>&#60; 1</th>
                  <th className={'bg-light'}>1-14</th>
                  <th className={'bg-light'}>15-19</th>
                  <th className={'bg-light'}>20-24</th>
                  <th className={'bg-light'}>25-29</th>
                  <th className={'bg-light'}>30-34</th>
                  <th className={'bg-light'}>35-39</th>
                  <th className={'bg-light'}>40-44</th>
                  <th className={'bg-light'}>45-49</th>
                  <th className={'bg-light'}>&gt; 50</th>
                  <th className={'bg-light'}>Jumlah</th>
                  <th className={'bg-light'}>&#60; 1</th>
                  <th className={'bg-light'}>1-14</th>
                  <th className={'bg-light'}>15-19</th>
                  <th className={'bg-light'}>20-24</th>
                  <th className={'bg-light'}>25-29</th>
                  <th className={'bg-light'}>30-34</th>
                  <th className={'bg-light'}>35-39</th>
                  <th className={'bg-light'}>40-44</th>
                  <th className={'bg-light'}>45-49</th>
                  <th className={'bg-light'}>&gt; 50</th>
                  <th className={'bg-light'}>Jumlah</th>
                </tr>
              </thead>
              {/* {dataById && Object.keys(dataById.content).map((key, idx) => {
                if (total[idx] === undefined) {
                  total[idx] = [];
                }
                dataById.content[key].data.map((value) => {
                  value.data.slice(0, 10).map((valueCount, idxCount) => {
                    if (total[idx][0] === undefined) {
                      total[idx][0] = [];
                    }
                    if (total[idx][0][idxCount] === undefined) {
                      total[idx][0][idxCount] = 0;
                    }
                    total[idx][0][idxCount] += valueCount;
                    return TextTrackCueList;
                  });
                  value.data.slice(10, 20).map((valueCount, idxCount) => {
                    if (total[idx][1] === undefined) {
                      total[idx][1] = [];
                    }
                    if (total[idx][1][idxCount] === undefined) {
                      total[idx][1][idxCount] = 0;
                    }
                    total[idx][1][idxCount] += valueCount;
                    return TextTrackCueList;
                  })
                  return true;
                })
                return true;
              })} */}
              <tbody>
                {Object.keys(data.content).map((key, idx) => {
                  let granTotalMale = 0;
                  let granTotalFemale = 0;
                  let sumMaleContent = 0;
                  let sumFemaleContent = 0;
                  return (
                    <React.Fragment key={idx}>
                      <tr>
                        <th className={'sticky-td bg-light'}>{data.content[key].title}</th>
                        {data.content[key] && data.content[key].total.slice(0, 10).map(v => {
                          granTotalMale += v;
                          return (
                            <th>{v}</th>
                          )
                        })}
                        <th>{granTotalMale}</th>
                        {data.content[key] && data.content[key].total.slice(10, 20).map(v => {
                          granTotalFemale += v;
                          return (
                            <th>{v}</th>
                          )
                        })}
                        <th>{granTotalFemale}</th>
                        <th>{granTotalMale + granTotalFemale}</th>
                      </tr>
                      {data.content[key].data.map((value, idxData) => {
                        let sumMale = 0;
                        let sumFemale = 0;

                        return (
                          <tr key={idxData}>
                            <td className={'sticky-td bg-light'}>{value.label}</td>
                            {value.data
                              .slice(0, 10)
                              .map((valueCount, idxCount) => {
                                sumMale = sumMale + valueCount;
                                sumMaleContent = sumMaleContent + valueCount;
                                return (
                                  <td key={idxCount} className={!["WPS", "Bumil"].includes(value.value) ? '' : 'bg-light'}>
                                    {!["WPS", "Bumil"].includes(value.value) ? formatNumber(valueCount) : ''}
                                  </td>
                                );
                              })}
                            <td className={!["WPS", "Bumil"].includes(value.value) ? '' : 'bg-light'}>{!["WPS", "Bumil"].includes(value.value) ? formatNumber(sumMale) : ''}</td>
                            {value.data
                              .slice(10, 20)
                              .map((valueCount, idxCount) => {
                                sumFemale = sumFemale + valueCount;
                                sumFemaleContent = sumFemaleContent + valueCount;
                                return (
                                  <td key={idxCount} className={!["Waria", "LSL"].includes(value.value) ? '' : 'bg-light'}>
                                    {!["Waria", "LSL"].includes(value.value) ? formatNumber(valueCount) : ''}
                                  </td>
                                );
                              })}
                            <td className={!["Waria", "LSL"].includes(value.value) ? '' : 'bg-light'}>{!["Waria", "LSL"].includes(value.value) ? formatNumber(sumFemale) : ''}</td>
                            <td>{formatNumber(sumMale + sumFemale)}</td>
                          </tr>
                        );
                      })}
                      <tr>
                        <th className={'sticky-td bg-light'}></th>
                        <th colSpan="10">Jumlah Kasus Laki-laki Berdasarkan Kelompok Populasi</th>
                        <th>{sumMaleContent}</th>
                        <th colSpan="10">Jumlah Kasus Perempuan Berdasarkan Kelompok Populasi</th>
                        <th>{sumFemaleContent}</th>
                        <th>{sumMaleContent + sumFemaleContent}</th>
                      </tr>
                    </React.Fragment>
                  );
                })}
              </tbody>
            </Table>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default View;
