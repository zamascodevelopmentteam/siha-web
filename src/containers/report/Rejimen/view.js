import React, { useContext } from 'react'
import GlobalContext from "containers/GlobalContext";
import { Typography, Filter, DisplayDate } from "components"
import { Spin } from "antd";
import { Table } from "reactstrap";
import { ROLE } from "constant";

const RejimenView = props => {
    const user = useContext(GlobalContext);

    const meta = props.data.meta;
    let rejimenStandar = props.data.rejimenStandarTable;
    let rejimenLain = props.data.rejimenLainTable;

    const monthNames = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Augustus", "September", "Oktober", "November", "Desember"];
    let dateObj = new Date();
    let month = monthNames[dateObj.getMonth()];
    let day = String(dateObj.getDate()).padStart(2, '0');
    let year = dateObj.getFullYear();
    let output = day + ' ' + month + ' ' + year;

    let month2 = null, year2 = null;
    if (props.currentFilter && props.currentFilter.MONTH !== "") {
        month2 = parseInt(props.currentFilter.MONTH.split('-')[1]) - 1;
        year2 = props.currentFilter.MONTH.split('-')[0];
    }

    let dws = 0, ank = 0;

    if (rejimenStandar) {
        Object.keys(rejimenStandar.content).map((key, idx) => {
            rejimenStandar.content[key].regimentList = rejimenStandar.content[key].regimentList.filter(v => {
                if (v.regiment.createdByData) {
                    if (user.role == ROLE.MINISTRY_STAFF) {
                        return true;
                    }
                    if (user.role == ROLE.PROVINCE_STAFF) {
                        if (v.regiment.createdByData.province) {
                            const province = v.regiment.createdByData.province.id;
                            return user.provinceId === province;
                        }
                    }
                    if (user.role == ROLE.SUDIN_STAFF) {
                        if (v.regiment.createdByData.sudinKabKota) {
                            const kabupaten = v.regiment.createdByData.sudinKabKota.id;
                            return user.sudinKabKotaId === kabupaten;
                        }
                    }
                    if (user.role == ROLE.PHARMA_STAFF) {
                        if (v.regiment.createdByData.upk) {
                            const upk = v.regiment.createdByData.upk.id;
                            return user.upkId === upk;
                        }
                    }
                }
                return false;
            })
        })
    }

    if (rejimenLain) {
        rejimenLain.content.regimentList = rejimenLain.content.regimentList.filter(v => {
            if (v.regiment.createdByData) {
                if (user.role == ROLE.MINISTRY_STAFF) {
                    return true;
                }
                if (user.role == ROLE.PROVINCE_STAFF) {
                    if (v.regiment.createdByData.province) {
                        const province = v.regiment.createdByData.province.id;
                        return user.provinceId === province;
                    }
                }
                if (user.role == ROLE.SUDIN_STAFF) {
                    if (v.regiment.createdByData.sudinKabKota) {
                        const kabupaten = v.regiment.createdByData.sudinKabKota.id;
                        return user.sudinKabKotaId === kabupaten;
                    }
                }
                if (user.role == ROLE.PHARMA_STAFF) {
                    if (v.regiment.createdByData.upk) {
                        const upk = v.regiment.createdByData.upk.id;
                        return user.upkId === upk;
                    }
                }
            }
            return false;
        });
    }


    return (
        <React.Fragment>
            <div className="container h-100 pt-4">
                <div className="row mb-2">
                    <div className="col-12 bg-white rounded p-4 border">
                        <div className="col-12 p-0">
                            <Filter
                                onFilter={props.onFilter}
                                currentFilter={props.currentFilter}
                                excel
                                onDownloadExcel={props.onDownloadExcel}
                            />
                        </div>
                    </div>
                </div>
                <div className="row bg-white rounded p-1 border mb-2" style={{ fontSize: 12 }}>
                    <div className="col-12">
                        {props.isLoading && (
                            <div className="text-center">
                                <Spin />
                            </div>
                        )}
                    </div>
                    <div className="col-6 ">
                        <dl className="dl-horizontal">
                            <dt>Nama UPK</dt>
                            <dd>{meta && meta.upk.name ? meta.upk.name : "-"}</dd>
                            <dt>Nama Kab/Kota</dt>
                            <dd>
                                {meta && meta.sudinKabKota.name ? meta.sudinKabKota.name : "-"}
                            </dd>
                            <dt>Nama Provinsi</dt>
                            <dd>{meta && meta.province.name ? meta.province.name : "-"}</dd>
                        </dl>
                    </div>
                    <div className="col-6">
                        <dl className="dl-horizontal">
                            <dt>Bulan</dt>
                            <dd>{month2 !== null ? monthNames[month2] : (meta && meta.bulan ? meta.bulan : "-")}</dd>
                            <dt>Tahun</dt>
                            <dd>{year2 !== null ? year2 : (meta && meta.tahun ? meta.tahun : "-")}</dd>
                            <dt>Tanggal Akses</dt>
                            <dd>{output}</dd>
                        </dl>
                    </div>
                </div>
                <div className="row">
                    <div className="col-12 bg-white rounded p-4 border mb-5">
                        <Typography
                            fontSize="15px"
                            fontWeight="bold"
                            className="d-block mb-3 text-center"
                        >
                            Laporan Rejimen
                        </Typography>
                        <Table bordered responsive striped style={{ fontSize: 12 }}>
                            <thead>
                                <tr>
                                    <th>Tahun</th>
                                    <th>Bulan</th>
                                    <th>Povinsi</th>
                                    <th>Kabupaten</th>
                                    <th>Upk</th>
                                    <th>Kode Upk</th>
                                    <th>Dewasa</th>
                                    <th>Anak</th>
                                    <th>Total</th>
                                    <th>Rejimen</th>
                                    <th>Lini</th>
                                    <th>tipe Rejimen</th>
                                </tr>
                            </thead>
                            <tbody>
                                {rejimenStandar && Object.keys(rejimenStandar.content).map((key, idx) => {
                                    return rejimenStandar.content[key].regimentList.map(v => {
                                        let province = "-";
                                        let kabupaten = "-";
                                        let upk = "-";
                                        let upkCode = "-";
                                        let dewasa = 0;
                                        let anak = 0;

                                        if (v.regiment.createdByData) {
                                            if (v.regiment.createdByData.province) {
                                                province = v.regiment.createdByData.province.name;
                                            }
                                            if (v.regiment.createdByData.sudinKabKota) {
                                                kabupaten = v.regiment.createdByData.sudinKabKota.name;
                                            }
                                            if (v.regiment.createdByData.upk) {
                                                upk = v.regiment.createdByData.upk.name;
                                                upkCode = v.regiment.createdByData.upk.codeId;
                                            }
                                        }

                                        dewasa += v.data.pasienMultiMonth.dewasa;
                                        dewasa += v.data.pasienReguler.dewasa;
                                        dewasa += v.data.pasienTransit.dewasa;

                                        anak += v.data.pasienMultiMonth.anak;
                                        anak += v.data.pasienReguler.anak;
                                        anak += v.data.pasienTransit.anak;

                                        if (dewasa + anak) {
                                            dws += dewasa;
                                            ank += anak;

                                            return (
                                                <tr key={idx + v.regiment.id}>
                                                    <td>{<DisplayDate date={v.regiment.createdAt} format="YYYY" />}</td>
                                                    <td>{<DisplayDate date={v.regiment.createdAt} format="MMM" />}</td>
                                                    <td>{province}</td>
                                                    <td>{kabupaten}</td>
                                                    <td>{upk}</td>
                                                    <td>{upkCode}</td>
                                                    <td>{dewasa}</td>
                                                    <td>{anak}</td>
                                                    <td>{dewasa + anak}</td>
                                                    <td>{v.regiment.combinationStrInfo}</td>
                                                    <td>{rejimenStandar.content[key].title}</td>
                                                    <td>Rejimen Standar</td>
                                                </tr>
                                            )
                                        }
                                    });
                                })}
                                {rejimenLain && rejimenLain.content.regimentList.map((v, idx) => {
                                    let province = "-";
                                    let kabupaten = "-";
                                    let upk = "-";
                                    let upkCode = "-";
                                    let dewasa = 0;
                                    let anak = 0;

                                    if (v.regiment.createdByData) {
                                        if (v.regiment.createdByData.province) {
                                            province = v.regiment.createdByData.province.name;
                                        }
                                        if (v.regiment.createdByData.sudinKabKota) {
                                            kabupaten = v.regiment.createdByData.sudinKabKota.name;
                                        }
                                        if (v.regiment.createdByData.upk) {
                                            upk = v.regiment.createdByData.upk.name;
                                            upkCode = v.regiment.createdByData.upk.codeId;
                                        }
                                    }

                                    dewasa += v.data.pasienMultiMonth.dewasa;
                                    dewasa += v.data.pasienReguler.dewasa;
                                    dewasa += v.data.pasienTransit.dewasa;

                                    anak += v.data.pasienMultiMonth.anak;
                                    anak += v.data.pasienReguler.anak;
                                    anak += v.data.pasienTransit.anak;

                                    if (dewasa + anak) {
                                        dws += dewasa;
                                        ank += anak;

                                        return (
                                            <tr key={idx}>
                                                <td>{<DisplayDate date={v.regiment.createdAt} format="YYYY" />}</td>
                                                <td>{<DisplayDate date={v.regiment.createdAt} format="MMM" />}</td>
                                                <td>{province}</td>
                                                <td>{kabupaten}</td>
                                                <td>{upk}</td>
                                                <td>{upkCode}</td>
                                                <td>{dewasa}</td>
                                                <td>{anak}</td>
                                                <td>{dewasa + anak}</td>
                                                <td>{v.regiment.combinationStrInfo}</td>
                                                <td>-</td>
                                                <td>Rejimen Non Standar</td>
                                            </tr>
                                        )
                                    }
                                })}
                                <tr>
                                    <td colSpan={6}><b>Total</b></td>
                                    <td>{dws}</td>
                                    <td>{ank}</td>
                                    <td>{dws + ank}</td>
                                    <td colSpan={4}></td>
                                </tr>
                            </tbody>
                        </Table>
                    </div>
                </div>
            </div>
        </React.Fragment>
    )
}

export default RejimenView