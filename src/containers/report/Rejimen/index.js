import React from 'react'
import Rejimen from './view'
import { ENUM } from "constant";
import GlobalContext from "containers/GlobalContext";
import axios from "axios";
import API from "utils/API";
import { BASE_URL } from "utils/request";

const emptyState = {
    rejimenStandarTable: {
        content: {},
        footer: {
            totalPasienRejimenStandar: {
                pasienReguler: {
                    dewasa: 0,
                    anak: 0
                },
                pasienTransit: {
                    dewasa: 0,
                    anak: 0
                },
                pasienMultiMonth: {
                    dewasa: 0,
                    anak: 0
                }
            },
            totalPasien: {
                pasienReguler: {
                    dewasa: 0,
                    anak: 0
                },
                pasienTransit: {
                    dewasa: 0,
                    anak: 0
                },
                pasienMultiMonth: {
                    dewasa: 0,
                    anak: 0
                }
            }
        }
    },
    rejimenLainTable: {
        content: {
            title: "Rejimen Lain",
            regimentList: []
        },
        footer: {
            totalPasienRejimenLain: {
                pasienReguler: {
                    dewasa: 0,
                    anak: 0
                },
                pasienTransit: {
                    dewasa: 0,
                    anak: 0
                },
                pasienMultiMonth: {
                    dewasa: 0,
                    anak: 0
                }
            }
        }
    },
    FDCTable: {
        content: [],
        footer: {
            jumlahTabDiperlukan: {
                pasienReguler: {
                    anak: 0,
                    jumlahTab: 0
                },
                pasienTransit: {
                    anak: 0,
                    jumlahTab: 0
                },
                pasienMultiMonth: {
                    anak: 0,
                    jumlahTab: 0
                }
            },
            jumlahTabDiperlukanBuffer: {
                pasienReguler: {
                    anak: 0,
                    jumlahTab: 0
                },
                pasienTransit: {
                    anak: 0,
                    jumlahTab: 0
                },
                pasienMultiMonth: {
                    anak: 0,
                    jumlahTab: 0
                }
            }
        }
    },
    inventoryTable: {
        content: [],
        footer: {}
    }
};

class RejimenIndex extends React.Component {
    static contextType = GlobalContext;
    user = this.context;
    source = axios.CancelToken.source();

    state = {
        filter: {
            PROVINCE: [
                ENUM.LOGISTIC_ROLE.PROVINCE_ENTITY,
                ENUM.LOGISTIC_ROLE.SUDIN_ENTITY,
                ENUM.LOGISTIC_ROLE.UPK_ENTITY
            ].includes(this.user.logisticrole) ? this.user.provinceId : "",
            SUDINKAB: [
                ENUM.LOGISTIC_ROLE.SUDIN_ENTITY,
                ENUM.LOGISTIC_ROLE.UPK_ENTITY
            ].includes(this.user.logisticrole) ? this.user.sudinKabKotaId : "",
            UPK: this.user.logisticrole === ENUM.LOGISTIC_ROLE.UPK_ENTITY ? this.user.upkId : "",
            MONTH: ""
        },
        isLoading: false,
        data: emptyState
    }

    filterHandler = async (type, value) => {
        if (typeof value === "undefined") {
            value = "";
        }
        await this.setState({
            filter: {
                ...this.state.filter,
                [type]: value
            }
        });
        this.loadData();
    };

    loadData = async () => {
        this.setState({ isLoading: true });
        await API.Report.lbpha2(
            this.source.token,
            this.state.filter.PROVINCE,
            this.state.filter.SUDINKAB,
            this.state.filter.UPK,
            this.state.filter.MONTH
        ).then(rsp => {
            this.setState({
                data: rsp || emptyState,
                isLoading: false
            })
        }).catch(e => {
            console.error("e: ", e);
            this.setState({ isLoading: false });
        }).finally(() => {
            this.setState({ isLoading: false });
        });
    };

    onDownloadExcel = async () => {
        try {
            this.setState({ isLoading: true });
            await API.Report.RegimenExcel(
                this.source.token,
                this.state.filter.PROVINCE,
                this.state.filter.SUDINKAB,
                this.state.filter.UPK,
                this.state.filter.MONTH
            ).then(res => {
                window.open(`${BASE_URL}/${res.data.split('/').slice(2).join('/')}`);
                this.setState({ isLoading: false });
            }).catch(e => {
                console.error("e: ", e);
                this.setState({ isLoading: false });
            });
        } catch (ex) {
            console.log(ex)
        }
    }

    componentDidMount() {
        this.loadData();
    }

    render() {
        return (
            <Rejimen
                onFilter={this.filterHandler}
                currentFilter={this.state.filter}
                isLoading={this.state.isLoading}
                data={this.state.data}
                onDownloadExcel={this.onDownloadExcel}
            />
        )
    }
}

export default RejimenIndex