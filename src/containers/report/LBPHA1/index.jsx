import React, { useState, useContext, useEffect } from "react";
import PerawatanHIV from "./PerawatanHIV";
import MasukART from "./MasukART";
import DampakART from "./DampakART";
import KoinfeksiTBHIV from "./KoinfeksiTBHIV";
import PencegahanKotrimoksazol from "./PencegahanKotrimoksazol";
import PencegahanTbc from "./PencegahanTbc";
import ViralLoad from "./ViralLoad";
import JumlahPenggunaanARV from "./JumlahPenggunaanARV";
import NotifikasiPasangan from "./NotifikasiPasangan";
import HIVIMS from "./HIVIMS";
import { Filter } from "components";
import { ENUM } from "constant";
import GlobalContext from "containers/GlobalContext";
// import moment from "moment";
import API from "utils/API";
import axios from "axios";
import { BASE_URL } from "utils/request";

const LBPHA1 = () => {
  const user = useContext(GlobalContext);

  const defaultFilter = {
    PROVINCE: [
      ENUM.LOGISTIC_ROLE.PROVINCE_ENTITY,
      ENUM.LOGISTIC_ROLE.SUDIN_ENTITY,
      ENUM.LOGISTIC_ROLE.UPK_ENTITY
    ].includes(user.logisticrole)
      ? user.provinceId
      : "",
    SUDINKAB: [
      ENUM.LOGISTIC_ROLE.SUDIN_ENTITY,
      ENUM.LOGISTIC_ROLE.UPK_ENTITY
    ].includes(user.logisticrole)
      ? user.sudinKabKotaId
      : "",
    UPK: user.logisticrole === ENUM.LOGISTIC_ROLE.UPK_ENTITY ? user.upkId : "",
    MONTH: ""
  };
  const [filter, setFilter] = useState(defaultFilter);
  const [meta, setMeta] = useState(null);

  const monthNames = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Augustus", "September", "Oktober", "November", "Desember"];
  let dateObj = new Date();
  let month = monthNames[dateObj.getMonth()];
  let day = String(dateObj.getDate()).padStart(2, '0');
  let year = dateObj.getFullYear();
  let output = day + ' ' + month + ' ' + year;

  let month2 = null, year2 = null;
  if (filter && filter.MONTH !== "") {
    month2 = parseInt(filter.MONTH.split('-')[1]) - 1;
    year2 = filter.MONTH.split('-')[0];
  }

  const _handleFilter = (type, value) => {
    if (typeof value === "undefined") {
      value = "";
    }
    setFilter({
      ...filter,
      [type]: value
    });
  };

  const _handleDownloadExcel = async () => {
    let source = axios.CancelToken.source();
    try {
      // setLoading(true);
      // const { meta: meta } = data;
      await API.Msiha.Lbpha1Excel(
        source.token,
        filter.PROVINCE,
        filter.SUDINKAB,
        filter.UPK,
        filter.MONTH
      )
        .then(res => {
          window.open(`${BASE_URL}/${res.data.split('/').slice(2).join('/')}`);
          // setLoading(false);
        })
        .catch(e => {
          // setLoading(false);
          console.error("e: ", e);
        });
    } catch (ex) {
      console.log(ex)
    }
  }

  const _getMetaReport = async () => {
    let source = axios.CancelToken.source();
    try {
      await API.Msiha.getMeta(
        source.token,
        filter.PROVINCE,
        filter.SUDINKAB,
        filter.UPK,
        filter.MONTH
      )
        .then(res => {
          setMeta(res.meta);
        })
        .catch(e => {
          console.error("e: ", e);
        });
    } catch (ex) {
      console.log(ex)
    }
  }

  useEffect(() => {
    let source = axios.CancelToken.source();
    _getMetaReport();

    return () => {
      source.cancel("request canceled");
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [filter])

  return (
    <div className="container h-100 pt-4">
      <div className="row">
        <div className="col-12 bg-white rounded p-4 border mb-1">
          <div className="pb-4 p-2">
            <Filter
              onFilter={_handleFilter}
              // disabledDate={current =>
              //   current &&
              //   current.format("YYYY-MM") !== moment().format("YYYY-MM")
              // }
              currentFilter={filter}
              excel
              onDownloadExcel={_handleDownloadExcel}
            />
          </div>
        </div>
        <div className="col-12 bg-white rounded p-4 border mb-1">
          <div className="row" style={{fontSize: 12}}>
            <div className="col-6">
              <dl className="dl-horizontal">
                <dt>Nama UPK</dt>
                <dd>{meta && meta.upk.name ? meta.upk.name : "-"}</dd>
                <dt>Nama Kab/Kota</dt>
                <dd>
                  {meta && meta.sudinKabKota.name ? meta.sudinKabKota.name : "-"}
                </dd>
                <dt>Nama Provinsi</dt>
                <dd>{meta && meta.province.name ? meta.province.name : "-"}</dd>
              </dl>
            </div>
            <div className="col-6">
              <dl className="dl-horizontal">
                <dt>Bulan</dt>
                <dd>{month2 !== null ? monthNames[month2] : (meta && meta.bulan ? meta.bulan : "-")}</dd>
                <dt>Tahun</dt>
                <dd>{year2 !== null ? year2 : (meta && meta.tahun ? meta.tahun : "-")}</dd>
                <dt>Tanggal Akses</dt>
                <dd>{output}</dd>
              </dl>
            </div>
          </div>
        </div>
        <div className="col-12 bg-white rounded p-4 border mb-5">
          <PerawatanHIV currentFilter={filter} />
          <MasukART currentFilter={filter} />
          <DampakART currentFilter={filter} />
          <KoinfeksiTBHIV currentFilter={filter} />
          <PencegahanKotrimoksazol currentFilter={filter} />
          <PencegahanTbc currentFilter={filter} />
          <ViralLoad currentFilter={filter} />
          <JumlahPenggunaanARV currentFilter={filter} />
          <NotifikasiPasangan currentFilter={filter} />
          <HIVIMS currentFilter={filter} />
        </div>
      </div>
    </div>
  );
};

export default LBPHA1;
