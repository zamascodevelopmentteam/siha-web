import React, { useEffect, useState } from "react";
import API from "utils/API";
import axios from "axios";

import { LBPHA1 } from "components";

const Handler = ({ currentFilter }) => {
  let source = axios.CancelToken.source();

  const [data, setData] = useState([]);
  const [isLoading, setLoading] = useState(false);

  const _loadData = () => {
    setLoading(true);
    API.Msiha.arvSpecial(
      source.token,
      currentFilter.PROVINCE,
      currentFilter.SUDINKAB,
      currentFilter.UPK,
      currentFilter.MONTH
    )
      .then(rsp => {
        setData(rsp.data || []);
        setLoading(false);
      })
      .catch(e => {
        setLoading(false);
        console.error("e: ", e);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  useEffect(() => {
    _loadData();

    return () => {
      source.cancel("request canceled");
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [currentFilter]);

  return (
    <LBPHA1
      data={data}
      isLoading={isLoading}
      title="8. Jumlah Penggunaan ARV Khusus"
    />
  );
};

export default Handler;
