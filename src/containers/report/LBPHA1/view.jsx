import React from "react";

import { Spin } from "antd";
import { Table } from "reactstrap";
import { Typography } from "components";
import { formatNumber } from "utils";
import { c } from "constant";
const { VARIABLE_TYPE } = c;

const View = ({ data, isLoading }) => {
  return (
    <React.Fragment>
      <div className="container h-100 pt-4">
        <div className="row">
          <div className="col-12 bg-white rounded p-4 border mb-5">
            {isLoading && (
              <div className="text-center">
                <Spin />
              </div>
            )}
            <Typography
              fontSize="15px"
              fontWeight="bold"
              className="d-block mb-3 text-center"
            >
              Judul
            </Typography>
            <Table bordered responsive striped style={{ fontSize: 12 }}>
              <thead className="text-center">
                <tr>
                  <th rowSpan="2" style={{ paddingRight: "300px" }}>
                    Variabel
                  </th>
                  <th colSpan="11">Laki-Laki</th>
                  <th colSpan="11">Perempuan</th>
                  <th colSpan="13">Kelompok Populasi Khusus</th>
                </tr>
                <tr>
                  <th>&#60; 1</th>
                  <th>1-14</th>
                  <th>15-19</th>
                  <th>20-24</th>
                  <th>25-29</th>
                  <th>30-34</th>
                  <th>35-39</th>
                  <th>40-44</th>
                  <th>45-49</th>
                  <th>{">"} 50</th>
                  <th>Jumlah</th>
                  <th>&#60; 1</th>
                  <th>1-14</th>
                  <th>15-19</th>
                  <th>20-24</th>
                  <th>25-29</th>
                  <th>30-34</th>
                  <th>35-39</th>
                  <th>40-44</th>
                  <th>45-49</th>
                  <th>{">"} 50</th>
                  <th>Jumlah</th>
                  <th>WPS</th>
                  <th>Waria</th>
                  <th>Penasun</th>
                  <th>LSL</th>
                  <th>Bumil</th>
                  <th>Pasien TB</th>
                  <th>Pasien IMS</th>
                  <th>Pasien Hepatitis</th>
                  <th>Pasangan ODHA</th>
                  <th>Anak ODHA</th>
                  <th>WBP</th>
                  <th>Lainnya</th>
                  <th>Total</th>
                </tr>
              </thead>
              {data.map((value, idx) => {
                const key = Object.keys(value)[0];
                return (
                  <tr key={idx}>
                    <td width="400px">{VARIABLE_TYPE[key]}</td>
                    {Object.keys(value[key]).map((key2, idx2) => (
                      <React.Fragment>
                        {value[key][key2].map((value2, idx3) => {
                          const num = value2.count || value2.jumlah;
                          return <td key={idx3}>{num || 0}</td>;
                        })}
                      </React.Fragment>
                    ))}
                  </tr>
                );
              })}
              {/* {Object.keys(data).map((key, index) => (
                <tr>
                  <td>{key}</td>
                </tr>
              ))} */}
            </Table>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default View;
// (key, idx2) => {
//   const num = value.count || value.jumlah;
//   return <td key={idx2}>{num}</td>;
// };
