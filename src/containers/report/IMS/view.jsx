import React from "react";
// import moment from "moment";

import { Spin } from "antd";
import { Table } from "reactstrap";
import { Typography, Filter } from "components";
// import { formatNumber } from "utils";
// import { ROLE } from '../../../constant'

import './style.css';

const View = ({ data, isLoading, onFilter, currentFilter, button, onDownloadExcel }) => {
  return (
    <React.Fragment>
      <div className="container h-100 pt-4">
        <div className="row mb-2">
          <div className="col-12 bg-white rounded p-4 border">
            <div className="mb-2">
              <Filter
                onFilter={onFilter}
                disableNextMonth={true}
                currentFilter={currentFilter}
                excel
                onDownloadExcel={onDownloadExcel}
              />
            </div>
          </div>
        </div>

        <div className="row">
          <div className="col-12 bg-white rounded p-4 border mb-5">
            {isLoading && (
              <div className="text-center">
                <Spin />
              </div>
            )}
            <Typography
              fontSize="15px"
              fontWeight="bold"
              className="d-block mb-3 text-center"
            >
              Laporan IMS
            </Typography>
            <Table bordered responsive style={{ fontSize: 12 }}>
              <thead>
                <tr>
                  <th className={'sticky-td bg-light'} rowSpan="2" style={{ paddingRight: "250px" }}>
                    Indikator
                  </th>
                  <th className={'bg-light'} colSpan="13">Perempuan</th>
                  {/* <th rowSpan="2">Total</th> */}
                  <th className={'bg-light'} colSpan="13">Laki-laki</th>
                  {/* <th rowSpan="2">Total</th> */}
                  {/* <th colSpan="12">Kelompok Populasi Khusus</th> */}
                  {/* <th rowSpan="2">Total</th> */}
                </tr>
                <tr>
                  <th className={'bg-light'}>{'< 1'}</th>
                  <th className={'bg-light'}>1-4</th>
                  <th className={'bg-light'}>5-14</th>
                  <th className={'bg-light'}>15-19</th>
                  <th className={'bg-light'}>20-24</th>
                  <th className={'bg-light'}>25-29</th>
                  <th className={'bg-light'}>30-34</th>
                  <th className={'bg-light'}>35-39</th>
                  <th className={'bg-light'}>40-44</th>
                  <th className={'bg-light'}>45-49</th>
                  <th className={'bg-light'}>50-59</th>
                  <th className={'bg-light'}>{">="} 60</th>
                  {/* <th className={'bg-light'}>>= 60</th> */}
                  <th className={'bg-light'}>Jumlah</th>

                  <th className={'bg-light'}>{'< 1'}</th>
                  <th className={'bg-light'}>1-4</th>
                  <th className={'bg-light'}>5-14</th>
                  <th className={'bg-light'}>15-19</th>
                  <th className={'bg-light'}>20-24</th>
                  <th className={'bg-light'}>25-29</th>
                  <th className={'bg-light'}>30-34</th>
                  <th className={'bg-light'}>35-39</th>
                  <th className={'bg-light'}>40-44</th>
                  <th className={'bg-light'}>45-49</th>
                  <th className={'bg-light'}>50-59</th>
                  <th className={'bg-light'}>{">="} 60</th>
                  {/* <th className={'bg-light'}>>= 60</th> */}
                  <th className={'bg-light'}>Jumlah</th>
                  {/* <th className={'bg-light'}>WPS</th>
                  <th className={'bg-light'}>Waria</th>
                  <th className={'bg-light'}>Penasun</th>
                  <th className={'bg-light'}>LSL</th>
                  <th className={'bg-light'}>Bumil</th>
                  <th className={'bg-light'}>Pasien TB</th>
                  <th className={'bg-light'}>Pasien IMS</th>
                  <th className={'bg-light'}>Pasien Hepatitis</th>
                  <th className={'bg-light'}>Pasangan ODHA</th>
                  <th className={'bg-light'}>Anak ODHA</th>
                  <th className={'bg-light'}>WBP</th>
                  <th className={'bg-light'}>Lainnya</th> */}
                </tr>
              </thead>
              <tbody>
                {data.map((v, i) => {
                  let hideLaki = false;
                  let hidePerempuan = false;
                  return (
                    <tr key={i}>
                      {
                        v.length ?
                          v.map((k, ii) => {
                            if (["Duh Tubuh Vagina", "Servisitis gonore", "Servisitis non gonore", "Vaginitis gonore", "Vaginitis non gonore"].includes(k)) {
                              hideLaki = true;
                            }
                            if (["Pembengkakan Skortum"].includes(k)) {
                              hidePerempuan = true;
                            }
                            return (
                              <React.Fragment>
                                <td className={(ii === 0 ? 'sticky-td bg-light ' + (v.length === 1 ? 'text-primary font-weight-bold h6 ' : '') : '') + (k ? '' : (hideLaki ? (ii >= 14 && ii <= 26 ? 'bg-light' : '') : (hidePerempuan ? (ii >= 1 && ii <= 13 ? 'bg-light' : '') : '')))} key={ii}>{k ? k : (hideLaki ? (ii >= 14 && ii <= 26 ? '' : k) : (hidePerempuan ? (ii >= 1 && ii <= 13 ? '' : k) : '0'))}</td>
                                {v.length === 1 && (
                                  <td colSpan={26}></td>
                                )}
                              </React.Fragment>
                            )
                          })
                          :
                          null
                      }
                    </tr>
                  )
                })}
                {/* {Object.keys(data.content).map((key, idx) => (
                  <React.Fragment key={idx}>
                    <tr>
                      <td colSpan="24">{data.content[key].title}</td>
                    </tr>
                    {data.content[key].data.map((value, idxData) => {
                      let sumMale = 0;
                      let sumFemale = 0;

                      return (
                        <tr key={idxData}>
                          <td>{value.label}</td>
                          {value.data
                            .slice(0, 10)
                            .map((valueCount, idxCount) => {
                              sumMale = sumMale + valueCount;
                              return (
                                <td key={idxCount}>
                                  {formatNumber(valueCount)}
                                </td>
                              );
                            })}
                          <td>{formatNumber(sumMale)}</td>
                          {value.data
                            .slice(10, 20)
                            .map((valueCount, idxCount) => {
                              sumFemale = sumFemale + valueCount;
                              return (
                                <td key={idxCount}>
                                  {formatNumber(valueCount)}
                                </td>
                              );
                            })}
                          <td>{formatNumber(sumFemale)}</td>
                          <td>{formatNumber(sumMale + sumFemale)}</td>
                        </tr>
                      ); */}
                {/* }
                    )}
                  </React.Fragment> */}
                {/* ))} */}
              </tbody>
            </Table>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default View;
