import React from "react";
// import moment from "moment";

import { Spin } from "antd";
import { Table } from "reactstrap";
import { Typography, Filter } from "components";
// import { formatNumber } from "utils";
// import './style.css';

const View = ({ data, isLoading, onFilter, currentFilter, onDownloadExcel }) => {
  const meta = data.meta;

  const monthNames = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Augustus", "September", "Oktober", "November", "Desember"];
  let dateObj = new Date();
  let month = monthNames[dateObj.getMonth()];
  let day = String(dateObj.getDate()).padStart(2, '0');
  let year = dateObj.getFullYear();
  let output = day + ' ' + month + ' ' + year;

  let month2 = null, year2 = null;
  if (currentFilter && currentFilter.MONTH !== "") {
    month2 = parseInt(currentFilter.MONTH.split('-')[1]) - 1;
    year2 = currentFilter.MONTH.split('-')[0];
  } else {
    month2 = dateObj.getMonth();
    year2 = year;
  }

  return (
    <React.Fragment>
      <div className="container h-100 pt-4">
        <div className="row mb-2">
          <div className="col-12 bg-white rounded p-4 border">
            <div className="mb-2">
              <Filter
                onFilter={onFilter}
                // disabledDate={current =>
                //   current &&
                //   current.format("YYYY-MM") !== moment().format("YYYY-MM")
                // }
                currentFilter={currentFilter}
              // excel
              // onDownloadExcel={onDownloadExcel}
              />
            </div>
          </div>
        </div>
        <div
          className="row bg-white rounded p-1 border mb-1"
          style={{ fontSize: 12 }}
        >
          <div className="col-12">
            {isLoading && (
              <div className="text-center">
                <Spin />
              </div>
            )}
          </div>
          <div className="col-6">
            <dl className="dl-horizontal">
              <dt>Nama UPK</dt>
              <dd>{meta && meta.upk && meta.upk.name ? meta.upk.name : "-"}</dd>
              <dt>Nama Kab/Kota</dt>
              <dd>
                {meta && meta.sudinKabKota && meta.sudinKabKota.name ? meta.sudinKabKota.name : "-"}
              </dd>
              <dt>Nama Provinsi</dt>
              <dd>{meta && meta.province && meta.province.name ? meta.province.name : "-"}</dd>
            </dl>
          </div>
          <div className="col-6">
            <dl className="dl-horizontal">
              <dt>Bulan</dt>
              <dd>{month2 !== null ? monthNames[month2] : (meta && meta.bulan ? meta.bulan : "-")}</dd>
              <dt>Tahun</dt>
              <dd>{year2 !== null ? year2 : (meta && meta.tahun ? meta.tahun : "-")}</dd>
              <dt>Tanggal Akses</dt>
              {/* <dd>{meta && meta.tanggal ? meta.tanggal : "-"}</dd> */}
              <dd>{output}</dd>
            </dl>
          </div>
        </div>
        <div className="row">
          <div className="col-12 bg-white rounded p-4 border mb-5">
            <Typography
              fontSize="15px"
              fontWeight="bold"
              className="d-block mb-3 text-center"
            >
              Laporan Penggunaan Tes VL dan CD4
            </Typography>
            <Table bordered responsive style={{ fontSize: 12 }}>
              <thead>
                <tr>
                  <th>Penggunaan</th>
                  <th>Satuan</th>
                  <th>Stok</th>
                  <th>Jumlah Reagen Yang Digunakan</th>
                  <th>Jumlah Reagen Yang Terbuang</th>
                </tr>
              </thead>
              <tbody>
                {data.content && Object.keys(data.content).map(k => (
                  <React.Fragment>
                    <tr>
                      <td colSpan="5" style={{fontSize: 13, fontWeight: "bold", color: "blue"}}>{k}</td>
                    </tr>
                    {Object.keys(data.content[k]).map(kk => (
                      <React.Fragment>
                        <tr>
                        <td colSpan="5" style={{fontSize: 13, fontWeight: "bold", color: "#33ccff"}}>{kk}</td>
                        </tr>
                        {data.content[k][kk].map(r => (
                          <tr>
                            <td>{r.name}</td>
                            <td>{r.stockUnitType}</td>
                            <td>{r.packageMultiplier}</td>
                            <td>{r.usedReagent}</td>
                            <td>{r.terbuang}</td>
                          </tr>
                        ))}
                      </React.Fragment>
                    ))}
                  </React.Fragment>
                ))}
              </tbody>
            </Table>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default View;
