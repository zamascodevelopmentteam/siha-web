import React from "react";

import { Spin } from "antd";
import { Table } from "reactstrap";
import { Typography, Filter } from "components";
// import { formatNumber } from "utils";
// import { ROLE } from '../../../constant'

const View = ({ data = [], isLoading, onDownloadExcel, onFilter, currentFilter }) => {
  return (
    <React.Fragment>
      <div className="container h-100 pt-4">
        <div className="row mb-2">
          <div className="col-12 bg-white rounded p-4 border">
            <div className="mb-2">
              {/* <Button
                onClick={onDownloadExcel}
                className={`hs-btn-outline ${ROLE.RR_STAFF}`}
                icon="download">
                Download Excel
              </Button> */}
              <Filter
                onFilter={onFilter}
                disableNextMonth={true}
                currentFilter={currentFilter}
                excel
                onDownloadExcel={onDownloadExcel}
              />
            </div>
          </div>
        </div>

        <div className="row">
          <div className="col-12 bg-white rounded p-4 border mb-5">
            {isLoading && (
              <div className="text-center">
                <Spin />
              </div>
            )}
            <Typography
              fontSize="15px"
              fontWeight="bold"
              className="d-block mb-3 text-center"
            >
              Laporan IMS
            </Typography>
            <Table bordered responsive striped style={{ fontSize: 12 }}>
              <thead>
                <tr>
                  <th rowSpan="2" style={{ paddingRight: "250px" }}>
                    Indikator
                  </th>
                </tr>
                <tr>
                  <th>WPS</th>
                  <th>LSL</th>
                  <th>Waria</th>
                  <th>Penasun Perempuan</th>
                  <th>Penasun Laki - Laki</th>
                  <th>Ibu Hamil</th>
                  <th>ODHA Perempuan</th>
                  <th>ODHA Laki - Laki</th>
                </tr>
              </thead>
              <tbody>
                {data.map((v, i) =>
                  <tr key={i}>
                    {v.length ?
                      v.map((k, i) => <td key={i}>{k ? k : ''}</td>)
                      :
                      Array(22).fill('-').map((k, i) => <td key={i}>{k ? k : ''}</td>)
                    }
                  </tr>
                )}
              </tbody>
            </Table>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default View;
