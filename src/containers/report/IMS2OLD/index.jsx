import React, { useEffect, useState, useContext } from "react";
import API from "utils/API";
import { BASE_URL } from "utils/request";
import GlobalContext from "../../GlobalContext";

import View from "./view";
import { ENUM, ROLE } from "constant";
import moment from "moment";

export default function () {
  const user = useContext(GlobalContext);

  const defaultFilter = {
    PROVINCE: [
      ENUM.LOGISTIC_ROLE.PROVINCE_ENTITY,
      ENUM.LOGISTIC_ROLE.SUDIN_ENTITY,
      ENUM.LOGISTIC_ROLE.UPK_ENTITY
    ].includes(user.logisticrole)
      ? user.provinceId
      : "",
    SUDINKAB: [
      ENUM.LOGISTIC_ROLE.SUDIN_ENTITY,
      ENUM.LOGISTIC_ROLE.UPK_ENTITY
    ].includes(user.logisticrole)
      ? user.sudinKabKotaId
      : "",
    UPK: undefined,
    MONTH: ""
  };

  const [data, setData] = useState([]);
  const [filter, setFilter] = useState(defaultFilter);

  const _handleFilter = (type, value) => {
    if (typeof value === "undefined") {
      value = "";
    }

    setFilter({
      ...filter,
      [type]: value
    });
  };

  async function _handleDownloadExcel() {
    try {
      let role = user.role;

      let res = await API.IMS.laporan2old(
        role === ROLE.RR_STAFF && user.upkId,
        role === ROLE.SUDIN_STAFF && user.sudinKabKotaId,
        role === ROLE.PROVINCE_STAFF && user.provinceId,
        !filter.MONTH.length ? moment().format('YYYY-MM') + "-01" : `${filter.MONTH}-01`,
        true
      );
      window.open(`${BASE_URL}/${res.data.split('/').slice(2).join('/')}`);
    } catch (ex) {
      console.log(ex)
    }
  }

  const _fetchData = async () => {
    try {
      console.log(filter);
      let role = user.role

      let res = await API.IMS.laporan2old(
        filter.UPK ? filter.UPK : (role === ROLE.RR_STAFF && user.upkId),
        filter.SUDINKAB ? filter.SUDINKAB : (role === ROLE.SUDIN_STAFF && user.sudinKabKotaId),
        filter.PROVINCE ? filter.PROVINCE : (role === ROLE.PROVINCE_STAFF && user.provinceId),
        !filter.MONTH.length ? moment().format('YYYY-MM') + "-01" : `${filter.MONTH}-01`
      );
      setData(res.data.data)
    } catch (ex) {
      console.log(ex)
    }
  }

  useEffect(() => {
    _fetchData();
  }, [filter]);

  return (
    <View
      data={data}
      isLoading={false}
      onDownloadExcel={_handleDownloadExcel}
      onFilter={_handleFilter}
      currentFilter={filter}
    />
  );
};
