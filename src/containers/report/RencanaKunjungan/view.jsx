import React from "react";
// import moment from "moment";

import { Spin } from "antd";
import { Table } from "reactstrap";
import { Typography, Filter, DisplayDate } from "components";

const View = ({ data, isLoading, onFilter, currentFilter, onDownloadExcel }) => {
  const meta = data.meta;

  const monthNames = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Augustus", "September", "Oktober", "November", "Desember"];
  let dateObj = new Date();
  let month = monthNames[dateObj.getMonth()];
  let day = String(dateObj.getDate()).padStart(2, '0');
  let year = dateObj.getFullYear();
  let output = day + ' ' + month + ' ' + year;

  let month2 = null, year2 = null;
  if (currentFilter && currentFilter.MONTH !== "") {
    month2 = parseInt(currentFilter.MONTH.split('-')[1]) - 1;
    year2 = currentFilter.MONTH.split('-')[0];
  } else {
    month2 = dateObj.getMonth();
    year2 = year;
  }

  let lastUpk;

  return (
    <React.Fragment>
      <div className="container h-100 pt-4">
        <div className="row mb-2">
          <div className="col-12 bg-white rounded p-4 border">
            <div className="mb-2">
              <Filter
                onFilter={onFilter}
                // disabledDate={current =>
                //   current &&
                //   current.format("YYYY-MM") !== moment().format("YYYY-MM")
                // }
                currentFilter={currentFilter}
                excel
                onDownloadExcel={onDownloadExcel}
              />
            </div>
          </div>
        </div>
        <div
          className="row bg-white rounded p-1 border mb-1"
          style={{ fontSize: 12 }}
        >
          <div className="col-12">
            {isLoading && (
              <div className="text-center">
                <Spin />
              </div>
            )}
          </div>
          <div className="col-6">
            <dl className="dl-horizontal">
              <dt>Nama UPK</dt>
              <dd>{meta && meta.upk && meta.upk.name ? meta.upk.name : "-"}</dd>
              <dt>Nama Kab/Kota</dt>
              <dd>
                {meta && meta.sudinKabKota && meta.sudinKabKota.name ? meta.sudinKabKota.name : "-"}
              </dd>
              <dt>Nama Provinsi</dt>
              <dd>{meta && meta.province && meta.province.name ? meta.province.name : "-"}</dd>
            </dl>
          </div>
          <div className="col-6">
            <dl className="dl-horizontal">
              <dt>Bulan</dt>
              <dd>{month2 !== null ? monthNames[month2] : (meta && meta.bulan ? meta.bulan : "-")}</dd>
              <dt>Tahun</dt>
              <dd>{year2 !== null ? year2 : (meta && meta.tahun ? meta.tahun : "-")}</dd>
              <dt>Tanggal Akses</dt>
              {/* <dd>{meta && meta.tanggal ? meta.tanggal : "-"}</dd> */}
              <dd>{output}</dd>
            </dl>
          </div>
        </div>
        <div className="row">
          <div className="col-12 bg-white rounded p-4 border mb-5">
            <Typography
              fontSize="15px"
              fontWeight="bold"
              className="d-block mb-3 text-center"
            >
              Laporan Rekap Rencana Kunjungan Odha
            </Typography>
            <Table bordered responsive striped style={{ fontSize: 12 }}>
              <thead>
                <tr>
                  <th>Provinsi</th>
                  <th>KabKota</th>
                  <th>Faskes</th>
                  <th>Kunjungan Bulan Sebelumnya</th>
                  <th>Rencana Berkunjung</th>
                  <th>Kunjungan Bulan ini</th>
                  <th>No Register Nasional</th>
                  <th>NIK</th>
                  <th>Nama</th>
                  <th>Jenis Kelamin</th>
                  <th>Tanggal Lahir</th>
                  <th>Tanggal Mulai ART</th>
                  <th>No HP</th>
                  <th>Nama PMO</th>
                  <th>No HP PMO</th>
                  <th>Keterangan</th>
                </tr>
              </thead>
              <tbody>
                {data && data.content.map(r => {
                  let showUpk = false;
                  if (lastUpk !== r.id_upk) {
                    lastUpk = r.id_upk;
                    showUpk = true;
                  }

                  return (
                    <tr>
                      <td>{showUpk ? r.provinsi : ''}</td>
                      <td>{showUpk ? r.kabkota : ''}</td>
                      <td>{showUpk ? r.upk : ''}</td>
                      <td>{r.kunjungan_sebelumnya ? <DisplayDate date={r.kunjungan_sebelumnya} /> : ''}</td>
                      <td>{r.rencana_kunjungan ? <DisplayDate date={r.rencana_kunjungan} /> : ''}</td>
                      <td>{r.kunjungan_bulan_ini ? <DisplayDate date={r.kunjungan_bulan_ini} /> : ''}</td>
                      <td>{r.no_register_nasional}</td>
                      <td>{r.nik}</td>
                      <td>{r.nama}</td>
                      <td>{r.jenis_kelamin}</td>
                      <td>{r.tgl_lahir}</td>
                      <td>{r.tanggal_mulai_art ? <DisplayDate date={r.tanggal_mulai_art} /> : ''}</td>
                      <td>{r.no_hp}</td>
                      <td>{r.nama_pmo}</td>
                      <td>{r.no_hp_pmo}</td>
                      <td></td>
                    </tr>
                  )
                })}
              </tbody>
            </Table>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default View;
