import React from "react";

import { Spin } from "antd";
import { Table } from "reactstrap";
import { Typography, Filter } from "components";
// import { formatNumber } from "utils";
// import { ROLE } from '../../../constant'

const View = ({ data = [], isLoading, onDownloadExcel, onFilter, currentFilter }) => {
  return (
    <React.Fragment>
      <div className="container h-100 pt-4">
        <div className="row mb-2">
          <div className="col-12 bg-white rounded p-4 border">
            <div className="mb-2">
              {/* <Button
                onClick={onDownloadExcel}
                className={`hs-btn-outline ${ROLE.RR_STAFF}`}
                icon="download">
                Download Excel
              </Button> */}
              <Filter
                onFilter={onFilter}
                disableNextMonth={true}
                currentFilter={currentFilter}
                excel
                onDownloadExcel={onDownloadExcel}
              />
            </div>
          </div>
        </div>

        <div className="row">
          <div className="col-12 bg-white rounded p-4 border mb-5">
            {isLoading && (
              <div className="text-center">
                <Spin />
              </div>
            )}
            <Typography
              fontSize="15px"
              fontWeight="bold"
              className="d-block mb-3 text-center"
            >
              Laporan IMS
            </Typography>
            <Table bordered responsive style={{ fontSize: 12 }}>
              <thead>
                <tr>
                  <th className={'bg-light'}>Indikator</th>
                  <th className={'bg-light'} width={'10%'}>WPS</th>
                  <th className={'bg-light'} width={'10%'}>LSL</th>
                  <th className={'bg-light'} width={'10%'}>Waria</th>
                  <th className={'bg-light'} width={'10%'}>Penasun Perempuan</th>
                  <th className={'bg-light'} width={'10%'}>Penasun Laki - Laki</th>
                  <th className={'bg-light'} width={'10%'}>Ibu Hamil</th>
                  <th className={'bg-light'} width={'10%'}>ODHA Perempuan</th>
                  <th className={'bg-light'} width={'10%'}>ODHA Laki - Laki</th>
                </tr>
              </thead>
              <tbody>
                {data.map((v, i) => {
                  let hideLaki = false;
                  // let hidePerempuan = false;
                  return (
                    <tr key={i}>
                      {
                        v.length ?
                          v.map((k, ii) => {
                            if (["Servisitis gonore", "Servisitis non gonore", "Vaginitis gonore", "Vaginitis non gonore"].includes(k)) {
                              hideLaki = true;
                            }
                            // if (["Pembengkakan Skortum"].includes(k)) {
                            //   hidePerempuan = true;
                            // }
                            return (
                              <td key={ii} className={(ii === 0 ? 'bg-light' : (k ? '' : (hideLaki ? ([2, 3, 5, 8].includes(ii) ? 'bg-light' : '') : '')))}>{k ? k : (hideLaki ? ([2, 3, 5, 8].includes(ii) ? '' : k) : '0')}</td>
                              // <td key={ii}>{k ? k : (hideLaki ? ([].includes(ii) ? '' : k) : '')}</td>
                            )
                          })
                          :
                          null
                      }
                    </tr>
                  )
                }
                )}
              </tbody>
            </Table>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default View;
