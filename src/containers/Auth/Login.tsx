import * as React from "react";
import { Alert, Button, Form, FormGroup } from "reactstrap";
import { c } from "constant";
import { inject, observer } from "mobx-react";
import { IAuthStore } from "stores/authStore";
import { ICommonStore } from "stores/commonStore";
import { Link, withRouter } from "react-router-dom";
import { RouteComponentProps } from "react-router";

// import bg_left from "images/backgroud/bg-curva-2.png";
// import bg_right from "images/backgroud/bg-curva-1.png";
import iconPasien from "images/icons/icon-data-pasien.svg";
import iconAssignment from "images/icons/icon-assignment.svg";
import iconList from "images/icons/icon-list.svg";
import logo_hiv_aids from 'images/icons/logo_hiv_aids.png';
import logo_kemenkes from 'images/icons/logo_kemenkes.png';
import { Input } from "antd";

// new
import API from "utils/API";

import { ModalForgotPassword } from "components";

interface LoginProps extends RouteComponentProps<any> { }
interface InjectedProps extends LoginProps {
  commonStore: ICommonStore;
  authStore: IAuthStore;
}

interface ILoginState {
  alertType: string;
  alertMessage: string;
  visible: boolean;
  isModal: boolean;
  modalData: object;
  modalAlert: string;
  modalInProgres: boolean;
}

@inject(c.STORES.AUTH)
@inject(c.STORES.COMMON)
@observer
class Login extends React.Component<LoginProps, ILoginState> {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);

    this.state = {
      alertType: "",
      alertMessage: "",
      visible: false,
      isModal: false,
      modalData: {
        nik: null,
        password: null,
        confirmPassword: null
      },
      modalAlert: "",
      modalInProgres: false
    };
    this.onDismiss = this.onDismiss.bind(this);
  }

  get injected() {
    return this.props as InjectedProps;
  }

  onDismiss() {
    this.setState({ visible: false });
  }

  handleSubmit = e => {
    e.preventDefault();
    this.injected.authStore
      .login()
      .then(rsp => {
        if (rsp) {
          this.props.history.push("/dashboard");
          return;
        }
        let alertType = c.ALERT_TYPE.DANGER;
        let alertMessage = "Anda tidak memiliki akses!";
        this.setState({
          alertType: alertType,
          alertMessage: alertMessage,
          visible: true
        });
        return;
      })
      .catch(() => {
        let alertType = c.ALERT_TYPE.DANGER;
        let alertMessage = this.injected.authStore.errors;
        if (alertMessage) {
          this.setState({
            alertType: alertType,
            alertMessage: alertMessage,
            visible: true
          });
          return;
        }
      });
  };

  handleChange(e: React.ChangeEvent<HTMLInputElement>) {
    const { updateLoginData } = this.injected.authStore;
    const { name, value } = e.target;
    updateLoginData(name, value);
  }

  // new
  setModal = () => {
    this.setState({
      isModal: !this.state.isModal, modalData: {
        nik: null,
        password: null,
        confirmPassword: null
      }
    });
  }

  modalOnchange = e => {
    e.preventDefault();
    let data = this.state.modalData;
    const { name, value } = e.target;
    let alert = "";
    if (name == "confirmPassword") {
      if (data['password'] !== value) {
        alert = "Password Tidak Cocok"
      }
    }
    let newData = {
      ...data,
      [name]: value
    }

    this.setState({ modalData: newData, modalAlert: alert });
  }

  modalSubmit = async (e) => {
    e.preventDefault();
    this.setState({ modalInProgres: true });
    const data = {
      name: this.state.modalData['nik'],
      password: this.state.modalData['password']
    };
    await API.MasterData.User.fotgotPassword(data)
      .then(() => {
        this.setState({
          modalInProgres: false,
          alertType: "success",
          alertMessage: "Berhasil mengganti password",
          visible: true
        });
        this.setModal();
      })
      .catch(e => {
        this.setState({
          modalInProgres: false,
          alertType: "danger",
          alertMessage: "Gagal mengubah password",
          visible: true
        });
        console.error("e: ", e);
        this.setModal();
      });
  }
  // end new

  render() {
    const { inProgress } = this.injected.authStore;

    // const sectionStyleleft = {
    //   backgroundImage: `url(${bg_left})`,
    //   backgroundPosition: "bottom left",
    //   backgroundRepeat: "no-repeat",
    //   backgroundColor: "#fff"
    // };
    // const sectionStyleRight = {
    //   backgroundImage: `url(${bg_right})`,
    //   backgroundPosition: "top right",
    //   backgroundRepeat: "no-repeat",
    //   backgroundColor: "#fff"
    // };
    return (
      <React.Fragment>
        {
          this.state.isModal ?
            <ModalForgotPassword
              data={this.state.modalData}
              inProgress={this.state.modalInProgres}
              setModal={this.setModal}
              changeHandler={this.modalOnchange}
              alert={this.state.modalAlert}
              submitHanler={this.modalSubmit}
              /> :
            null
        }
        <div className="container-login">
          <section className="section--logo">
            <img src={logo_hiv_aids} alt="logo hiv aids" />
            <div className="logo-wrapper">
              <img src={logo_kemenkes} alt="logo kemenkes" />
            </div>
          </section>

          <section className="section-title-login">
            <h1>SIHA 2.0</h1>
            <p>Sistem Informasi HIV AIDS Kementrian Kesehatan Republik Indonesia untuk Pencegahan dan Pengendalian HIV AIDS dan PIMS</p>
          </section>

          <section className="section-form-login">
            <div className="login-box-wrapper">
              <div className="left-login-box-wrapper">
                <h3>Aplikasi ini digunakan untuk mencatat dan melaporkan :</h3>

                <div className="pb-2 d-flex w-100">
                  <img
                    className="mr-3 icon-login"
                    src={iconPasien}
                    style={{ width: "36px" }}
                    alt="icon"
                  />
                  <div>
                    <h4>1. Tes dan Pengobatan HIV</h4>
                    <p>Mencatat, melaporkan dan memonitor pencatatan dan pengobatan HIV AIDS.</p>
                  </div>
                </div>

                <div className="pb-2 d-flex w-100">
                  <img
                    className="mr-3 icon-login"
                    src={iconAssignment}
                    style={{ width: "36px" }}
                    alt="icon"
                  />
                  <div>
                    <h4>2. Tes dan Pengobatan PIMS</h4>
                    <p>Mencatat, melaporkan dan memonitor pencatatan dan pengobatan HIV AIDS.</p>
                  </div>
                </div>

                <div className="pb-2 d-flex w-100">
                  <img
                    className="mr-3 icon-login"
                    src={iconList}
                    style={{ width: "36px" }}
                    alt="icon"
                  />
                  <div>
                    <h4>3. Distribusi dan Pemakaian Komoditas Logistik</h4>
                    <p>Mencatat, melaporkan dan memonitor distribusi dan pemakaian Logistik.</p>
                  </div>
                </div>
              </div>

              <div className="right-login-box-wrapper">
                <Form className="" onSubmit={this.handleSubmit}>
                  <div className="login-alert-wrapper">
                    <Alert
                      color={this.state.alertType}
                      isOpen={this.state.visible}
                      toggle={this.onDismiss}>
                      {this.state.alertMessage}
                    </Alert>
                  </div>
                  <FormGroup>
                    <Input
                      className="width-full"
                      size="large"
                      type="text"
                      name="nik"
                      required
                      onChange={this.handleChange}
                      placeholder="NIK"
                    />
                  </FormGroup>
                  <FormGroup style={{ marginBottom: "0.5rem" }}>
                    <Input.Password
                      placeholder="password"
                      name="password"
                      size="large"
                      className="mb-4 w-100"
                      required
                      onChange={this.handleChange}
                    />
                  </FormGroup>
                  <div className="text-right" style={{ marginBottom: "0.5rem" }}>
                    <Button className="btn-forgot" color="link" onClick={this.setModal}>
                      Lupa Password ?
                    </Button>
                  </div>
                  <div className="text-center">
                    <Button
                      color="primary"
                      className="width-full btn-login"
                      type="submit"
                      disabled={inProgress}
                    >
                      {inProgress ? (
                        <React.Fragment>
                          <div
                            className="spinner-border spinner-border-sm text-light"
                            role="status"
                          />
                          <span> Loading...</span>
                        </React.Fragment>
                      ) : (
                          "Masuk"
                        )}
                    </Button>
                  </div>
                </Form>
              </div>
            </div>
          </section>
        </div>
      </React.Fragment>

      // <React.Fragment>
      //   <div className="row" style={{ height: "100vh" }}>
      //     <div
      //       className="col-md-5 pt-md-7 pl-md-7 pt-5 pl-5"
      //       style={sectionStyleleft}
      //     >
      //       <div className="text-left pr-2 pl-2" style={{ height: "50%" }}>
      //         <Button tag={Link} to="/home" color="link" className="p-0">
      //           <h1 className="text-primary">SiHA 2.0</h1>
      //         </Button>
      //         <h4 className="text-muted">
      //           Akses akun Anda untuk pengambilan obat dan melihat laporan hasil
      //           pemeriksaan
      //         </h4>
      //       </div>
      //       <div className="text-left pr-2 pl-2">
      //         <h5 className="text-muted">
      //           Website SiHA 2.0 ini mempermudah petugas untuk melakukan laporan
      //           :
      //         </h5>
      //         <div className="p-3">
      //           <div className="pb-4 row col-12">
      //             <img
      //               className="mr-3"
      //               src={iconPasien}
      //               style={{ width: "36px" }}
      //             />
      //             <Label className="text-primary font-weight-bold m-0">
      //               Peggunaan Regen
      //               <br />
      //               <span className="text-muted" style={{ fontSize: "12px" }}>
      //                 Melacak data penggunaan regen
      //               </span>
      //             </Label>
      //           </div>
      //           <div className="pb-4 row col-12">
      //             <img
      //               className="mr-3"
      //               src={iconAssignment}
      //               style={{ width: "36px" }}
      //             />
      //             <Label className="text-primary font-weight-bold m-0">
      //               Pemeriksaan Sifilis
      //               <br />
      //               <span className="text-muted" style={{ fontSize: "12px" }}>
      //                 Melacak data pemeriksaan sifilis
      //               </span>
      //             </Label>
      //           </div>
      //           <div className="pb-4 row col-12">
      //             <img
      //               className="mr-3"
      //               src={iconList}
      //               style={{ width: "36px" }}
      //             />
      //             <Label className="text-primary font-weight-bold m-0">
      //               Laporan Pemeriksaan HIV
      //               <br />
      //               <span className="text-muted" style={{ fontSize: "12px" }}>
      //                 Melacak laporan pemeriksaan HIV dengan mudah
      //               </span>
      //             </Label>
      //           </div>
      //         </div>
      //       </div>
      //     </div>
      //     <div
      //       className="col-md-7 p-md-7 p-5 pb-7 pt-7 d-flex justify-content-center align-items-end"
      //       style={sectionStyleRight}
      //     >
      //       <div
      //         className="col-md-10 text-center bg-white card-border-radius p-md-5 p-4 box-shadow-card"
      //         style={{
      //           height: "min-content"
      //         }}
      //       >
      //         <Form className="" onSubmit={this.handleSubmit}>
      //           <Alert
      //             color={this.state.alertType}
      //             isOpen={this.state.visible}
      //             toggle={this.onDismiss}
      //           >
      //             {this.state.alertMessage}
      //           </Alert>
      //           <FormGroup>
      //             <Input
      //               className="width-full"
      //               type="text"
      //               name="nik"
      //               required
      //               onChange={this.handleChange}
      //               placeholder="NIK"
      //             />
      //           </FormGroup>
      //           <FormGroup style={{ marginBottom: "0.5rem" }}>
      //             <Input.Password
      //               placeholder="password"
      //               name="password"
      //               size="large"
      //               className="mb-4 w-100"
      //               required
      //               onChange={this.handleChange}
      //             />
      //           </FormGroup>
      //           <div className="text-right" style={{ marginBottom: "0.5rem" }}>
      //             <Button tag={Link} to="/forgot-password" color="link">
      //               Lupa Password ?
      //             </Button>
      //           </div>
      //           <Button
      //             color="primary"
      //             className="width-full"
      //             type="submit"
      //             disabled={inProgress}
      //           >
      //             {inProgress ? (
      //               <React.Fragment>
      //                 <div
      //                   className="spinner-border spinner-border-sm text-light"
      //                   role="status"
      //                 />
      //                 <span> Loading...</span>
      //               </React.Fragment>
      //             ) : (
      //                 "Masuk"
      //               )}
      //           </Button>
      //         </Form>
      //       </div>
      //     </div>
      //   </div>
      // </React.Fragment>
    );
  }
}

export default withRouter(Login);
