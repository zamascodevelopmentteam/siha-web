import React from "react";

import { Button } from "antd";
import { SummaryWrapper } from "./style";
import { Typography } from "components";
import { ENUM } from "constant";

export const Summary = ({
  nik,
  name,
  status,
  toggleModalODHA,
  toggleModalOTP
}) => {
  return (
    <div className="bg-white rounded border p-4 d-flex justify-content-between align-items-center">
      <div>
        <Typography
          fontSize="14px"
          color="PHARMA_STAFF"
          fontWeight="bold"
          className="d-block mb-2"
        >
          Profil Pasien
        </Typography>
        <SummaryWrapper>
          <tbody>
            <tr>
              <td width="200px">NIK</td>
              <td>{nik}</td>
            </tr>
            <tr>
              <td>Nama</td>
              <td>{name}</td>
            </tr>
            <tr>
              <td>Status Pasien</td>
              <td>{ENUM.LABEL_STATUS_PATIENT[status]}</td>
            </tr>
          </tbody>
        </SummaryWrapper>
      </div>
      <div className="d-flex flex-column">
        <Button size="large" className="hs-btn-outline PHARMA_STAFF mb-2">
          Detail Pasien
        </Button>
        <Button
          size="large"
          className="hs-btn PHARMA_STAFF mb-2"
          onClick={toggleModalODHA}
        >
          Ubah Menjadi ODHA
        </Button>
        <Button
          size="large"
          className="hs-btn PHARMA_STAFF"
          onClick={toggleModalOTP}
        >
          Verifikasi Akun
        </Button>
      </div>
    </div>
  );
};
