import React, { useState } from "react";

import { Button } from "antd";
import { DataKunjungan } from "./DataKunjungan";
import {
  ModalConvertODHA,
  ModalHIV,
  ModalIMS,
  ModalMedicine,
  ModalOTP,
  ModalPDP,
  ModalRecipe,
  ModalVLCD4
} from "components";
import { RiwayatHIV } from "./RiwayatHIV";
import { RiwayatIMS } from "./RiwayatIMS";
import { Summary } from "./Summary";
import { useHistory } from "react-router";

export const ProfilePasien = () => {
  const [isHIVOpen, setHIVOpen] = useState(false);
  const [isODHAOpen, setODHAOpen] = useState(false);
  const [isOTPOpen, setOTPOpen] = useState(false);
  const [isIMSOpen, setIMSOpen] = useState(false);
  const [isMedicineOpen, setMedicineOpen] = useState(false);
  const [isPDPOpen, setPDPOpen] = useState(false);
  const [isRecipeOpen, setRecipeOpen] = useState(false);
  const [isVLCDOpen, setVLCCDOpen] = useState(false);

  const { goBack } = useHistory();

  const _goBack = () => {
    goBack();
  };

  const _toggleModalHIV = () => {
    setHIVOpen(!isHIVOpen);
  };

  const _toggleModalIMS = () => {
    setIMSOpen(!isIMSOpen);
  };

  const _toggleModalVLCD = () => {
    setVLCCDOpen(!isVLCDOpen);
  };

  const _toggleModalODHA = () => {
    setODHAOpen(!isODHAOpen);
  };

  const _toggleModalOTP = () => {
    setOTPOpen(!isOTPOpen);
  };

  const _toggleModalPDP = () => {
    setPDPOpen(!isPDPOpen);
  };

  const _toggleModalMedicine = () => {
    setMedicineOpen(!isMedicineOpen);
  };

  const _toggleModalRecipe = () => {
    setRecipeOpen(!isRecipeOpen);
  };

  return (
    <React.Fragment>
      {isVLCDOpen && (
        <ModalVLCD4
          onCancel={_toggleModalVLCD}
          onOk={_toggleModalVLCD}
          isLoading={false}
          role="PHARMA_STAFF"
        />
      )}

      {isHIVOpen && (
        <ModalHIV
          onCancel={_toggleModalHIV}
          onOk={_toggleModalHIV}
          isLoading={false}
          role="PHARMA_STAFF"
        />
      )}

      {isIMSOpen && (
        <ModalIMS
          onCancel={_toggleModalIMS}
          onOk={_toggleModalIMS}
          isLoading={false}
          role="PHARMA_STAFF"
        />
      )}

      {isODHAOpen && (
        <ModalConvertODHA
          onCancel={_toggleModalODHA}
          onOk={_toggleModalODHA}
          isLoading={false}
          role="PHARMA_STAFF"
        />
      )}

      {isOTPOpen && (
        <ModalOTP
          onCancel={_toggleModalOTP}
          onOk={_toggleModalOTP}
          isLoading={false}
          role="PHARMA_STAFF"
        />
      )}

      {isMedicineOpen && (
        <ModalMedicine
          onCancel={_toggleModalMedicine}
          onOk={_toggleModalMedicine}
          isLoading={false}
          role="PHARMA_STAFF"
        />
      )}

      {isPDPOpen && (
        <ModalPDP
          onCancel={_toggleModalPDP}
          onOk={_toggleModalPDP}
          isLoading={false}
          role="PHARMA_STAFF"
        />
      )}

      {isRecipeOpen && (
        <ModalRecipe
          onCancel={_toggleModalRecipe}
          onOk={_toggleModalRecipe}
          isLoading={false}
          role="PHARMA_STAFF"
        />
      )}

      <div className="container h-100 pt-4">
        <div className="row mb-3">
          <div className="col-12">
            <Button
              className="hs-btn-outline PHARMA_STAFF"
              icon="left"
              onClick={_goBack}
            >
              Kembali
            </Button>
          </div>
        </div>
        <div className="row mb-3">
          <div className="col-12 ">
            <Summary
              nik="333276813689001"
              name="Ignatius Roland"
              status="ODHA"
              toggleModalODHA={_toggleModalODHA}
              toggleModalOTP={_toggleModalOTP}
            />
          </div>
        </div>
        <div className="row mb-3">
          <div className="col-6">
            <RiwayatHIV />
          </div>
          <div className="col-6">
            <RiwayatIMS />
          </div>
        </div>
        <div className="row mb-4">
          <div className="col-12">
            <DataKunjungan
              toggleModalVLCD={_toggleModalVLCD}
              toggleModalHIV={_toggleModalHIV}
              toggleModalIMS={_toggleModalIMS}
              toggleModalPDP={_toggleModalPDP}
              toggleModalMedicine={_toggleModalMedicine}
              toggleModalRecipe={_toggleModalRecipe}
            />
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};
