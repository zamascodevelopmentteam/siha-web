import React from "react";
import shortid from "shortid";

import { Button } from "antd";
import { DataTable, Typography } from "components";

export const RiwayatHIV = () => {
  return (
    <div className="bg-white rounded p-4 border">
      <Typography
        fontSize="14px"
        color="PHARMA_STAFF"
        fontWeight="bold"
        className="d-block mb-2"
      >
        Riwayat Pemeriksaan HIV
      </Typography>
      <DataTable columns={columns} data={data} />
    </div>
  );
};

const columns = [
  {
    title: "Pemeriksaan ke",
    dataIndex: "ordinal",
    key: "ordinal"
  },
  {
    title: "Kunjungan ke",
    dataIndex: "visitNumber",
    key: "visitNumber"
  },
  {
    title: "Tanggal Pemeriksaan",
    dataIndex: "visitDate",
    key: "visitDate"
  },
  {
    title: "Aksi",
    key: "action",
    render: text => {
      return (
        <Button type="primary" size="small" className="hs-btn PHARMA_STAFF">
          Detail
        </Button>
      );
    }
  }
];

const data = [
  {
    key: shortid.generate(),
    ordinal: 1,
    visitNumber: 1,
    visitDate: "2 Desember 2019"
  },
  {
    key: shortid.generate(),
    ordinal: 1,
    visitNumber: 1,
    visitDate: "2 Desember 2019"
  },
  {
    key: shortid.generate(),
    ordinal: 1,
    visitNumber: 1,
    visitDate: "2 Desember 2019"
  },
  {
    key: shortid.generate(),
    ordinal: 1,
    visitNumber: 1,
    visitDate: "2 Desember 2019"
  }
];
