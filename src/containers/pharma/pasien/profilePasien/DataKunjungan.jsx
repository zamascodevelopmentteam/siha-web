import React from "react";
import shortid from "shortid";

import { Button } from "antd";
import { DataTable, Typography } from "components";

export const DataKunjungan = ({
  toggleModalHIV,
  toggleModalIMS,
  toggleModalPDP,
  toggleModalMedicine,
  toggleModalRecipe,
  toggleModalVLCD
}) => {
  const columns = [
    {
      title: "No",
      dataIndex: "index",
      key: "index"
    },
    {
      title: "Tanggal",
      dataIndex: "visitDate",
      key: "visitDate"
    },
    {
      title: "Status",
      dataIndex: "visitStatus",
      key: "visitStatus",
      render: visitStatus => {
        if (visitStatus.toLowerCase() === "dalam kunjungan") {
          return (
            <Typography fontWeight="bold" fontSize="12px" color="red">
              {visitStatus}
            </Typography>
          );
        }
        return (
          <Typography fontWeight="bold" fontSize="12px" color="green">
            {visitStatus}
          </Typography>
        );
      }
    },
    {
      title: "PDP",
      dataIndex: "pdp",
      key: "pdp",
      render: text => {
        return (
          <Button
            size="small"
            onClick={toggleModalPDP}
            className="hs-btn-outline PHARMA_STAFF"
          >
            Lihat
          </Button>
        );
      }
    },
    {
      title: "Lab HIV",
      dataIndex: "hivExam",
      key: "hivExam",
      render: text => {
        return (
          <Button
            size="small"
            onClick={toggleModalHIV}
            className="hs-btn-outline PHARMA_STAFF"
          >
            Lihat
          </Button>
        );
      }
    },
    {
      title: "Lab IMS",
      dataIndex: "imsExam",
      key: "imsExam",
      render: text => {
        return (
          <Button
            type="primary"
            size="small"
            className="hs-btn PHARMA_STAFF"
            onClick={toggleModalIMS}
          >
            Lihat
          </Button>
        );
      }
    },
    {
      title: "Lab VL/CD4",
      dataIndex: "vlcd4Exam",
      key: "vlcd4Exam",
      render: text => {
        return (
          <Button
            size="small"
            onClick={toggleModalVLCD}
            className="hs-btn-outline PHARMA_STAFF"
          >
            Lihat
          </Button>
        );
      }
    },
    {
      title: "Resep Obat",
      dataIndex: "recipe",
      key: "recipe",
      render: text => {
        return (
          <Button
            size="small"
            onClick={toggleModalRecipe}
            className="hs-btn-outline PHARMA_STAFF"
          >
            Lihat
          </Button>
        );
      }
    },
    {
      title: "Sisa Obat",
      dataIndex: "sisaObat",
      key: "sisaObat",
      render: text => {
        return (
          <Button
            size="small"
            onClick={toggleModalMedicine}
            className="hs-btn PHARMA_STAFF"
          >
            Ubah
          </Button>
        );
      }
    }
  ];

  const data = [
    {
      key: shortid.generate(),
      index: 1,
      visitDate: "2 Desember 2019",
      visitStatus: "Dalam Kunjungan"
    },
    {
      key: shortid.generate(),
      index: 1,
      visitDate: "2 Desember 2019",
      visitStatus: "Kunjungan Selesai"
    },
    {
      key: shortid.generate(),
      index: 1,
      visitDate: "2 Desember 2019",
      visitStatus: "Dalam Kunjungan"
    },
    {
      key: shortid.generate(),
      index: 1,
      visitDate: "2 Desember 2019",
      visitStatus: "Dalam Kunjungan"
    },
    {
      key: shortid.generate(),
      index: 1,
      visitDate: "2 Desember 2019",
      visitStatus: "Dalam Kunjungan"
    },
    {
      key: shortid.generate(),
      index: 1,
      visitDate: "2 Desember 2019",
      visitStatus: "Dalam Kunjungan"
    }
  ];
  return (
    <div className="bg-white rounded p-4 border">
      <Typography
        fontSize="14px"
        color="PHARMA_STAFF"
        fontWeight="bold"
        className="d-block mb-2"
      >
        Data Kunjungan
      </Typography>
      <DataTable columns={columns} data={data} />
    </div>
  );
};
