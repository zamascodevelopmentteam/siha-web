import React, { useState } from "react";
import shortid from "shortid";

import { Link } from "react-router-dom";
import { Button } from "antd";
import { DataTable, Typography, ModalPatient } from "components";
import { ENUM } from "constant";

export const DataPasien = () => {
  const [isAddPatientOpen, setAddPatientOpen] = useState(false);

  const _onFilter = value => {
    console.log("value: ", value);
  };

  const _toggleModalAddPatient = () => {
    setAddPatientOpen(!isAddPatientOpen);
  };

  return (
    <React.Fragment>
      {isAddPatientOpen && (
        <ModalPatient
          onCancel={_toggleModalAddPatient}
          onOk={_toggleModalAddPatient}
          isLoading={false}
        />
      )}
      <div className="container h-100 pt-4">
        <div className="row">
          <div className="col-12 bg-white rounded p-4 border">
            <DataTable
              title="Pasien UPK"
              onSearch={_onFilter}
              columns={columns}
              data={data}
              button={
                <Button
                  onClick={_toggleModalAddPatient}
                  className="hs-btn-outline PHARMA_STAFF"
                  icon="plus"
                >
                  Tambah Pasien
                </Button>
              }
            />
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

const columns = [
  {
    title: "No",
    dataIndex: "number",
    key: "number"
  },
  {
    title: "Nama",
    dataIndex: "name",
    key: "name"
  },
  {
    title: "Status HIV Pasien",
    dataIndex: "patientStatus",
    key: "patientStatus",
    render: patientStatus => ENUM.LABEL_STATUS_PATIENT[patientStatus]
  },
  {
    title: "Terakhir Berkunjung",
    dataIndex: "lastVisit",
    key: "lastVisit"
  },
  {
    title: "Status Kunjungan",
    dataIndex: "visitStatus",
    key: "visitStatus",
    render: visitStatus => {
      if (visitStatus.toLowerCase() === "dalam kunjungan") {
        return (
          <Typography fontWeight="bold" fontSize="12px" color="red">
            {visitStatus}
          </Typography>
        );
      }
      return (
        <Typography fontWeight="bold" fontSize="12px" color="green">
          {visitStatus}
        </Typography>
      );
    }
  },
  {
    title: "Aksi",
    key: "action",
    render: text => {
      return (
        <Link
          to={`/rr/data-pasien/${text.key}`}
          className="ant-btn hs-btn PHARMA_STAFF ant-btn-primary ant-btn-sm"
        >
          Detail
        </Link>
      );
    }
  }
];

const data = [
  {
    key: shortid.generate(),
    number: 1,
    name: "John Brown",
    patientStatus: "ODHA",
    lastVisit: "2 Desember 2019",
    visitStatus: "Dalam Kunjungan"
  },
  {
    key: shortid.generate(),
    number: 1,
    name: "John Brown",
    patientStatus: "ODHA",
    lastVisit: "2 Desember 2019",
    visitStatus: "Kunjungan Selesai"
  },
  {
    key: shortid.generate(),
    number: 1,
    name: "John Brown",
    patientStatus: "ODHA",
    lastVisit: "2 Desember 2019",
    visitStatus: "Dalam Kunjungan"
  },
  {
    key: shortid.generate(),
    number: 1,
    name: "John Brown",
    patientStatus: "ODHA",
    lastVisit: "2 Desember 2019",
    visitStatus: "Dalam Kunjungan"
  },
  {
    key: shortid.generate(),
    number: 1,
    name: "John Brown",
    patientStatus: "ODHA",
    lastVisit: "2 Desember 2019",
    visitStatus: "Dalam Kunjungan"
  },
  {
    key: shortid.generate(),
    number: 1,
    name: "John Brown",
    patientStatus: "ODHA",
    lastVisit: "2 Desember 2019",
    visitStatus: "Dalam Kunjungan"
  },
  {
    key: shortid.generate(),
    number: 1,
    name: "John Brown",
    patientStatus: "ODHA",
    lastVisit: "2 Desember 2019",
    visitStatus: "Dalam Kunjungan"
  },
  {
    key: shortid.generate(),
    number: 1,
    name: "John Brown",
    patientStatus: "ODHA",
    lastVisit: "2 Desember 2019",
    visitStatus: "Dalam Kunjungan"
  }
];
