import React from "react";

// import { DataPasien } from "./DataPasien";
import DataPasien from "./handler";
import { ProfilePasien } from "./profilePasien";
import { useParams } from "react-router";

const Pasien = () => {
  const { id } = useParams();

  return (
    <React.Fragment>
      {!id && <DataPasien />}
      {id && <ProfilePasien />}
    </React.Fragment>
  );
};

export default Pasien;
