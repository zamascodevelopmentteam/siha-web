import React, { useEffect, useState, useContext } from "react";
import API from "utils/API";
import axios from "axios";
import { openNotification } from "utils/Notification";
import { ENUM } from "constant";
import GlobalContext from "containers/GlobalContext";

import View from "./view";

const emptyData = {
  inventoryId: null,
  adjustmentType: null,
  valueAdjustment: null,
  notes: null,
  reportCode: null,
  unitType: ENUM.UNIT_TYPE.PAKET
  // unitType: null
};

const Handler = ({
  toggleModal,
  reload,
  id,
  name,
  batchCode,
  stockUnitType,
  packageQuantity,
  packageUnitType,
  setIsReloadStockHistory,
  stockQty,
  type
}) => {
  let source = axios.CancelToken.source();
  const user = useContext(GlobalContext);

  const [data, setData] = useState({ ...emptyData, inventoryId: id });
  const [isLoading, setLoading] = useState(false);

  const _handleChange = (name, value) => {
    setData({
      ...data,
      [name]: value
    });
  };

  const _onSubmit = () => {
    if ([
      ENUM.LOGISTIC_ROLE.LAB_ENTITY,
      ENUM.LOGISTIC_ROLE.PHARMA_ENTITY
    ].includes(user.logisticrole)) {
      if (!data.unitType) {
        return openNotification("error", "Gagal", "Tipe Kosong");
      }
    }

    let quantity = packageQuantity;
    if (ENUM.UNIT_TYPE.SATUAN === data.unitType) {
      quantity = stockQty;
    }
    if (quantity + data.valueAdjustment < 0) {
      openNotification(
        "error",
        "Adjustment Gagal",
        "Nilai Input melebihi Stok yang ada "
      );
      return;
    }
    setLoading(true);

    if (type === 'KALIBRATION') {
      // API.Logistic.kalibration(source.token, data)
      //   .then(() => {
      //     openNotification("success", "Berhasil", "Data berhasil diinput");
      //     reload();
      //     setLoading(false);
      //     toggleModal(null, null, null, null, null, type)
      //     setIsReloadStockHistory(true);
      //   })
      //   .catch(e => {
      //     setLoading(false);
      //     openNotification("error", "Gagal", e.message || String(e.data ? e.data.message : "").replace(/_/g, " "));
      //     console.error("e: ", e);
      //   });
      data.reportCode = "KALIBRASI";
      data.valueAdjustment = data.valueAdjustment * -1;
    }

    API.Logistic.adjustment(source.token, data)
      .then(() => {
        openNotification("success", "Berhasil", "Data berhasil diinput");
        reload();
        setLoading(false);
        if (type === 'KALIBRATION') {
          toggleModal(null, null, null, null, null, type);
        } else {
          toggleModal();
        }
        setIsReloadStockHistory(true);
      })
      .catch(e => {
        setLoading(false);
        openNotification("error", "Gagal", e.message || String(e.data ? e.data.message : "").replace(/_/g, " "));
        console.error("e: ", e);
      });
  };

  useEffect(() => {
    return () => {
      source.cancel("request canceled");
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <View
      stockUnitType={stockUnitType}
      packageUnitType={packageUnitType}
      isLoading={isLoading}
      onCancel={toggleModal}
      onOk={_onSubmit}
      onChange={_handleChange}
      data={data}
      name={name}
      batchCode={batchCode}
      type={type}
    />
  );
};

export default Handler;
