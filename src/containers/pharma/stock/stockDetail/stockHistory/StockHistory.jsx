import React, { useContext } from "react";

import { Button } from "antd";
import { DataTable, DisplayDate, Typography } from "components";
import { formatNumber, titleCase } from "utils";

import { ROLE } from "constant";
import GlobalContext from "containers/GlobalContext";

export const StockHistory = ({
  packageUnitType,
  toggleModalAdjustment,
  data,
  pagination,
  onChangeTable,
  isLoading
}) => {
  const user = useContext(GlobalContext);

  data = data.filter(r => r.stockQty !== 0);

  const columns = [
    {
      title: "ID Inventori",
      dataIndex: "id",
      key: "id",
      render: (id, r) => {
        var date1 = new Date();
        var date2 = new Date(r.expiredDate);
        var difference_In_Time = date2.getTime() - date1.getTime();
        var difference_In_Days = difference_In_Time / (1000 * 3600 * 24);
        let textClass = "";
        // console.log(difference_In_Days);
        if (difference_In_Days <= 90) {
          textClass = "text-warning";
        }
        if (difference_In_Days <= 30) {
          textClass = "text-danger";
        }
        return <span className={textClass}>{id}</span>;
      }
    },
    {
      title: "Nama Brand",
      dataIndex: "brand",
      key: "name",
      render: (brand, r) => {
        var date1 = new Date();
        var date2 = new Date(r.expiredDate);
        var difference_In_Time = date2.getTime() - date1.getTime();
        var difference_In_Days = difference_In_Time / (1000 * 3600 * 24);
        let textClass = "";
        if (difference_In_Days <= 90) {
          textClass = "text-warning";
        }
        if (difference_In_Days <= 30) {
          textClass = "text-danger";
        }
        return <span className={textClass}>{brand.name}</span>;
      }
    },
    {
      title: "Jumlah " + titleCase(packageUnitType),
      dataIndex: "packageQuantity",
      key: "packageQuantity",
      render: (num, r) => {
        var date1 = new Date();
        var date2 = new Date(r.expiredDate);
        var difference_In_Time = date2.getTime() - date1.getTime();
        var difference_In_Days = difference_In_Time / (1000 * 3600 * 24);
        let textClass = "";
        if (difference_In_Days <= 90) {
          textClass = "text-warning";
        }
        if (difference_In_Days <= 30) {
          textClass = "text-danger";
        }
        return <span className={textClass}>{formatNumber(parseInt(r.stockQty / r.brand.packageMultiplier))}</span>;
      }
    },
    {
      title: "Jumlah Satuan",
      dataIndex: "stockQty",
      key: "stockQty",
      render: (num, r) => {
        var date1 = new Date();
        var date2 = new Date(r.expiredDate);
        var difference_In_Time = date2.getTime() - date1.getTime();
        var difference_In_Days = difference_In_Time / (1000 * 3600 * 24);
        let textClass = "";
        if (difference_In_Days <= 90) {
          textClass = "text-warning";
        }
        if (difference_In_Days <= 30) {
          textClass = "text-danger";
        }
        return <span className={textClass}>{formatNumber(num)}</span>;
      }
    },
    {
      title: "Batch Code",
      dataIndex: "batchCode",
      key: "batchCode",
      render: (batchCode, r) => {
        var date1 = new Date();
        var date2 = new Date(r.expiredDate);
        var difference_In_Time = date2.getTime() - date1.getTime();
        var difference_In_Days = difference_In_Time / (1000 * 3600 * 24);
        let textClass = "";
        if (difference_In_Days <= 90) {
          textClass = "text-warning";
        }
        if (difference_In_Days <= 30) {
          textClass = "text-danger";
        }
        return <span className={textClass}>{batchCode}</span>;
      }
    },
    {
      title: "Expired Date",
      dataIndex: "expiredDate",
      key: "expiredDate",
      render: (expiredDate, r) => {
        var date1 = new Date();
        var date2 = new Date(r.expiredDate);
        var difference_In_Time = date2.getTime() - date1.getTime();
        var difference_In_Days = difference_In_Time / (1000 * 3600 * 24);
        let textClass = "";
        if (difference_In_Days <= 90) {
          textClass = "text-warning";
        }
        if (difference_In_Days <= 30) {
          textClass = "text-danger";
        }
        return <span className={textClass}><DisplayDate date={expiredDate} /></span>;
      }
    },
    {
      title: "Sumber Dana",
      dataIndex: "fundSource",
      key: "fundSource",
      render: (fundSource, r) => {
        var date1 = new Date();
        var date2 = new Date(r.expiredDate);
        var difference_In_Time = date2.getTime() - date1.getTime();
        var difference_In_Days = difference_In_Time / (1000 * 3600 * 24);
        let textClass = "";
        if (difference_In_Days <= 90) {
          textClass = "text-warning";
        }
        if (difference_In_Days <= 30) {
          textClass = "text-danger";
        }
        return <span className={textClass}>{fundSource}</span>;
      }
    },
    {
      title: "Harga " + titleCase(packageUnitType),
      dataIndex: "packagePrice",
      key: "packagePrice",
      render: (num, r) => {
        var date1 = new Date();
        var date2 = new Date(r.expiredDate);
        var difference_In_Time = date2.getTime() - date1.getTime();
        var difference_In_Days = difference_In_Time / (1000 * 3600 * 24);
        let textClass = "";
        if (difference_In_Days <= 90) {
          textClass = "text-warning";
        }
        if (difference_In_Days <= 30) {
          textClass = "text-danger";
        }
        return <span className={textClass}>{formatNumber(Number(num))}</span>;
      }
    },
    {
      title: "Manufacture",
      dataIndex: "manufacture",
      key: "manufacture",
      render: (manufacture, r) => {
        var date1 = new Date();
        var date2 = new Date(r.expiredDate);
        var difference_In_Time = date2.getTime() - date1.getTime();
        var difference_In_Days = difference_In_Time / (1000 * 3600 * 24);
        let textClass = "";
        if (difference_In_Days <= 90) {
          textClass = "text-warning";
        }
        if (difference_In_Days <= 30) {
          textClass = "text-danger";
        }
        return <span className={textClass}>{manufacture}</span>;
      }
    },
    {
      title: "Adjustment",
      key: "adjustment",
      render: item => {
        return (
          <Button
            size="small"
            onClick={() =>
              toggleModalAdjustment(
                item.id,
                item.brand.name,
                item.batchCode,
                parseInt(item.stockQty / item.brand.packageMultiplier),
                item.stockQty
              )
            }
            className="hs-btn PHARMA_STAFF"
          >
            Tambah
          </Button>
        );
      }
    },
    // user.role == ROLE.LAB_STAFF ?
    //   {
    //     title: "Kalibrasi",
    //     key: "kalibrasi",
    //     render: item => {
    //       return (
    //         <Button
    //           size="small"
    //           onClick={() =>
    //             toggleModalAdjustment(
    //               item.id,
    //               item.brand.name,
    //               item.batchCode,
    //               parseInt(item.stockQty / item.brand.packageMultiplier),
    //               item.stockQty,
    //               'KALIBRATION'
    //             )
    //           }
    //           className="hs-btn PHARMA_STAFF"
    //         >
    //           Tambah
    //         </Button>
    //       );
    //     }
    //   } : {}
  ];

  return (
    <div className="bg-white rounded p-4 border">
      <Typography
        fontSize="14px"
        color="PHARMA_STAFF"
        fontWeight="bold"
        className="d-block mb-2"
      >
        Data Stok
      </Typography>
      <DataTable
        columns={columns}
        data={data}
        pagination={pagination}
        onChange={onChangeTable}
        isLoading={isLoading}
      />
    </div>
  );
};
