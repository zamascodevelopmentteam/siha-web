import React, { useEffect, useState } from "react";
import API from "utils/API";
import axios from "axios";

import { useParams } from "react-router";
import { StockHistory } from "./StockHistory";

const paginationInitalState = {
  currentPage: 1,
  total: 0
};

const Handler = ({ toggleModalAdjustment, isReload, packageUnitType }) => {
  let source = axios.CancelToken.source();
  const [stocks, setStocks] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [pagination, setPagination] = useState(paginationInitalState);

  const { id } = useParams();

  const _handleChangeTable = page => {
    setPagination({
      ...pagination,
      currentPage: page.current
    });
  };

  const _listStock = () => {
    setIsLoading(true);
    API.Inventory.listByMedicineID(source.token, id)
      .then(data => {
        setStocks(data.data || []);
        setPagination({
          ...pagination,
          total: data.paging.total
        });
      })
      .catch(e => {
        console.error("e: ", e);
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  useEffect(() => {
    _listStock();

    return () => {
      source.cancel("request canceled");
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [pagination.currentPage, isReload]);

  return (
    <StockHistory
      packageUnitType={packageUnitType}
      toggleModalAdjustment={toggleModalAdjustment}
      data={stocks}
      onChangeTable={_handleChangeTable}
      isLoading={isLoading}
      pagination={pagination}
    />
  );
};

export default Handler;
