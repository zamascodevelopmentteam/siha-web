import React from "react";
import StockListHandler from "./handler";

const index = () => {
  return (
    <React.Fragment>
      <StockListHandler />
    </React.Fragment>
  );
};

export default index;
