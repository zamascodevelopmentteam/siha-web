import React, { useEffect, useState, useContext } from "react";
import GlobalContext from "containers/GlobalContext";
import API from "utils/API";
import axios from "axios";

import { StockList } from "./StockList";
import { ENUM } from "constant";
import { BASE_URL } from "utils/request";

const paginationInitalState = {
  currentPage: 1
};

const Handler = () => {
  const user = useContext(GlobalContext);
  let source = axios.CancelToken.source();
  const [medicines, setMedicines] = useState([]);
  // new
  const [filteredMedicines, setFilteredMedicines] = useState([]);
  // end new
  const [isLoading, setIsLoading] = useState(false);
  const [type, setType] = useState("ARV");
  const [keyword, setKeyword] = useState("");
  const [pagination, setPagination] = useState(paginationInitalState);

  const _handleChangeTable = page => {
    setPagination({
      currentPage: page.current
    });
  };

  const _handleSearch = value => {
    setPagination(paginationInitalState);

    let data = medicines;
    let filteredData = filteredMedicines;
    setKeyword(value);

    if (value.length) {
      filteredData = data.filter(item => {
        let startsWithCondition =
          item.name.toLowerCase().startsWith(value.toLowerCase()) ||
          item.codename.toLowerCase().startsWith(value.toLowerCase())
        let includesCondition =
          item.name.toLowerCase().includes(value.toLowerCase()) ||
          item.codename.toLowerCase().includes(value.toLowerCase())
        if (startsWithCondition) {
          return startsWithCondition
        } else if (!startsWithCondition && includesCondition) {
          return includesCondition
        } else return null
      })
      setFilteredMedicines(filteredData);
    }
  };

  const _handleChangeType = value => {
    setType(value);
  };

  const _reload = () => {
    setPagination(paginationInitalState);
    _listStock();
  };

  const _listStock = () => {
    setIsLoading(true);
    setPagination({
      currentPage: 1
    });
    API.Medicine.listStock(source.token, type)
      .then(data => {
        setMedicines(data.data || []);
      })
      .catch(e => {
        console.error("e: ", e);
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  async function _handleDownloadExcel() {
    try {
      API.Medicine.listStock(source.token, type, "excel=t")
        .then(res => {
          window.open(`${BASE_URL}/${res.data.split('/').slice(2).join('/')}`);
        })
        .catch(e => {
          console.error("e: ", e);
        });
    } catch (ex) {
      console.log(ex)
    }
  }

  useEffect(() => {
    if (user.logisticrole === ENUM.LOGISTIC_ROLE.LAB_ENTITY) {
      setType("NON_ARV");
    }
    _listStock();

    return () => {
      source.cancel("request canceled");
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [keyword, type]);

  return (
    <StockList
      data={keyword.length ? filteredMedicines : medicines}
      type={type}
      onChangeTable={_handleChangeTable}
      onSearch={_handleSearch}
      onChangeType={_handleChangeType}
      isLoading={isLoading}
      reload={_reload}
      pagination={pagination}
      onDownloadExcel={_handleDownloadExcel}
    />
  );
};

export default Handler;
