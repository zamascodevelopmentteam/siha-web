export { default as PharmaDashboard } from "./dashboard";
export { default as PharmaStock } from "./stock";
export { default as PharmaStockPharma } from "./stockpharma";
export { default as PharmaStockLab } from "./stocklab";
export { default as PharmaPasien } from "./pasien";
