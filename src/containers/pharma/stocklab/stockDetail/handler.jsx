import React, { useEffect, useState } from "react";
import API from "utils/API";
import axios from "axios";

import { useParams } from "react-router";
import { StockDetail } from "./StockDetail";

const paginationInitalState = {
  currentPage: 1,
  total: 0
};

const Handler = () => {
  let source = axios.CancelToken.source();
  const [stocks, setStocks] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [keyword, setKeyword] = useState("");
  const [pagination, setPagination] = useState(paginationInitalState);

  const { id } = useParams();

  const _handleChangeTable = page => {
    setPagination({
      ...pagination,
      currentPage: page.current
    });
  };

  const _handleSearch = value => {
    setPagination(paginationInitalState);
    setKeyword(value);
  };

  const _reload = () => {
    setPagination(paginationInitalState);
    _listStock();
  };

  const _listStock = () => {
    setIsLoading(true);
    API.Inventory.listByMedicineID(source.token, id)
      .then(data => {
        setStocks(data.data || []);
        setPagination({
          ...pagination,
          total: data.paging.total
        });
      })
      .catch(e => {
        console.error("e: ", e);
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  useEffect(() => {
    // _listStock();

    return () => {
      source.cancel("request canceled");
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [keyword]);

  return (
    <StockDetail
      data={stocks}
      onChangeTable={_handleChangeTable}
      onSearch={_handleSearch}
      isLoading={isLoading}
      reload={_reload}
      pagination={pagination}
    />
  );
};

export default Handler;
