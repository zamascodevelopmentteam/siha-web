import React from "react";

import { DataTable, Typography, DisplayDate } from "components";
import { formatNumber } from "utils";

const View = ({ data }) => {
  const columns = [
    {
      title: "ID Inventori",
      dataIndex: "inventoryId",
      key: "inventoryId"
    },
    {
      title: "Berita Acara",
      dataIndex: "reportCode",
      key: "reportCode"
    },
    {
      title: "Nama Brand",
      dataIndex: "inventory",
      key: "name",
      render: inventory => inventory.brand.brandName
    },
    // {
    //   title: "Jenis Penyesuaian",
    //   dataIndex: "adjustmentType",
    //   key: "adjustmentType"
    // },
    {
      title: "Penyesuaian",
      dataIndex: "stockQty",
      key: "stockQty",
      render: num => formatNumber(num)
    },
    {
      title: "Alasan Penyesuaian",
      dataIndex: "notes",
      key: "notes",
      render: notes => notes || "-"
    },
    {
      title: "Dibuat Tanggal",
      dataIndex: "createdAt",
      key: "createdAt",
      render: createdAt => (
        <DisplayDate date={createdAt} format="D MMM YYYY, HH:mm" />
      )
    },
    {
      title: "Dibuat Oleh",
      dataIndex: "createdByData",
      key: "createdBy",
      render: createdByData => createdByData.createdName
    }
  ];

  return (
    <div className="bg-white rounded p-4 border">
      <Typography
        fontSize="14px"
        color="PHARMA_STAFF"
        fontWeight="bold"
        className="d-block mb-2"
      >
        Riwayat Adjusment
      </Typography>
      <DataTable columns={columns} data={data} />
    </div>
  );
};

export default View;
