import React, { useEffect, useState } from "react";
import API from "utils/API";
import axios from "axios";
import { openNotification } from "utils/Notification";
import { ENUM } from "constant";

import View from "./view";

const emptyData = {
  inventoryId: null,
  adjustmentType: null,
  valueAdjustment: null,
  notes: null,
  reportCode: null,
  unitType: ENUM.UNIT_TYPE.PAKET
};

const Handler = ({
  toggleModal,
  reload,
  id,
  name,
  batchCode,
  stockUnitType,
  packageQuantity,
  packageUnitType,
  setIsReloadStockHistory
}) => {
  let source = axios.CancelToken.source();

  const [data, setData] = useState({ ...emptyData, inventoryId: id });
  const [isLoading, setLoading] = useState(false);

  const _handleChange = (name, value) => {
    setData({
      ...data,
      [name]: value
    });
  };

  const _onSubmit = () => {
    if (packageQuantity + data.valueAdjustment < 0) {
      openNotification(
        "error",
        "Adjustment Gagal",
        "Nilai Input melebihi Stok yang ada "
      );
      return;
    }
    setLoading(true);
    API.Logistic.adjustment(source.token, data)
      .then(() => {
        openNotification("success", "Berhasil", "Data berhasil diinput");
        reload();
        setLoading(false);
        toggleModal();
        setIsReloadStockHistory(true);
      })
      .catch(e => {
        setLoading(false);
        openNotification("error", "Gagal", e.message || String(e.data ? e.data.message : "").replace(/_/g, " "));
        console.error("e: ", e);
      });
  };

  useEffect(() => {
    return () => {
      source.cancel("request canceled");
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <View
      stockUnitType={stockUnitType}
      packageUnitType={packageUnitType}
      isLoading={isLoading}
      onCancel={toggleModal}
      onOk={_onSubmit}
      onChange={_handleChange}
      data={data}
      name={name}
      batchCode={batchCode}
    />
  );
};

export default Handler;
