import React, { useEffect, useState } from "react";
import API from "utils/API";
import axios from "axios";

import { useParams } from "react-router";
import { StockDetail } from "./StockDetail";

const detailEmpty = {
  id: null,
  name: null,
  codename: null,
  medicine_type: null,
  is_lini_1: null,
  is_lini_2: null,
  is_alkes: null,
  is_anak: null,
  is_cd_vl: null,
  is_io_ims: null,
  is_preventif: null,
  is_r1: null,
  is_r2: null,
  is_r3: null,
  is_sifilis: null,
  package_multiplier: null,
  package_quantity: null,
  stock_qty: null,
  stock_unit_type: null,
  package_unit_type: null
};

const Handler = () => {
  let source = axios.CancelToken.source();
  const [data, setData] = useState(detailEmpty);
  const [isLoading, setIsLoading] = useState(false);

  const { id } = useParams();

  const _loadData = () => {
    setIsLoading(true);
    API.Medicine.stockDetail(source.token, id)
      .then(rsp => {
        setData(rsp.data);
      })
      .catch(e => {
        console.error("e: ", e);
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  useEffect(() => {
    _loadData();

    return () => {
      source.cancel("request canceled");
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <StockDetail summary={data} isLoading={isLoading} reload={_loadData} />
  );
};

export default Handler;
