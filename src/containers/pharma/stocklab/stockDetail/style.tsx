import styled from "styled-components";

export const SummaryWrapper = styled.table`
  font-size: 12px;
  tr > td {
    padding: 5px 0;
  }
`;
