import React, { useState } from "react";

import AdjustmentHistory from "./AdjustmentHistory";
import { Button } from "antd";
import StockHistory from "./stockHistory";
import { Summary } from "./Summary";
import { useHistory, useParams } from "react-router";
import FormAddAdjustment from "./addAdjustment";

export const StockDetail = ({ summary, reload }) => {
  const [isAdjustmentOpen, setAdjustmentOpen] = useState(false);
  const [isReloadStockHistory, _setIsReloadStockHistory] = useState(false);
  const [adjustmentData, setAdjustmentData] = useState({
    id: null,
    name: null,
    batchCode: null,
    packageQuantity: null
  });

  const { goBack } = useHistory();
  const { id, subpage } = useParams();

  const _goBack = () => {
    goBack();
  };

  const _toggleModalAdjustment = (
    id = null,
    name = null,
    batchCode = null,
    packageQuantity = null
  ) => {
    if (typeof id === "number") {
      setAdjustmentData({ id, name, batchCode, packageQuantity });
    }
    setAdjustmentOpen(!isAdjustmentOpen);
  };

  return (
    <React.Fragment>
      {isAdjustmentOpen && (
        <FormAddAdjustment
          packageUnitType={summary.package_unit_type}
          stockUnitType={summary.stock_unit_type}
          toggleModal={_toggleModalAdjustment}
          id={adjustmentData.id}
          name={adjustmentData.name}
          batchCode={adjustmentData.batchCode}
          packageQuantity={adjustmentData.packageQuantity}
          reload={reload}
          setIsReloadStockHistory={_setIsReloadStockHistory}
        />
      )}

      <div className="container h-100 pt-4">
        <div className="row mb-3">
          <div className="col-12">
            <Button
              className="hs-btn-outline PHARMA_STAFF"
              icon="left"
              onClick={_goBack}
            >
              Kembali
            </Button>
          </div>
        </div>
        <div className="row mb-3">
          <div className="col-12 ">
            <Summary
              name={summary.name}
              codename={summary.codename}
              type={summary.medicine_type}
              totalPacket={summary.package_quantity}
              totalMedicine={summary.stock_qty}
              packageUnitType={summary.package_unit_type}
              stockUnitType={summary.stock_unit_type}
              packageMultiplier={summary.package_multiplier}
              id={id}
              showButton={subpage !== "adjustment-history"}
            />
          </div>
        </div>
        <div className="row mb-4">
          <div className="col-12">
            {!subpage && (
              <StockHistory
                packageUnitType={summary.package_unit_type}
                toggleModalAdjustment={_toggleModalAdjustment}
                isReload={isReloadStockHistory}
              />
            )}
            {subpage && (
              <AdjustmentHistory
                toggleModalAdjustment={_toggleModalAdjustment}
                id={id}
              />
            )}
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};
