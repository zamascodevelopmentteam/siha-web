import React from "react";

import StockList from "./stockList";
import StockDetail from "./stockDetail";
import { useParams } from "react-router";

const Stock = () => {
  const { id } = useParams();

  return (
    <React.Fragment>
      {!id && <StockList />}
      {id && <StockDetail />}
    </React.Fragment>
  );
};

export default Stock;
