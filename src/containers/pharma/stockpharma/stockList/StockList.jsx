import React, { useContext } from "react";
import GlobalContext from "containers/GlobalContext";

import { DataTable, Typography } from "components";
import { Link } from "react-router-dom";
import { Select } from "antd";
import { formatNumber } from "utils";
import { ENUM } from "constant";

const { Option } = Select;

export const StockList = ({
  type,
  onChangeType,
  onSearch,
  data,
  pagination,
  onChangeTable,
  isLoading
}) => {
  const user = useContext(GlobalContext);
  return (
    <div className="container h-100 pt-4">
      <div className="row mb-3">
        <div className="col-4 pl-0">
          <Typography
            fontSize="18px"
            fontWeight="bold"
            color="PHARMA_STAFF"
            className="d-block mb-3"
          >
            Stok
          </Typography>

          <Select
            style={{ width: "180px" }}
            onChange={onChangeType}
            value={type}
          >
            {user.logisticrole !== ENUM.LOGISTIC_ROLE.LAB_ENTITY && (
              <Option value="ARV">ARV</Option>
            )}
            {user.logisticrole !== ENUM.LOGISTIC_ROLE.PHARMA_ENTITY && (
              <Option value="NON_ARV">Non-ARV</Option>
            )}
          </Select>
        </div>
      </div>
      <div className="row">
        <div className="col-12 bg-white rounded p-4 border">
          <DataTable
            pagination={pagination}
            title="Daftar Stok"
            searchPlaceholder="Masukan Nama Obat"
            onSearch={onSearch}
            columns={columns}
            data={data}
            onChange={onChangeTable}
            isLoading={isLoading}
          />
        </div>
      </div>
    </div>
  );
};

const columns = [
  // {
  //   title: "ID",
  //   dataIndex: "id",
  //   key: "id"
  // },
  {
    title: "Nama Barang",
    dataIndex: "name",
    key: "name"
  },
  {
    title: "Kode Barang",
    dataIndex: "codename",
    key: "codename"
  },
  {
    title: "Jumlah Barang Dalam Kemasan",
    dataIndex: "package_multiplier",
    key: "package_multiplier"
  },
  {
    title: "Jenis Barang",
    dataIndex: "medicine_type",
    key: "medicine_type"
  },
  {
    title: "Stok",
    key: "stock_qty",
    render: item => (
      <React.Fragment>
        {formatNumber(item.stock_qty)} {item.stock_unit_type}
      </React.Fragment>
    )
  },
  {
    title: "Jumlah Paket",
    key: "package_quantity",
    render: item => (
      <React.Fragment>
        {formatNumber(item.package_quantity)} {item.package_unit_type}
      </React.Fragment>
    )
  },
  {
    title: "Aksi",
    key: "action",
    render: item => {
      return (
        <Link
          to={`/pharma/stock/${item.id}`}
          className="ant-btn hs-btn PHARMA_STAFF ant-btn-primary ant-btn-sm"
        >
          Detail
        </Link>
      );
    }
  }
];
