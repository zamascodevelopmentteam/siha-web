import React, { useContext } from "react";
import { Button, Modal, Input, Select, InputNumber } from "antd";
import { Label } from "components";
import { ENUM } from "constant";
import GlobalContext from "containers/GlobalContext";

const { Option } = Select;
const { TextArea } = Input;
const { UNIT_TYPE } = ENUM;

const Form = ({
  onOk,
  onCancel,
  isLoading,
  data,
  onChange,
  stockUnitType,
  packageUnitType,
  name,
  batchCode
}) => {
  const user = useContext(GlobalContext);
  const _handleChange = e => {
    onChange(e.target.name, e.target.value);
  };

  const _handleSubmit = e => {
    e.preventDefault();
    if (isLoading) {
      return;
    }
    onOk();
  };
  return (
    <Modal
      title="Tambah Penyesuaian"
      visible
      maskClosable={false}
      onOk={onOk}
      onCancel={onCancel}
      footer={null}
    >
      <form onSubmit={_handleSubmit}>
        <Label>Nama Brand</Label>
        <Label isInfo>{name}</Label>

        <Label>Batch Code</Label>
        <Label isInfo>{batchCode}</Label>

        <Label>
          Nomor Berita Acara <span className="text-danger">*Harus Diisi</span>
        </Label>
        <Input
          placeholder="Masukan Berita Acara"
          style={{ width: "100%" }}
          className="mb-3"
          name="reportCode"
          value={data.reportCode}
          onChange={_handleChange}
          required
        />

        {/* <Label>Jenis Penyesuaian *</Label>
        <Select
          placeholder="Pilih Tipe Paket Unit"
          style={{ width: "100%" }}
          className="mb-3"
          name="adjustmentType"
          value={data.adjustmentType}
          onChange={value => onChange("adjustmentType", value)}
        >
          <Option value={null} disabled>
            Pilih Jenis Penyesuaian
          </Option>
          {Object.keys(ADJUSTMENT_TYPE).map(key => (
            <Option key={ADJUSTMENT_TYPE[key]} value={ADJUSTMENT_TYPE[key]}>
              {ADJUSTMENT_TYPE[key]}
            </Option>
          ))}
        </Select> */}
        {[
          ENUM.LOGISTIC_ROLE.LAB_ENTITY,
          ENUM.LOGISTIC_ROLE.PHARMA_ENTITY
        ].includes(user.logisticrole) && (
          <React.Fragment>
            <Label>Tipe Penyesuaian *</Label>
            <Select
              placeholder="Pilih Tipe "
              style={{ width: "100%" }}
              className="mb-3"
              name="unitType"
              value={data.unitType}
              onChange={value => onChange("unitType", value)}
            >
              <Option value={null} disabled>
                Pilih Jenis Penyesuaian
              </Option>
              <Option key={UNIT_TYPE.PAKET} value={UNIT_TYPE.PAKET}>
                {packageUnitType}
              </Option>
              <Option key={UNIT_TYPE.SATUAN} value={UNIT_TYPE.SATUAN}>
                {stockUnitType}
              </Option>
            </Select>
          </React.Fragment>
        )}

        <Label>
          Nilai Penyesuaian (
          {data.unitType === ENUM.UNIT_TYPE.PAKET
            ? packageUnitType
            : stockUnitType}
          ) <span className="text-danger">*Harus Diisi</span>
        </Label>
        <Label style={{ font: "12px" }} className="text-black-50">
          Tambahkan + untuk Penambahan dan - Untuk pengurangan (Contoh: -10)
        </Label>
        <InputNumber
          placeholder="Masukan Nilai Penyesuaian"
          style={{ width: "100%" }}
          className="mb-3"
          name="valueAdjustment"
          value={data.valueAdjustment}
          onChange={value => onChange("valueAdjustment", value)}
          required
        />

        <Label>Alasan Penyesuaian</Label>
        <TextArea
          placeholder="Masukkan Alasan Penyesuaian"
          style={{ width: "100%" }}
          className="mb-3"
          name="notes"
          value={data.notes}
          onChange={_handleChange}
        />

        <div className="d-flex justify-content-end mt-2">
          <Button key="back" type="danger" onClick={onCancel} className="mr-2">
            Tutup
          </Button>
          <Button
            key="submit"
            type="primary"
            loading={isLoading}
            htmlType="submit"
          >
            Simpan
          </Button>
        </div>
      </form>
    </Modal>
  );
};

export default Form;
