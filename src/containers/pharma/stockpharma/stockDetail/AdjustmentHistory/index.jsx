import React, { useEffect, useState } from "react";
import API from "utils/API";
import axios from "axios";

import View from "./view";

const Handler = ({ id }) => {
  let source = axios.CancelToken.source();

  const [data, setData] = useState([]);
  const [isLoading, setLoading] = useState(false);

  const _loadData = () => {
    setLoading(true);
    API.Logistic.listAdjustmentHistory(source.token, id)
      .then(rsp => {
        setData(rsp.data || []);
      })
      .catch(e => {
        console.error("e: ", e);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  useEffect(() => {
    _loadData();

    return () => {
      source.cancel("request canceled");
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return <View isLoading={isLoading} data={data} />;
};

export default Handler;
