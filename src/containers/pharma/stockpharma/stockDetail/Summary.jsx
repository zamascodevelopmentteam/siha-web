import React from "react";

import { Avatar, Typography } from "components";
import { Link } from "react-router-dom";
import { SummaryWrapper } from "./style";
import { formatNumber } from "utils";

export const Summary = ({
  id,
  name,
  codename,
  type,
  totalPacket,
  totalMedicine,
  stockUnitType,
  packageMultiplier,
  packageUnitType,
  showButton
}) => {
  return (
    <div className="bg-white rounded border p-4 d-flex justify-content-between align-items-center">
      <div className="d-flex">
        <Avatar
          src="https://source.unsplash.com/kfJkpeI6Lgc"
          size="128px"
          className="mr-4"
        />
        <div>
          <Typography
            fontSize="14px"
            color="PHARMA_STAFF"
            fontWeight="bold"
            className="d-block mb-2"
          >
            Detail Barang
          </Typography>
          <SummaryWrapper>
            <tbody>
              <tr>
                <td width="170px">Nama Barang</td>
                <td>
                  <b>{name}</b>
                </td>
                <td width="170px" className="pl-5">
                  Total Paket
                </td>
                <td>
                  <b>
                    {formatNumber(totalPacket)} {packageUnitType}
                  </b>
                </td>
              </tr>
              <tr>
                <td>Jenis barang</td>
                <td>
                  <b>{codename}</b>
                </td>
                <td className="pl-5">Total {stockUnitType}</td>
                <td>
                  <b>
                    {formatNumber(totalMedicine)} {stockUnitType}
                  </b>
                </td>
              </tr>
              <tr>
                <td>Jenis</td>
                <td>
                  <b>{type}</b>
                </td>
                <td className="pl-5">
                  Total {stockUnitType}/{packageUnitType}
                </td>
                <td>
                  <b>
                    {formatNumber(packageMultiplier)} {stockUnitType} / 1{" "}
                    {packageUnitType}
                  </b>
                </td>
              </tr>
            </tbody>
          </SummaryWrapper>
        </div>
      </div>
      <div className="d-flex flex-column">
        {showButton && (
          <Link
            to={`/pharma/stock/${id}/adjustment-history`}
            className="ant-btn hs-btn-outline PHARMA_STAFF  ant-btn-lg"
          >
            Riwayat Adjustment
          </Link>
        )}
      </div>
    </div>
  );
};
