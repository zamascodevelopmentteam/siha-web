import React from "react";

import { Button } from "antd";
import { DataTable, DisplayDate, Typography } from "components";
import { formatNumber, titleCase } from "utils";

export const StockHistory = ({
  packageUnitType,
  toggleModalAdjustment,
  data,
  pagination,
  onChangeTable,
  isLoading
}) => {
  const columns = [
    {
      title: "ID Inventori",
      dataIndex: "id",
      key: "id"
    },
    {
      title: "Nama Brand",
      dataIndex: "brand",
      key: "name",
      render: brand => brand.name
    },
    {
      title: "Jumlah " + titleCase(packageUnitType),
      dataIndex: "packageQuantity",
      key: "packageQuantity",
      render: num => formatNumber(num)
    },
    {
      title: "Jumlah Satuan",
      dataIndex: "stockQty",
      key: "stockQty",
      render: num => formatNumber(num)
    },
    {
      title: "Batch Code",
      dataIndex: "batchCode",
      key: "batchCode"
    },
    {
      title: "Expired Date",
      dataIndex: "expiredDate",
      key: "expiredDate",
      render: expiredDate => <DisplayDate date={expiredDate} />
    },
    {
      title: "Sumber Dana",
      dataIndex: "fundSource",
      key: "fundSource"
    },
    {
      title: "Harga " + titleCase(packageUnitType),
      dataIndex: "packagePrice",
      key: "packagePrice",
      render: num => formatNumber(Number(num))
    },
    {
      title: "Manufacture",
      dataIndex: "manufacture",
      key: "manufacture"
    },

    {
      title: "Adjustment",
      key: "adjustment",
      render: item => {
        return (
          <Button
            size="small"
            onClick={() =>
              toggleModalAdjustment(
                item.id,
                item.brand.name,
                item.batchCode,
                item.packageQuantity
              )
            }
            className="hs-btn PHARMA_STAFF"
          >
            Tambah
          </Button>
        );
      }
    }
  ];

  return (
    <div className="bg-white rounded p-4 border">
      <Typography
        fontSize="14px"
        color="PHARMA_STAFF"
        fontWeight="bold"
        className="d-block mb-2"
      >
        Data Stok
      </Typography>
      <DataTable
        columns={columns}
        data={data}
        pagination={pagination}
        onChange={onChangeTable}
        isLoading={isLoading}
      />
    </div>
  );
};
