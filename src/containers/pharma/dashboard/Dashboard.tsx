import * as React from "react";
import { WelcomeMessage, QuickStatV2, Typography } from "components";

export const Dashboard = () => {
  return (
    <div className="container p-4">
      <div className="row mb-4">
        <div className="col-12 ">
          <WelcomeMessage name="Yudhasena" />
        </div>
      </div>

      <div className="row">
        <div className="col-12">
          <div className="d-flex justify-content-between mb-3">
            <Typography fontSize="16px" fontWeight="bold" color="black">
              Kunjungan Bulan Desember 2019
            </Typography>
          </div>
          <div className="row">
            <div className="col-4">
              <QuickStatV2
                count={400}
                title="Jumlah Pasien ODHA Berkunjung Bulan ini"
                description="400 orang yang mengunjungi Poli RS pada bulan November 2019"
                inverted
              />
            </div>
            <div className="col-4">
              <QuickStatV2
                count={400}
                title="Jumlah Pasien Check-in Hari ini"
                description="400 orang yang mengunjungi Poli RS pada bulan Oktober 2019"
                inverted
              />
            </div>
            <div className="col-4">
              <QuickStatV2
                count={400}
                title="Jumlah Pasien ODHA Transit Bulan Ini"
                description="400 orang yang mengunjungi Poli RS pada bulan September 2019"
                inverted
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
