import * as React from "react";
import { Tag } from "antd";
import { Typography } from "components";
import { Link } from "react-router-dom";

export const Visitor = ({ name, ordinal, date, visitStatus, userStatus }) => {
  return (
    <div className="d-flex justify-content-between align-items-center pl-3 pr-2 py-2 border-bottom">
      <div>
        <Typography
          fontSize="14px"
          fontWeight="600"
          color="black"
          className="d-block"
        >
          {name}
        </Typography>
        <Typography
          fontSize="10px"
          fontWeight="600"
          color="secondary"
          className="d-block"
        >
          Kunjungan ke - {ordinal}
        </Typography>
        <Typography
          fontSize="10px"
          fontWeight="600"
          color="secondary"
          className="d-block"
        >
          {date}
        </Typography>
        <Typography
          fontSize="10px"
          fontWeight="600"
          color="secondary"
          className="d-block"
        >
          Status: {visitStatus}
        </Typography>
      </div>
      <div className="d-flex flex-column align-items-end">
        <Tag color="#ff4b4b" className="mb-2 mr-0">
          {userStatus}
        </Tag>
        <Link
          to={`/pharma/data-pasien/${name}`}
          className="ant-btn hs-btn PHARMA_STAFF ant-btn-primary ant-btn-sm"
        >
          Lihat Detail
        </Link>
      </div>
    </div>
  );
};
