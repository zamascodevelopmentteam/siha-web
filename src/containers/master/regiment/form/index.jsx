import React, { useEffect, useState } from "react";
import API from "utils/API";
import axios from "axios";
import { openNotification } from "utils/Notification";
import { cloneDeep } from "lodash";
import shortid from "shortid";

import View from "./view";

const emptyData = {
  name: null,
  medicineIds: [
    {
      id: shortid.generate(),
      value: ""
    }
  ]
};

const Handler = ({ toggleModal, reload, id }) => {
  let source = axios.CancelToken.source();
  const isEdit = id !== null;

  const [data, setData] = useState(emptyData);
  const [isLoading, setLoading] = useState(false);

  const _handleChange = (name, value) => {
    setData({
      ...data,
      [name]: value
    });
  };

  const _addMedicine = () => {
    let newMedicineIds = cloneDeep(data.medicineIds);
    newMedicineIds.push({
      id: shortid.generate(),
      value: ""
    });
    setData({
      ...data,
      medicineIds: newMedicineIds
    });
  };

  const _removeMedicine = idx => {
    data.medicineIds.splice(idx, 1);
    setData(data);
  };

  const _handleChangeMedicines = (idx, value) => {
    let newMedicineIds = cloneDeep(data.medicineIds);
    newMedicineIds[idx].value = value;
    setData({
      ...data,
      medicineIds: newMedicineIds
    });
  };

  const _onSubmit = () => {
    const payload = _constructPayload(data);
    if (isEdit) {
      _update(payload);
      return;
    }
    _create(payload);
  };

  const _loadData = () => {
    setLoading(true);
    API.MasterData.Regiment.getByID(source.token, id)
      .then(rsp => {
        const data = _constructResponse(rsp.data);
        setData(data || emptyData);
      })
      .catch(e => {
        console.error("e: ", e);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const _create = payload => {
    setLoading(true);
    API.MasterData.Regiment.create(source.token, payload)
      .then(() => {
        openNotification("success", "Berhasil", "Data berhasil diinput");
        reload();
        setLoading(false);
        toggleModal();
      })
      .catch(e => {
        openNotification("error", "Gagal", e.message || String(e.data ? e.data.message : "").replace(/_/g, " "));
        setLoading(false);
        console.error("e: ", e);
      });
  };

  const _update = payload => {
    setLoading(true);
    API.MasterData.Regiment.update(source.token, id, payload)
      .then(() => {
        openNotification("success", "Berhasil", "Data berhasil diinput");
        reload();
        setLoading(false);
        toggleModal();
      })
      .catch(e => {
        openNotification("error", "Gagal", e.message || String(e.data ? e.data.message : "").replace(/_/g, " "));
        setLoading(false);
        console.error("e: ", e);
      });
  };

  useEffect(() => {
    if (isEdit) {
      _loadData();
    }

    return () => {
      source.cancel("request canceled");
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <View
      isLoading={isLoading}
      onCancel={toggleModal}
      onOk={_onSubmit}
      onChange={_handleChange}
      onChangeMedicine={_handleChangeMedicines}
      addMedicine={_addMedicine}
      removeMedicine={_removeMedicine}
      data={data}
    />
  );
};

export default Handler;

const _constructPayload = data => {
  const { name, medicineIds: oldIds } = data;
  let medicineIds = [];

  for (let x = 0; x < oldIds.length; x++) {
    medicineIds.push(oldIds[x].value);
  }

  return { name, medicineIds };
};

const _constructResponse = res => {
  const { name, medicines } = res;
  let medicineIds = [];
  for (let x = 0; x < medicines.length; x++) {
    medicineIds.push({
      id: shortid.generate(),
      value: medicines[x].id
    });
  }

  return {
    name,
    medicineIds
  };
};
