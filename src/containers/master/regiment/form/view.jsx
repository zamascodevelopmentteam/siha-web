import React from "react";
import { Button, Modal, Input } from "antd";
import { ChooseMedicine, Label } from "components";

const Form = ({
  onOk,
  onCancel,
  isLoading,
  data,
  onChange,
  addMedicine,
  removeMedicine,
  onChangeMedicine
}) => {
  const _handleChange = e => {
    onChange(e.target.name, e.target.value);
  };

  const _handleSubmit = e => {
    e.preventDefault();
    if (isLoading) {
      return;
    }
    onOk();
  };
  return (
    <Modal
      title="Tambah/Ubah Master Brand"
      visible
      maskClosable={false}
      onOk={onOk}
      onCancel={onCancel}
      footer={null}
    >
      <form onSubmit={_handleSubmit}>
        <Label>Nama Lini Regiment *</Label>
        <Input
          placeholder="Masukan Nama Brand"
          style={{ width: "100%" }}
          className="mb-3"
          name="name"
          value={data.name}
          onChange={_handleChange}
          required
        />
        <div className="row mb-2">
          <div className="col-2">
            <Label>No.</Label>
          </div>
          <div className="col-8">Nama Obat</div>
          <div className="col-2">
            <Button
              size="small"
              type="primary"
              icon="plus"
              onClick={addMedicine}
            />
          </div>
        </div>

        {data.medicineIds.map((item, idx) => (
          <MedicineRow
            key={item.id}
            idx={idx}
            medicineId={item.value}
            onChange={onChangeMedicine}
            onRemove={removeMedicine}
          />
        ))}

        <div className="d-flex justify-content-end mt-2">
          <Button key="back" type="danger" onClick={onCancel} className="mr-2">
            Tutup
          </Button>
          <Button
            key="submit"
            type="primary"
            loading={isLoading}
            htmlType="submit"
          >
            Simpan
          </Button>
        </div>
      </form>
    </Modal>
  );
};

export default Form;

const MedicineRow = ({ idx, medicineId, onRemove, onChange }) => {
  return (
    <div className="row">
      <div className="col-2">{idx + 1}</div>
      <div className="col-8">
        <ChooseMedicine
          placeholder="Pilih Obat"
          style={{ width: "calc(100% - 20px)" }}
          className="ml-1 mb-3"
          name="medicineId"
          value={medicineId}
          onChange={value => onChange(idx, value)}
          required
        />
      </div>
      <div className="col-2">
        <Button
          size="small"
          type="danger"
          icon="delete"
          onClick={() => onRemove(idx)}
        />
      </div>
    </div>
  );
};
