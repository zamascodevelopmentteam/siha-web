import React, { useState } from "react";

import { DataTable, DisplayDate } from "components";
import { Button } from "antd";
import Form from "./form";

const View = ({ data, isLoading, reload, handleDelete, onSearch }) => {
  const [isFormOpen, setFormOpen] = useState(false);
  const [id, setID] = useState(null);

  const _toggleModal = (id = null) => {
    if (typeof id === "number") {
      setID(id);
    } else {
      setID(null);
    }
    setFormOpen(!isFormOpen);
  };

  const columns = [
    {
      title: "Nama Regiment",
      dataIndex: "combinationStrInfo",
      key: "combinationStrInfo"
    },
    {
      title: "Lini Regiment",
      dataIndex: "name",
      key: "name"
    },
    {
      title: "Terakhir Diubah",
      dataIndex: "updatedAt",
      key: "updatedAt",
      render: date => <DisplayDate date={date} />
    },
    {
      title: "Diedit Oleh",
      dataIndex: "updatedBy",
      key: "updatedBy"
    },
    {
      title: "Aksi",
      key: "action",
      fixed: "right",
      render: item => (
        <React.Fragment>
          <Button
            size="small"
            type="primary"
            className="mr-2"
            onClick={() => _toggleModal(item.id)}
          >
            Ubah
          </Button>
          <Button
            size="small"
            type="danger"
            onClick={() => handleDelete(item.id, item.name)}
          >
            Hapus
          </Button>
        </React.Fragment>
      )
    }
  ];

  return (
    <React.Fragment>
      {isFormOpen && (
        <Form toggleModal={_toggleModal} reload={reload} id={id} />
      )}
      <div className="container h-100 pt-4">
        <div className="row">
          <div className="col-12 bg-white rounded p-4 border">
            <DataTable
              title="Master Regiment"
              columns={columns}
              data={data}
              isLoading={isLoading}
              isScrollX
              button={
                <Button type="primary" onClick={_toggleModal}>
                  Tambah Baru
                </Button>
              }
              onSearch={onSearch}
            />
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default View;
