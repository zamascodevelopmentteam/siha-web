import React, { useEffect, useState } from "react";
import API from "utils/API";
import axios from "axios";
import { openNotification } from "utils/Notification";

import View from "./view";
import { ENUM } from "constant";

const emptyData = {
  name: null,
  nik: null,
  isActive: null,
  password: null,
  role: null,
  logisticrole: null,
  email: null,
  phone: null,
  provinceId: null,
  upkId: null,
  sudinKabKotaId: null
};

const { LAB_STAFF, PHARMA_STAFF, RR_STAFF } = ENUM.USER_ROLE;

const upkRole = {
  LAB_STAFF,
  PHARMA_STAFF,
  RR_STAFF
};

const Handler = ({ toggleModal, reload, id, roleName }) => {
  let source = axios.CancelToken.source();
  const isEdit = id !== null;

  const [data, setData] = useState(emptyData);
  const [isLoading, setLoading] = useState(false);

  const _loadData = () => {
    setLoading(true);
    API.MasterData.User.getByID(source.token, id)
      .then(rsp => {
        const data = _constructResponse(rsp.data);
        setData(data || emptyData);
      })
      .catch(e => {
        console.error("e: ", e);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const _handleChange = (name, value) => {
    setData({
      ...data,
      [name]: value
    });
  };

  const _onSubmit = () => {
    const payload = _constructPayload(data);
    if (isEdit) {
      _update(payload);
      return;
    }
    _create(payload);
  };

  const _create = payload => {
    setLoading(true);
    API.MasterData.User.create(source.token, payload)
      .then(() => {
        openNotification("success", "Berhasil", "Data berhasil diinput");
        reload();
        setLoading(false);
        toggleModal();
      })
      .catch(e => {
        openNotification("error", "Gagal", e.message || String(e.data ? e.data.message : "").replace(/_/g, " "));
        setLoading(false);
        console.error("e: ", e);
      });
  };

  const _update = payload => {
    setLoading(true);
    API.MasterData.User.update(source.token, id, payload)
      .then(() => {
        openNotification("success", "Berhasil", "Data berhasil diinput");
        reload();
        setLoading(false);
        toggleModal();
      })
      .catch(e => {
        openNotification("error", "Gagal", e.message || String(e.data ? e.data.message : "").replace(/_/g, " "));
        setLoading(false);
        console.error("e: ", e);
      });
  };

  useEffect(() => {
    if (isEdit) {
      _loadData();
    }

    return () => {
      source.cancel("request canceled");
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <View
      isLoading={isLoading}
      onCancel={toggleModal}
      onOk={_onSubmit}
      onChange={_handleChange}
      data={data}
      roleName={roleName}
    />
  );
};

export default Handler;

const _constructPayload = data => {
  switch (data.role) {
    case ENUM.USER_ROLE.PROVINCE_STAFF:
      data.upkId = null;
      data.sudinKabKotaId = null;
      break;
    case ENUM.USER_ROLE.SUDIN_STAFF:
      data.provinceId = null;
      data.upkId = null;
      break;
    case ENUM.USER_ROLE.MINISTRY_STAFF:
      data.provinceId = null;
      data.upkId = null;
      data.sudinKabKotaId = null;
      break;
    case ENUM.USER_ROLE.LAB_STAFF:
    case ENUM.USER_ROLE.PHARMA_STAFF:
    case ENUM.USER_ROLE.RR_STAFF:
      data.provinceId = null;
      data.sudinKabKotaId = null;
      break;
    default:
      break;
  }
  if (data.role === ENUM.USER_ROLE.PROVINCE_STAFF) {
    data.upkId = null;
    data.sudinKabKotaId = null;
  }
  if (data.role === ENUM.USER_ROLE.SUDIN_STAFF) {
    data.provinceId = null;
    data.upkId = null;
  }
  if (data.role === ENUM.USER_ROLE.MINISTRY_STAFF) {
    data.provinceId = null;
    data.upkId = null;
    data.sudinKabKotaId = null;
  }

  if (Object.values(upkRole).includes(data.role)) {
    data.provinceId = null;
    data.sudinKabKotaId = null;
  }

  return data;
};

const _constructResponse = res => {
  const {
    name,
    nik,
    isActive,
    password,
    role,
    email,
    phone,
    provinceId,
    upkId,
    sudinKabKotaId,
    address
  } = res;

  return {
    name,
    nik,
    isActive,
    password,
    role,
    email,
    phone,
    provinceId,
    upkId,
    sudinKabKotaId,
    address
  };
};
