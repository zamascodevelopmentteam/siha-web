import React from "react";
import { Button, Modal, Input, Radio, Select } from "antd";
import { ChooseProvince, ChooseSudin, ChooseUpk, Label } from "components";
import { ENUM } from "constant";

const { Option } = Select;
const { USER_ROLE, LOGISTIC_ROLE } = ENUM;

const { LAB_STAFF, PHARMA_STAFF, RR_STAFF, DOCTOR } = USER_ROLE;

const upkRole = {
  LAB_STAFF,
  PHARMA_STAFF,
  RR_STAFF,
  DOCTOR
};

const Form = ({ onOk, onCancel, isLoading, data, onChange, roleName }) => {
  const _handleChange = e => {
    onChange(e.target.name, e.target.value);
  };

  const _handleSubmit = e => {
    e.preventDefault();
    if (isLoading) {
      return;
    }
    onOk();
  };
  return (
    <Modal
      title="Tambah/Ubah Master User"
      visible
      maskClosable={false}
      onOk={onOk}
      onCancel={onCancel}
      footer={null}
    >
      <form onSubmit={_handleSubmit}>
        <Label>
          NIK <span className="text-danger">*Harus Diisi</span>
        </Label>
        <Input
          placeholder="Masukan NIK"
          style={{ width: "100%" }}
          className="mb-3"
          name="nik"
          minLength={5}
          maxLength={50}
          value={data.nik}
          onChange={_handleChange}
        />

        <Label>
          Nama <span className="text-danger">*Harus Diisi</span>
        </Label>
        <Input
          placeholder="Masukan Nama "
          style={{ width: "100%" }}
          className="mb-3"
          name="name"
          value={data.name}
          onChange={_handleChange}
          required
        />

        <Label>
          Aktif <span className="text-danger">*Harus Diisi</span>
        </Label>
        <Radio.Group
          className="mb-3"
          value={data.isActive}
          name="isActive"
          onChange={_handleChange}
          required
        >
          <Radio value={true}>Ya</Radio>
          <Radio value={false}>Tidak</Radio>
        </Radio.Group>

        <Label>
          Role <span className="text-danger">*Harus Diisi</span>
        </Label>
        <Select
          style={{ width: "100%" }}
          className="mb-3"
          name="role"
          value={data.role}
          onChange={value => onChange("role", value)}
        >
          <Option value={null} disabled>
            Pilih Role
          </Option>
          <Option value={roleName}>
            {roleName}
          </Option>
          {/* {Object.keys(USER_ROLE).map(key => (
            <Option key={USER_ROLE[key]} value={USER_ROLE[key]}>
              {USER_ROLE[key]}
            </Option>
          ))} */}
        </Select>

        {data.role === USER_ROLE.PHARMA_STAFF && (
          <React.Fragment>
            <Label>
              Pharma Role <span className="text-danger">*Harus Diisi</span>
            </Label>
            <Select
              style={{ width: "100%" }}
              className="mb-3"
              name="logisticrole"
              value={data.logisticrole}
              onChange={value => onChange("logisticrole", value)}
            >
              <Option value={null} disabled>
                Pilih Role
              </Option>
              <Option
                key={LOGISTIC_ROLE.PHARMA_ENTITY}
                value={LOGISTIC_ROLE.PHARMA_ENTITY}
              >
                Farmasi
              </Option>
              <Option
                key={LOGISTIC_ROLE.UPK_ENTITY}
                value={LOGISTIC_ROLE.UPK_ENTITY}
              >
                Layanan
              </Option>
            </Select>
          </React.Fragment>
        )}

        <Label>
          Password <span className="text-danger">*Harus Diisi</span>
        </Label>
        <Input
          placeholder="Masukan Password "
          style={{ width: "100%" }}
          className="mb-3"
          name="password"
          type="password"
          value={data.password}
          onChange={_handleChange}
          required
        />

        <Label>Email</Label>
        <Input
          placeholder="Masukan Email "
          style={{ width: "100%" }}
          className="mb-3"
          name="email"
          type="email"
          value={data.email}
          onChange={_handleChange}
        />

        <Label>
          Nomor Telepon <span className="text-danger">*Harus Diisi</span>
        </Label>
        <Input
          placeholder="Masukan Nomor Telepon "
          style={{ width: "100%" }}
          className="mb-3"
          name="phone"
          value={data.phone}
          onChange={_handleChange}
          required
        />

        {data.role === USER_ROLE.PROVINCE_STAFF && (
          <React.Fragment>
            <Label>Provinsi</Label>
            <ChooseProvince
              placeholder="Pilih Provinsi"
              style={{ width: "100%" }}
              className="ml-1 mb-3"
              name="provinceId"
              value={data.provinceId}
              onChange={value => onChange("provinceId", value)}
              required
            />
          </React.Fragment>
        )}

        {data.role === USER_ROLE.SUDIN_STAFF && (
          <React.Fragment>
            <Label>Sudin Kab/Kota</Label>
            <ChooseSudin
              placeholder="Pilih Sudin Kab/Kota"
              style={{ width: "100%" }}
              className="ml-1 mb-3"
              name="sudinKabKotaId"
              value={data.sudinKabKotaId}
              onChange={value => onChange("sudinKabKotaId", value)}
              required
            />
          </React.Fragment>
        )}

        {Object.values(upkRole).includes(data.role) && (
          <React.Fragment>
            <Label>UPK</Label>
            <ChooseUpk
              placeholder="Pilih Sudin UPK"
              style={{ width: "100%" }}
              className="ml-1 mb-3"
              name="upkId"
              value={data.upkId}
              onChange={value => onChange("upkId", value)}
              required
            />
          </React.Fragment>
        )}

        <Label>
          Alamat
        </Label>
        <Input.TextArea
          rows="4"
          placeholder="Masukan Alamat"
          style={{ width: "100%" }}
          className="mb-3"
          name="address"
          value={data.address}
          onChange={_handleChange}
          required
        />

        <div className="d-flex justify-content-end mt-2">
          <Button key="back" type="danger" onClick={onCancel} className="mr-2">
            Tutup
          </Button>
          <Button
            key="submit"
            type="primary"
            loading={isLoading}
            htmlType="submit"
          >
            Simpan
          </Button>
        </div>
      </form>
    </Modal>
  );
};

export default Form;
