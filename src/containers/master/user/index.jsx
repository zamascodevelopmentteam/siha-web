import React, { useEffect, useState } from "react";
import API from "utils/API";
import axios from "axios";

import { deleteConfirm } from "components";
import { openNotification } from "utils/Notification";
// import { useParams } from "react-router";
import View from "./view";

const Handler = ({ location }) => {
  const roleName = location.state.roleName

  let source = axios.CancelToken.source();

  // const [keyword, setKeyword] = useState("")
  const [data, setData] = useState([]);
  const [isLoading, setLoading] = useState(false);
  // const { type } = useParams();

  const _handleSearch = async v => {
    try {
      setLoading(true)
      // setKeyword(v)
      const res = await API.MasterData.User.listByRole(source.token, roleName, v)
      setData(res.data || [])
    } catch (ex) {
      console.error("e: ", ex);
    } finally {
      setLoading(false)
    }
  }

  const _loadData = () => {
    setLoading(true);
    API.MasterData.User.listByRole(source.token, roleName)
      .then(rsp => {
        setData(rsp.data || []);
      })
      .catch(e => {
        console.error("e: ", e);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const _onDelete = id => {
    setLoading(true);
    API.MasterData.User.delete(source.token, id)
      .then(() => {
        openNotification("success", "Berhasil", "Data berhasil dihapus");
        _loadData();
      })
      .catch(e => {
        console.error("e: ", e);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const _handleDelete = (id, name) => {
    deleteConfirm({
      onDelete: () => _onDelete(id),
      message: `Anda yakin akan menghapus data user ${name}`
    });
  };

  useEffect(() => {
    _loadData();

    return () => {
      source.cancel("request canceled");
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [roleName]);

  return (
    <View
      handleSearch={_handleSearch}
      data={data}
      isLoading={isLoading}
      reload={_loadData}
      handleDelete={_handleDelete}
      roleName={roleName}
    />
  );
};

export default Handler;
