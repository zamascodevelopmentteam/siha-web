import React, { useEffect, useState } from "react";
import API from "utils/API";
import axios from "axios";

import { deleteConfirm } from "components";
import { openNotification } from "utils/Notification";
import { useParams } from "react-router";
import View from "./view";

const Handler = () => {
  let source = axios.CancelToken.source();

  const [data, setData] = useState([]);
  const [isLoading, setLoading] = useState(false);
  const { type } = useParams();

  const _loadData = () => {
    setLoading(true);
    API.MasterData.Product.list(source.token)
      .then(rsp => {
        setData(rsp.data || []);
      })
      .catch(e => {
        console.error("e: ", e);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const _onDelete = id => {
    setLoading(true);
    API.MasterData.Product.delete(source.token, id)
      .then(() => {
        openNotification("success", "Berhasil", "Data berhasil dihapus");
        _loadData();
      })
      .catch(e => {
        console.error("e: ", e);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const _handleDelete = (id, name) => {
    deleteConfirm({
      onDelete: () => _onDelete(id),
      message: `Anda yakin akan menghapus data provinsi ${name}`
    });
  };

  useEffect(() => {
    _loadData();

    return () => {
      source.cancel("request canceled");
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [type]);

  return (
    <View
      data={data}
      isLoading={isLoading}
      reload={_loadData}
      handleDelete={_handleDelete}
    />
  );
};

export default Handler;
