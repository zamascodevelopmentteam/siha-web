import React from "react";
import { Button, Modal, Input, Select } from "antd";
import { Label } from "components";
import { ENUM } from "constant";

const { Option } = Select;
const { MEDICINE_TYPE } = ENUM;

const Form = ({ onOk, onCancel, isLoading, data, onChange }) => {
  const _handleChange = e => {
    onChange(e.target.name, e.target.value);
  };

  const _handleSubmit = e => {
    e.preventDefault();
    if (isLoading) {
      return;
    }
    onOk();
  };

  return (
    <Modal
      title="Tambah/Ubah Master Produsen"
      visible
      maskClosable={false}
      onOk={onOk}
      onCancel={onCancel}
      footer={null}
    >
      <form onSubmit={_handleSubmit}>
        <Label>Nama Produsen</Label>
        <Input
          placeholder="Masukan Nama Produsen"
          style={{ width: "100%" }}
          className="mb-3"
          name="name"
          value={data.name}
          onChange={_handleChange}
          required
        />

        <Label>Tipe</Label>
        <Select
          style={{ width: "100%" }}
          className="mb-3"
          name="medicineType"
          value={data.medicineType}
          onChange={value => {
            var e = {
              target: {
                name: "medicineType",
                value: value
              }
            };
            _handleChange(e);
          }}
          required
        >
          <Option value={null} disabled>
            Pilih Tipe 
          </Option>
          {Object.keys(MEDICINE_TYPE).map(key => (
            <Option key={MEDICINE_TYPE[key]} value={key}>
              {MEDICINE_TYPE[key]}
            </Option>
          ))}
        </Select>

        <div className="d-flex justify-content-end mt-2">
          <Button key="back" type="danger" onClick={onCancel} className="mr-2">
            Tutup
          </Button>
          <Button
            key="submit"
            type="primary"
            loading={isLoading}
            htmlType="submit"
          >
            Simpan
          </Button>
        </div>
      </form>
    </Modal>
  );
};

export default Form;
