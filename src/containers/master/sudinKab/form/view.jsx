import React from "react";
import { Button, Modal, Input, Radio } from "antd";
import { ChooseProvince, Label } from "components";
// import { ENUM, ROLE } from "constant";

const Form = ({ onOk, onCancel, isLoading, data, onChange }) => {
  const _handleChange = e => {
    onChange(e.target.name, e.target.value);
  };

  const _handleSubmit = e => {
    e.preventDefault();
    if (isLoading) {
      return;
    }
    onOk();
  };
  return (
    <Modal
      title="Tambah/Ubah Master Kab/Kota"
      visible
      maskClosable={false}
      onOk={onOk}
      onCancel={onCancel}
      footer={null}
    >
      <form onSubmit={_handleSubmit}>
        <Label>Nama Sudin Kab/Kota</Label>
        <Input
          placeholder="Masukan Nama Kab/Kota"
          style={{ width: "100%" }}
          className="mb-3"
          name="name"
          value={data.name}
          onChange={_handleChange}
          required
        />

        <Label>Nama Provinsi</Label>
        <ChooseProvince
          placeholder="Pilih Provinsi"
          style={{ width: "100%" }}
          className="ml-1 mb-3"
          name="provinceId"
          value={data.provinceId}
          onChange={value => onChange("provinceId", value)}
          required
        />

        <Label>Aktif Layanan PDP</Label>
        <Radio.Group
          className="mb-3"
          value={data.isActive}
          name="isActive"
          onChange={_handleChange}
          required
        >
          <Radio value={true}>Ya</Radio>
          <Radio value={false}>Tidak</Radio>
        </Radio.Group>

        <Label>Pengganda Pesanan *</Label>
        <Input
          placeholder="Masukan Pengganda Pesanan"
          style={{ width: "100%" }}
          className="mb-3"
          name="orderMultiplier"
          value={data.orderMultiplier}
          onChange={_handleChange}
          required
        />

        <Label>Alamat</Label>
        <Input.TextArea
          rows="4"
          placeholder="Masukan Alamat"
          style={{ width: "100%" }}
          className="mb-3"
          name="address"
          value={data.address}
          onChange={_handleChange}
          required
        />

        <div className="d-flex justify-content-end mt-2">
          <Button key="back" type="danger" onClick={onCancel} className="mr-2">
            Tutup
          </Button>
          <Button
            key="submit"
            type="primary"
            loading={isLoading}
            htmlType="submit"
          >
            Simpan
          </Button>
        </div>
      </form>
    </Modal>
  );
};

export default Form;
