import React from "react";
import { Button, Modal, Input, Select } from "antd";
import { ChooseMedicine, Label } from "components";
import { ENUM } from "constant";

const { Option } = Select;
const { PKG_UNIT_TYPE } = ENUM;

const Form = ({ onOk, onCancel, isLoading, data, onChange }) => {
  const _handleChange = e => {
    onChange(e.target.name, e.target.value);
  };

  const _handleSubmit = e => {
    e.preventDefault();
    if (isLoading) {
      return;
    }
    onOk();
  };
  return (
    <Modal
      title="Tambah/Ubah Master Brand"
      visible
      maskClosable={false}
      onOk={onOk}
      onCancel={onCancel}
      footer={null}
    >
      <form onSubmit={_handleSubmit}>
        <Label>Nama Brand *</Label>
        <Input
          placeholder="Masukan Nama Brand"
          style={{ width: "100%" }}
          className="mb-3"
          name="name"
          value={data.name}
          onChange={_handleChange}
          required
        />

        <Label>Obat *</Label>
        <ChooseMedicine
          placeholder="Pilih Obat"
          style={{ width: "calc(100% - 20px)" }}
          className="ml-1 mb-3"
          name="medicineId"
          value={data.medicineId}
          onChange={value => onChange("medicineId", value)}
          required
        />

        <Label>Tipe Paket Unit *</Label>
        <Select
          placeholder="Pilih Tipe Paket Unit"
          style={{ width: "100%" }}
          className="mb-3"
          name="packageUnitType"
          value={data.packageUnitType}
          onChange={value => onChange("packageUnitType", value)}
        >
          <Option value={null} disabled>
            Pilih Tipe Paket Unit
          </Option>
          {Object.keys(PKG_UNIT_TYPE).map(key => (
            <Option key={PKG_UNIT_TYPE[key]} value={PKG_UNIT_TYPE[key]}>
              {PKG_UNIT_TYPE[key]}
            </Option>
          ))}
        </Select>

        <Label>Pengkali Stok *</Label>
        <Input
          placeholder="Masukan Pengkali Stok"
          style={{ width: "100%" }}
          className="mb-3"
          name="packageMultiplier"
          value={data.packageMultiplier}
          onChange={_handleChange}
          required
        />

        <Label>Catatan</Label>
        <Input
          placeholder="Catatan"
          style={{ width: "100%" }}
          className="mb-3"
          name="notes"
          value={data.notes}
          onChange={_handleChange}
        />

        <div className="d-flex justify-content-end mt-2">
          <Button key="back" type="danger" onClick={onCancel} className="mr-2">
            Tutup
          </Button>
          <Button
            key="submit"
            type="primary"
            loading={isLoading}
            htmlType="submit"
          >
            Simpan
          </Button>
        </div>
      </form>
    </Modal>
  );
};

export default Form;
