import React, { useEffect, useState } from "react";
import API from "utils/API";
import axios from "axios";
import { openNotification } from "utils/Notification";

import View from "./view";

const emptyData = {
  name: null,
  packageUnitType: null,
  packageMultiplier: null,
  notes: null,
  medicineId: null
};

const Handler = ({ toggleModal, reload, id }) => {
  let source = axios.CancelToken.source();
  const isEdit = id !== null;

  const [data, setData] = useState(emptyData);
  const [isLoading, setLoading] = useState(false);

  const _handleChange = (name, value) => {
    setData({
      ...data,
      [name]: value
    });
  };

  const _onSubmit = () => {
    const payload = _constructPayload(data);
    if (isEdit) {
      _update(payload);
      return;
    }
    _create(payload);
  };

  const _loadData = () => {
    setLoading(true);
    API.MasterData.Brand.getByID(source.token, id)
      .then(rsp => {
        const data = _constructResponse(rsp.data);
        setData(data || emptyData);
      })
      .catch(e => {
        console.error("e: ", e);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const _create = payload => {
    setLoading(true);
    API.MasterData.Brand.create(source.token, payload)
      .then(() => {
        openNotification("success", "Berhasil", "Data berhasil diinput");
        reload();
        setLoading(false);
        toggleModal();
      })
      .catch(e => {
        openNotification("error", "Gagal", e.message || String(e.data ? e.data.message : "").replace(/_/g, " "));
        setLoading(false);
        console.error("e: ", e);
      });
  };

  const _update = payload => {
    setLoading(true);
    API.MasterData.Brand.update(source.token, id, payload)
      .then(() => {
        openNotification("success", "Berhasil", "Data berhasil diinput");
        reload();
        setLoading(false);
        toggleModal();
      })
      .catch(e => {
        openNotification("error", "Gagal", e.message || String(e.data ? e.data.message : "").replace(/_/g, " "));
        setLoading(false);
        console.error("e: ", e);
      });
  };

  useEffect(() => {
    if (isEdit) {
      _loadData();
    }

    return () => {
      source.cancel("request canceled");
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <View
      isLoading={isLoading}
      onCancel={toggleModal}
      onOk={_onSubmit}
      onChange={_handleChange}
      data={data}
    />
  );
};

export default Handler;

const _constructPayload = data => {
  const { name, packageUnitType, packageMultiplier, notes, medicineId } = data;

  return { name, packageUnitType, packageMultiplier, notes, medicineId };
};

const _constructResponse = res => {
  const { name, packageUnitType, packageMultiplier, notes, medicineId } = res;

  return {
    name,
    packageUnitType,
    packageMultiplier,
    notes,
    medicineId
  };
};
