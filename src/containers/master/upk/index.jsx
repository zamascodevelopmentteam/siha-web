import React, { useEffect, useState } from "react";
import API from "utils/API";
import axios from "axios";

import { deleteConfirm } from "components";
import { openNotification } from "utils/Notification";
import { useParams } from "react-router";
import View from "./view";

const Handler = () => {
  let source = axios.CancelToken.source();

  const [data, setData] = useState([]);
  const [filteredData, setFilteredData] = useState([]);
  const [keyword, setKeyword] = useState([]);
  const [isLoading, setLoading] = useState(false);
  const { type } = useParams();

  const _loadData = (keyword) => {
    setLoading(true);
    API.MasterData.Upk.list(source.token, 0, 1000, keyword)
      .then(rsp => {
        setData(rsp.data || []);
      })
      .catch(e => {
        console.error("e: ", e);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const _onDelete = id => {
    setLoading(true);
    API.MasterData.Upk.delete(source.token, id)
      .then(() => {
        openNotification("success", "Berhasil", "Data berhasil dihapus");
        _loadData();
      })
      .catch(e => {
        console.error("e: ", e);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const _handleDelete = (id, name) => {
    deleteConfirm({
      onDelete: () => _onDelete(id),
      message: `Anda yakin akan menghapus data ${name}`
    });
  };

  const _handleSearch = value => {
    // let dataa = [...data];
    // let filteredDataa = [...filteredData];

    // setKeyword(value);

    // if (value.length) {
    //   let lowerval = value.toLowerCase();
    //   filteredDataa = dataa.filter(item => {
    //     let isActive = lowerval.includes('tid') ? item.isActive === false : (lowerval.includes('akt') && !lowerval.includes('tid') ? item.isActive === true : false);
    //     let startsWithCondition =
    //       (item.name ? item.name.toLowerCase().startsWith(lowerval) : false) ||
    //       (item.sudinKabKota.name ? item.sudinKabKota.name.toLowerCase().startsWith(lowerval) : false) ||
    //       (item.orderMultiplier ? item.orderMultiplier.toString().toLowerCase().startsWith(lowerval) : false)
    //     let includesCondition =
    //       (item.name ? item.name.toLowerCase().includes(lowerval) : false) ||
    //       (item.sudinKabKota.name ? item.sudinKabKota.name.toLowerCase().includes(lowerval) : false) ||
    //       (item.orderMultiplier ? item.orderMultiplier.toString().toLowerCase().includes(lowerval) : false)
    //     if (isActive) {
    //       return isActive;
    //     } else if (startsWithCondition) {
    //       return startsWithCondition;
    //     } else if (!startsWithCondition && includesCondition) {
    //       return includesCondition;
    //     } else return null
    //   })

    //   setFilteredData(filteredDataa);
    // }

    // new
    _loadData(value);
    // end new
  };

  useEffect(() => {
    _loadData();

    return () => {
      source.cancel("request canceled");
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [type]);

  return (
    <View
      data={keyword.length ? filteredData : data}
      isLoading={isLoading}
      reload={_loadData}
      handleDelete={_handleDelete}
      onSearch={_handleSearch}
    />
  );
};

export default Handler;
