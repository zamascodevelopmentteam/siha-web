import React from "react";
import { Button, Modal, Input, Radio, Select, InputNumber } from "antd";
import { Label } from "components";
import { ENUM } from "constant";

const { Option, OptGroup } = Select;
const { MEDICINE_TYPE, PKG_UNIT_TYPE, MEDICINE_SEDIAAN, MEDICINE_CATEGORIES } = ENUM;

const Form = ({ onOk, onCancel, isLoading, data, onChange }) => {
  const _handleChange = e => {
    onChange(e.target.name, e.target.value);
  };

  const _handleSubmit = e => {
    e.preventDefault();
    if (isLoading) {
      return;
    }
    onOk();
  };
  return (
    <Modal
      title="Tambah/Ubah Master Obat"
      visible
      maskClosable={false}
      onOk={onOk}
      onCancel={onCancel}
      footer={null}
    >
      <form onSubmit={_handleSubmit}>
        <Label>Nama Obat *</Label>
        <Input
          placeholder="Masukan Nama Obat"
          style={{ width: "100%" }}
          className="mb-3"
          name="name"
          value={data.name}
          onChange={_handleChange}
          required
        />

        <Label>Kode Obat *</Label>
        <Input
          placeholder="Masukan Kode Obat"
          style={{ width: "100%" }}
          className="mb-3"
          name="codeName"
          value={data.codeName}
          onChange={_handleChange}
          required
        />

        <Label>Tipe Obat *</Label>
        <Select
          style={{ width: "100%" }}
          className="mb-3"
          name="medicineType"
          value={data.medicineType}
          onChange={value => onChange("medicineType", value)}
        >
          <Option value={null} disabled>
            Pilih Tipe Obat
          </Option>
          {Object.keys(MEDICINE_TYPE).map(key => (
            <Option key={MEDICINE_TYPE[key]} value={key}>
              {MEDICINE_TYPE[key]}
            </Option>
          ))}
        </Select>

        {data.medicineType === MEDICINE_TYPE.ARV && (
          <React.Fragment>
            {/* BEGIN ARV */}
            <Label>Lini Regimen 1</Label>
            <Radio.Group
              className="mb-3"
              value={data.isLini1}
              name="isLini1"
              onChange={_handleChange}
              required
            >
              <Radio value={true}>Ya</Radio>
              <Radio value={false}>Tidak</Radio>
            </Radio.Group>

            <Label>Lini Regimen 2</Label>
            <Radio.Group
              className="mb-3"
              value={data.isLini2}
              name="isLini2"
              onChange={_handleChange}
              required
            >
              <Radio value={true}>Ya</Radio>
              <Radio value={false}>Tidak</Radio>
            </Radio.Group>
            {/* END ARV */}
          </React.Fragment>
        )}

        {data.medicineType === MEDICINE_TYPE.NON_ARV && (
          <React.Fragment>
            {/* BEGIN NON ARV */}
            <Label>Reagen 1</Label>
            <Radio.Group
              className="mb-3"
              value={data.isR1}
              name="isR1"
              onChange={_handleChange}
              required
            >
              <Radio value={true}>Ya</Radio>
              <Radio value={false}>Tidak</Radio>
            </Radio.Group>

            <Label>Reagen 2</Label>
            <Radio.Group
              className="mb-3"
              value={data.isR2}
              name="isR2"
              onChange={_handleChange}
              required
            >
              <Radio value={true}>Ya</Radio>
              <Radio value={false}>Tidak</Radio>
            </Radio.Group>

            <Label>Reagen 3</Label>
            <Radio.Group
              className="mb-3"
              value={data.isR3}
              name="isR3"
              onChange={_handleChange}
              required
            >
              <Radio value={true}>Ya</Radio>
              <Radio value={false}>Tidak</Radio>
            </Radio.Group>

            <Label>Test Sifilis</Label>
            <Radio.Group
              className="mb-3"
              value={data.isSifilis}
              name="isSifilis"
              onChange={_handleChange}
              required
            >
              <Radio value={true}>Ya</Radio>
              <Radio value={false}>Tidak</Radio>
            </Radio.Group>

            <Label>Test Anak</Label>
            <Radio.Group
              className="mb-3"
              value={data.isAnak}
              name="isAnak"
              onChange={_handleChange}
              required
            >
              <Radio value={true}>Ya</Radio>
              <Radio value={false}>Tidak</Radio>
            </Radio.Group>

            <Label>Alat Kesehatan</Label>
            <Radio.Group
              className="mb-3"
              value={data.isAlkes}
              name="isAlkes"
              onChange={_handleChange}
              required
            >
              <Radio value={true}>Ya</Radio>
              <Radio value={false}>Tidak</Radio>
            </Radio.Group>

            {/* <Label>CD/VL4</Label>
            <Radio.Group
              className="mb-3"
              value={data.isCdVl}
              name="isCdVl"
              onChange={_handleChange}
              required
            >
              <Radio value={true}>Ya</Radio>
              <Radio value={false}>Tidak</Radio>
            </Radio.Group> */}

            <Label>VL</Label>
            <Radio.Group
              className="mb-3"
              value={data.isVl}
              name="isVl"
              onChange={_handleChange}
              required
            >
              <Radio value={true}>Ya</Radio>
              <Radio value={false}>Tidak</Radio>
            </Radio.Group>

            <Label>CD4</Label>
            <Radio.Group
              className="mb-3"
              value={data.isCd4}
              name="isCd4"
              onChange={_handleChange}
              required
            >
              <Radio value={true}>Ya</Radio>
              <Radio value={false}>Tidak</Radio>
            </Radio.Group>

            <Label>IO IMS</Label>
            <Radio.Group
              className="mb-3"
              value={data.isIoIms}
              name="isIoIms"
              onChange={_handleChange}
              required
            >
              <Radio value={true}>Ya</Radio>
              <Radio value={false}>Tidak</Radio>
            </Radio.Group>

            <Label>Preventif</Label>
            <Radio.Group
              className="mb-3"
              value={data.isPreventif}
              name="isPreventif"
              onChange={_handleChange}
              required
            >
              <Radio value={true}>Ya</Radio>
              <Radio value={false}>Tidak</Radio>
            </Radio.Group>

            {/* END NON ARV */}
          </React.Fragment>
        )}

        {(data.isCd4 || data.isVl) && (
          <React.Fragment>
            <Label>Kategori VL / CD4 *</Label>
            <Select
              placeholder="Pilih Kategori"
              style={{ width: "100%" }}
              className="mb-3"
              name="vlcd4Category"
              value={data.vlcd4Category}
              onChange={value => onChange("vlcd4Category", value)}
            >
              <Option value={null} disabled>
                Pilih Kategori
              </Option>
              {
                Object.keys(ENUM.VLCD4CATEGORY_OPTIONS).map(key => {
                  return (
                    <OptGroup label={key}>
                      {
                        Object.keys(ENUM.VLCD4CATEGORY_OPTIONS[key]).map(kk => (
                          <Option key={ENUM.VLCD4CATEGORY[kk]} value={ENUM.VLCD4CATEGORY[kk]}>
                            {ENUM.VLCD4CATEGORY[kk]}
                          </Option>
                        ))
                      }
                    </OptGroup>
                  )
                })
              }
            </Select>
          </React.Fragment>
        )}

        <Label>Tipe Stok Unit *</Label>
        <Select
          placeholder="Pilih Tipe Stok Unit"
          style={{ width: "100%" }}
          className="mb-3"
          name="stockUnitType"
          value={data.stockUnitType}
          onChange={value => onChange("stockUnitType", value)}
        >
          <Option value={null} disabled>
            Pilih Tipe Stok Unit
          </Option>
          {Object.keys(PKG_UNIT_TYPE).map(key => (
            <Option key={PKG_UNIT_TYPE[key]} value={PKG_UNIT_TYPE[key]}>
              {PKG_UNIT_TYPE[key]}
            </Option>
          ))}
        </Select>

        <Label>Tipe Paket Unit *</Label>
        <Select
          placeholder="Pilih Tipe Paket Unit"
          style={{ width: "100%" }}
          className="mb-3"
          name="packageUnitType"
          value={data.packageUnitType}
          onChange={value => onChange("packageUnitType", value)}
        >
          <Option value={null} disabled>
            Pilih Tipe Paket Unit
          </Option>
          {Object.keys(PKG_UNIT_TYPE).map(key => (
            <Option key={PKG_UNIT_TYPE[key]} value={PKG_UNIT_TYPE[key]}>
              {PKG_UNIT_TYPE[key]}
            </Option>
          ))}
        </Select>

        <Label>Pengkali Stok *</Label>
        <Input
          placeholder="Masukan Pengkali Stok"
          style={{ width: "100%" }}
          className="mb-3"
          name="packageMultiplier"
          value={data.packageMultiplier}
          onChange={_handleChange}
          required
        />

        <Label>Bahan Dasar</Label>
        <Input
          placeholder="Masukan Bahan Dasar"
          style={{ width: "100%" }}
          className="mb-3"
          name="ingredients"
          value={data.ingredients}
          onChange={_handleChange}
        />

        <Label>Sediaan</Label>
        <Select
          placeholder="Pilih Tipe Paket Unit"
          style={{ width: "100%" }}
          className="mb-3"
          name="sediaan"
          value={data.sediaan}
          onChange={value => onChange("sediaan", value)}
        >
          <Option value={null} disabled>
            Pilih Sediaan
          </Option>
          {Object.keys(MEDICINE_SEDIAAN).map(key => (
            <Option key={MEDICINE_SEDIAAN[key]} value={MEDICINE_SEDIAAN[key]}>
              {MEDICINE_SEDIAAN[key]}
            </Option>
          ))}
        </Select>

        <Label>Harga per Paket</Label>
        <InputNumber
          placeholder="Contoh: 10000"
          style={{ width: "100%" }}
          className="mb-3"
          name="unitPrice"
          value={data.unitPrice}
          onChange={value => onChange("unitPrice", value)}
          required
        />

        <Label>URL Gambar</Label>
        <Input
          placeholder="Masukan URL Gambar"
          style={{ width: "100%" }}
          className="mb-3"
          name="imageUrl"
          value={data.imageUrl}
          onChange={_handleChange}
        />

        <Label>Kategori</Label>
        <Select
          // placeholder="Pilih Tipe Paket Unit"
          style={{ width: "100%" }}
          className="mb-3"
          name="category"
          value={data.category}
          onChange={value => onChange("category", value)}
        >
          <Option value={null} disabled>
            Pilih Kategori
          </Option>
          {Object.keys(MEDICINE_CATEGORIES).map(key => (
            <Option key={MEDICINE_CATEGORIES[key]} value={MEDICINE_CATEGORIES[key]}>
              {MEDICINE_CATEGORIES[key]}
            </Option>
          ))}
        </Select>

        <div className="d-flex justify-content-end mt-2">
          <Button key="back" type="danger" onClick={onCancel} className="mr-2">
            Tutup
          </Button>
          <Button
            key="submit"
            type="primary"
            loading={isLoading}
            htmlType="submit"
          >
            Simpan
          </Button>
        </div>
      </form>
    </Modal>
  );
};

export default Form;
