import React, { useEffect, useState } from "react";
import API from "utils/API";
import axios from "axios";
import { openNotification } from "utils/Notification";

import View from "./view";
import { ENUM } from "constant";

const emptyMedicine = {
  name: null,
  codeName: null,
  stockUnitType: null,
  packageUnitType: null,
  packageMultiplier: null,
  ingredients: null,
  sediaan: null,
  unitPrice: null,
  imageUrl: "https://picsum.photos/200",
  isR1: null,
  isR2: null,
  isR3: null,
  isSifilis: null,
  isAnak: null,
  isAlkes: null,
  isIoIms: null,
  isCdVl: null,
  isVl: null,
  isCd5: null,
  isPreventif: null,
  isLini1: null,
  isLini2: null,
  medicineType: null,
  vlcd4Category: null
};

const Handler = ({ toggleModal, reload, id }) => {
  let source = axios.CancelToken.source();
  const isEdit = id !== null;

  const [data, setData] = useState(emptyMedicine);
  const [isLoading, setLoading] = useState(false);

  const _loadData = () => {
    setLoading(true);
    API.MasterData.Medicine.getByID(source.token, id)
      .then(rsp => {
        const data = _constructResponse(rsp.data);
        setData(data || emptyMedicine);
      })
      .catch(e => {
        console.error("e: ", e);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const _handleChange = (name, value) => {
    setData({
      ...data,
      [name]: value
    });
  };

  const _onSubmit = () => {
    const payload = _constructPayload(data);
    if (isEdit) {
      _update(payload);
      return;
    }
    _create(payload);
  };

  const _create = payload => {
    setLoading(true);
    API.MasterData.Medicine.create(source.token, payload)
      .then(() => {
        openNotification("success", "Berhasil", "Data berhasil diinput");
        reload();
        setLoading(false);
        toggleModal();
      })
      .catch(e => {
        openNotification("error", "Gagal", e.message || String(e.data ? e.data.message : "").replace(/_/g, " "));
        setLoading(false);
        console.error("e: ", e);
      });
  };

  const _update = payload => {
    setLoading(true);
    API.MasterData.Medicine.update(source.token, id, payload)
      .then(() => {
        openNotification("success", "Berhasil", "Data berhasil diinput");
        reload();
        setLoading(false);
        toggleModal();
      })
      .catch(e => {
        openNotification("error", "Gagal", e.message || String(e.data ? e.data.message : "").replace(/_/g, " "));
        setLoading(false);
        console.error("e: ", e);
      });
  };

  useEffect(() => {
    if (isEdit) {
      _loadData();
    }

    return () => {
      source.cancel("request canceled");
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <View
      isLoading={isLoading}
      onCancel={toggleModal}
      onOk={_onSubmit}
      onChange={_handleChange}
      data={data}
    />
  );
};

export default Handler;

const _constructPayload = data => {
  if (data.medicineType === ENUM.MEDICINE_TYPE.ARV) {
    data.isR1 = null;
    data.isR2 = null;
    data.isR3 = null;
    data.isSifilis = null;
    data.isAnak = null;
    data.isAlkes = null;
    data.isIoIms = null;
    data.isCdVl = null;
    data.isVl = null;
    data.isCd4 = null;
    data.isPreventif = null;
  }

  if (data.medicineType === ENUM.MEDICINE_TYPE.NON_ARV) {
    data.isLini1 = null;
    data.isLini2 = null;
  }

  return data;
};

const _constructResponse = res => {
  const {
    name,
    codeName,
    stockUnitType,
    packageUnitType,
    packageMultiplier,
    ingredients,
    sediaan,
    unitPrice,
    imageUrl,
    medicineType,
    arvMedicine,
    nonArvMedicine,
    category
  } = res;
  let data = {
    name,
    codeName,
    stockUnitType,
    packageUnitType,
    packageMultiplier,
    ingredients,
    sediaan,
    unitPrice,
    imageUrl,
    medicineType,
    isR1: null,
    isR2: null,
    isR3: null,
    isSifilis: null,
    isAnak: null,
    isAlkes: null,
    isIoIms: null,
    isCdVl: null,
    isVl: null,
    isCd4: null,
    isPreventif: null,
    isLini1: null,
    isLini2: null,
    category
  };

  if (arvMedicine !== null) {
    const { isLini1, isLini2 } = arvMedicine;
    data = {
      ...data,
      isLini1,
      isLini2
    };
  }
  if (nonArvMedicine !== null) {
    const {
      isR1,
      isR2,
      isR3,
      isSifilis,
      isAnak,
      isAlkes,
      isIoIms,
      isCdVl,
      isVl,
      isCd4,
      isPreventif,
      vlcd4Category
    } = nonArvMedicine;
    data = {
      ...data,
      isR1,
      isR2,
      isR3,
      isSifilis,
      isAnak,
      isAlkes,
      isIoIms,
      isCdVl,
      isVl,
      isCd4,
      isPreventif,
      vlcd4Category
    };
  }

  return data;
};
