import React, { useEffect, useState } from "react";
import API from "utils/API";
import axios from "axios";

import { deleteConfirm } from "components";
import { openNotification } from "utils/Notification";
import { useParams } from "react-router";
import View from "./view";

const Handler = () => {
  let source = axios.CancelToken.source();

  const [data, setData] = useState([]);
  const [filteredData, setFilteredData] = useState([]);
  const [keyword, setKeyword] = useState([]);
  const [isLoading, setLoading] = useState(false);
  const { type } = useParams();

  const _loadData = () => {
    setLoading(true);
    API.MasterData.Medicine.list(source.token)
      .then(rsp => {
        setData(rsp.data || []);
      })
      .catch(e => {
        console.error("e: ", e);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const _onDelete = id => {
    setLoading(true);
    API.MasterData.Medicine.delete(source.token, id)
      .then(() => {
        openNotification("success", "Berhasil", "Data berhasil dihapus");
        _loadData();
      })
      .catch(e => {
        console.error("e: ", e);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const _handleDelete = (id, name) => {
    deleteConfirm({
      onDelete: () => _onDelete(id),
      message: `Anda yakin akan menghapus data obat ${name}`
    });
  };

  const _handleSearch = value => {
    console.log(value);
    let dataa = [...data];
    let filteredDataa = [...filteredData];

    setKeyword(value);

    if (value.length) {
      let lowerval = value.toLowerCase();
      filteredDataa = dataa.filter(item => {
        let medicineType = lowerval.includes('non') ? item.medicineType === "NON_ARV" : (lowerval === "arv" ? item.medicineType === "ARV" : false);
        let startsWithCondition =
          (item.name ? item.name.toLowerCase().startsWith(lowerval) : false) ||
          (item.codeName ? item.codeName.toLowerCase().startsWith(lowerval) : false) ||
          // (item.medicineType ? (lowerval.includes('non') ? item.medicineType.toLowerCase() === 'non_arv' : item.medicineType.toLowerCase() === 'arv') : false) ||
          (item.stockUnitType ? item.stockUnitType.toLowerCase().startsWith(lowerval) : false) ||
          (item.packageUnitType ? item.packageUnitType.toLowerCase().startsWith(lowerval) : false) ||
          (item.packageMultiplier ? item.packageMultiplier.toString().toLowerCase().startsWith(lowerval) : false) ||
          (item.sediaan ? item.sediaan.toLowerCase().startsWith(lowerval) : false) ||
          (item.unitPrice ? item.unitPrice.toLowerCase().startsWith(lowerval) : false)
        let includesCondition =
          (item.name ? item.name.toLowerCase().includes(lowerval) : false) ||
          (item.codeName ? item.codeName.toLowerCase().includes(lowerval) : false) ||
          // (item.medicineType ? item.medicineType.toLowerCase().includes(lowerval) : false) ||
          (item.stockUnitType ? item.stockUnitType.toLowerCase().includes(lowerval) : false) ||
          (item.packageUnitType ? item.packageUnitType.toLowerCase().includes(lowerval) : false) ||
          (item.packageMultiplier ? item.packageMultiplier.toString().toLowerCase().includes(lowerval) : false) ||
          (item.sediaan ? item.sediaan.toLowerCase().includes(lowerval) : false) ||
          (item.unitPrice ? item.unitPrice.toLowerCase().includes(lowerval) : false)
        if (medicineType) {
          return medicineType;
        } else if (startsWithCondition) {
          return startsWithCondition;
        } else if (!startsWithCondition && includesCondition) {
          return includesCondition;
        } else return null
      })

      setFilteredData(filteredDataa);
    }
  };

  useEffect(() => {
    _loadData();

    return () => {
      source.cancel("request canceled");
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [type]);

  return (
    <View
      data={keyword.length ? filteredData : data}
      isLoading={isLoading}
      reload={_loadData}
      handleDelete={_handleDelete}
      onSearch={_handleSearch}
    />
  );
};

export default Handler;
