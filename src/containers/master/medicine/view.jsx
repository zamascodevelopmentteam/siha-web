import React, { useState } from "react";

import { DataTable, DisplayDate } from "components";
import { Button } from "antd";
import Form from "./form";
import { formatNumber } from "utils";

const View = ({ data, isLoading, reload, handleDelete, onSearch }) => {
  const [isFormOpen, setFormOpen] = useState(false);
  const [id, setID] = useState(null);

  const _toggleModal = (id = null) => {
    if (typeof id === "number") {
      setID(id);
    } else {
      setID(null);
    }
    setFormOpen(!isFormOpen);
  };

  const columns = [
    {
      title: "Image",
      dataIndex: "imageUrl",
      key: "imageUrl"
    },
    {
      title: "Nama Obat",
      dataIndex: "name",
      key: "name"
    },
    {
      title: "Kode Obat",
      dataIndex: "codeName",
      key: "codeName"
    },
    {
      title: "Tipe Obat",
      dataIndex: "medicineType",
      key: "medicineType"
    },
    {
      title: "Tipe Stok Unit",
      dataIndex: "stockUnitType",
      key: "stockUnitType"
    },
    {
      title: "Tipe Paket Unit",
      dataIndex: "packageUnitType",
      key: "packageUnitType"
    },
    {
      title: "Pengkali Stok",
      dataIndex: "packageMultiplier",
      key: "packageMultiplier"
    },
    {
      title: "Bahan Dasar",
      dataIndex: "ingredients",
      key: "ingredients"
    },
    {
      title: "Sediaan",
      dataIndex: "sediaan",
      key: "sediaan"
    },
    {
      title: "Harga per Paket",
      dataIndex: "unitPrice",
      key: "unitPrice",
      render: r => formatNumber(Number(r))
    },
    {
      title: "Terakhir Diubah",
      dataIndex: "updatedAt",
      key: "updatedAt",
      render: date => <DisplayDate date={date} />
    },
    {
      title: "Diedit Oleh",
      dataIndex: "updatedByData",
      key: "updatedBy",
      render: updatedByData => (updatedByData ? updatedByData.updatedName : "-")
    },
    {
      title: "Aksi",
      key: "action",
      fixed: "right",
      render: item => (
        <React.Fragment>
          <Button
            size="small"
            type="primary"
            className="mr-2"
            onClick={() => _toggleModal(item.id)}
          >
            Ubah
          </Button>
          <Button
            size="small"
            type="danger"
            onClick={() => handleDelete(item.id, item.name)}
          >
            Hapus
          </Button>
        </React.Fragment>
      )
    }
  ];

  return (
    <React.Fragment>
      {isFormOpen && (
        <Form toggleModal={_toggleModal} reload={reload} id={id} />
      )}
      <div className="container h-100 pt-4">
        <div className="row">
          <div className="col-12 bg-white rounded p-4 border">
            <DataTable
              title="Master Obat ARV dan Non-ARV"
              columns={columns}
              data={data}
              isLoading={isLoading}
              isScrollX
              button={
                <Button type="primary" onClick={_toggleModal}>
                  Tambah Baru
                </Button>
              }
              onSearch={onSearch}
            />
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default View;
