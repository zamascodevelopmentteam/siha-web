import React, { useEffect, useState } from "react";
import { cloneDeep } from "lodash";
import API from "utils/API";
import axios from "axios";
import shortid from "shortid";
import { openNotification } from "utils/Notification";

import { ModalPengiriman } from "components";
import { ROLE } from "constant";

const itemEmptyState = {
  packageQuantity: null,
  quantityAvailable: null,
  packageUnitType: null,
  inventoryId: null
};

const emptyData = {
  droppingNumber: null,
  logisticRole: null,
  provinceId: null,
  sudinKabKotaId: null,
  upkId: null,
  notes: null
};

const Handler = ({ toggleModal, reload }) => {
  let source = axios.CancelToken.source();

  const [data, setData] = useState(emptyData);
  const [selectedDest, setSelectedDest] = useState({
    id: 0,
    type: ""
  });
  const [isLoading, setLoading] = useState(false);
  const [items, setItems] = useState([
    { id: shortid.generate(), ...itemEmptyState }
  ]);

  const _addItem = () => {
    const newItem = {
      id: shortid.generate(),
      ...itemEmptyState
    };
    setItems([...items, newItem]);
  };

  const _removeItem = idx => {
    items.splice(idx, 1);
    setItems([...items]);
  };

  const _handleChangeItem = (idx, name, value) => {
    let newItems = [...items];
    newItems[idx][name] = value;

    setItems(newItems);
  };

  const _handleChange = (name, value) => {
    setData({
      ...data,
      [name]: value
    });
  };

  const _handleChangeDestination = ({ itemId, type, logisticRole }) => {
    switch (type) {
      case "upkid":
        setSelectedDest({ id: itemId, type: "upkId" });
        setData({
          ...data,
          sudinKabKotaId: null,
          provinceId: null,
          upkId: itemId,
          logisticRole: logisticRole
        });
        break;
      case "sudinkotakabid":
        setSelectedDest({ id: itemId, type: "sudinKabKotaId" });
        setData({
          ...data,
          sudinKabKotaId: itemId,
          provinceId: null,
          upkId: null
        });
        break;
      case "provinceid":
        setSelectedDest({ id: itemId, type: "provinceId" });
        setData({
          ...data,
          sudinKabKotaId: null,
          provinceId: itemId,
          upkId: null
        });
        break;
      default:
        break;
    }
  };

  const _onSubmit = () => {
    const payload = _constructPayload(data, items);
    setLoading(true);
    API.Logistic.createPengiriman(source.token, payload)
      .then(() => {
        setLoading(false);
        openNotification("success", "Berhasil", "Data berhasil diinput");
        reload();
        toggleModal();
      })
      .catch(e => {
        openNotification("error", "Gagal", e.message || String(e.data ? e.data.message : "").replace(/_/g, " "));
        console.error("e: ", e);
        setLoading(false);
      });
  };

  useEffect(() => {
    return () => {
      source.cancel("request canceled");
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <ModalPengiriman
      onCancel={toggleModal}
      onOk={_onSubmit}
      isLoading={isLoading}
      onChange={_handleChange}
      role={ROLE.MINISTRY_STAFF}
      selectedDest={selectedDest}
      data={data}
      items={items}
      addItem={_addItem}
      removeItem={_removeItem}
      onChangeItem={_handleChangeItem}
      onChangeDestination={_handleChangeDestination}
    />
  );
};

export default Handler;

const _constructPayload = (data, items) => {
  const newItems = cloneDeep(items);
  const newData = { ...data };
  for (let x = 0; x < newItems.length; x++) {
    delete newItems[x].id;
  }

  delete newData.id;

  return {
    distributionPlans: newData,
    distributionPlansItems: newItems
  };
};
