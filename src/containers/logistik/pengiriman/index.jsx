import React from "react";

import { useParams } from "react-router";
import List from "./list";
// import Detail from "./detail";
import Detail from "containers/logistik/detailDistribusi";

const Permintaan = () => {
  const { id } = useParams();

  return (
    <React.Fragment>
      {!id && <List />}
      {id && <Detail />}
    </React.Fragment>
  );
};

export default Permintaan;
