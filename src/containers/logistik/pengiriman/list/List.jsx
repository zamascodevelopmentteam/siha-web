import React, { useState, useContext } from "react";

import { DataTable, DisplayDate } from "components";
import { Link } from "react-router-dom";
import {
  ROLE,
  ENUM,
  TAG_LABEL,
  LIST_PLAN_STATUS,
  LIST_JENIS_PENGIRIMAN,
  LIST_RELOKASI
} from "constant";
import { Button, Tag, Typography, Select } from "antd";
import GlobalContext from "containers/GlobalContext";
import FormPengiriman from "containers/logistik/pengiriman/form";
const { Option } = Select;

export const List = ({
  data,
  isLoading,
  reload,
  onChangeStatus,
  status,
  onChangeDist,
  distributionType,
  onChangeRelokasi,
  relokasi
}) => {
  const [isFormOpen, setFormOpen] = useState(false);
  const user = useContext(GlobalContext);
  // const isUpk = user.logisticrole === ENUM.LOGISTIC_ROLE.UPK_ENTITY;

  const _toggleModal = () => {
    setFormOpen(!isFormOpen);
  };

  const columns = [
    {
      title: "Invoice Number",
      dataIndex: "droppingNumber",
      key: "droppingNumber"
    },
    {
      title: "Status Approval",
      dataIndex: "approvalStatus",
      key: "approvalStatus",
      render: approvalStatus => (
        <Tag color={TAG_LABEL.TAG_APPROVAL_STATUS[approvalStatus]}>
          {
            user.role !== ROLE.PHARMA_STAFF ?
              TAG_LABEL.LABEL_APPROVAL_STATUS[approvalStatus] : TAG_LABEL.LABEL_APPROVAL_STATUS[approvalStatus].replace('Pengelola Program', 'Kepala Faskes')
          }
        </Tag>
      )
    },
    {
      title: "Status Pengiriman",
      dataIndex: "planStatus",
      key: "planStatus",
      render: planStatus => (
        <Tag color={TAG_LABEL.TAG_PLAN_STATUS[planStatus]}>
          {TAG_LABEL.LABEL_PLAN_STATUS[planStatus]}
        </Tag>
      )
    },
    {
      title: "Dikirim Ke",
      dataIndex: "recieverName",
      key: "recieverName",
      render: (r, i) => user.logisticrole === ENUM.LOGISTIC_ROLE.UPK_ENTITY ?
        (
          i.logisticRole === ENUM.LOGISTIC_ROLE.PHARMA_ENTITY ?
            "Gudang Farmasi" : (
              i.logisticRole === ENUM.LOGISTIC_ROLE.LAB_ENTITY ?
                'Gudang Laboratorium' : r
            )
        ) : r
    },
    {
      title: "Tanggal dibuat",
      dataIndex: "createdAt",
      key: "createdAt",
      render: date => <DisplayDate date={date} />
    },
    {
      title: "Terakhir Diubah",
      dataIndex: "updatedAt",
      key: "updatedAt",
      render: date => <DisplayDate date={date} />
    },
    {
      title: "Aksi",
      key: "action",
      fixed: "right",
      render: item => {
        return (
          <Link
            to={`/pharma/pengiriman/${item.id}`}
            className="ant-btn hs-btn PHARMA_STAFF ant-btn-primary ant-btn-sm"
          >
            Detail
          </Link>
        );
      }
    }
  ];

  return (
    <React.Fragment>
      {isFormOpen && (
        <FormPengiriman toggleModal={_toggleModal} reload={reload} />
      )}
      <div className="container h-100 pt-4">
        <div className="row mb-3">
          <div className="col-2 pl-0">
            <Typography
              fontSize="18px"
              fontWeight="bold"
              color="PHARMA_STAFF"
              className="d-block mb-3"
            >
              Status Pengiriman
            </Typography>
            <Select
              style={{ width: "100%" }}
              onChange={onChangeStatus}
              value={status}
            >
              {LIST_PLAN_STATUS.map(item => (
                <Option key={item} value={item}>
                  <Tag color={TAG_LABEL.TAG_PLAN_STATUS[item]}>
                    {TAG_LABEL.LABEL_PLAN_STATUS[item]}
                  </Tag>
                </Option>
              ))}
            </Select>
          </div>
          <div className="col-2 pl-1">
            <Typography
              fontSize="18px"
              fontWeight="bold"
              color="PHARMA_STAFF"
              className="d-block mb-3"
            >
              Jenis Distribusi
            </Typography>
            <Select
              style={{ width: "100%" }}
              onChange={onChangeDist}
              value={distributionType}
            >
              {LIST_JENIS_PENGIRIMAN.map(item => (
                <Option key={item} value={item}>
                  <Tag color={TAG_LABEL.TAG_JENIS_PENGIRIMAN[item]}>
                    {TAG_LABEL.LABEL_JENIS_PENGIRIMAN[item]}
                  </Tag>
                </Option>
              ))}
            </Select>
          </div>
          <div className="col-2 pl-1">
            <Typography
              fontSize="18px"
              fontWeight="bold"
              color="PHARMA_STAFF"
              className="d-block mb-3"
            >
              Relokasi
            </Typography>
            <Select
              style={{ width: "100%" }}
              onChange={onChangeRelokasi}
              value={relokasi}
            >
              {LIST_RELOKASI.map(item => (
                <Option key={item} value={item}>
                  <Tag color={TAG_LABEL.TAG_RELOKASI[item]}>
                    {TAG_LABEL.LABEL_RELOKASI[item]}
                  </Tag>
                </Option>
              ))}
            </Select>
          </div>
        </div>
        <div className="row">
          <div className="col-12 bg-white rounded p-4 border">
            <DataTable
              title="Daftar Pengiriman"
              columns={columns}
              data={data}
              isLoading={isLoading}
              isScrollX
              button={<Button onClick={_toggleModal}>Tambah Pengiriman</Button>}
            />
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};
