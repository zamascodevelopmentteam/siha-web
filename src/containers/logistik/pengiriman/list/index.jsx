import React, { useEffect, useState } from "react";
import API from "utils/API";
import axios from "axios";

import { List } from "./List";

const Handler = () => {
  let source = axios.CancelToken.source();

  const [data, setData] = useState([]);
  const [isLoading, setLoading] = useState(false);
  const [status, setStatus] = useState("ALL");
  const [distributionType, setDistributionType] = useState("ALL");
  const [relokasi, setRelokasi] = useState("ALL");

  const _handleChangeStatus = value => {
    setStatus(value);
  };

  const _handleChangeDistributionType = value => {
    setDistributionType(value);
  };

  const _handleChangeRelokasi = value => {
    setRelokasi(value);
  };

  const _loadData = () => {
    setLoading(true);
    API.Logistic.listDistributionPlan(
      source.token,
      status,
      distributionType,
      relokasi
    )
      .then(rsp => {
        setData(rsp.data || []);
      })
      .catch(e => {
        console.error("e: ", e);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  useEffect(() => {
    _loadData();

    return () => {
      source.cancel("request canceled");
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [status, distributionType, relokasi]);

  return (
    <List
      data={data}
      isLoading={isLoading}
      reload={_loadData}
      onChangeStatus={_handleChangeStatus}
      status={status}
      onChangeDist={_handleChangeDistributionType}
      distributionType={distributionType}
      onChangeRelokasi={_handleChangeRelokasi}
      relokasi={relokasi}
    />
  );
};

export default Handler;
