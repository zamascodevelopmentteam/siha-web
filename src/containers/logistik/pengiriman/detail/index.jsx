import React, { useEffect, useState } from "react";
import API from "utils/API";
import axios from "axios";

import { useParams } from "react-router";
import { Detail } from "./Detail";

const orderDetailEmptyState = {
  id: "",
  droppingNumber: "",
  picId: "",
  fundSource: "",
  planStatus: "",
  logisticRole: "",
  createdBy: "",
  updatedBy: "",
  provinceId: "",
  sudinKabKotaId: "",
  upkId: "",
  createdAt: "",
  updatedAt: "",
  deletedAt: "",
  orderId: "",
  senderName: "",
  recieverName: "",
  distribution_plans_items: []
};

const Handler = () => {
  let source = axios.CancelToken.source();

  const [data, setData] = useState(orderDetailEmptyState);
  const [isLoading, setLoading] = useState(false);
  const { type, id } = useParams();

  const _loadData = () => {
    setLoading(true);

    API.Logistic.getDistributionPlanByID(source.token, id)
      .then(rsp => {
        setData(rsp.data || orderDetailEmptyState);
      })
      .catch(e => {
        console.error("e: ", e);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  useEffect(() => {
    _loadData();

    return () => {
      source.cancel("request canceled");
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [type]);

  return <Detail data={data} isLoading={isLoading} />;
};

export default Handler;

const data = [
  {
    orderItemId: 3,
    packageUnitType: "BOTOL",
    packageQuantity: 5,
    expiredDate: "2020-10-22T00:00:00.000Z",
    notes: "5",
    medicine: {
      name: "Nevirapine",
      codeName: "NVP(200)"
    },
    inventory: null
  },
  {
    orderItemId: 2,
    packageUnitType: "BOTOL",
    packageQuantity: 4,
    expiredDate: "2020-10-22T00:00:00.000Z",
    notes: "4",
    medicine: {
      name: "Lamividine",
      codeName: "3TC(150)"
    },
    inventory: null
  },
  {
    orderItemId: 1,
    packageUnitType: "BOTOL",
    packageQuantity: 5,
    expiredDate: "2020-10-22T00:00:00.000Z",
    notes: "5",
    medicine: {
      name: "Zidovudine",
      codeName: "ZDV(100)"
    },
    inventory: null
  }
];
