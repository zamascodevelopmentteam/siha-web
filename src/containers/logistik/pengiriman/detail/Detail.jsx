import React, { useState, useContext } from "react";

import { DataTable, Typography, DisplayDate } from "components";
import { useParams } from "react-router";
import { ROLE, ENUM, TAG_LABEL } from "constant";
import { Button, Tag } from "antd";

export const Detail = ({ data, isLoading }) => {
  const user = useContext(GlobalContext);
  const [isConfirmOpen, setConfirmOpen] = useState(false);

  const _toggleModal = () => {
    setConfirmOpen(!isConfirmOpen);
  };

  const _onFilter = value => {
    console.log("value: ", value);
  };

  const { type } = useParams();

  return (
    <React.Fragment>
      <div className="container h-100 pt-4">
        <div className="row">
          <div className="col-12 p-4">
            <Typography
              fontSize="20px"
              fontWeight="bold"
              className="d-block mb-3"
            >
              Detail Pengiriman
            </Typography>
          </div>
        </div>
        <div className="row">
          <div className="col-12 bg-white rounded p-4 border">
            <Label text="Nomor Surat Jalan" />
            <Info>{data.droppingNumber}</Info>

            <Label text="Pengirim" />
            <Info>{data.senderName}</Info>

            <Label text="Terakhir Diubah" />
            <Info>
              <DisplayDate date={data.updatedAt} />
            </Info>

            <Typography
              fontSize="14px"
              fontWeight="bold"
              className="d-block mb-3"
            >
              Status
            </Typography>

            <Label text="Status Penerimaan" />
            <Info>
              <Tag color={TAG_LABEL.TAG_PLAN_STATUS[data.planStatus]}>
                {TAG_LABEL.LABEL_PLAN_STATUS[data.planStatus]}
              </Tag>
            </Info>

            <Label text="Status Approval" />
            <Info>
              <Tag color={TAG_LABEL.TAG_APPROVAL_STATUS[data.approvalStatus]}>
                {
                  user.role !== ROLE.PHARMA_STAFF ?
                    TAG_LABEL.LABEL_APPROVAL_STATUS[data.approvalStatus] : TAG_LABEL.LABEL_APPROVAL_STATUS[data.approvalStatus].replace('Pengelola Program', 'Kepala Faskes')
                }
              </Tag>
            </Info>

            <DataTable
              columns={columns}
              data={data.distribution_plans_items}
              rowKey="id"
              pagination={false}
              isLoading={isLoading}
            />
            {data.isAllowedToConfirm && (
              <div className="d-flex justify-content-center mt-3">
                <Button className="hs-btn PHARMA_STAFF" onClick={_toggleModal}>
                  Konfirmasi
                </Button>
              </div>
            )}
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

const Label = ({ text, ...props }) => (
  <Typography
    fontSize="12px"
    fontWeight="bold"
    className="d-block mb-2"
    {...props}
  >
    {text}
  </Typography>
);

const Info = ({ children, ...props }) => (
  <Typography
    fontSize="12px"
    fontWeight="600"
    className="d-block mb-3"
    {...props}
  >
    {children}
  </Typography>
);

const columns = [
  {
    title: "Nama Barang",
    dataIndex: "medicine",
    key: "medicine",
    render: medicine => medicine.name
  },
  {
    title: "Code Name Barang",
    dataIndex: "medicine",
    key: "codeName",
    render: medicine => medicine.codeName
  },
  {
    title: "Nama Brand",
    dataIndex: "packageQuantity",
    key: "packageQuantity"
  },
  {
    title: "Batch Code",
    dataIndex: "inventory",
    key: "inventory",
    render: item => item.batchCode
  },
  {
    title: "Minimal Expired Date",
    dataIndex: "expiredDate",
    key: "expiredDate",
    render: date => <DisplayDate date={date} />
  },
  {
    title: "Stok Dikirim",
    key: "packageQuantity",
    render: item => `${item.packageQuantity} ${item.packageUnitType}`
  }
];
