import React, { useState } from "react";
// import GlobalContext from "containers/GlobalContext";

import { DataTable, DisplayDate } from "components";
import { Link } from "react-router-dom";
import { Button, Tag, Typography, Select } from "antd";
import { TAG_LABEL, LIST_PLAN_STATUS } from "constant";
import FormPenerimaan from "containers/logistik/penerimaan/form";
const { Option } = Select;

export const List = ({ data, isLoading, reload, onChangeStatus, status }) => {
  // const user = useContext(GlobalContext);
  const [isFormOpen, setFormOpen] = useState(false);

  const _toggleModal = () => {
    setFormOpen(!isFormOpen);
  };

  const columns = [
    {
      title: "Nomor Surat Jalan",
      dataIndex: "droppingNumber",
      key: "droppingNumber"
    },
    {
      title: "Status Pengiriman",
      dataIndex: "planStatus",
      key: "planStatus",
      render: planStatus => (
        <Tag color={TAG_LABEL.TAG_PLAN_STATUS[planStatus]}>
          {TAG_LABEL.LABEL_PLAN_STATUS[planStatus]}
        </Tag>
      )
    },
    {
      title: "Pengirim",
      dataIndex: "senderName",
      key: "senderName"
    },
    {
      title: "Jenis Pembelian",
      dataIndex: "distType",
      key: "distType",
      render: distType => (distType ? TAG_LABEL.LABEL_DIST_TYPE[distType] : "-")
    },
    {
      title: "Tanggal dibuat",
      dataIndex: "createdAt",
      key: "createdAt",
      render: date => <DisplayDate date={date} />
    },
    {
      title: "Terakhir Diubah",
      dataIndex: "updatedAt",
      key: "updatedAt",
      render: date => <DisplayDate date={date} />
    },
    {
      title: "Aksi",
      key: "action",
      render: item => {
        return (
          <Link
            to={`/pharma/penerimaan/${item.id}`}
            className="ant-btn hs-btn PHARMA_STAFF ant-btn-primary ant-btn-sm"
          >
            Detail
          </Link>
        );
      }
    }
  ];

  return (
    <React.Fragment>
      {isFormOpen && (
        <FormPenerimaan toggleModal={_toggleModal} reload={reload} />
      )}
      <div className="container h-100 pt-4">
        <div className="row mb-3">
          <div className="col-4">
            <Typography
              fontSize="18px"
              fontWeight="bold"
              color="PHARMA_STAFF"
              className="d-block mb-3"
            >
              Status Pengiriman
            </Typography>
            <Select
              style={{ width: "220px" }}
              onChange={onChangeStatus}
              value={status}
            >
              {LIST_PLAN_STATUS.map(item => (
                <Option key={item} value={item}>
                  <Tag color={TAG_LABEL.TAG_PLAN_STATUS[item]}>
                    {TAG_LABEL.LABEL_PLAN_STATUS[item]}
                  </Tag>
                </Option>
              ))}
            </Select>
          </div>
        </div>
        <div className="row">
          <div className="col-12 bg-white rounded p-4 border">
            <DataTable
              title="Daftar Penerimaan"
              columns={columns}
              data={data}
              isLoading={isLoading}
              button={
                  <Button
                    className="hs-btn-outline PHARMA_STAFF"
                    onClick={_toggleModal}
                  >
                    Tambah Pembelian
                  </Button>
              }
            />
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};
