import React, { useEffect, useState } from "react";
import API from "utils/API";
import axios from "axios";
import { useParams } from "react-router";
import { openNotification } from "utils/Notification";

import { ModalKonfirmasiPermintaan } from "components";
import { ROLE } from "constant";

const imsEmptyState = {
  id: "",
  ditestIms: false,
  namaReagen: "",
  qtyReagen: 1,
  hasilTestIms: null,
  ditestSifilis: false,
  hasilTestSifilis: false,
  namaReagenData: {
    name: "",
    codeName: ""
  }
};

const Handler = ({ toggleModal, visitID }) => {
  let source = axios.CancelToken.source();
  const { role } = useParams();

  const [data, setData] = useState(imsEmptyState);
  const [isLoading, setLoading] = useState(false);

  const _handleChange = (name, value) => {
    setData({
      ...data,
      [name]: value
    });
  };

  const _onSubmit = () => {
    // setLoading(true);
    // API.Exam.createOrUpdateIMS(source.token, visitID, data)
    //   .then(() => {
    //     setLoading(false);
    //     openNotification("success", "Berhasil", "Exam IMS berhasil diinput");
    //     toggleModal();
    //   })
    //   .catch(e => {
    //     console.error("e: ", e);
    //     setLoading(false);
    //   });
  };

  useEffect(() => {
    return () => {
      source.cancel("request canceled");
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <ModalKonfirmasiPermintaan
      onCancel={toggleModal}
      onOk={_onSubmit}
      isLoading={isLoading}
      onChange={_handleChange}
      role={ROLE.PHARMA_STAFF}
      data={data}
    />
  );
};

export default Handler;
