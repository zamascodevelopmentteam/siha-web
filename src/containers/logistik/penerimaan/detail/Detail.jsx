import React, { useState, useContext } from "react";

import {
  DataTable,
  Typography,
  DisplayDate,
  ModalKonfirmasiPermintaan
} from "components";
import { useParams } from "react-router";
import { ROLE, ENUM, TAG_LABEL } from "constant";
import { Button, Tag } from "antd";
import GlobalContext from "containers/GlobalContext";

export const Detail = ({ data, isLoading }) => {
  const user = useContext(GlobalContext);

  const [isConfirmOpen, setConfirmOpen] = useState(false);

  const _toggleModal = () => {
    setConfirmOpen(!isConfirmOpen);
  };

  const _onFilter = value => {
    console.log("value: ", value);
  };

  const { type } = useParams();

  return (
    <React.Fragment>
      {isConfirmOpen && (
        <ModalKonfirmasiPermintaan
          onCancel={_toggleModal}
          onOk={_toggleModal}
          isLoading={false}
          role={ROLE.PHARMA_STAFF}
        />
      )}
      <div className="container h-100 pt-4">
        <div className="row">
          <div className="col-12 p-4">
            <Typography
              fontSize="20px"
              fontWeight="bold"
              className="d-block mb-3"
            >
              Detail Penerimaan
            </Typography>
            {type === "out" &&
              data.lastOrderStatus === ENUM.LAST_ORDER_STATUS.DRAFT && (
                <Button>Ubah Penerimaan</Button>
              )}
          </div>
        </div>
        <div className="row">
          <div className="col-12 bg-white rounded p-4 border">
            <Label text="Nomor Invoice Permintaan" />
            <Info>{data.orderCode}</Info>

            <Label text="Status Penerimaan" />
            <Info>
              <Tag color={TAG_LABEL.TAG_PLAN_STATUS[data.lastOrderStatus]}>
                {TAG_LABEL.LABEL_PLAN_STATUS[data.lastOrderStatus]}
              </Tag>
            </Info>

            <Label text="Status Approval" />
            <Info>
              <Tag
                color={TAG_LABEL.TAG_APPROVAL_STATUS[data.lastApprovalStatus]}
              >
                {
                  user.role !== ROLE.PHARMA_STAFF ?
                    TAG_LABEL.LABEL_APPROVAL_STATUS[data.lastApprovalStatus] : TAG_LABEL.LABEL_APPROVAL_STATUS[data.lastApprovalStatus].replace('Pengelola Program', 'Kepala Faskes')
                }
              </Tag>
            </Info>

            <Label text="Catatan" />
            <Info>{data.orderNotes}</Info>

            <Label text="Catatan Approval" />
            <Info>{data.approvalNotes}</Info>

            <Label text="Tanggal Terakhir Diubah" />
            <Info>
              <DisplayDate date={data.createdAt} />
            </Info>

            <Label text="Dibuat Oleh" />
            <Info>{data.orderCode}</Info>

            <Label text="Terakhir Diubah Oleh" />
            <Info>{data.orderCode}</Info>

            <DataTable
              columns={columns}
              data={data.orderItems}
              rowKey="id"
              pagination={false}
              isLoading={isLoading}
            />
            {data.isAllowedToConfirm && (
              <div className="d-flex justify-content-center mt-3">
                <Button className="hs-btn PHARMA_STAFF" onClick={_toggleModal}>
                  Konfirmasi
                </Button>
              </div>
            )}
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

const Label = ({ text, ...props }) => (
  <Typography
    fontSize="12px"
    fontWeight="bold"
    className="d-block mb-2"
    {...props}
  >
    {text}
  </Typography>
);

const Info = ({ children, ...props }) => (
  <Typography
    fontSize="12px"
    fontWeight="600"
    className="d-block mb-3"
    {...props}
  >
    {children}
  </Typography>
);

const columns = [
  {
    title: "Nama Jenis Barang",
    key: "packageUnitType",
    render: item => `${item.medicine.name} ${item.packageUnitType} `
  },
  {
    title: "Jumlah Barang",
    dataIndex: "packageQuantity",
    key: "packageQuantity"
  },
  {
    title: "Expired Date",
    dataIndex: "expiredDate",
    key: "expiredDate",
    render: date => <DisplayDate date={date} />
  }
];
