import React, { useEffect, useState, useContext } from "react";
import { cloneDeep } from "lodash";
import API from "utils/API";
import axios from "axios";
import shortid from "shortid";
import { openNotification } from "utils/Notification";

import { ModalPenerimaan } from "components";
import { TAG_LABEL } from "constant";
import GlobalContext from "containers/GlobalContext";

const itemEmptyState = {
  brandId: null,
  medicineId: null,
  packageQuantity: null,
  expiredDate: null,
  batchCode: null,
  fundSource: null,
  packagePrice: null,
  manufacture: null,
  unit: null
};

const distributionPlanEmptyState = {
  distType: null,
  entityName: null,
  droppingNumber: null,
  provinceId: null,
  sudinKabKotaId: null,
  upkId: null
};

const Handler = ({ toggleModal, reload, isEdit, id }) => {
  let source = axios.CancelToken.source();

  const [data, setData] = useState(distributionPlanEmptyState);
  const [isLoading, setLoading] = useState(false);
  // const [isEdit, setIsEdit] = useState(false);
  const [items, setItems] = useState([
    { id: shortid.generate(), ...itemEmptyState }
  ]);
  const user = useContext(GlobalContext);
  const [medicineType, setMedicineType] = useState("ALL");
  const [producers, setProducers] = useState([]);

  const _addItem = () => {
    const newItem = {
      id: shortid.generate(),
      ...itemEmptyState
    };
    setItems([...items, newItem]);
  };

  const _removeItem = idx => {
    items.splice(idx, 1);
    setItems([...items]);
  };

  const _handleChangeItem = async (idx, name, value) => {
    let newItems = [...items];
    newItems[idx][name] = value;
    if (name === "medicineId") {
      newItems[idx].brandId = null;
      await API.MasterData.Medicine.getByID(source.token, value)
        .then(rsp => {
          newItems[idx].unit = rsp.data.packageUnitType + (rsp.data.packageMultiplier ? ` (${rsp.data.packageMultiplier})` : ``);
          newItems[idx].medicineType = rsp.data.medicineType;
        })
        .catch(e => {
          console.error("e: ", e);
        })
        .finally(() => {
          // setLoading(false);
        });
    }

    setItems(newItems);
  };

  const _handleChange = (name, value) => {
    setData({
      ...data,
      [name]: value
    });
    if (name === "distType") {
      // setMedicineType(
      //   // value === TAG_LABEL.DIST_TYPE.PEMBELIAN_APBD ? "NON_ARV" : "ALL"
      //   [TAG_LABEL.DIST_TYPE.PEMBELIAN_APBD, TAG_LABEL.DIST_TYPE.BLUD, TAG_LABEL.DIST_TYPE.DAK].includes(TAG_LABEL.DIST_TYPE[value]) ? "NON_ARV" : "ALL"
      // );
    }
  };

  const _loadData = () => {
    // setLoading(true);
    //   API.Logistic.getOrderByID(source.token, id)
    //     .then(rsp => {
    //       console.log("getOrderByID rsp: ", rsp);
    //       setData(rsp.data || orderDetailEmptyState);
    //       if (rsp.data) {
    //         setItems(rsp.data.orderItems);
    //       }
    //     })
    //     .catch(e => {
    //       console.error("e: ", e);
    //     })
    //     .finally(() => {
    //       setLoading(false);
    //     });
  };

  const _loadProducers = () => {
    API.MasterData.Product.list(source.token)
      .then(rsp => {
        console.log(rsp);
        setProducers(rsp.data || []);
      })
      .catch(e => {
        console.error("e: ", e);
      });
  }

  const _create = payload => {
    setLoading(true);
    API.Logistic.createDistributionPlan(source.token, payload)
      .then(() => {
        setLoading(false);
        openNotification("success", "Berhasil", "Penerimaan berhasil diinput");
        reload();
        toggleModal();
      })
      .catch(e => {
        openNotification("error", "Gagal", e.message || String(e.data ? e.data.message : "").replace(/_/g, " "));
        console.error("e: ", e);
        setLoading(false);
      });
  };

  const _onSubmit = () => {
    const payload = _constructPayload(data, items);
    return _create(payload);
    // if (isEdit) {
    //   return _update(payload);
    // }
  };

  useEffect(() => {
    if (isEdit) {
      _loadData();
    }

    _loadProducers();

    return () => {
      source.cancel("request canceled");
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <ModalPenerimaan
      medicineType={medicineType}
      onCancel={toggleModal}
      isEdit={isEdit}
      onOk={_onSubmit}
      isLoading={isLoading}
      onChange={_handleChange}
      role={user.role}
      data={data}
      items={items}
      addItem={_addItem}
      removeItem={_removeItem}
      onChangeItem={_handleChangeItem}
      producers={producers}
    />
  );
};

export default Handler;

const _constructPayload = (data, items) => {
  const newItems = cloneDeep(items);
  const newData = { ...data };
  for (let x = 0; x < newItems.length; x++) {
    delete newItems[x].id;
  }

  delete newData.id;

  return {
    distributionPlans: { ...newData },
    distributionPlansItems: newItems
  };
};
