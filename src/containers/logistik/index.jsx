export { default as Permintaan } from "./permintaan";
export { default as Pengirman } from "./pengiriman";
export { default as Penerimaan } from "./penerimaan";
export { default as DetailDistribusi } from "./detailDistribusi";
