import React, { useEffect, useState } from "react";
import API from "utils/API";
import axios from "axios";
import { cloneDeep } from "lodash";
import { useParams } from "react-router";
// import { openNotification } from "utils/Notification";

import { ModalUbahBarang } from "components";
import { ROLE } from "constant";

const Handler = ({ toggleModal }) => {
  let source = axios.CancelToken.source();
  const { id } = useParams();

  const [data, setData] = useState([]);
  const [isLoading, setLoading] = useState(false);

  const _handleChange = (idDist, idItems, value) => {
    let newData = cloneDeep(data);
    newData[idDist].pickings[idItems].packageQuantity = value;
    setData(newData);
  };

  const _loadData = async () => {
    setLoading(true);
    try {
      const rsp = await API.Logistic.getDistributionPlanByID(source.token, id);
      const distributions = rsp.data.distribution_plans_items;
      for (let x = 0; x < distributions.length; x++) {
        const medicineID = distributions[x].medicine.id;
        const { expiredDate } = distributions[x];
        const rsp = await API.Logistic.getPickingList(
          source.token,
          medicineID,
          expiredDate
        );
        distributions[x].pickings = rsp.data;
      }
      setData([...distributions]);
      setLoading(false);
    } catch (e) {
      console.error("e: ", e);
      setLoading(false);
    }
  };

  const _onSubmit = () => {
    // setLoading(true);
    // API.Exam.createOrUpdateIMS(source.token, visitID, data)
    //   .then(() => {
    //     setLoading(false);
    //     openNotification("success", "Berhasil", "Exam IMS berhasil diinput");
    //     toggleModal();
    //   })
    //   .catch(e => {
    //     console.error("e: ", e);
    //     setLoading(false);
    //   });
  };

  useEffect(() => {
    _loadData();

    return () => {
      source.cancel("request canceled");
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <ModalUbahBarang
      onCancel={toggleModal}
      onOk={_onSubmit}
      isLoading={isLoading}
      onChange={_handleChange}
      role={ROLE.PHARMA_STAFF}
      data={data}
    />
  );
};

export default Handler;

// const _constructPayload = (data) => {
//   let payload = {
//     distribution_plans_items: []
//   }
//   for(let x = 0; x < data.length ; x++) {
//     const item = {
//       id: data[x]
//     }
//   }

//   payload.distribution_plans_items.push()
// }
