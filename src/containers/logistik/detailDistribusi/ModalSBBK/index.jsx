import React, { useRef } from "react";
import { Button, Modal } from "antd";
import { DataTable, DisplayDate, Label, Typography } from "components";
import ReactToPrint from "react-to-print";
import { formatNumber } from "utils";

let data2;

const Form = ({ onOk, onCancel, data, onChange }) => {
  data2 = data;
  const printComponent = useRef();

  return (
    <Modal
      title="SBBK"
      visible
      width={'95%'}
      maskClosable={false}
      onOk={onOk}
      onCancel={onCancel}
      footer={null}
    >
      <div ref={printComponent} style={{overflow: 'hidden'}}>
        <div className="text-center">
          <Typography fontSize="16px" fontWeight="bold" underLine={true}>
            SURAT BUKTI BARANG KELUAR
          </Typography>
          <Label isInfo>Nomor : {data.droppingNumber}</Label>
        </div>

        <Label>Dialamatkan kepada : {data.recieverName}</Label>
        <Label className={'mb-5'}>Alamat : -</Label>
        <Label>Tanggal : <DisplayDate date={data.createdAt} /></Label>


        {/* <Label>Pelaksana Pengeluaran barang</Label> */}
        <DataTable
          columns={columns}
          data={data.distribution_plans_items}
          // isScrollX
          rowKey="distPlanItemId"
          pagination={false}
        />

        <table className={'w-100 mt-5'}>
          <tr>
            <td width={'44%'} className={'text-center'}>
              Ekspedisi / Pengambil :
              <br/>
              <br/>
              <br/>
              <br/>
              (___________________________)
            </td>
            <td></td>
            <td width={'44%'} className={'text-center'}>
              User
              <br/>
              <br/>
              <br/>
              <br/>
              (___________________________)
            </td>
          </tr>
          <tr>
            <td></td>
            <td width={'2%'} className={'text-center'}><b>Mengetahui</b></td>
            <td></td>
          </tr>
          <tr>
            <td className={'text-center'}>
              Kasubag TU dan Rumah Tangga
              <br/>
              <br/>
              <br/>
              <br/>
              (___________________________)
            </td>
            <td></td>
            <td className={'text-center'}>
              Kepala Bagian Kepegawaian dan Umum
              <br/>
              <br/>
              <br/>
              <br/>
              (___________________________)
            </td>
          </tr>
        </table>
        <br />
        <br />
        <br />
        <br />
        Barang-barang tsb telah dihitung satu persatu dan diterima dengan baik dan cukup.
        <table className={'table table-bordered'}>
          <tr>
            <td width={'20%'}>PENERIMA</td>
            <td width={'30%'} className={'text-center'}>Mengetahui</td>
            <td></td>
          </tr>
          <tr>
            <td colSpan={'2'}>Nama&nbsp;&nbsp;:&nbsp;&nbsp;</td>
            <td rowSpan={'4'}>Kepada ....................................................................</td>
          </tr>
          <tr>
            <td colSpan={'2'}>Tanggal&nbsp;&nbsp;:&nbsp;&nbsp;</td>
          </tr>
          <tr>
            <td colSpan={'2'}>Jabatan&nbsp;&nbsp;:&nbsp;&nbsp;</td>
          </tr>
          <tr>
            <td colSpan={'2'}>Tanda Tangan&nbsp;&nbsp;:&nbsp;&nbsp;</td>
          </tr>
          <tr>
            <td className={'text-center'} colSpan={'2'}>
              (___________________________)
              <br/>
              NIP
            </td>
            <td className={'text-center'}>
              (___________________________)
              <br/>
              NIP
            </td>
          </tr>
          <tr>
            <td className={'text-center'} colSpan={'3'}>
              Jika barang diterima rusak/pecah/kurang, agar diberi catatan pada Lembaran Lain, SBBK ini tidak boleh di coret – coret dan apabila tidak ada catatan apa-apa maka barang dianggap diterima dalam keadaan baik / cukup.
            </td>
          </tr>
        </table>
      </div>

      <div className="d-flex justify-content-end mt-2">
        <Button key="back" type="danger" onClick={onCancel} className="mr-2">
          Tutup
        </Button>
        <ReactToPrint
          trigger={() => (
            <Button type="primary" icon="printer">
              Print
            </Button>
          )}
          content={() => printComponent.current}
          bodyClass="p-5"
        />
      </div>
    </Modal>
  );
};
// 
const columns = [
  {
    title: "No",
    dataIndex: "no",
    key: "no",
    render: (row, x, i) => {
      // console.log(x);
      return (i + 1);
    }
  },
  {
    title: "Nama Barang",
    dataIndex: "medicine",
    key: "medicine",
    render: (medicine, r) => (r.inventory.medicine ? r.inventory.medicine.name : "-")
  },
  {
    title: "Batch",
    dataIndex: "batch",
    key: "batch",
    render: (r, x) => (x.inventory ? x.inventory.batchCode : "-")
  },
  {
    title: "Expired date",
    dataIndex: "expiredDate",
    key: "expiredDate",
    render: (expiredDate, r) => <DisplayDate date={r.inventory.expiredDate} />
  },
  {
    title: "Satuan",
    dataIndex: "packageUnitType",
    key: "packageUnitType"
  },
  {
    title: "Jumlah",
    key: "packageQuantity",
    dataIndex: "packageQuantity",
    render: number => formatNumber(number)
  },
  {
    title: "Harga Satuan",
    dataIndex: "unitPrice",
    key: "unitPrice",
    render: (number, i) => `Rp. ${formatNumber(Number(i.inventory.packagePrice))}`
  },
  {
    title: "Jumlah Harga",
    dataIndex: "unitPrice",
    key: "unitPrice",
    render: (unitPrice, r) => `Rp. ${formatNumber(Number(r.inventory.packagePrice * r.packageQuantity))}`
  },
  {
    title: "Ket",
    dataIndex: "notes",
    key: "notes",
    render: notes => (notes ? notes : "-")
  },
  {
    title: "No DO",
    dataIndex: "doo",
    key: "doo",
    render: doo => (data2.droppingNumber)
  },
];

export default Form;