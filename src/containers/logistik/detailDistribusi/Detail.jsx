import React, { useState, useRef, useContext } from "react";

import { DataTable, Typography, DisplayDate } from "components";
import { Button, Tag } from "antd";
import FormEdit from "./FormEdit";
import ModalConfirm from "./ModalConfirm";
import { formatNumber } from "utils";
import QRCode from "qrcode.react";
import ReactToPrint from "react-to-print";
import ModalSSBK from "./ModalSBBK";
import { ROLE, ENUM, TAG_LABEL } from "constant";

// new
import GlobalContext from "containers/GlobalContext";
// end new

export const Detail = ({ data, isLoading, reload, handleDownload }) => {
  const user = useContext(GlobalContext);
  const [isConfirmOpen, setConfirmOpen] = useState(false);
  const [isEditFormOpen, setEditFormOpen] = useState(false);
  const [isSBBKOpen, setSBBKOpen] = useState(false);
  const printComponent = useRef();

  const _toggleModal = {
    confirm: () => setConfirmOpen(!isConfirmOpen),
    edit: () => setEditFormOpen(!isEditFormOpen),
    SBBK: () => setSBBKOpen(!isSBBKOpen)
  };

  return (
    <React.Fragment>
      {isEditFormOpen && (
        <FormEdit toggleModal={_toggleModal.edit} reload={reload} />
      )}

      {isConfirmOpen && (
        <ModalConfirm
          planStatus={data.planStatus}
          toggleModal={_toggleModal.confirm}
          reload={reload}
        />
      )}

      {isSBBKOpen && (
        <ModalSSBK
          data={data}
          onOk={_toggleModal.SBBK}
          onCancel={_toggleModal.SBBK}
        />
      )}

      <div className="container h-100 pt-4">
        <div className="row">
          <div className="col-12 pl-0 pb-4">
            <Typography fontSize="20px" fontWeight="bold" className="d-block">
              Detail Distribusi
            </Typography>
          </div>
          {/* <div className="col-12">
            <Button onClick={_toggleModal.edit}>Ubah Rencana Distribusi</Button>
          </div> */}
        </div>
        <div className="row mb-3">
          <div className="col-12 pl-0">
            <ReactToPrint
              trigger={() => (
                <Button type="primary" size="large" icon="printer">
                  Print
                </Button>
              )}
              content={() => printComponent.current}
              bodyClass="p-5"
            />
            {(data.planStatus === ENUM.PLAN_STATUS.PROCESSED || data.planStatus === ENUM.PLAN_STATUS.ACCEPTED_FULLY) && (
              <Button className="ml-2" size="large" onClick={_toggleModal.SBBK}>
                Lihat SBBK
              </Button>
            )}
            {/* <Button className="ml-2" size="large" onClick={handleDownload}>
              Download SBBK
            </Button> */}
          </div>
        </div>

        <div className="row" ref={printComponent}>
          <div className="col-12 bg-white rounded p-4 border">
            <div className="row">
              <div className="col-10">
                <Label text="Nomor Surat Jalan" />
                <Info>{data.droppingNumber}</Info>

                {data.distType && (
                  <React.Fragment>
                    <Label text="Jenis Pembelian" />
                    <Info>{TAG_LABEL.LABEL_DIST_TYPE[data.distType]}</Info>
                  </React.Fragment>
                )}

                <Label text="Pengirim" />
                <Info>{data.senderName}</Info>

                <Label text="Terakhir Diubah" />
                <Info>
                  <DisplayDate date={data.updatedAt} />
                </Info>

                <Typography
                  fontSize="14px"
                  fontWeight="bold"
                  className="d-block mb-3"
                >
                  Status
                </Typography>

                <Label text="Status Penerimaan" />
                <Info>
                  <Tag color={TAG_LABEL.TAG_PLAN_STATUS[data.planStatus]}>
                    {TAG_LABEL.LABEL_PLAN_STATUS[data.planStatus]}
                  </Tag>
                </Info>

                <Label text="Status Approval" />
                <Info>
                  <Tag
                    color={TAG_LABEL.TAG_APPROVAL_STATUS[data.approvalStatus]}
                  >
                    {
                      user.role !== ROLE.PHARMA_STAFF ?
                        TAG_LABEL.LABEL_APPROVAL_STATUS[data.approvalStatus] : (
                          TAG_LABEL.LABEL_APPROVAL_STATUS[data.approvalStatus] !== undefined ?
                            TAG_LABEL.LABEL_APPROVAL_STATUS[data.approvalStatus].replace('Pengelola Program', 'Kepala Faskes') :
                            null
                        )
                    }
                  </Tag>
                </Info>
              </div>
              <div className="col-2">
                <QRCode
                  value={data.droppingNumber}
                  renderAs={"canvas"}
                  className="d-print-block"
                />
              </div>
            </div>
            <div className="row">
              <div className="col-12">
                <DataTable
                  isScrollX
                  columns={columns}
                  data={data.distribution_plans_items}
                  rowKey="distPlanItemId"
                  pagination={false}
                  isLoading={isLoading}
                />

                {/* Pengirim */}
                {data.isAllowedToConfirm && [TAG_LABEL.LABEL_PLAN_STATUS.DRAFT, TAG_LABEL.LABEL_PLAN_STATUS.EVALUATION].includes(TAG_LABEL.LABEL_PLAN_STATUS[data.planStatus]) && (
                  <div className="d-flex justify-content-center mt-3 d-print-none">
                    <Button
                      className="hs-btn PHARMA_STAFF"
                      onClick={_toggleModal.confirm}
                    >
                      Konfirmasi
                    </Button>
                  </div>
                )}

                {/* Penerima */}
                {data.isAllowedToConfirm && [ENUM.USER_ROLE.PHARMA_STAFF, ENUM.USER_ROLE.SUDIN_STAFF, ENUM.USER_ROLE.PROVINCE_STAFF, ENUM.USER_ROLE.LAB_STAFF].includes(user.role) && [TAG_LABEL.LABEL_PLAN_STATUS.PROCESSED].includes(TAG_LABEL.LABEL_PLAN_STATUS[data.planStatus]) && (
                  <div className="d-flex justify-content-center mt-3 d-print-none">
                    <Button
                      className="hs-btn PHARMA_STAFF"
                      onClick={_toggleModal.confirm}
                    >
                      Konfirmasi
                    </Button>
                  </div>
                )}

                {/* {TAG_LABEL.LABEL_PLAN_STATUS[data.planStatus] === TAG_LABEL.LABEL_PLAN_STATUS.PROCESSED && (
                  <div className="d-flex justify-content-center mt-3 d-print-none">
                    <Button
                      className="hs-btn PHARMA_STAFF"
                      onClick={_toggleModal.confirm}
                    >
                      Konfirmasi
                    </Button>
                  </div>
                )} */}
              </div>
            </div>
            {data.receipts && data.receipts[0] && (
              <div className="row pt-5">
                <div className="col-12">
                  <Typography
                    fontSize="14px"
                    fontWeight="bold"
                    className="d-block"
                  >
                    Barang yang telah diterima
                  </Typography>
                  <DataTable
                    isScrollX
                    columns={columnsReceive}
                    data={data.receipts[0].receiptItems}
                    rowKey="receiptId"
                    pagination={false}
                    isLoading={isLoading}
                  />
                </div>
              </div>
            )}
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

const Label = ({ text, ...props }) => (
  <Typography
    fontSize="12px"
    fontWeight="bold"
    className="d-block mb-2"
    {...props}
  >
    {text}
  </Typography>
);

const Info = ({ children, ...props }) => (
  <Typography
    fontSize="12px"
    fontWeight="600"
    className="d-block mb-3"
    {...props}
  >
    {children}
  </Typography>
);

const columns = [
  {
    title: "Nama Barang",
    dataIndex: "medicine",
    key: "name",
    ellipsis: true,
    render: (medicine, r) => {
      // console.log(r);
      var date1 = new Date();
      var date2 = new Date(r.inventory ? r.inventory.expiredDate : (r.inventoryForData ? r.inventoryForData.expiredDate : ''));
      var difference_In_Time = date2.getTime() - date1.getTime();
      var difference_In_Days = difference_In_Time / (1000 * 3600 * 24);
      let textClass = "";
      if (difference_In_Days <= 90) {
        textClass = "text-warning";
      }
      if (difference_In_Days <= 30) {
        textClass = "text-danger";
      }
      if (r.inventory) {
        return <span className={textClass}>{(r.inventory.medicine ? (r.orderItem ? (r.orderItem.type ? r.orderItem.type.toUpperCase() + ' ' : '') : '') + r.inventory.medicine.name : "-")}</span>;
      } else if (r.inventoryForData) {
        return <span className={textClass}>{(r.inventoryForData.medicine ? (r.orderItem ? (r.orderItem.type ? r.orderItem.type.toUpperCase() + ' ' : '') : '') + r.inventoryForData.medicine.name : "-")}</span>;
      } else {
        return <span className={textClass}>{'-'}</span>;
      }
    }
  },
  {
    title: "Code Name Barang",
    dataIndex: "medicine",
    key: "codeName",
    render: (medicine, r) => {
      var date1 = new Date();
      var date2 = new Date(r.inventory ? r.inventory.expiredDate : (r.inventoryForData ? r.inventoryForData.expiredDate : ''));
      var difference_In_Time = date2.getTime() - date1.getTime();
      var difference_In_Days = difference_In_Time / (1000 * 3600 * 24);
      let textClass = "";
      if (difference_In_Days <= 90) {
        textClass = "text-warning";
      }
      if (difference_In_Days <= 30) {
        textClass = "text-danger";
      }
      if (r.inventory) {
        return <span className={textClass}>{(r.inventory.medicine ? r.inventory.medicine.codeName : "-")}</span>;
      } else if (r.inventoryForData) {
        return <span className={textClass}>{(r.inventoryForData.medicine ? r.inventoryForData.medicine.codeName : "-")}</span>;
      } else {
        return <span className={textClass}>{'-'}</span>;
      }
    }
  },
  {
    title: "Nama Brand",
    key: "brandName",
    render: (data, r) => {
      var date1 = new Date();
      var date2 = new Date(r.inventory ? r.inventory.expiredDate : (r.inventoryForData ? r.inventoryForData.expiredDate : ''));
      var difference_In_Time = date2.getTime() - date1.getTime();
      var difference_In_Days = difference_In_Time / (1000 * 3600 * 24);
      let textClass = "";
      if (difference_In_Days <= 90) {
        textClass = "text-warning";
      }
      if (difference_In_Days <= 30) {
        textClass = "text-danger";
      }
      return <span className={textClass}>{(
        data.inventory
          ? data.inventory.brand.name
          : data.inventoryForData
            ? data.inventoryForData.brand.name
            : "-"
      )}</span>;
    }
  },
  {
    title: "Batch Code",
    key: "inventory",
    render: (data, r) => {
      var date1 = new Date();
      var date2 = new Date(r.inventory ? r.inventory.expiredDate : (r.inventoryForData ? r.inventoryForData.expiredDate : ''));
      var difference_In_Time = date2.getTime() - date1.getTime();
      var difference_In_Days = difference_In_Time / (1000 * 3600 * 24);
      let textClass = "";
      if (difference_In_Days <= 90) {
        textClass = "text-warning";
      }
      if (difference_In_Days <= 30) {
        textClass = "text-danger";
      }
      return <span className={textClass}>{(
        data.inventory
          ? data.inventory.batchCode
          : data.inventoryForData
            ? data.inventoryForData.batchCode
            : "-"
      )}</span>;
    }
  },
  {
    title: "Sumber Dana",
    key: "fundSource",
    render: (data, r) => {
      var date1 = new Date();
      var date2 = new Date(r.inventory ? r.inventory.expiredDate : (r.inventoryForData ? r.inventoryForData.expiredDate : ''));
      var difference_In_Time = date2.getTime() - date1.getTime();
      var difference_In_Days = difference_In_Time / (1000 * 3600 * 24);
      let textClass = "";
      if (difference_In_Days <= 90) {
        textClass = "text-warning";
      }
      if (difference_In_Days <= 30) {
        textClass = "text-danger";
      }
      return <span className={textClass}>{(
        data.inventory
          ? data.inventory.fundSource
          : data.inventoryForData
            ? data.inventoryForData.fundSource
            : "-"
      )}</span>;
    }
  },
  {
    title: "Harga per Satuan",
    key: "packagePrice",
    render: (data, r) => {
      var date1 = new Date();
      var date2 = new Date(r.inventory ? r.inventory.expiredDate : (r.inventoryForData ? r.inventoryForData.expiredDate : ''));
      var difference_In_Time = date2.getTime() - date1.getTime();
      var difference_In_Days = difference_In_Time / (1000 * 3600 * 24);
      let textClass = "";
      if (difference_In_Days <= 90) {
        textClass = "text-warning";
      }
      if (difference_In_Days <= 30) {
        textClass = "text-danger";
      }
      return <span className={textClass}>{(
        data.inventory
          ? formatNumber(Number(data.inventory.packagePrice))
          : data.inventoryForData
            ? formatNumber(Number(data.inventoryForData.packagePrice))
            : "-"
      )}</span>;
    }
  },
  {
    title: "Manufacture",
    key: "manufacture",
    render: (data, r) => {
      var date1 = new Date();
      var date2 = new Date(r.inventory ? r.inventory.expiredDate : (r.inventoryForData ? r.inventoryForData.expiredDate : ''));
      var difference_In_Time = date2.getTime() - date1.getTime();
      var difference_In_Days = difference_In_Time / (1000 * 3600 * 24);
      let textClass = "";
      if (difference_In_Days <= 90) {
        textClass = "text-warning";
      }
      if (difference_In_Days <= 30) {
        textClass = "text-danger";
      }
      return <span className={textClass}>{(
        data.inventory
          ? data.inventory.manufacture
          : data.inventoryForData
            ? data.inventoryForData.manufacture
            : "-"
      )}</span>;
    }
  },
  {
    title: "Expired Date",
    dataIndex: "expiredDate",
    key: "expiredDate",
    render: (date, r) => {
      var date1 = new Date();
      var date2 = new Date(r.inventory ? r.inventory.expiredDate : (r.inventoryForData ? r.inventoryForData.expiredDate : ''));
      var difference_In_Time = date2.getTime() - date1.getTime();
      var difference_In_Days = difference_In_Time / (1000 * 3600 * 24);
      let textClass = "";
      if (difference_In_Days <= 90) {
        textClass = "text-warning";
      }
      if (difference_In_Days <= 30) {
        textClass = "text-danger";
      }
      
      if (r.inventory) {
        return <span className={textClass}><DisplayDate date={r.inventory.expiredDate} /></span>;
      }
      return <span className={textClass}><DisplayDate date={r.inventoryForData.expiredDate} /></span>;
    }
  },
  {
    title: "Stok Dikirim",
    key: "packageQuantity",
    render: (item, r) => {
      var date1 = new Date();
      var date2 = new Date(r.inventory ? r.inventory.expiredDate : (r.inventoryForData ? r.inventoryForData.expiredDate : ''));
      var difference_In_Time = date2.getTime() - date1.getTime();
      var difference_In_Days = difference_In_Time / (1000 * 3600 * 24);
      let textClass = "";
      if (difference_In_Days <= 90) {
        textClass = "text-warning";
      }
      if (difference_In_Days <= 30) {
        textClass = "text-danger";
      }
      return <span className={textClass}>{`${formatNumber(item.packageQuantity)} ${item.packageUnitType || ""}`}</span>;
    }
  }
];

const columnsReceive = [
  {
    title: "Nama Barang",
    // dataIndex: "inventories",
    key: "name",
    render: r => {
      console.log(r);
      return r.inventories &&
      r.inventories[0] &&
      r.inventories[0].brand &&
      r.inventories[0].brand.medicine &&
      r.inventories[0].brand.medicine.name
      ? r.inventories[0].brand.medicine.name
      // ? (r.orderItem.type ? r.orderItem.type.toUpperCase() + ' ' : '') + inventories[0].brand.medicine.name
      : "-"
    }
  },
  {
    title: "Code Name Barang",
    dataIndex: "inventories",
    key: "codeName",
    render: inventories =>
      inventories &&
        inventories[0] &&
        inventories[0].brand &&
        inventories[0].brand.medicine &&
        inventories[0].brand.medicine.codeName
        ? inventories[0].brand.medicine.codeName
        : "-"
  },
  {
    title: "Nama Brand",
    dataIndex: "inventories",
    key: "name",
    render: inventories =>
      inventories &&
        inventories[0] &&
        inventories[0].brand &&
        inventories[0].brand.name
        ? inventories[0].brand.name
        : "-"
  },
  {
    title: "Batch Code",
    dataIndex: "inventories",
    key: "batchCode",
    render: inventories =>
      inventories && inventories[0] && inventories[0].batchCode
        ? inventories[0].batchCode
        : "-"
  },
  {
    title: "Sumber Dana",
    key: "fundSource",
    dataIndex: "inventories",
    render: inventories =>
      inventories && inventories[0] && inventories[0].fundSource
        ? inventories[0].fundSource
        : "-"
  },
  {
    title: "Harga per Satuan",
    key: "packagePrice",
    dataIndex: "inventories",
    render: inventories =>
      inventories && inventories[0] && inventories[0].packagePrice
        ? formatNumber(Number(inventories[0].packagePrice))
        : "-"
  },
  {
    title: "Manufacture",
    key: "manufacture",
    dataIndex: "inventories",
    render: inventories =>
      inventories && inventories[0] && inventories[0].manufacture
        ? inventories[0].manufacture
        : "-"
  },
  {
    title: "Expired Date",
    dataIndex: "receivedExpiredDate",
    key: "receivedExpiredDate",
    render: receivedExpiredDate => <DisplayDate date={receivedExpiredDate} />
  },
  {
    title: "Stok Diterima",
    key: "receivedPkgQuantity",
    render: item =>
      `${formatNumber(item.receivedPkgQuantity)} ${item.receivedPkgUnitType ||
      ""}`
  }
];
