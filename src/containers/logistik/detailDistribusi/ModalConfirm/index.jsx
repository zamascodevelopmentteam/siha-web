import React, { useEffect, useState } from "react";
import API from "utils/API";
import axios from "axios";
import { useParams } from "react-router";
import { openNotification } from "utils/Notification";

import {
  ModalKonfirmasiPermintaan,
  ModalKonfirmasiPenerimaanBarang
} from "components";
import { ROLE, ENUM } from "constant";
const { PLAN_STATUS } = ENUM;

const orderDetailEmptyState = {
  notes: null,
  status: null
};

const STATUS_OPTION = {
  DRAFT: ["IN_EVALUATION"],
  IN_EVALUATION: ["APPROVED_FULL", "REJECTED"],
  APPROVED_FULL: ["ACCEPTED_DIFFERENT", "ACCEPTED_FULLY"]
};

const Handler = ({ toggleModal, reload, planStatus }) => {
  let source = axios.CancelToken.source();
  const { id } = useParams();

  const [data, setData] = useState(orderDetailEmptyState);
  const [isLoading, setLoading] = useState(false);
  const [isKonfirmBarangOpen, setKonfirmBarangOpen] = useState(false);
  // const [error, setError] = useState("");
  const [error] = useState("");
  const [options, setOptions] = useState([]);

  const _toggleModal = () => {
    setKonfirmBarangOpen(!isKonfirmBarangOpen);
  };

  const _handleChange = (name, value) => {
    setData({
      ...data,
      [name]: value
    });
  };

  const _handleChangeBarang = (idx, value) => {
    data.distribution_plans_items[idx]["actualRecievedPackageQuantity"] = value;
    setData(data);
  };

  const _onSubmit = () => {
    const { notes, status } = data;
    let payload = { note: notes };
    if (status === "ACCEPTED_DIFFERENT") {
      _toggleModal();
      return;
    }
    _submitData(status, payload);
  };

  const _submitData = (status, payload, closeKonfirmBarang = false) => {
    if (planStatus === PLAN_STATUS.PROCESSED) {
      return _submitPenerimaan(status, payload, closeKonfirmBarang);
    }
    return _submitPengiriman(status, payload, closeKonfirmBarang);
  };

  // penerimaan accpet, pengiriman
  const _submitPenerimaan = (status, payload, closeKonfirmBarang = false) => {
    setLoading(true);
    API.Logistic.confirmPenerimaan(source.token, id, status, payload)
      .then(() => {
        openNotification("success", "Berhasil", "Konfirmasi berhasil diinput");
        toggleModal();
        reload();
        if (closeKonfirmBarang) {
          _toggleModal();
        }
      })
      .catch(e => {
        openNotification("error", "Gagal", e.message || String(e.data ? e.data.message : "").replace(/_/g, " "));
        console.error("e: ", e);
      })
      .finally(() => {
        setLoading(false);
      });
  };
  const _submitPengiriman = (status, payload, closeKonfirmBarang = false) => {
    setLoading(true);
    API.Logistic.confirmPengiriman(source.token, id, status, payload)
      .then(() => {
        openNotification("success", "Berhasil", "Konfirmasi berhasil diinput");
        reload();
        toggleModal();
        if (closeKonfirmBarang) {
          _toggleModal();
        }
      })
      .catch(e => {
        openNotification("error", "Gagal", e.message || String(e.data ? e.data.message : "").replace(/_/g, " "));
        console.error("e: ", e);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const _onSubmitKonfirmBarang = () => {
    const { notes, status } = data;
    const distributionPlansItems = _constructPayload(data);
    const payload = {
      notes,
      distributionPlansItems
    };
    _submitData(status, payload, true);
  };

  const _loadData = () => {
    setLoading(true);
    API.Logistic.getDistributionPlanByID(source.token, id)
      .then(rsp => {
        setData(rsp.data || orderDetailEmptyState);
        setOptions(STATUS_OPTION[rsp.data.approvalStatus] || []);
      })
      .catch(e => {
        console.error("e: ", e);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  useEffect(() => {
    _loadData();

    return () => {
      source.cancel("request canceled");
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <React.Fragment>
      {isKonfirmBarangOpen && (
        <ModalKonfirmasiPenerimaanBarang
          onChange={_handleChangeBarang}
          data={data}
          onOk={_onSubmitKonfirmBarang}
          onCancel={_toggleModal}
        />
      )}
      <ModalKonfirmasiPermintaan
        onCancel={toggleModal}
        onOk={_onSubmit}
        isLoading={isLoading}
        onChange={_handleChange}
        role={ROLE.PHARMA_STAFF}
        data={data}
        options={options}
        error={error}
      />
    </React.Fragment>
  );
};

export default Handler;

const _constructPayload = ({ distribution_plans_items }) => {
  let distributionPlansItems = [];

  for (let x = 0; x < distribution_plans_items.length; x++) {
    const {
      distPlanItemId,
      actualRecievedPackageQuantity
    } = distribution_plans_items[x];
    distributionPlansItems.push({
      id: distPlanItemId,
      actualRecievedPackageQuantity
    });
  }

  return distributionPlansItems;
};
