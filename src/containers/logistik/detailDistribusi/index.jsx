import React, { useEffect, useState } from "react";
import API from "utils/API";
import axios from "axios";

import { useParams } from "react-router";
import { Detail } from "./Detail";

const orderDetailEmptyState = {
  id: "",
  droppingNumber: "",
  picId: "",
  fundSource: "",
  planStatus: "",
  logisticRole: "",
  createdBy: "",
  updatedBy: "",
  provinceId: "",
  sudinKabKotaId: "",
  upkId: "",
  createdAt: "",
  updatedAt: "",
  deletedAt: "",
  orderId: "",
  senderName: "",
  recieverName: "",
  distribution_plans_items: []
};

const Handler = () => {
  let source = axios.CancelToken.source();

  const [data, setData] = useState(orderDetailEmptyState);
  const [isLoading, setLoading] = useState(false);
  const { type, id } = useParams();

  const _loadData = () => {
    setLoading(true);

    API.Logistic.getDistributionPlanByID(source.token, id)
      .then(rsp => {
        setData(rsp.data || orderDetailEmptyState);
      })
      .catch(e => {
        console.error("e: ", e);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const _handleDownload = () => {
    console.log("_handleDownload: ");

    API.ReportExcel.downloadExcel(source.token)
      .then(response => {
        console.log("response: ", response);
        // let fileName = response.headers["content-disposition"].split(
        //   "filename="
        // )[1];
        // console.log("fileName: ", fileName);
        console.log("response.headers: ", response.headers);
        console.log(
          'response.headers["content-disposition"]: ',
          response.headers["content-disposition"]
        );
        const url = window.URL.createObjectURL(new Blob([response.data]));
        const link = document.createElement("a");
        link.href = url;
        link.setAttribute("download", "file.xlsx"); //or any other extension
        document.body.appendChild(link);
        link.click();
        // if (window.navigator && window.navigator.msSaveOrOpenBlob) {
        //   // IE variant
        //   window.navigator.msSaveOrOpenBlob(
        //     new Blob([response.data], {
        //       type:
        //         "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
        //     }),
        //     fileName
        //   );
        // } else {
        //   const url = window.URL.createObjectURL(
        //     new Blob([response.data], {
        //       type:
        //         "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
        //     })
        //   );
        //   const link = document.createElement("a");
        //   link.href = url;
        //   link.setAttribute(
        //     "download",
        //     response.headers["content-disposition"].split("filename=")[1]
        //   );
        //   document.body.appendChild(link);
        //   link.click();
        // }
        // const fileName = "Download Example";

        // var fileDownload = require("js-file-download");
        // fileDownload(rsp);
        // window.open(rsp);
      })
      .catch(e => {
        console.error("e: ", e);
      });
  };

  useEffect(() => {
    _loadData();

    return () => {
      source.cancel("request canceled");
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [type]);

  return (
    <Detail
      data={data}
      isLoading={isLoading}
      reload={_loadData}
      handleDownload={_handleDownload}
    />
  );
};

export default Handler;
