import React, { useEffect, useState, useContext } from "react";
import { cloneDeep } from "lodash";
import API from "utils/API";
import axios from "axios";
import shortid from "shortid";
import { openNotification } from "utils/Notification";

import { ModalOrder } from "components";
import { ROLE, ENUM } from "constant";
import moment from "moment";

// new
import GlobalContext from "containers/GlobalContext";
// end new

const itemEmptyState = {
  medicineId: null,
  packageQuantity: null,
  notes: null,
  expiredDate: moment().format("YYYY-MM-DD"),
  unit: null,
  medicineType: null
};

const orderEmptyState = {
  upkId: null,
  sudinKabKotaId: null,
  provinceId: null,
  orderNotes: ""
};

const orderDetailEmptyState = {
  id: "",
  orderCode: "",
  orderNumber: "",
  lastOrderStatus: "",
  lastApprovalStatus: "",
  orderNotes: "",
  approvalNotes: "",
  isRegular: "",
  picId: "",
  logisticRole: "",
  upkIdOwner: "",
  sudinKabKotaIdOwner: "",
  provinceIdOwner: "",
  upkIdFor: "",
  sudinKabKotaIdFor: "",
  provinceIdFor: "",
  createdBy: "",
  updatedBy: "",
  deletedAt: "",
  createdAt: "",
  updatedAt: "",
  orderItems: [],
  distributionPlans: []
};

const DEST_TYPE = {
  UPK_ENTITY: "upkId",
  PROVINCE_ENTITY: "provinceId",
  SUDIN_ENTITY: "sudinKabKotaId"
};

const Handler = ({ toggleModal, reload, isEdit, id, type, mediciceType }) => {
  let source = axios.CancelToken.source();

  const [data, setData] = useState(orderEmptyState);
  const [selectedDest, setSelectedDest] = useState({
    id: 0,
    logisticRole: "",
    type: ""
  });
  const [isLoading, setLoading] = useState(false);
  // const [isEdit, setIsEdit] = useState(false);
  const [items, setItems] = useState([
    { id: shortid.generate(), ...itemEmptyState, medicineType: "ARV" },
    { id: shortid.generate(), ...itemEmptyState, medicineType: "NON_ARV" }
  ]);

  const _addItem = (type) => {
    const newItem = {
      id: shortid.generate(),
      ...itemEmptyState,
      medicineType: type
    };
    setItems([...items, newItem]);
  };

  const _removeItem = idx => {
    items.splice(idx, 1);
    setItems([...items]);
  };

  const _handleChangeItem = async (idx, name, value, type) => {
    let newItems = [...items];
    newItems[idx][name] = value;
    if (type) {
      newItems[idx]['medicineType'] = type;
    }
    if (name === "medicineId") {
      await API.MasterData.Medicine.getByID(source.token, value)
        .then(rsp => {
          newItems[idx].unit = rsp.data.packageUnitType;
        })
        .catch(e => {
          console.error("e: ", e);
        })
        .finally(() => {
          // setLoading(false);
        });
    }

    setItems(newItems);
  };

  const _handleChange = (name, value) => {
    setData({
      ...data,
      [name]: value
    });
  };

  const _handleChangeDestination = ({ itemId, type }) => {
    switch (type) {
      case "upkid":
        setSelectedDest({
          id: itemId,
          type: "upkId",
          logisticRole: ENUM.LOGISTIC_ROLE.UPK_ENTITY
        });
        setData({
          ...data,
          sudinKabKotaId: null,
          provinceId: null,
          upkId: itemId
        });
        break;
      case "sudinkotakabid":
        setSelectedDest({
          id: itemId,
          type: "sudinKabKotaId",
          logisticRole: ENUM.LOGISTIC_ROLE.SUDIN_ENTITY
        });
        setData({
          ...data,
          sudinKabKotaId: itemId,
          provinceId: null,
          upkId: null
        });
        break;
      case "provinceid":
        setSelectedDest({
          id: itemId,
          type: "provinceId",
          logisticRole: ENUM.LOGISTIC_ROLE.PROVINCE_ENTITY
        });
        setData({
          ...data,
          sudinKabKotaId: null,
          provinceId: itemId,
          upkId: null
        });
        break;
      default:
        setSelectedDest({
          id: null,
          type: "",
          logisticRole: ENUM.LOGISTIC_ROLE.MINISTRY_ENTITY
        });
        setData({
          ...data,
          sudinKabKotaId: null,
          provinceId: null,
          upkId: null
        });
        break;
    }
  };

  async function _loadDataReguler() {
    try {
      setLoading(true)
      let res = await API.Logistic.getRegularDemand()
      setData(res.data)
    } catch (ex) {
      console.log('error ', ex)
    }

    setLoading(false)
  }


  // new
  const user = useContext(GlobalContext);

  const defaultFilter = {
    PROVINCE: [
      ENUM.LOGISTIC_ROLE.PROVINCE_ENTITY,
      ENUM.LOGISTIC_ROLE.SUDIN_ENTITY,
      ENUM.LOGISTIC_ROLE.UPK_ENTITY
    ].includes(user.logisticrole)
      ? user.provinceId
      : "",
    SUDINKAB: [
      ENUM.LOGISTIC_ROLE.SUDIN_ENTITY,
      ENUM.LOGISTIC_ROLE.UPK_ENTITY
    ].includes(user.logisticrole)
      ? user.sudinKabKotaId
      : "",
    UPK: user.logisticrole === ENUM.LOGISTIC_ROLE.UPK_ENTITY ? user.upkId : "",
    MONTH: ""
  };

  // const [filter, setFilter] = useState(defaultFilter);
  const [filter] = useState(defaultFilter);
  const [dataReguler, setDataReguler] = useState([]);

  const _loadDataReguler2 = async (returnItem = false) => {
    setLoading(true);

    let regulerData = [];
    let pengkali = 3;
    if (user.role == ROLE.SUDIN_STAFF) {
      pengkali = 6;
    } else if (user.role == ROLE.PROVINCE_STAFF) {
      pengkali = 9;
    }

    await API.Report.lbpha2(
      source.token,
      filter.PROVINCE,
      filter.SUDINKAB,
      filter.UPK,
      filter.MONTH,
      true,
      true
    )
      .then(async rsp => {
        // let rdt = {
        //   r1: {
        //     data: {
        //       medicineName: "",
        //       medicine_id: "",
        //       rataPemakaian: 0,
        //       onHand: 0,
        //       package_quantity: 0,
        //       orderQty: 0,
        //       reason: "",
        //       type: "",
        //       expiredDate: moment().format('YYYY-MM-DD'),
        //       medicine_ids: []
        //     },
        //     meds: []
        //   },
        //   r2: {
        //     data: {
        //       medicineName: "",
        //       medicine_id: "",
        //       rataPemakaian: 0,
        //       onHand: 0,
        //       package_quantity: 0,
        //       orderQty: 0,
        //       reason: "",
        //       type: "",
        //       expiredDate: moment().format('YYYY-MM-DD'),
        //       medicine_ids: []
        //     },
        //     meds: []
        //   },
        //   r3: {
        //     data: {
        //       medicineName: "",
        //       medicine_id: "",
        //       rataPemakaian: 0,
        //       onHand: 0,
        //       package_quantity: 0,
        //       orderQty: 0,
        //       reason: "",
        //       type: "",
        //       expiredDate: moment().format('YYYY-MM-DD'),
        //       medicine_ids: []
        //     },
        //     meds: []
        //   }
        // };
        let rdtIds = [];
        let rdtAllIds = [];
        let rdtMeds = [];

        if (mediciceType === "NON_ARV") {
          await API.Report.lbadbDetail(
            source.token,
            filter.PROVINCE,
            filter.SUDINKAB,
            filter.UPK,
            filter.MONTH,
            true
          ).then(rsp => {
            rsp.lbadbDetail.RDT.map(r => {
              rdtAllIds.push(r.medicine.id);
              for (let i = 1; i <= 3; i++) {
                // if (r.data['r' + i].stok_dipakai > 0) {
                rdtMeds[r.medicine.id] = {
                  totalPatient: 0,
                  rataPemakaian: Math.round((rdtMeds[r.medicine.id] ? (rdtMeds[r.medicine.id].rataPemakaian + r.data['r' + i].stok_dipakai) : r.data['r' + i].stok_dipakai) / 6),
                  onHand: 0,
                  smallOrderQty: (Math.round((rdtMeds[r.medicine.id] ? (rdtMeds[r.medicine.id].rataPemakaian + r.data['r' + i].stok_dipakai) : r.data['r' + i].stok_dipakai) / 6)) * pengkali,
                  package_quantity: Math.round(((Math.round((rdtMeds[r.medicine.id] ? (rdtMeds[r.medicine.id].rataPemakaian + r.data['r' + i].stok_dipakai) : r.data['r' + i].stok_dipakai) / 6)) * pengkali) / r.medicine.packageMultiplier),
                  orderQty: 0,
                  reason: "",
                  medicine_id: r.medicine.id,
                  stockUnitType: r.medicine.stockUnitType,
                  medicineName: r.medicine.name,
                  packageMultiplier: r.medicine.packageMultiplier,
                  expiredDate: moment().format('YYYY-MM-DD'),
                };
                // }
              }
              rdtIds.push(r.medicine.id);
            })
          })
            .catch(e => {
              console.error("e: ", e);
            });
          console.log(rdtMeds);

          rdtMeds.map(r => {
            regulerData.push(r);
          })

          // console.log(rdt);
          // for (let i = 1; i <= 3; i++) {
          //   let medIds = [];
          //   let total = 0;

          //   // console.log(rdt['r' + i].meds, 'ddddddddddddddddddddd');
          //   rdt['r' + i].meds.map(r => {
          //     medIds.push(r.medicine_id);
          //     total += r.package_quantity;
          //     // console.log(r.medicine_id, 'sssssssssssssssssss');
          //   })

          //   rdt['r' + i].data.medicineName = "Rapid Diagnostic Test " + i;
          //   rdt['r' + i].data.medicine_id = medIds.join('+');
          //   rdt['r' + i].data.medicine_ids = medIds;
          //   // rdt['r' + i].data.rataPemakaian = parseInt(total / rdt['r' + i].meds.length);
          //   rdt['r' + i].data.rataPemakaian = parseInt(total / 6);
          //   rdt['r' + i].data.rataPemakaian = rdt['r' + i].data.rataPemakaian ? rdt['r' + i].data.rataPemakaian : 0;
          //   rdt['r' + i].data.package_quantity = rdt['r' + i].data.rataPemakaian * pengkali;
          //   rdt['r' + i].data.orderQty = rdt['r' + i].data.package_quantity;
          //   rdt['r' + i].data.type = 'r' + i;
          //   rdt['r' + i].data.rdtAllIds = rdtAllIds;
          //   regulerData.push(rdt['r' + i].data);
          // }
          // console.log(rdt, '---------------');

        }

        // console.log(rdtIds);
        rsp.inventoryTable.content.map(v => {
          // if (v.medicine) {
          //   console.log(v.medicine.id, rdtIds.includes(v.medicine.id), v);
          // }
          // if (v.data.paket_keluar_reguler > 0 && v.data.column_G <= v.data.column_I) {
          if (v.data && v.medicine.medicineType === mediciceType && !rdtIds.includes(v.medicine.id)) {
            // console.log(v.medicine.id, '......................................');
            // console.log(v.data.paket_keluar_reguler);
            let qty = (Math.round(v.data.paket_keluar_reguler ? (v.data.paket_keluar_reguler) : 0)) * pengkali;
            regulerData.push({
              totalPatient: v.data.paket_keluar_reguler ? v.data.paket_keluar_reguler : 0,
              rataPemakaian: 0,
              onHand: v.data.column_G ? v.data.column_G : 0,
              smallOrderQty: (Math.round(v.data.paket_keluar_reguler ? (v.data.paket_keluar_reguler * v.medicine.packageMultiplier) : 0)) * pengkali,
              package_quantity: qty - v.data.column_G > 0 ? qty - v.data.column_G : 0,
              orderQty: qty - v.data.column_G > 0 ? qty - v.data.column_G : 0,
              reason: (qty - v.data.column_G > 0 ? qty - v.data.column_G : 0) <= v.data.column_I ? "" : "Cukup",
              medicine_id: v.medicine.id,
              stockUnitType: v.medicine.stockUnitType,
              medicineName: v.medicine.name,
              packageMultiplier: v.medicine.packageMultiplier,
              expiredDate: moment().format('YYYY-MM-DD'),

              // medicine_id: v.medicine.id,
              // stockUnitType: v.medicine.stockUnitType,
              // package_quantity: v.data.column_I,
              // medicineName: v.medicine.name,
              // totalPatient: v.data.paket_keluar_reguler ? v.data.paket_keluar_reguler : 0,
              // rataPemakaian: v.data.paket_keluar_reguler ? v.data.paket_keluar_reguler : 0,
              // onHand: v.data.column_G ? v.data.column_G : 0,
              // expiredDate: moment().format('YYYY-MM-DD'),
              // orderQty: (v.data.column_I - v.data.column_G) > 0 ? (v.data.column_I - v.data.column_G) : 0,
              // reason: v.data.column_G <= v.data.column_I ? "" : "Cukup",
              // packageMultiplier: v.medicine.packageMultiplier
            });
          } else {
            // stock
            const medId = v.medicine ? v.medicine.id : null;
            if (medId) {
              let filteredData = regulerData.filter(x => {
                if (x.medicine_id) {
                  // console.log(x, medId, 'mmmmmmmmmmmmmmmm');
                  return x.medicine_id == medId;
                }
                return false;
              });
              // console.log(filteredData, '======================', medId);
              filteredData.map(r => {
                const ii = regulerData.indexOf(r);
                // console.log(ii, regulerData[ii].onHand, '-------------', v.data.column_G ? v.data.column_G : 0);
                regulerData[ii].onHand = (regulerData[ii].onHand ? regulerData[ii].onHand : 0) + (v.data.column_G ? v.data.column_G : 0);
                regulerData[ii].reason = regulerData[ii].onHand <= regulerData[ii].package_quantity ? "" : "Cukup";
                regulerData[ii].package_quantity = regulerData[ii].package_quantity - (regulerData[ii].onHand * regulerData[ii].packageMultiplier);
                regulerData[ii].package_quantity = regulerData[ii].package_quantity < 0 ? 0 : regulerData[ii].package_quantity;
                regulerData[ii].orderQty = regulerData[ii].onHand <= regulerData[ii].package_quantity ? regulerData[ii].package_quantity : 0;
              })
            }
          }
          // } 
          return v;
        });
        setLoading(false);
      })
      .catch(e => {
        setLoading(false);
        console.error("e: ", e);
      })
      .finally(() => {
        setLoading(false);
      });
    console.log(regulerData);
    if (returnItem) {
      return regulerData
    }
    setDataReguler(regulerData);
  };

  const _onChangeQty = (name, e, idx) => {
    const value = e.target.value;
    let newDataReguler = [...dataReguler];
    newDataReguler[idx][name] = value;
    setDataReguler(newDataReguler);
  }

  // const _loadDataReguler3 = async () => {
  //   let regulerData = []
  //   let lbph2 = await _loadDataReguler2(true);
  //   await API.Logistic.listOrder(source.token, 'in', 'ALL', true)
  //     .then(rsp => {
  //       rsp.data.map(async v => {
  //         await API.Logistic.getOrderByID(source.token, v.id)
  //           .then(r => {
  //             r.data.orderItems.map(vv => {
  //               // console.log(vv, 'aaaaaa');
  //               let obj = regulerData.find(x => x.medicine_id === vv.medicine.id);
  //               let index = regulerData.indexOf(obj);
  //               // console.log(index, 'aaaa');
  //               if (index >= 0) {
  //                 regulerData[index].package_quantity += vv.packageQuantity;
  //               } else {
  //                 // console.log(lbph2, 'aasdasdsadasdasdasd');
  //                 let o = lbph2.find(x => x.medicine_id === vv.medicine.id);
  //                 let i = lbph2.indexOf(o);
  //                 let totalPatient = 0;
  //                 let onHand = 0;
  //                 if (i >= 0) {
  //                   totalPatient = lbph2[i].totalPatient ? lbph2[i].totalPatient : 0;
  //                   onHand = lbph2[i].onHand ? lbph2[i].onHand : 0;
  //                 }

  //                 if (totalPatient > 0) {
  //                   regulerData.push({
  //                     medicine_id: vv.medicine.id,
  //                     stockUnitType: vv.medicine.stockUnitType,
  //                     package_quantity: vv.packageQuantity,
  //                     medicineName: vv.medicine.name,
  //                     totalPatient: totalPatient,
  //                     onHand: onHand,
  //                     expiredDate: moment().format('YYYY-MM-DD')
  //                   });
  //                 }
  //               }
  //             })
  //           })
  //           .catch(e => {
  //             console.error("e: ", e);
  //           })
  //           .finally(() => {
  //           });
  //       })
  //       setDataReguler(regulerData);
  //       setLoading(false);
  //     })
  //     .catch(e => {
  //       setLoading(false);
  //       console.error("e: ", e);
  //     })
  //     .finally(() => {
  //       setLoading(false);
  //     });
  // }
  // end new

  const _loadData = () => {
    setLoading(true);
    API.Logistic.getOrderByID(source.token, id)
      .then(rsp => {
        setData(rsp.data || orderDetailEmptyState);
        setSelectedDest({
          id: rsp.data.senderId,
          type: DEST_TYPE[rsp.data.senderType]
        });
        if (rsp.data) {
          setItems(rsp.data.orderItems);
        }
      })
      .catch(e => {
        console.error("e: ", e);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  async function _createRegular() {
    try {
      setLoading(true);
      await API.Logistic.postRegularDemand(dataReguler, data);
      openNotification("success", "Berhasil", "Permintaan berhasil diinput");
      reload();
      setLoading(false);
      toggleModal();
    } catch (ex) {
      openNotification("error", "Gagal", ex.data ? ex.data.message : "Permintaan gagal diinput");
      console.error("e: ", ex);
      setLoading(false);
    }
  }

  const _create = payload => {
    setLoading(true);
    API.Logistic.requestOrder(source.token, payload)
      .then(() => {
        setLoading(false);
        openNotification("success", "Berhasil", "Permintaan berhasil diinput");
        reload();
        toggleModal();
      })
      .catch(e => {
        openNotification("error", "Gagal", e.message || String(e.data ? e.data.message : "").replace(/_/g, " "));
        console.error("e: ", e);
        setLoading(false);
      });
  };

  const _update = payload => {
    setLoading(true);
    API.Logistic.updateOrder(source.token, id, payload)
      .then(() => {
        setLoading(false);
        openNotification("success", "Berhasil", "Permintaan berhasil diinput");
        reload();
        toggleModal();
      })
      .catch(e => {
        openNotification("error", "Gagal", e.message || String(e.data ? e.data.message : "").replace(/_/g, " "));
        console.error("e: ", e);
        setLoading(false);
      });
  };

  const _onSubmit = () => {
    if (type === 'reguler') {
      return _createRegular()
    }

    const payload = _constructPayload(data, items);
    if (isEdit) {
      return _update(payload);
    }

    return _create(payload);
  };

  useEffect(() => {
    if (isEdit) {
      _loadData();
    }

    if (type === 'reguler') {
      _loadDataReguler();
      if (user.logisticrole === ENUM.LOGISTIC_ROLE.UPK_ENTITY) {
        _loadDataReguler2();
      } else if ([ENUM.LOGISTIC_ROLE.SUDIN_ENTITY, ENUM.LOGISTIC_ROLE.PROVINCE_ENTITY].includes(user.logisticrole)) {
        // _loadDataReguler3();
        _loadDataReguler2();
      }
    }

    return () => {
      source.cancel("request canceled");
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <ModalOrder
      onCancel={toggleModal}
      isEdit={isEdit}
      onOk={_onSubmit}
      isLoading={isLoading}
      onChange={_handleChange}
      role={ROLE.PHARMA_STAFF}
      selectedDest={selectedDest}
      data={data}
      items={items}
      addItem={_addItem}
      removeItem={_removeItem}
      onChangeItem={_handleChangeItem}
      onChangeDestination={_handleChangeDestination}
      type={type}
      dataReguler={dataReguler}
      onChangeQty={_onChangeQty}
      medicineType={mediciceType}
    />
  );
};

export default Handler;

const _constructPayload = (data, items) => {
  const newItems = cloneDeep(items);
  const nnewItems = [];
  newItems.map(v => {
    if (v.medicineId !== null && v.packageQuantity !== null) {
      nnewItems.push(v);
    }
  });
  const newData = { ...data };
  for (let x = 0; x < nnewItems.length; x++) {
    delete nnewItems[x].id;
  }

  delete newData.id;

  return {
    ...newData,
    ordersItems: nnewItems
  };
};