import React from "react";

import { useParams } from "react-router";
import ListPermintaan from "./list";
import DetailPermintaan from "./detail";

const Permintaan = () => {
  const { id } = useParams();

  return (
    <React.Fragment>
      {!id && <ListPermintaan />}
      {id && <DetailPermintaan />}
    </React.Fragment>
  );
};

export default Permintaan;
