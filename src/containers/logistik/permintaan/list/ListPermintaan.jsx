import React, { useState, useContext } from "react";

import { DataTable, DisplayDate } from "components";
import { Link } from "react-router-dom";
import { useParams } from "react-router";
import {
  ROLE,
  LOGISTIK,
  TAG_LABEL,
  LIST_APPROVAL_STATUS,
  ENUM,
  REQUEST_TYPE
} from "constant";
import { Button, Tag, Typography, Select } from "antd";
import FormOrder from "containers/logistik/permintaan/form";
import GlobalContext from "containers/GlobalContext";
const { Option } = Select;

export const ListPermintaan = ({
  data,
  isLoading,
  reload,
  onChangeStatus,
  status,
  onChangeRequestType,
  requestType
}) => {
  const user = useContext(GlobalContext);
  const { type } = useParams();
  const [isFormOpen, setFormOpen] = useState(false);
  const [mediciceType, setMediciceType] = useState(null);

  const _toggleModal = (medicice_type) => {
    setFormOpen(!isFormOpen);
    setMediciceType(medicice_type);
  };

  const columns = [
    {
      title: "Invoice Number",
      dataIndex: "orderCode",
      key: "orderCode"
    },
    {
      title: "Status Permintaan",
      dataIndex: "lastOrderStatus",
      key: "lastOrderStatus",
      render: lastOrderStatus =>
        type === "in" ? (
          <Tag color={TAG_LABEL.TAG_PLAN_STATUS_IN[lastOrderStatus]}>
            {TAG_LABEL.LABEL_PLAN_STATUS_IN[lastOrderStatus]}
          </Tag>
        ) : (
            <Tag color={TAG_LABEL.TAG_PLAN_STATUS[lastOrderStatus]}>
              {TAG_LABEL.LABEL_PLAN_STATUS[lastOrderStatus]}
            </Tag>
          )
    },
    {
      title: "Status Approval",
      dataIndex: "lastApprovalStatus",
      key: "lastApprovalStatus",
      render: lastApprovalStatus =>
        type === "in" ? (
          <Tag color={TAG_LABEL.TAG_APPROVAL_STATUS_IN[lastApprovalStatus]}>
            {TAG_LABEL.LABEL_APPROVAL_STATUS_IN[lastApprovalStatus]}
          </Tag>
        ) : (
            <Tag color={TAG_LABEL.TAG_APPROVAL_STATUS[lastApprovalStatus]}>
              {
                user.role !== ROLE.PHARMA_STAFF ?
                  TAG_LABEL.LABEL_APPROVAL_STATUS[lastApprovalStatus] : TAG_LABEL.LABEL_APPROVAL_STATUS[lastApprovalStatus].replace('Pengelola Program', 'Kepala Faskes')
              }
            </Tag>
          )
    },
    {
      title: "Pengaju",
      dataIndex: "recieverName",
      key: "recieverName"
    },
    {
      title: "Dituju",
      dataIndex: "senderName",
      key: "senderName"
    },
    // {
    //   title: "Reguler",
    //   dataIndex: "isRegular",
    //   key: "isRegular",
    //   render: isRegular =>
    //     isRegular ? (
    //       <Tag color={TAG_LABEL.TAG_REQUEST_TYPE.PERMINTAAN_REGULER}>Ya</Tag>
    //     ) : (
    //         <Tag color={TAG_LABEL.TAG_REQUEST_TYPE.PERMINTAAN_KHUSUS}>Tidak</Tag>
    //       )
    // },
    {
      title: "Tanggal dibuat",
      dataIndex: "createdAt",
      key: "createdAt",
      render: date => <DisplayDate date={date} />
    },
    {
      title: "Terakhir Diubah",
      dataIndex: "updatedAt",
      key: "updatedAt",
      render: date => <DisplayDate date={date} />
    },
    {
      title: "Aksi",
      key: "action",
      fixed: "right",
      render: item => {
        return (
          <Link
            to={`/pharma/permintaan/${type}/${item.id}`}
            className="ant-btn hs-btn PHARMA_STAFF ant-btn-primary ant-btn-sm"
          >
            Detail
          </Link>
        );
      }
    }
  ];

  const columns2 = [
    {
      title: "Invoice Number",
      dataIndex: "orderCode",
      key: "orderCode"
    },
    {
      title: "Status Permintaan",
      dataIndex: "lastOrderStatus",
      key: "lastOrderStatus",
      render: lastOrderStatus =>
        type === "in" ? (
          <Tag color={TAG_LABEL.TAG_PLAN_STATUS_IN[lastOrderStatus]}>
            {TAG_LABEL.LABEL_PLAN_STATUS_IN[lastOrderStatus]}
          </Tag>
        ) : (
            <Tag color={TAG_LABEL.TAG_PLAN_STATUS[lastOrderStatus]}>
              {TAG_LABEL.LABEL_PLAN_STATUS[lastOrderStatus]}
            </Tag>
          )
    },
    {
      title: "Status Approval",
      dataIndex: "lastApprovalStatus",
      key: "lastApprovalStatus",
      render: lastApprovalStatus =>
        type === "in" ? (
          <Tag color={TAG_LABEL.TAG_APPROVAL_STATUS_IN[lastApprovalStatus]}>
            {TAG_LABEL.LABEL_APPROVAL_STATUS_IN[lastApprovalStatus]}
          </Tag>
        ) : (
            <Tag color={TAG_LABEL.TAG_APPROVAL_STATUS[lastApprovalStatus]}>
              {
                user.role !== ROLE.PHARMA_STAFF ?
                  TAG_LABEL.LABEL_APPROVAL_STATUS[lastApprovalStatus] : TAG_LABEL.LABEL_APPROVAL_STATUS[lastApprovalStatus].replace('Pengelola Program', 'Kepala Faskes')
              }
            </Tag>
          )
    },
    {
      title: "Pengaju",
      dataIndex: "recieverName",
      key: "recieverName"
    },
    {
      title: "Dituju",
      dataIndex: "senderName",
      key: "senderName"
    },
    {
      title: "Jenis Permintaan",
      dataIndex: "isRegular",
      key: "isRegular",
      render: isRegular =>
        isRegular ? (
          <Tag color={TAG_LABEL.TAG_REQUEST_TYPE.PERMINTAAN_REGULER}>Reguler</Tag>
        ) : (
            <Tag color={TAG_LABEL.TAG_REQUEST_TYPE.PERMINTAAN_KHUSUS}>Khusus</Tag>
          )
    },
    {
      title: "Tanggal dibuat",
      dataIndex: "createdAt",
      key: "createdAt",
      render: date => <DisplayDate date={date} />
    },
    {
      title: "Terakhir Diubah",
      dataIndex: "updatedAt",
      key: "updatedAt",
      render: date => <DisplayDate date={date} />
    },
    {
      title: "Aksi",
      key: "action",
      fixed: "right",
      render: item => {
        return (
          <Link
            to={`/pharma/permintaan/${type}/${item.id}`}
            className="ant-btn hs-btn PHARMA_STAFF ant-btn-primary ant-btn-sm"
          >
            Detail
          </Link>
        );
      }
    }
  ];

  return (
    <React.Fragment>
      {isFormOpen && <FormOrder toggleModal={_toggleModal} reload={reload} type={type} mediciceType={mediciceType} />}

      <div className="container h-100 pt-4">

        <div className="row mb-3">
          <div className="col-3">
            <Typography
              fontSize="18px"
              fontWeight="bold"
              color="PHARMA_STAFF"
              className="d-block mb-3"
            >
              Status Approval
            </Typography>
            <Select
              style={{ width: "220px" }}
              onChange={onChangeStatus}
              value={status}
            >
              {type === "in"
                ? LIST_APPROVAL_STATUS.map(
                  item =>
                    item !== ENUM.LAST_APPROVAL_STATUS.DRAFT &&
                    item !== ENUM.LAST_APPROVAL_STATUS.IN_EVALUATION &&
                    item !== ENUM.LAST_APPROVAL_STATUS.REJECTED_INTERNAL &&
                    item !== ENUM.LAST_APPROVAL_STATUS.REJECTED && (
                      <Option key={item} value={item}>
                        <Tag color={TAG_LABEL.TAG_APPROVAL_STATUS_IN[item]}>
                          {TAG_LABEL.LABEL_APPROVAL_STATUS_IN[item]}
                        </Tag>
                      </Option>
                    )
                )
                : LIST_APPROVAL_STATUS.map(
                  item =>
                    item !== ENUM.LAST_APPROVAL_STATUS.REJECTED && (
                      <Option key={item} value={item}>
                        <Tag color={TAG_LABEL.TAG_APPROVAL_STATUS[item]}>
                          {
                            user.role !== ROLE.PHARMA_STAFF ?
                              TAG_LABEL.LABEL_APPROVAL_STATUS[item] : TAG_LABEL.LABEL_APPROVAL_STATUS[item].replace('Pengelola Program', 'Kepala Faskes')
                          }
                        </Tag>
                      </Option>
                    )
                )}
            </Select>
          </div>
          <div className="col-4">
            <Typography
              fontSize="18px"
              fontWeight="bold"
              color="PHARMA_STAFF"
              className="d-block mb-3"
            >
              Jenis Permintaan
            </Typography>
            <Select
              style={{ width: "220px" }}
              onChange={onChangeRequestType}
              value={requestType}
            >
              {REQUEST_TYPE.map(item => (
                <Option key={item} value={TAG_LABEL.VALUE_REQUEST_TYPE[item]}>
                  <Tag color={TAG_LABEL.TAG_REQUEST_TYPE[item]}>
                    {TAG_LABEL.LABEL_REQUEST_TYPE[item]}
                  </Tag>
                </Option>
              ))}
            </Select>
          </div>
        </div>

        <div className="row">
          <div className="col-12 bg-white rounded p-4 border">
            <DataTable
              title={`Daftar Permintaan ${type === LOGISTIK.PENERIMAAN_TYPE.IN ? "Masuk" : type === 'reguler' ? "Reguler" : "Keluar"
                }`}
              columns={type === "in" ? columns2 : columns}
              // columns={type !== 'reguler' ? columns : regulerTableColumn}
              data={data}
              isLoading={isLoading}
              isScrollX
              button={
                type === LOGISTIK.PENERIMAAN_TYPE.OUT ? (
                  <Button
                    className="hs-btn-outline PHARMA_STAFF"
                    onClick={_toggleModal}
                  >
                    Buat Permintaan Khusus
                  </Button>
                ) : type === 'reguler' && (
                  <React.Fragment>
                    <Button
                      className="hs-btn-outline PHARMA_STAFF mr-3"
                      onClick={() => _toggleModal('ARV')}
                    >
                      Buat Permintaan Reguler (ARV)
                    </Button>
                    <Button
                      className="hs-btn-outline PHARMA_STAFF"
                      onClick={() => _toggleModal('NON_ARV')}
                    >
                      Buat Permintaan Reguler (NON_ARV)
                    </Button>
                  </React.Fragment>
                )
              }
            />
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};
