import React, { useEffect, useState } from "react";
import API from "utils/API";
import axios from "axios";
// import shortid from "shortid";

import { useParams } from "react-router";
import { ListPermintaan } from "./ListPermintaan";

const Handler = () => {
  let source = axios.CancelToken.source();

  const [data, setData] = useState([]);
  const [isLoading, setLoading] = useState(false);
  const { type } = useParams();
  const [status, setStatus] = useState("ALL");
  const [requestType, setRequestType] = useState("ALL");

  const _handleChangeStatus = value => {
    setStatus(value);
  };

  const _handleChangeRequestType = value => {
    setRequestType(value);
  };

  const _loadData = () => {
    setLoading(true);
    const orderType = type === "in" ? type : "";
    API.Logistic.listOrder(source.token, orderType, status, requestType)
      .then(rsp => {
        setData(rsp.data || []);
      })
      .catch(e => {
        console.error("e: ", e);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  useEffect(() => {
    _loadData();

    // if (type === 'reguler') {
    //   setRequestType(true)
    // } else if (type !== 'reguler' && requestType === 'ALL') {
    //   setRequestType('ALL')
    // } else if (type !== 'reguler' && requestType === false) {
    //   setRequestType(false)
    // }

    if (type === 'reguler') {
      setRequestType(true)
    } else if (type === 'out') {
      setRequestType(false)
    } else {
      setRequestType('ALL')
    }

    return () => {
      source.cancel("request canceled");
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [type, status, requestType]);

  return (
    <ListPermintaan
      data={data}
      isLoading={isLoading}
      reload={_loadData}
      onChangeStatus={_handleChangeStatus}
      status={status}
      onChangeRequestType={_handleChangeRequestType}
      requestType={requestType}
    />
  );
};

export default Handler;
