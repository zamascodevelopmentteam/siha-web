import React, { useEffect, useState } from "react";
import API from "utils/API";
import axios from "axios";
import { cloneDeep } from "lodash";
import { useParams } from "react-router";
import { openNotification } from "utils/Notification";

import { ModalRencanaDistribusi } from "components";
import { ROLE } from "constant";

const Handler = ({ toggleModal, reload }) => {
  let source = axios.CancelToken.source();
  const { id } = useParams();

  const [data, setData] = useState([]);
  const [orderID, setOrderID] = useState(null);
  const [isLoading, setLoading] = useState(false);

  const _handleChange = (idDist, idItems, value) => {
    let newData = cloneDeep(data);
    newData[idDist].pickings[idItems].packageQuantity = value;
    
    let total = 0;
    newData[idDist].pickings.map(r => {
      total += r.packageQuantity;
    })

    if (newData[idDist].type && total > newData[idDist].rAmount) {
      newData[idDist].isValid = false;
    } else {
      newData[idDist].isValid = true;
    }

    setData(newData);
  };

  let sisa = [];
  let sisaInven = {};

  const _loadData = async () => {
    setLoading(true);
    try {
      const rsp = await API.Logistic.getOrderByID(source.token, id);
      const { orderItems } = rsp.data;
      setOrderID(rsp.data.id);
      for (let x = 0; x < orderItems.length; x++) {
        const { medicineId, id: orderItemId } = orderItems[x];
        const { expiredDate } = orderItems[x];
        const rsp = await API.Logistic.getPickingList(
          source.token,
          medicineId,
          expiredDate
        );
        const pickings = rsp.data;
        const newPickings = [];
        for (let y = 0; y < pickings.length; y++) {
          const {
            inventoryId,
            batchCode,
            expiredDate,
            packageQuantity,
            packageUnitType,
            brand
          } = pickings[y];
          const newPicking = {
            orderItemId,
            inventoryId,
            batchCode,
            expiredDate,
            packageUnitType,
            stockQty: sisaInven[pickings[y].inventoryId] === undefined ? packageQuantity : sisaInven[pickings[y].inventoryId],
            // stockQty: packageQuantity,
            brandName: brand.brandName,
            packageQuantity: orderItems[x].packageQuantity
          };

          // if (total[x] === undefined) {
          //   total[x] = 0;
          // }
          if (sisaInven[pickings[y].inventoryId] === undefined) {
            sisaInven[pickings[y].inventoryId] = pickings[y].packageQuantity
          }
          if (sisa[x] === undefined) {
            sisa[x] = orderItems[x].packageQuantity;
          }
          let qty = 0;
          if (sisa[x] > newPicking.stockQty) {
            sisa[x] -= newPicking.stockQty;
            qty = newPicking.stockQty;
            newPicking.packageQuantity = qty;
          } else {
            qty = sisa[x];
            newPicking.packageQuantity = qty;
            sisa[x] = 0;
          }
          // total[x] += qty;
          newPicking.max = qty;
          sisaInven[pickings[y].inventoryId] -= qty;

          newPickings.push(newPicking);
        }
        orderItems[x].pickings = newPickings;
        orderItems[x].isValid = true;
      }
      // console.log(orderItems);
      setData([...orderItems]);
      setLoading(false);
    } catch (e) {
      console.error("e: ", e);
      setLoading(false);
    }
  };

  const _onSubmit = () => {
    const payload = _constructPayload(data, orderID);
    setLoading(true);
    API.Logistic.createDistributionPlanWithOrder(source.token, payload)
      .then(() => {
        setLoading(false);
        openNotification(
          "success",
          "Berhasil",
          "Distribusi plan berhasil diinput"
        );
        reload();
        toggleModal();
      })
      .catch(e => {
        openNotification("error", "Gagal", e.message || String(e.data ? e.data.message : "").replace(/_/g, " "));
        console.error("e: ", e);
        setLoading(false);
      });
  };

  useEffect(() => {
    _loadData();

    return () => {
      source.cancel("request canceled");
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <ModalRencanaDistribusi
      onCancel={toggleModal}
      onOk={_onSubmit}
      isLoading={isLoading}
      onChange={_handleChange}
      role={ROLE.PHARMA_STAFF}
      data={data}
    />
  );
};

export default Handler;

const _constructPayload = (orders, orderId) => {
  const distributionPlansItems = [];
  const distributionPlans = {
    orderId
  };

  for (let x = 0; x < orders.length; x++) {
    for (let y = 0; y < orders[x].pickings.length; y++) {
      if (
        orders[x].pickings[y].packageQuantity !== null &&
        orders[x].pickings[y].packageQuantity !== 0
      ) {
        distributionPlansItems.push(orders[x].pickings[y]);
      }
    }
  }

  return {
    distributionPlans,
    distributionPlansItems
  };
};
