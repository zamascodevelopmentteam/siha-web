import React, { useState, useContext } from "react";

import { DataTable, Typography, DisplayDate } from "components";
import { useParams } from "react-router";
import { ROLE, ENUM, TAG_LABEL } from "constant";
import { Button, Tag } from "antd";

import FormOrder from "containers/logistik/permintaan/form";
import FormConfirm from "./ModalConfirm";
import DistribusiForm from "./DistribusiForm";
import ChangeLogs from "./ChangeLogs";
import { Link } from "react-router-dom";
import { formatNumber } from "utils";

// new
import GlobalContext from "containers/GlobalContext";
// end new

export const DetailPermintaan = ({ data, isLoading, reload }) => {
  const user = useContext(GlobalContext);

  const [isConfirmOpen, setConfirmOpen] = useState(false);
  const [isFormOpen, setFormOpen] = useState(false);
  const [isDistributionOpen, setDistributionOpen] = useState(false);
  const [isChangeLogsOpen, setChangeLogsOpen] = useState(false);
  const { type, id } = useParams();

  const _toggleModal = {
    form: () => setFormOpen(!isFormOpen),
    confirm: () => setConfirmOpen(!isConfirmOpen),
    distribution: () => setDistributionOpen(!isDistributionOpen),
    changeLogs: () => setChangeLogsOpen(!isChangeLogsOpen)
  };

  const columnsDistribusi = [
    {
      title: "Nomor Surat Jalan",
      dataIndex: "droppingNumber",
      key: "droppingNumber"
    },
    {
      title: "Status",
      dataIndex: "planStatus",
      key: "planStatus",
      render: planStatus => (
        <Tag color={TAG_LABEL.TAG_PLAN_STATUS[planStatus]}>
          {TAG_LABEL.LABEL_PLAN_STATUS[planStatus]}
        </Tag>
      )
    },
    {
      title: "Total Harga",
      dataIndex: "totalPrice",
      key: "totalPrice",
      render: price => `Rp. ${formatNumber(Number(price))}`
    },
    {
      title: "Terakhir Update",
      dataIndex: "updatedAt",
      key: "updatedAt",
      render: date => <DisplayDate date={date} />
    },
    {
      title: "Aksi",
      key: "action",
      render: item => (
        <Link
          className="ant-btn hs-btn-outline PHARMA_STAFF"
          to={`/pharma/${type === "out" ? "penerimaan" : "pengiriman"}/${item.id
            }`}
          type="small"
        >
          Lihat
        </Link>
      )
    }
  ];

  const allowToConfirmPusat = user.logisticrole === ENUM.LOGISTIC_ROLE.MINISTRY_ENTITY && TAG_LABEL.LABEL_APPROVAL_STATUS[data.lastApprovalStatus] === TAG_LABEL.LABEL_APPROVAL_STATUS.APPROVED_INTERNAL;

  return (
    <React.Fragment>
      {isFormOpen && (
        <FormOrder
          toggleModal={_toggleModal.form}
          isEdit
          id={id}
          reload={reload}
        />
      )}

      {isConfirmOpen && (
        <FormConfirm toggleModal={_toggleModal.confirm} reload={reload} />
      )}

      {isDistributionOpen && (
        <DistribusiForm
          toggleModal={_toggleModal.distribution}
          reload={reload}
        />
      )}

      {isChangeLogsOpen && (
        <ChangeLogs
          toggleModal={_toggleModal.changeLogs}
          reload={reload}
          data={data.orderLogs}
        />
      )}

      <div className="container h-100 pt-4">
        <div className="row">
          <div className="col-12 p-4">
            <Typography
              fontSize="20px"
              fontWeight="bold"
              className="d-block mb-3"
            >
              Detail Permintaan
            </Typography>
          </div>
        </div>
        <div className="row">
          <div className="col-12 bg-white rounded p-4 border mb-5">
            <Label text="Nomor Invoice Permintaan" />
            <Info>{data.orderCode}</Info>

            <Label text="Status Penerimaan" />
            <Info>
              <Tag color={TAG_LABEL.TAG_PLAN_STATUS[data.lastOrderStatus]}>
                {TAG_LABEL.LABEL_PLAN_STATUS[data.lastOrderStatus]}
              </Tag>
            </Info>

            <Label text="Status Approval" />
            <Info>
              <Tag
                color={TAG_LABEL.TAG_APPROVAL_STATUS[data.lastApprovalStatus]}
              >
                {
                  user.role !== ROLE.PHARMA_STAFF ?
                    TAG_LABEL.LABEL_APPROVAL_STATUS[data.lastApprovalStatus] : (
                      TAG_LABEL.LABEL_APPROVAL_STATUS[data.lastApprovalStatus] !== undefined ?
                      TAG_LABEL.LABEL_APPROVAL_STATUS[data.lastApprovalStatus].replace('Pengelola Program', 'Kepala Faskes') : null
                    )
                }
              </Tag>
            </Info>

            <Label text="Catatan" />
            <Info>{data.orderNotes}</Info>

            <Label text="Catatan Approval" />
            <Info>{data.approvalNotes}</Info>

            <Label text="Tanggal Terakhir Diubah" />
            <Info>
              <DisplayDate date={data.updatedAt} />
            </Info>

            <Label text="Dibuat Oleh" />
            <Info>{data.recieverName}</Info>

            <Label text="Terakhir Diubah Oleh" />
            <Info>
              {data.updateByData && data.updateByData.updatedName}
              {data.createdByData &&
                !data.updateByData &&
                data.createdByData.createdName}
            </Info>

            {type === "out" && data.lastOrderStatus === ENUM.LAST_ORDER_STATUS.DRAFT && (<React.Fragment><Button onClick={_toggleModal.form}>Ubah Permintaan</Button><br /><br /></React.Fragment>)}

            <Button
              className="hs-btn-outline PHARMA_STAFF"
              onClick={_toggleModal.changeLogs}
            >
              Detail Perubahan
            </Button>

            <Typography
              fontSize="14px"
              fontWeight="bold"
              className="d-block my-3 text-center"
            >
              Data Barang Permintaan
            </Typography>

            <DataTable
              columns={data.isRegular ? columns : columnsSpecial}
              data={data.orderItems}
              isScrollX
              rowKey="id"
              pagination={false}
              isLoading={isLoading}
            />

            <Typography
              fontSize="14px"
              fontWeight="bold"
              className="d-block my-3 text-center"
            >
              Data Rencana Distribusi
            </Typography>

            {type === "in" &&
              data.lastOrderStatus === ENUM.LAST_ORDER_STATUS.PROCESSED && data.distributionPlans.length <= 0 && (
                <Button
                  className="hs-btn-outline PHARMA_STAFF"
                  onClick={_toggleModal.distribution}
                >
                  Pemenuhan Permintaan
                </Button>
              )
            }

            <DataTable
              columns={columnsDistribusi}
              data={data.distributionPlans}
              isScrollX
              rowKey="id"
              pagination={false}
              isLoading={isLoading}
            />

            {data.isAllowedToConfirm && (TAG_LABEL.LABEL_APPROVAL_STATUS[data.lastApprovalStatus] === TAG_LABEL.LABEL_APPROVAL_STATUS.DRAFT || TAG_LABEL.LABEL_APPROVAL_STATUS[data.lastApprovalStatus] === TAG_LABEL.LABEL_APPROVAL_STATUS.IN_EVALUATION || allowToConfirmPusat || ([ENUM.USER_ROLE.SUDIN_STAFF, ENUM.USER_ROLE.PROVINCE_STAFF].includes(user.role) && TAG_LABEL.LABEL_APPROVAL_STATUS[data.lastApprovalStatus] === TAG_LABEL.LABEL_APPROVAL_STATUS.APPROVED_INTERNAL)) && (
              <div className="d-flex justify-content-center mt-3">
                <Button
                  className="hs-btn PHARMA_STAFF"
                  onClick={_toggleModal.confirm}
                >
                  Konfirmasi
                </Button>
              </div>
            )}
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

const Label = ({ text, ...props }) => (
  <Typography
    fontSize="12px"
    fontWeight="bold"
    className="d-block mb-2"
    {...props}
  >
    {text}
  </Typography>
);

const Info = ({ children, ...props }) => (
  <Typography
    fontSize="12px"
    fontWeight="600"
    className="d-block mb-3"
    {...props}
  >
    {children}
  </Typography>
);

const columns = [
  {
    title: "Nama Jenis Barang",
    // dataIndex: "medicine",
    key: "name",
    render: r => r.medicine.name
  },
  {
    title: "Kode",
    dataIndex: "medicine",
    key: "codeName",
    render: medicine => `${medicine.codeName} `
  },
  {
    title: "Tipe",
    dataIndex: "medicine",
    key: "medicineType",
    render: medicine => `${medicine.medicineType} `
  },
  {
    title: "Jumlah Barang dalam Kemasan",
    dataIndex: "medicine",
    key: "packageMultiplier",
    render: medicine => `${medicine.packageMultiplier} `
  },
  {
    title: "Jumlah Barang",
    // key: "packageUnitType",
    render: item => `${item.packageQuantity} ${item.packageUnitType}`
  },
  {
    title: "Jumlah Pasien",
    key: "totalPatient",
    render: item => `${item.totalPatient}`
  },
  {
    title: "Alasan",
    key: "reason",
    render: item => `${item.reason}`
  },
  // {
  //   title: "Harga Satuan",
  //   dataIndex: "medicine",
  //   key: "unitPrice",
  //   render: medicine => `Rp. ${formatNumber(medicine.unitPrice)}`
  // },
  // {
  //   title: "Minimal Expired Date",
  //   dataIndex: "expiredDate",
  //   key: "expiredDate",
  //   render: date => <DisplayDate date={date} />
  // }
];

const columnsSpecial = [
  {
    title: "Nama Jenis Barang",
    dataIndex: "medicine",
    key: "name",
    render: medicine => `${medicine.name} `
  },
  {
    title: "Kode",
    dataIndex: "medicine",
    key: "codeName",
    render: medicine => `${medicine.codeName} `
  },
  {
    title: "Tipe",
    dataIndex: "medicine",
    key: "medicineType",
    render: medicine => `${medicine.medicineType} `
  },
  {
    title: "Jumlah Barang dalam Kemasan",
    dataIndex: "medicine",
    key: "packageMultiplier",
    render: medicine => `${medicine.packageMultiplier} `
  },
  {
    title: "Jumlah Barang",
    key: "packageUnitType",
    render: item => `${item.packageQuantity} ${item.packageUnitType}`
  },
  // {
  //   title: "Jumlah Pasien",
  //   key: "totalPatient",
  //   render: item => `${item.totalPatient}`
  // },
  // {
  //   title: "Alasan",
  //   key: "reason",
  //   render: item => `${item.reason}`
  // },
  // {
  //   title: "Harga Satuan",
  //   dataIndex: "medicine",
  //   key: "unitPrice",
  //   render: medicine => `Rp. ${formatNumber(medicine.unitPrice)}`
  // },
  // {
  //   title: "Minimal Expired Date",
  //   dataIndex: "expiredDate",
  //   key: "expiredDate",
  //   render: date => <DisplayDate date={date} />
  // }
];