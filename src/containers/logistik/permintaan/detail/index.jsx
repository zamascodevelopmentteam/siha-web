import React, { useEffect, useState } from "react";
import API from "utils/API";
import axios from "axios";

import { useParams } from "react-router";
import { DetailPermintaan } from "./DetailPermintaan";

const orderDetailEmptyState = {
  id: "",
  orderCode: "",
  orderNumber: "",
  lastOrderStatus: "",
  lastApprovalStatus: "",
  orderNotes: "",
  approvalNotes: "",
  isRegular: "",
  picId: "",
  logisticRole: "",
  upkIdOwner: "",
  sudinKabKotaIdOwner: "",
  provinceIdOwner: "",
  upkIdFor: "",
  sudinKabKotaIdFor: "",
  provinceIdFor: "",
  createdBy: "",
  updatedBy: "",
  deletedAt: "",
  createdAt: "",
  updatedAt: "",
  orderItems: [],
  distributionPlans: [],
  createdByData: {
    createdName: ""
  }
};

const Handler = () => {
  let source = axios.CancelToken.source();

  const [data, setData] = useState(orderDetailEmptyState);
  const [isLoading, setLoading] = useState(false);
  const { type, id } = useParams();

  const _loadData = () => {
    setLoading(true);
    API.Logistic.getOrderByID(source.token, id)
      .then(rsp => {
        let lastType = "";
        const orderItems = [...rsp.data.orderItems];
        const newOrderItems = [];

        orderItems.map(r => {
          if (r.type && lastType != r.type) {
            let newData = {...r};
            newData.medicine.name = newData.type.replace('r', 'Rapid Diagnostic Test ');
            newData.medicine.codeName = "-";
            newData.medicine.packageMultiplier = "-";
            newData.packageQuantity = newData.rAmount;
            newData.totalPatient = "-";
            newOrderItems.push(newData);
            lastType = r.type;
          } else if (!r.type) {
            newOrderItems.push(r);
          }
        });

        const newRsp = {
          ...rsp.data,
          orderItems: newOrderItems
        };

        console.log(newOrderItems);

        setData(newRsp || orderDetailEmptyState);
      })
      .catch(e => {
        console.error("e: ", e);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  useEffect(() => {
    _loadData();

    return () => {
      source.cancel("request canceled");
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [type]);

  return (
    <DetailPermintaan data={data} isLoading={isLoading} reload={_loadData} />
  );
};

export default Handler;
