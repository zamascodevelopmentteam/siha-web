import React, { useEffect, useState } from "react";
import API from "utils/API";
import axios from "axios";
import { useParams } from "react-router";
import { openNotification } from "utils/Notification";

import { ModalKonfirmasiPermintaan } from "components";
import { ROLE } from "constant";

const orderDetailEmptyState = {
  notes: null,
  status: null
};

const STATUS_OPTION = {
  DRAFT: ["IN_EVALUATION"],
  IN_EVALUATION: ["APPROVED_INTERNAL", "REJECTED_INTERNAL"],
  APPROVED_INTERNAL: ["APPROVED_FULL", "REJECTED_UPPER"]
};

const Handler = ({ toggleModal, reload }) => {
  let source = axios.CancelToken.source();
  const { id } = useParams();

  const [data, setData] = useState(orderDetailEmptyState);
  const [isLoading, setLoading] = useState(false);
  const [error, setError] = useState("");
  const [options, setOptions] = useState([]);

  const _handleChange = (name, value) => {
    setData({
      ...data,
      [name]: value
    });
  };

  const _onSubmit = () => {
    setLoading(true);
    const { notes, status } = data;
    const payload = { note: notes };
    API.Logistic.confirmOrder(source.token, id, status, payload)
      .then(() => {
        openNotification("success", "Berhasil", "Konfirmasi berhasil diinput");
        toggleModal();
        reload();
      })
      .catch(e => {
        openNotification("error", "Gagal", e.message || String(e.data ? e.data.message : "").replace(/_/g, " "));
        console.error("e: ", e);
        setError(e.message);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const _loadData = () => {
    setLoading(true);
    API.Logistic.getOrderByID(source.token, id)
      .then(rsp => {
        setData(rsp.data || orderDetailEmptyState);
        setOptions(STATUS_OPTION[rsp.data.lastApprovalStatus]);
      })
      .catch(e => {
        console.error("e: ", e);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  useEffect(() => {
    _loadData();

    return () => {
      source.cancel("request canceled");
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <ModalKonfirmasiPermintaan
      onCancel={toggleModal}
      onOk={_onSubmit}
      isLoading={isLoading}
      onChange={_handleChange}
      role={ROLE.PHARMA_STAFF}
      data={data}
      options={options}
      error={error}
    />
  );
};

export default Handler;
