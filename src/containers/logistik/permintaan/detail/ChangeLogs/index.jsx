import React, { useEffect, useState } from "react";
import axios from "axios";

import { ModalChangeLogs } from "components";

const Handler = ({ toggleModal, reload, data }) => {
  let source = axios.CancelToken.source();

  // const [isLoading, setLoading] = useState(false);
  const [isLoading] = useState(false);

  useEffect(() => {
    return () => {
      source.cancel("request canceled");
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <ModalChangeLogs onCancel={toggleModal} isLoading={isLoading} data={data} />
  );
};

export default Handler;
