import * as React from "react";
import { Switch } from "react-router-dom";

import PrivateRoute from "utils/PrivateRoute";
import UnprivateRoot from "utils/UnprivateRoot";

import { Login } from "./Auth";
import Root from "./Root";

class App extends React.Component<{}, {}> {
  render() {
    return (
      <React.Fragment>
        <Switch>
          <UnprivateRoot path="/login" component={Login} />
          <PrivateRoute path="/" component={Root} />
        </Switch>
      </React.Fragment>
    );
  }
}

export default App;
