import * as React from "react";
import { c } from "constant";
import { inject, observer } from "mobx-react";
import { ICommonStore } from "stores/commonStore";
import { IMsihaStore } from "stores/msihaStore";

import CardHeader from "components/CardHeader";
import { Typography } from "components";

interface KondisiOdhaProps {}

interface InjectedProps extends KondisiOdhaProps {
  commonStore: ICommonStore;
  msihaStore: IMsihaStore;
}

interface IKondisiOdhaState {}

@inject(c.STORES.COMMON)
@inject(c.STORES.MSIHA)
@observer
class KondisiOdha extends React.Component<KondisiOdhaProps, IKondisiOdhaState> {
  // constructor(props) {
  //   super(props);
  // }

  get injected() {
    return this.props as InjectedProps;
  }

  componentWillMount() {
    this.injected.commonStore.setBreadcrumbs(c.BREADCRUMBS.KONDISI_ODHA);
    this.injected.msihaStore.getAfterArt();
  }

  render() {
    const fakePopulateData = this.injected.msihaStore.generateFakeData();

    const { listAfterArt, inProgress } = this.injected.msihaStore;
    const classCard =
      "col-12 mx-auto bg-white p-3 m-0 card-border-radius height-full";

    const styleWidth_1 = { minWidth: "4rem", paddingBottom: "0.5rem" };
    // const styleWidth_2 = { minWidth: "6rem", paddingBottom: "0.5rem" };
    const classBg_0 = "bg-gray-soft text-center p-2 pt-3";
    const classBg_1 = "bg-secondary-soft text-center p-2 pt-3";
    const orderList = ["0", "1", "2", "3", "4", "5", "6"];
    const listData: any = [];
    if (listAfterArt.length > 0) {
      listAfterArt.map((item, index) => {
        return listData[orderList[index]] = item;
      });
    }
    const type = [
      "odhaDeath",
      "odhaStopArt",
      "odhaNotVisit",
      "failFollowUp3Month",
      // "cumulativeOdhaArt_3",
      "odhaRejimen1Original",
      "odhaRejimen1",
      "odhaRejimen2"
    ];

    return (
      <React.Fragment>
        <div
          className="pb-4 pr-5 pl-5 pt-4 bg-light mx-auto"
          style={{
            maxHeight: "calc(100vh - 75px)",
            minHeight: "calc(100vh - 75px)"
          }}
        >
          <Typography
            color="black"
            fontSize="18px"
            fontWeight="bold"
            className="d-block text-center my-4"
          >
            {c.BREADCRUMBS.KONDISI_ODHA}
          </Typography>
          <div
            className={classCard}
            style={{ minHeight: "calc(100vh - 12rem)" }}
          >
            <div
              className="overflow-auto card-border-radius"
              style={{ minHeight: "calc(100vh - 10rem)" }}
            >
              {inProgress ? (
                <div className="text-center">
                  <div className="spinner-border text-info" role="status">
                    <span className="sr-only">Loading...</span>
                  </div>
                </div>
              ) : (
                <table className="width-full">
                  <CardHeader
                    isRealData={false}
                    useGroupByPopulation
                  ></CardHeader>
                  <tbody>
                    {listAfterArt.length === 0 ? (
                      <tr>
                        <td
                          className="bg-gray-soft text-left border-right-gray p-3 text-danger font-weight-bolder"
                          colSpan={2}
                        >
                          Data Invalid
                        </td>
                      </tr>
                    ) : (
                      listData.map((item, index) => {
                        return (
                          <tr key={item.age}>
                            <td
                              className="bg-gray-soft text-left border-right-gray p-3"
                              style={{ minWidth: "30rem", maxWidth: "30rem" }}
                            >
                              {c.VARIABLE_TYPE[type[index]]}
                            </td>
                            {item[type[index]]["LAKI-LAKI"] &&
                              item[type[index]]["LAKI-LAKI"].map(
                                (item, index) => {
                                  if (index === 10) {
                                    return (
                                      <td
                                        className={
                                          index % 2 === 0
                                            ? classBg_0
                                            : classBg_1
                                        }
                                        style={styleWidth_1}
                                        key={type[index] + "male" + index}
                                      >
                                        {item.jumlah}
                                      </td>
                                    );
                                  }
                                  return (
                                    <td
                                      className={
                                        index % 2 === 0 ? classBg_0 : classBg_1
                                      }
                                      style={styleWidth_1}
                                      key={type[index] + "male" + index}
                                    >
                                      {item.count}
                                    </td>
                                  );
                                }
                              )}
                            {item[type[index]]["PEREMPUAN"] &&
                              item[type[index]]["PEREMPUAN"].map(
                                (item, index) => {
                                  if (index === 10) {
                                    return (
                                      <td
                                        className={
                                          index % 2 === 0
                                            ? classBg_1
                                            : classBg_0
                                        }
                                        style={styleWidth_1}
                                        key={type[index] + "famale" + index}
                                      >
                                        {item.jumlah}
                                      </td>
                                    );
                                  }
                                  return (
                                    <td
                                      className={
                                        index % 2 === 0 ? classBg_1 : classBg_0
                                      }
                                      style={styleWidth_1}
                                      key={type[index] + "famale" + index}
                                    >
                                      {item.count}
                                    </td>
                                  );
                                }
                              )}
                            {fakePopulateData.map((value, idx) => {
                              return (
                                <td
                                  className={
                                    idx % 2 === 0 ? classBg_0 : classBg_1
                                  }
                                >
                                  {value}
                                </td>
                              );
                            })}
                          </tr>
                        );
                      })
                    )}
                  </tbody>
                </table>
              )}
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default KondisiOdha;
