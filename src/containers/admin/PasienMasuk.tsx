import * as React from "react";
import { c } from "constant";
import { inject, observer } from "mobx-react";
import { ICommonStore } from "stores/commonStore";
import { IMsihaStore } from "stores/msihaStore";

import CardHeader from "components/CardHeader";
// import iconBottom from "images/icons/icon-botton.png";
// import { toJS } from "mobx";
import { Typography } from "components";

interface PasienMasukProps {}

interface InjectedProps extends PasienMasukProps {
  commonStore: ICommonStore;
  msihaStore: IMsihaStore;
}

interface IPasienMasukState {
  dropdownOpen: boolean;
  typeSelected: string;
}

@inject(c.STORES.COMMON)
@inject(c.STORES.MSIHA)
@observer
class PasienMasuk extends React.Component<PasienMasukProps, IPasienMasukState> {
  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
    this.onClickType = this.onClickType.bind(this);
    this.state = {
      dropdownOpen: false,
      typeSelected: c.PATIENT_TYPE.HIV
    };
  }

  get injected() {
    return this.props as InjectedProps;
  }

  componentWillMount() {
    this.injected.commonStore.setBreadcrumbs(c.BREADCRUMBS.PASIEN_MASUK);
    this.injected.msihaStore.getEntriHiv();
  }

  toggle() {
    this.setState(prevState => ({
      dropdownOpen: !prevState.dropdownOpen
    }));
  }

  onClickType(type) {
    this.setState({
      typeSelected: type
    });
    if (type === c.PATIENT_TYPE.HIV) {
      this.injected.msihaStore.getEntriHiv();
      return;
    }
    this.injected.msihaStore.getEntriArt();
  }

  render() {
    const fakePopulateData = this.injected.msihaStore.generateFakeData();

    const { listEntriArt, listEntriHiv, inProgress } = this.injected.msihaStore;
    const classCard =
      "col-12 mx-auto bg-white p-3 m-0 card-border-radius height-full";

    const styleWidth_1 = { minWidth: "4rem", paddingBottom: "0.5rem" };
    // const styleWidth_2 = { minWidth: "6rem", paddingBottom: "0.5rem" };
    const classBg_0 = "bg-gray-soft text-center p-2 pt-3";
    const classBg_1 = "bg-secondary-soft text-center p-2 pt-3";
    const type_hiv = [
      "newOdhaHIV",
      "odhaReferIn",
      "odhaReferOut",
      "cumulativeOdhaHIV",
      "odhaNonArtNotVisit",
      "cumulativeOdhaNonArtDeath"
    ];
    const type_arv = [
      "cumulativeOdhaArt",
      "newOdhaArt",
      "odhaReferInArt",
      "odhaReferOutArt"
    ];

    return (
      <React.Fragment>
        <div
          className="pb-4 pr-5 pl-5 pt-4 bg-light mx-auto"
          style={{
            maxHeight: "calc(100vh - 75px)",
            minHeight: "calc(100vh - 75px)"
          }}
        >
          {/* <div className="col-12 p-0 pb-3">
            <Dropdown isOpen={this.state.dropdownOpen} toggle={this.toggle}>
              <DropdownToggle>
                {c.PATIENT_TYPE_LABEL[this.state.typeSelected]}
                <img
                  className="cursor-pointer ml-3"
                  src={iconBottom}
                  style={{ width: "16px" }}
                />
              </DropdownToggle>
              <DropdownMenu>
                <DropdownItem
                  onClick={() => this.onClickType(c.PATIENT_TYPE.HIV)}
                >
                  {c.PATIENT_TYPE_LABEL.HIV}
                </DropdownItem>
                <DropdownItem
                  onClick={() => this.onClickType(c.PATIENT_TYPE.ART)}
                >
                  {c.PATIENT_TYPE_LABEL.ART}
                </DropdownItem>
              </DropdownMenu>
            </Dropdown>
          </div> */}
          <Typography
            color="black"
            fontSize="18px"
            fontWeight="bold"
            className="d-block text-center my-4"
          >
            {c.BREADCRUMBS.PASIEN_MASUK}
          </Typography>
          <div
            className={classCard}
            style={{ minHeight: "calc(100vh - 12rem)" }}
          >
            <div
              className="overflow-auto card-border-radius"
              style={{ minHeight: "calc(100vh - 13rem)" }}
            >
              {inProgress ? (
                <div className="text-center">
                  <div className="spinner-border text-info" role="status">
                    <span className="sr-only">Loading...</span>
                  </div>
                </div>
              ) : (
                <table className="width-full">
                  <CardHeader
                    isRealData={false}
                    useGroupByPopulation
                  ></CardHeader>
                  <tbody>
                    {listEntriHiv.length === 0 ? (
                      <tr>
                        <td
                          className="bg-gray-soft text-left border-right-gray p-3 text-danger font-weight-bolder"
                          colSpan={2}
                        >
                          Data Invalid
                        </td>
                      </tr>
                    ) : this.state.typeSelected === c.PATIENT_TYPE.HIV ? (
                      listEntriHiv.map((item, index) => {
                        if (index !== 6) {
                          // type_hiv[index]
                          return (
                            <tr key={"hiv_" + item.age}>
                              <td
                                className="bg-gray-soft text-left border-right-gray p-3"
                                style={{ minWidth: "30rem", maxWidth: "30rem" }}
                              >
                                {c.VARIABLE_TYPE[type_hiv[index]]}
                              </td>
                              {item[type_hiv[index]]["LAKI-LAKI"] &&
                                item[type_hiv[index]]["LAKI-LAKI"].map(
                                  (item, index) => {
                                    if (index === 10) {
                                      return (
                                        <td
                                          className={
                                            index % 2 === 0
                                              ? classBg_0
                                              : classBg_1
                                          }
                                          style={styleWidth_1}
                                          key={type_hiv[index] + "male" + index}
                                        >
                                          {item.jumlah}
                                        </td>
                                      );
                                    }
                                    return (
                                      <td
                                        className={
                                          index % 2 === 0
                                            ? classBg_0
                                            : classBg_1
                                        }
                                        style={styleWidth_1}
                                        key={type_hiv[index] + "male" + index}
                                      >
                                        {item.count}
                                      </td>
                                    );
                                  }
                                )}
                              {item[type_hiv[index]]["PEREMPUAN"] &&
                                item[type_hiv[index]]["PEREMPUAN"].map(
                                  (item, index) => {
                                    if (index === 10) {
                                      return (
                                        <td
                                          className={
                                            index % 2 === 0
                                              ? classBg_1
                                              : classBg_0
                                          }
                                          style={styleWidth_1}
                                          key={
                                            type_hiv[index] + "famale" + index
                                          }
                                        >
                                          {item.jumlah}
                                        </td>
                                      );
                                    }
                                    return (
                                      <td
                                        className={
                                          index % 2 === 0
                                            ? classBg_1
                                            : classBg_0
                                        }
                                        style={styleWidth_1}
                                        key={type_hiv[index] + "famale" + index}
                                      >
                                        {item.count}
                                      </td>
                                    );
                                  }
                                )}
                              {fakePopulateData.map((value, idx) => {
                                return (
                                  <td
                                    className={
                                      idx % 2 === 0 ? classBg_0 : classBg_1
                                    }
                                  >
                                    {value}
                                  </td>
                                );
                              })}
                            </tr>
                          );
                        }
                        return (
                          <React.Fragment></React.Fragment>
                        )
                      })
                    ) : (
                      listEntriArt.map((item, index) => {
                        if (index !== 6) {
                          return (
                            <tr key={"ARV_" + item.age}>
                              <td
                                className="bg-gray-soft text-left border-right-gray p-3"
                                style={{ minWidth: "30rem", maxWidth: "30rem" }}
                              >
                                {c.VARIABLE_TYPE[type_arv[index]]}
                              </td>
                              {item[type_arv[index]]["LAKI-LAKI"] &&
                                item[type_arv[index]]["LAKI-LAKI"].map(
                                  (item, index) => {
                                    if (index === 10) {
                                      return (
                                        <td
                                          className={
                                            index % 2 === 0
                                              ? classBg_0
                                              : classBg_1
                                          }
                                          style={styleWidth_1}
                                          key={type_arv[index] + "male" + index}
                                        >
                                          {item.jumlah}
                                        </td>
                                      );
                                    }
                                    return (
                                      <td
                                        className={
                                          index % 2 === 0
                                            ? classBg_0
                                            : classBg_1
                                        }
                                        style={styleWidth_1}
                                        key={type_arv[index] + "male" + index}
                                      >
                                        {item.count}
                                      </td>
                                    );
                                  }
                                )}
                              {item[type_arv[index]]["PEREMPUAN"] &&
                                item[type_arv[index]]["PEREMPUAN"].map(
                                  (item, index) => {
                                    if (index === 10) {
                                      return (
                                        <td
                                          className={
                                            index % 2 === 0
                                              ? classBg_1
                                              : classBg_0
                                          }
                                          style={styleWidth_1}
                                          key={
                                            type_arv[index] + "famale" + index
                                          }
                                        >
                                          {item.jumlah}
                                        </td>
                                      );
                                    }
                                    return (
                                      <td
                                        className={
                                          index % 2 === 0
                                            ? classBg_1
                                            : classBg_0
                                        }
                                        style={styleWidth_1}
                                        key={type_arv[index] + "famale" + index}
                                      >
                                        {item.count}
                                      </td>
                                    );
                                  }
                                )}
                              {fakePopulateData.map((value, idx) => {
                                return (
                                  <td
                                    className={
                                      idx % 2 === 0 ? classBg_0 : classBg_1
                                    }
                                  >
                                    {value}
                                  </td>
                                );
                              })}
                            </tr>
                          );
                        }
                        return (<React.Fragment></React.Fragment>)
                      })
                    )}
                  </tbody>
                </table>
              )}
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default PasienMasuk;
