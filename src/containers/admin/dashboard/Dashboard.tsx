import * as React from "react";
import { c } from "constant";
import { inject, observer } from "mobx-react";
import { ICommonStore } from "stores/commonStore";
import { IUserStore } from "stores/userStore";
import { RouteComponentProps } from "react-router";

import StatisticSection from "./StatisticSection";
import UserSection from "./UserSection";
import VisitorSection from "./VisitorSection";

interface DashboardProps extends RouteComponentProps<any> {}

interface InjectedProps extends DashboardProps {
  commonStore: ICommonStore;
  userStore: IUserStore;
}

interface IDashboardState {}

@inject(c.STORES.COMMON)
@inject(c.STORES.USER)
@observer
class Dashboard extends React.Component<DashboardProps, IDashboardState> {
  // constructor(props) {
  //   super(props);
  // }

  get injected() {
    return this.props as InjectedProps;
  }

  componentWillMount() {
    this.injected.commonStore.setBreadcrumbs(c.BREADCRUMBS.DASHBOARD);
  }

  render() {
    const classCard = "col-12 mx-auto p-0 m-0 card-border-radius ";
    const { user } = this.injected.userStore;
    const { avatar, name, role } = user;
    return (
      <React.Fragment>
        <div
          className="pb-5 p-4 bg-light mx-auto"
          style={{
            maxHeight: "calc(100vh - 75px)",
            minHeight: "calc(100vh - 75px)"
          }}
        >
          <div className=" col-12 mx-auto mb-4">
            <UserSection
              fullName={name}
              role={role}
              avatar={avatar}
              classStyle={classCard}
            />
          </div>
          <div className="row col-12 m-0 p-0">
            <div className=" col-7 mx-auto">
              <StatisticSection classStyle={classCard + " bg-white"} />
            </div>
            <div className="col-5 mx-auto">
              <VisitorSection
                classStyle={classCard + " height-full bg-white"}
              />
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default Dashboard;
