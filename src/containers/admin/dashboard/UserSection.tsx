import * as React from "react";

interface UserSectionProps {
  fullName: string;
  role: string;
  avatar: string;
  classStyle: string;
}

class UserSection extends React.Component<UserSectionProps, {}> {
  render() {
    const { fullName, role, avatar, classStyle } = this.props;
    const classSection =
      classStyle + " pl-4 pr-5 pt-4 pb-4 bg-gradient-card text-white row";
    return (
      <React.Fragment>
        <div className={classSection}>
          <div className="col-6">
            <h5 className="font-weight-bolder">
              Selamat datang kembali,
              <br />
              {fullName} ({role})
            </h5>
            <br />
          </div>
          <div className="col-6 text-right">
            <img
              className="cursor-pointer mr-3 img-avatar"
              src={avatar}
              style={{ width: "96px" }}
              alt="avatar"
            />
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default UserSection;
