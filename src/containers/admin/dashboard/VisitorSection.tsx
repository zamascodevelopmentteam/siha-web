import * as React from "react";
import { c } from "constant";
import { inject, observer } from "mobx-react";
import { ICommonStore } from "stores/commonStore";
import { Label } from "reactstrap";

import Moment from "moment";

interface VisitorSectionProps {
  classStyle: string;
}

interface InjectedProps extends VisitorSectionProps {
  commonStore: ICommonStore;
}

@inject(c.STORES.COMMON)
@observer
class VisitorSection extends React.Component<VisitorSectionProps, {}> {
  constructor(props) {
    super(props);
    Moment.locale("id");
  }

  get injected() {
    return this.props as InjectedProps;
  }

  componentWillMount() {
    this.injected.commonStore.getAllVisit();
  }

  render() {
    const { visits, inProgress } = this.injected.commonStore;
    const { classStyle } = this.props;
    const classSection = classStyle;
    // const visit2 = [
    //   {
    //     id: 2,
    //     ordinal: 1,
    //     User: {
    //       name: "Leornardo Jardin",
    //       avatar: ""
    //     }
    //   },
    //   {
    //     id: 1,
    //     ordinal: 1,
    //     User: {
    //       name: "Diana Destian",
    //       avatar: ""
    //     }
    //   },
    //   {
    //     id: 1,
    //     ordinal: 1,
    //     User: {
    //       name: "Brandon Pena",
    //       avatar: ""
    //     }
    //   },
    //   {
    //     id: 1,
    //     ordinal: 1,
    //     User: {
    //       name: "Diana Destian",
    //       avatar: ""
    //     }
    //   },
    //   {
    //     id: 1,
    //     ordinal: 1,
    //     User: {
    //       name: "Brandon Pena",
    //       avatar: ""
    //     }
    //   },
    //   {
    //     id: 1,
    //     ordinal: 1,
    //     User: {
    //       name: "Diana Destian",
    //       avatar: ""
    //     }
    //   },
    //   {
    //     id: 1,
    //     ordinal: 1,
    //     User: {
    //       name: "Diana Destian",
    //       avatar: ""
    //     }
    //   },
    //   {
    //     id: 1,
    //     ordinal: 1,
    //     User: {
    //       name: "Brandon Pena",
    //       avatar: ""
    //     }
    //   }
    // ];
    return (
      <React.Fragment>
        <div className={classSection}>
          <div
            className="
            width-full border-bottom"
          >
            <Label className="text-nunito font-weight-bold pl-4 pt-3 pr-4">
              Pengunjung Hari ini
            </Label>
          </div>
          <div
            className="overflow-auto"
            style={{ minHeight: "35rem", maxHeight: "35rem" }}
          >
            {inProgress ? (
              <div
                className="d-flex justify-content-center"
                style={{ minHeight: "10rem" }}
              >
                <div className="spinner-border text-primary" role="status" />
                <span> Loading...</span>
              </div>
            ) : visits.length > 0 ? (
              visits.map((item, index) => {
                return (
                  <div
                    className="row m-0 p-3 width-full border-bottom"
                    key={index}
                  >
                    <div className="col-7 m-0 p-0 align-middle">
                      <Label
                        className="font-weight-bold m-0"
                        style={{ fontSize: "16px" }}
                      >
                        ID Pasien: {item.patient.id}
                      </Label>
                      <br />
                      <Label className="m-0" style={{ fontSize: "14px" }}>
                        Kunjungan ke {item.ordinal}
                      </Label>
                    </div>
                    <div className="col-5 text-right">
                      <span className="d-md-inline-block">
                        {Moment(item.visitDate).format("HH:MM D MMM, YYYY")}
                      </span>
                    </div>
                  </div>
                );
              })
            ) : (
              <div className="p-3 text-center">
                <h3 className="pt-3 pb-5">Tidak ada pengunjung hari ini</h3>
              </div>
            )}
          </div>
          <div
            className="pt-4 mx-auto"
            style={
              visits.length > 1
                ? {
                    maxHeight: "2rem",
                    boxShadow: "0 -4px 5px -4px #c0c2c3"
                  }
                : { maxHeight: "2rem" }
            }
          />
        </div>
      </React.Fragment>
    );
  }
}

export default VisitorSection;
