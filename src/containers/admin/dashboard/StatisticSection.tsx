import * as React from "react";
import { c } from "constant";
import { inject, observer } from "mobx-react";
import { ICommonStore } from "stores/commonStore";
import { QuickStat } from "components";

interface StatisticSectionProps {
  classStyle: string;
}

interface InjectedProps extends StatisticSectionProps {
  commonStore: ICommonStore;
}

@inject(c.STORES.COMMON)
@observer
class StatisticSection extends React.Component<StatisticSectionProps, {}> {
  // constructor(props) {
  //   super(props);
  // }

  get injected() {
    return this.props as InjectedProps;
  }

  componentWillMount() {
    this.injected.commonStore.getCountVisit();
  }

  render() {
    const { visitCount } = this.injected.commonStore;
    const { classStyle } = this.props;
    const classSection = classStyle + " p-3";
    return (
      <React.Fragment>
        <div className={classSection}>
          <div className="row pl-3 pr-3">
            <div className="col-6 p-1">
              <QuickStat
                title="Jumlah ODHA"
                subTitle="masuk perawatan HIV hingga hari ini"
                number={visitCount || 0}
                description="pasien"
                colorClass="bg-gradien-blue"
              />
            </div>
            <div className="col-6 p-1 pt-0">
              <QuickStat
                title="Jumlah ODHA"
                subTitle="memulai ART bulan ini"
                number="-"
                description="pasien bulan ini"
                colorClass="bg-gradien-blue"
              />
            </div>
          </div>
          <div className="row pr-3 pl-3 pt-2">
            <div className="col-4 p-1">
              <QuickStat
                title="Jumlah ODHA"
                subTitle="dirujuk masuk selama perawatan bulan ini"
                number="-"
                description="pasien"
                colorClass="bg-card-green"
              />
            </div>
            <div className="col-4 p-1">
              <QuickStat
                title="Jumlah ODHA"
                subTitle="dirujuk keluar selama perawatan bulan ini"
                number="-"
                description="pasien"
                colorClass="bg-gradien-blue"
              />
            </div>
            <div className="col-4 p-1">
              <QuickStat
                title="Jumlah Kumulatif ODHA"
                subTitle="pernah masuk perawatan HIV akhir bulan ini"
                number="-"
                description="pasien"
                colorClass="bg-card-green"
              />
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default StatisticSection;
