import * as React from "react";

import ArvKhusus from "./ArvKhusus";
import HivIms from "./HivIms";
import KondisiOdha from "./KondisiOdha";
import NotifikasiPasangan from "./NotifikasiPasangan";
import PasienMasuk from "./PasienMasuk";
import Ppk from "./Ppk";
import TbHiv from "./TbHiv";
import Tpt from "./Tpt";
import ViralLoad from "./ViralLoad";

class App extends React.Component<{}, {}> {
  render() {
    return (
      <React.Fragment>
        <PasienMasuk />
        <KondisiOdha />
        <TbHiv />
        <Ppk />
        <Tpt />
        <ViralLoad />
        <ArvKhusus />
        <NotifikasiPasangan />
        <HivIms />
      </React.Fragment>
    );
  }
}

export default App;
