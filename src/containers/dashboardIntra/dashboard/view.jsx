import React, { useContext } from "react";
import { WelcomeMessage, QuickStatV2, Typography } from "components";
import GlobalContext from "containers/GlobalContext";

const View = ({
  visitEstimation,
  visitThisDay,
  visitThisMonth,
  examThisDay
}) => {
  const user = useContext(GlobalContext);

  const monthNames = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Augustus", "September", "Oktober", "November", "Desember"];
  let dateObj = new Date();
  let month = monthNames[dateObj.getMonth()];
  // let day = String(dateObj.getDate()).padStart(2, '0');
  let year = dateObj.getFullYear();
  let output = month + ' ' + year;

  return (
    <div className="container p-4">
      <div className="row mb-4">
        <div className="col-12 ">
          <WelcomeMessage name={user.name} avatar={user.avatar} />
        </div>
      </div>

      <div className="row">
        <div className="col-12">
          <div className="d-flex justify-content-between mb-3">
            <Typography fontSize="16px" fontWeight="bold" color="black">
              Kunjungan Bulan {output}
            </Typography>
          </div>
          <div className="row">
            <div className="col-3">
              <QuickStatV2
                count={visitEstimation}
                title="Perkiraan Jumlah Kunjungan ODHA Bulan ini"
                description={`${visitEstimation} orang yang akan mengunjungi bulan ini`}
                inverted
              />
            </div>
            <div className="col-3">
              <QuickStatV2
                count={visitThisMonth}
                title="Jumlah Kunjungan Bulan ini"
                description={`${visitThisMonth} orang yang mengunjungi bulan ini`}
                inverted
              />
            </div>
            <div className="col-3">
              <QuickStatV2
                count={examThisDay}
                title="Jumlah Pasien Melakukan Test HIV Bulan ini"
                description={`${examThisDay} orang yang melakukan test hiv bulan ini`}
                inverted
              />
            </div>
            <div className="col-3">
              <QuickStatV2
                count={visitThisDay}
                title="Jumlah Pasien Melakukan Kunjungan ini"
                description={`${visitThisDay} orang yang melakukan kunjungan hari ini`}
                inverted
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default View;
