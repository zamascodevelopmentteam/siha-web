import React, { useEffect, useState } from "react";
import API from "utils/API";
import axios from "axios";

import View from "./view";

const emptyData = {
  visitEstimation: "-",
  visitThisDay: "-",
  visitThisMonth: "-",
  examThisDay: "-"
};
const Handler = () => {
  let source = axios.CancelToken.source();
  const [data, setData] = useState(emptyData);
  const [isLoading, setIsLoading] = useState(false);

  const _loadData = () => {
    setIsLoading(true);
    API.Dashboard.intra(source.token)
      .then(rsp => {
        setData(rsp.data);
      })
      .catch(e => {
        console.error("e: ", e);
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  useEffect(() => {
    _loadData();

    return () => {
      source.cancel("request canceled");
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <View
      visitEstimation={data.visitEstimation}
      visitThisDay={data.visitThisDay}
      visitThisMonth={data.visitThisMonth}
      examThisDay={data.examThisDay}
      isLoading={isLoading}
    />
  );
};

export default Handler;
