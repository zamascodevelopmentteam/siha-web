import React, { useEffect, useState, useContext } from "react";
import GlobalContext from "containers/GlobalContext";
import API from "utils/API";
import axios from "axios";
import { ENUM } from "constant";

import List from "./list";

const Handler = () => {
  let source = axios.CancelToken.source();
  const [data, setData] = useState([]);
  const [keyword, setKeyword] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const user = useContext(GlobalContext);

  const _handleSearch = value => {
    setKeyword(value);
  };

  const _loadData = () => {
    setIsLoading(true);
    API.Visit.listToday(source.token, keyword)
      .then(rsp => {
        setData(rsp.data);
      })
      .catch(e => {
        console.error("e: ", e);
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  useEffect(() => {
    _loadData();

    return () => {
      source.cancel("request canceled");
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [keyword]);

  if (user.logisticrole === ENUM.LOGISTIC_ROLE.UPK_ENTITY) {
    return null;
  }

  return <List data={data} isLoading={isLoading} onSearch={_handleSearch} />;
};

export default Handler;
