import React from "react";
import { Input, Empty } from "antd";
import { debounce } from "lodash";
import { Typography } from "components";
import { VisitorListWrapper } from "../style";
import Item from "./item";

const { Search } = Input;

const VisitorList = ({ data, onSearch }) => {
  const _onSearch = debounce(value => {
    onSearch(value);
  }, 600);
  const todayVisitors = data.filter(visitor => !visitor.checkOutDate)
  return (
    <div className="bg-white h-100">
      <div className="p-3 border-bottom">
        <Search
          placeholder="Cari pasien"
          className="mb-3"
          onChange={e => _onSearch(e.target.value)}
          onSearch={value => {
            onSearch(value);
            _onSearch.cancel();
          }}
        />
        <Typography fontSize="20px" fontWeight="bold" color="black">
          Pengunjung Hari Ini
        </Typography>
      </div>
      <VisitorListWrapper>
        {todayVisitors.length === 0 && (
          <Empty description="Belum ada pengunjung hari ini" />
        )}
        {todayVisitors.map(value => (
          <Item
            key={value.id}
            id={value.patient.id}
            name={value.patient.name}
            ordinal={value.ordinal}
            date={value.visitDate}
            statusPatient={value.patient.statusPatient}
            jenisTest={
              (value.testHiv ? "HIV" : "") +
              (value.testIms ? ", IMS" : "") +
              (value.testVL ? ", VL/CD4" : "")
            }
            visitStatus={
              value.checkOutDate === null
                ? "Dalam Kunjungan"
                : "Kunjungan Selesai"
            }
            userStatus={value.isComplete}
          />
        ))}
      </VisitorListWrapper>
    </div>
  );
};

export default VisitorList;
