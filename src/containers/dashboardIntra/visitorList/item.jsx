import React, { useContext } from "react";
import { Tag } from "antd";
import { DisplayDate, Typography } from "components";
import { Link } from "react-router-dom";
import GlobalContext from "containers/GlobalContext";
import { ENUM } from "constant";

const Visitor = ({
  id,
  name,
  ordinal,
  date,
  visitStatus,
  userStatus,
  statusPatient,
  jenisTest
}) => {
  const user = useContext(GlobalContext);
  return (
    <React.Fragment>
      <div className="d-flex justify-content-between align-items-center pl-3 pr-2 pt-2">
        <div>
          <Typography
            fontSize="14px"
            fontWeight="600"
            color="black"
            className="d-block"
          >
            {name}
          </Typography>
          <Typography
            fontSize="10px"
            fontWeight="600"
            color="secondary"
            className="d-block"
          >
            Kunjungan ke - {ordinal}
          </Typography>
          <Typography
            fontSize="10px"
            fontWeight="600"
            color="secondary"
            className="d-block"
          >
            <DisplayDate date={date} />
          </Typography>
          <Typography
            fontSize="10px"
            fontWeight="600"
            color="secondary"
            className="d-block"
          >
            Status: {visitStatus}
          </Typography>
        </div>
        <div className="d-flex flex-column align-items-end">
          <Tag color={userStatus ? "#06d2ae" : "#ff4b4b"} className="mb-2 mr-0">
            {userStatus ? "Data Lengkap" : "Data Belum lengkap"}
          </Tag>
          <Link
            to={`/data-pasien/${user.role}/${id}`}
            className={`ant-btn hs-btn ${user.role} ant-btn-primary ant-btn-sm`}
          >
            Lihat Detail
          </Link>
        </div>
      </div>
      <div className="d-flex justify-content-between align-items-center border-bottom pl-3 pr-2 py-2">
        <Typography
          fontSize="10px"
          fontWeight="600"
          color="secondary"
          className="d-block"
        >
          Pasien {ENUM.LABEL_STATUS_PATIENT[statusPatient]} {'->'} Test: {jenisTest}
        </Typography>
      </div>
    </React.Fragment>
  );
};

export default Visitor;
