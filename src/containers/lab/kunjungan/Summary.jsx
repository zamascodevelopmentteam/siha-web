import React from "react";
import { Typography } from "components";
import { DatePicker } from "antd";

const { MonthPicker } = DatePicker;

export const Summary = ({ onChangeFilter, totalVisit, totalOdha }) => {
  return (
    <React.Fragment>
      <div className="col-4">
        <Typography
          fontSize="18px"
          fontWeight="bold"
          color="primary"
          className="d-block mb-3"
        >
          Kunjungan
        </Typography>
        <MonthPicker onChange={onChangeFilter} placeholder="Pilih Bulan" />
      </div>
      <div className="col-4">
        <Typography
          fontSize="18px"
          fontWeight="bold"
          color="primary"
          className="d-block mb-3"
        >
          Data Kunjungan
        </Typography>
        <div className="d-flex">
          <div className="mr-4">
            <Typography fontSize="12px" fontWeight="bold" className="d-block">
              Total Kunjungan
            </Typography>
            <Typography fontSize="12px">{totalVisit} Orang</Typography>
          </div>
          <div>
            <Typography fontSize="12px" fontWeight="bold" className="d-block">
              Pasien ODHA
            </Typography>
            <Typography fontSize="12px">{totalOdha} Orang</Typography>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};
