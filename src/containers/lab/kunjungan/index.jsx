import React from "react";
import shortid from "shortid";

import { DataTable, Typography } from "components";
import { Link } from "react-router-dom";
import { Summary } from "./Summary";
import { ENUM } from "constant";

const index = () => {
  const _onFilter = value => {
    console.log("value: ", value);
  };

  return (
    <div className="container h-100 pt-4">
      <div className="row mb-3">
        <Summary onChangeFilter={_onFilter} totalVisit={100} totalOdha={34} />
      </div>
      <div className="row">
        <div className="col-12 bg-white rounded p-4 border">
          <DataTable
            title="Kunjungan pada bulan Desember 2019"
            columns={columns}
            data={data}
          />
        </div>
      </div>
    </div>
  );
};

export default index;

const columns = [
  {
    title: "No",
    dataIndex: "number",
    key: "number"
  },
  {
    title: "Nama",
    dataIndex: "name",
    key: "name"
  },
  {
    title: "Status HIV Pasien",
    dataIndex: "patientStatus",
    key: "patientStatus",
    render: patientStatus => ENUM.LABEL_STATUS_PATIENT[patientStatus]
  },
  {
    title: "Terakhir Berkunjung",
    dataIndex: "lastVisit",
    key: "lastVisit"
  },
  {
    title: "Status Kunjungan",
    dataIndex: "visitStatus",
    key: "visitStatus",
    render: visitStatus => {
      if (visitStatus.toLowerCase() === "dalam kunjungan") {
        return (
          <Typography fontWeight="bold" fontSize="12px" color="red">
            {visitStatus}
          </Typography>
        );
      }
      return (
        <Typography fontWeight="bold" fontSize="12px" color="green">
          {visitStatus}
        </Typography>
      );
    }
  },
  {
    title: "Aksi",
    key: "action",
    render: text => {
      return (
        <Link
          to={`/lab/data-pasien/${text.key}`}
          className="ant-btn hs-btn LAB_STAFF ant-btn-primary ant-btn-sm"
        >
          Detail
        </Link>
      );
    }
  }
];

const data = [
  {
    key: shortid.generate(),
    number: 1,
    name: "John Brown",
    patientStatus: "ODHA",
    lastVisit: "2 Desember 2019",
    visitStatus: "Dalam Kunjungan"
  },
  {
    key: shortid.generate(),
    number: 1,
    name: "John Brown",
    patientStatus: "ODHA",
    lastVisit: "2 Desember 2019",
    visitStatus: "Kunjungan Selesai"
  },
  {
    key: shortid.generate(),
    number: 1,
    name: "John Brown",
    patientStatus: "ODHA",
    lastVisit: "2 Desember 2019",
    visitStatus: "Dalam Kunjungan"
  },
  {
    key: shortid.generate(),
    number: 1,
    name: "John Brown",
    patientStatus: "ODHA",
    lastVisit: "2 Desember 2019",
    visitStatus: "Dalam Kunjungan"
  },
  {
    key: shortid.generate(),
    number: 1,
    name: "John Brown",
    patientStatus: "ODHA",
    lastVisit: "2 Desember 2019",
    visitStatus: "Dalam Kunjungan"
  },
  {
    key: shortid.generate(),
    number: 1,
    name: "John Brown",
    patientStatus: "ODHA",
    lastVisit: "2 Desember 2019",
    visitStatus: "Dalam Kunjungan"
  },
  {
    key: shortid.generate(),
    number: 1,
    name: "John Brown",
    patientStatus: "ODHA",
    lastVisit: "2 Desember 2019",
    visitStatus: "Dalam Kunjungan"
  },
  {
    key: shortid.generate(),
    number: 1,
    name: "John Brown",
    patientStatus: "ODHA",
    lastVisit: "2 Desember 2019",
    visitStatus: "Dalam Kunjungan"
  }
];
