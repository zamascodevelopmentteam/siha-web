export { default as LabDashboard } from "./dashboard";
export { default as LabKunjungan } from "./kunjungan";
export { default as Pasien } from "./pasien";
export { default as NonPasien } from "./nonPasien";
export { default as PenggunaanVlcd4 } from "./penggunaan_vlcd4";
