import React, { useEffect, useState } from "react";
import API from "utils/API";
import axios from "axios";

import { DataPasien } from "./DataPasien";

const paginationInitalState = {
  currentPage: 1,
  total: 0
};

const Handler = ({ role }) => {
  let source = axios.CancelToken.source();
  const [patients, setPatients] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [keyword, setKeyword] = useState("");
  const [pagination, setPagination] = useState(paginationInitalState);

  const _handleChangeTable = page => {
    setPagination({
      ...pagination,
      currentPage: page.current
    });
  };

  const _handleSearch = value => {
    setPagination(paginationInitalState);
    setKeyword(value);
  };

  const _reload = () => {
    setPagination(paginationInitalState);
    _listPatient();
  };

  const _listPatient = () => {
    setIsLoading(true);
    API.Patient.list(source.token, keyword, pagination.currentPage - 1)
      .then(data => {
        setPatients(data.data || []);
        setPagination({
          ...pagination,
          total: data.paging.total
        });
      })
      .catch(e => {
        console.error("e: ", e);
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  useEffect(() => {
    _listPatient();

    return () => {
      source.cancel("request canceled");
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [pagination.currentPage, keyword]);

  return (
    <DataPasien
      patients={patients}
      pagination={pagination}
      onChangeTable={_handleChangeTable}
      onSearch={_handleSearch}
      isLoading={isLoading}
      reload={_reload}
      role={role}
    />
  );
};

export default Handler;
