import React, { useState } from "react";

import { Button } from "antd";
import { DataKunjungan } from "./DataKunjungan";
import { RiwayatHIV } from "./RiwayatHIV";
import { RiwayatIMS } from "./RiwayatIMS";
import { Summary } from "./Summary";
import { useHistory } from "react-router";
import { ModalHIV, ModalIMS, ModalVLCD4 } from "components";

export const ProfilePasien = () => {
  const [isHIVOpen, setHIVOpen] = useState(false);
  const [isIMSOpen, setIMSOpen] = useState(false);
  const [isVLCDOpen, setVLCCDOpen] = useState(false);

  const { goBack } = useHistory();

  const _goBack = () => {
    goBack();
  };

  const _toggleModalHIV = () => {
    setHIVOpen(!isHIVOpen);
  };

  const _toggleModalIMS = () => {
    setIMSOpen(!isIMSOpen);
  };

  const _toggleModalVLCD = () => {
    setVLCCDOpen(!isVLCDOpen);
  };

  return (
    <React.Fragment>
      {isVLCDOpen && (
        <ModalVLCD4
          onCancel={_toggleModalVLCD}
          onOk={_toggleModalVLCD}
          isLoading={false}
          role="LAB_STAFF"
        />
      )}

      {isHIVOpen && (
        <ModalHIV
          onCancel={_toggleModalHIV}
          onOk={_toggleModalHIV}
          isLoading={false}
          role="LAB_STAFF"
        />
      )}

      {isIMSOpen && (
        <ModalIMS
          onCancel={_toggleModalIMS}
          onOk={_toggleModalIMS}
          isLoading={false}
          role="LAB_STAFF"
        />
      )}

      <div className="container h-100 pt-4">
        <div className="row mb-3">
          <div className="col-12">
            <Button
              className="hs-btn-outline LAB_STAFF"
              icon="left"
              onClick={_goBack}
            >
              Kembali
            </Button>
          </div>
        </div>
        <div className="row mb-3">
          <div className="col-12 ">
            <Summary
              nik="333276813689001"
              name="Ignatius Roland"
              status="ODHA"
            />
          </div>
        </div>
        <div className="row mb-3">
          <div className="col-6">
            <RiwayatHIV />
          </div>
          <div className="col-6">
            <RiwayatIMS />
          </div>
        </div>
        <div className="row mb-4">
          <div className="col-12">
            <DataKunjungan
              toggleModalVLCD={_toggleModalVLCD}
              toggleModalHIV={_toggleModalHIV}
              toggleModalIMS={_toggleModalIMS}
            />
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};
