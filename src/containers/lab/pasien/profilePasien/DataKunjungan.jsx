import React from "react";
import shortid from "shortid";

import { Button } from "antd";
import { DataTable, Typography } from "components";

export const DataKunjungan = ({
  toggleModalHIV,
  toggleModalIMS,
  toggleModalVLCD
}) => {
  const columns = [
    {
      title: "No",
      dataIndex: "index",
      key: "index"
    },
    {
      title: "Tanggal Kunjungan",
      dataIndex: "visitDate",
      key: "visitDate"
    },
    {
      title: "Pemeriksaan HIV",
      dataIndex: "hivExam",
      key: "hivExam",
      render: text => {
        return (
          <Button
            size="small"
            onClick={toggleModalHIV}
            className="hs-btn-outline LAB_STAFF"
          >
            Tambah
          </Button>
        );
      }
    },
    {
      title: "Pemeriksaan IMS",
      dataIndex: "imsExam",
      key: "imsExam",
      render: text => {
        return (
          <Button
            type="primary"
            size="small"
            className="hs-btn LAB_STAFF"
            onClick={toggleModalIMS}
          >
            Tambah
          </Button>
        );
      }
    },
    {
      title: "Pemeriksaan VL/CD4",
      dataIndex: "vlcd4Exam",
      key: "vlcd4Exam",
      render: text => {
        return (
          <Button
            size="small"
            onClick={toggleModalVLCD}
            className="hs-btn-outline LAB_STAFF"
          >
            Tambah
          </Button>
        );
      }
    },
    {
      title: "Status Kunjungan",
      dataIndex: "visitStatus",
      key: "visitStatus",
      render: visitStatus => {
        if (visitStatus.toLowerCase() === "dalam kunjungan") {
          return (
            <Typography fontWeight="bold" fontSize="12px" color="red">
              {visitStatus}
            </Typography>
          );
        }
        return (
          <Typography fontWeight="bold" fontSize="12px" color="green">
            {visitStatus}
          </Typography>
        );
      }
    }
  ];

  const data = [
    {
      key: shortid.generate(),
      index: 1,
      visitDate: "2 Desember 2019",
      visitStatus: "Dalam Kunjungan"
    },
    {
      key: shortid.generate(),
      index: 1,
      visitDate: "2 Desember 2019",
      visitStatus: "Kunjungan Selesai"
    },
    {
      key: shortid.generate(),
      index: 1,
      visitDate: "2 Desember 2019",
      visitStatus: "Dalam Kunjungan"
    },
    {
      key: shortid.generate(),
      index: 1,
      visitDate: "2 Desember 2019",
      visitStatus: "Dalam Kunjungan"
    },
    {
      key: shortid.generate(),
      index: 1,
      visitDate: "2 Desember 2019",
      visitStatus: "Dalam Kunjungan"
    },
    {
      key: shortid.generate(),
      index: 1,
      visitDate: "2 Desember 2019",
      visitStatus: "Dalam Kunjungan"
    }
  ];
  return (
    <div className="bg-white rounded p-4 border">
      <Typography
        fontSize="14px"
        color="primary"
        fontWeight="bold"
        className="d-block mb-2"
      >
        Data Kunjungan
      </Typography>
      <DataTable columns={columns} data={data} />
    </div>
  );
};
