import styled from "styled-components";

export const DashboardWrapper = styled.div`
  width: calc(100% - 300px);
`;

export const VisitorWrapper = styled.div`
  width: 300px;
  height: 100%;
`;

export const StatWrapper = styled.div`
  border-radius: 12px;
  border: solid 1px #e0e0e0;
  background-color: #ffffff;
`;

export const VisitorListWrapper = styled.div`
  height: calc(100% - 110px);
  overflow: auto;
`;
