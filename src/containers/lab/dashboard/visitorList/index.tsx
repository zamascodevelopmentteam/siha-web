import * as React from "react";
import shortid from "shortid";
import { Input } from "antd";
import { Typography } from "components";
import { VisitorListWrapper } from "../style";
import { Visitor } from "./Visitor";
const { Search } = Input;

export const VisitorList = () => {
  return (
    <div className="bg-white h-100">
      <div className="p-3 border-bottom">
        <Search
          placeholder="Cari pasien, masukan NIK"
          className="mb-3"
          onSearch={value => console.log(value)}
        />
        <Typography fontSize="20px" fontWeight="bold" color="black">
          Pengunjung Hari Ini
        </Typography>
      </div>
      <VisitorListWrapper>
        {dummies.map(value => (
          <Visitor
            key={value.id}
            name={value.name}
            ordinal={value.ordinal}
            date={value.date}
            visitStatus={value.visitStatus}
            userStatus={value.userStatus}
          />
        ))}
      </VisitorListWrapper>
    </div>
  );
};

const dummies = [
  {
    id: shortid.generate(),
    name: "John Doe",
    ordinal: "1",
    date: "01-01-2020",
    visitStatus: "Dalam Kunjungan",
    userStatus: "Data Belum Lengkap"
  },
  {
    id: shortid.generate(),
    name: "John Doe",
    ordinal: "1",
    date: "01-01-2020",
    visitStatus: "Dalam Kunjungan",
    userStatus: "Data Belum Lengkap"
  },
  {
    id: shortid.generate(),
    name: "John Doe",
    ordinal: "1",
    date: "01-01-2020",
    visitStatus: "Dalam Kunjungan",
    userStatus: "Data Belum Lengkap"
  },
  {
    id: shortid.generate(),
    name: "John Doe",
    ordinal: "1",
    date: "01-01-2020",
    visitStatus: "Dalam Kunjungan",
    userStatus: "Data Belum Lengkap"
  }
];
