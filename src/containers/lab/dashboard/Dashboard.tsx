import * as React from "react";
import { Link } from "react-router-dom";
import { WelcomeMessage, QuickStatV2, Typography } from "components";
import { StatWrapper } from "./style";

export const Dashboard = () => {
  return (
    <div className="container p-4">
      <div className="row mb-4">
        <div className="col-12 ">
          {/* <WelcomeMessage name="Yudhasena" avatar="https://picsum.photos/200" /> */}
          <WelcomeMessage name="Yudhasena" />
        </div>
      </div>
      <div className="row mb-4">
        <div className="col-4">
          <QuickStatV2
            count={200}
            title="HIV Bulan Ini"
            description="200 pasien sudah melakukan pemerikassan HIV bulan ini "
          />
        </div>
        <div className="col-4">
          <QuickStatV2
            count={200}
            title="IMS Bulan Ini"
            description="200 pasien sudah melakukan pemerikassan IMS bulan ini "
          />
        </div>
        <div className="col-4">
          <QuickStatV2
            count={200}
            title="VL/CD4 Bulan Ini"
            description="200 pasien sudah melakukan pemerikassan VL/CD4 bulan ini "
          />
        </div>
      </div>
      <div className="row">
        <div className="col-12">
          <StatWrapper className="p-3">
            <div className="d-flex justify-content-between mb-3">
              <Typography fontSize="16px" fontWeight="bold" color="black">
                Kunjungan di bulan-bulan sebelumnya
              </Typography>
              <Link to="">Lihat Semua</Link>
            </div>
            <div className="row">
              <div className="col-4">
                <QuickStatV2
                  count={400}
                  title="Total Kunjungan"
                  description="400 orang yang mengunjungi Poli RS pada bulan November 2019"
                  inverted
                />
              </div>
              <div className="col-4">
                <QuickStatV2
                  count={400}
                  title="Total Kunjungan"
                  description="400 orang yang mengunjungi Poli RS pada bulan Oktober 2019"
                  inverted
                />
              </div>
              <div className="col-4">
                <QuickStatV2
                  count={400}
                  title="Total Kunjungan"
                  description="400 orang yang mengunjungi Poli RS pada bulan September 2019"
                  inverted
                />
              </div>
            </div>
          </StatWrapper>
        </div>
      </div>
    </div>
  );
};
