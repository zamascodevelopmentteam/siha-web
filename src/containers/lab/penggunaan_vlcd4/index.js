import React, { useEffect, useState } from "react";
import API from "utils/API";
import axios from "axios";
import { validateForm } from "utils/validator";
import { DisplayDate } from "components";
import { Button } from "antd";
import View from "./view";

const Handler = () => {
    const formEmptyState = {
        testType: null,
        reagentType: null,
        date: null,
        examination: null,
        medicines: [
            {
                medicineId: null,
                amount: 0
            }
        ]
    };

    let source = axios.CancelToken.source();

    const [data, setData] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
    const [isDetail, setIsDetail] = useState(false);
    const [detailData, setDetailData] = useState({});
    const [action, setAction] = useState('add'); // add / edit
    const [isFormOpen, setIsFormOpen] = useState(false);
    const [formData, setFormData] = useState(formEmptyState);

    const _getData = async () => {
        setIsLoading(true);
        await API.Lab.getUsageVlCd4Data(source.token)
            .then(res => {
                setData(res.data || []);
            })
            .catch(e => {
                console.error("e: ", e);
            })
            .finally(() => {
                setIsLoading(false);
            });
    };

    const _openForm = async (action, data) => {
        if (action === "add") {
            await setFormData(formEmptyState);
        } else {
            let editData = {
                id: data.id,
                testType: data.testType,
                reagentType: data.reagentType,
                date: data.date,
                medicines: []
            };
            data.usageVlCd4Medicines.map(r => {
                editData.medicines.push({
                    medicineId: r.medicineId,
                    amount: r.amount
                });
            })
            await setFormData(editData);
        }

        setAction(action);
        setIsFormOpen(true);
    }

    const _closeForm = () => {
        setIsFormOpen(false);
    }

    const _onChange = (name, value, idx = null) => {
        let newFormData = { ...formData };

        if (idx === null) {
            newFormData[name] = value;
        } else {
            newFormData.medicines[idx][name] = value;
        }

        // console.log(newFormData);
        setFormData(newFormData);
    }

    const _onAddMedicine = () => {
        let newFormData = { ...formData };
        newFormData.medicines.push(
            {
                medicineId: null,
                amount: 0
            }
        );

        setFormData(newFormData);
    }

    const _onDeleteMedicine = (idx) => {
        let newFormData = { ...formData };
        newFormData.medicines.splice(idx, 1);

        setFormData(newFormData);
    }

    const _onSubmit = async e => {
        e.preventDefault();

        let fields = {
            testType: "Tipe Tes",
            date: "Tanggal",
            reagentType: "Kategori VL / CD4",
            examination: "Jumlah Hasil Pemeriksaan"
        };
        let validated = validateForm(formData, fields);
        if (!validated) {
            return
        }

        validated = [];
        formData.medicines.forEach((r, i) => {
            fields = {
                medicineId: "Reagen " + (i + 1),
                amount: "Jumlah Reagen " + (i + 1),
            }
            validated.push(validateForm(r, fields));
        })
        
        if (validated.includes(false)) {
            return
        }

        setIsLoading(true);

        if (action === "add") {
            await API.Lab.saveUsageVlCd4Data(source.token, formData)
                .then(() => {
                    // setIsLoading(false);
                    _getData();
                    _closeForm();
                    // openNotification("success", "Berhasil", "Test VL/CD4 berhasil diinput");
                    // toggleModal();
                    // reload(true);
                })
                .catch(e => {
                    // openNotification("error", "Gagal", e.message || String(e.data ? e.data.message : "").replace(/_/g, " "));
                    console.error("e: ", e);
                    setIsLoading(false);
                });
        } else {
            await API.Lab.updateUsageVlCd4Data(source.token, formData, formData.id)
                .then(() => {
                    // setIsLoading(false);
                    _getData();
                    _closeForm();
                    // openNotification("success", "Berhasil", "Test VL/CD4 berhasil diinput");
                    // toggleModal();
                    // reload(true);
                })
                .catch(e => {
                    // openNotification("error", "Gagal", e.message || String(e.data ? e.data.message : "").replace(/_/g, " "));
                    console.error("e: ", e);
                    setIsLoading(false);
                });
        }
    }
    
    const _deleteData = async (id) => {
        setIsLoading(true);
        await API.Lab.deleteUsageVlCd4Data(source.token, id)
                .then(() => {
                    // setIsLoading(false);
                    _getData();
                    // openNotification("success", "Berhasil", "Test VL/CD4 berhasil diinput");
                    // toggleModal();
                    // reload(true);
                })
                .catch(e => {
                    // openNotification("error", "Gagal", e.message || String(e.data ? e.data.message : "").replace(/_/g, " "));
                    console.error("e: ", e);
                    setIsLoading(false);
                });
    }

    const _onShowDetail = async (data) => {
        setDetailData(data);
        setIsDetail(true);
    }

    const _onHideDetail = async () => {
        setIsDetail(false);
    }

    useEffect(() => {
        _getData();

        return () => {
            source.cancel("request canceled");
        };

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const columns = [
        {
            title: "Tipe Tes",
            render: r => r.testType
        },
        {
            title: "Tipe Reagen",
            render: r => r.reagentType
        },
        {
            title: "Tanggal",
            render: r => <DisplayDate date={r.date} />
        },
        {
            title: "Aksi",
            render: (r) => {
                return (
                    <React.Fragment>
                        {/* <Button
                            onClick={() => _openForm('edit', r)}
                            className={`hs-btn-outline LAB_STAFF`}
                            size="small"
                        >
                            Edit
                        </Button> */}
                        {/* <Button
                            onClick={() => _deleteData(r.id)}
                            className={`hs-btn-outline LAB_STAFF ml-3`}
                            size="small"
                        >
                            Hapus
                        </Button> */}
                        <Button
                            onClick={() => _onShowDetail(r)}
                            className={`hs-btn-outline LAB_STAFF ml-3`}
                            size="small"
                        >
                            Detail
                        </Button>
                    </React.Fragment>
                )
            }
        },
    ];

    return (
        <View
            data={data}
            detailData={detailData}
            columns={columns}
            isLoading={isLoading}
            isDetail={isDetail}
            onHideDetail={_onHideDetail}
            action={action}
            isFormOpen={isFormOpen}
            openForm={_openForm}
            closeForm={_closeForm}
            formData={formData}
            onChange={_onChange}
            onAddMedicine={_onAddMedicine}
            onDeleteMedicine={_onDeleteMedicine}
            onSubmit={_onSubmit}
        />
    );
}

export default Handler;