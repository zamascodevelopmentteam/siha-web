import React from "react";
import { Button, Input, Modal, Select, DatePicker } from "antd";
import { DataTable, Typography, ChooseReagen } from "components";
import moment from "moment";
// import { useParams } from "react-router";
import { Label, Table } from "reactstrap";
import { ENUM } from "constant";

const { VL_TEST_TYPE } = ENUM;
const { Option, OptGroup } = Select;

const View = ({
    data,
    detailData,
    columns,
    isLoading,
    isDetail,
    onHideDetail,
    action,
    isFormOpen,
    openForm,
    closeForm,
    formData,
    onChange,
    onAddMedicine,
    onDeleteMedicine,
    onSubmit
}) => {
    return (
        <React.Fragment>
            <div className="container h-100 pt-4">
                <div className="row mb-3">
                    <div className="col-4">
                        <Typography
                            fontSize="18px"
                            fontWeight="bold"
                            // color={role}
                            className="d-block mb-3"
                        >
                            Penggunaan Vl/CD4
                        </Typography>
                    </div>
                </div>
                <div className="row">
                    <div className="col-12 bg-white rounded p-4 border">
                        <DataTable
                            title="Penggunaan Vl/CD4"
                            columns={columns}
                            //   onSearch={onSearch}
                            data={data}
                            isLoading={isLoading}
                            button={
                                <Button
                                    onClick={() => openForm('add')}
                                    className={`hs-btn-outline LAB_STAFF`}
                                    icon="plus"
                                >
                                    Tambah Penggunaan Vl/CD4
                                </Button>
                            }
                        />
                    </div>
                </div>
            </div>
            {isFormOpen && (
                <Modal
                    title={`${action == "add" ? 'Tambah' : 'Ubah'} Penggunaan Vl/CD4`}
                    visible
                    style={{ top: 30 }}
                    // onOk={onOk}
                    maskClosable={false}
                    onCancel={closeForm}
                    footer={null}
                >
                    <form onSubmit={onSubmit}>
                        <Label>Tipe Tes <span className="text-danger">*Harus Diisi</span></Label>
                        <Select
                            placeholder="Pilih Tipe Tes"
                            style={{ width: "100%" }}
                            className="mb-3"
                            value={formData.testType || null}
                            name="testType"
                            onChange={value => onChange("testType", value)}
                            required
                        >
                            <Option disabled value={null}>
                                Pilih Tipe Tes
                            </Option>
                            {Object.keys(VL_TEST_TYPE).map(key => (
                                <Option key={VL_TEST_TYPE[key]} value={VL_TEST_TYPE[key]}>
                                    {VL_TEST_TYPE[key]}
                                </Option>
                            ))}
                        </Select>

                        <Label>
                            Tanggal <span className="text-danger">*Harus Diisi</span>
                        </Label>
                        <DatePicker
                            placeholder="Masukkan Tanggal"
                            style={{ width: "100%" }}
                            allowClear={false}
                            className="mb-3"
                            name="date"
                            value={formData.date ? moment(formData.date) : null}
                            onChange={date => onChange("date", date ? date.toISOString() : null)}
                            required
                        />

                        <Label>Kategori VL / CD4 <span className="text-danger">*Harus Diisi</span></Label>
                        <Select
                            placeholder="Pilih Kategori"
                            style={{ width: "100%" }}
                            className="mb-3"
                            name="reagentType"
                            value={formData.reagentType}
                            onChange={value => onChange("reagentType", value)}
                        >
                            <Option value={null} disabled>
                                Pilih Kategori
                            </Option>
                            {
                                Object.keys(ENUM.VLCD4CATEGORY_OPTIONS).map(key => {
                                    if (key === formData.testType) {
                                        return (
                                            // <OptGroup label={key}>
                                            // {
                                            Object.keys(ENUM.VLCD4CATEGORY_OPTIONS[key]).map(kk => (
                                                <Option key={ENUM.VLCD4CATEGORY[kk]} value={ENUM.VLCD4CATEGORY[kk]}>
                                                    {ENUM.VLCD4CATEGORY[kk]}
                                                </Option>
                                            ))
                                            // }
                                            // </OptGroup>
                                        )
                                    }
                                })
                            }
                        </Select>

                        <Label>Jumlah Hasil Pemeriksaan <span className="text-danger">*Harus Diisi</span></Label>
                        <Input
                            type="number"
                            placeholder="Masukan Jumlah Hasil Pemeriksaan"
                            style={{ width: "100%" }}
                            className="mb-3"
                            name="examination"
                            value={formData.examination}
                            onChange={e => onChange("examination", e.target.value)}
                            required
                        />

                        <Label style={{ fontSize: 17, fontWeight: "bold" }}>Reagen</Label>
                        <Button
                            onClick={() => onAddMedicine()}
                            className={`hs-btn-outline LAB_STAFF ml-3`}
                            icon="plus"
                            size="small"
                        >
                            Tambah
                        </Button>
                        <br />
                        {formData.medicines.map((r, idx) => (
                            <React.Fragment>
                                <Label>
                                    Reagen <span className="text-danger">*Harus Diisi</span>
                                    {idx != 0 && (
                                        <Button
                                            onClick={() => onDeleteMedicine(idx)}
                                            className={`ml-3`}
                                            type="danger"
                                            size="small"
                                        >
                                            Hapus
                                        </Button>
                                    )}
                                </Label>
                                <ChooseReagen
                                    placeholder="Pilih Reagen"
                                    style={{ width: "100%" }}
                                    className="mb-3"
                                    value={r.medicineId || null}
                                    name="medicineId"
                                    onChange={value => onChange("medicineId", value, idx)}
                                    required
                                    isNotNol={true}
                                    vlcd4={true}
                                    vlcd4Type={formData.testType}
                                    vlcd4ReagentType={formData.reagentType}
                                />

                                <Label>Jumlah <span className="text-danger">*Harus Diisi</span></Label>
                                <Input
                                    type="number"
                                    placeholder="Masukan Jumlah"
                                    style={{ width: "100%" }}
                                    className="mb-3"
                                    name="amount"
                                    value={r.amount}
                                    onChange={e => onChange("amount", e.target.value, idx)}
                                    required
                                />
                                <hr />
                            </React.Fragment>
                        ))}
                        <Button
                            // onClick={() => onAddMedicine()}
                            className={`hs-btn-outline LAB_STAFF`}
                            htmlType="submit"
                            loading={isLoading}
                        >
                            Simpan
                        </Button>
                        <Button key="back" type="danger" onClick={closeForm} className="ml-2">
                            Batal
                        </Button>
                    </form>
                </Modal>
            )}
            {isDetail && (
                <Modal
                    title={'Detail Penggunaan Vl/CD4'}
                    visible
                    style={{ top: 30 }}
                    // onOk={onOk}
                    maskClosable={false}
                    onCancel={onHideDetail}
                    footer={null}
                >
                    <Table bordered responsive>
                        <thead>
                            <tr>
                                <td>Reagen</td>
                                <td>Jumlah</td>
                            </tr>
                        </thead>
                        <tbody>
                            {detailData.usageVlCd4Medicines && detailData.usageVlCd4Medicines.map(r => (
                                <tr>
                                    <td>{r.medicine.name}</td>
                                    <td>{r.amount}</td>
                                </tr>
                            ))}
                        </tbody>
                    </Table>
                </Modal>
            )}
        </React.Fragment>
    );
};

export default View;