import React, { useState } from "react";

import { DataTable, DisplayDate } from "components";
import { ENUM } from "constant";
import { Button, Typography, Select } from "antd";
import FormNonPasien from "containers/lab/nonPasien/formNonPasien";
import FormKertasDBS from "containers/lab/nonPasien/formKertasDBS";
const { Option } = Select;

export const ListNonPasien = ({
  data,
  isLoading,
  reload,
  onChangeStatus,
  status
}) => {
  const [isFormOpen, setFormOpen] = useState(false);
  const [isFormDBSOpen, setFormDBSOpen] = useState(false);

  const _toggleModal = () => {
    setFormOpen(!isFormOpen);
  };

  const _toggleModalDBS = () => {
    setFormDBSOpen(!isFormDBSOpen);
  };

  const columns = [
    {
      title: "Tujuan Penggunaan",
      dataIndex: "testType",
      key: "testType",
      render: testType => ENUM.REAGEN_TYPE_ALL[testType]
    },
    {
      title: "Tanggal Penggunaan",
      dataIndex: "dateUsage",
      key: "dateUsage",
      render: date => <DisplayDate date={date} />
    },
    {
      title: "Alat",
      key: "toolName",
      render: data => data?.toolData?.name || "-"
    },
    {
      title: "Reagen",
      key: "medicineName",
      render: data => data?.medicineData?.name || "-"
    },
    {
      title: "Kontrol",
      dataIndex: "usageControl",
      key: "usageControl"
    },
    {
      title: "Kalibrasi",
      dataIndex: "usageError",
      key: "usageError"
    },
    {
      title: "Terbuang",
      dataIndex: "usageWasted",
      key: "usageWasted"
    },
    {
      title: "Gangguan Teknis",
      dataIndex: "usageOthers",
      key: "usageOthers"
    },
    {
      title: "Sebab Lainnya",
      dataIndex: "usageStock",
      key: "usageStock"
    }
  ];

  return (
    <React.Fragment>
      {isFormOpen && (
        <FormNonPasien
          toggleModal={_toggleModal}
          reload={reload}
          isAdd={true}
        />
      )}
      {isFormDBSOpen && (
        <FormKertasDBS
          toggleModal={_toggleModalDBS}
          reload={reload}
          isAdd={true}
        />
      )}

      <div className="container h-100 pt-4">
        <div className="row mb-3">
          <div className="col-3 pl-0">
            <Typography
              fontSize="18px"
              fontWeight="bold"
              color="PHARMA_STAFF"
              className="d-block mb-3"
            >
              Jenis Penggunaan
            </Typography>
            <Select
              style={{ width: "220px" }}
              onChange={onChangeStatus}
              value={status}
            >
              <Option key="ALL" value="ALL">
                Semua penggunaan
              </Option>
              {Object.keys(ENUM.REAGEN_TYPE_ALL).map(key => (
                <Option key={key} value={key}>
                  {ENUM.REAGEN_TYPE_ALL[key]}
                </Option>
              ))}
            </Select>
          </div>
        </div>
        <div className="row">
          <div className="col-12 bg-white rounded p-4 border">
            <DataTable
              title={`Penggunaan Lab Nonpasien`}
              columns={columns}
              data={data}
              isLoading={isLoading}
              isScrollX
              button={
                <React.Fragment>
                  <Button
                    className="hs-btn-outline LAB_STAFF"
                    onClick={_toggleModalDBS}
                  >
                    Tambah Penggunaan Kertas DBS
                  </Button>
                  <Button
                    className="hs-btn-outline LAB_STAFF ml-2"
                    onClick={_toggleModal}
                  >
                    Tambah Penggunaan Nonpasien
                  </Button>
                </React.Fragment>
              }
            />
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};
