import React, { useEffect, useState } from "react";
import API from "utils/API";
import axios from "axios";
import { useParams } from "react-router";
import { openNotification } from "utils/Notification";

import { ModalKertasDBS } from "components";

const labUsageEmptyState = {
  isDbs: true,
  testType: "",
  toolId: "",
  dateUsage: "",
  usageStock: ""
};

const Handler = ({ toggleModal, isAdd = false, reload }) => {
  let source = axios.CancelToken.source();
  const { role } = useParams();

  const [data, setData] = useState(labUsageEmptyState);
  const [isLoading, setLoading] = useState(false);

  const _handleChange = (name, value) => {
    setData({
      ...data,
      [name]: value
    });
  };

  const _onSubmit = () => {
    setLoading(true);
    API.Exam.createLabUsageNonpatient(source.token, data)
      .then(() => {
        setLoading(false);
        openNotification(
          "success",
          "Berhasil",
          "Penggunaan Kertas DBS berhasil diinput"
        );
        toggleModal();
        reload(true);
      })
      .catch(e => {
        openNotification("error", "Gagal", e.message || String(e.data ? e.data.message : "").replace(/_/g, " "));
        console.error("e: ", e);
        setLoading(false);
      });
  };

  useEffect(() => {
    return () => {
      source.cancel("request canceled");
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isAdd]);

  return (
    <ModalKertasDBS
      isAdd={isAdd}
      onCancel={toggleModal}
      onOk={_onSubmit}
      isLoading={isLoading}
      onChange={_handleChange}
      role={role}
      data={data}
    />
  );
};

export default Handler;
