import React, { useEffect, useState } from "react";
import API from "utils/API";
import axios from "axios";

import { useParams } from "react-router";
import { ListNonPasien } from "./ListNonPasien";

const Handler = () => {
  let source = axios.CancelToken.source();

  const [data, setData] = useState([]);
  const [isLoading, setLoading] = useState(false);
  const { type } = useParams();
  const [status, setStatus] = useState("ALL");

  const _handleChangeStatus = value => {
    setStatus(value);
  };

  const _loadData = () => {
    setLoading(true);
    API.Exam.listLabUsageNonpatient(source.token, status)
      .then(rsp => {
        setData(rsp.data || []);
      })
      .catch(e => {
        console.error("e: ", e);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  useEffect(() => {
    _loadData();

    return () => {
      source.cancel("request canceled");
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [type, status]);

  return (
    <ListNonPasien
      data={data}
      isLoading={isLoading}
      reload={_loadData}
      onChangeStatus={_handleChangeStatus}
      status={status}
    />
  );
};

export default Handler;
