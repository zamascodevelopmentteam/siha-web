import * as React from "react";
import { DashboardWrapper, VisitorWrapper } from "./style";
import { Dashboard } from "./Dashboard";
import { VisitorList } from "./visitorList";

const index = () => {
  return (
    <React.Fragment>
      <div className="w-100 h-100 d-flex">
        <DashboardWrapper>
          <Dashboard />
        </DashboardWrapper>
        <VisitorWrapper>
          <VisitorList />
        </VisitorWrapper>
      </div>
    </React.Fragment>
  );
};

export default index;
