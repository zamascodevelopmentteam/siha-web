import React, { useEffect, useState, useContext } from "react";
import API from "utils/API";
import axios from "axios";
import { openNotification } from "utils/Notification";
import GlobalContext from "containers/GlobalContext";

import { ModalKunjungan, deleteConfirm } from "components";

const Handler = ({ toggleModal, reload }) => {
  let source = axios.CancelToken.source();
  const userContext = useContext(GlobalContext);
  const { role } = userContext;

  const [data, setData] = useState(null);
  const [isLoading, setLoading] = useState(false);
  const [isAdded, setAdded] = useState(false);
  const [nik, setNik] = useState("");

  const _onSearch = () => {
    setLoading(true);
    API.Patient.getByNik(source.token, nik)
      .then(rsp => {
        if (!rsp.data) {
          openNotification("error", "Gagal", "NIK tidak ditemukan");
          setData(null);
        } else {
          setData(rsp.data || null);
        }
        setLoading(false);
      })
      .catch(e => {
        openNotification("error", "Gagal", e.message || String(e.data ? e.data.message : "").replace(/_/g, " "));
        console.error("e: ", e);
        setLoading(false);
      });
  };

  const _handleAdd = e => {
    deleteConfirm({
      onDelete: () => _createVisit(),
      message: `Anda yakin akan menambah kunjungan ${data.name}?`,
      typeConfirm: "warning"
    });
  };

  const _createVisit = () => {
    setLoading(true);
    API.Visit.createNewVisit(source.token, data.id)
      .then(() => {
        openNotification(
          "success",
          "Berhasil",
          "Pasien berhasil menambah kunjungan"
        );
        reload();
        setAdded(true);
      })
      .catch(e => {
        openNotification("error", "Gagal", e.message || String(e.data ? e.data.message : "").replace(/_/g, " "));
        console.error("e: ", e);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  useEffect(() => {
    return () => {
      source.cancel("request canceled");
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <ModalKunjungan
      onCancel={toggleModal}
      onSearch={_onSearch}
      onOk={_handleAdd}
      isLoading={isLoading}
      onChange={setNik}
      role={role}
      data={data}
      nik={nik}
      isAdded={isAdded}
    />
  );
};

export default Handler;
