import React from "react";
import KunjunganHandler from "./handler";

const index = () => {
  return (
    <React.Fragment>
      <KunjunganHandler />
    </React.Fragment>
  );
};

export default index;
