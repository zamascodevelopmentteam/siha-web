import React, { useContext, useState } from "react";

import { DataTable, DisplayDate, Typography } from "components";
import { Link } from "react-router-dom";
import { Summary } from "./Summary";
import GlobalContext from "containers/GlobalContext";
import { DatePicker, Select, Button } from "antd";
import { ENUM, ROLE } from "constant";
import FormKunjungan from "containers/rr/kunjungan/formKunjungan";

const { RangePicker } = DatePicker;
const { Option } = Select;

const FILTER_TYPE = {
  visitDate: "visitDate",
  ischeckOut: "ischeckOut",
  isOnTransit: "isOnTransit",
  isComplete: "isComplete"
};

export const Kunjungan = ({
  reload,
  visits,
  pagination,
  onChangeTable,
  isLoading,
  onFilter,
  onSearch,
  currentFilter
}) => {
  const userContext = useContext(GlobalContext);
  const [isVisitFormOpen, setVisitFormOpen] = useState(false);

  const _toggleModalAddVisit = () => {
    setVisitFormOpen(!isVisitFormOpen);
  };
  const columns = [
    {
      title: "Nama",
      dataIndex: "patient",
      key: "name",
      render: patient => patient.name
    },
    {
      title: "Kunjungan ke",
      dataIndex: "ordinal",
      key: "ordinal"
    },
    {
      title: "Tanggal",
      dataIndex: "visitDate",
      key: "visitDate",
      render: visitDate => <DisplayDate date={visitDate} />
    },
    {
      title: "Status HIV Pasien",
      dataIndex: "patient",
      key: "statusPatient",
      render: patient => ENUM.LABEL_STATUS_PATIENT[patient.statusPatient]
    },
    {
      title: "Status Kelengkapan",
      dataIndex: "isComplete",
      key: "isComplete",
      render: isComplete => {
        if (!isComplete) {
          return (
            <Typography fontWeight="bold" fontSize="12px" color="red">
              Data Belum Lengkap
            </Typography>
          );
        }
        return (
          <Typography fontWeight="bold" fontSize="12px" color="green">
            Data Lengkap
          </Typography>
        );
      }
    },
    {
      title: "Status Kunjungan",
      dataIndex: "checkOutDate",
      key: "checkOutDate",
      render: checkOutDate => {
        if (checkOutDate === null) {
          return (
            <Typography fontWeight="bold" fontSize="12px" color="red">
              Dalam Kunjungan
            </Typography>
          );
        }
        return (
          <Typography fontWeight="bold" fontSize="12px" color="green">
            Kunjungan Selesai
          </Typography>
        );
      }
    },
    {
      title: "Status Transit",
      key: "isOnTransit",
      render: data => {
        if (data.treatment && data.treatment.isOnTransit) {
          return (
            <Typography fontWeight="bold" fontSize="12px" color="red">
              Pasien Transit
            </Typography>
          );
        }
        return (
          <Typography fontWeight="bold" fontSize="12px" color="green">
            Pasien Reguler
          </Typography>
        );
      }
    },
    {
      title: "UPK Sebelumnya",
      key: "upkSebelumnya",
      render: data => {
        if (data.treatment && data.treatment.isOnTransit) {
          return (
            <Typography fontSize="12px">
              {data.treatment.upkSebelumnyaData ? data.treatment.upkSebelumnyaData.name : "-"}
            </Typography>
          );
        }
        return "-";
      }
    },
    {
      title: "Aksi",
      key: "action",
      fixed: "right",
      render: item => {
        return (
          <Link
            to={`/data-pasien/${userContext.role}/${item.patientId}`}
            className="ant-btn hs-btn RR_STAFF ant-btn-primary ant-btn-sm"
          >
            Detail
          </Link>
        );
      }
    }
  ];

  // TODO: Filter perbulan & total visit total odha

  return (
    <React.Fragment>
      <div className="container h-100 pt-4">
        <div className="row mb-3 pl-0">
          <Summary />
        </div>
        <div className="row mb-3 pl-0">
          <div className="col-4 pl-0">
            <Typography
              fontSize="16px"
              fontWeight="bold"
              className="d-block mb-3"
            >
              Periode
            </Typography>
            <RangePicker
              onChange={(date, dtString) =>
                onFilter(FILTER_TYPE.visitDate, dtString)
              }
              placeholder="Pilih Periode"
            />
          </div>
          <div className="col-2">
            <Typography
              fontSize="16px"
              fontWeight="bold"
              className="d-block mb-3"
            >
              Status Kunjungan
            </Typography>
            <Select
              style={{ width: "100%" }}
              onChange={value => onFilter(FILTER_TYPE.ischeckOut, value)}
              value={currentFilter.ischeckOut}
            >
              <Option key="ischeckOut_null" value={"all"}>
                <Typography fontWeight="bold" fontSize="12px">
                  Semua Data
                </Typography>
              </Option>
              <Option key="ischeckOut_true" value={"true"}>
                <Typography fontWeight="bold" fontSize="12px" color="green">
                  Kunjungan Selesai
                </Typography>
              </Option>
              <Option key="ischeckOut_false" value={"false"}>
                <Typography fontWeight="bold" fontSize="12px" color="red">
                  Dalam Kunjungan
                </Typography>
              </Option>
            </Select>
          </div>
          <div className="col-2">
            <Typography
              fontSize="16px"
              fontWeight="bold"
              className="d-block mb-3"
            >
              Status Transit
            </Typography>
            <Select
              style={{ width: "100%" }}
              onChange={value => onFilter(FILTER_TYPE.isOnTransit, value)}
              value={currentFilter.isOnTransit}
            >
              <Option key="isOnTransit_null" value={"all"}>
                <Typography fontWeight="bold" fontSize="12px">
                  Semua Data
                </Typography>
              </Option>
              <Option key="isOnTransit_true" value={"false"}>
                <Typography fontWeight="bold" fontSize="12px" color="green">
                  Pasien Regular
                </Typography>
              </Option>
              <Option key="isOnTransit_false" value={"true"}>
                <Typography fontWeight="bold" fontSize="12px" color="red">
                  Pasien Transit
                </Typography>
              </Option>
            </Select>
          </div>
        </div>
        <div className="row">
          <div className="col-12 bg-white rounded p-4 border">
            <DataTable
              title="Kunjungan"
              columns={columns}
              onSearch={onSearch}
              data={visits}
              isScrollX
              pagination={pagination}
              isLoading={isLoading}
              onChange={onChangeTable}
              button={
                (userContext.role === ROLE.RR_STAFF ||
                  userContext.role === ROLE.LAB_STAFF) && (
                  <Button
                    onClick={_toggleModalAddVisit}
                    className={`hs-btn-outline ${ROLE.RR_STAFF}`}
                    icon="plus"
                  >
                    Tambah Kunjungan
                  </Button>
                )
              }
            />
          </div>
        </div>
      </div>
      {isVisitFormOpen && (
        <FormKunjungan
          reload={reload}
          toggleModal={_toggleModalAddVisit}
        ></FormKunjungan>
      )}
    </React.Fragment>
  );
};
