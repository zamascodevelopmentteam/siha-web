import React from "react";
import { Typography } from "components";

export const Summary = () => {
  return (
    <React.Fragment>
      <div className="col-4 pl-0">
        <Typography
          fontSize="18px"
          fontWeight="bold"
          color="RR_STAFF"
          className="d-block mb-3"
        >
          Data Kunjungan
        </Typography>
      </div>
    </React.Fragment>
  );
};
