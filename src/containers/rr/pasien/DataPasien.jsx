import React, { useState } from "react";
import FormPasien from "./form.handler";

import { Button } from "antd";
import { DataTable } from "components";
import { Link } from "react-router-dom";
import { ROLE, ROLEURL, ENUM } from "constant";

export const DataPasien = ({
  patients,
  pagination,
  onChangeTable,
  onSearch,
  isLoading,
  reload,
  role
}) => {
  const [isAddPatientOpen, setAddPatientOpen] = useState(false);

  const _toggleModalAddPatient = () => {
    if (isAddPatientOpen) {
      reload();
    }
    setAddPatientOpen(!isAddPatientOpen);
  };

  const columns = [
    {
      title: "Nama",
      dataIndex: "name",
      width: "40%"
    },
    {
      title: "Status HIV Pasien",
      dataIndex: "statusPatient",
      render: statusPatient => ENUM.LABEL_STATUS_PATIENT[statusPatient],
      width: "40%"
    },
    {
      title: "Aksi",
      key: "action",
      render: item => {
        return (
          <Link
            to={`/${ROLEURL[role || ROLE.RR_STAFF]}/data-pasien/${item.id}`}
            className={`ant-btn hs-btn ${role ||
              ROLE.RR_STAFF} ant-btn-primary ant-btn-sm`}
          >
            Detail
          </Link>
        );
      },
      width: "20%"
    }
  ];

  return (
    <React.Fragment>
      {isAddPatientOpen && <FormPasien toggleModal={_toggleModalAddPatient} />}
      <div className="container h-100 pt-4">
        <div className="row">
          <div className="col-12 bg-white rounded p-4 border">
            <DataTable
              title="Pasien UPK"
              onSearch={onSearch}
              columns={columns}
              data={patients}
              pagination={pagination}
              isLoading={isLoading}
              onChange={onChangeTable}
              button={
                role === ROLE.RR_STAFF && (
                  <Button
                    onClick={_toggleModalAddPatient}
                    className={`hs-btn-outline ${ROLE.RR_STAFF}`}
                    icon="plus"
                  >
                    Tambah Pasien
                  </Button>
                )
              }
            />
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};
