import React, { useState, useEffect } from "react";
import API from "utils/API";
import axios from "axios";
import { openNotification } from "utils/Notification";

import { ModalPatient } from "components";

const patientEmptyForm = {
  nik: "",
  name: "",
  addressKTP: "",
  addressDomicile: "",
  dateBirth: "",
  gender: "",
  statusPatient: "",
  phone: "",
  namaPmo: "",
  hubunganPmo: "",
  noHpPmo: "",
  tglMeninggal: ""
};

const Handler = ({ toggleModal }) => {
  let source = axios.CancelToken.source();
  const [formData, setFormData] = useState(patientEmptyForm);
  const [isLoading, setLoading] = useState(false);

  const _handleChange = (name, value) => {
    setFormData({
      ...formData,
      [name]: value
    });
  };

  const _onSubmit = () => {
    setLoading(true);
    API.Patient.create(source.token, formData)
      .then(() => {
        setLoading(false);
        openNotification("success", "Berhasil", "Pasien berhasil ditambahkan!");
        toggleModal();
      })
      .catch(e => {
        openNotification("error", "Gagal", e.message || String(e.data ? e.data.message : "").replace(/_/g, " "));
        console.error("e: ", e);
        setLoading(false);
      });
  };

  useEffect(() => {
    return () => {
      source.cancel("request canceled");
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <ModalPatient
      onCancel={toggleModal}
      onOk={_onSubmit}
      isLoading={isLoading}
      onChange={_handleChange}
    />
  );
};

export default Handler;
