import React, { useEffect, useState } from "react";
import API from "utils/API";
import axios from "axios";
import { useParams } from "react-router";

import { ProfilePasien } from "./ProfilePasien";

const patientInitialState = {
  nik: "",
  name: "",
  addressKTP: "",
  addressDomicile: "",
  dateBirth: "",
  gender: "",
  phone: "",
  statusPatient: "",
  weight: "",
  height: "",
  namaPmo: "",
  hubunganPmo: "",
  noHpPmo: ""
};

// const userInitialState = {
//   nik: "",
//   name: "",
//   addressKTP: "",
//   addressDomicile: "",
//   dateBirth: "",
//   gender: "",
//   phone: "",
//   statusPatient: "",
//   weight: "",
//   height: "",
//   namaPmo: "",
//   hubunganPmo: "",
//   noHpPmo: ""
// };

const Handler = ({ role }) => {
  let source = axios.CancelToken.source();
  const { id } = useParams();
  const [patient, setPatient] = useState(patientInitialState);
  // const [user, setUser] = useState(userInitialState);
  const [isLoading, setIsLoading] = useState(false);

  const _reload = () => {
    _getPatient();
  };

  const _getPatient = () => {
    setIsLoading(true);
    API.Patient.getByID(source.token, id)
      .then(data => {
        setPatient(data.data || patientInitialState);
      })
      .catch(e => {
        console.error("e: ", e);
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  // const _getUser = () => {
  //   setIsLoading(true);
  //   API.Patient.getByID(source.token, id)
  //     .then(data => {
  //       console.log("data: ", data);
  //       setPatient(data.data || patientInitialState);
  //     })
  //     .catch(e => {
  //       console.error("e: ", e);
  //     })
  //     .finally(() => {
  //       setIsLoading(false);
  //     });
  // };

  useEffect(() => {
    _getPatient();

    return () => {
      source.cancel("request canceled");
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <ProfilePasien
      patient={patient}
      isLoading={isLoading}
      reload={_reload}
      role={role}
    />
  );
};

export default Handler;
