import React, { useState } from "react";

import { Button } from "antd";
import {
  ModalHIV,
  ModalMedicine,
  ModalPDP,
  ModalRecipe,
  ModalVLCD4
} from "components";
import { Summary } from "./Summary";
import { useHistory } from "react-router";

import { ROLE } from "constant";

import DataKunjungan from "./DataKunjungan";
import RiwayatHIV from "./RiwayatHIV";
import RiwayatIMS from "./RiwayatIMS";
import ConvertODHA from "./ConvertODHA";
import ConfirmOTP from "./ConfirmOTP";

import ModalAddIMS from './modal'

export const ProfilePasien = ({ patient, reload, role }) => {
  const [isHIVOpen, setHIVOpen] = useState(false);
  const [isODHAOpen, setODHAOpen] = useState(false);
  const [isOTPOpen, setOTPOpen] = useState(false);
  const [isIMSOpen, setIMSOpen] = useState(false);
  const [isMedicineOpen, setMedicineOpen] = useState(false);
  const [isPDPOpen, setPDPOpen] = useState(false);
  const [isRecipeOpen, setRecipeOpen] = useState(false);
  const [isVLCDOpen, setVLCCDOpen] = useState(false);

  const { goBack } = useHistory();

  const _goBack = () => {
    goBack();
  };

  const _toggleModal = {
    HIV: () => setHIVOpen(!isHIVOpen),
    IMS: () => setIMSOpen(!isIMSOpen),
    VLCD: () => setVLCCDOpen(!isVLCDOpen),
    ODHA: (isReload = false) => {
      if (isReload) {
        reload();
      }
      setODHAOpen(!isODHAOpen);
    },
    OTP: () => setOTPOpen(!isOTPOpen),
    PDP: () => setPDPOpen(!isPDPOpen),
    Medicine: () => setMedicineOpen(!isMedicineOpen),
    Recipe: () => setRecipeOpen(!isRecipeOpen)
  };

  return (
    <React.Fragment>
      <div className="container h-100 pt-4">
        <div className="row mb-3">
          <div className="col-12">
            <Button
              className={`hs-btn-outline ${role || ROLE.RR_STAFF} `}
              icon="left"
              onClick={_goBack}
            >
              Kembali
            </Button>
          </div>
        </div>
        <div className="row mb-3">
          <div className="col-12 ">
            <Summary
              nik={patient.nik}
              name={patient.name}
              status={patient.statusPatient}
              toggleModalODHA={_toggleModal.ODHA}
              toggleModalOTP={_toggleModal.OTP}
            />
          </div>
        </div>
        <div className="row mb-3">
          <div className="col-6">
            <RiwayatHIV />
          </div>
          <div className="col-6">
            <RiwayatIMS />
          </div>
        </div>
        <div className="row mb-4">
          <div className="col-12">
            <DataKunjungan
              toggleModalVLCD={_toggleModal.VLCD}
              toggleModalHIV={_toggleModal.HIV}
              toggleModalIMS={_toggleModal.IMS}
              toggleModalPDP={_toggleModal.PDP}
              toggleModalMedicine={_toggleModal.Medicine}
              toggleModalRecipe={_toggleModal.Recipe}
            />
          </div>
        </div>
      </div>

      {isODHAOpen && <ConvertODHA toggleModal={_toggleModal.ODHA} />}

      {isOTPOpen && <ConfirmOTP toggleModal={_toggleModal.OTP} />}

      {/* {isOTPOpen && (
        <ModalOTP
          onCancel={_toggleModal.OTP}
          onOk={_toggleModal.OTP}
          isLoading={false}
          role="RR_STAFF"
        />
      )} */}

      {isVLCDOpen && (
        <ModalVLCD4
          onCancel={_toggleModal.VLCD}
          onOk={_toggleModal.VLCD}
          isLoading={false}
          role={role || ROLE.RR_STAFF}
        />
      )}

      {isHIVOpen && (
        <ModalHIV
          onCancel={_toggleModal.HIV}
          onOk={_toggleModal.HIV}
          isLoading={false}
          role={role || ROLE.RR_STAFF}
        />
      )}

      {isIMSOpen && (
        <ModalAddIMS
          onCancel={_toggleModal.IMS}
          onOk={_toggleModal.IMS}
          isLoading={false}
          role={role || ROLE.RR_STAFF}
        />
      )}

      {isMedicineOpen && (
        <ModalMedicine
          onCancel={_toggleModal.Medicine}
          onOk={_toggleModal.Medicine}
          isLoading={false}
          role={role || ROLE.RR_STAFF}
        />
      )}

      {isPDPOpen && (
        <ModalPDP
          onCancel={_toggleModal.PDP}
          onOk={_toggleModal.PDP}
          isLoading={false}
          role={role || ROLE.RR_STAFF}
        />
      )}

      {isRecipeOpen && (
        <ModalRecipe
          onCancel={_toggleModal.Recipe}
          onOk={_toggleModal.Recipe}
          isLoading={false}
          role={role || ROLE.RR_STAFF}
        />
      )}
    </React.Fragment>
  );
};
