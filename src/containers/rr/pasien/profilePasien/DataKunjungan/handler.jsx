import React, { useEffect, useState } from "react";
import API from "utils/API";
import axios from "axios";
// import shortid from "shortid";

import { useParams } from "react-router";
import { DataKunjungan } from "components";
import { ROLE } from "constant";

const Handler = ({
  toggleModalHIV,
  toggleModalIMS,
  toggleModalPDP,
  toggleModalMedicine,
  toggleModalRecipe,
  toggleModalVLCD
}) => {
  let source = axios.CancelToken.source();
  const { id } = useParams();
  const [visits, setVisits] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  const _reload = () => {
    _loadData();
  };

  const _loadData = () => {
    setIsLoading(true);
    API.Visit.listByPatientID(source.token, id)
      .then(data => {
        setVisits(data.data || []);
      })
      .catch(e => {
        console.error("e: ", e);
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  useEffect(() => {
    _loadData();

    return () => {
      source.cancel("request canceled");
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <DataKunjungan
      data={visits}
      isLoading={isLoading}
      reload={_reload}
      role={ROLE.RR_STAFF}
      toggleModalVLCD={toggleModalVLCD}
      toggleModalHIV={toggleModalHIV}
      toggleModalIMS={toggleModalIMS}
      toggleModalPDP={toggleModalPDP}
      toggleModalMedicine={toggleModalMedicine}
      toggleModalRecipe={toggleModalRecipe}
    />
  );
};

export default Handler;
