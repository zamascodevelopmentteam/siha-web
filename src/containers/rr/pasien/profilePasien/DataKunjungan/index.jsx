import React from "react";

import DataKunjungan from "./handler";

const index = ({
  toggleModalHIV,
  toggleModalIMS,
  toggleModalPDP,
  toggleModalMedicine,
  toggleModalRecipe,
  toggleModalVLCD
}) => (
  <DataKunjungan
    toggleModalVLCD={toggleModalVLCD}
    toggleModalHIV={toggleModalHIV}
    toggleModalIMS={toggleModalIMS}
    toggleModalPDP={toggleModalPDP}
    toggleModalMedicine={toggleModalMedicine}
    toggleModalRecipe={toggleModalRecipe}
  />
);

export default index;
