import React from "react";

import ProfilePasienHandler from "./handler";

const index = ({ role }) => <ProfilePasienHandler role={role} />;

export default index;
