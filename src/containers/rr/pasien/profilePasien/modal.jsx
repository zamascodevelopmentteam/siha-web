import React from "react";
import { ENUM, ROLE } from "constant";
import { Modal, Button, Select } from "antd";
import { Label } from "../../../../components/modal/style";
// import { Typography } from "components";
// import moment from "moment";
// import { validateForm } from "../../../../utils/validator";

const { Option } = Select;
const { ALASAN_TEST_IMS, KELUHAN_IMS } = ENUM;
// let requiredField = {
//   tanggalKunjunga: "Tanggal Kunjungan",
//   kunjunganKe: "Kunjungan Ke",
//   kelompokPopulasi: "Kelompok Populasi",
//   lsmRujukan: "LSM Rujukan",
//   alasanKunjungan: "Alasan Kunjungan",
// }

const ModalPatient = ({
  onOk,
  onCancel,
  isLoading,
  role,
  onChange,
  data,
  isView,
  // isEdit,
  // gender
}) => {
  // const _handleChange = e => {
  //   onChange(e.target.name, e.target.value);
  // };

  const _handleChangeSelect = (name, value) => {
    onChange(name, value);
  };

  // const _handleChangeDate = date => {
  //   onChange("dateBirth", date.toISOString());
  // };

  // const _handleChangeDateMeninggal = date => {
  //   onChange("tglMeninggal", date.toISOString());
  // };

  const _handleSubmit = e => {
    e.preventDefault();
    onOk()
  };

  return (
    <Modal
      title="Pemeriksaan IMS"
      visible
      style={{ top: 30 }}
      onOk={onOk}
      maskClosable={false}
      onCancel={onCancel}
      footer={null}
    >
      <form onSubmit={_handleSubmit}>

        {!isView && (
          <React.Fragment>
            {/* <Label>
              LSM Rujukan
            </Label>
            <Select
              placeholder="Masukan LSM Rujukan"
              style={{ width: "100%" }}
              className="mb-3"
              name="lsmReferral"
              value={data.lsmReferral.length ? data.lsmReferral : undefined}
              onChange={value => _handleChangeSelect("lsmReferral", value)}
              required
            >
              <Option disabled value={null}>Pilih LSM Rujukan</Option>
              {Object.keys(LSM_RUJUKAN).map(key => (
                <Option key={LSM_RUJUKAN[key]} value={LSM_RUJUKAN[key]}>
                  {LSM_RUJUKAN[key]}
                </Option>
              ))}
            </Select> */}

            {/* <Input
              placeholder="Masukan LSM Rujukan"
              style={{ width: "100%" }}
              className="mb-3"
              name="lsmReferral"
              value={data.lsmReferral}
              onChange={_handleChange}
              required
              disabled={role === ROLE.DOCTOR}
            /> */}

            <Label>
              Alasan Kunjungan
            </Label>
            <Select
              placeholder="Pilih Alasan Kunjungan"
              style={{ width: "100%" }}
              className="mb-3"
              name="reasonVisit"
              value={data.reasonVisit && data.reasonVisit.length ? data.reasonVisit : undefined}
              onChange={value => _handleChangeSelect("reasonVisit", value)}
              required
              disabled={role === ROLE.DOCTOR}
            >
              <Option disabled value={null}>
                Pilih Alasan Kunjungan
              </Option>
              {Object.keys(ALASAN_TEST_IMS).map(key => (
                <Option key={ALASAN_TEST_IMS[key]} value={ALASAN_TEST_IMS[key]}>
                  {ALASAN_TEST_IMS[key]}
                </Option>
              ))}
            </Select>

            <Label>
              Keluhan IMS <span className="label--required">* Di isi oleh Dokter</span>
            </Label>
            <Select
              placeholder="Pilih Keluhan IMS"
              style={{ width: "100%" }}
              className="mb-3"
              name="complaint"
              disabled={role !== ROLE.DOCTOR}
              value={data.complaint && data.complaint.length ? data.complaint : undefined}
              onChange={value => _handleChangeSelect("complaint", value)}
            >
              <Option disabled value={null}>
                Pilih Keluhan IMS
              </Option>
              {Object.keys(KELUHAN_IMS).map(key => (
                <Option key={KELUHAN_IMS[key]} value={KELUHAN_IMS[key]}>
                  {KELUHAN_IMS[key]}
                </Option>
              ))}
            </Select>

          </React.Fragment>
        )}


        <div className="d-flex justify-content-end mt-2">
          <Button key="back" type="danger" onClick={onCancel}>
            Batal
            </Button>

          <Button
            key="submit"
            type="primary"
            className={`hs-btn ${role} ml-2`}
            loading={isLoading}
            htmlType="submit"
          >
            Simpan
            </Button>
        </div>
      </form>
    </Modal>
  );
};

export default ModalPatient;
