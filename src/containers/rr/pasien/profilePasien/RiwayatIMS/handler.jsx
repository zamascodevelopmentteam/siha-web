import React, { useEffect, useState } from "react";
import API from "utils/API";
import axios from "axios";
import { useParams } from "react-router";
import { ROLE } from "constant";

import { RiwayatIMS } from "components";

const Handler = () => {
  let source = axios.CancelToken.source();
  const { id } = useParams();
  const [examList, setExamList] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  const _reload = () => {
    _loadData();
  };

  const _loadData = () => {
    setIsLoading(true);
    API.Exam.listIMSByPatientID(source.token, id)
      .then(data => {
        setExamList(data.data || []);
      })
      .catch(e => {
        console.error("e: ", e);
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  useEffect(() => {
    _loadData();

    return () => {
      source.cancel("request canceled");
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <RiwayatIMS
      data={examList}
      isLoading={isLoading}
      reload={_reload}
      role={ROLE.RR_STAFF}
    />
  );
};

export default Handler;
